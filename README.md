<div align="center">

A Quasi-Continuum Non-Equilibrium Solver

[![pipeline status](
    https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/badges/main/pipeline.svg?ignore_skipped=true)](
    https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/commits/main)
[![License](
    https://img.shields.io/badge/License-GPLv3-blue.svg)](
    https://www.gnu.org/licenses/gpl-3.0)
[![code style: clang-format](
    https://img.shields.io/badge/code%20style-clang--format-000000.svg)](
    https://clang.llvm.org/docs/ClangFormat.html)
[![code linter: clang-tidy](
    https://img.shields.io/badge/clang--tidy-checked-blue)](
    https://clang.llvm.org/extra/clang-tidy/)
[![DOI](
    https://img.shields.io/badge/DOI-10.5905%2Fethz--1007--824-blue)](
    https://doi.org/10.5905/ethz-1007-824)


**[Build](#build)** •
**[Usage](#run)** •
**[Docs](https://qc.mm.ethz.ch/aqcnes/)**

# AQCNES

</div>

## Build

First step: Clone the repo or get its source code via
```sh
wget -O - \
 "https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/archive/main/aqcnes-main.tar.gz" \
 | tar -xvzf -
```

The project uses [CMake](https://cmake.org) as its build system
generator. The following third party libraries are required and
located using CMake's ```find_package```.

- [Boost](https://www.boost.org) (components: program_options, mpi, serialization): version 1.67
- [Eigen](http://eigen.tuxfamily.org): version 3.4
- [MPI](https://cmake.org/cmake/help/latest/module/FindMPI.html): version 3.1
- [PETSc](https://www.mcs.anl.gov/petsc/): version 3.15
- [CGAL](https://www.cgal.org/): version 5.4
- [PnetCDF](https://parallel-netcdf.github.io/): Version 1.12
- [nlohmannjson](https://github.com/nlohmann/json): Version 3.10
- [Scotch/ParMETIS](https://www.labri.fr/perso/pelegrin/scotch/)
- [qcmesh](https://qc.mm.ethz.ch/qcmesh/): Version 1.1.0
- [VTK](https://vtk.org/): Version 9.3

Optionally:
- [torch](https://pytorch.org/cppdocs/installing.html)
- [openKIM kim-api](https://openkim.org/): Version 2.2

Optionally (run tests):
- [Google Test](https://github.com/google/googletest): version 1.8
- [python >= 3.8](https://www.python.org/)
- [check-jsonschema](https://github.com/python-jsonschema/check-jsonschema)

The version numbers specified above are tested in our CI
pipeline.
Different versions often also work. However, in libraries such as
e.g. petsc, there are updates that potentially can change the
computation results significantly.

Of course, these libraries are covered by their own license terms.

Once you have installed these libraries, run CMake to build the project, choosing a location to install the library to by specifying `CMAKE_INSTALL_PREFIX`. Of course, depending on your setup, you might need to add a `-DCMAKE_PREFIX_PATH='...'` parameter to tell CMake the location of the third party library installations.

```sh
cmake -B build -S src -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Werror -Wall -Wextra" -G Ninja
cmake --build build --target all
```

Of course, feel free to use a different build type (e.g. `Debug`, `RelWithDebugInfo`).

### Building on Euler

If you are building on Euler, all the necessary dependencies are already available.
Therefore, using a Docker container is not necessary.
First, load the required modules:

```sh
. requirements/euler.sh
```

Now you can run CMake to generate the build system and build the project:

```sh
cmake -B build -S src -G Ninja -DCMAKE_BUILD_TYPE=Release
cmake --build build --target all
```

Again, a different build type can be used depending on the use case.

### Using the build container

In the following we will use the image built in CI to run a container.
Note that the path to the image contains the name of the branch that
built it, so feel free to change that part if you've modified the
Docker environment for your branch.

When running a container and bind mounting folders of the host system
into that container, one has to deal with the problem of how to make
sure that the container correctly sets the correct owner for newly
created files.

For example, if you run a container with docker and the user inside
the container is root, then newly created files in a bind mount will
also be owned by root outside the container on the host system.
Below, there are listed 3 methods to solve this problem.

**Method 1:** Using rootless docker 🐋

Rootless docker only simulates root privileges for the user in the
container. Therefore the user in the container has correct permissions
by default.

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

**Method 2:** Using docker 🐋, specify uid at runtime

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work --user $(id --user) registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

Then run the above build commands.

**Method 3:** Use podman 🦭

The user inside the container will be root but a bind
mount will be mapped correctly: inside the container, files will be
owned by root, outside they will be owned by the host OS user.

Run:

```sh
podman run -it --rm -v .:/work -w /work --user $(id --user) --userns=keep-id registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

ℹ Here, podman also allows to mount `.` (you dont need the `"$(pwd)"`).

## Run

1. Create a project directory (we will refer to it as `<project_dir>`)
   ```sh
   mkdir <project_dir>
   ```
2. Copy [data/input/userinput.json](data/input/userinput.json) and
   [data/lattice_gen_params/latticegenerator_parameters.json](data/lattice_gen_params/latticegenerator_parameters.json)
   to your project directory.
   ```sh
   cp data/input/userinput.json data/lattice_gen_params/latticegenerator_parameters.json <project_dir>
   ```
   These are the userinput files for running the code and building the initial mesh file respectively. 
3. Generate an initial mesh with a lattice generator by using `mesh_generator` using
   ```sh
   mpirun -np <N> <builddir>/main/mesh_generator
   ```
   inside the project directory (`<N>` is the number of cores) or
   ```sh
   mpirun -np <N> <builddir>/main/mesh_generator <project_dir>
   ```
   from an arbitraty working directory.
   
   For a list of supported command line parameters, use
   ```sh
   <builddir>/main/mesh_generator --help
   ```
   To customize the generated mesh (you might want to
   have lesser elements for smaller `N`), change `n_coarsening_levels`
   to `1` and `atomistic_domain` to `22.0, 22.0, 22.0` in
   `latticegenerator_parameters.json`:
   
   ```json
   "n_coarsening_levels": 1
   "atomistic_domain":[22.0, 22.0, 22.0]
   ```
   
   The initial mesh file will be saved in `mesh_files/XYZ.nc`
4. Once you have the initial file, put its path in `userinput.json` at
   `mesh_file` field:
   
   ```json
   "SimulationSettings": {"mesh_file" : "/path/to/your/NanoIndentation_XXXXXX.nc"}
   ```

Now you should be able to run

```sh
mpirun -np <N> <builddir>/main/Remeshing3D
```

## Running tests

Run

```sh
CTEST_OUTPUT_ON_FAILURE=1 cmake --build build --target test
```

You can omit tests by using the `GTEST_FILTER` (see
[.gitlab-ci.yml](.gitlab-ci.yml) for an example).

or
```sh
cd build
ctest
```

If you want to include a mesh repair test involving a mesh file,
then set the environment variable `QC_MESH_FILE` (see [.gitlab-ci.yml](.gitlab-ci.yml)).

## Contribute

Please read our [contribution guidelines](./CONTRIBUTING.md)!
