#!/bin/bash

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

function diff_dat () {
    python3 - <<EOF "$1" "$2"
import sys
import numpy as np
try:
   data = np.loadtxt(sys.argv[1])
   reference_data = np.loadtxt(sys.argv[2])
except FileNotFoundError as e:
   sys.stderr.write(f"diff_dat: {e}\n")
   sys.exit(1)
try:
   np.testing.assert_allclose(data, reference_data)
except AssertionError as e:
   sys.stderr.write(format(e) + "\n")
   sys.exit(1)
EOF
}

src_dir=$(realpath $(dirname "$0")/..)

# Prepare an out of source testing environment:
work_dir=$(mktemp -d)
mkdir "$work_dir"/build
ln -s "${src_dir}/build/main/" "${work_dir}/build/main"
ln -s "${src_dir}/data" "${work_dir}/data"
cp -r "${src_dir}/examples/" "${work_dir}/examples/"

examples=("$work_dir"/examples/*/)
return_code=0
if [ $# -gt 0 ] ; then examples=("$@") ; fi

# Run examples in testing environment:
for dir in "${examples[@]}" ; do
    pushd "$dir" > /dev/null
    echo "> Running example ${dir}"
    ref="$(ls *.ref)"
    local_return_code=0
    if [ -f run.sh ] ; then ./run.sh > /dev/null || local_return_code=1 ; fi
    if [ -f run.py ] ; then python3 run.py > /dev/null || local_return_code=1 ; fi
    diff_dat "${ref%.ref}" "$ref" || local_return_code=1
    if [ $local_return_code -eq 1 ] ; then
        return_code=1
        echo "🛑 $dir" >&2
    fi
    popd > /dev/null
done

rm -rf "$work_dir"
[ $return_code -eq 1 ] && echo "💥" >&2 && exit 1
echo "✅"
exit 0
