#!/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Helper script to parametrize and run a user input json file."""

import json
import subprocess

temps = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

for temp in temps:
    with open("userinput.json", "rb") as stream:
        data = json.load(stream)
    data["QC:input::Userinput"]["ThermalSettings"]["T_ref"] = temp

    with open("userinput.json", "w", encoding="utf-8") as stream:
        json.dump(data, stream, indent=2)

    subprocess.check_call("../../build/main/InfiniteModelRelaxation")
