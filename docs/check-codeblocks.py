#!/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Consistency checker of markdown code blocks with source files.

Searches for code blocks (```...```) in md files.
For each code block with an attribute `src=<path>[:<line_number>]`,
it will check if the file at `<path>`
contains the content of the code block. If `:<line_number>` is specified
the code block has to present at the line number `<line number>`.
On mismatches, exit with status 1.
"""

import difflib
import glob
import os
import sys
from collections.abc import Iterable
from dataclasses import dataclass

__version__ = "0.0.1"


def check(doc_root: str, src_root: str, src_attr: str) -> None:
    """Scan for inconsistencies between examples and codeblocks in docs."""
    errors = []
    md_files = collect_md_files(doc_root)
    for md_file in md_files:
        for codeblock in find_code_blocks(md_file, read_file(md_file), src_attr):
            src_path = os.path.realpath(os.path.join(src_root, codeblock.src_path))
            try:
                src = read_file(src_path)
            except FileNotFoundError:
                errors.append(f"{md_file}: Source file does not exist: {src_path}")
                continue

            if not codeblock.src_line_number:
                if "\n".join(codeblock.code) not in src:
                    errors.append(
                        f"{md_file}:{codeblock.doc_line_number}: "
                        f"Could not find code block in {src_path}"
                    )
            else:
                delta = diff(
                    src_path,
                    src.splitlines(),
                    codeblock.src_line_number,
                    codeblock.code,
                    md_file,
                )
                if delta is not None:
                    errors.append("".join(delta))
    if errors:
        raise RuntimeError(
            "💥 Code block inconsistencies detected:\n" + "\n".join(errors)
        )


def collect_md_files(root: str) -> list[str]:
    """Recursively find md files in `root`."""
    return glob.glob(f"{root}/**/*.md", recursive=True)


def read_file(path: str) -> str:
    """Open and read a file."""
    with open(path, encoding="utf-8") as stream:
        return stream.read()


def diff(
    src_path: str,
    src_code: list[str],
    src_line_no: int,
    doc_code: list[str],
    doc_path: str,
) -> Iterable[str] | None:
    """Compare the content of `path` at line number `line_no` to `code`.

    On a mismatch return a diff using `doc_path` as a caption for the
    version given in `code`.
    """
    reference_code = src_code[src_line_no - 1 : src_line_no - 1 + len(doc_code)]
    if reference_code != doc_code:
        return difflib.context_diff(
            [line + "\n" for line in doc_code],
            [line + "\n" for line in reference_code],
            fromfile=doc_path,
            tofile=src_path,
        )
    return None


@dataclass
class CodeBlock:
    """Content and metadata of a markdown codeblock."""

    path: str
    src_path: str
    src_line_number: int | None
    doc_line_number: int
    code: list[str]


def find_code_blocks(path: str, body: str, src_attr: str) -> Iterable[CodeBlock]:
    """Extract code blocks from `body`.

    Returns an iterable of tuples consisting
    of the path and line number (specified by the class argument of the code
    block) of the reference code and the code contained in the code block.
    """
    lines = body.splitlines()
    start_delimiters: list[CodeBlockDelimiter] = []
    code_block_lines = []
    for n, line in enumerate(lines, start=1):
        if start_delimiters and not line.startswith(" " * start_delimiters[-1].indent):
            msg = f"💥 {path}:{n}: Unexpected indent"
            raise RuntimeError(msg)
        delimiter = code_block_delimiter(line, n)
        if delimiter is None:
            if start_delimiters:
                code_block_lines.append(line)
            continue
        is_start = bool(delimiter.attributes) or not start_delimiters
        if is_start:
            start_delimiters.append(delimiter)
        else:
            code = [
                line.removeprefix(" " * delimiter.indent) for line in code_block_lines
            ]
            code_block_lines = []
            try:
                delimiter = start_delimiters.pop()
            except IndexError:
                msg = f"💥 {path}:{n}: Syntax error"
                raise RuntimeError(msg) from None
            if not start_delimiters:
                src = source_file(delimiter, src_attr)
                if src is None:
                    continue
                src_path, src_no = src
                if src_path:
                    yield CodeBlock(
                        path=path,
                        src_path=src_path,
                        src_line_number=src_no,
                        doc_line_number=delimiter.line_number,
                        code=code,
                    )


@dataclass
class CodeBlockDelimiter:
    """Opening or closing ``` delimiter of markdown codeblocks."""

    attributes: tuple[str, ...]
    line_number: int
    indent: int


def source_file(
    delimiter: CodeBlockDelimiter, attribute_name: str = "src"
) -> tuple[str, int | None] | None:
    """Extract the source file attribute of a code block, if one exists.

    Returns the file and optional line number if an attribute exists.
    """
    attr_prefix = attribute_name + "="
    src_attributes = [
        attr.removeprefix(attr_prefix).strip('"')
        for attr in delimiter.attributes
        if attr.startswith(attr_prefix)
    ]
    if not src_attributes:
        return None
    try:
        (raw_path,) = src_attributes
    except ValueError:
        msg = (
            f"💥 {delimiter.line_number}: Found multiple `{attr_prefix}` attributes:"
            f" {' '.join(delimiter.attributes)}"
        )
        raise RuntimeError(msg) from None
    if ":" not in raw_path:
        return raw_path, None
    path, line_no_raw = raw_path.split(":", 1)
    return path, int(line_no_raw)


def code_block_delimiter(line: str, line_number: int) -> CodeBlockDelimiter | None:
    """Scans a line if it is a markdown code block delimiter."""
    pos = line.find("```")
    if pos == -1 or (line[:pos] and not line[:pos].isspace()):
        return None
    attributes = line.strip().removeprefix("```")
    return CodeBlockDelimiter(
        tuple(a.strip() for a in attributes.split()), line_number, pos
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--doc", required=True, help="Folder to scan for docs")
    parser.add_argument(
        "--src",
        required=True,
        help="Base directory which gets prepended to source file annotations",
    )
    parser.add_argument(
        "--src-attr",
        default="src",
        help="Attribute name used to annotate the source of markdown code blocks",
    )
    args = parser.parse_args()
    try:
        check(args.doc, args.src, src_attr=args.src_attr)
    except RuntimeError as e:
        sys.stderr.write(e.args[0])
        sys.exit(1)
    sys.exit(0)
