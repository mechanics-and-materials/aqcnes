# Getting Started

## Cloning the repository
The source code is available [here](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes) and can be cloned by using:
```sh
git clone https://gitlab.ethz.ch/mechanics-and-materials/aqcnes.git
```
This will make a new directory called `aqcnes` in the current working directory. The following table gives an overview of the files and subdirectories in the source code:

|    Directory   | Contents  |
| ----------- | ----------- |
| cmake      | dependency discovery and unit test declarartion       |
| data   | sample user input `json` files and their schemas        |
| container   | dockerfile to build a docker image with all dependencies        |
| docs   | documentation markdown scripts        |
| examples   | resources for the exercises on [this page](docExamples.html)        |
| requirements   | required modules to run on the [ETH HPC cluster](https://scicomp.ethz.ch/wiki/Euler)        |
| src   | AQCNES source files       |
| README.md   | short description of the software         |

## Dependencies
Building AQCNES locally requires the installation of some dependencies which can be easily done using the commands in [container/Dockerfile](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/blob/main/container/Dockerfile?ref_type=heads).

Alternatively, you can also build AQCNES in a build container (e.g. [docker](https://www.docker.com/) or [podman](https://podman.io/)).

The mesh building and repair functionality of AQCNES is outsourced to a separate repository, known as [qcmesh](https://qc.mm.ethz.ch/qcmesh/). The source code for qcmesh is available [here](https://gitlab.ethz.ch/mechanics-and-materials/qcmesh).

## Installing docker 
Detailed environment dependent installation instructions can be found [here](https://docs.docker.com/engine/install/). However, we provide here a short summary for Linux Ubuntu users.
```sh
sudo apt install gnome-terminal
sudo apt-get update
sudo apt-get install ./docker-desktop-<version>-<arch>.deb
```
After successfull installation, the docker verison can be checked using:
```sh
docker --version
```

