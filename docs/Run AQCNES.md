# Run AQCNES

This page explains a basic workflow of generating an initial mesh followed by a quasistatic relaxation at finite temperature, once you have successfully [built AQCNES](docBuild%20AQCNES.html) in any `<builddir>` of your choice. 

## Initial Mesh Generation

You can start by generating a simple mesh using the `<build_dir>/main/mesh_generator` executable. For this, you need to have the `latticegenerator_parameters.json` input parameters file in the current working directory. A sample mesh input parameters file can be found [here](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/blob/main/data/lattice_gen_params/latticegenerator_parameters.json?ref_type=heads). For more information on the specific input parameters, click [here](docUserinput%20Parameters.html). The mesh generation can be executed simply by doing

   ```sh
   mpirun -np <N> <build_dir>/main/mesh_generator
   ```
   inside the project directory where `<N>` is the number of CPU cores.
   
The `mesh_generator` executable also supports a list of command line parameters which can be viewed using
   ```sh
   <build_dir>/main/mesh_generator --help
   ```

A successful execution (with the default input parameters) will create a lattice file called `my_lattice.txt` and an initial mesh file called `my_mesh_000365.nc` in the directory `mesh_files`. The suffix at the end of the mesh file name corresponds to the total number of atoms in the lattice. Apart from this, a file called `mesh_names.txt` is generated which contains the relative path to the initial mesh file, and `periodic_info.txt` contains information about the lowest and highest Cartesian bounds of the mesh. For mesh visualization, `vtu` files are also created inside the directory `initial_partition` which can be viewed using [Paraview](https://www.paraview.org/download/).

Note that the initial mesh file is unrepaired and can potentially have bad elements. The degenerate elements can be visualized using the file `./mesh_files/degenerate_elements.pvtu`.
 

## Quasistatic Relaxation

Once you have the initial mesh and lattice file, you can use the `<build_dir>/main/Relaxation3D_FINITE_K` to relax the mesh. For this, you need to have the `userinput.json` input parameters file in the current working directory, a sample of which can be found [here](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/blob/main/data/input/userinput.json?ref_type=heads). For more information on the specific input parameters, click [here](docUserinput%20Parameters.html).

If you did not use the default parameters for initial mesh generation, you might have to put the mesh file and lattice file path in `userinput.json` at `mesh_file` and `lattice_file` fields respectively:
   
   ```json
   "SimulationSettings": {
    "mesh_file" : "/path/to/your/mesh_file.nc",
    "lattice_file" : "/path/to/your/lattice_file.txt"
    }
   ```

You might also have to change the path to the interatomic potential SETFL file, depending on the directory you use to run this example:
   ```json
   "Potentials": {
      "material": {
                "$type": "SetflEAM",
                "potential_file_path": "aqcnes/data/potentials_NIST/copper/mishin/Cu.eam.alloy",
                "potential_material_name": "CuEAM",
                "funcfl_file": false
            }
    }
   ```

Now you should be able to run

```sh
mpirun -np <N> <build_dir>/main/Relaxation3D_FINITE_K 
```

Upon relaxation, two more directories are created. The `solution_files` directory consists of the unrelaxed and relaxed mesh visualization `pvtp` files, with the suffix `0` and `1` respectively. The `restart_files` directory consists of the unrelaxed and relaxed mesh files to restart the simulation with a relaxed mesh. 

The relaxation can also produce 4 additional output files based on the choices in the field `OutputPrintingChoices` in `userinput.json`. The contents of these files have been explained in detail below. Note that the output file names are suffixed with the material name assigned in the input field `potential_material_name` and the reference temperature assigned in the input field `T_ref`.

```json
"Potentials": {
  "material":   {
  "potential_material_name": "CuEAM" 
  }
}

"ThermalSettings": {
  "T_ref": 300.0
}
```

#### Energy Volume Output
The `energy_volume_***.dat` output file contains thermodynamic quantities for the relaxed mesh as a row vector in the following order (sample values from this exercise have been shown here).

```txt
temperature:                        300
mesh volume:                        2996.394698  
Helmholtz free energy:              -673.0034789   
average potential energy:           -776.3479841
average kinetic energy:             9.927164506
total no. of atomic_sites:          256 
average no. of solute atoms:        25.6 
periodic length X:                  14.41671589 
periodic length Y:                  14.41671589
periodic length Z:                  14.41671589
```

#### Per Atom Info 
The `per_atom_quantities_***.dat` file contains an array of thermodynamic quantities for each atomic site. Each row corresponds to an atom and contains information in the following order.
```txt
mean_coordinates:[x,y,z], average_potential_energy(eV), average_kinetic_energy(eV), temperature*entropy(eV), chemical_potentials, impurity_concentrations  
```

#### Simulation Box Bounds
The `simuation_box_bounds_***.dat` file contains the minimum coordinate and the lengths of relaxed periodic dimensions.
```txt
minimum coordinate in periodic directions:                      -7.208357947 -7.208357947 -14.41671589 
relaxed periodic lengths in periodic directions:                 14.41671589 14.41671589 14.41671589
```

#### Potential Log
The `potential_log.dat` contains computed electronic embedding values and pair potential values computed for every call to the interatomic potential. It is advised to turn off the printing of this file, otherwise it could lead to a huge data dump and could slow down the simulations.


The user is recommended to get familiarized with all userinput parameters, shown on the [next page](docUserinput%20Parameters.html). More working examples can be found on [this page](docExamples.html).
