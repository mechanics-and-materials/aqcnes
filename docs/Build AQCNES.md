# Building AQCNES with CMake

This page describes the steps to build AQCNES either on your local machine or using a build container. A description of the basic build options can also be found below.

## Build locally

To build locally, you must have all the [dependencies](docGetting%20Started.html#Dependencies) installed. 

Installing with CMake involves the generation of a build environment in a new `build` directory, followed by the actual compilation and linking of libraries to the executables. After the repository has been successfully cloned, use the following commands to build AQCNES.
```sh
cd aqcnes
cmake -B build -S src -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Werror -Wall -Wextra" -G Ninja
cmake --build build --target all
```

Of course, feel free to use a different build type (e.g. `Debug`, `RelWithDebugInfo`). This will create a new directory called `build` and produce all executables inside `build/main` directory.

## Using a build container

You may also use the image built in CI to run a container. When running a container and binding mounting folders of the host system into that container, one has to deal with the problem of how to make sure that the container correctly sets the correct owner for newly created files.

For example, if you run a container with docker and the user inside the container is root, then newly created files in a bind mount will also be owned by root outside the container on the host system. Below, there are listed 3 methods to solve this problem.

**Method 1:** Using rootless docker 🐋

Rootless docker only simulates root privileges for the user in the
container. Therefore the user in the container has correct permissions
by default.

Login (only required once):

```sh
docker login registry.ethz.ch
```

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

**Method 2:** Using docker 🐋, specify uid at runtime

Login (only required once):

```sh
docker login registry.ethz.ch
```

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work --user $(id --user) registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

Then run the above build commands.

**Method 3:** Use podman 🦭

The user inside the container will be root but a bind
mount will be mapped correctly: inside the container, files will be
owned by root, outside they will be owned by the host OS user.

Login (only required once):

```sh
podman login registry.ethz.ch
```

Run:

```sh
podman run -it --rm -v .:/work -w /work --user $(id --user) --userns=keep-id registry.ethz.ch/mechanics-and-materials/aqcnes/dev:main
```

Note that podman also allows to mount `.` (you do not need the `"$(pwd)"`).

Once you have used any of the three methods above to enter a docker container, you will find yourself inside a directory called `work` in the docker environment. The build process can then be continued in the usual way as described [above](#Build%20Locally).




## Basic build options

The build can be modified by adding or removing code features. For this, you need to set the CMake *variables* with either the `-D` command line flag while using `cmake`, or in the text mode of the GUI while using `ccmake`. The following table gives a list of the basic build options.

|  Build flag     |  Funcionality  | Default |
| ----------- | ----------- |  ----------- |
| FINITE_TEMPERATURE      |  Simulate at non-zero temperatures      | On |
|  MULTI_THREADING  |  Enable parallelization with openMP along with MPI    |   Off |
|  USE_KIM  | Use interatomic potentials from the [KIM](https://openkim.org/) repository | On |
|  USE_TORCH  |  Enable libtorch to use [GNN assisted phase space averaging](https://www.sciencedirect.com/science/article/pii/S0167663623001278)   | On |

