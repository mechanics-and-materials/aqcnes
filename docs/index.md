# ﻿AQCNES

*A Quasi-Continuum Non-Equilibrium Solver*

[![source: ETHZ GitLab](https://img.shields.io/badge/source-ETHZ-red?logo=gitlab)](
  https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/)
[![license: GPLv3](https://img.shields.io/badge/license-GPLv3-blue)](https://www.gnu.org/licenses/gpl-3.0)

![](nanoindentation_temperature.mp4)
![](GB.svg)
![](surfaces.svg)

AQCNES is a scalable C++ framework to simulate atomistic ensembles at finite temperatures in a spatial and temporal upscaled fashion. The implementation is based on the statistical-mechanics-based [Gaussian Phase Packets (GPP)](https://www.sciencedirect.com/science/article/pii/S0022509621001630?via%3Dihub) and [Quasi-Continuum (QC)](https://www.sciencedirect.com/science/article/pii/S0022509615000630?via%3Dihub) approach. The code has been developed by the [Mechanics & Materials lab](https://mm.ethz.ch/) at ETH Zurich.

AQCNES can be used to find finite-temperature relaxed configurations (in a statistical sense) of mesoscopic systems, which finds applications in solid-state materials science. It can also be used to simulate non-equilibrium mass diffusion using atom-hopping energy barriers computed on the fly.

> **NOTE:**  AQCNES is still under active development. New contents are being added to the code as well as the documentation. For the latest version, please go to our [repository](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes).

For citing *aqcnes*, please use the following:

[https://doi.org/10.5905/ethz-1007-774](https://doi.org/10.5905/ethz-1007-774)

Click [here](docGetting%20Started.html) to get started!

# Contributing and questions
If you wish to contribute to AQCNES or have some applications in mind, please check out our [contributing guidelines](docContributing%20Guidelines.html).
