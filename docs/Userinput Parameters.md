# User Input Parameters
This page provides a comprehensive description of the several parameters a user can play with while creating an initial mesh and performing a quasistatic relaxation.

## Lattice Generation
The table below explains the userinput parameters in `latticegenerator_parameters.json`.
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
| method      |  Type of lattice generator to use (see below for possible options)       | NanoIndentation |
| lattice_structure      |  Type of primitive lattice to build (supported values: BCC, FCC, SimpleCubic). Note that non-primitive lattices like HCP and Diamond are just FCC lattices with multiple seed points.       | FCC |
| lattice_parameter      |  Edge length of the unit cell. This information together with `lattice_structure` is used to initialize appropriate lattice vectors. | 3.615 |
| hcp_c_by_a | The axial ratio to use for building a hexagonal close packed crystal. | 1.632 | 
| seed_point_offset_coeffs | Seed points to use for lattice generation (in addition to `[0.0,0.0,0.0]`). Each entry defines a linear combination of the lattice vectors to obtain the additional seed point. Note that the additional seed points are ignored in coarse-grained regions.  | `[[ ]]` |
| make_atomistic_domain      |  Make a fully atomistic region followed by coarse-grained regions. A purely coarse-grained mesh is generated if this parameter is set to `false`  | true |
| n_coarsening_levels      |  Number of coarsening levels to add, including the `atomistic_domain`. Hence, a value of 1 will make just the fully atomistic domain.    | 2 |
| atomistic_domain      |  Maximum Cartesian dimensions of the atomistic domain on each side of the origin.   | `[7.3,7.3,7.3]` |
| coarsening_factors | Coefficients to multiply the atomistic lattice vectors for generating lattice points in coarse-grained regions. The atomic lattice vectors are multiplied by the values in this array to yield coarse-grained lattice vectors. | `[1.0,4.0]`  |
| domain_expansion_factors | Coefficients to determine the bounds of every coarsening level based on the `atomistic_domain`. | `[15.0, 40.0]` |
| rotation | Initial rotation matrix to rotate the generated lattice. Each vector entry corresponds to a row of the rotation matrix | `[[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]` |
| lattice_file_name | Relative path to save the lattice file. | "lattice_data" |
| name | path to save the lattice file relative to the directory `./mesh_files` | "mesh_data" |

AQCNES provides three major ways of creating an initial lattice. These correspond to the field `method` in the table above.
* **NanoIndentation**
This option is used to generate atomistic and/or coarse-grained monocrystalline samples. The following input variables need to be specified for using this option:
```json
"method": {
    "$type": "NanoIndentation",
    "domain_boundaries": {                 // specifies the lower and upper bounds of the simulation box 
      "lower": [
        -5000.0,
        -5000.0,
        -10000.0
      ],
      "upper": [
        5000.0,
        5000.0,
        0.1
      ]
    }
  }
```
Any lattice sites generated outside the `lower` and `upper` boundaries of the simulation domain are deleted.

* **SymmetricGrainboundaries**
This option is used to generate atomistic bicrystalline samples. The following input variables need to be specified for using this option:
```json
"method": {
    "$type": "SymmetricGrainboundaries", 
    "overlap_deletion_distance": 1.0,        // delete any lattice site that is closer than 1.0 to any another lattice site
    "delete_across_boundary": true,          // delete close enough lattices as specified above assuming periodic boundaries
    "domain_boundaries": {                   // specifies the lower and upper bounds of the simulation box 
      "lower": [
        -5000.0,
        -5000.0,
        -10000.0
      ],
      "upper": [
        5000.0,
        5000.0,
        0.1
      ]
    }
  }
```
This lattice generator is used to generate symmetric tilt grain boundaries. Any lattice sites generated outside the `lower` and `upper` boundaries of the simulation domain are deleted. The parameters `overlap_deletion_distance` and `delete_across_boundary` are used to avoid any two atoms being too close to each other to provide a better initial guess for the geometry.

* **ReadCoordinates**
This option provides the flexibility to generate a mesh of any possible geometry by simply reading the atomic coordinates of a previously generated lattice. The atomic coordinates file must be written in a [standard LAMMPS format](https://docs.lammps.org/read_data.html). The following input variables need to be specified for using this option:
```json
"method": {
    "$type": "SymmetricGrainboundaries", 
    "coordinates_file_name": "path/to/lattice_file"    
  }
```



## Quasistatic Relaxation
Each sub-header below corresponds to a section of the `userinput.json` input file. 

### SimulationSettings
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
| lattice_file      |  Path to the lattice file generated in Lattice Generation     | " " |
| mesh_file      |   Path to the mesh file generated in Lattice Generation   | " " |
| mesh_overlap_buffer_radius    |   Buffer padding (in $\AA$) used in addition to the cutoff radius of the interatomic potential while exchanging inter-proc and periodic halo mesh    |      1.5        |  
| minimum_barycenter_weight      |  Minimum sampling atom weight of the simplex centroid needed to create a central sampling atom      | 1.0 |
| neighbour_update_displacement      |   Maximum relative distance buildup of a neighbor from its sampling atom (in $\AA$) needed to regenerate the neighborhoods for that particular sampling atom     |  0.05 |
| neighbour_update_style      |  Criterion used to update the sampling atom neighbor lists. Can be set to VERLET or ADAPTIVE    | ADAPTIVE  |
| node_effective_radius      |   Radius of a simplex node used to compute the nodal sampling atom weights    | 2.5 |
| output_centro_symmetry      |  Compute and output nodal sampling atom centrosymmetries in `vtu` files     | true |
| output_initial_partition    |  Print a `vtu` file after mesh repair before beginning the quasistatic relaxation    | true |
| quad_type      |  The order of Gaussian quadrature used to compute phase averaged energy and forces. Possible options are 3 or 5    | 3 |
| remove_previous_neighbours      | Delete the previous neighbors after regeneration of sampling atom neighborhood     | true |
| verlet_buffer_factor      |  The coefficient to be multiplied to `neighbour_update_displacement` to obtain the buffer radius (in $\AA$) used in addition to the cutoff radius of the interatomic potential while gathering sampling atom neighbors     | 1.75  |

### ThermalSettings
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
|    T_ref        |    Temperature of the system in Kelvin     |   300.0           |
|   Phi_ref         |     Initial position entropy ($\phi = -\log \Sigma$) of the atoms, where $\Sigma = \langle \vert( \boldsymbol{q} - \bar{\boldsymbol{q}} \vert^2 \rangle $ is the position variance of the sampling atoms in $\AA^2$.    |    5.545         |
|   isentropic      |   Relaxes the ensemble isentropically if set to 'true', and isothermally if set to 'false'            |           false  |
|     T_min	       |    Minimum temperature allowed for any site while isentropic relaxation or heat transport simulations           |    0.01         |
|  T_max          |    Maximum temperature allowed for any site while isentropic relaxation or heat transport simulations           |    5000.0         |
|    heat_rate_tolerance_per_atom (*optional*)   |   Maximum heat rate allowed at a sampling atom. Relevant only for heat conduction simulations.                |   1.0e-3          |
|     onsager_coefficient (*optional*)   |   Onsager coefficient used for heat transport from a sampling atom to its neighbors. Relevant only for heat conduction simulations.               |      15.926734       |

### FIRERelaxation
This section includes the parameters for the optimizer FIRE (fast inertial relaxation engine). The optimum values for all of them have been borrowed from [this manuscipt](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.97.170201) and it is recommended to use the same values.

### PETSC
This section includes the parameters for [PetSc solvers](https://petsc4py.readthedocs.io/en/stable/manual/snes/). Integrating PetSc solvers with FIRE is an ongoing work right now.

### SolverTolerances
AQCNES requires the L1 norm of the physical, thermal, and periodic forces to reach below the user-specified tolerances to ensure convergence.
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
| max_force_per_atom | L1 force tolerance per atom |  5e-05  |
| max_thermal_force_per_atom | L1 thermal tolerance per atom  | 5e-05  |
| max_periodic_force_residue_per_atom | L1 periodic force tolerance | 1e-06  |
Note that the `max_periodic_force_residue_per_atom` is a global quantity and is ignored when simulating a non-periodic domain.


### Periodicity
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
| a_ref | A reference length (in Angstroms) to update the periodic lengths based on the periodic strains  | 3.61 |
| periodicity |  Defines whether the domain is periodic or not, along the x, y and z directions respectively. Each entry can either be a `null` or a struct with three parameters as shown below      |      `[null,null,null]`  |

```json
{
          "min_coordinate": -14.44,      // minimum value of any sampling atom coordinate in the corresponding Cartesian direction 
          "length": 28.88,               // periodic length of the simulation domain in the corresponding Cartesian direction
          "relaxation_constraint": false // Allow the periodic length to relax or not (analogous to NPT vs NVT simulations in MD)
        }

```

### Multispecies (*optional*)
AQCNES supports simulating the equilibrium properties of a multi-element system (e.g. a binary alloy) by specifying the following parameters. This section is completely optional and can be skipped if simulating pure species.
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
|  impurity_atomic_masses |      An array consisting of the atomic masses of the impurities. This includes vacancies (having a mass of 0.0) and any other alloying solutes.      |        `[0.0]`      |
|  initial_impurity_concentrations |   Concentration of solutes to be initialized at every atomic site    |   `[0.0]`      |
|  embedding_interpolation_coeffs |                  |        `[[1.0]]`      |
Note that if a vacancy needs to be specified together with other solutes, it should always be specified as the first entry of the array `impurity_atomic_masses` and `initial_impurity_concentrations`.


### Potentials
AQCNES provides the following options of specifying the inter-atomic potential to be used:
* **EAM potentials**:
An embedded atom method (EAM) potential can be used simply by specifying the `Potentials` section as shown below.
```json
"Potentials": {
            "material": {
                "$type": "SetflEAM",
                "potential_file_path": "path/to/eam/file",
                "potential_material_name": "user_specified_name",
                "funcfl_file": false
            }
        }
```
Note that the specified file is considered to be of a SETFL format by default. Make sure to specify `"funcfl_file": true` when using a FUNCFL file format. The `potential_material_name` is just a user-specified name of material which is appended to output file names.


* **MEAM potentials**:
Modified embedded atom method (MEAM) potentials can be used as shown below.
```json
"Potentials": {
            "material": {
                "$type": "Meam",
                "library_file_path": "path/to/meam_library_file",
                "parameter_file_path": "path/to/meam_parameter_file",
                "potential_material_name": "user_specified_name",
                "species": ["Cu","Al"]
            }
        }
```
The library and parameter files need to be specified for the MEAM potential separately. The field `species` should contain the chemical symbols of the elements present in your simulation. This is used to identify the correct parameters from the library file.

* **KIM potential repository**:
[OpenKIM](https://openkim.org/) is a repository of interatomic potentials which includes different types of pair, EAM, MEAM, and bond order potentials. Any potential available in this repository can be used in AQCNES by simply specifying the model name as shown below.
```json
"Potentials": {
            "material": {
                "$type": "Meam",
                "atomic_mass": 63.546,
                "model_name": "EAM_NN_Johnson_1988_Cu__MO_887933271505_002",
                "species": ["Cu"]
            }
        }
```
Note that OpenKIM potentials are optimised for molecular dynamics simulations and hence using them can be a bit slow for this framework.

* **GNN models**:
Using all the options specified above is combined with a third or fifth-order Gaussian quadrature for computing phase averaged energies and forces. However, this comes with its own set of limitations which can corrupt the simulation accuracy when dealing with amorphous systems or at higher temperatures (see [here](https://www.sciencedirect.com/science/article/pii/S0167663623001278) for further details). Hence, a GNN model can be used as a surrogate for phase-space averaging by specifying:
```json
"Potentials": {
            "material": {
                "$type": "Meam",
                "model_path": "path/to/trained_GNN_model",
                "atomic_mass": 63.546,
                "potential_material_name": "user_specified_name"
            }
        }
```


Apart from this, some simple potentials have also been hardcoded in the software. These include:
* [Extended Finnis Sinclair (EFS) potential](https://iopscience.iop.org/article/10.1088/0953-8984/18/19/008) for copper
* [Yukawa type pair potential](https://pubs.rsc.org/en/content/articlelanding/2018/cp/c8cp01313f) for silica


### OutputPrintingChoices
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
| energy_volume      |   Print the relaxed thermodynamic energies of the entire ensemble in in `energy_volume_***.dat`. Look [here](docRun%20AQCNES.html) for additional information about the output values.   | true |
| per_atom_info      |  Print an array of atom-wise relaxed thermodynamic values in `per_atom_quantities_***.dat`   | false |
| simulation_box_bounds      |  Print the minimum and maximum bounds of the relaxed domain in `simulation_box_bounds_***.dat`    | true |
| potential_log      |  Print the computed energy values for every call to the interatomic potential in `potential_log.dat`   | false |
| solution_files_frequency (*optional*)     |  Relaxed mesh visualization `vtu` files are dumped in the `solution_files` directory after every `n` time steps  | `null` |
| mid_fire_solution_files_frequency (*optional*)   |  Unrelaxed mesh visualization `vtu` files are dumped in the `solution_files` directory after every `n` FIRE iterations        | `null` |



### ConsolePrintingChoices
|  Variable Name     |  Functionality  | Default |
| ----------- | ----------- |  ----------- |
|  relaxation_steps   | Print the relaxation metrics for every iteration    |  true  |
|  nan_forces   |  Print the location of the sampling atom and its neighbor if a `nan` force is detected. The simulation terminates after this operation.    |  true  |
|  halo_mesh_exchange   |  Print the number of halo nodes and elements exchanged between processors    |   false |
|  regenerated_neighborhoods   |  Print the total number of nodal, central, inter-proc, and periodic neighborhoods regenerated at any iteration    |  false  |
|  neighborhood_and_force_time   |  Compute and print the time taken by each processor to generate neighborhoods and compute forces   |  false  |

### NeighbourDisplays
This section can be used to output additional visualization `vtu` files for user-specified sampling atoms. This can help visualize atomic neighborhood movements during extreme deformations or near critical material defects. Approximate initial locations of the desired sampling atoms can be specified as an array of arrays as shown below:
```json
"display_neighbourhoods_sampling_atoms": [
        [
          0.0,
          0.0,
          0.0
        ],
        [
          -5.41,
          -7.22,
          -5.41
        ],
        [
          5.41,
          -7.22,
          -5.41
        ]
      ]
```
The sampling atoms closest to the specified locations would be tracked along with their neighbors, and their information is dumped in a directory called `neighborhoods`.
