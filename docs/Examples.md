# Examples

Given below are three basic examples to get the user started with using the codebase.

## Thermal expansion of bulk copper

This example computes the relaxed lattice parameter and average
Hamiltonian per atom of bulk copper, as modeled by the [Mishin EAM
potential](https://www.ctcms.nist.gov/potentials/entry/2001--Mishin-Y-Mehl-M-J-Papaconstantopoulos-D-A-et-al--Cu-1/). For
this, a mesh generation is not needed as our statistical mechanics
approach can work with just one bulk atom and all of its
neighbors. because every atom is equivalent in a bulk. This is done
using the `InfiniteModelRelaxation` executable found inside
`build/main/`. The only requirement is a lattice file, which is used
to generate an initial lattice before relaxation. Additionally, we use
a python script to run the executable for 50K to 500K in steps of
50K. The lattice file, userinput json and python script can be found
inside `examples/copper_thermal_expansion`.

The example can be tried simply by executing

```sh
python run.py
```

Upon completion, two files will be generated in the working
directory. The file named `RelaxationInfo.txt` consists of the relaxed
bulk parameters for every temperature. Each row corresponds to entries
in the following order:

```
temperature(K), lattice_spacing(A), position_entropy, average_Hamiltonian(eV), Helmholtz_free_energy(eV)
```

The output file `TimeSeries.txt` consists of all the relaxation
metrics for every iteration. The executable `InfiniteModelRelaxation`
can also be used to compute finite-temperature bulk elastic constants
by specifying an external displacement gradient and fixing structural
relaxation in the userinput script.

```json
"ExternalIndenter": {
  "external_displacement_gradient": [
    [
      1.0,
      0.0,
      0.0
    ],
    [
      0.0,
      1.0,
      0.0
    ],
    [
      0.0,
      0.0,
      1.0
    ]
  ],
  "indenter_increment": 0.01
}
"Periodicity": {
  "a_ref": 3.61,
  "periodicity": [
    {
      "min_coordinate": -14.44,
      "length": 28.88,
      "relaxation_constraint": true
    },
    null,
    null
  ]
}
```

In the example above, the executable would hydrostatically dilate the
lattice and set the `relaxation_constraint` to `true`, thus allowing
only for thermal relaxation of the atoms. Finite-temperature bulk
mechanical properties for pure metals and metallic alloys can thus be
obtained using this executable with just one processor and being
orders of magnitude faster than using molecular dynamics simulations.

## Quasistatic relaxation of a bicrystal

This example computes the relaxed Helmholtz free energy of a periodic
copper bicrystal consisting of a $\Sigma~5$ symmetric tilt grain
boundary at its interface at $300~$K. For this purpose, the
`Relaxation3D_FINITE_K` executable found inside `build/main/` is
used. The files required for this exercise can be found inside
`examples/copper_grain_boundary_relaxation`. A small bicrystal is
created using the lattice generation json script
`latticegenerator_parameters.json`. Once inside the exercise
directory, this can be done simply by:

```sh src=examples/copper_grain_boundary_relaxation/run.sh
mpirun -np 1 ../../build/main/mesh_generator
```

This operation will generate an initial `.nc` mesh and a `.txt`
lattice file and save it in the `mesh_files` directory. The mesh file
name can also be found in `./mesh_files/mesh_names.txt`. Initial
periodic lengths are saved in `./mesh_files/periodic_info.txt`.
Now, we can proceed with the actual simulation. For this, we will need
the `userinput.json` script (also provided). The important parameters
to make sure the simulation works are summarized below:

These periodicity parameters make a geometry that is periodic in all
the Cartesian directions (x, y, and z).
Make sure to provide the correct initial periodic lengths, which can
be seen in `./mesh_files/periodic_info.txt`:

```json src=examples/copper_grain_boundary_relaxation/userinput.json
    "BoundaryConditions": {
      "a_ref": 3.615,
      "periodicity": [
        {
          "min_coordinate": -22.86326748,
          "length": 45.72653497,
          "relaxation_constraint": false
        },
        {
          "min_coordinate": -4.440892099e-16,
          "length": 22.86326748,
          "relaxation_constraint": false
        },
        {
          "min_coordinate": 0.0,
          "length": 7.23,
          "relaxation_constraint": false
        }
      ],
```

Make sure that you are using the correct potential that models your
material:

```json src=examples/copper_grain_boundary_relaxation/userinput.json
    "Potentials": {
      "material": {
        "$type": "SetflEAM",
        "potential_file_path": "../../data/potentials_NIST/copper/mishin/Cu.eam.alloy",
        "potential_material_name": "CuEAM",
        "funcfl_file": false
      }
    },
```
Provide the correct paths to the mesh and lattice files generated in
the previous step:

```json src=examples/copper_grain_boundary_relaxation/userinput.json
    "SimulationSettings": {
      "fixed_box": null,
      "constrained_direction": null,
      "lattice_file": "./mesh_files/sigma5_2x2x1.txt",
      "mesh_file": "./mesh_files/sigma5_2x2x1_000823.nc",
```
Check the thermal settings for temperature and initial position
entropy guess:

```json src=examples/copper_grain_boundary_relaxation/userinput.json
    "ThermalSettings": {
      "Phi_ref": 5.545,
      "T_max": 5000.0,
      "T_min": 0.01,
      "T_ref": 300,
```

The simulation can then be run by executing:

```sh src=examples/copper_grain_boundary_relaxation/run.sh
mpirun -np 4 ../../build/main/Relaxation3D_FINITE_K
```

Upon successful completion, you will notice an `energy_volume_***.dat`
output file, which has also been provided as
`energy_volume_sample.dat` in the exercise directory. Mesh
visualization `vtu` files for every strain itertion can also been
accessed in the directory `solution_files`. The maximum number of solver iterations has been reduced to `10` for this example. To obtain realistic results, it is recommended to change the field `fire_max_iter` to `1e4` in the userinput.


## Quasistatic deformation of a monocrystalline slab

This example computes the Helmholtz free energy of a copper slab with
two free surfaces as a function of uniaxial strain at a temperature of
$300~$K. For this purpose, the `ElasticConstants_FINITE_K` executable
found inside `build/main/` is used. This executable first relaxes the
initial slab geometry at 0 strain, and then incrementally strains the
sample with staggered quasistatic relaxation steps. The files required
for this exercise can be found inside
`examples/copper_slab_deformation`. To begin with, we will create a
small $4\times 4\times 4$ unit cell box of copper using the lattice
generation json script `latticegenerator_parameters.json`. Once inside
the exercise directory, this can be done simply by:

```sh src=examples/copper_slab_deformation/run.sh
mpirun -np 1 ../../build/main/mesh_generator
```

This operation will generate an initial `.nc` mesh and a `.txt`
lattice file and save it in the `mesh_files` directory. The mesh file
name can also be found in `./mesh_files/mesh_names.txt`. Another
important information is the initial periodic lengths, which is saved
in `./mesh_files/periodic_info.txt`. 
Now, we can proceed with the actual simulation. For this, we will need
the `userinput.json` script (also provided).
The important parameters to make sure the simulation works are
summarized below:

These periodicity parameters make a geometry that is periodic in x and
y directions and has free surfaces along z.
Make sure to provide the correct initial periodic lengths, which can
be seen in `./mesh_files/periodic_info.txt`.

```json src=examples/copper_slab_deformation/userinput.json
    "BoundaryConditions": {
      "a_ref": 3.61,
      "periodicity": [
        {
          "min_coordinate": -7.23,
          "length": 14.46,
          "relaxation_constraint": false
        },
        {
          "min_coordinate": -7.23,
          "length": 14.46,
          "relaxation_constraint": false
        },
        null
      ],
```

Make sure that you are using the correct potential that models your
material:

```json src=examples/copper_slab_deformation/userinput.json
    "Potentials": {
      "material": {
        "$type": "SetflEAM",
        "potential_file_path": "../../data/potentials_NIST/copper/mishin/Cu.eam.alloy",
        "potential_material_name": "CuEAM",
        "funcfl_file": false
      }
    },
```

Provide the correct paths to the mesh and lattice files generated in
the previous step:

```json src=examples/copper_slab_deformation/userinput.json
    "SimulationSettings": {
      "fixed_box": null,
      "constrained_direction": null,
      "lattice_file": "./mesh_files/Cu_4x4x4.txt",
      "mesh_file": "./mesh_files/Cu_4x4x4_000365.nc",
```

Check the thermal settings for temperature and initial position
entropy guess:

```json src=examples/copper_slab_deformation/userinput.json
    "ThermalSettings": {
      "Phi_ref": 5.545,
      "T_max": 5000.0,
      "T_min": 0.01,
      "T_ref": 300,
```

Provide the correct form of displacement gradient for uniaxial
tension, number of strain iterations and the strain increment at every
step:

```json src=examples/copper_slab_deformation/userinput.json
    "ExternalIndenter": {
      "external_displacement_gradient": [
        [
          1.0,
          0.0,
          0.0
        ],
        [
          0.0,
          0.0,
          0.0
        ],
        [
          0.0,
          0.0,
          0.0
        ]
      ],
      "external_iterations": 2,
      "force_constant": 1000.0,
      "indenter_center": [
        0.0,
        0.0,
        0.0
      ],
      "indenter_increment": 0.001,
```

The simulation can then be run by executing:

```sh examples/copper_slab_deformation/run.sh
mpirun -np 4 ../build/main/ElasticConstants_FINITE_K
```

The simulation should take approximately 30 minutes when using 4
processors. Upon successful completion, you would notice an
`energy_volume_***.dat` output file, which has also been provided as
`energy_volume_sample.dat` in the exercise directory. Mesh
visualization `vtu` files for every strain iteration can also be
accessed in the directory `solution_files`. The maximum number of solver iterations has been reduced to `5` for this example. To obtain realistic results, it is recommended to change the field `fire_max_iter` to `1e4` in the userinput.
