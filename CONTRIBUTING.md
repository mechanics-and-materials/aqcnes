# Contributing Guidelines
We welcome any kind of improvement to our code base (following
these guidelines) as well as bug reports!

## ETH Zürich repo vs. gitlab.com repo
As our CI pipeline relies on the infrastructure of [ETH Zürich](https://ethz.ch/), the main repository is hosted on [gitlab.ethz.ch](https://gitlab.ethz.ch) which is read-only for externals to ETHZ. To also allow external developers to create issues / merge requests, we provide a [mirror repo](https://gitlab.com/qc-devs/aqcnes) on [gitlab.com](https://gitlab.com).

Contributors from within ETH Zürich are requested to use the [ETHZ repo](https://gitlab.ethz.ch/mechanics-and-materials/aqcnes), while others can use the [gitlab.com repo](https://gitlab.com/qc-devs/aqcnes).

## Code quality

In order to ensure that a contribution gets merged to our release branch (`main`), it is required that a CI pipeline successfully runs over your feature branch and passes. As we do not run CI on the [gitlab.com repo](https://gitlab.com/qc-devs/aqcnes), developers external to ETHZ will not get an automated feadback regarding the CI pipeline status and would have to run such a pipeline manually on their infrastructure. Applying the following code quality checks prior to the submission of a merge request would be appreciated:
* [clang-format](https://clang.llvm.org/docs/ClangFormat.html) (version 14.0.0-1ubuntu1.1) to
  format the code, 
* Check that each source file has a license header (use [check-license.sh](check-license.sh)),
* All tests pass: `cmake --build build --target test` (some tests run long),
* [clang-tidy](https://clang.llvm.org/extra/clang-tidy/) (Ubuntu LLVM version 14.0.0) for static code
  analysis (can take more than 20 minutes).

Using the docker development container (see the [corresponding section in README.md](./#using-the-build-container)) `registry.ethz.ch/mechanics-and-materials/aqcnes/dev` is highly useful for running these checks on a feature branch. It provides the exact same environment as in our CI and also comes with all the necessary tools preinstalled. In case of any confusion, contributors are encouraged to reach out to the following developers:
* [Shashank Saxena](https://mm.ethz.ch/people/person-detail.saxena.html) (ssaxena@ethz.ch)
* [Dr. Gerhard Bräunlich](https://mm.ethz.ch/people/person-detail.braeunlich.html) (gerhard.braeunlich@id.ethz.ch) 
* [Prof. Prateek Gupta](https://web.iitd.ac.in/~prgupta/) (Prateek.Gupta@am.iitd.ac.in)

## Tests

Contributors are requested to include a test for any new feature they implement. This will also serve as a documentation for using the new functionality. If that is not possible, please provide us with a sample input and output generated from the newly implemented feature. This will help us to implement a relevant test.
