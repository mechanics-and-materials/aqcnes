# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

find_package(PkgConfig)

pkg_check_modules(PnetCDF
                  pnetcdf
                  IMPORTED_TARGET
)

if (PnetCDF_FOUND)
  find_package_handle_standard_args(PnetCDF
    REQUIRED_VARS
    PnetCDF_FOUND
    VERSION_VAR
    PnetCDF_VERSION
  )
  if (NOT TARGET aqcnes::external::pnetcdf)
    add_library(aqcnes::external::pnetcdf
      INTERFACE
      IMPORTED
    )
    target_link_libraries(aqcnes::external::pnetcdf
      INTERFACE
      PkgConfig::PnetCDF
    )
  endif()
else()
  find_library(PNETCDF_LIBRARY
    NAMES pnetcdf
  )

  set(PNETCDF_DIR "${PNETCDF_LIBRARY}/..")
  find_path(PNETCDF_INCLUDE_DIR
    PATHS "${PNETCDF_DIR}"
    NO_DEFAULT_PATHS
    NAMES pnetcdf.h
  )

  find_package_handle_standard_args(PnetCDF
    REQUIRED_VARS
      PNETCDF_LIBRARY
      PNETCDF_INCLUDE_DIR
  )
  if (NOT TARGET aqcnes::external::pnetcdf)
    add_library(aqcnes_external_pnetcdf
      INTERFACE
    )
    target_include_directories(aqcnes_external_pnetcdf
      INTERFACE
      "${PNETCDF_INCLUDE_DIR}"
    )
    target_link_libraries(aqcnes_external_pnetcdf
      INTERFACE
      "${PNETCDF_LIBRARY}"
    )
    add_library(aqcnes::external::pnetcdf
      ALIAS
      aqcnes_external_pnetcdf
    )
  endif()
endif()
