# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.
find_package(MPI 3.1)

function(aqcnes_add_mpi_test)
  cmake_parse_arguments(AQCNES_MPI_TEST_ARGS "" "TARGET" "N_PROC" ${ARGN})
  if(NOT AQCNES_MPI_TEST_ARGS_TARGET)
    message(FATAL_ERROR "aqcnes_add_mpi_test: Invalid arguments")
  endif()
  add_test(NAME ${AQCNES_MPI_TEST_ARGS_TARGET} COMMAND $<TARGET_FILE:${AQCNES_MPI_TEST_ARGS_TARGET}>)

  if (MPI_CXX_FOUND)
    foreach(n ${AQCNES_MPI_TEST_ARGS_N_PROC})
      add_test(NAME ${AQCNES_MPI_TEST_ARGS_TARGET}_mpi_${n}
        COMMAND ${MPIEXEC_EXECUTABLE}
        ${MPIEXEC_NUMPROC_FLAG} ${n}
        ${MPIEXEC_PREFLAGS} $<TARGET_FILE:${AQCNES_MPI_TEST_ARGS_TARGET}> ${MPIEXEC_POSTFLAGS}
      )
    endforeach()
  endif()
endfunction()
