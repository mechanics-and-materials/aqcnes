# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

if(NOT TARGET qcmesh::mesh)
  find_path(QCMESH_SRC_DIRECTORY
    "qcmesh/CMakeLists.txt"
    NO_DEFAULT_PATH
    HINTS "/usr/src/qcmesh" $ENV{QCMESH_SRC} ${QCMESH_SRC}
  )

  if(EXISTS "${QCMESH_SRC_DIRECTORY}")
    add_subdirectory("${QCMESH_SRC_DIRECTORY}/qcmesh" "${CMAKE_CURRENT_BINARY_DIR}/3rdParty/qcmesh" EXCLUDE_FROM_ALL)
  else()
    find_package(qcmesh 1.1.0 CONFIG REQUIRED)
  endif()
endif()
