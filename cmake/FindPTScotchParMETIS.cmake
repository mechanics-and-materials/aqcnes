# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

find_library(PTSCOTCHPARMETIS_LIBRARY_ptscotchparmetis
  NAMES ptscotchparmetis ptscotchparmetisv3
)

set(PTSCOTCHPARMETIS_DIR "${PTSCOTCHPARMETIS_LIBRARY}/..")

foreach(LIBRARY_NAME ptscotch ptscotcherr scotch)
  find_library(PTSCOTCHPARMETIS_LIBRARY_${LIBRARY_NAME}
    PATHS "${PTSCOTCHPARMETIS_DIR}"
    NO_DEFAULT_PATHS
    NAMES ${LIBRARY_NAME}
  )
endforeach()

find_path(PTSCOTCHPARMETIS_INCLUDE_DIR
  PATHS "${PTSCOTCHPARMETIS_DIR}/.." /usr/include/parmetis/
  NO_DEFAULT_PATHS
  NAMES parmetis.h
)

find_package(ZLIB)

find_package_handle_standard_args(PTScotchParMETIS
  REQUIRED_VARS
    PTSCOTCHPARMETIS_LIBRARY_ptscotchparmetis
    PTSCOTCHPARMETIS_LIBRARY_ptscotch
    PTSCOTCHPARMETIS_LIBRARY_ptscotcherr
    PTSCOTCHPARMETIS_LIBRARY_scotch
    PTSCOTCHPARMETIS_INCLUDE_DIR
    ZLIB_FOUND
)

if (PTScotchParMETIS_FOUND)
  if (NOT TARGET aqcnes::external::ptscotch-parmetis)
    add_library(aqcnes_external_ptscotch-parmetis
      INTERFACE
    )
    target_link_libraries(aqcnes_external_ptscotch-parmetis
      INTERFACE
      ZLIB::ZLIB
    )
    foreach(LIBRARY_NAME ptscotchparmetis ptscotch ptscotcherr scotch)
      target_link_libraries(aqcnes_external_ptscotch-parmetis
        INTERFACE
        "${PTSCOTCHPARMETIS_LIBRARY_${LIBRARY_NAME}}"
      )
    endforeach()
    target_include_directories(aqcnes_external_ptscotch-parmetis
      INTERFACE
      "${PTSCOTCHPARMETIS_INCLUDE_DIR}"
    )
    add_library(aqcnes::external::ptscotch-parmetis
      ALIAS
      aqcnes_external_ptscotch-parmetis
    )
  endif()
endif()
