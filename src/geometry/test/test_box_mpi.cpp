// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "geometry/box.hpp"
#include "geometry/point.hpp"
#include <gmock/gmock.h>
#include <vector>

namespace geometry {
namespace {

template <std::size_t Dimension> using Box = qcmesh::geometry::Box<Dimension>;

TEST(test_box_mpi, distributed_bounding_box_works_for_multiple_points) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto points = std::vector<Point<3>>{
      Point<3>{0.0 + mpi_rank, 1.0 + mpi_rank, 2.0 + mpi_rank},
      Point<3>{1.0 + mpi_rank, -1.0 + mpi_rank, -2.0 + mpi_rank},
      Point<3>{5.0 + mpi_rank, 0.0 + mpi_rank, 0.0 + mpi_rank}};
  const auto box = distributed_bounding_box(points);
  EXPECT_DOUBLE_EQ(box.lower[0], 0.0);
  EXPECT_DOUBLE_EQ(box.lower[1], -1.0);
  EXPECT_DOUBLE_EQ(box.lower[2], -2.0);
  const auto offset = world.size() - 1;
  EXPECT_DOUBLE_EQ(box.upper[0], 5.0 + offset);
  EXPECT_DOUBLE_EQ(box.upper[1], 1.0 + offset);
  EXPECT_DOUBLE_EQ(box.upper[2], 2.0 + offset);
}

TEST(test_box_mpi, distributed_box_hull_works) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto box =
      Box<3>{Point<3>{0.0 + mpi_rank, -1.0 + mpi_rank, -2.0 - mpi_rank},
             Point<3>{1.0 + mpi_rank, 1.0 + mpi_rank, 2.0 - mpi_rank}};
  const auto hull = distributed_box_hull(box);
  const auto offset = world.size() - 1;
  EXPECT_DOUBLE_EQ(hull.lower[0], 0.0);
  EXPECT_DOUBLE_EQ(hull.lower[1], -1.0);
  EXPECT_DOUBLE_EQ(hull.lower[2], -2.0 - offset);
  EXPECT_DOUBLE_EQ(hull.upper[0], 1.0 + offset);
  EXPECT_DOUBLE_EQ(hull.upper[1], 1.0 + offset);
  EXPECT_DOUBLE_EQ(hull.upper[2], 2.0);
}

} // namespace
} // namespace geometry

int main(int argc, char **argv) {
  const auto env = boost::mpi::environment{};

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
