// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "geometry/point_is_near_segment.hpp"
#include <gmock/gmock.h>

namespace geometry {
namespace {

TEST(SegmentTest, point_is_near_segment_2d_returns_true_if_point_on_segment) {
  const auto start = Point<2>{1., 0.};
  const auto end = Point<2>{1., 2.};
  const auto point = Point<2>{1., 0.5};
  const auto result = point_is_near_segment(point, start, end, 0.125);
  EXPECT_DOUBLE_EQ(result, true);
}
TEST(SegmentTest, point_is_near_segment_3d_returns_true_if_point_on_segment) {
  const auto start = Point<3>{1., 0., 0.};
  const auto end = Point<3>{1., 2., 0.};
  const auto point = Point<3>{1., 0.5, 0.};
  const auto result = point_is_near_segment(point, start, end, 0.125);
  EXPECT_DOUBLE_EQ(result, true);
}

TEST(SegmentTest, point_is_near_segment_2d_returns_true_if_point_in_cylinder) {
  const auto start = Point<2>{1., 0.};
  const auto end = Point<2>{1., 2.};
  const auto point = Point<2>{1.125, 0.5};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}
TEST(SegmentTest, point_is_near_segment_3d_returns_true_if_point_in_cylinder) {
  const auto start = Point<3>{1., 0., 0.};
  const auto end = Point<3>{1., 2., 0.};
  const auto point = Point<3>{1.125, 0.5, 0.};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}

TEST(SegmentTest, point_is_near_segment_2d_returns_true_if_point_in_end_ball) {
  const auto start = Point<2>{1., 0.};
  const auto end = Point<2>{1., 2.};
  const auto point = Point<2>{1.0, 2.125};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}
TEST(SegmentTest, point_is_near_segment_3d_returns_true_if_point_in_end_ball) {
  const auto start = Point<3>{1., 0., 0.};
  const auto end = Point<3>{1., 2., 0.};
  const auto point = Point<3>{1.0, 2.125, 0.};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}

TEST(SegmentTest,
     point_is_near_segment_2d_returns_true_if_point_in_start_ball) {
  const auto start = Point<2>{1., 0.};
  const auto end = Point<2>{1., 2.};
  const auto point = Point<2>{1.0, -0.125};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}
TEST(SegmentTest,
     point_is_near_segment_3d_returns_true_if_point_in_start_ball) {
  const auto start = Point<3>{1., 0., 0.};
  const auto end = Point<3>{1., 2., 0.};
  const auto point = Point<3>{1.0, -0.125, 0.};
  const auto result = point_is_near_segment(point, start, end, 0.25);
  EXPECT_DOUBLE_EQ(result, true);
}

TEST(SegmentTest,
     point_is_near_segment_2d_returns_false_if_point_too_far_away) {
  const auto start = Point<2>{1., 0.};
  const auto end = Point<2>{1., 2.};
  const auto points = std::array{Point<2>{1.0, -0.25}, Point<2>{1.0, 2.25},
                                 Point<2>{1.25, 0.5}};
  for (const auto &p : points) {
    const auto result = point_is_near_segment(p, start, end, 0.125);
    EXPECT_DOUBLE_EQ(result, false);
  }
}
TEST(SegmentTest, point_is_near_segment_3d_returns_true_if_point_too_far_away) {
  const auto start = Point<3>{1., 0., 0.};
  const auto end = Point<3>{1., 2., 0.};
  const auto points =
      std::array{Point<3>{1.0, -0.25, 0.}, Point<3>{1.0, 2.25, 0.},
                 Point<3>{1.25, 0.5, 0.}, Point<3>{1.0, 0.5, 0.25}};
  for (const auto &p : points) {
    const auto result = point_is_near_segment(p, start, end, 0.125);
    EXPECT_DOUBLE_EQ(result, false);
  }
}

} // namespace
} // namespace geometry
