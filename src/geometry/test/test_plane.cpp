// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "geometry/plane.hpp"
#include <cmath>
#include <gmock/gmock.h>

namespace geometry {
namespace {

struct PlaneTestSuite : testing::Test {
  Plane plane = Plane{Point<3>{1., 1., 1.}, Point<3>{1., 1., 1.}};
};

TEST_F(PlaneTestSuite, signed_distance_is_zero_for_reference_point) {
  EXPECT_DOUBLE_EQ(signed_distance(plane, plane.reference_point), 0.);
}

TEST_F(PlaneTestSuite, signed_distance_is_non_zero_for_point_off_plane) {
  EXPECT_DOUBLE_EQ(signed_distance(plane, Point<3>{1., 2., 3.}), sqrt(3.));
}

TEST(PlaneTest, signed_distance_is_nan_for_degenerate_plane) {
  const auto degenerate_plane =
      Plane{Point<3>{1., 1., 1.}, Point<3>{0., 0., 0.}};
  EXPECT_THAT(signed_distance(degenerate_plane, Point<3>{0., 0., 0.}),
              ::testing::NanSensitiveDoubleEq(NAN));
}

TEST(PlaneTest, create_plane_from_triangle_works) {
  const auto a = Point<3>{1., 0., 1.};
  const auto b = Point<3>{0., 1., 1.};
  const auto c = Point<3>{1., 1., 0.};
  const auto plane = create_plane_from_triangle(a, b, c);
  EXPECT_DOUBLE_EQ(signed_distance(plane, a), 0.);
  EXPECT_DOUBLE_EQ(signed_distance(plane, b), 0.);
  EXPECT_DOUBLE_EQ(signed_distance(plane, c), 0.);
  EXPECT_DOUBLE_EQ(signed_distance(plane, Point<3>{0., 0., 0.}), 2. / sqrt(3.));
}

TEST(PlaneTest, signed_distance_variants_agree) {
  const auto triangle = std::array{Point<3>{1., 0., 1.}, Point<3>{0., 1., 1.},
                                   Point<3>{1., 1., 0.}};
  const auto plane =
      create_plane_from_triangle(triangle[0], triangle[1], triangle[2]);
  const auto point = Point<3>{0., 0., 0.};
  EXPECT_DOUBLE_EQ(signed_distance(plane, triangle[0]),
                   signed_distance(triangle, triangle[0]));
  EXPECT_DOUBLE_EQ(signed_distance(plane, triangle[1]),
                   signed_distance(triangle, triangle[1]));
  EXPECT_DOUBLE_EQ(signed_distance(plane, triangle[2]),
                   signed_distance(triangle, triangle[2]));
  EXPECT_DOUBLE_EQ(signed_distance(plane, point),
                   signed_distance(triangle, point));
}

} // namespace
} // namespace geometry
