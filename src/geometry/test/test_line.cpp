// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "geometry/line.hpp"
#include <gmock/gmock.h>

namespace geometry {
namespace {

struct LineTestSuite : testing::Test {
  Point<2> a = Point<2>{}, b = Point<2>{1., 0.};
  Line line = Line(a, b);
};

TEST_F(LineTestSuite, signed_distance_is_zero_for_points_on_line) {
  EXPECT_DOUBLE_EQ(signed_distance(line, a), 0.);
  EXPECT_DOUBLE_EQ(signed_distance(line, b), 0.);
}

TEST_F(LineTestSuite, signed_distance_is_nonzero_for_points_off_line) {
  EXPECT_DOUBLE_EQ(signed_distance(line, Point<2>{0., 1.}), -1.);
  EXPECT_DOUBLE_EQ(signed_distance(line, Point<2>{0., -2.}), 2.);
  EXPECT_DOUBLE_EQ(signed_distance(line, Point<2>{-10., 1.}), -1.);
  EXPECT_DOUBLE_EQ(signed_distance(line, Point<2>{10., -2.}), 2.);
}

TEST(LineTest, signed_distance_is_nan_for_degenerate_line) {
  const auto degenerate_line = Line{Point<2>{0., 0.}, Point<2>{0., 0.}};
  EXPECT_THAT(signed_distance(degenerate_line, Point<2>{0., 0.}),
              ::testing::NanSensitiveDoubleEq(NAN));
}

TEST_F(LineTestSuite, tangent_and_normal_are_orthogonal) {
  const auto n = normal(line);
  const auto t = line.tangent;
  EXPECT_DOUBLE_EQ(n[0] * t[0] + n[1] * t[1], 0.);
}

} // namespace
} // namespace geometry
