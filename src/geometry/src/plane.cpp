// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to represent a plane in 3 dimensions.
 * @authors P. Gupta
 */

#include "geometry/plane.hpp"

namespace geometry {

namespace array_ops = qcmesh::array_ops;

namespace {
Eigen::Matrix<double, 3, 1> make_normal(const Point<3> &a, const Point<3> &b,
                                        const Point<3> &c) {
  return (array_ops::as_eigen(c) - array_ops::as_eigen(b))
      .cross(array_ops::as_eigen(a) - array_ops::as_eigen(b));
}
} // namespace

Plane create_plane_from_triangle(const Point<3> &a, const Point<3> &b,
                                 const Point<3> &c) {
  auto plane = Plane{};
  plane.reference_point = b;
  array_ops::as_eigen(plane.normal) = make_normal(a, b, c);
  return plane;
}

double signed_distance(const Plane &plane, const Point<3> &p) {
  const auto normal = array_ops::as_eigen(plane.normal);
  return normal.dot(array_ops::as_eigen(p) -
                    array_ops::as_eigen(plane.reference_point)) /
         normal.norm();
}

double signed_distance(const std::array<Point<3>, 3> &plane,
                       const Point<3> &p) {
  const auto normal = make_normal(plane[0], plane[1], plane[2]);
  return normal.dot(array_ops::as_eigen(p) - array_ops::as_eigen(plane[1])) /
         normal.norm();
}

} // namespace geometry
