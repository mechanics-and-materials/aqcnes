// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to represent a line through two points.
 * @authors P. Gupta
 */

#include "geometry/line.hpp"
#include "qcmesh/array_ops/array_ops.hpp"

namespace array_ops = qcmesh::array_ops;

namespace geometry {

Line::Line(const Point<2> &from, const Point<2> &to) : reference_point{from} {
  array_ops::as_eigen(this->tangent) =
      array_ops::as_eigen(to) - array_ops::as_eigen(from);
}

double signed_distance(const Line &l, const Point<2> &p) {
  const auto n = normal(l);
  return (array_ops::as_eigen(p) - array_ops::as_eigen(l.reference_point))
             .dot(array_ops::as_eigen(n)) /
         array_ops::as_eigen(n).norm();
}

} // namespace geometry
