// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to check if a point lies near a segment.
 * @authors P. Gupta
 */

#include "geometry/point_is_near_segment.hpp"

namespace array_ops = qcmesh::array_ops;

namespace geometry {
namespace {
template <std::size_t Dimension>
bool point_is_near_segment_generic(
    const geometry::Point<Dimension> &point,
    const geometry::Point<Dimension> &segment_start,
    const geometry::Point<Dimension> &segment_end, const double tolerance) {
  const auto p_start =
      array_ops::as_eigen(point) - array_ops::as_eigen(segment_start);
  const auto start_end =
      array_ops::as_eigen(segment_end) - array_ops::as_eigen(segment_start);
  const auto ll_segment = start_end.squaredNorm();
  const auto axial_rel_pos = p_start.dot(start_end) / ll_segment;
  const auto r_vec = p_start - start_end * axial_rel_pos;
  const auto radial_dist_squared = r_vec.squaredNorm();
  const auto tt = tolerance * tolerance;
  return (0 <= axial_rel_pos && axial_rel_pos <= 1.0 &&
          radial_dist_squared < tt) ||
         p_start.squaredNorm() < tt ||
         (array_ops::as_eigen(point) - array_ops::as_eigen(segment_end))
                 .squaredNorm() < tt;
}
} // namespace
bool point_is_near_segment(const geometry::Point<3> &point,
                           const geometry::Point<3> &segment_start,
                           const geometry::Point<3> &segment_end,
                           const double tolerance) {
  return point_is_near_segment_generic(point, segment_start, segment_end,
                                       tolerance);
}
bool point_is_near_segment(const geometry::Point<2> &point,
                           const geometry::Point<2> &segment_start,
                           const geometry::Point<2> &segment_end,
                           const double tolerance) {
  return point_is_near_segment_generic(point, segment_start, segment_end,
                                       tolerance);
}
} // namespace geometry
