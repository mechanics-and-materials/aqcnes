// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Extension to @ref qcmesh::geometry to find smalles box containing other boxes (box hull) and mpi serialization.
 */

#pragma once

#include "qcmesh/geometry/box.hpp"
#include <boost/mpi.hpp>
#include <tuple>

namespace geometry {

template <std::size_t Dimension> struct BoxUnion {
  qcmesh::geometry::Box<Dimension> &
  operator()(qcmesh::geometry::Box<Dimension> &a,
             const qcmesh::geometry::Box<Dimension> &b) {
    for (std::size_t i = 0; i < Dimension; i++)
      a.lower[i] = std::min(a.lower[i], b.lower[i]);
    for (std::size_t i = 0; i < Dimension; i++)
      a.upper[i] = std::max(a.upper[i], b.upper[i]);
    return a;
  }
};

} // namespace geometry

/**
 * @brief Mark @ref geometry::BoxUnion as a commutative opperation.
 */
template <std::size_t Dimension>
struct boost::mpi::is_commutative<geometry::BoxUnion<Dimension>,
                                  std::array<double, Dimension>>
    : public boost::mpl::true_ {};

namespace boost {
namespace serialization {

/**
 * @brief Implement serialization for @ref qcmesh::geometry::Box.
 */
template <typename Archive, std::size_t Dimension>
void serialize(Archive &ar, qcmesh::geometry::Box<Dimension> &data,
               const unsigned int) {
  ar &data.lower;
  ar &data.upper;
}

/**
 * See
 * https://www.boost.org/doc/libs/release/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <std::size_t Dimension>
struct tracking_level<qcmesh::geometry::Box<Dimension>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<qcmesh::geometry::Box<Dimension>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t Dimension>
struct implementation_level<qcmesh::geometry::Box<Dimension>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};
} // namespace serialization
template <std::size_t Dimension>
struct mpi::is_mpi_datatype<qcmesh::geometry::Box<Dimension>>
    : public mpl::true_ {};

} // namespace boost
namespace geometry {

/**
 * @brief Constructs a box containing the boxes from all MPI processes.
 */
template <std::size_t Dimension>
qcmesh::geometry::Box<Dimension>
distributed_box_hull(const qcmesh::geometry::Box<Dimension> &box) {

  auto hull = decltype(box){};
  boost::mpi::all_reduce(boost::mpi::communicator{}, box, hull,
                         BoxUnion<Dimension>{});
  return hull;
}

/**
 * @brief Constructs the bounding box containing a given range of points over all MPI processes.
 *
 * If the range of points is empty, return an empty bounding box (containing no point at all).
 */
template <std::size_t Dimension>
qcmesh::geometry::Box<Dimension> distributed_bounding_box(
    const std::vector<std::array<double, Dimension>> &points) {
  const auto local_domain = qcmesh::geometry::bounding_box(points);
  return distributed_box_hull(local_domain);
}

} // namespace geometry
