// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to represent a plane in 3 dimensions.
 * @authors P. Gupta
 */

#pragma once

#include "geometry/point.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <Eigen/Dense>

namespace geometry {

/** A plane in 3 dimensions. */
struct Plane {
  /**
   * @brief Some point on the plain.
   */
  Point<3> reference_point{};
  /**
   * @brief Normal vector to the plain.
   */
  Point<3> normal{};
};

/**
 * @brief Create a Plane through 3 given points.
 */
Plane create_plane_from_triangle(const Point<3> &a, const Point<3> &b,
                                 const Point<3> &c);

/**
 * @brief Return the distance from p to the plane plane.
 *
 * @remark Returns nan if the plane is degenerate.
 */
double signed_distance(const Plane &plane, const Point<3> &p);

/**
 * @brief Return the distance from p to a plane through 3 points.
 *
 * @remark Returns nan if the plane is degenerate.
 */
double signed_distance(const std::array<Point<3>, 3> &plane, const Point<3> &p);

} // namespace geometry
