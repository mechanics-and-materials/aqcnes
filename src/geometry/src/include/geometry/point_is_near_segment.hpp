// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to check if a point lies near a segment.
 * @authors P. Gupta
 */

#pragma once

#include "geometry/point.hpp"
#include "qcmesh/array_ops/array_ops.hpp"

namespace array_ops = qcmesh::array_ops;

namespace geometry {

/**
 * @brief Returns true if a point is within a tolerance-neighbourhood of
 * a segment `segment_start` - `segment_end` (union of a cylinder with 2 balls of radius tolerance at its ends).
 */
bool point_is_near_segment(const geometry::Point<3> &point,
                           const geometry::Point<3> &segment_start,
                           const geometry::Point<3> &segment_end,
                           const double tolerance);
bool point_is_near_segment(const geometry::Point<2> &point,
                           const geometry::Point<2> &segment_start,
                           const geometry::Point<2> &segment_end,
                           const double tolerance);

} // namespace geometry
