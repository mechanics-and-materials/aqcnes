// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation to represent a line through two points.
 * @authors P. Gupta
 */

#pragma once

#include "geometry/point.hpp"

namespace geometry {

/**
 * @brief A line in 2-D.
 */
struct Line {
  Point<2> reference_point{};
  Point<2> tangent{};

  /**
   * @brief Construct a line through two points.
   *
   * @remark The orientation of a line pointing in the x-direction
   * (e.g. source: (0,0), target: (1,0)) will have its normal pointing
   * to the negative y-direction (0,-1)!
   */
  explicit Line(const Point<2> &from, const Point<2> &to);
};

/**
 * @brief Return the normal to the line.
 */
inline Point<2> normal(const Line &line) {
  return Point<2>{line.tangent[1], -line.tangent[0]};
}

/**
 * @brief Distance to the line.
 *
 * @remark Returns nan for a degenerate line.
 */
double signed_distance(const Line &l, const Point<2> &p);

} // namespace geometry
