// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check if @ref netcdf_io::OrderedMapValues are in the correct sequence
 */

#include "netcdf_io/ordered_map_values.hpp"
#include <gmock/gmock.h>

namespace netcdf_io {
namespace {

TEST(test_ordered_map_values, ordered_map_values_works) {
  const auto map =
      std::unordered_map<std::size_t, double>{{1, 2.0}, {0, 1.0}, {10, 15.0}};
  auto values = std::vector<double>{};
  for (const auto value : OrderedMapValues(map))
    values.emplace_back(value);
  const auto expected_values = std::vector<double>{1.0, 2.0, 15.0};

  EXPECT_THAT(values, testing::ContainerEq(expected_values));
}

} // namespace
} // namespace netcdf_io
