// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load mesh and cell data from a restart file written by @ref write_restart_file.
 * @authors P. Gupta, S. Saxena
 */

#include "netcdf_io/load_nc_file.hpp"
#include "qcmesh/mpi/mpi_sub_range.hpp"

namespace netcdf_io::detail {

ChunkSpan mpi_chunk_span(const std::size_t total_size) {
  const auto [start_idx, range_size] = qcmesh::mpi::mpi_sub_range(total_size);
  return netcdf_io::ChunkSpan{start_idx, range_size};
}

} // namespace netcdf_io::detail
