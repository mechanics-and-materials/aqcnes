// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief C++ wrapper of the pnetcdf library.
 * @author G. Bräunlich
 */

#include "netcdf_io/pnetcdfpp.hpp"
#include "pnetcdf.h"
#include <boost/mpi.hpp>
#include <initializer_list>
#include <string>

namespace netcdf_io {

namespace {

template <class F> void assert_no_error(const int err, const F context) {
  if (err != NC_NOERR)
    throw std::runtime_error{"Error: " + context() + ": " +
                             ncmpi_strerror(err)};
}
namespace detail {
int ncmpi_get_vara_all(int ncid, int varid, const MPI_Offset *start,
                       const MPI_Offset *count, unsigned int *buf) {
  return ncmpi_get_vara_uint_all(ncid, varid, start, count, buf);
}
int ncmpi_get_vara_all(int ncid, int varid, const MPI_Offset *start,
                       const MPI_Offset *count, double *buf) {
  return ncmpi_get_vara_double_all(ncid, varid, start, count, buf);
}

template <class T>
std::vector<T>
read_2d_array_block(const int ncfile, const std::string_view &name,
                    const std::size_t size, const ChunkSpan &chunk_span) {
  auto out = std::vector<T>(chunk_span.size * size);
  auto varid = int{};
  assert_no_error(ncmpi_inq_varid(ncfile, name.data(), &varid), [&]() {
    return "Determine variable id for '" + std::string{name} + '\'';
  });
  const auto start =
      std::array{static_cast<MPI_Offset>(chunk_span.start), MPI_Offset{0}};
  const auto count = std::array{static_cast<MPI_Offset>(chunk_span.size),
                                static_cast<MPI_Offset>(size)};
  assert_no_error(
      detail::ncmpi_get_vara_all(ncfile, varid, start.data(), count.data(),
                                 out.data()),
      [&]() { return "Read 2d array block for '" + std::string{name} + '\''; });
  return out;
}

} // namespace detail

std::size_t query_size(const int ncfile, const std::string_view &name) {
  MPI_Offset size = 0;
  assert_no_error(
      ncmpi_inq_attlen(ncfile, NC_GLOBAL, name.data(), &size),
      [&]() { return "Query variable size for '" + std::string{name} + '\''; });
  return size;
}

void assert_size(const std::string_view &name, const std::size_t size,
                 const std::size_t expected_size) {
  if (size != expected_size)
    throw std::runtime_error{"Invalid dimension for '" + std::string{name} +
                             "': " + std::to_string(size) + " (expected: " +
                             std::to_string(expected_size) + ')'};
}

} // namespace

NetCdfFileHandle::NetCdfFileHandle(const int ncid)
    : ncfile{ncid}, handle_manager(&this->ncfile, &close) {}

void NetCdfFileHandle::close(void *ncfile) {
  assert_no_error(ncmpi_close(*static_cast<int *>(ncfile)), [&]() {
    return "Close file with id " + std::to_string(*static_cast<int *>(ncfile));
  });
}

NetCdfFileHandleRO::NetCdfFileHandleRO(const std::filesystem::path &path)
    : NetCdfFileHandle{open(path)} {}

int NetCdfFileHandleRO::open(const std::filesystem::path &path) {
  auto ncfile = int{};
  assert_no_error(ncmpi_open(MPI_COMM_WORLD, path.c_str(), NC_NOWRITE,
                             MPI_INFO_NULL, &ncfile),
                  [&]() { return "Open " + std::string{path}; });
  return ncfile;
}

int NetCdfFileHandleRO::read_int(const std::string_view &name) const {
  assert_size(name, query_size(this->ncfile, name), 1);
  auto out = int{};
  assert_no_error(
      ncmpi_get_att_int(this->ncfile, NC_GLOBAL, name.data(), &out),
      [&]() { return "Read integer '" + std::string{name} + '\''; });
  return out;
}
bool NetCdfFileHandleRO::read_bool(const std::string_view &name) const {
  return static_cast<bool>(this->read_int(name));
}
double NetCdfFileHandleRO::read_double(const std::string_view &name) const {
  assert_size(name, query_size(this->ncfile, name), 1);
  auto out = double{};
  assert_no_error(
      ncmpi_get_att_double(this->ncfile, NC_GLOBAL, name.data(), &out),
      [&]() { return "Read double '" + std::string{name} + '\''; });
  return out;
}

std::vector<double>
NetCdfFileHandleRO::read_double_vec(const std::string_view &name) const {
  const auto size = query_size(this->ncfile, name);
  auto out = std::vector<double>(size);
  assert_no_error(
      ncmpi_get_att_double(this->ncfile, NC_GLOBAL, name.data(), out.data()),
      [&]() { return "Read double array '" + std::string{name} + '\''; });
  return out;
}

std::string
NetCdfFileHandleRO::read_string(const std::string_view &name) const {
  auto size = MPI_Offset{};
  assert_no_error(
      ncmpi_inq_attlen(this->ncfile, NC_GLOBAL, name.data(), &size),
      [&]() { return "Read string length '" + std::string{name} + '\''; });

  auto out = std::string(size, '\0');
  assert_no_error(
      ncmpi_get_att_text(this->ncfile, NC_GLOBAL, name.data(), out.data()),
      [&]() { return "Read string '" + std::string{name} + '\''; });
  return out;
}

std::vector<unsigned int> NetCdfFileHandleRO::read_2d_uint_array_block(
    const std::string_view &name, const std::size_t size,
    const ChunkSpan &chunk_span) const {
  return detail::read_2d_array_block<unsigned int>(this->ncfile, name, size,
                                                   chunk_span);
}
std::vector<double> NetCdfFileHandleRO::read_2d_double_array_block(
    const std::string_view &name, const std::size_t size,
    const ChunkSpan &chunk_span) const {
  return detail::read_2d_array_block<double>(this->ncfile, name, size,
                                             chunk_span);
}
std::vector<double> NetCdfFileHandleRO::read_1d_double_array_slice(
    const std::string_view &name, const ChunkSpan &chunk_span) const {
  auto out = std::vector<double>(chunk_span.size);
  auto varid = int{};
  assert_no_error(ncmpi_inq_varid(ncfile, name.data(), &varid), [&]() {
    return "Determine variable id for '" + std::string{name} + '\'';
  });
  auto start = static_cast<MPI_Offset>(chunk_span.start);
  auto count = static_cast<MPI_Offset>(chunk_span.size);
  assert_no_error(
      detail::ncmpi_get_vara_all(ncfile, varid, &start, &count, out.data()),
      [&]() { return "Read 1d array slice for '" + std::string{name} + '\''; });
  return out;
}

std::size_t
NetCdfFileHandleRO::read_dimension(const std::string_view &name) const {
  auto dim_id = int{};
  assert_no_error(ncmpi_inq_dimid(this->ncfile, name.data(), &dim_id), [&]() {
    return "read dimension: '" + std::string{name} + '\'';
  });

  auto dim = MPI_Offset{};
  assert_no_error(ncmpi_inq_dimlen(this->ncfile, dim_id, &dim), [&]() {
    return "read dimension length: '" + std::string{name} +
           "' dim_id=" + std::to_string(dim_id);
  });
  return static_cast<std::size_t>(dim);
}

void NetCdfFileHandleRO::check_dimension(const DimensionDef &def) const {
  const auto dim = this->read_dimension(def.name);
  assert_size(def.name, dim, def.size);
}

NetCdfFileHandleW::NetCdfFileHandleW(const std::filesystem::path &path)
    : NetCdfFileHandle{open(path)} {}

int NetCdfFileHandleW::open(const std::filesystem::path &path) {
  auto ncfile = int{};
  assert_no_error(ncmpi_create(MPI_COMM_WORLD, path.c_str(),
                               NC_CLOBBER | NC_64BIT_DATA, MPI_INFO_NULL,
                               &ncfile),
                  [&]() { return "Open " + std::string{path}; });
  return ncfile;
}

void NetCdfFileHandleW::write_attribute(const std::string_view &name,
                                        const int value) const {
  assert_no_error(ncmpi_put_att_int(this->ncfile, NC_GLOBAL, name.data(),
                                    NC_INT, 1, &value),
                  [&]() { return "write int: '" + std::string{name} + '\''; });
}
void NetCdfFileHandleW::write_attribute(const std::string_view &name,
                                        const std::size_t value) const {
  this->write_attribute(name, static_cast<int>(value));
}
void NetCdfFileHandleW::write_attribute(const std::string_view &name,
                                        const bool value) const {
  this->write_attribute(name, static_cast<int>(value));
}
void NetCdfFileHandleW::write_attribute(const std::string_view &name,
                                        const double value) const {
  assert_no_error(
      ncmpi_put_att_double(this->ncfile, NC_GLOBAL, name.data(), NC_DOUBLE, 1,
                           &value),
      [&]() { return "write double: '" + std::string{name} + '\''; });
}
void NetCdfFileHandleW::write_attribute(
    const std::string_view &name, const std::array<double, 2> &values) const {
  assert_no_error(ncmpi_put_att_double(this->ncfile, NC_GLOBAL, name.data(),
                                       NC_DOUBLE, 2, values.data()),
                  [&]() {
                    return "write std::array<double, 2>: '" +
                           std::string{name} + '\'';
                  });
}
void NetCdfFileHandleW::write_attribute(
    const std::string_view &name, const std::array<double, 3> &values) const {
  assert_no_error(ncmpi_put_att_double(this->ncfile, NC_GLOBAL, name.data(),
                                       NC_DOUBLE, 3, values.data()),
                  [&]() {
                    return "write std::array<double, 3>: '" +
                           std::string{name} + '\'';
                  });
}
void NetCdfFileHandleW::write_attribute(
    const std::string_view &name, const std::vector<double> &values) const {
  assert_no_error(
      ncmpi_put_att_double(this->ncfile, NC_GLOBAL, name.data(), NC_DOUBLE,
                           static_cast<MPI_Offset>(values.size()),
                           values.data()),
      [&]() {
        return "write std::vector<double>: '" + std::string{name} + '\'';
      });
}

void NetCdfFileHandleW::write_attribute(const std::string_view &name,
                                        const std::string &value) const {
  assert_no_error(ncmpi_put_att_text(this->ncfile, NC_GLOBAL, name.data(),
                                     static_cast<MPI_Offset>(value.size()),
                                     value.c_str()),
                  [&]() { return "write text: '" + std::string{name} + '\''; });
}

Dimension NetCdfFileHandleW::define_dimension(const DimensionDef &def) const {
  auto dim_id = int{};
  assert_no_error(
      ncmpi_def_dim(this->ncfile, def.name.data(), static_cast<int>(def.size),
                    &dim_id),
      [&]() { return "Define dimension: '" + std::string{def.name} + '\''; });
  return Dimension{dim_id, def.size};
}
Dimension NetCdfFileHandleW::define_dimension(const std::string_view &name,
                                              const std::size_t size) const {
  return this->define_dimension(DimensionDef{name, size});
}
void NetCdfFileHandleW::switch_to_data_mode() const {
  assert_no_error(ncmpi_enddef(this->ncfile),
                  [&]() { return std::string{"Switch to data mode"}; });
}

namespace {

namespace detail {
int ncmpi_put_vara_all(int ncid, int varid, const MPI_Offset *start,
                       const MPI_Offset *count, const unsigned int *buf) {
  return ncmpi_put_vara_uint_all(ncid, varid, start, count, buf);
}
int ncmpi_put_vara_all(int ncid, int varid, const MPI_Offset *start,
                       const MPI_Offset *count, const double *buf) {
  return ncmpi_put_vara_double_all(ncid, varid, start, count, buf);
}

template <class T> nc_type ncmpi_type();
template <> nc_type ncmpi_type<double>() { return NC_DOUBLE; }
template <> nc_type ncmpi_type<unsigned int>() { return NC_UINT; }

template <class T>
void write_2d_array_block(int ncid, const std::string_view &name,
                          const std::vector<T> &data,
                          const std::array<Dimension, 2> &dimensions,
                          const ChunkSpan &chunk_span) {
  auto var_id = int{};
  const auto dim_ids = std::array<int, 2>{dimensions[0].id, dimensions[1].id};
  assert_no_error(ncmpi_redef(ncid),
                  [&]() { return std::string{"Switch to define mode"}; });
  assert_no_error(
      ncmpi_def_var(ncid, name.data(), ncmpi_type<T>(), 2, dim_ids.data(),
                    &var_id),
      [&]() { return "Define variable: '" + std::string{name} + '\''; });
  assert_no_error(ncmpi_enddef(ncid),
                  [&]() { return std::string{"Switch to data mode"}; });
  const auto start =
      std::array{static_cast<MPI_Offset>(chunk_span.start), MPI_Offset{0}};
  const auto count = std::array{static_cast<MPI_Offset>(chunk_span.size),
                                static_cast<MPI_Offset>(dimensions[1].size)};
  assert_no_error(
      ncmpi_put_vara_all(ncid, var_id, start.data(), count.data(), data.data()),
      [&]() { return "Write 2d data block: '" + std::string{name} + '\''; });
}

} // namespace detail

} // namespace

void NetCdfFileHandleW::write_2d_array_block(
    const std::string_view &name, const std::vector<unsigned int> &data,
    const std::array<Dimension, 2> &dimensions,
    const ChunkSpan &chunk_span) const {
  detail::write_2d_array_block(this->ncfile, name, data, dimensions,
                               chunk_span);
}

void NetCdfFileHandleW::write_2d_array_block(
    const std::string_view &name, const std::vector<double> &data,
    const std::array<Dimension, 2> &dimensions,
    const ChunkSpan &chunk_span) const {
  detail::write_2d_array_block(this->ncfile, name, data, dimensions,
                               chunk_span);
}
void NetCdfFileHandleW::write_1d_array_slice(
    const std::string_view &name, const std::vector<double> &data,
    const Dimension &dimension, const ChunkSpan &chunk_span) const {
  auto var_id = int{};
  assert_no_error(ncmpi_redef(this->ncfile),
                  [&]() { return std::string{"Switch to define mode"}; });
  assert_no_error(
      ncmpi_def_var(this->ncfile, name.data(), NC_DOUBLE, 1, &dimension.id,
                    &var_id),
      [&]() { return "Define variable: '" + std::string{name} + '\''; });
  assert_no_error(ncmpi_enddef(this->ncfile),
                  [&]() { return std::string{"Switch to data mode"}; });
  const auto start = std::array{static_cast<MPI_Offset>(chunk_span.start)};
  const auto count = std::array{static_cast<MPI_Offset>(chunk_span.size)};
  assert_no_error(
      detail::ncmpi_put_vara_all(this->ncfile, var_id, start.data(),
                                 count.data(), data.data()),
      [&]() { return "Write 1d data block: '" + std::string{name} + '\''; });
}

} // namespace netcdf_io
