// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load boundary conditions from a restart file written by @ref write_restart_file.
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "netcdf_io/pnetcdfpp.hpp"
#include "netcdf_io/storage_schema.hpp"
#include "netcdf_io/vec_to_array.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <boost/mpi.hpp>
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <stdexcept>
#include <unordered_set>
#include <vector>

namespace netcdf_io {

/**
 * @brief Raw data for @ref Boundarycondition.
 */
template <std::size_t D> struct BoundaryConditionDef {
  boundarycondition::BoundaryConditionType zone_type{};
  std::array<double, D> normal{};
  std::size_t coordinate{};
  double bound{};
};

/**
 * @brief Load boundary conditions from a restart file written by @ref write_restart_file.
 *
 * Ignores the rest of the data contained in the file.
 */
template <std::size_t D>
std::vector<BoundaryConditionDef<D>>
load_boundary_conditions(const std::filesystem::path &path) {
  const auto ncfile = NetCdfFileHandleRO(path);
  const auto n_zones =
      static_cast<std::size_t>(ncfile.read_int("number of zones"));

  auto boundary_conditions = std::vector<BoundaryConditionDef<D>>{};
  boundary_conditions.reserve(n_zones);
  const auto deserialize_zone_type = [](const auto &name) {
    for (const auto boundary_condition_type :
         boundarycondition::BOUNDARY_CONDITION_TYPES)
      if (name == zone_type_name(boundary_condition_type))
        return boundary_condition_type;
    throw std::runtime_error{"Invalid zone type: " + name};
  };
  for (std::size_t i_zone = 1; i_zone <= n_zones; i_zone++) {
    const auto boundary_attribute_number =
        utils::to_string_with_zeros(i_zone, 2);
    const auto zone_type = deserialize_zone_type(
        ncfile.read_string("boundaryType_" + boundary_attribute_number));
    const auto bound =
        ncfile.read_double("bounds_" + boundary_attribute_number);
    const auto coordinate = static_cast<std::size_t>(
        ncfile.read_int("boundaryCoordinate_" + boundary_attribute_number));
    const auto normal = detail::vec_to_array<D>(
        ncfile.read_double_vec("boundaryNormal_" + boundary_attribute_number));
    boundary_conditions.emplace_back(
        BoundaryConditionDef<D>{zone_type, normal, coordinate, bound});
  }
  return boundary_conditions;
}

} // namespace netcdf_io
