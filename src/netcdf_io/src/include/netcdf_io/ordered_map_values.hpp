// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Provides @ref OrderedMapValues which allows iterating over
 * the values of a `std::unordered_map`, sorted by key.
 */

#pragma once

#include <algorithm>
#include <unordered_map>
#include <vector>

namespace netcdf_io {

/**
 * @brief Extract the ordered keys of a `std::unordered_map`.
 */
template <class K, class V>
std::vector<K> ordered_keys(const std::unordered_map<K, V> &map) {
  auto order = std::vector<K>{};
  order.reserve(map.size());
  for (const auto &[key, _] : map)
    order.push_back(key);
  std::sort(order.begin(), order.end());
  return order;
}

template <class K, class V> class OrderedMapValues;

/**
 * @brief Iterator obtained by @ref OrderedMapValues::begin() and @ref OrderedMapValues::end().
 *
 * In contrast to `std::unordered_map::begin()`, it dereferences to a value instead of a key, value pair.
 */
template <class K, class V> class OrderedMapValuesIter {
  const OrderedMapValues<K, V> &ordered_map_values;
  std::size_t pos{};

public:
  explicit OrderedMapValuesIter(
      const std::size_t pos, const OrderedMapValues<K, V> &ordered_map_values)
      : ordered_map_values{ordered_map_values}, pos{pos} {}
  OrderedMapValuesIter &operator++() {
    this->pos++;
    return *this;
  }
  bool operator!=(const OrderedMapValuesIter<K, V> &other) {
    return this->pos != other.pos;
  }
  const V &operator*() {
    return this->ordered_map_values.map.at(
        this->ordered_map_values.order[this->pos]);
  }
};

/**
 * @brief An iterable which iterates over the values of a `std::unordered_map`, sorted by the key.
 *
 * Can be used multiple times (sorting is only done once).
 * Can be used in a range based for-loop.
 */
template <class K, class V> class OrderedMapValues {
  const std::unordered_map<K, V> &map;
  const std::vector<K> order{};

public:
  explicit OrderedMapValues(const std::unordered_map<K, V> &map)
      : map{map}, order{ordered_keys(map)} {}
  [[nodiscard]] std::size_t size() const { return this->order.size(); }
  [[nodiscard]] OrderedMapValuesIter<K, V> begin() const {
    return OrderedMapValuesIter<K, V>(0, *this);
  }
  [[nodiscard]] OrderedMapValuesIter<K, V> end() const {
    return OrderedMapValuesIter<K, V>(this->order.size(), *this);
  }
  friend class OrderedMapValuesIter<K, V>;
};

} // namespace netcdf_io
