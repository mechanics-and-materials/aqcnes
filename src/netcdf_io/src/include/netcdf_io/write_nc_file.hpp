// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Write a NetCdf restart file
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "boundarycondition/external_force_applicator.hpp"
#include "meshing/traits.hpp"
#include "netcdf_io/ordered_map_values.hpp"
#include "netcdf_io/pnetcdfpp.hpp"
#include "netcdf_io/storage_schema.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <filesystem>
#include <stdexcept>
#include <vector>

namespace netcdf_io {

namespace detail {
/**
 * @brief Determines the start position of a MPI local sub array within a MPI-global array consisting of sub arrays from all MPI processes and the total size of the global array.
 */
std::tuple<std::size_t, std::size_t> all_reduce_cum_sum(const std::size_t x);

/**
 * @brief Pack integer data from cells into a `std::vector<unsigned int>`.
 */
template <class CellIterable>
std::vector<unsigned int> collect_cell_int_data(const CellIterable &cells) {
  constexpr std::size_t K =
      std::tuple_size<decltype((*cells.begin()).nodes)>{} - 1;
  constexpr std::size_t N_INT = K + 2;
  auto data = std::vector<unsigned int>{};
  data.reserve(cells.size() * N_INT);
  for (const auto &cell : cells) {
    data.emplace_back(static_cast<unsigned int>(cell.id));
    for (const auto node_id : cell.nodes)
      data.emplace_back(static_cast<unsigned int>(node_id));
  }
  return data;
}

/**
 * @brief Pack `double` restart data from cells into a `std::vector<double>`.
 */
template <class CellIterable, class BundleCellDataCallback>
std::vector<double>
collect_cell_restart_data(const CellIterable &cells,
                          const BundleCellDataCallback bundle_cell_data) {
  constexpr std::size_t K =
      std::tuple_size<decltype((*cells.begin()).nodes)>{} - 1;
  using CellDataType = decltype((*cells.begin()).data);
  using BundledCellDataType =
      decltype(bundle_cell_data(qcmesh::mesh::SimplexCell<K, CellDataType>{}));
  constexpr std::size_t D =
      std::tuple_size<decltype(BundledCellDataType{}.basis)>{};
  constexpr std::size_t N_DOUBLE = dims::restart_cell_data_size(D);
  auto data = std::vector<double>{};
  data.reserve(cells.size() * N_DOUBLE);
  for (const auto &cell : cells) {
    const auto cell_data = bundle_cell_data(cell);
    for (const auto &vec : cell_data.basis)
      for (const auto x : vec)
        data.emplace_back(x);
    data.emplace_back(cell_data.density);
    data.emplace_back(cell_data.jacobian);
    data.emplace_back(cell_data.weight);
    data.emplace_back(cell_data.is_atomistic);
  }
  return data;
}

/**
 * @brief Pack `double` mesh data from cells into a `std::vector<double>`.
 */
template <class CellIterable>
std::vector<double> collect_cell_mesh_data(const CellIterable &cells) {
  constexpr std::size_t K =
      std::tuple_size<decltype((*cells.begin()).nodes)>{} - 1;
  using CellDataType = decltype((*cells.begin()).data);
  using LatticeBasisTrait = qcmesh::mesh::traits::LatticeBasis<
      qcmesh::mesh::SimplexCell<K, CellDataType>>;
  static_assert(qcmesh::mesh::traits::IS_IMPLEMENTED<LatticeBasisTrait>);
  constexpr std::size_t D = LatticeBasisTrait::DIMENSION;
  constexpr std::size_t N_DOUBLE = dims::restart_cell_data_size(D);
  auto data = std::vector<double>{};
  data.reserve(cells.size() * N_DOUBLE);
  for (const auto &cell : cells) {
    const auto basis = LatticeBasisTrait::lattice_basis(cell);
    for (const auto &vec : basis)
      for (const auto x : vec)
        data.emplace_back(x);
  }
  return data;
}

/**
 * @brief Unzip nodal data into multiple vectors belonging to @ref RestartUnzippedData.
 */
template <class NodeIterable, class BundleNodalDataCallback>
std::tuple<std::vector<unsigned int>, std::vector<double>, std::vector<double>,
           std::vector<double>>
unzip_nodal_restart_data(const NodeIterable &nodes,
                         const BundleNodalDataCallback bundle_nodal_data) {
  constexpr std::size_t D =
      std::tuple_size<decltype((*nodes.begin()).position)>{};
  using NodalDataType = decltype((*nodes.begin()).data);
  using BundledNodalDataType =
      decltype(bundle_nodal_data(qcmesh::mesh::Node<D, NodalDataType>{}));

  auto out_int = std::vector<unsigned int>{};
  auto out_double = std::vector<double>{};
  auto out_thermal_dofs = std::vector<double>{};
  auto out_nodal_data = std::vector<double>{};
  for (const auto &node : nodes) {
    // we simply add all local nodes, which can potential lead to duplication
    // of nodes by different processes.
    const auto nodal_data = bundle_nodal_data(node);
    out_int.emplace_back(static_cast<unsigned int>(node.id));
    out_int.emplace_back(static_cast<unsigned int>(nodal_data.material_id));
    out_int.emplace_back(static_cast<unsigned int>(nodal_data.type));
    out_int.emplace_back(nodal_data.flags);
    out_int.emplace_back(
        static_cast<unsigned int>(node.data.periodic_boundary_flag));
    for (const auto x : node.position)
      out_double.emplace_back(x);
    out_double.emplace_back(nodal_data.weight);

    if constexpr (traits::NodeDimensions<BundledNodalDataType>::N_TH > 0) {
      for (const auto x : nodal_data.thermal_point)
        out_thermal_dofs.push_back(x);
      out_thermal_dofs.push_back(nodal_data.entropy);
    }
    for (const auto c : nodal_data.impurity_concentrations)
      out_nodal_data.push_back(c);
  }
  return std::make_tuple(out_int, out_double, out_thermal_dofs, out_nodal_data);
}

/**
 * @brief Unzip nodal data into multiple vectors belonging to @ref MeshUnzippedData.
 */
template <class NodeIterable, class BundleNodalDataCallback>
std::tuple<std::vector<unsigned int>, std::vector<double>>
unzip_nodal_mesh_data(const NodeIterable &nodes,
                      const BundleNodalDataCallback bundle_nodal_data) {
  auto out_int = std::vector<unsigned int>{};
  auto out_double = std::vector<double>{};
  for (const auto &node : nodes) {
    // we simply add all local nodes, which can potential lead to duplication
    // of nodes by different processes.
    const auto nodal_data = bundle_nodal_data(node);
    out_int.emplace_back(static_cast<unsigned int>(node.id));
    out_int.emplace_back(static_cast<unsigned int>(nodal_data.material_id));
    out_int.emplace_back(static_cast<unsigned int>(nodal_data.type));
    for (const auto x : node.position)
      out_double.emplace_back(x);
  }
  return std::make_tuple(out_int, out_double);
}

/**
 * @brief Serialize data according to @ref traits::NetCdfSchema.
 *
 * Reuses `cell_start` and `n_cells`.
 */
template <class T>
void serialize_nc(const NetCdfFileHandleW &ncfile, const T &data,
                  const std::size_t cell_start, const std::size_t n_cells) {
  using Schema = traits::NetCdfSchema<T>;

  const auto [node_start, n_nodes] = all_reduce_cum_sum(data.n_nodes);
  const auto dyn_dims =
      std::array{ncfile.define_dimension(dims::N_CELLS.name, n_cells),
                 ncfile.define_dimension(dims::N_NODES.name, n_nodes)};
  // First write all dimensions and store, the dimension ids created by pnetcdf here to reuse them in data mode:
  auto dimensions = std::array<Dimension, Schema::INT_TABLES.size() +
                                              Schema::DOUBLE_TABLES.size()>{};
  auto dim_counter = std::size_t{0};
  for (const auto &var : Schema::INT_TABLES)
    if (var.inner_dim.size > 0)
      dimensions[dim_counter++] =
          ncfile.define_dimension(var.inner_dim.name, var.inner_dim.size);
  for (const auto &var : Schema::DOUBLE_TABLES)
    if (var.inner_dim.size > 0)
      dimensions[dim_counter++] =
          ncfile.define_dimension(var.inner_dim.name, var.inner_dim.size);

  ncfile.switch_to_data_mode();

  const auto chunk_spans = std::array{ChunkSpan{cell_start, data.n_cells},
                                      ChunkSpan{node_start, data.n_nodes}};
  dim_counter = 0;
  for (const auto &var : Schema::INT_TABLES)
    if (var.inner_dim.size > 0) {
      const auto &span = chunk_spans[var.outer_dim.index];
      ncfile.write_2d_array_block(
          var.name, data.*(var.vec),
          std::array{dyn_dims[var.outer_dim.index], dimensions[dim_counter++]},
          span);
    }
  for (const auto &var : Schema::DOUBLE_TABLES)
    if (var.inner_dim.size > 0) {
      const auto &span = chunk_spans[var.outer_dim.index];
      const auto dims =
          std::array{dyn_dims[var.outer_dim.index], dimensions[dim_counter++]};
      ncfile.write_2d_array_block(var.name, data.*(var.vec), dims, span);
    }
}
/**
 * @brief Serialize data according to @ref traits::NetCdfSchema.
 */
template <class T>
void serialize_nc(const NetCdfFileHandleW &ncfile, const T &data) {
  const auto [cell_start, n_cells] = all_reduce_cum_sum(data.n_cells);
  serialize_nc(ncfile, data, cell_start, n_cells);
}

} // namespace detail

/**
 * @brief Write a restart mesh to a netcdf file
 *
 * Data saved is
 * 1. elements IDs, node IDs
 * 2. node coordinates
 * 3. Similar order node IDs, material indices of nodes
 * 4. element lattice vectors
 * 5. node internal degrees of freedom
 * 6. boundary conditions and bounds
 * 7. external force applicator if any
 *
 * The callbacks `bundle_cell_data` and `bundle_nodal_data` provide support for generic mesh types.
 *
 * @param bundle_cell_data Accepts a `qcmesh::mesh::SimplexCell<K, CellDataType>` instance and returns an instance of either @ref RestartNodalDataZeroT or @ref RestartNodalDataFiniteT.
 * @param bundle_nodal_data Accepts a `qcmesh::mesh::Node<D, NodalDataType>` instance and returns an instance of @ref RestartCellData.
 */
template <std::size_t D, std::size_t K, class NodalDataType, class CellDataType,
          class BundleNodalDataCallback, class BundleCellDataCallback>
void write_restart_file(
    const std::filesystem::path &path,
    const std::unordered_map<qcmesh::mesh::SimplexCellId,
                             qcmesh::mesh::SimplexCell<K, CellDataType>> &cells,
    const BundleCellDataCallback bundle_cell_data,
    const std::unordered_map<qcmesh::mesh::NodeId,
                             qcmesh::mesh::Node<D, NodalDataType>> &nodes,
    const BundleNodalDataCallback bundle_nodal_data,
    const RestartData<D> &restart_data,
    const std::vector<boundarycondition::Boundarycondition<D>>
        &boundary_conditions,
    const boundarycondition::ExternalForceApplicator<D>
        &external_force_applicator) {
  using BundledNodalDataType =
      decltype(bundle_nodal_data(qcmesh::mesh::Node<D, NodalDataType>{}));
  using NodeDimensions = traits::NodeDimensions<BundledNodalDataType>;
  auto unzipped_data =
      RestartUnzippedData<D, K, NodeDimensions::N_IC, NodeDimensions::N_TH>{
          cells.size(), nodes.size()};
  const auto &ordered_cells = OrderedMapValues(cells);
  const auto &ordered_nodes = OrderedMapValues(nodes);
  unzipped_data.cell_ids = detail::collect_cell_int_data(ordered_cells);
  unzipped_data.cell_data =
      detail::collect_cell_restart_data(ordered_cells, bundle_cell_data);
  std::tie(unzipped_data.node_integers, unzipped_data.node_coordinates,
           unzipped_data.node_thermal_dofs, unzipped_data.node_impurities) =
      detail::unzip_nodal_restart_data(ordered_nodes, bundle_nodal_data);

  std::filesystem::create_directories(path.parent_path());
  const auto ncfile = NetCdfFileHandleW(path);

  ncfile.write_attribute("output file iterator", restart_data.outputfile_iter);
  ncfile.write_attribute("restart file iterator",
                         restart_data.restartfile_iter);
  ncfile.write_attribute("number of zones", boundary_conditions.size());
  ncfile.write_attribute("external strain", restart_data.external_strain);
  ncfile.write_attribute("external strain increment",
                         restart_data.external_strain_increment);
  ncfile.write_attribute("external strain time step",
                         restart_data.external_strain_time_step);
  ncfile.write_attribute("apply external fixed box",
                         restart_data.apply_fixed_box);
  ncfile.write_attribute("periodic lengths", restart_data.periodic_lengths);
  // output boundary constraints/conditions
  auto zone_counter = std::size_t{1};
  for (const auto &boundary_condition : boundary_conditions) {
    const auto boundary_attribute_number =
        utils::to_string_with_zeros(zone_counter++, 2);
    ncfile.write_attribute(
        "boundaryType_" + boundary_attribute_number,
        std::string{zone_type_name(boundary_condition.zone_type)});
    ncfile.write_attribute("bounds_" + boundary_attribute_number,
                           boundary_condition.bound);
    ncfile.write_attribute("boundaryCoordinate_" + boundary_attribute_number,
                           boundary_condition.coordinate);
    ncfile.write_attribute("boundaryNormal_" + boundary_attribute_number,
                           boundary_condition.normal);
  }
  ncfile.write_attribute(
      "external force applicator",
      std::string{force_applicator_name(external_force_applicator.type)});
  ncfile.write_attribute("force applicator center",
                         external_force_applicator.center);
  ncfile.write_attribute("force applicator radius",
                         external_force_applicator.radius);
  ncfile.write_attribute("force applicator force constant",
                         external_force_applicator.force_constant);
  ncfile.write_attribute("force applicator increments",
                         external_force_applicator.delta);

  detail::serialize_nc(ncfile, unzipped_data);
}

/**
 * @brief Write a mesh to a netcdf file
 *
 * Data saved is
 * 1. elements IDs, node IDs
 * 2. node coordinates
 * 3. Similar order node IDs, material indices of nodes
 * 4. element lattice vectors (optional)
 * 5. node internal degrees of freedom
 * 6. boundary conditions and bounds
 *
 * The callback `bundle_nodal_data` provides support for generic mesh types.
 *
 * @param bundle_nodal_data Accepts a `qcmesh::mesh::Node<D, NodalDataType>` instance and returns an instance of @ref MeshCellData.
 */
template <std::size_t D, std::size_t K, class NodalDataType, class CellDataType,
          class BundleNodalDataCallback>
void write_mesh_file(
    const std::filesystem::path &path,
    const std::unordered_map<qcmesh::mesh::SimplexCellId,
                             qcmesh::mesh::SimplexCell<K, CellDataType>> &cells,
    const std::unordered_map<qcmesh::mesh::NodeId,
                             qcmesh::mesh::Node<D, NodalDataType>> &nodes,
    const BundleNodalDataCallback bundle_nodal_data,
    const MeshData<D> &mesh_data) {
  using UnzippedDimensions = traits::NetCdfSchema<MeshUnzippedData<D, K>>;
  auto unzipped_data = MeshUnzippedData<D, K>{cells.size(), nodes.size()};
  const auto &ordered_cells = OrderedMapValues(cells);
  const auto &ordered_nodes = OrderedMapValues(nodes);
  unzipped_data.cell_ids = detail::collect_cell_int_data(ordered_cells);
  unzipped_data.cell_data = detail::collect_cell_mesh_data(ordered_cells);
  std::tie(unzipped_data.node_integers, unzipped_data.node_coordinates) =
      detail::unzip_nodal_mesh_data(ordered_nodes, bundle_nodal_data);

  std::filesystem::create_directories(path.parent_path());
  const auto ncfile = NetCdfFileHandleW(path);

  auto atomistic_box = std::vector<double>{};
  atomistic_box.reserve(2 * D);
  for (const auto x : mesh_data.atomistic_box_upper)
    atomistic_box.emplace_back(x);
  for (const auto x : mesh_data.atomistic_box_lower)
    atomistic_box.emplace_back(x);
  ncfile.write_attribute("atomist domain box", atomistic_box);
  ncfile.write_attribute("saved lattice vectors",
                         mesh_data.have_lattice_vectors);

  auto lattice_vec_dim_outer = Dimension{};
  const auto lattice_vec_dim_inner =
      UnzippedDimensions::LATTICE_VECTORS.inner_dim;
  const auto [cell_start, n_cells] = detail::all_reduce_cum_sum(cells.size());
  if (mesh_data.have_lattice_vectors)
    lattice_vec_dim_outer = ncfile.define_dimension(
        UnzippedDimensions::LATTICE_VECTORS.inner_dim.name,
        lattice_vec_dim_inner.size * n_cells);

  detail::serialize_nc(ncfile, unzipped_data);
  if (mesh_data.have_lattice_vectors) {
    const auto span = ChunkSpan{cell_start * lattice_vec_dim_inner.size,
                                cells.size() * lattice_vec_dim_inner.size};
    ncfile.write_1d_array_slice(UnzippedDimensions::LATTICE_VECTORS.name,
                                unzipped_data.*
                                    (UnzippedDimensions::LATTICE_VECTORS.vec),
                                lattice_vec_dim_outer, span);
  }
}

} // namespace netcdf_io
