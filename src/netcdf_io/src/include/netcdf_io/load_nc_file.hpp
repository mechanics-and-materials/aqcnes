// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load mesh and cell data from a restart file written by @ref write_restart_file.
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "meshing/bare_mesh.hpp"
#include "meshing/traits.hpp"
#include "netcdf_io/pnetcdfpp.hpp"
#include "netcdf_io/storage_schema.hpp"
#include "qcmesh/mesh/serialization/node.hpp"
#include "qcmesh/mesh/serialization/simplex_cell.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <array>
#include <boost/mpi.hpp>
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <boost/serialization/vector.hpp>
#include <filesystem>
#include <stdexcept>
#include <unordered_set>
#include <vector>

namespace netcdf_io {

namespace detail {
/**
 * @brief Extract a chunk of size `Size` at offset `offset` from a `std::vector` into a `std::array`.
 */
template <std::size_t Size, class T>
std::array<T, Size> collect_array(const std::vector<T> &data,
                                  const std::size_t offset) {
  auto arr = std::array<T, Size>{};
  for (std::size_t i = 0; i < Size; i++)
    arr[i] = data[offset + i];
  return arr;
}

/**
 * @brief Extract a chunk of size `Size x Size` at offset `offset` from a `std::vector` into a nested `std::array`.
 */
template <std::size_t Size, class T>
std::array<std::array<T, Size>, Size> collect_matrix(const std::vector<T> &data,
                                                     const std::size_t offset) {
  auto matrix = std::array<std::array<T, Size>, Size>{};
  auto l = std::size_t{0};
  for (std::size_t i = 0; i < Size; i++)
    for (std::size_t j = 0; j < Size; j++)
      matrix[i][j] = data[offset + l++];
  return matrix;
}

/**
 * @brief Create a @ref ChunkSpan based on the total size of a NetCdf dimension and the number of MPI processes.
 *
 * For a given mpi rank, the span will be defined by
 * - `chunk_size` as the smallest number such that `mpi_size * chunk_size >= total_size`,
 * - `start = mpi_rank * chunk_size`,
 * - `size = min(chunk_size, total_size - start_idx)`.
 */
ChunkSpan mpi_chunk_span(const std::size_t total_size);

/**
 * @brief Share nodes between all MPI processes such that each process has all
 * nodes referenced by any of its cells.
 */
template <std::size_t D, std::size_t K, class NodalDataType, class CellDataType>
void distribute_nodes(
    meshing::BareMesh<D, K, NodalDataType, CellDataType> &bare_mesh) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();
  if (mpi_size == 1)
    return;

  const auto available_node_ids = [&]() {
    auto out = std::unordered_set<qcmesh::mesh::NodeId>{};
    for (const auto &node : bare_mesh.nodes)
      out.insert(node.id);
    return out;
  }();
  auto locally_used_node_ids = std::unordered_set<qcmesh::mesh::NodeId>{};
  auto missing_node_ids = std::unordered_set<qcmesh::mesh::NodeId>{};
  for (const auto &cell : bare_mesh.cells)
    for (const auto node_id : cell.nodes)
      if (available_node_ids.find(node_id) != available_node_ids.end())
        locally_used_node_ids.insert(node_id);
      else
        missing_node_ids.insert(node_id);
  auto missing_node_ids_by_proc =
      std::vector<std::unordered_set<qcmesh::mesh::NodeId>>{};
  boost::mpi::all_gather(world, missing_node_ids, missing_node_ids_by_proc);
  auto nodes_to_send_by_proc =
      std::vector<std::vector<qcmesh::mesh::Node<D, NodalDataType>>>(mpi_size);
  for (const auto &node : bare_mesh.nodes)
    for (std::size_t i = 0; i < missing_node_ids_by_proc.size(); i++) {
      if (i == static_cast<std::size_t>(mpi_rank))
        continue;
      if (missing_node_ids_by_proc[i].find(node.id) !=
          missing_node_ids_by_proc[i].end())
        nodes_to_send_by_proc[i].push_back(node);
    }
  auto nodes_received_by_proc =
      std::vector<std::vector<qcmesh::mesh::Node<D, NodalDataType>>>(mpi_size);
  qcmesh::mpi::all_scatter(nodes_to_send_by_proc, nodes_received_by_proc);
  for (auto &nodes : nodes_received_by_proc)
    bare_mesh.nodes.insert(bare_mesh.nodes.end(), nodes.begin(), nodes.end());
}

/**
 * @brief Deserialize all arrays specified by @ref traits::NetCdfSchema from a
 * NetCdf file to an instance of `T`.
 */
template <class T> T deserialize_nc(const NetCdfFileHandleRO &ncfile) {
  using Schema = traits::NetCdfSchema<T>;
  auto data = T{};
  const auto dimensions = std::array{ncfile.read_dimension(dims::N_CELLS.name),
                                     ncfile.read_dimension(dims::N_NODES.name)};
  data.n_cells = dimensions[0];
  data.n_nodes = dimensions[1];
  const auto chunk_spans = std::array{detail::mpi_chunk_span(data.n_cells),
                                      detail::mpi_chunk_span(data.n_nodes)};
  for (const auto &var : Schema::INT_TABLES)
    if (var.inner_dim.size > 0) {
      ncfile.check_dimension(var.inner_dim);
      const auto &span = chunk_spans[var.outer_dim.index];
      data.*(var.vec) =
          ncfile.read_2d_uint_array_block(var.name, var.inner_dim.size, span);
    }
  for (const auto &var : Schema::DOUBLE_TABLES)
    if (var.inner_dim.size > 0) {
      ncfile.check_dimension(var.inner_dim);
      const auto &span = chunk_spans[var.outer_dim.index];
      data.*(var.vec) =
          ncfile.read_2d_double_array_block(var.name, var.inner_dim.size, span);
    }

  return data;
}

} // namespace detail

/**
 * @brief Load a restart NetCdf file to @ref meshing::BareMesh instance and a @ref RestartCellData instance.
 *
 * Ignores data for the external force applicator and boundary conditions.
 */
template <std::size_t D, std::size_t K, class NodalDataType>
std::tuple<meshing::BareMesh<D, K, NodalDataType, RestartCellData<D>>,
           RestartData<D>>
load_restart_file(const std::filesystem::path &path) {
  using NodeDimensions = traits::NodeDimensions<NodalDataType>;
  auto bare_mesh = meshing::BareMesh<D, K, NodalDataType, RestartCellData<D>>{};
  const auto ncfile = NetCdfFileHandleRO(path);
  auto data = RestartData<D>{};
  data.outputfile_iter = ncfile.read_int("output file iterator");
  data.restartfile_iter = ncfile.read_int("restart file iterator");
  data.external_strain = ncfile.read_double("external strain");
  data.external_strain_increment =
      ncfile.read_double("external strain increment");
  data.external_strain_time_step =
      ncfile.read_double("external strain time step");
  const auto periodic_lengths = ncfile.read_double_vec("periodic lengths");
  for (std::size_t i = 0; i < periodic_lengths.size(); i++)
    data.periodic_lengths[i] = periodic_lengths[i];
  data.apply_fixed_box = ncfile.read_bool("apply external fixed box");

  const auto unzipped_data = detail::deserialize_nc<
      RestartUnzippedData<D, K, NodeDimensions::N_IC, NodeDimensions::N_TH>>(
      ncfile);

  using UnzippedDimensions = traits::NetCdfSchema<
      RestartUnzippedData<D, K, NodeDimensions::N_IC, NodeDimensions::N_TH>>;
  const auto node_span = detail::mpi_chunk_span(unzipped_data.n_nodes);
  auto node_ids = std::unordered_set<qcmesh::mesh::NodeId>{};
  for (std::size_t i = 0; i < node_span.size; i++) {
    const auto id = qcmesh::mesh::NodeId{static_cast<std::size_t>(
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size])};
    const auto [_, is_new_id] = node_ids.emplace(id);
    // Skip duplicate nodes. This can happen when the number of
    // processes used for writing and reading differ:
    if (!is_new_id)
      continue;
    auto node_data = NodalDataType{};
    node_data.material_id =
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size + 1];
    node_data.flags =
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size + 3];
    if constexpr (UnzippedDimensions::NODE_THERMAL_DOFS.size > 0) {
      constexpr std::size_t N_THERMAL_DOFS =
          UnzippedDimensions::NODE_THERMAL_DOFS.size - 1;
      node_data.thermal_point = detail::collect_array<N_THERMAL_DOFS>(
          unzipped_data.node_thermal_dofs,
          i * UnzippedDimensions::NODE_THERMAL_DOFS.size);
      node_data.entropy =
          unzipped_data.node_thermal_dofs
              [(i + 1) * UnzippedDimensions::NODE_THERMAL_DOFS.size - 1];
    }
    if constexpr (UnzippedDimensions::NODE_IC.size > 0)
      node_data.impurity_concentrations =
          detail::collect_array<UnzippedDimensions::NODE_IC.size>(
              unzipped_data.node_impurities,
              i * UnzippedDimensions::NODE_IC.size);
    node_data.weight =
        unzipped_data
            .node_coordinates[(i + 1) * UnzippedDimensions::NODE_COORDS.size -
                              1];
    node_data.periodic_boundary_flag = static_cast<bool>(
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size + 4]);
    node_data.type = static_cast<int>(
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size + 2]);
    bare_mesh.nodes.emplace_back(qcmesh::mesh::Node<D, NodalDataType>{
        .id = id,
        .position =
            detail::collect_array<D>(unzipped_data.node_coordinates,
                                     i * UnzippedDimensions::NODE_COORDS.size),
        .data = node_data});
  }
  const auto cell_span = detail::mpi_chunk_span(unzipped_data.n_cells);
  for (std::size_t i = 0; i < cell_span.size; i++) {
    const auto nodes_uint =
        detail::collect_array<K + 1>(unzipped_data.cell_ids, i * (K + 2) + 1);
    auto nodes = std::array<qcmesh::mesh::NodeId, K + 1>{};
    for (std::size_t j = 0; j < K + 1; j++)
      nodes[j] = qcmesh::mesh::NodeId{nodes_uint[j]};

    bare_mesh.cells.emplace_back(qcmesh::mesh::SimplexCell<K,
                                                           RestartCellData<D>>{
        .id = qcmesh::mesh::SimplexCellId{unzipped_data.cell_ids[i * (K + 2)]},
        .nodes = nodes,
        .data = RestartCellData<D>{
            .is_atomistic = static_cast<bool>(
                unzipped_data
                    .cell_data[(i + 1) * UnzippedDimensions::CELL_DATA.size -
                               1]),
            .density = unzipped_data.cell_data
                           [(i + 1) * UnzippedDimensions::CELL_DATA.size - 4],
            .jacobian = unzipped_data.cell_data
                            [(i + 1) * UnzippedDimensions::CELL_DATA.size - 3],
            .weight = unzipped_data.cell_data
                          [(i + 1) * UnzippedDimensions::CELL_DATA.size - 2],
            .basis = detail::collect_matrix<D>(
                unzipped_data.cell_data,
                i * UnzippedDimensions::CELL_DATA.size),
        }});
  }
  detail::distribute_nodes(bare_mesh);
  return std::make_tuple(bare_mesh, data);
}

/**
 * @brief Load a mesh NetCdf file to @ref meshing::BareMesh instance and a @ref MeshData instance.
 */
template <std::size_t D, std::size_t K>
std::tuple<meshing::BareMesh<D, K, MeshNodalData, MeshCellData<D>>, MeshData<D>>
load_mesh_file(const std::filesystem::path &path) {
  auto bare_mesh = meshing::BareMesh<D, K, MeshNodalData, MeshCellData<D>>{};
  const auto ncfile = NetCdfFileHandleRO(path);
  const auto atomistic_box = ncfile.read_double_vec("atomist domain box");
  if (atomistic_box.size() != 2 * D)
    throw std::runtime_error{
        "Dimension mismatch for atomist domain box: Expected " +
        std::to_string(2 * D) + ", got " +
        std::to_string(atomistic_box.size())};
  auto data = MeshData<D>{
      .have_lattice_vectors = ncfile.read_bool("saved lattice vectors"),
      .atomistic_box_lower = detail::collect_array<D>(atomistic_box, D),
      .atomistic_box_upper = detail::collect_array<D>(atomistic_box, 0),
  };

  auto unzipped_data = detail::deserialize_nc<MeshUnzippedData<D, K>>(ncfile);
  using UnzippedDimensions = traits::NetCdfSchema<MeshUnzippedData<D, K>>;
  const auto cell_span = detail::mpi_chunk_span(unzipped_data.n_cells);
  const auto inner_dim = UnzippedDimensions::LATTICE_VECTORS.inner_dim;
  if (data.have_lattice_vectors) {
    ncfile.check_dimension(
        DimensionDef{inner_dim.name, inner_dim.size * unzipped_data.n_cells});
    unzipped_data.*(UnzippedDimensions::LATTICE_VECTORS.vec) =
        ncfile.read_1d_double_array_slice(
            UnzippedDimensions::LATTICE_VECTORS.name,
            ChunkSpan{cell_span.start * inner_dim.size,
                      cell_span.size * inner_dim.size});
  }

  const auto node_span = detail::mpi_chunk_span(unzipped_data.n_nodes);
  auto node_ids = std::unordered_set<qcmesh::mesh::NodeId>{};
  for (std::size_t i = 0; i < node_span.size; i++) {
    const auto id = qcmesh::mesh::NodeId{static_cast<std::size_t>(
        unzipped_data
            .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size])};
    const auto [_, is_new_id] = node_ids.emplace(id);
    // Skip duplicate nodes. This can happen when the number of
    // processes used for writing and reading differ:
    if (!is_new_id)
      continue;
    bare_mesh.nodes.emplace_back(qcmesh::mesh::Node<D, MeshNodalData>{
        .id = id,
        .position =
            detail::collect_array<D>(unzipped_data.node_coordinates,
                                     i * UnzippedDimensions::NODE_COORDS.size),
        .data = MeshNodalData{
            .material_id = unzipped_data.node_integers
                               [i * UnzippedDimensions::NODE_INTEGERS.size + 1],
            .type = static_cast<int>(
                unzipped_data
                    .node_integers[i * UnzippedDimensions::NODE_INTEGERS.size +
                                   2]),
        }});
  }
  for (std::size_t i = 0; i < cell_span.size; i++) {
    const auto nodes_uint =
        detail::collect_array<K + 1>(unzipped_data.cell_ids, i * (K + 2) + 1);
    auto nodes = std::array<qcmesh::mesh::NodeId, K + 1>{};
    for (std::size_t j = 0; j < K + 1; j++)
      nodes[j] = qcmesh::mesh::NodeId{nodes_uint[j]};

    auto cell_data = MeshCellData<D>{};
    if (data.have_lattice_vectors)
      cell_data.basis = detail::collect_matrix<D>(
          unzipped_data.cell_data,
          i * UnzippedDimensions::CELL_LATTICE_VECTORS.size);
    bare_mesh.cells.emplace_back(qcmesh::mesh::SimplexCell<K, MeshCellData<D>>{
        .id = qcmesh::mesh::SimplexCellId{unzipped_data.cell_ids[i * (K + 2)]},
        .nodes = nodes,
        .data = cell_data});
  }
  detail::distribute_nodes(bare_mesh);
  return std::make_tuple(bare_mesh, data);
}

} // namespace netcdf_io

namespace qcmesh::mesh {

/**
 * @brief Implement @ref traits::LatticeBasis for @ref SimplexCell holding @ref netcdf_io::RestartCellData cell data.
 */
template <std::size_t Dimension>
struct traits::LatticeBasis<
    SimplexCell<Dimension, netcdf_io::RestartCellData<Dimension>>> {
  constexpr static std::size_t DIMENSION = Dimension;
  inline static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(
      SimplexCell<Dimension, netcdf_io::RestartCellData<Dimension>> &cell) {
    return cell.data.basis;
  }
  inline static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(
      const SimplexCell<Dimension, netcdf_io::RestartCellData<Dimension>>
          &cell) {
    return cell.data.basis;
  }
};

/**
 * @brief Implement @ref traits::LatticeBasis for @ref SimplexCell holding @ref netcdf_io::MeshCellData cell data.
 */
template <std::size_t Dimension>
struct traits::LatticeBasis<
    SimplexCell<Dimension, netcdf_io::MeshCellData<Dimension>>> {
  constexpr static std::size_t DIMENSION = Dimension;
  inline static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(
      SimplexCell<Dimension, netcdf_io::MeshCellData<Dimension>> &cell) {
    return cell.data.basis;
  }
  inline static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(
      const SimplexCell<Dimension, netcdf_io::MeshCellData<Dimension>> &cell) {
    return cell.data.basis;
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::Jacobian for @ref netcdf_io::RestartCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::Jacobian<netcdf_io::RestartCellData<D>> {
  static double jacobian(const netcdf_io::RestartCellData<D> &data) {
    return data.jacobian;
  }
  static double &jacobian(netcdf_io::RestartCellData<D> &data) {
    return data.jacobian;
  }
};

} // namespace qcmesh::mesh

namespace meshing {

/**
 * @brief Implement @ref traits::Weight for @ref netcdf_io::RestartCellData.
 */
template <std::size_t D> struct traits::Weight<netcdf_io::RestartCellData<D>> {
  static double weight(const netcdf_io::RestartCellData<D> &data) {
    return data.weight;
  }
};
/**
 * @brief Implement @ref traits::Density for @ref netcdf_io::RestartCellData.
 */
template <std::size_t D> struct traits::Density<netcdf_io::RestartCellData<D>> {
  inline static double density(const netcdf_io::RestartCellData<D> &data) {
    return data.density;
  }
};

/**
 * @brief Implement @ref traits::Weight for @ref netcdf_io::RestartNodalDataFiniteT.
 */
template <std::size_t NOI>
struct traits::Weight<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static double weight(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.weight;
  }
};

/**
 * @brief Implementation of @ref traits::NImpurities for @ref netcdf_io::RestartNodalDataFiniteT.
 */
template <std::size_t Noi>
struct traits::NImpurities<netcdf_io::RestartNodalDataFiniteT<Noi>> {
  constexpr static std::size_t VALUE = Noi;
};

/**
 * @brief Implement @ref traits::ImpurityConcentrations for @ref netcdf_io::RestartNodalDataFiniteT.
 */
template <std::size_t Noi>
struct traits::ImpurityConcentrations<netcdf_io::RestartNodalDataFiniteT<Noi>> {
  static const std::array<
      double, traits::N_IMPURITIES<netcdf_io::RestartNodalDataFiniteT<Noi>>> &
  impurity_concentrations(const netcdf_io::RestartNodalDataFiniteT<Noi> &data) {
    return data.impurity_concentrations;
  }
  static std::array<
      double, traits::N_IMPURITIES<netcdf_io::RestartNodalDataFiniteT<Noi>>> &
  impurity_concentrations(netcdf_io::RestartNodalDataFiniteT<Noi> &data) {
    return data.impurity_concentrations;
  }
};

/**
 * @brief Implement @ref traits::Weight for @ref netcdf_io::RestartNodalDataZeroT.
 */
template <std::size_t NOI>
struct traits::Weight<netcdf_io::RestartNodalDataZeroT<NOI>> {
  inline static double
  weight(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.weight;
  }
};

/**
 * @brief Implementation of @ref traits::NImpurities for @ref netcdf_io::RestartNodalDataZeroT.
 */
template <std::size_t Noi>
struct traits::NImpurities<netcdf_io::RestartNodalDataZeroT<Noi>> {
  constexpr static std::size_t VALUE = Noi;
};

/**
 * @brief Implement @ref traits::ImpurityConcentrations for @ref netcdf_io::RestartNodalDataZeroT.
 */
template <std::size_t NOI>
struct traits::ImpurityConcentrations<netcdf_io::RestartNodalDataZeroT<NOI>> {
  static const std::array<double, NOI> &
  impurity_concentrations(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.impurity_concentrations;
  }
  static std::array<double, NOI> &
  impurity_concentrations(netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.impurity_concentrations;
  }
};

} // namespace meshing

/**
 * @brief Implement @ref qcmesh::mesh::traits::IsAtomistic for @ref netcdf_io::RestartCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::IsAtomistic<netcdf_io::RestartCellData<D>> {
  inline static bool is_atomistic(const netcdf_io::RestartCellData<D> &data) {
    return data.is_atomistic;
  }
};

namespace boost {
namespace serialization {

/**
 * @brief Implement serialization for @ref netcdf_io::RestartCellData.
 */
template <typename Archive, std::size_t Dimension>
void serialize(Archive &ar, netcdf_io::RestartCellData<Dimension> &data,
               const unsigned int) {
  ar &data.is_atomistic;
  ar &data.density;
  ar &data.jacobian;
  ar &data.weight;
  ar &data.basis;
}

/**
 * See
 * https://www.boost.org/doc/libs/release/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <std::size_t Dimension>
struct tracking_level<netcdf_io::RestartCellData<Dimension>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<netcdf_io::RestartCellData<Dimension>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t Dimension>
struct implementation_level<netcdf_io::RestartCellData<Dimension>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

/**
 * @brief Implement serialization for @ref netcdf_io::MeshCellData.
 */
template <typename Archive, std::size_t Dimension>
void serialize(Archive &ar, netcdf_io::MeshCellData<Dimension> &data,
               const unsigned int) {
  ar &data.basis;
}

/**
 * See
 * https://www.boost.org/doc/libs/release/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <std::size_t Dimension>
struct tracking_level<netcdf_io::MeshCellData<Dimension>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<netcdf_io::MeshCellData<Dimension>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t Dimension>
struct implementation_level<netcdf_io::MeshCellData<Dimension>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

/**
 * @brief Implement serialization for @ref netcdf_io::RestartNodalDataZeroT.
 */
template <typename Archive, std::size_t NOI>
void serialize(Archive &ar, netcdf_io::RestartNodalDataZeroT<NOI> &data,
               const unsigned int) {
  ar &data.material_id;
  ar &data.type;
  ar &data.flags;
  ar &data.weight;
  ar &data.periodic_boundary_flag;
  ar &data.impurity_concentrations;
}
template <std::size_t NOI>
struct tracking_level<netcdf_io::RestartNodalDataZeroT<NOI>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<netcdf_io::RestartNodalDataZeroT<NOI>>,
                    mpl::int_<primitive_type>>::value));
};

/**
 * @brief Implement serialization for @ref netcdf_io::MeshNodalData.
 */
template <typename Archive>
void serialize(Archive &ar, netcdf_io::MeshNodalData &data,
               const unsigned int) {
  ar &data.material_id;
}

template <std::size_t NOI>
struct implementation_level<netcdf_io::RestartNodalDataZeroT<NOI>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

/**
 * @brief Implement serialization for @ref netcdf_io::RestartNodalDataFiniteT.
 */
template <typename Archive, std::size_t NOI>
void serialize(Archive &ar, netcdf_io::RestartNodalDataFiniteT<NOI> &data,
               const unsigned int) {
  ar &data.material_id;
  ar &data.type;
  ar &data.flags;
  ar &data.weight;
  ar &data.periodic_boundary_flag;
  ar &data.impurity_concentrations;
  ar &data.thermal_point;
  ar &data.entropy;
}
template <std::size_t NOI>
struct tracking_level<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<
          implementation_level<netcdf_io::RestartNodalDataFiniteT<NOI>>,
          mpl::int_<primitive_type>>::value));
};

template <std::size_t NOI>
struct implementation_level<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

} // namespace serialization

namespace mpi {
template <std::size_t Dimension>
struct is_mpi_datatype<netcdf_io::RestartCellData<Dimension>>
    : public mpl::true_ {};
template <std::size_t Dimension>
struct is_mpi_datatype<netcdf_io::MeshCellData<Dimension>> : public mpl::true_ {
};
template <std::size_t NOI>
struct is_mpi_datatype<netcdf_io::RestartNodalDataZeroT<NOI>>
    : public mpl::true_ {};
template <std::size_t NOI>
struct is_mpi_datatype<netcdf_io::RestartNodalDataFiniteT<NOI>>
    : public mpl::true_ {};
} // namespace mpi

} // namespace boost

BOOST_CLASS_IMPLEMENTATION(netcdf_io::MeshNodalData, object_serializable)
BOOST_CLASS_TRACKING(netcdf_io::MeshNodalData, track_never)
BOOST_IS_MPI_DATATYPE(netcdf_io::MeshNodalData)
