// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief C++ wrapper of the pnetcdf library.
 * @author G. Bräunlich
 */

#pragma once
#include "netcdf_io/chunk_span.hpp"
#include "netcdf_io/dimension_def.hpp"
#include <filesystem>
#include <memory>
#include <string_view>
#include <vector>

namespace netcdf_io {

/**
 * @brief Base class for @ref NetCdfFileHandleRO and @ref NetCdfFileHandleW.
 *
 * Handles opening and closing of NetCdf files.
 */
class NetCdfFileHandle {
public:
  NetCdfFileHandle() = delete;

protected:
  explicit NetCdfFileHandle(const int ncid);
  static void close(void *);
  using HandleManager = std::unique_ptr<int, decltype(&close)>;
  int ncfile{};
  HandleManager handle_manager;
};

/**
 * @brief NetCdf file object for reading data.
 */
class NetCdfFileHandleRO : NetCdfFileHandle {
public:
  /**
   * @brief Open a pnetcdf file in read only mode.
   *
   * The file is automatically closed when
   * the instance of NetCdfFileHandle is destroyed.
   *
   * @param path Path to the file to open.
   */
  explicit NetCdfFileHandleRO(const std::filesystem::path &path);

private:
  /**
   * @brief Open the file.
   */
  static int open(const std::filesystem::path &path);

public:
  /**
   * @brief Read the size of a nc dimension.
   */
  [[nodiscard]] std::size_t read_dimension(const std::string_view &name) const;
  /**
   * @brief Read an nc int attribute.
   */
  [[nodiscard]] int read_int(const std::string_view &name) const;
  /**
   * @brief Read an nc bool attribute.
   */
  [[nodiscard]] bool read_bool(const std::string_view &name) const;
  /**
   * @brief Read an nc double attribute.
   */
  [[nodiscard]] double read_double(const std::string_view &name) const;
  /**
   * @brief Read an nc text attribute.
   */
  [[nodiscard]] std::string read_string(const std::string_view &name) const;
  /**
   * @brief Read a multi value nc double attribute.
   */
  [[nodiscard]] std::vector<double>
  read_double_vec(const std::string_view &name) const;
  /**
   * @brief Check that the nc dimension specified by `def` has the correct size.
   * @throws std::runtime_error If the sizes do not match.
   */
  void check_dimension(const DimensionDef &def) const;
  /**
   * @brief Read a nc double variable of 1 dimension.
   */
  [[nodiscard]] std::vector<double>
  read_1d_double_array_slice(const std::string_view &name,
                             const ChunkSpan &chunk_span) const;
  /**
   * @brief Read a nc unsigned int variable of 2 dimensions.
   */
  [[nodiscard]] std::vector<unsigned int>
  read_2d_uint_array_block(const std::string_view &name, const std::size_t size,
                           const ChunkSpan &chunk_span) const;
  /**
   * @brief Read a nc double variable of 2 dimensions.
   */
  [[nodiscard]] std::vector<double>
  read_2d_double_array_block(const std::string_view &name,
                             const std::size_t size,
                             const ChunkSpan &chunk_span) const;
};

/**
 * @brief Representation of a nc dimension.
 */
struct Dimension {
  int id;
  std::size_t size;
};

/**
 * @brief NetCdf file object for writing data.
 */
class NetCdfFileHandleW : NetCdfFileHandle {
public:
  /**
   * @brief Open a pnetcdf file in write only mode.
   *
   * The file is automatically closed when
   * the instance of NetCdfFileHandle is destroyed.
   *
   * Currently, all dimensions and attributes have to be written first,
   * followed by a call to @ref switch_to_data_mode before nc variables can
   * be written.
   *
   * @param path Path to the file to open.
   */
  explicit NetCdfFileHandleW(const std::filesystem::path &path);

private:
  /**
   * @brief Open the file.
   */
  static int open(const std::filesystem::path &path);

public:
  /**
   * @brief Write a nc dimension with `name` and `size`.
   */
  [[nodiscard]] Dimension define_dimension(const std::string_view &name,
                                           const std::size_t size) const;
  /**
   * @brief Write a nc dimension specified by `def`.
   */
  [[nodiscard]] Dimension define_dimension(const DimensionDef &def) const;
  /**
   * @brief Switch to data mode to allow calls to @ref write_2d_array_block, @ref write_1d_array_slice.
   */
  void switch_to_data_mode() const;
  /**
   * @brief Write a bool nc attribute.
   */
  void write_attribute(const std::string_view &name, const bool value) const;
  /**
   * @brief Write an int nc attribute.
   */
  void write_attribute(const std::string_view &name, const int value) const;
  /**
   * @brief Write an int nc attribute casted from a std::size_t.
   */
  void write_attribute(const std::string_view &name,
                       const std::size_t value) const;
  /**
   * @brief Write a double nc attribute.
   */
  void write_attribute(const std::string_view &name, const double value) const;
  /**
   * @brief Write a double nc attribute with 2 values.
   */
  void write_attribute(const std::string_view &name,
                       const std::array<double, 2> &values) const;
  /**
   * @brief Write a double nc attribute with 3 values.
   */
  void write_attribute(const std::string_view &name,
                       const std::array<double, 3> &values) const;
  /**
   * @brief Write a double nc attribute with multiple values.
   */
  void write_attribute(const std::string_view &name,
                       const std::vector<double> &values) const;
  /**
   * @brief Write a text nc attribute with 3 values.
   */
  void write_attribute(const std::string_view &name,
                       const std::string &value) const;
  /**
   * @brief Write a block of a unsigned int nc 2-dim variable.
   */
  void write_2d_array_block(const std::string_view &name,
                            const std::vector<unsigned int> &data,
                            const std::array<Dimension, 2> &dimensions,
                            const ChunkSpan &chunk_span) const;
  /**
   * @brief Write a block of a double nc 2-dim variable.
   */
  void write_2d_array_block(const std::string_view &name,
                            const std::vector<double> &data,
                            const std::array<Dimension, 2> &dimensions,
                            const ChunkSpan &chunk_span) const;
  /**
   * @brief Write a slice of a double nc 1-dim variable.
   */
  void write_1d_array_slice(const std::string_view &name,
                            const std::vector<double> &data,
                            const Dimension &dimension,
                            const ChunkSpan &chunk_span) const;
};

} // namespace netcdf_io
