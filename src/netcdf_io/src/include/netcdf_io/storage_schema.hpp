// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Shared schema for reading and writing restart files.
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "boundarycondition/external_force_applicator.hpp"
#include "meshing/traits.hpp"
#include "netcdf_io/chunk_span.hpp"
#include "netcdf_io/dimension_def.hpp"
#include <array>
#include <vector>

namespace netcdf_io {

/**
 * @brief Shared schema between write and read.
 */
namespace traits {

/**
 * @brief Infos about dimensions of fields of nodal data.
 */
template <class T> struct NodeDimensions {};
/**
 * @brief Schema defining multidimensional arrays stored in NetCdf files.
 */
template <class T> struct NetCdfSchema {};

} // namespace traits

/**
 * @brief Cell data when reading a restart file.
 */
template <std::size_t Dimension> struct RestartCellData {
  bool is_atomistic{};
  double density{};
  double jacobian{};
  double weight{};
  std::array<std::array<double, Dimension>, Dimension> basis{};
};

/**
 * @brief Restart file nodal data for T=0.
 */
template <std::size_t NOI> struct RestartNodalDataZeroT {
  std::size_t material_id{};
  int type{};
  unsigned int flags{};
  double weight{};
  bool periodic_boundary_flag{};
  std::array<double, NOI> impurity_concentrations{};
};

template <std::size_t NOI>
struct traits::NodeDimensions<RestartNodalDataZeroT<NOI>> {
  constexpr static std::size_t N_IC = NOI;
  constexpr static std::size_t N_TH = 0;
};

} // namespace netcdf_io
template <std::size_t NOI>
struct meshing::traits::MaterialId<netcdf_io::RestartNodalDataZeroT<NOI>> {
  static std::size_t
  material_id(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.material_id;
  }
};
template <std::size_t NOI>
struct meshing::traits::PeriodicBoundaryFlag<
    netcdf_io::RestartNodalDataZeroT<NOI>> {
  static bool
  periodic_boundary_flag(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.periodic_boundary_flag;
  }
};
template <std::size_t NOI>
struct meshing::traits::IsFixed<netcdf_io::RestartNodalDataZeroT<NOI>> {
  static bool is_fixed(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.flags != 0;
  }
};
template <std::size_t NOI>
struct meshing::traits::NodeType<netcdf_io::RestartNodalDataZeroT<NOI>> {
  static int node_type(const netcdf_io::RestartNodalDataZeroT<NOI> &data) {
    return data.type;
  }
};
namespace netcdf_io {

/**
 * @brief Restart file nodal data for T>0.
 */
template <std::size_t NOI> struct RestartNodalDataFiniteT {
  std::size_t material_id{};
  int type{};
  unsigned int flags{};
  double weight{};
  bool periodic_boundary_flag{};
  std::array<double, NOI> impurity_concentrations{};
  std::array<double, 2> thermal_point{};
  double entropy{};
};

template <std::size_t NOI>
struct traits::NodeDimensions<RestartNodalDataFiniteT<NOI>> {
  constexpr static std::size_t N_IC = NOI;
  constexpr static std::size_t N_TH = 3;
};

} // namespace netcdf_io
template <std::size_t NOI>
struct meshing::traits::MaterialId<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static std::size_t
  material_id(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.material_id;
  }
};
template <std::size_t NOI>
struct meshing::traits::PeriodicBoundaryFlag<
    netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static bool
  periodic_boundary_flag(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.periodic_boundary_flag;
  }
};
template <std::size_t NOI>
struct meshing::traits::IsFixed<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static bool is_fixed(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.flags != 0;
  }
};
template <std::size_t NOI>
struct meshing::traits::NodeType<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static int node_type(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.type;
  }
};
template <std::size_t NOI>
struct meshing::traits::ThermalCoordinates<
    netcdf_io::RestartNodalDataFiniteT<NOI>> {
  constexpr static std::size_t DIMENSION = 2;
  static const std::array<double, DIMENSION> &
  thermal_coordinates(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.thermal_point;
  }
};
template <std::size_t NOI>
struct meshing::traits::Entropy<netcdf_io::RestartNodalDataFiniteT<NOI>> {
  static double entropy(const netcdf_io::RestartNodalDataFiniteT<NOI> &data) {
    return data.entropy;
  }
};
namespace netcdf_io {

/**
 * @brief Extra data when reading a restart file.
 */
template <std::size_t D> struct RestartData {
  std::size_t outputfile_iter = 0;
  std::size_t restartfile_iter = 0;
  double external_strain = 0.0;
  double external_strain_increment = 0.0;
  double external_strain_time_step = 0.0;
  std::array<double, D> periodic_lengths{0.0};
  bool apply_fixed_box = false;
};

/**
 * @brief Representation of a NetCdf dimension whose size is only known at runtime.
 *
 * Currently only in use for number of cells and number of nodes.
 * A dynamic dimension contains a name and an index.
 * The index is needed so that variables can refer by position to the
 * corresponding dimension defined during run time, stored in a std::array.
 */
struct DynamicDimension {
  std::string_view name;
  std::size_t index;
};

/**
 * @brief Represents a 2-dim NetCdf variable with dimensions `(outer_dim, inner_dim)`.
 */
template <class Container, class T> struct NC2dArray {
  std::string_view name; ///< nc name of the variable
  std::vector<T> Container::*vec;
  ///< Pointer to data member of class Container, this variable will be serialized to / deserialized from
  const DynamicDimension &outer_dim;
  const DimensionDef &inner_dim;
};

/**
 * @brief Convenence factory for @ref NC2dArray objects.
 */
template <class Container, class T>
constexpr NC2dArray<Container, T> make_nc_table(
    const std::string_view &name, std::vector<T> Container::*const vec,
    const DynamicDimension &outer_dim, const DimensionDef &inner_dim) {
  return NC2dArray<Container, T>{name, vec, outer_dim, inner_dim};
}

/**
 * @brief C++ representation of the content (arrays only) of a NetCdf restart file.
 */
template <std::size_t D, std::size_t K, std::size_t NOI, std::size_t NTh>
struct RestartUnzippedData {
  std::size_t n_cells{};
  std::size_t n_nodes{};
  std::vector<unsigned int> cell_ids{};
  std::vector<double> cell_data{};
  std::vector<unsigned int> node_integers{};
  std::vector<double> node_coordinates{};
  std::vector<double> node_thermal_dofs{};
  std::vector<double> node_impurities{};
};

/**
 * @brief NetCdf dimensions and sizes shared between read and write.
 */
namespace dims {

constexpr static DynamicDimension N_CELLS = DynamicDimension{"mesh cells", 0};
constexpr static DynamicDimension N_NODES = DynamicDimension{"mesh nodes", 1};

constexpr std::size_t restart_cell_data_size(const std::size_t d) {
  return d * d + 4;
}

} // namespace dims

/**
 * @brief NetCdf schema for restart data (arrays only).
 */
template <std::size_t D, std::size_t K, std::size_t NOI, std::size_t NTh>
struct traits::NetCdfSchema<RestartUnzippedData<D, K, NOI, NTh>> {
  constexpr static DimensionDef CELL_DATA =
      DimensionDef{"element data size", dims::restart_cell_data_size(D)};
  constexpr static DimensionDef CELL_IDS =
      DimensionDef{"cell id and node ids", K + 2};
  constexpr static DimensionDef NODE_INTEGERS =
      DimensionDef{"node int data", 5};
  constexpr static DimensionDef NODE_IC = DimensionDef{"nodal data", NOI};
  constexpr static DimensionDef NODE_COORDS =
      DimensionDef{"node coords", D + 1};
  constexpr static DimensionDef NODE_THERMAL_DOFS =
      DimensionDef{"node thermal dofs", NTh};
  constexpr static std::array DIMENSIONS = std::array{
      CELL_DATA, CELL_IDS, NODE_INTEGERS, NODE_IC, NODE_COORDS,
  };
  constexpr static std::array INT_TABLES = std::array{
      make_nc_table("cell and nodes",
                    &RestartUnzippedData<D, K, NOI, NTh>::cell_ids,
                    dims::N_CELLS, CELL_IDS),
      make_nc_table("node keys material ids site types periodic boundary flags",
                    &RestartUnzippedData<D, K, NOI, NTh>::node_integers,
                    dims::N_NODES, NODE_INTEGERS),
  };
  constexpr static std::array DOUBLE_TABLES = std::array{
      make_nc_table("element data",
                    &RestartUnzippedData<D, K, NOI, NTh>::cell_data,
                    dims::N_CELLS, CELL_DATA),
      make_nc_table("coordinates",
                    &RestartUnzippedData<D, K, NOI, NTh>::node_coordinates,
                    dims::N_NODES, NODE_COORDS),
      make_nc_table("node thermal dofs",
                    &RestartUnzippedData<D, K, NOI, NTh>::node_thermal_dofs,
                    dims::N_NODES, NODE_THERMAL_DOFS),
      make_nc_table("nodal data",
                    &RestartUnzippedData<D, K, NOI, NTh>::node_impurities,
                    dims::N_NODES, NODE_IC),
  };
};

/**
 * @brief C++ representation of the content (arrays only) of a NetCdf mesh file.
 */
template <std::size_t D, std::size_t K> struct MeshUnzippedData {
  std::size_t n_cells{};
  std::size_t n_nodes{};
  std::vector<unsigned int> cell_ids{};
  std::vector<double> cell_data{};
  std::vector<unsigned int> node_integers{};
  std::vector<double> node_coordinates{};
};

/**
 * @brief NetCdf schema for mesh data (arrays only).
 */
template <std::size_t D, std::size_t K>
struct traits::NetCdfSchema<MeshUnzippedData<D, K>> {
  constexpr static DimensionDef CELL_LATTICE_VECTORS =
      DimensionDef{"mesh lattice vectors", D *D};
  constexpr static DimensionDef CELL_IDS =
      DimensionDef{"cell id and node ids", K + 2};
  constexpr static DimensionDef NODE_INTEGERS =
      DimensionDef{"node int data", 3};
  constexpr static DimensionDef NODE_COORDS = DimensionDef{"node coords", D};
  constexpr static std::array DIMENSIONS = std::array{
      CELL_LATTICE_VECTORS,
      CELL_IDS,
      NODE_INTEGERS,
      NODE_COORDS,
  };
  constexpr static std::array INT_TABLES = std::array{
      make_nc_table("cell and nodes", &MeshUnzippedData<D, K>::cell_ids,
                    dims::N_CELLS, CELL_IDS),
      make_nc_table("node keys material ids site types",
                    &MeshUnzippedData<D, K>::node_integers, dims::N_NODES,
                    NODE_INTEGERS),
  };
  constexpr static std::array DOUBLE_TABLES = std::array{
      make_nc_table("coordinates", &MeshUnzippedData<D, K>::node_coordinates,
                    dims::N_NODES, NODE_COORDS),
  };
  constexpr static NC2dArray LATTICE_VECTORS =
      make_nc_table("cell lattice vectors", &MeshUnzippedData<D, K>::cell_data,
                    dims::N_CELLS, CELL_LATTICE_VECTORS);
};

/**
 * @brief Cell data when reading a mesh file.
 */
template <std::size_t Dimension> struct MeshCellData {
  std::array<std::array<double, Dimension>, Dimension> basis{};
};

/**
 * @brief Nodal data when reading a mesh file.
 */
struct MeshNodalData {
  std::size_t material_id{};
  int type{};
};

} // namespace netcdf_io
template <> struct meshing::traits::MaterialId<netcdf_io::MeshNodalData> {
  inline static std::size_t material_id(const netcdf_io::MeshNodalData &data) {
    return data.material_id;
  }
};
template <> struct meshing::traits::NodeType<netcdf_io::MeshNodalData> {
  inline static int node_type(const netcdf_io::MeshNodalData &data) {
    return data.type;
  }
};
namespace netcdf_io {

/**
 * @brief Extra data when reading mesh file.
 */
template <std::size_t D> struct MeshData {
  bool have_lattice_vectors{};
  std::array<double, D> atomistic_box_lower{};
  std::array<double, D> atomistic_box_upper{};
};

constexpr std::string_view
zone_type_name(const boundarycondition::BoundaryConditionType type) {
  switch (type) {
  case boundarycondition::BoundaryConditionType::Free:
    return "free";
  case boundarycondition::BoundaryConditionType::Slip:
    return "slip";
  case boundarycondition::BoundaryConditionType::Fixed:
    return "fixed";
  default:
    return "invalid";
  }
}

constexpr std::string_view
force_applicator_name(const boundarycondition::ForceApplicatorType type) {
  switch (type) {
  case boundarycondition::ForceApplicatorType::Null:
    return "NULL";
  case boundarycondition::ForceApplicatorType::Sphere:
    return "indenter: sphere";
  default:
    return "invalid";
  }
}

} // namespace netcdf_io
