// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load external force applicator data from a restart file written by @ref write_restart_file.
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/external_force_applicator.hpp"
#include "netcdf_io/pnetcdfpp.hpp"
#include "netcdf_io/storage_schema.hpp"
#include "netcdf_io/vec_to_array.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <boost/mpi.hpp>
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <stdexcept>
#include <unordered_set>
#include <vector>

namespace netcdf_io {

/**
 * @brief Raw data for @ref ExternalForceApplicator.
 */
template <std::size_t D> struct ExternalForceApplicatorDef {
  boundarycondition::ForceApplicatorType type =
      boundarycondition::ForceApplicatorType::Null;
  double force_constant{};
  double delta{};
  double radius{};
  std::array<double, D> center{};
};

/**
 * @brief Load external force applicator data from a restart file written by @ref write_restart_file.
 *
 * Ignores the rest of the data contained in the file.
 */
template <std::size_t D>
ExternalForceApplicatorDef<D>
load_external_force_applicator(const std::filesystem::path &path) {
  const auto ncfile = NetCdfFileHandleRO(path);
  const auto deserialize_applicator_type = [](const auto &name) {
    for (const auto applicator_type : boundarycondition::FORCE_APPLICATOR_TYPES)
      if (name == force_applicator_name(applicator_type))
        return applicator_type;
    throw std::runtime_error{"Invalid force applicator type: " + name};
  };

  const auto type = deserialize_applicator_type(
      ncfile.read_string("external force applicator"));
  const auto center = detail::vec_to_array<D>(
      ncfile.read_double_vec("force applicator center"));
  const auto radius = ncfile.read_double("force applicator radius");
  const auto force_constant =
      ncfile.read_double("force applicator force constant");
  const auto delta = ncfile.read_double("force applicator increments");

  auto external_force_applicator = ExternalForceApplicatorDef<D>{
      type, force_constant, delta, radius, center};
  return external_force_applicator;
}

} // namespace netcdf_io
