// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include <array>
#include <stdexcept>
#include <vector>

namespace netcdf_io::detail {

/**
 * @brief Convert a std::vector<double> to a std::array<double, Size>.
 *
 * @throws Exception if the size of the vector does not match Size.
 */
template <std::size_t Size>
std::array<double, Size> vec_to_array(const std::vector<double> &vec) {
  if (vec.size() != Size)
    throw std::runtime_error{"Wrong size: Expected " + std::to_string(Size) +
                             ", got " + std::to_string(vec.size())};
  auto arr = std::array<double, Size>{};
  for (std::size_t i = 0; i < arr.size(); i++)
    arr[i] = vec[i];
  return arr;
}
} // namespace netcdf_io::detail
