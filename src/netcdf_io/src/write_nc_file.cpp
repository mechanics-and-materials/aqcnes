// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Write a NetCdf restart file
 * @authors P. Gupta, S. Saxena
 */

#include "netcdf_io/write_nc_file.hpp"
#include <boost/mpi.hpp>

namespace netcdf_io::detail {

std::tuple<std::size_t, std::size_t> all_reduce_cum_sum(const std::size_t x) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  auto cummulative_size = std::size_t{0};
  auto local_sizes = std::vector<std::size_t>(mpi_size);
  boost::mpi::all_gather(world, x, local_sizes);
  for (int mpi_source = 0; mpi_source < mpi_rank; mpi_source++)
    cummulative_size += local_sizes[mpi_source];
  auto total_size = cummulative_size;
  for (int mpi_source = mpi_rank; mpi_source < mpi_size; mpi_source++)
    total_size += local_sizes[mpi_source];
  return std::make_tuple(cummulative_size, total_size);
}

} // namespace netcdf_io::detail
