// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "app/app.hpp"
#include <gmock/gmock.h>

namespace app {
namespace {

struct ParseBoolTest
    : ::testing::TestWithParam<std::tuple<const char *, bool, bool>> {};

TEST_P(ParseBoolTest, parse_bool_works) {
  const auto [input, default_value, expected_output] = GetParam();
  const auto actual_output = app::parse_bool(input, default_value);
  EXPECT_EQ(actual_output, expected_output);
}

INSTANTIATE_TEST_CASE_P(ParseBoolTestSuite, ParseBoolTest,
                        testing::Values(std::make_tuple(nullptr, true, true),
                                        std::make_tuple(nullptr, false, false),
                                        std::make_tuple("", true, true),
                                        std::make_tuple("", false, false),
                                        std::make_tuple("no", true, false),
                                        std::make_tuple("no", false, false),
                                        std::make_tuple("false", true, false),
                                        std::make_tuple("false", false, false),
                                        std::make_tuple("False", true, false),
                                        std::make_tuple("False", false, false),
                                        std::make_tuple("FALSE", true, false),
                                        std::make_tuple("FALSE", false, false),
                                        std::make_tuple("yes", true, true),
                                        std::make_tuple("yes", false, true),
                                        std::make_tuple("true", true, true),
                                        std::make_tuple("true", false, true),
                                        std::make_tuple("True", true, true),
                                        std::make_tuple("True", false, true),
                                        std::make_tuple("TRUE", true, true),
                                        std::make_tuple("TRUE", false, true),
                                        std::make_tuple("1", true, true),
                                        std::make_tuple("1", false, true)));

} // namespace
} // namespace app
