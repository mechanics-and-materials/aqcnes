// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include <filesystem>

namespace app {
/**
 * @brief Tries to access an environment variable and to parse its value to a bool with a fallback
 * value if the variable is not set
 */
bool get_bool_env(const char *env_var, bool default_value = false);
/**
 * @brief Parse a string value to a bool with a fallback
 * value if str is nullptr
 */
bool parse_bool(const char *str, bool default_value);

} // namespace app
