// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "app/app.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <string_view>

namespace app {
bool parse_bool(const char *str, bool default_value) {
  if (str == nullptr)
    return default_value;
  auto value = std::string_view{str};
  if (value.empty())
    return default_value;
  if (value == "false" || value == "FALSE" || value == "False" ||
      value == "no" || value == "0")
    return false;
  if (value == "true" || value == "TRUE" || value == "True" || value == "yes" ||
      value == "1")
    return true;
  throw std::runtime_error(std::string{"Could not parse '"} + str +
                           "' as a bool!");
}

bool get_bool_env(const char *env_var, bool default_value) {
  // NOLINTNEXTLINE(concurrency-mt-unsafe)
  return parse_bool(std::getenv(env_var), default_value);
}

} // namespace app
