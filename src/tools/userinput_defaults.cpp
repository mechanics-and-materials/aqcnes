// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Using this file, the input data for the different material types can be written out as json.
 *
 * @author S. Zimmermann
 */

#include "cli/cli.hpp"
#include "input/userinput.hpp"
#include <boost/program_options.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <string_view>

namespace {

using Entry = std::tuple<std::string_view, input::Userinput (*)()>;
constexpr std::array<Entry, 6> CONFIG_BY_MATERIAL = std::array{
    Entry{"default", []() { return input::Userinput{}; }},
    Entry{"KimFcc",
          []() {
            return input::Userinput{.material = input::KimFccProperties{}};
          }},
    Entry{"NNMaterial",
          []() {
            return input::Userinput{.material = input::NNMaterialProperties{}};
          }},
    Entry{"EFSCopper",
          []() {
            return input::Userinput{.material = input::EFSCopperProperties{}};
          }},
    Entry{
        "Meam",
        []() { return input::Userinput{.material = input::MeamProperties{}}; }},
    Entry{"SetflEAM",
          []() {
            return input::Userinput{.material = input::SetflEAMProperties{}};
          }},
};

struct CliArgs {
  std::string material = "default";
  bool output_all = false;
};

void output_all() {
  std::filesystem::create_directories("material_config");
  for (const auto &[material, make_config] : CONFIG_BY_MATERIAL) {
    std::ofstream out;
    const auto path =
        std::string{"material_config/"} + std::string{material} + ".json";
    out.open(path);
    input::save_userinput_to_json(make_config(), out);
    out.close();
  }
}

} // namespace

int main(int argc, const char **argv) {
  const auto result =
      cli::Parser<CliArgs>{}
          .add_option("material", &CliArgs::material,
                      "Input material. Choose from: " +
                          cli::collect_entries(CONFIG_BY_MATERIAL))
          .add_option("all", &CliArgs::output_all,
                      "Output samples for all available materials to a folder "
                      "material_config/")
          .parse(argc, argv);
  if (result.status != cli::Status::Success)
    return result.return_code;
  if (result.args.output_all) {
    output_all();
    return 0;
  }
  for (const auto &[material, make_config] : CONFIG_BY_MATERIAL)
    if (result.args.material == material) {
      input::save_userinput_to_json(make_config(), std::cout);
      break;
    }
  return 0;
}
