// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "cli/cli.hpp"
#include <gmock/gmock.h>
#include <sstream>

namespace cli {
namespace {

struct CliArgs {
  int count = 0;
  double number = 0.0;
  bool enable = true;
  std::string text{};
  std::optional<unsigned int> verbosity = std::nullopt;
  std::optional<bool> disable = std::nullopt;
};

struct TestCli : testing::Test {
  std::stringstream dev_null{};
};

TEST_F(TestCli, parser_detects_error) {
  auto cmd_line = std::array{"binary", "--invalid-option", "1"};
  const auto result = Parser<CliArgs>{}.parse(cmd_line.size(), cmd_line.data(),
                                              dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Error);
  EXPECT_EQ(result.return_code, 1);
}

TEST_F(TestCli, parser_parses_help) {
  auto cmd_line = std::array{"binary", "--help"};
  const auto result = Parser<CliArgs>{}.parse(cmd_line.size(), cmd_line.data(),
                                              dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Help);
  EXPECT_EQ(result.return_code, 0);
}

TEST_F(TestCli, parser_parses_int) {
  auto cmd_line = std::array{"binary", "--count", "2"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("count", &CliArgs::count, "A number")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.count, 2);
}

TEST_F(TestCli, parser_parses_double) {
  auto cmd_line = std::array{"binary", "--number", "2.5"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("number", &CliArgs::number, "A floating point number")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.number, 2.5);
}

TEST_F(TestCli, parser_parses_string) {
  auto cmd_line = std::array{"binary", "--text", "..."};
  const auto result =
      Parser<CliArgs>{}
          .add_option("text", &CliArgs::text, "Some text")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.text, "...");
}

TEST_F(TestCli, parser_parses_bool_with_switch) {
  auto cmd_line = std::array{"binary", "--enable"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("enable", &CliArgs::enable, "Set enable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_TRUE(result.args.enable);
}

TEST_F(TestCli, parser_parses_bool_without_switch) {
  auto cmd_line = std::array{"binary"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("enable", &CliArgs::enable, "Set enable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_FALSE(result.args.enable);
}

TEST_F(TestCli, parser_parses_optional_uint_with_argument) {
  auto cmd_line = std::array{"binary", "-v", "3"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("-v", &CliArgs::verbosity, "Set verbosity level")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.verbosity, 3);
}

TEST_F(TestCli, parser_parses_optional_uint_without_argument) {
  auto cmd_line = std::array{"binary"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("-v", &CliArgs::verbosity, "Set verbosity level")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.verbosity, std::nullopt);
}

TEST_F(TestCli, parser_parses_optional_bool_as_switch) {
  auto cmd_line = std::array{"binary", "--disable"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("disable", &CliArgs::disable, "Set disable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.disable, true);
}

TEST_F(TestCli, parser_parses_optional_bool_with_1) {
  auto cmd_line = std::array{"binary", "--disable=1"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("disable", &CliArgs::disable, "Set disable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.disable, true);
}

TEST_F(TestCli, parser_parses_optional_bool_with_0) {
  auto cmd_line = std::array{"binary", "--disable=0"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("disable", &CliArgs::disable, "Set disable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.disable, false);
}

TEST_F(TestCli, parser_parses_optional_bool_without_argument) {
  auto cmd_line = std::array{"binary"};
  const auto result =
      Parser<CliArgs>{}
          .add_option("disable", &CliArgs::disable, "Set disable to true")
          .parse(cmd_line.size(), cmd_line.data(), dev_null, dev_null);
  EXPECT_EQ(result.status, Status::Success);
  EXPECT_EQ(result.return_code, 0);
  EXPECT_EQ(result.args.disable, std::nullopt);
}

} // namespace
} // namespace cli
