// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <boost/program_options.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <string_view>

namespace cli {

namespace po = boost::program_options;

/**
 * @brief Creates a CLI option (program_options value) from a struct field.
 */
template <typename T> auto prm(T &dest_value) {
  return boost::program_options::value<T>()
      ->default_value(dest_value)
      ->notifier([&](const T &value) { dest_value = value; });
}
template <> inline auto prm(bool &dest_value) {
  return boost::program_options::bool_switch()->default_value(false)->notifier(
      [&](const bool &value) { dest_value = value; });
}
template <typename T> auto prm(std::optional<T> &dest_value) {
  return boost::program_options::value<T>()->notifier(
      [&](const T &value) { dest_value = value; });
}

template <> inline auto prm(std::optional<bool> &dest_value) {
  return boost::program_options::value<bool>()->implicit_value(true)->notifier(
      [&](const bool &value) { dest_value = value; });
}

namespace detail {

template <std::size_t I = 0, class B, class Args, class... Options>
auto add_options(B &&option_builder, const std::tuple<Options...> &options,
                 Args &args) {
  if constexpr (I >= sizeof...(Options))
    return option_builder;
  else {
    const auto &option = std::get<I>(options);
    return add_options<I + 1>(
        std::move(option_builder(option.name.data(),
                                 cli::prm(args.*option.dest),
                                 option.help.data())),
        options, args);
  }
}

} // namespace detail

/**
 * @brief A single option for @ref Parser.
 */
template <class Args, class T> struct Option {
  std::string_view name{};
  T Args::*dest;
  std::string_view help{};
};

/**
 * @brief Status reported by @ref Parser.
 */
enum class Status { Success, Help, Error };

/**
 * @brief Parser result returned by @ref Parser::parse.
 */
template <class Args> struct Result {
  /// Parsed arguments.
  Args args{};
  /// Parser status (Success / Error / Help).
  Status status = Status::Success;
  /// Return code in the case of Error (1) or Help (0).
  int return_code = 0;
};

/**
 * @brief Command line parser.
 *
 * Intended invocation:
 * `cli::Parser<CliArgs>{}.add_option(...)[.add_option(...) ...].parse(argc, argv)`,
 * Where `CliArgs` is a srtuct holding the parsed values and where
 * the `add_option(...)` calls register all fields of `CliArgs`.
 */
template <class Args, class... Options> struct Parser {
  std::string description = "Usage";
  std::tuple<Options...> options{};

  /**
   * @brief Register a command line option.
   *
   * Currently the main primitive types (booleans, integers, floating point numbers and strings) are supported, as well as `std::optional` thereof.
   * Options for booleans will be parsed as switches (no arguments,
   * if passed to the command line yield `true`, if ommited `false`).
   */
  template <class O>
  Parser<Args, Options..., Option<Args, O>>
  add_option(std::string_view &&name, O Args::*dest, std::string_view &&help) {
    return {std::move(this->description),
            std::tuple_cat(std::move(this->options),
                           std::make_tuple(Option<Args, O>{name, dest, help}))};
  }
  /**
   * @brief Parse the command line.
   *
   * @return A @ref Result instance with parser status and parsed arguments.
   */
  Result<Args> parse(const int argc, const char **const argv,
                     std::ostream &stdout, std::ostream &stderr) const {
    auto result = Result<Args>{};

    namespace po = boost::program_options;
    auto options_description = po::options_description(this->description);
    detail::add_options<>(options_description.add_options()("help,h", "Help"),
                          this->options, result.args);
    auto mapper = po::variables_map{};
    try {
      po::store(po::parse_command_line(argc, argv, options_description),
                mapper);
    } catch (po::error const &e) {
      result.status = Status::Error;
      result.return_code = 1;
      stderr << e.what() << "\n";
      return result;
    }
    po::notify(mapper);
    if (mapper.count("help") != 0U) {
      result.status = Status::Help;
      stdout << options_description << "\n";
    }
    return result;
  }
  Result<Args> parse(const int argc, const char **const argv) const {
    return this->parse(argc, argv, std::cout, std::cerr);
  }
};

/**
 * @brief Joins all keys of an array of (key, value) tuples with a ", " separator.
 */
template <std::size_t N, class T>
std::string
collect_entries(const std::array<std::tuple<std::string_view, T>, N> &entries) {
  auto choices = std::string{};
  for (std::size_t i = 0; i < entries.size(); i++) {
    choices += std::string{std::get<0>(entries[i])};
    if (i + 1 < entries.size())
      choices += ", ";
  }
  return choices;
}

} // namespace cli
