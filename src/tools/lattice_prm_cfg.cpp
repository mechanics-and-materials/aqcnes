// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Using this file, the lattice parameter data for the different methods of lattice generation can be written out as json.
 * @author S. Zimmermann
 */

#include "cli/cli.hpp"
#include "lattice/lattice_generators/lattice_generator_parameters.hpp"
#include <boost/program_options.hpp>
#include <filesystem>
#include <iostream>
#include <optional>
#include <string_view>

namespace {

using Entry =
    std::tuple<std::string_view, lattice::LatticeGeneratorParameters<3> (*)()>;
constexpr std::array CONFIG_BY_METHOD = std::array{
    Entry{"default", []() { return lattice::LatticeGeneratorParameters<3>{}; }},
    Entry{
        "nano-indentation",
        []() {
          return lattice::LatticeGeneratorParameters<3>{
              .n_sub_lattice = 1,
              .n_coarsening_levels = 1,
              .atomistic_domain = {{7.3, 7.3, 7.3}},
              .center = {{0.0, 0.0, 0.0}},
              .save_lattice_vectors = false,
              .lattice_parameter = 3.52,
              .lattice_structure = lattice::LatticeStructure::FCC,
              .name = "NanoIndentation",
              .coarsening_factors = {{1.0, 4.0, 16.0, 50.0, 100}},
              .domain_expansion_factors = {{15.0, 40.0, 50.0, 100.0, 500}},
              .hcp_c_by_a = 1.632,
              .make_atomistic_domain = true,
              .lattice_file_name = "./mesh_files/MyLattice.txt",
              .rotation = {{{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}},
              .seed_point_offset_coeffs = {{0.25, 0.25, 0.25}},

              .method = lattice::NanoIndentation<3>{
                  .domain_boundaries = lattice::Domain<3>{
                      .lower = {{-5000.0, -5000.0, -10000.0}},
                      .upper = {{5000.0, 5000.0, 0.1}}}}};
        }},
    Entry{
        "symmetric-grainboundaries",
        []() {
          return lattice::LatticeGeneratorParameters<3>{
              .n_sub_lattice = 1,
              .n_coarsening_levels = 1,
              .atomistic_domain = {{7.3, 7.3, 7.3}},
              .center = {{0.0, 0.0, 0.0}},
              .save_lattice_vectors = false,
              .lattice_parameter = 3.52,
              .lattice_structure = lattice::LatticeStructure::FCC,
              .name = "NanoIndentation",
              .coarsening_factors = {{1.0, 4.0}},
              .domain_expansion_factors = {{2.0, 4.0}},
              .hcp_c_by_a = 1.632,
              .make_atomistic_domain = true,
              .lattice_file_name = "./mesh_files/MyLattice.txt",
              .rotation = {{{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}},
              .seed_point_offset_coeffs = {{0.25, 0.25, 0.25}},

              .method = lattice::SymmetricGrainboundaries<3>{
                  .domain_boundaries =
                      lattice::Domain<3>{
                          .lower = {{-5000.0, -5000.0, -10000.0}},
                          .upper = {{5000.0, 5000.0, 0.1}}},
                  .overlap_deletion_distance = 1.2,
                  .delete_across_boundary = false}};
        }},
    Entry{
        "read-coordinates",
        []() {
          return lattice::LatticeGeneratorParameters<3>{
              .atomistic_domain = {{7.3, 7.3, 7.3}},
              .center = {{0.0, 0.0, 0.0}},
              .lattice_parameter = 3.52,
              .lattice_structure = lattice::LatticeStructure::FCC,
              .name = "NanoIndentation",
              .hcp_c_by_a = 1.632,
              .make_atomistic_domain = true,
              .lattice_file_name = "./mesh_files/MyLattice.txt",
              .rotation = {{{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}},
              .seed_point_offset_coeffs = {{0.25, 0.25, 0.25}},

              .method = lattice::ReadCoordinates{
                  .coordinates_file_name = "exampleCoordinatesFile.txt"}};
        }},
};

struct CliArgs {
  std::string method = "default";
  bool output_all = false;
};

void output_all() {
  std::filesystem::create_directories("lattice_generator_config");
  for (const auto &[method, make_config] : CONFIG_BY_METHOD) {
    std::ofstream out;
    const auto path = std::string{"lattice_generator_config/"} +
                      std::string{method} + ".json";
    out.open(path);
    lattice::save_lattice_parameters_to_json<3>(make_config(), out);
    out.close();
  }
}

} // namespace

int main(int argc, const char **argv) {
  const auto result =
      cli::Parser<CliArgs>{}
          .add_option("method", &CliArgs::method,
                      "Lattice generator method. Choose from: " +
                          cli::collect_entries(CONFIG_BY_METHOD))
          .add_option("all", &CliArgs::output_all,
                      "Output samples for all available methods to a folder "
                      "lattice_generator_config/")
          .parse(argc, argv);
  if (result.status != cli::Status::Success)
    return result.return_code;
  if (result.args.output_all) {
    output_all();
    return 0;
  }
  for (const auto &[method, make_config] : CONFIG_BY_METHOD)
    if (result.args.method == method) {
      lattice::save_lattice_parameters_to_json<3>(make_config(), std::cout);
      break;
    }
  return 0;
}
