// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Generic json serialization of std::variant
 * @authors S. Zimmermann, G. Bräunlich
 */

#pragma once

#include <nlohmann/json.hpp>
#include <string_view>
#include <variant>

namespace utils::json {
using Json = nlohmann::json;

template <class T> struct VariantName;

namespace detail {
// from_json
template <const std::size_t N, typename... Variants>
void from_json_by_variant_name(const Json &j, std::variant<Variants...> &obj,
                               const std::string_view &variant_name) {
  if constexpr (N == sizeof...(Variants))
    throw std::runtime_error(std::string{"Invalid variant type: "} +
                             std::string{variant_name});
  else {
    using Variant =
        typename std::tuple_element<N, std::tuple<Variants...>>::type;
    if (variant_name != VariantName<Variant>::NAME) {
      from_json_by_variant_name<N + 1, Variants...>(j, obj, variant_name);
      return;
    }
    obj = Variant{};
    std::visit([&](auto &obj_variant) { from_json(j, obj_variant); }, obj);
  }
}
} //namespace detail

template <typename... Variants>
void from_json(const Json &j, std::variant<Variants...> &obj) {
  const std::string variant_str = j.at("$type");
  detail::from_json_by_variant_name<0, Variants...>(j, obj, variant_str);
}

namespace detail {
// to_json
template <typename T> std::string_view get_name(const T &) {
  return VariantName<T>::NAME;
}

template <typename... Variants>
std::string_view get_variant_name(const std::variant<Variants...> &obj) {
  return std::visit([&](auto &obj_variant) { return get_name(obj_variant); },
                    obj);
}
} //namespace detail

template <typename... Variants>
void to_json(Json &j, const std::variant<Variants...> &obj) {
  j["$type"] = detail::get_variant_name(obj);
  std::visit([&](auto &obj_variant) { to_json(j, obj_variant); }, obj);
}

} // namespace utils::json
