// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file Backport from nlohmann_json 3.11.
 * @authors S. Zimmermann
 */

#pragma once

#ifndef NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NLOHMANN_JSON_FROM_WITH_DEFAULT(v1)                                    \
  nlohmann_json_t.v1 = nlohmann_json_j.value(#v1, nlohmann_json_default_obj.v1);
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Type, ...)             \
  inline void to_json(nlohmann::json &nlohmann_json_j,                         \
                      const Type &nlohmann_json_t) {                           \
    NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_TO, __VA_ARGS__))   \
  }                                                                            \
  inline void from_json(const nlohmann::json &nlohmann_json_j,                 \
                        Type &nlohmann_json_t) {                               \
    Type nlohmann_json_default_obj;                                            \
    NLOHMANN_JSON_EXPAND(                                                      \
        NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM_WITH_DEFAULT, __VA_ARGS__))     \
  }
#endif
