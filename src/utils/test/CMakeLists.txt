# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

find_package(AQCNES_GTest REQUIRED)

add_executable(test_utils
  test_to_string_with_zeros.cpp
  test_string_to_bool.cpp
  test_string_split.cpp
  test_string_strip.cpp
  test_evaluate_polynomial_and_derivative.cpp
)
target_link_libraries(test_utils PRIVATE utils GTest::gmock_main)
add_test(NAME test_utils
  COMMAND $<TARGET_FILE:test_utils>
)
