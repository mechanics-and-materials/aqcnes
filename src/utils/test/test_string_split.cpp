// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "exception/qc_exception.hpp"
#include "utils/string_split.hpp"
#include <gmock/gmock.h>

namespace utils {
namespace {

TEST(test_string_split, string_split_once_splits_at_multichar_separator) {
  const auto actual = utils::string_split_once("1, b", ", ");
  const auto expected =
      std::make_tuple(std::string_view{"1"}, std::string_view{"b"});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_once_splits_at_char) {
  const auto actual = utils::string_split_once("1,b", ',');
  const auto expected =
      std::make_tuple(std::string_view{"1"}, std::string_view{"b"});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_once_splits_at_chargroup) {
  const auto actual = utils::string_split_once("1;b", AnyOf{",;"});
  const auto expected =
      std::make_tuple(std::string_view{"1"}, std::string_view{"b"});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split,
     string_split_splits_whitespace_separated_string_with_whitespace_padding) {
  const auto actual =
      utils::string_split<std::string, double, double, std::string>(
          " ...\t 1.0 \t 2  ...", AnyOf{" \t"}, AnyOf{" \t"});
  const auto expected = std::make_tuple(std::string{"..."}, double{1.},
                                        double{2.}, std::string{"..."});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_splits_at_multichar_separator) {
  const auto actual =
      utils::string_split<int, std::string_view, double>("1, b, 3.0", ", ");
  const auto expected = std::make_tuple(1, std::string_view{"b"}, 3.);
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_handles_one_tuples) {
  const auto actual = utils::string_split<int>("1", ',');
  const auto expected = std::make_tuple(1);
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_splits_comma_separated_string) {
  const auto actual =
      utils::string_split<std::string, double, double, std::string>(
          " ..., 1.0, 2, ...", ',', ' ');
  const auto expected =
      std::make_tuple(std::string{"..."}, 1., 2., std::string{"..."});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_splits_space_separated_string) {
  const auto actual =
      utils::string_split<unsigned int, std::string_view>("2 Cu", ' ');
  const auto expected =
      std::make_tuple((unsigned int){2}, std::string_view{"Cu"});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split,
     string_split_splits_comma_separated_string_without_whitespace) {
  const auto actual =
      utils::string_split<unsigned int, double, unsigned int, double, double>(
          "1,2.0,3,4,5.", ',');
  const auto expected = std::make_tuple(
      (unsigned int){1}, double{2.}, (unsigned int){3}, double{4.}, double{5.});
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_detects_wrong_data) {
  EXPECT_THROW((utils::string_split<unsigned int, std::string>("..., 4", ',')),
               exception::QCException);
}
TEST(test_string_split, string_split_detects_too_many_parts) {
  EXPECT_THROW((utils::string_split<std::string, int>("1.,2,_,_", ',')),
               exception::QCException);
}
TEST(test_string_split, string_split_detects_too_few_parts) {
  EXPECT_THROW((utils::string_split<int, int, double>("1,2", ',')),
               exception::QCException);
}

TEST(test_string_split,
     string_split_many_returns_empty_vector_for_empty_string) {
  const auto actual_arr = utils::string_split_many<double>(" ", ',', ' ');
  const auto expected_arr = std::vector<double>{};
  EXPECT_EQ(actual_arr, expected_arr);
}
TEST(test_string_split, string_split_many_splits_comma_separated_string) {
  const auto actual =
      utils::string_split_many<std::string>(" ..., 1.0, 2, ...", ',', ' ');
  const auto expected = std::vector<std::string>{"...", "1.0", "2", "..."};
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split,
     string_split_many_splits_comma_separated_string_without_comma) {
  const auto s = std::string{"10000   0.1200000000000000E-02   10000  "
                             "0.5506786000000000E-03  0.5506786000000000E+01"};
  const auto actual = utils::string_split_many<std::string>(s, ',');
  const auto expected = std::vector{s};
  EXPECT_EQ(actual, expected);
}
TEST(test_string_split, string_split_many_splits_whitespace_separated_string) {
  const auto actual = utils::string_split_many<std::string_view>(
      "10000   0.1200000000000000E-02   10000  "
      "0.5506786000000000E-03  0.5506786000000000E+01",
      ' ', ' ');
  const auto expected = std::vector<std::string_view>{
      "10000", "0.1200000000000000E-02", "10000", "0.5506786000000000E-03",
      "0.5506786000000000E+01"};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_split, string_split_returns_array_for_comma_separated_string) {
  const auto actual_arr =
      utils::string_split<double, 3>("  1.0, 2.0  ,3.0 ", ',', ' ');
  const auto expected_arr = std::array<double, 3>{1., 2., 3.};
  EXPECT_EQ(actual_arr, expected_arr);
}
TEST(test_string_split, string_split_returns_array_for_space_separated_string) {
  const auto actual_arr =
      utils::string_split<double, 3>("  1.0  2.0   3.0 ", ' ', ' ');
  const auto expected_arr = std::array<double, 3>{1., 2., 3.};
  EXPECT_EQ(actual_arr, expected_arr);
}
TEST(test_string_split,
     string_split_returns_array_for_comma_separated_string_without_whitespace) {
  const auto actual_arr = utils::string_split<double, 3>("1.0,2.0,3.0", ',');
  const auto expected_arr = std::array<double, 3>{1., 2., 3.};
  EXPECT_EQ(actual_arr, expected_arr);
}

TEST(test_string_split, string_split_throws_error_for_wrong_data) {
  EXPECT_THROW((utils::string_split<double, 3>("...", ',')),
               std::runtime_error);
}
TEST(test_string_split, string_split_throws_error_for_typo) {
  EXPECT_THROW((utils::string_split<double, 3>("1.0, 2.0  3.0", ',')),
               std::runtime_error);
}

} // namespace
} // namespace utils
