// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "exception/qc_exception.hpp"
#include "utils/string_to_bool.hpp"
#include <gmock/gmock.h>
#include <stdexcept>

namespace utils {
namespace {

struct StringToBoolTest : testing::Test {};

TEST_F(StringToBoolTest, string_to_bool_returns_true_for_true) {
  const auto b = utils::string_to_bool("true");
  EXPECT_EQ(b, true);
}
TEST_F(StringToBoolTest,
       string_to_bool_returns_true_for_true_with_whitespaces) {
  const auto b = utils::string_to_bool("  true ");
  EXPECT_EQ(b, true);
}
TEST_F(StringToBoolTest, string_to_bool_returns_false_for_false) {
  const auto b = utils::string_to_bool("false");
  EXPECT_EQ(b, false);
}
TEST_F(StringToBoolTest,
       string_to_bool_returns_false_for_false_with_whitespaces) {
  const auto b = utils::string_to_bool(" false  ");
  EXPECT_EQ(b, false);
}
TEST_F(StringToBoolTest, string_to_bool_throws_error_for_arbitrary_string) {
  EXPECT_THROW(utils::string_to_bool("truefalse"), exception::QCException);
}

} // namespace
} // namespace utils
