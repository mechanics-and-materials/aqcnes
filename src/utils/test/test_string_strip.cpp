// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "exception/qc_exception.hpp"
#include "utils/string_strip.hpp"
#include <gmock/gmock.h>

namespace utils {
namespace {

TEST(test_string_strip, string_strip_removes_quotes) {
  const auto actual = utils::string_strip(std::string_view{"'...'"}, '\'');
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}
TEST(test_string_strip, string_strip_removes_mixed_quotes) {
  const auto actual =
      utils::string_strip(std::string_view{"'\"...\"'"}, AnyOf{"'\""});
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_removes_trailing_space) {
  const auto actual = utils::string_strip(std::string{"... "}, ' ');
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_removes_trailing_white_spaces) {
  const auto actual = utils::string_strip(std::string{"... \t "}, AnyOf{" \t"});
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_removes_leading_space) {
  const auto actual = utils::string_strip(std::string{" ..."}, ' ');
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_removes_leading_white_spaces) {
  const auto actual = utils::string_strip(std::string{" \t ..."}, AnyOf{" \t"});
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_leaves_string_if_nothing_to_strip) {
  const auto actual = utils::string_strip(std::string{"..."}, ' ');
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

TEST(test_string_strip, string_strip_leaves_string_without_whitespaces) {
  const auto actual = utils::string_strip(std::string{"..."}, AnyOf{"\t "});
  const auto expected = std::string_view{"..."};
  EXPECT_EQ(actual, expected);
}

} // namespace
} // namespace utils
