// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author S. Saxena
 */

#include "exception/qc_exception.hpp"
#include "utils/evaluate_polynomial.hpp"
#include <gmock/gmock.h>
#include <stdexcept>

namespace utils {
namespace {

struct EvaluatePolynomialAndDerivativeTest : testing::Test {};

TEST_F(EvaluatePolynomialAndDerivativeTest,
       value_of_a_polynomial_is_computed_correctly) {
  const auto y = utils::evaluate_polynomial({1.0, 3.0, 0.0, 2.0}, 2.0);
  EXPECT_EQ(y, 46.0);
}
TEST_F(EvaluatePolynomialAndDerivativeTest,
       derivative_of_a_polynomial_is_computed_correctly) {
  const auto y =
      utils::evaluate_polynomial_derivative({3.0, 0.0, 2.0, 5.0}, 5.0);
  EXPECT_EQ(y, 2653.0);
}

} // namespace
} // namespace utils
