// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "utils/string_split.hpp"
#include "exception/qc_exception.hpp"

namespace utils {

namespace {
constexpr std::size_t delimiter_length(const char) { return 1; }

constexpr std::size_t delimiter_length(const std::string_view &delimiter) {
  return delimiter.size();
}
constexpr std::size_t delimiter_length(const AnyOf &) { return 1; }

template <class C>
std::size_t find_delimiter(const std::string_view &s, const C &delimiter) {
  return s.find(delimiter);
}
template <>
std::size_t find_delimiter<AnyOf>(const std::string_view &s,
                                  const AnyOf &delimiter) {
  return s.find_first_of(delimiter.view);
}

namespace implementation {
template <class D>
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const D delimiter) {
  const auto pos = find_delimiter(s, delimiter);
  if (pos == std::string_view::npos)
    return {s, std::nullopt};
  return {s.substr(0, pos), s.substr(pos + delimiter_length(delimiter))};
}

} // namespace implementation
} // namespace

std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const char delimiter) {
  return implementation::string_split_once(s, delimiter);
}
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s,
                  const std::string_view &delimiter) {
  return implementation::string_split_once(s, delimiter);
}
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const AnyOf &delimiter) {
  return implementation::string_split_once(s, delimiter);
}

} // namespace utils
