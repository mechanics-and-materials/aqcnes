// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author S. Saxena
 */

#include "utils/evaluate_polynomial.hpp"
namespace utils {

double evaluate_polynomial(const std::vector<double> &polynomial_coeffs,
                           const double x) {
  auto value = 0.0;
  auto y = 1.0;
  for (const auto coef : polynomial_coeffs) {
    y *= x;
    value += coef * y;
  }
  return value;
}

double
evaluate_polynomial_derivative(const std::vector<double> &polynomial_coeffs,
                               const double x) {
  auto d_value = 0.0;
  auto y = 1.0;
  for (std::size_t ti = 0; ti < polynomial_coeffs.size(); ti++) {
    d_value += static_cast<double>(ti + 1) * polynomial_coeffs[ti] * y;
    y *= x;
  }
  return d_value;
}

} // namespace utils
