// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author P. Gupta
 */

#include "utils/timer.hpp"
#include <mpi.h>

namespace utils {

void Timer::tic() { this->initial_time = MPI_Wtime(); }
double Timer::toc() const {
  const auto final_time = MPI_Wtime();
  return final_time - this->initial_time;
}

} // namespace utils
