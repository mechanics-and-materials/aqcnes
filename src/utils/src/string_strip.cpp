// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "utils/string_strip.hpp"

namespace utils {

namespace implementation {

namespace {
template <class T>
std::string_view string_strip(const std::string_view &s, const T c) {
  auto view = s;
  view.remove_prefix(std::min(view.find_first_not_of(c), view.size()));
  view.remove_suffix(view.size() -
                     std::min(view.find_last_not_of(c) + 1, view.size()));
  return view;
}

template <class T>
std::string_view string_strip(const std::string &s, const T c) {
  return string_strip(std::string_view{s}, c);
}

} // namespace
} // namespace implementation

std::string_view string_strip(const std::string_view &s, const AnyOf &chars) {
  return implementation::string_strip(s, chars.view);
}
std::string_view string_strip(const std::string_view &s, const char c) {
  return implementation::string_strip(s, c);
}
std::string_view string_strip(const std::string &s, const AnyOf &chars) {
  return implementation::string_strip(s, chars.view);
}
std::string_view string_strip(const std::string &s, const char c) {
  return implementation::string_strip(s, c);
}

} // namespace utils
