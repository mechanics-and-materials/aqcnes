// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author P. Gupta
 */

#include "utils/to_string_with_zeros.hpp"
#include <iomanip>
#include <sstream>

namespace utils {
std::string to_string_with_zeros(unsigned int i, unsigned int total_length) {
  auto buffer = std::ostringstream{};
  buffer << std::setfill('0') << std::setw(static_cast<int>(total_length)) << i;
  return buffer.str();
}
} // namespace utils
