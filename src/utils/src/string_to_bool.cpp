// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @authors P. Gupta
 */

#include "utils/string_to_bool.hpp"
#include "exception/qc_exception.hpp"
#include "utils/string_strip.hpp"
#include <iostream>
#include <string>

namespace utils {
bool string_to_bool(const std::string &s) {
  constexpr auto TRUE_REPR = "true";
  constexpr auto FALSE_REPR = "false";

  const auto trimmed = string_strip(s, ' ');
  if (trimmed == TRUE_REPR)
    return true;
  if (trimmed == FALSE_REPR)
    return false;
  throw exception::QCException{
      std::string{"The string '"} + s + "' is not '" + TRUE_REPR + "' or '" +
      FALSE_REPR +
      "' and can therefore not be interpreted as a boolean value."};
}

} // namespace utils
