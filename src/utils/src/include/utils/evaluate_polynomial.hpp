// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author S. Saxena
 */

#pragma once
#include <vector>
namespace utils {
/**
 * @brief Computes the value or derivative of a polynomial given the polynomial coefficients in a vector.
 * It is assumed that the constant coefficient of the polynomial is zero.
 * The vector should contain the linear and higher order coefficients in an increasing order.
 */
double evaluate_polynomial(const std::vector<double> &polynomial_coeffs,
                           const double x);

double
evaluate_polynomial_derivative(const std::vector<double> &polynomial_coeffs,
                               const double x);
} // namespace utils