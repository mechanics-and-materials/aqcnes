// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "exception/qc_exception.hpp"
#include "utils/string_strip.hpp"
#include <charconv>
#include <optional>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

namespace utils {

/**
 * @brief Split a string at a given delimiter.
 *
 * If the string does not contain the delimiter, the 2nd value of the tuple will be `std::nullopt`.
 */
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const char delimiter);

/**
 * @brief Split a string at a given delimiter.
 *
 * If the string does not contain the delimiter, the 2nd value of the tuple will be `std::nullopt`.
 */
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const std::string_view &delimiter);

/**
 * @brief Split a string at a given delimiter.
 *
 * If the string does not contain the delimiter, the 2nd value of the tuple will be `std::nullopt`.
 */
std::tuple<std::string_view, std::optional<std::string_view>>
string_split_once(const std::string_view &s, const AnyOf &delimiter);

template <class T> T parse_str(const std::string_view &line) {
  auto value = T{};
  const auto [ptr, ec] =
      std::from_chars(line.data(), line.data() + line.size(), value);
  if (ec == std::errc::invalid_argument || ptr != line.end())
    throw exception::QCException{"Invalid value: \"" + std::string{line} + '"'};
  return value;
}
template <> inline std::string_view parse_str(const std::string_view &line) {
  return line;
}
template <> inline std::string parse_str(const std::string_view &line) {
  return std::string{line};
}

/**
 * @brief Parses a string containing values separated by @param separator into an exsiting vector.
 */
template <class T, class D, class S>
void string_split_many_to_vec(const std::string_view &s, std::vector<T> &parts,
                              const D &delimiter, const S &padding) {
  auto substr = string_strip(s, padding);
  if (substr.empty())
    return;
  while (true) {
    const auto [head, tail] = string_split_once(substr, delimiter);
    parts.emplace_back(parse_str<T>(string_strip(head, padding)));
    if (!tail.has_value())
      break;
    substr = string_strip(tail.value(), padding);
  }
}

/**
 * @brief Parses a string containing values separated by @param separator into a vector of given type.
 */
template <class T, class D, class S>
std::vector<T> string_split_many(const std::string_view &s, const D &delimiter,
                                 const S &padding) {
  auto parts = std::vector<T>{};
  string_split_many_to_vec(s, parts, delimiter, padding);
  return parts;
}

/**
 * @brief Parses a string containing values separated by @param separator into a vector of given type.
 */
template <class T, class D>
std::vector<T> string_split_many(const std::string_view &s,
                                 const D &delimiter) {
  return string_split_many<T>(s, delimiter, NoStrip{});
}

/**
 * @brief Parses a string containing values separated by @param separator into a tuple of given types.
 *
 * @exception exception::QCException if the string contains an incorrect number of values or incorrect types.
 */
template <class T0, typename... T, class D, class S>
std::tuple<T0, T...> string_split(const std::string_view &s, const D &delimiter,
                                  const S &padding) {
  if constexpr (sizeof...(T) == 0) {
    return std::tuple<T0>{parse_str<T0>(string_strip(s, padding))};
  } else {
    const auto &[head, tail] =
        string_split_once(string_strip(s, padding), delimiter);
    if (!tail.has_value())
      throw exception::QCException{"Too few parts: " + std::string{s}};
    return std::tuple_cat(
        std::tuple<T0>{string_split<T0>(head, delimiter, padding)},
        string_split<T...>(tail.value(), delimiter, padding));
  }
}
template <class... T, class D>
std::tuple<T...> string_split(const std::string_view &s, const D delimiter) {
  return string_split<T...>(s, delimiter, NoStrip{});
}

/**
 * @brief Parses a string containing values separated by @param separator into an array of given type.
 *
 * @exception exception::QCException if the string contains an incorrect number of values or incorrect types.
 */
template <class T, std::size_t N, class D, class S>
std::array<T, N> string_split(const std::string_view &s, const D &delimiter,
                              const S &padding) {
  auto parts = std::array<T, N>{};
  auto tail = std::optional{string_strip(s, padding)};
  for (auto &part : parts) {
    if (!tail.has_value())
      throw exception::QCException{"Too few parts: " + std::string{s}};
    const auto &[head, new_tail] =
        string_split_once(string_strip(tail.value(), padding), delimiter);
    part = parse_str<T>(string_strip(head, padding));
    tail = new_tail;
  }
  return parts;
}

/**
 * @brief Parses a string containing values separated by @param separator into an array of given type.
 *
 * @exception exception::QCException if the string contains an incorrect number of values or incorrect types.
 */
template <class T, std::size_t N, class D>
std::array<T, N> string_split(const std::string_view &s, const D &delimiter) {
  return string_split<T, N>(s, delimiter, NoStrip{});
}

} // namespace utils
