// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <string_view>

namespace utils {

/**
 * @brief Helper struct to indicate to @ref string_strip, that nothing
 * should be stripped.
 *
 * This is useful when used via @ref str_split.
 */
struct NoStrip {};

/**
 * @brief Helper struct for @red string_strip, @ref str_split_once, @str_split to split at or strip any of the chars in `view`.
 */
struct AnyOf {
  std::string_view view{};
};

/**
 * @brief Returns a view into a string not containing leading and trailing occurrences of any char in `chars`.
 */
std::string_view string_strip(const std::string_view &s, const AnyOf &chars);
/**
 * @brief Returns a view into a string not containing leading and trailing occurrences of any char in `chars`.
 */
inline std::string_view string_strip(const std::string_view &s, const NoStrip) {
  return s;
};

/**
 * @brief Returns a view into a string not containing leading and trailing occurrences of `c`.
 */
std::string_view string_strip(const std::string_view &s, const char c);
/**
 * @brief Returns a view into a string not containing leading and trailing occurrences of any char in `chars`.
 */
std::string_view string_strip(const std::string &s, const AnyOf &chars);
/**
 * @brief Returns a view into a string not containing leading and trailing occurrences of `c`.
 */
std::string_view string_strip(const std::string &s, const char c);

} // namespace utils
