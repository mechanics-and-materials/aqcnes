// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <cstddef>
#include <stdexcept>

namespace utils {

/**
 * @brief Partial "backport" of C++20 `std::span<T, std::dynamic_extent>`.
 */
template <class T> class DynamicSpan {
  const T *p_storage{};
  std::size_t p_size{};

public:
  [[nodiscard]] std::size_t size() const { return this->p_size; }
  [[nodiscard]] const T *begin() const { return this->p_storage; };
  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  [[nodiscard]] const T *end() const { return &this->p_storage[this->p_size]; };
  template <class Container>

  constexpr explicit DynamicSpan(const Container &container) noexcept
      : p_storage{container.data()}, p_size{container.size()} {}
  constexpr DynamicSpan(const T *data, const std::size_t size) noexcept
      : p_storage{data}, p_size{size} {}
  const T &operator[](const std::size_t index) const {
    if (index >= this->p_size)
      throw std::out_of_range{"Index out of range: " + std::to_string(index) +
                              " >= " + std::to_string(this->p_size)};
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return this->p_storage[index];
  }
};

} // namespace utils
