// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "input/userinput.hpp"
#include <gmock/gmock.h>
#include <sstream>
#include <stdexcept>

namespace input {

bool operator==(const SetflEAMProperties &a, const SetflEAMProperties &b) {
  return a.potential_file_path == b.potential_file_path &&
         a.potential_material_name == b.potential_material_name &&
         a.funcfl_file == b.funcfl_file;
}
bool operator==(const KimFccProperties &, const KimFccProperties &) {
  throw std::runtime_error{"Not implemented"};
}
bool operator==(const NNMaterialProperties &, const NNMaterialProperties &) {
  throw std::runtime_error{"Not implemented"};
}
bool operator==(const EFSCopperProperties &, const EFSCopperProperties &) {
  throw std::runtime_error{"Not implemented"};
}
bool operator==(const MeamProperties &, const MeamProperties &) {
  throw std::runtime_error{"Not implemented"};
}
bool operator==(const SilicaYukawaProperties &,
                const SilicaYukawaProperties &) {
  throw std::runtime_error{"Not implemented"};
}

namespace {

SetflEAMProperties create_material_properties() {
  return SetflEAMProperties{
      .potential_file_path = "/home/chucknorris/potential.txt",
      .potential_material_name = "Chuck Norris' Potential",
      .funcfl_file = true,
  };
}

TEST(test_userinput, load_after_save_is_identity) {
  const auto material_properties = create_material_properties();
  const auto input =
      Userinput{.mesh_file = ".",
                .neighbour_update_style = NeighbourUpdateStyle::VERLET,
                .material = material_properties};
  auto stream = std::stringstream{};
  save_userinput_to_json(input, stream);
  const auto reloaded_input = load_userinput_from_json(stream);
  // Check some selected fields:
  EXPECT_EQ(reloaded_input.mesh_file, input.mesh_file);
  EXPECT_EQ(reloaded_input.neighbour_update_style,
            input.neighbour_update_style);

  EXPECT_EQ(reloaded_input.material, MaterialProperties{material_properties});

  EXPECT_EQ(reloaded_input.external_displacement_gradient,
            input.external_displacement_gradient);
}

TEST(test_userinput, enum_is_serialized_correctly) {
  const auto input =
      Userinput{.neighbour_update_style = NeighbourUpdateStyle::VERLET};
  auto stream = std::stringstream{};
  save_userinput_to_json(input, stream);
  const auto content = stream.str();
  EXPECT_TRUE(content.find("\"VERLET\"") != std::string::npos);
}

} // namespace
} // namespace input
