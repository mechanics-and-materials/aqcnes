// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief JSON serialization for user input parameters
 * @authors S. Zimmermann, S. Saxena, P. Gupta
 */

#include "exception/qc_exception.hpp"
#include "input/userinput.hpp"
#include "utils/dynamic_span.hpp"
#include "utils/json/nlohmann_define_type_non_intrusive_with_default.hpp"
#include "utils/json/optional.hpp"
#include "utils/json/variant_json_serialization.hpp"
#include <array>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <string_view>
#include <unordered_set>

// std::stringview <-> Variant type mapping
template <> struct utils::json::VariantName<input::KimFccProperties> {
  static constexpr std::string_view NAME = std::string_view{"KimFcc"};
};

template <> struct utils::json::VariantName<input::NNMaterialProperties> {
  static constexpr std::string_view NAME = std::string_view{"NNMaterial"};
};

template <> struct utils::json::VariantName<input::EFSCopperProperties> {
  static constexpr std::string_view NAME = std::string_view{"EFSCopper"};
};

template <> struct utils::json::VariantName<input::SilicaYukawaProperties> {
  static constexpr std::string_view NAME = std::string_view{"SilicaYukawa"};
};

template <> struct utils::json::VariantName<input::MeamProperties> {
  static constexpr std::string_view NAME = std::string_view{"Meam"};
};

template <> struct utils::json::VariantName<input::SetflEAMProperties> {
  static constexpr std::string_view NAME = std::string_view{"SetflEAM"};
};

namespace input {

using Json = nlohmann::json;
// NOLINTNEXTLINE(misc-unused-using-decls)
using utils::json::from_json;
// NOLINTNEXTLINE(misc-unused-using-decls)
using utils::json::to_json;

NLOHMANN_JSON_SERIALIZE_ENUM(NeighbourUpdateStyle,
                             {
                                 {NeighbourUpdateStyle::VERLET, "VERLET"},
                                 {NeighbourUpdateStyle::ADAPTIVE, "ADAPTIVE"},
                             })

NLOHMANN_JSON_SERIALIZE_ENUM(
    ConcentrationUpdateStyle,
    {
        {ConcentrationUpdateStyle::onsager, "onsager"},
        {ConcentrationUpdateStyle::empirical_master_equation,
         "empirical_master_equation"},
        {ConcentrationUpdateStyle::NEB_master_equation, "NEB_master_equation"},
    })

NLOHMANN_JSON_SERIALIZE_ENUM(
    ConcentrationUpdatePairDetectionProtocol,
    {
        {ConcentrationUpdatePairDetectionProtocol::concentration,
         "concentration"},
        {ConcentrationUpdatePairDetectionProtocol::chemical_potential,
         "chemical_potential"},
    })
} // namespace input

namespace meshing {
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(PeriodicityParameters,
                                                min_coordinate, length,
                                                relaxation_constraint)
} // namespace meshing

namespace boundarycondition {
NLOHMANN_JSON_SERIALIZE_ENUM(BoundaryConditionType,
                             {{BoundaryConditionType::Free, "free"},
                              {BoundaryConditionType::Fixed, "fixed"},
                              {BoundaryConditionType::Slip, "slip"}})

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(TwoSidedBoundaryConditions,
                                                lower, upper)
} // namespace boundarycondition

namespace input {
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(
    ConcentrationUpdatePairDetection, protocol, tolerance)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(KimFccProperties, atomic_mass,
                                                model_name, species)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(NNMaterialProperties,
                                                model_path,
                                                standardization_file_path,
                                                atomic_mass,
                                                potential_material_name)

void from_json(const Json &, EFSCopperProperties &) {}
void to_json(Json &, const EFSCopperProperties &) {}

void from_json(const Json &, SilicaYukawaProperties &) {}
void to_json(Json &, const SilicaYukawaProperties &) {}

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(SetflEAMProperties,
                                                potential_file_path,
                                                potential_material_name,
                                                funcfl_file)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(MeamProperties,
                                                library_file_path,
                                                parameter_file_path,
                                                potential_material_name,
                                                species)

namespace {

struct InputField {
  std::string_view name;
  void (*from_json)(const Json &, input::Userinput &);
  void (*to_json)(Json &, const input::Userinput &);
  bool required = true;
};

struct InputSection {
  std::string_view name;
  utils::DynamicSpan<InputField> fields;
  bool required;
};

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define FIELD(NAME, JSON_NAME)                                                 \
  InputField {                                                                 \
    .name = (JSON_NAME),                                                       \
    .from_json = [](const Json &j,                                             \
                    input::Userinput &input) { j.get_to(input.NAME); },        \
    .to_json = [](Json &j, const input::Userinput &input) { j = input.NAME; }  \
  }
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define OPTIONAL_FIELD(NAME, JSON_NAME)                                        \
  InputField {                                                                 \
    .name = (JSON_NAME),                                                       \
    .from_json = [](const Json &j,                                             \
                    input::Userinput &input) { j.get_to(input.NAME); },        \
    .to_json = [](Json &j, const input::Userinput &input) { j = input.NAME; }, \
    .required = false                                                          \
  }

constexpr std::array FIELDS_SOLVER_TOLERANCES = std::array{
    FIELD(max_force_per_atom, "max_force_per_atom"),
    FIELD(max_thermal_force_per_atom, "max_thermal_force_per_atom"),
    FIELD(max_periodic_force_residue_per_atom,
          "max_periodic_force_residue_per_atom"),
};
constexpr std::array FIELDS_SIMULATION_SETTINGS = std::array{
    FIELD(mesh_file, "mesh_file"),
    FIELD(lattice_file, "lattice_file"),
    FIELD(restart_file_name, "restart_file_name"),
    FIELD(output_initial_partition, "output_initial_partition"),
    FIELD(node_effective_radius, "node_effective_radius"),
    FIELD(minimum_barycenter_weight, "minimum_barycenter_weight"),
    FIELD(neighbour_update_displacement, "neighbour_update_displacement"),
    FIELD(mesh_overlap_buffer_radius, "mesh_overlap_buffer_radius"),
    FIELD(verlet_buffer_factor, "verlet_buffer_factor"),
    FIELD(fixed_box, "fixed_box"),
    FIELD(constrained_direction, "constrained_direction"),
    FIELD(output_centro_symmetry, "output_centro_symmetry"),
    FIELD(neighbour_update_style, "neighbour_update_style"),
    FIELD(remove_previous_neighbours, "remove_previous_neighbours"),
    FIELD(quad_type, "quad_type"),
};
constexpr std::array FIELDS_THERMAL_SETTINGS = std::array{
    FIELD(T_ref, "T_ref"),
    FIELD(T_min, "T_min"),
    FIELD(T_max, "T_max"),
    FIELD(Phi_ref, "Phi_ref"),
    FIELD(isentropic, "isentropic"),
    OPTIONAL_FIELD(onsager_coefficient, "onsager_coefficient"),
    OPTIONAL_FIELD(heat_rate_tolerance_per_atom,
                   "heat_rate_tolerance_per_atom"),
};
constexpr std::array FIELDS_NEB_SETTINGS = std::array{
    OPTIONAL_FIELD(number_of_replicas, "number_of_replicas"),
    OPTIONAL_FIELD(parallel_band_stiffness, "parallel_band_stiffness"),
    OPTIONAL_FIELD(perpendicular_band_stiffness,
                   "perpendicular_band_stiffness"),
    OPTIONAL_FIELD(neb_cutoff, "NEB_cutoff"),
    OPTIONAL_FIELD(reference_energy_barrier, "reference_energy_barrier"),
    OPTIONAL_FIELD(do_thermal_relaxation, "do_thermal_relaxation"),
    OPTIONAL_FIELD(fix_initial_final, "fix_initial_final"),
    OPTIONAL_FIELD(turn_on_nudging, "turn_on_nudging"),
    OPTIONAL_FIELD(neb_verbose, "NEB_verbose"),
};
constexpr std::array FIELDS_POTENTIALS = std::array{
    FIELD(material, "material"),
};
constexpr std::array FIELDS_MULTISPECIES = std::array{
    OPTIONAL_FIELD(impurity_atomic_masses, "impurity_atomic_masses"),
    OPTIONAL_FIELD(initial_impurity_concentrations,
                   "initial_impurity_concentrations"),
    OPTIONAL_FIELD(concentration_rate_coefficients,
                   "concentration_rate_coefficients"),
    OPTIONAL_FIELD(concentration_update_cutoff_radius,
                   "concentration_update_cutoff_radius"),
    OPTIONAL_FIELD(compute_concentration_forces,
                   "compute_concentration_forces"),
    OPTIONAL_FIELD(update_concentrations_via_gamma,
                   "update_concentrations_via_gamma"),
    OPTIONAL_FIELD(concentration_update_style, "concentration_update_style"),
    OPTIONAL_FIELD(concentration_update_pair_detection,
                   "concentration_update_pair_detection"),
    OPTIONAL_FIELD(embedding_interpolation_coeffs,
                   "embedding_interpolation_coeffs"),
};
constexpr std::array FIELDS_EXTERNAL_INDENTER = std::array{
    FIELD(intialize_force_applicator, "intialize_force_applicator"),
    FIELD(indenter_radius, "indenter_radius"),
    FIELD(force_constant, "force_constant"),
    FIELD(indenter_increment, "indenter_increment"),
    FIELD(external_iterations, "external_iterations"),
    FIELD(indenter_center, "indenter_center"),
    FIELD(initial_offset, "initial_offset"),
    FIELD(strain_increment_time_step, "strain_increment_time_step"),
    FIELD(external_displacement_gradient, "external_displacement_gradient"),
};
constexpr std::array FIELDS_FIRE_RELAXATION = std::array{
    FIELD(fire_time_step, "fire_time_step"),
    FIELD(fire_initial_alpha, "fire_initial_alpha"),
    FIELD(fire_f_alpha, "fire_f_alpha"),
    FIELD(fire_f_increase_t, "fire_f_increase_t"),
    FIELD(fire_f_decrease_t, "fire_f_decrease_t"),
    FIELD(fire_min_steps_reset, "fire_min_steps_reset"),
    FIELD(fire_steps_post_reset, "fire_steps_post_reset"),
    FIELD(fire_max_time_step, "fire_max_time_step"),
    FIELD(fire_dt_output_iter, "fire_dt_output_iter"),
    FIELD(fire_max_iter, "fire_max_iter"),
    FIELD(fire_min_residual, "fire_min_residual"),
};
constexpr std::array FIELDS_PETSC = std::array{
    FIELD(use_petsc, "use_PETSC"),
    FIELD(abs_tolerance, "abs_tolerance"),
    FIELD(rel_tolerance, "rel_tolerance"),
    FIELD(s_tolerance, "s_tolerance"),
    FIELD(max_iter, "max_iter"),
    FIELD(max_feval, "max_feval"),
};
constexpr std::array FIELDS_TIME_STEPPING = std::array{
    OPTIONAL_FIELD(safety_factor, "safety_factor"),
    OPTIONAL_FIELD(t_initial, "t_initial"),
    OPTIONAL_FIELD(t_final, "t_final"),
    OPTIONAL_FIELD(time_step, "time_step"),
    OPTIONAL_FIELD(max_time_steps, "max_time_steps"),
    OPTIONAL_FIELD(enable_vn_stable_time_stepping,
                   "enable_VN_stable_time_stepping"),
};
constexpr std::array FIELDS_BOUNDARY_CONDITIONS = std::array{
    FIELD(periodicity, "periodicity"),
    FIELD(a_ref, "a_ref"),
    FIELD(boundary_types, "boundary_types"),
};
constexpr std::array FIELDS_DATAGEN_FOR_GNN_TRAINING = std::array{
    OPTIONAL_FIELD(non_centrosymmetric_neighborhoods_frequency,
                   "non_centrosymmetric_neighborhoods_frequency"),
    OPTIONAL_FIELD(clusters_compilation_file_name,
                   "clusters_compilation_file_name"),
    OPTIONAL_FIELD(mean_pos_std_dev, "mean_pos_std_dev"),
    OPTIONAL_FIELD(phi_std_dev, "phi_std_dev"),
    OPTIONAL_FIELD(noof_samples, "noof_samples"),
    OPTIONAL_FIELD(dataset_stride, "dataset_stride"),
};
constexpr std::array FIELDS_OUTPUT_PRINTING_CHOICES = std::array{
    FIELD(energy_volume, "energy_volume"),
    FIELD(per_atom_info, "per_atom_info"),
    FIELD(simulation_box_bounds, "simulation_box_bounds"),
    FIELD(boundary_forces, "boundary_forces"),
    FIELD(potential_log, "potential_log"),
    OPTIONAL_FIELD(solution_files_frequency, "solution_files_frequency"),
    OPTIONAL_FIELD(mid_fire_solution_files_frequency,
                   "mid_fire_solution_files_frequency"),
};
constexpr std::array FIELDS_CONSOLE_PRINTING_CHOICES = std::array{
    FIELD(relaxation_steps, "relaxation_steps"),
    FIELD(nan_forces, "nan_forces"),
    FIELD(halo_mesh_exchange, "halo_mesh_exchange"),
    FIELD(regenerated_neighborhoods, "regenerated_neighborhoods"),
    FIELD(neighborhood_and_force_time, "neighborhood_and_force_time"),
};
constexpr std::array FIELDS_NEIGHBOUR_DISPLAYS = std::array{
    FIELD(display_neighbourhoods_sampling_atoms,
          "display_neighbourhoods_sampling_atoms"),
};

template <std::size_t N>
constexpr InputSection section(const std::string_view &name,
                               const std::array<InputField, N> &fields,
                               const bool required = true) {
  return InputSection{.name = name,
                      .fields = utils::DynamicSpan<InputField>(fields),
                      .required = required};
}

constexpr std::array INPUT_SECTIONS = std::array{
    section("SolverTolerances", FIELDS_SOLVER_TOLERANCES),
    section("SimulationSettings", FIELDS_SIMULATION_SETTINGS),
    section("ThermalSettings", FIELDS_THERMAL_SETTINGS),
    section("NEBSettings", FIELDS_NEB_SETTINGS, false),
    section("Potentials", FIELDS_POTENTIALS),
    section("Multispecies", FIELDS_MULTISPECIES, false),
    section("ExternalIndenter", FIELDS_EXTERNAL_INDENTER),
    section("FIRERelaxation", FIELDS_FIRE_RELAXATION),
    section("PETSC", FIELDS_PETSC),
    section("TimeStepping", FIELDS_TIME_STEPPING, false),
    section("BoundaryConditions", FIELDS_BOUNDARY_CONDITIONS),
    section("DatagenForGNNTraining", FIELDS_DATAGEN_FOR_GNN_TRAINING, false),
    section("OutputPrintingChoices", FIELDS_OUTPUT_PRINTING_CHOICES),
    section("ConsolePrintingChoices", FIELDS_CONSOLE_PRINTING_CHOICES),
    section("NeighbourDisplays", FIELDS_NEIGHBOUR_DISPLAYS, false),
};

template <typename ContainerType>
auto find_unknown_keys(const Json &j, const ContainerType &fields) {
  auto unknown_keys = std::unordered_set<std::string_view>{};
  for (const auto &[key, _] : j.items())
    unknown_keys.insert(key);
  for (const auto &field : fields)
    unknown_keys.erase(field.name);
  return unknown_keys;
}

void load_section(const Json &j, input::Userinput &input,
                  const InputSection &section) {
  for (const auto &f : section.fields) {
    if (j.contains(f.name.data()) || f.required)
      f.from_json(j.at(f.name.data()), input);
  }
  const auto unknown_keys = find_unknown_keys(j, section.fields);
  for (const auto &key : unknown_keys)
    std::cerr << "Warning: unknown field '" << section.name << "." << key
              << "' in userinput." << std::endl;
}

void validate(const input::Userinput &input) {
  if (input.constrained_direction.has_value() &&
      input.constrained_direction.value() >= 3)
    throw exception::QCException("constrained_direction must be < 3 or null");
}

void from_json(const Json &j, input::Userinput &input) {
  const auto root = j.at("QC:input::Userinput");
  for (const auto &section : INPUT_SECTIONS) {
    if (section.required || root.contains(section.name.data()))
      load_section(root.at(section.name.data()), input, section);
  }
  validate(input);
  const auto unknown_keys = find_unknown_keys(root, INPUT_SECTIONS);
  for (const auto &key : unknown_keys)
    std::cerr << "Warning: unknown section '" << key << "' in userinput."
              << std::endl;
}

Json dump_section(const input::Userinput &input,
                  const utils::DynamicSpan<InputField> &fields) {
  Json j;
  for (const auto &field : fields) {
    Json obj;
    field.to_json(obj, input);
    j[field.name.data()] = obj;
  }
  return j;
}

constexpr std::string_view SCHEMA_VERSION = "0.1.1";
constexpr std::string_view SCHEMA_URL =
    "https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/raw/master/data/"
    "input/userinput.schema.json";

void to_json(Json &j, const input::Userinput &input) {
  auto root = Json{};
  for (const auto &section : INPUT_SECTIONS)
    root[section.name.data()] = dump_section(input, section.fields);
  j = Json{{"QC:input::Userinput", root},
           {"$version", SCHEMA_VERSION},
           {"$schema", SCHEMA_URL}};
}

} // namespace

Userinput load_userinput_from_json(std::istream &stream) {
  input::Userinput input;
  from_json(Json::parse(stream), input);
  return input;
}

Userinput load_userinput_from_json(const std::filesystem::path &path) {
  std::ifstream f(path);
  f.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  return load_userinput_from_json(f);
}

void save_userinput_to_json(const input::Userinput &input,
                            std::ostream &stream) {
  Json j;
  to_json(j, input);
  stream << j.dump(2);
}

} // namespace input
