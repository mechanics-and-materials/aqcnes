// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct for holding parameters in the user input
 * @authors P. Gupta, S. Saxena, S. Zimmerman
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "meshing/periodicity_parameters.hpp"
#include <array>
#include <filesystem>
#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace input {

enum struct NeighbourUpdateStyle { VERLET, ADAPTIVE };
enum struct ConcentrationUpdateStyle {
  onsager,
  empirical_master_equation,
  NEB_master_equation
};
enum struct ConcentrationUpdatePairDetectionProtocol {
  concentration,
  chemical_potential
};

struct ConcentrationUpdatePairDetection {
  ConcentrationUpdatePairDetectionProtocol protocol =
      ConcentrationUpdatePairDetectionProtocol::concentration;
  double tolerance = 1e-3;
};

struct KimFccProperties {
  double atomic_mass = 63.546;
  std::string model_name{};
  std::vector<std::string> species = {};
};

/**
 * @brief Torch neuronal network config.
 */
struct NNMaterialProperties {
  std::string model_path{};
  std::string standardization_file_path{};
  double atomic_mass = 63.546;
  std::string potential_material_name{};
};

struct EFSCopperProperties {};
struct SilicaYukawaProperties {};

struct MeamProperties {
  std::filesystem::path library_file_path = "";
  std::filesystem::path parameter_file_path = "";
  std::string potential_material_name{};
  std::vector<std::string> species = {};
};

struct SetflEAMProperties {
  std::filesystem::path potential_file_path = "";
  std::string potential_material_name{};
  bool funcfl_file = false;
};

using MaterialProperties =
    std::variant<KimFccProperties, NNMaterialProperties, EFSCopperProperties,
                 SilicaYukawaProperties, MeamProperties, SetflEAMProperties>;

// NOLINTBEGIN(clang-analyzer-optin.performance.Padding)

/**
 * @brief Main configuration for workflows.
 */
struct Userinput {
  /** @defgroup Solver Tolerances
   *
   *  @{
   */
  double max_force_per_atom = 5.0e-5;
  double max_thermal_force_per_atom = 5.0e-5;
  double max_periodic_force_residue_per_atom = 1.0e-6;
  /** @} */

  /** @defgroup Simulation Settings
   *
   *  @{
   */
  std::filesystem::path mesh_file = "";
  std::filesystem::path lattice_file = "";
  std::optional<std::filesystem::path> restart_file_name = std::nullopt;
  bool output_initial_partition = false;
  bool output_centro_symmetry = false;
  double node_effective_radius = 2.5;
  double minimum_barycenter_weight = 1.0;
  double verlet_buffer_factor = 1.75;
  double neighbour_update_displacement = 0.05;
  double mesh_overlap_buffer_radius = 1.5;
  std::optional<std::array<std::array<double, 3>, 2>> fixed_box = std::nullopt;
  std::optional<unsigned int> constrained_direction = std::nullopt;
  NeighbourUpdateStyle neighbour_update_style = NeighbourUpdateStyle::ADAPTIVE;
  bool remove_previous_neighbours = true;
  unsigned int quad_type = 3;
  /** @} */

  /** @defgroup Thermal Settings
   *
   *  @{
   */
  // NOLINTBEGIN(readability-identifier-naming)
  double T_ref = 300.0;
  double T_min = 0.01;
  double T_max = 5000.0;
  double Phi_ref = 5.545;
  // NOLINTEND(readability-identifier-naming)
  bool isentropic = false;
  double onsager_coefficient = 15.926734;
  double heat_rate_tolerance_per_atom = 1.0e-3;
  /** @} */

  /** @defgroup NEB Settings
   *
   *  @{
   */
  std::size_t number_of_replicas = 9;
  double neb_cutoff = 0.01;
  double parallel_band_stiffness = 1.0;
  double perpendicular_band_stiffness = 0.0;
  double reference_energy_barrier = 0.6;
  bool do_thermal_relaxation = true;
  bool fix_initial_final = false;
  bool turn_on_nudging = true;
  bool neb_verbose = false;
  /** @} */

  /** @defgroup Potentials
   *
   *  @{
   */

  MaterialProperties material = EFSCopperProperties{};

  /** @} */

  /** @defgroup Impurities
   *
   *  @{
   */
  std::vector<double> impurity_atomic_masses = {0.};
  std::vector<double> initial_impurity_concentrations = {0.};
  std::vector<double> concentration_rate_coefficients = {0.};
  double concentration_update_cutoff_radius = 0.01;
  bool compute_concentration_forces = false;
  bool update_concentrations_via_gamma = false;
  ConcentrationUpdateStyle concentration_update_style =
      ConcentrationUpdateStyle::onsager;
  ConcentrationUpdatePairDetection concentration_update_pair_detection{};
  std::vector<std::vector<double>> embedding_interpolation_coeffs = {{1.0}};
  /** @} */

  /** @defgroup External Indenter
   *
   *  @{
   */
  bool intialize_force_applicator = true;
  double indenter_radius = 25.0;
  double force_constant = 1000.0;
  double indenter_increment = 0.0001;
  double strain_increment_time_step = 0.01;
  std::array<double, 3> indenter_center = {{0.0, 0.0, 0.0}};
  double initial_offset = -0.3;
  std::array<std::array<double, 3>, 3> external_displacement_gradient = {
      {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}};
  std::size_t external_iterations = 40;
  /** @} */

  /** @defgroup Fire parameters
   *
   *  @{
   */
  double fire_time_step = 5.0;
  double fire_initial_alpha = 0.1;
  double fire_f_alpha = 0.99;
  double fire_f_increase_t = 1.01;
  double fire_f_decrease_t = 0.5;
  std::size_t fire_min_steps_reset = 5;
  std::size_t fire_steps_post_reset = 0;
  double fire_max_time_step = 11;
  std::size_t fire_dt_output_iter = 9999;
  std::size_t fire_max_iter = 1000;
  double fire_min_residual = 0.001;
  /** @} */

  /** @defgroup PETSC Parameters
   *
   *  @{
   */
  bool use_petsc = false;
  double abs_tolerance = 1.0e-4;
  double rel_tolerance = 1.0e-4;
  /** SNES tolerance, see: https://petsc.org/release/docs/manualpages/SNES/SNESSetTolerances.html */
  double s_tolerance = 1.0e-4;
  unsigned int max_iter = 200;
  unsigned int max_feval = 200;
  /** @} */

  /** @defgroup Time Stepping
   *
   *  @{
   */
  unsigned int time_iter = 0;
  double safety_factor = 0.75;
  double t_initial = 0.0;
  double t_final = 1.0;
  double t = 0.0;
  double time_step = 1e-7;
  unsigned int max_time_steps = 9999;
  bool enable_vn_stable_time_stepping = false;
  /** @} */

  /** @defgroup Boundary Conditions
   *
   *  @{
   */
  std::array<std::optional<meshing::PeriodicityParameters>, 3> periodicity = {
      {std::nullopt, std::nullopt, std::nullopt}};
  double a_ref = 3.61;
  std::array<boundarycondition::TwoSidedBoundaryConditions, 3> boundary_types =
      {{}};
  /** @} */

  /** @defgroup Datagen for GNN Training
   *
   *  @{
   */
  std::optional<unsigned int> non_centrosymmetric_neighborhoods_frequency =
      std::nullopt;
  std::optional<std::string> clusters_compilation_file_name = std::nullopt;
  double mean_pos_std_dev = 0.01;
  double phi_std_dev = 0.1;
  unsigned int noof_samples = 1000;
  unsigned int dataset_stride = 1;
  /** @} */

  /** @defgroup Output Printing Choices
   *
   *  @{
   */
  bool energy_volume = true;
  bool per_atom_info = true;
  bool simulation_box_bounds = true;
  bool boundary_forces = false;
  bool potential_log = false;
  std::optional<unsigned int> solution_files_frequency = std::nullopt;
  std::optional<unsigned int> mid_fire_solution_files_frequency = std::nullopt;
  /** @} */

  /** @defgroup Console Printing Choices
   *
   *  @{
   */
  bool halo_mesh_exchange = false;
  bool relaxation_steps = true;
  bool regenerated_neighborhoods = false;
  bool nan_forces = true;
  bool neighborhood_and_force_time = false;
  /** @} */

  /** @defgroup Neighbour Displays
   *
   *  @{
   */
  std::vector<std::array<double, 3>> display_neighbourhoods_sampling_atoms = {
      {}};
  /** @} */
};

// NOLINTEND(clang-analyzer-optin.performance.Padding)

/**
 * @brief Return if the multispecies feature is activated and if vacancies exist
 * Currently, there is multiple species if the user provides some impurity_atomic_masses
 * vacancies are activated if the user sets impurity_atomic_masses[0] = 0.
 */
inline bool have_multispecies(const Userinput &input) {
  return !input.impurity_atomic_masses.empty();
}
inline bool simulating_vacancies(const Userinput &input) {
  return !input.impurity_atomic_masses.empty() &&
         input.impurity_atomic_masses[0] == 0.0;
}

/**
 * @brief Load userinput.json from the current working directory.
 */
Userinput load_userinput_from_working_dir();

/**
 * @brief Load from a stream.
 */
Userinput load_userinput_from_json(std::istream &stream);
/**
 * @brief Load from a file specified by path.
 */
Userinput load_userinput_from_json(const std::filesystem::path &path);
/**
 * @brief Save to a stream.
 */
void save_userinput_to_json(const input::Userinput &input,
                            std::ostream &stream);

} // namespace input
