// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "materials/meam/meamc.hpp"
#include "materials/meam/read_library_file.hpp"
#include "testing/find_path.hpp"
#include <gmock/gmock.h>
#include <string_view>

namespace materials::meam {
namespace {

auto double_vec_eq(const std::vector<double> &reference) {
  return testing::Pointwise(testing::DoubleEq(), reference);
};
auto meam_information_eq(
    const meam::MEAMInformation &expected_meam_information) {
  const auto double_matrix_eq = [&](const auto &reference) {
    return testing::Pointwise(testing::Eq(), reference);
  };
  const auto int_matrix_eq = [&](const auto &reference) {
    return testing::Pointwise(testing::Eq(), reference);
  };
  const auto double_vec_field_eq =
      [&](const std::string_view &name,
          const auto MEAMInformation::*const field) {
        return testing::Field(name.data(), field,
                              double_vec_eq(expected_meam_information.*field));
      };
  const auto double_matrix_field_eq =
      [&](const std::string_view &name,
          const auto MEAMInformation::*const field) {
        return testing::Field(
            name.data(), field,
            double_matrix_eq(expected_meam_information.*field));
      };
  const auto double_3_tensor_field_eq =
      [&](const std::string_view &name,
          const auto MEAMInformation::*const field) {
        return testing::Field(name.data(), field,
                              testing::Eq(expected_meam_information.*field));
      };
  const auto int_matrix_field_eq =
      [&](const std::string_view &name,
          const auto MEAMInformation::*const field) {
        return testing::Field(name.data(), field,
                              int_matrix_eq(expected_meam_information.*field));
      };
  const auto double_field_eq = [&](const std::string_view &name,
                                   const auto MEAMInformation::*const field) {
    return testing::Field(name.data(), field,
                          testing::DoubleEq(expected_meam_information.*field));
  };
  return testing::AllOf(
      double_vec_field_eq("a", &MEAMInformation::a),
      double_vec_field_eq("beta0", &MEAMInformation::beta0),
      double_vec_field_eq("beta1", &MEAMInformation::beta1),
      double_vec_field_eq("beta2", &MEAMInformation::beta2),
      double_vec_field_eq("beta3", &MEAMInformation::beta3),
      double_vec_field_eq("t0", &MEAMInformation::t0),
      double_vec_field_eq("t1", &MEAMInformation::t1),
      double_vec_field_eq("t2", &MEAMInformation::t2),
      double_vec_field_eq("t3", &MEAMInformation::t3),
      double_vec_field_eq("rho0", &MEAMInformation::rho0),
      double_vec_field_eq("atomic_mass", &MEAMInformation::atomic_mass),
      double_matrix_field_eq("alpha", &MEAMInformation::alpha),
      double_matrix_field_eq("re0", &MEAMInformation::re0),
      double_matrix_field_eq("ec", &MEAMInformation::ec),
      double_matrix_field_eq("delta", &MEAMInformation::delta),
      double_matrix_field_eq("alat", &MEAMInformation::alat),
      double_3_tensor_field_eq("cmin", &MEAMInformation::cmin),
      double_3_tensor_field_eq("cmax", &MEAMInformation::cmax),
      int_matrix_field_eq("ibar", &MEAMInformation::ibar),
      int_matrix_field_eq("atomic_number", &MEAMInformation::atomic_number),
      int_matrix_field_eq("z", &MEAMInformation::z),
      int_matrix_field_eq("nn2", &MEAMInformation::nn2),
      double_matrix_field_eq("attrac", &MEAMInformation::attrac),
      double_matrix_field_eq("repuls", &MEAMInformation::repuls),
      testing::Field(
          "lattice_structure", &meam::MEAMInformation::lattice_structure,
          testing::Pointwise(testing::Eq(),
                             expected_meam_information.lattice_structure)),
      double_field_eq("rc", &meam::MEAMInformation::rc),
      double_field_eq("delr", &meam::MEAMInformation::delr));
}

TEST(test_meam, read_library_file_reads_correct_parameters) {
  /*
#
# Generated by MPC on 20170628T111741
#
#
# elt        lat     z       ielement     atwt
# alpha      b0      b1      b2           b3    alat    esub    asub
# t0         t1              t2           t3            rozero  ibar
#

'Al' 'fcc' 12 13 27.000000
4.610000 3.200000 2.600000 6.000000 2.600000 4.050000 3.360000 1.160000
1.000000 3.050000 0.510000 7.750000 1.000000 -5

'Cu' 'fcc' 12 29 63.546000
5.200000 3.830000 2.200000 6.000000 2.200000 3.610000 3.540000 0.990000
1.000000 2.720000 3.040000 0.850000 1.000000 -5
     */
  const auto library_path =
      testing::io::find_path("data/potentials_NIST/Al-Cu/library.AlCu.meam");
  const auto parameter_path =
      testing::io::find_path("data/potentials_NIST/Al-Cu/AlCu.meam");
  const auto element_names = std::vector{std::string{"Cu"}, std::string{"Al"}};
  const auto meam_information =
      read_library_file(library_path, parameter_path, element_names);
  const auto expected_meam_information = MEAMInformation{
      .a = {0.99, 1.16},
      .beta0 = {3.83, 3.2},
      .beta1 = {2.2, 2.6},
      .beta2 = {6.0, 6.0},
      .beta3 = {2.2, 2.6},
      .t0 = {1.0, 1.0},
      .t1 = {2.72, 3.05},
      .t2 = {3.04, 0.51},
      .t3 = {0.85, 7.75},
      .rho0 = {1.0, 1.0},
      .atomic_mass = {63.546, 27.0},
      .alpha = {std::vector{4.61, 4.65}, std::vector{4.65, 5.2}},
      .re0 = {std::vector{2.86, 2.53}, std::vector{2.53, 2.56}},
      .ec = {std::vector{3.36, 0.0}, std::vector{0.0, 3.54}},
      .delta = {std::vector{0.0, 0.2}, std::vector{0.2, 0.0}},
      .alat = {std::vector{3.61, 0.0}, std::vector{0.0, 4.05}},
      .cmin =
          {
              std::vector{std::vector{0.49, 0.0}, std::vector{0.5, 0.9}},
              std::vector{std::vector{0.5, 0.9}, std::vector{1.0, 1.21}},
          },
      .cmax =
          {
              std::vector{std::vector{2.8, 2.8}, std::vector{2.8, 2.8}},
              std::vector{std::vector{2.8, 2.8}, std::vector{2.8, 2.8}},
          },
      .ibar = {-5, -5},
      .atomic_number = {29, 13},
      .z = {12, 12},
      .nn2 = {std::vector{1, 1}, std::vector{1, 1}},
      .attrac = {std::vector{0.0, 0.1}, std::vector{0.1, 0.0}},
      .repuls = {std::vector{0.0, 0.1}, std::vector{0.1, 0.0}},
      .lattice_structure = {std::vector{lattice::LatticeStructure::FCC,
                                        lattice::LatticeStructure::B1},
                            std::vector{lattice::LatticeStructure::B1,
                                        lattice::LatticeStructure::FCC}},
      .rc = 5.0,
      .delr = 0.1,
  };
  EXPECT_THAT(meam_information, meam_information_eq(expected_meam_information));
}

TEST(test_meam, construct_meam_information_yields_correct_values) {
  const auto library_path =
      testing::io::find_path("data/potentials_NIST/Al-Cu/library.AlCu.meam");
  const auto parameter_path =
      testing::io::find_path("data/potentials_NIST/Al-Cu/AlCu.meam");
  const auto element_names = std::vector{std::string{"Cu"}, std::string{"Al"}};
  const auto meam = MEAMC<3, 0, 0>(library_path, parameter_path, "lattice name",
                                   element_names, true, false, {});
  const auto expected_meam_information = MEAMInformation{
      .a = {0.99, 1.16},
      .beta0 = {3.83, 3.2},
      .beta1 = {2.2, 2.6},
      .beta2 = {6.0, 6.0},
      .beta3 = {2.2, 2.6},
      .t0 = {1.0, 1.0},
      .t1 = {3.23, 7.7},
      .t2 = {3.04, 0.51},
      .t3 = {0.85, 7.75},
      .rho0 = {1.0, 1.0},
      .atomic_mass = {63.546, 27.0},
      .alpha = {std::vector{4.61, 4.65}, std::vector{4.65, 5.2}},
      .re0 = {std::vector{2.86, 2.53}, std::vector{2.53, 2.56}},
      .ec = {std::vector{3.36, 3.25}, std::vector{3.25, 3.54}},
      .delta = {std::vector{0.0, 0.2}, std::vector{0.2, 0.0}},
      .alat = {std::vector{3.61, 0.0}, std::vector{0.0, 4.05}},
      .cmin =
          {
              std::vector{std::vector{0.49, 0.0}, std::vector{0.5, 0.9}},
              std::vector{std::vector{0.5, 0.9}, std::vector{1.0, 1.21}},
          },
      .cmax =
          {
              std::vector{std::vector{2.8, 2.8}, std::vector{2.8, 2.8}},
              std::vector{std::vector{2.8, 2.8}, std::vector{2.8, 2.8}},
          },
      .ibar = {-5, -5},
      .atomic_number = {29, 13},
      .z = {12, 12},
      .nn2 = {std::vector{1, 1}, std::vector{1, 1}},
      .attrac = {std::vector{0.0, 0.1}, std::vector{0.1, 0.0}},
      .repuls = {std::vector{0.0, 0.1}, std::vector{0.1, 0.0}},
      .lattice_structure = {std::vector{lattice::LatticeStructure::FCC,
                                        lattice::LatticeStructure::B1},
                            std::vector{lattice::LatticeStructure::B1,
                                        lattice::LatticeStructure::FCC}},
      .rc = 5.0,
      .delr = 0.1,
  };
  EXPECT_THAT(meam.meam_information,
              meam_information_eq(expected_meam_information));
  EXPECT_THAT(meam.atomic_masses,
              double_vec_eq(expected_meam_information.atomic_mass));
  EXPECT_THAT(meam.neighbor_cutoff_radius,
              testing::DoubleEq(expected_meam_information.rc));
}

} // namespace
} // namespace materials::meam
