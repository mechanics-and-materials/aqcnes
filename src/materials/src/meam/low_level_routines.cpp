// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author S. Saxena
 */

#include "materials/meam/low_level_routines.hpp"
#include <array>
#include <vector>

namespace materials::meam {
double f_cut(const double x) {
  if (x > 1.0)
    return 1.0;
  if (x < 0.0)
    return 0.0;
  auto a = 1. - x;
  a *= a;
  a *= a;
  a = 1. - a;
  return a * a;
}

double df_cut(const double x) {
  if (x > 1.0)
    return 0.0;
  if (x < 0)
    return 0.0;
  auto a = 1 - x;
  a *= a;
  return 8. * a * (1. - x) * (1.0 - a * a);
}

std::size_t get_z(const lattice::LatticeStructure lattice) {
  switch (lattice) {
  case lattice::LatticeStructure::FCC:
    return 12;
  case lattice::LatticeStructure::BCC:
    return 8;
  case lattice::LatticeStructure::HCP:
    return 12;
  case lattice::LatticeStructure::B1:
    return 6;
  case lattice::LatticeStructure::B2:
    return 8;
  case lattice::LatticeStructure::L12:
    return 12;
  case lattice::LatticeStructure::Diamond:
    return 4;
  default:
    throw exception::QCException{
        "Function get_Z: lattice structure not supported."};
  }
}

std::size_t get_z2_s_a_r(const lattice::LatticeStructure lattice,
                         const double cmin, const double cmax, double &a_r,
                         double &s) {
  std::size_t scr_atoms = 0;
  std::size_t z2 = 0;
  switch (lattice) {
  case lattice::LatticeStructure::FCC:
    z2 = 6;
    a_r = std::sqrt(2.);
    scr_atoms = 4;
    break;
  case lattice::LatticeStructure::BCC:
    z2 = 6;
    a_r = 2.0 / std::sqrt(3.);
    scr_atoms = 4;
    break;
  case lattice::LatticeStructure::HCP:
    z2 = 6;
    a_r = std::sqrt(2.);
    scr_atoms = 4;
    break;
  case lattice::LatticeStructure::B1:
    z2 = 12;
    a_r = std::sqrt(2.);
    scr_atoms = 2;
    break;
  case lattice::LatticeStructure::B2:
    z2 = 6;
    a_r = 2.0 / std::sqrt(3.0);
    scr_atoms = 4;
    break;
  case lattice::LatticeStructure::L12:
    z2 = 6;
    a_r = std::sqrt(2.0);
    scr_atoms = 4;
    break;
  case lattice::LatticeStructure::Diamond:
    z2 = 12;
    a_r = std::sqrt(8.0 / 3.0);
    scr_atoms = 1;
    break;
  default:
    throw exception::QCException{
        "Function get_Z2: lattice structure not supported"};
  }
  const auto c = 4. / (a_r * a_r) - 1.;
  const auto x = (c - cmin) / (cmax - cmin);
  s = std::pow(f_cut(x), scr_atoms);
  return z2;
}

namespace {

constexpr std::array C = std::array{0.028171, 0.28022, 0.50986, 0.18175};
constexpr std::array D = std::array{0.20162, 0.40290, 0.94229, 3.1998};
// A0 = (9pi^2/128)^1/3 (0.529) Angstroms
constexpr double A0 = 0.4685;
constexpr double CC = 14.3997;

} // namespace
double zbl_potential(const double r, const int atm_n_m, const int atm_n_g) {
  const auto a = A0 / (std::pow(atm_n_m, 0.23) + std::pow(atm_n_g, 0.23));
  auto result = 0.0;
  const auto x = r / a;
  for (std::size_t i = 0; i < C.size(); i++) {
    result += C[i] * std::exp(-D[i] * x);
  }
  if (r > 0.0)
    result *= atm_n_m * atm_n_g / r * CC;
  return result;
}

std::tuple<double, double> zbl_potential_and_derivative(const double r,
                                                        const int atm_n_m,
                                                        const int atm_n_g) {
  auto dphi_zbl = 0.0;
  const auto a = A0 / (std::pow(atm_n_m, 0.23) + std::pow(atm_n_g, 0.23));
  auto result = 0.0;
  const auto x = r / a;
  for (std::size_t i = 0; i < C.size(); i++) {
    const auto temp = C[i] * std::exp(-D[i] * x);
    result += temp;
    dphi_zbl += (-D[i] / a) * temp;
  }
  if (r > 0.0) {
    result *= atm_n_m * atm_n_g / r * CC;
    dphi_zbl *= atm_n_m * atm_n_g / r * CC;
    dphi_zbl -= result / r;
  }
  return {result, dphi_zbl};
}

/**
 * @brief derivative of C_ikj wrt r_ij r_jk r_ik
 */
std::tuple<double, double, double> d_cikj(const double r_ij, const double r_ik,
                                          const double r_kj) {
  const auto rij2 = r_ij * r_ij;
  const auto rik2 = r_ik * r_ik;
  const auto rjk2 = r_kj * r_kj;
  const auto a = rik2 - rjk2;
  const auto b = rik2 + rjk2;
  const auto a2 = a * a;
  const auto rij4 = rij2 * rij2;
  const auto rik4 = rik2 * rik2;
  const auto rjk4 = rjk2 * rjk2;
  auto denom = rij4 - a * a;
  denom *= denom;
  auto d_cikj_ij = -4. * (-2. * rij2 * a2 + rij4 * b + a2 * b) / denom;
  auto d_cikj_ik =
      4. * rij2 * (rij4 + rik4 + 2. * rik2 * rjk2 - 3. * rjk4 - 2. * rij2 * a) /
      denom;
  auto d_cikj_kj =
      4. * rij2 * (rij4 - 3. * rik4 + 2. * rik2 * rjk2 + rjk4 + 2. * rij2 * a) /
      denom;
  return {d_cikj_ij, d_cikj_ik, d_cikj_kj};
}

double comp_embedding_func(const double rhob, const double a, const double ec,
                           const int emb_lin_neg) {
  if (rhob > 0.0)
    return (a * ec * rhob * log(rhob));
  if (emb_lin_neg == 0)
    return 0.0;
  return -a * ec * rhob;
}

std::tuple<double, double>
comp_embedding_func_and_derivative(const double rhob, const double a,
                                   const double ec, const int emb_lin_neg) {
  if (rhob > 0.0) {
    auto d_f = a * ec * (1.0 + log(rhob));
    return {a * ec * rhob * log(rhob), d_f};
  }
  if (emb_lin_neg == 0) {
    return {0.0, 0.0};
  }
  auto d_f = -a * ec;
  return {-a * ec * rhob, d_f};
}

namespace {
double rose_factor(const double r, const int form, const double attrac,
                   const double repuls, const double r0, const double a3) {
  switch (form) {
  case 1:
    return -attrac + repuls / r;
  case 2:
    return a3;
  case 0:
    return a3 / (r / r0);
  default:
    throw exception::QCException{"meam: Unknown form of rose function: " +
                                 std::to_string(form)};
  }
}
double d_rose_factor(const double r, const int form, const double repuls,
                     const double r0, const double a3) {
  switch (form) {
  case 1:
    return -repuls / (r * r);
  case 2:
    return 0.;
  case 0:
    return -a3 * r0 / (r * r);
  default:
    throw exception::QCException{"meam: Unknown form of rose function: " +
                                 std::to_string(form)};
  }
}
} // namespace

double calc_rose_embedding_energy(const double r, const int form,
                                  const double attrac, const double repuls,
                                  const double r0, const double alpha,
                                  const double ec) {
  if (r <= 1.0e-6)
    return 0.0;
  const auto astar = alpha * (r / r0 - 1.);
  const auto a3 = (astar > 0.) ? attrac : repuls;
  const auto f = rose_factor(r, form, attrac, repuls, r0, a3);
  return -ec * (1. + astar + f * astar * astar * astar) * exp(-astar);
}

std::tuple<double, double> calc_rose_embedding_energy_and_derivative(
    const double r, const int form, const double attrac, const double repuls,
    const double r0, const double alpha, const double ec) {
  if (r <= 1e-6)
    return {0., 0.};
  const auto astar = alpha * (r / r0 - 1.);
  const auto a3 = (astar > 0) ? attrac : repuls;
  auto f = rose_factor(r, form, attrac, repuls, r0, a3);
  auto df = d_rose_factor(r, form, repuls, r0, a3);
  const auto aastar = astar * astar;
  const auto aaastar = aastar * astar;
  const auto exp_astar = std::exp(-astar);
  const auto eu = -ec * (1. + astar + f * aaastar) * exp_astar;
  const auto d_eu =
      (alpha / r0) * (-eu - ec * (1. + 3. * f * aastar) * exp_astar) -
      ec * df * aaastar * exp_astar;
  return {eu, d_eu};
}

std::tuple<double, double> calc_g_d_g(const double gamma, const int ibar,
                                      const double gsmooth_factor) {
  switch (ibar) {
  case 0:
  case 4: {
    const auto gsmooth_switchpoint = -gsmooth_factor / (gsmooth_factor + 1.);
    if (gamma < gsmooth_switchpoint) {
      const auto dg = 0.0;
      const auto g =
          std::sqrt(1. / (gsmooth_factor + 1) *
                    std::pow((gsmooth_switchpoint / gamma), gsmooth_factor));
      return {g, dg};
    }
    const auto g = std::sqrt(1.0 + gamma);
    const auto dg = 1.0 / (2. * g);
    return {g, dg};
  }
  case 1: {
    const auto g = std::exp(gamma / 2.0);
    const auto dg = g / 2.;
    return {g, dg};
  }
  case 3: {
    const auto g = 2.0 / (1.0 + std::exp(-gamma));
    const auto dg = g * g * std::exp(-gamma) / 2.;
    return {g, dg};
  }
  case -5:
    if ((1.0 + gamma) >= 0.) {
      const auto g = sqrt(1.0 + gamma);
      const auto dg = 1.0 / (2. * g);
      return {g, dg};
    } else {
      const auto g = -std::sqrt(-1.0 - gamma);
      const auto dg = -1.0 / (2. * g);
      return {g, dg};
    }
  default:
    throw exception::QCException{
        "Function calc_G_dG: i_bar value not supported"};
  }
}

} // namespace materials::meam
