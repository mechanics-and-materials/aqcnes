// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Function implementation to compute concentation interpolated
 *  electronic densities and derivatives using the modified embedded
 *  atom method (MEAM) potential
 * @author S. Saxena
 */

#include "materials/meam/read_library_file.hpp"
#include "utils/string_split.hpp"
#include "utils/string_strip.hpp"
#include <algorithm>
#include <array>
#include <charconv>
#include <cmath>
#include <fstream>

namespace materials::meam {

struct IndexedKey {
  std::string_view name{};
  std::vector<std::size_t> indices{};
};
} // namespace materials::meam

namespace utils {
template <>
materials::meam::IndexedKey
parse_str<materials::meam::IndexedKey>(const std::string_view &line) {
  const auto [raw_key, tail] = utils::string_split_once(line, '(');
  if (!tail.has_value())
    return materials::meam::IndexedKey{line, {}};
  auto indices_str = utils::string_split_many<std::string_view>(
      utils::string_strip(tail.value(), ')'), ',', ' ');
  auto key = materials::meam::IndexedKey{raw_key, {}};
  key.indices.reserve(indices_str.size());
  for (const auto &idx_str : indices_str) {
    const auto idx = utils::parse_str<std::size_t>(idx_str);
    if (idx == 0)
      throw exception::QCException{"meam: Invalid index 0: " +
                                   std::string{line}};
    key.indices.emplace_back(idx - 1);
  }
  return key;
}
} // namespace utils

namespace materials::meam {

template <std::size_t N>
std::array<std::size_t, N> indices(const IndexedKey &key) {
  if (N != key.indices.size())
    throw exception::QCException{"meam: Invalid number of indices for key " +
                                 std::string{key.name} + ": expected " +
                                 std::to_string(N) + ", got " +
                                 std::to_string(key.indices.size())};
  auto indices = std::array<std::size_t, N>{};
  for (std::size_t i = 0; i < N; i++)
    indices[i] = key.indices[i];
  return indices;
}

meam::MEAMInformation create_meam_information(const std::size_t n_elements) {
  return meam::MEAMInformation{
      .a = std::vector(n_elements, 0.0),
      .beta0 = std::vector(n_elements, 0.0),
      .beta1 = std::vector(n_elements, 0.0),
      .beta2 = std::vector(n_elements, 0.0),
      .beta3 = std::vector(n_elements, 0.0),
      .t0 = std::vector(n_elements, 0.0),
      .t1 = std::vector(n_elements, 0.0),
      .t2 = std::vector(n_elements, 0.0),
      .t3 = std::vector(n_elements, 0.0),
      .rho0 = std::vector(n_elements, 0.0),
      .atomic_mass = std::vector(n_elements, 0.0),
      .rho_ref = std::vector(n_elements, 0.0),
      .alpha = std::vector(n_elements, std::vector<double>(n_elements)),
      .re0 = std::vector(n_elements, std::vector<double>(n_elements)),
      .ec = std::vector(n_elements, std::vector<double>(n_elements)),
      .delta = std::vector(n_elements, std::vector<double>(n_elements)),
      .alat = std::vector(n_elements, std::vector<double>(n_elements)),
      .ebound = std::vector(n_elements, std::vector<double>(n_elements)),
      .cmin = std::vector(n_elements,
                          std::vector<std::vector<double>>(
                              n_elements, std::vector<double>(n_elements))),
      .cmax = std::vector(n_elements,
                          std::vector<std::vector<double>>(
                              n_elements, std::vector<double>(n_elements))),
      .ibar = std::vector(n_elements, 0),
      .atomic_number = std::vector(n_elements, 0),
      .z = std::vector(n_elements, 0),
      .nn2 = std::vector(n_elements, std::vector<int>(n_elements, 0.0)),
      .zbl = std::vector(n_elements, std::vector<int>(n_elements, 1.0)),
      .attrac = std::vector(n_elements, std::vector<double>(n_elements, 0.0)),
      .repuls = std::vector(n_elements, std::vector<double>(n_elements, 0.0)),
      .lattice_structure = std::vector(
          n_elements, std::vector<lattice::LatticeStructure>(n_elements)),
      .erose_form = 0,
      .mix_ref_t = 0,
      .ialloy = 0,
      .bkgd_dyn = 0,
  };
}

lattice::LatticeStructure
parse_lattice_structure(const std::string_view &name) {
  auto name_upper = std::string(name.size(), ' ');
  std::transform(name.begin(), name.end(), name_upper.begin(), ::toupper);
  if (name_upper == "FCC")
    return lattice::LatticeStructure::FCC;
  if (name_upper == "BCC")
    return lattice::LatticeStructure::BCC;
  if (name_upper == "HCP")
    return lattice::LatticeStructure::HCP;
  if (name_upper == "DIA")
    return lattice::LatticeStructure::Diamond;
  if (name_upper == "B1")
    return lattice::LatticeStructure::B1;
  if (name_upper == "B2")
    return lattice::LatticeStructure::B2;
  if (name_upper == "L12")
    return lattice::LatticeStructure::L12;
  throw exception::QCException{"Lattice structure " + name_upper +
                               " not yet supported"};
}

meam::MEAMInformation
read_library_file(const std::filesystem::path &library_file_path,
                  const std::filesystem::path &parameter_file_path,
                  const std::vector<std::string> &element_names,
                  const std::optional<std::ostream *const> logger) {
  auto meam_information = create_meam_information(element_names.size());
  auto file = std::ifstream(library_file_path);
  if (!file.is_open())
    throw exception::QCException{"Could not open " +
                                 std::string{library_file_path}};
  const auto noof_elements = element_names.size();
  constexpr std::size_t entries_per_element = 19;
  auto line = std::string{};
  for (std::size_t i = 0; i < noof_elements; i++) {
    auto words = std::vector<std::string>{};
    words.reserve(entries_per_element);
    // break loop if end of file is reached
    if (file.peek() == EOF)
      break;
    while (words.size() < entries_per_element) {
      // skip line if it is a comment or a blank line
      getline(file, line);
      if (line[0] == '#' || line.empty())
        continue;
      const auto parts = utils::string_split_many<std::string_view>(
          line, utils::AnyOf{" \t\n\r\f"});
      for (const auto &part : parts)
        words.emplace_back(part);
    }
    if (words.size() > entries_per_element)
      throw exception::QCException{
          "Wrong format of MEAM library file: noof_words=" +
          std::to_string(words.size()) + ">" +
          std::to_string(entries_per_element)};
    // Find the index for the element whose data is in complete_line
    std::size_t el_ind = 0;
    for (el_ind = 0; el_ind < element_names.size(); el_ind++) {
      if (utils::string_strip(words[0], '\'') == element_names[el_ind])
        break;
    }
    // If that element is not of interest to us, skip
    if (logger.has_value() && el_ind == element_names.size()) {
      *logger.value() << "element " << words[0]
                      << " found in MEAM library file but not required in this "
                         "simulation.\n";
      continue;
    }

    // If we found an element of interest, store parameters
    meam_information.lattice_structure[el_ind][el_ind] =
        parse_lattice_structure(utils::string_strip(words[1], '\''));
    meam_information.z[el_ind] = utils::parse_str<int>(words[2]);
    meam_information.atomic_number[el_ind] = utils::parse_str<int>(words[3]);
    meam_information.atomic_mass[el_ind] = utils::parse_str<double>(words[4]);
    meam_information.alpha[el_ind][el_ind] = utils::parse_str<double>(words[5]);
    meam_information.beta0[el_ind] = utils::parse_str<double>(words[6]);
    meam_information.beta1[el_ind] = utils::parse_str<double>(words[7]);
    meam_information.beta2[el_ind] = utils::parse_str<double>(words[8]);
    meam_information.beta3[el_ind] = utils::parse_str<double>(words[9]);
    meam_information.alat[el_ind][el_ind] = utils::parse_str<double>(words[10]);
    switch (meam_information.lattice_structure[el_ind][el_ind]) {
    case lattice::LatticeStructure::FCC:
      meam_information.re0[el_ind][el_ind] =
          utils::parse_str<double>(words[10]) / M_SQRT2;
      break;
    case lattice::LatticeStructure::BCC:
      meam_information.re0[el_ind][el_ind] =
          utils::parse_str<double>(words[10]) * std::sqrt(3.) / 2.;
      break;
    case lattice::LatticeStructure::HCP:
      meam_information.re0[el_ind][el_ind] =
          utils::parse_str<double>(words[10]);
      break;
    case lattice::LatticeStructure::Diamond:
      meam_information.re0[el_ind][el_ind] =
          utils::parse_str<double>(words[10]) * std::sqrt(3.) / 4.;
      break;
    default:
      throw exception::QCException{
          "Unimplemented Lattice structure: " +
          std::to_string(static_cast<std::size_t>(
              meam_information.lattice_structure[el_ind][el_ind]))};
    }

    meam_information.ec[el_ind][el_ind] = utils::parse_str<double>(words[11]);
    meam_information.a[el_ind] = utils::parse_str<double>(words[12]);
    meam_information.t0[el_ind] = utils::parse_str<double>(words[13]);
    meam_information.t1[el_ind] = utils::parse_str<double>(words[14]);
    meam_information.t2[el_ind] = utils::parse_str<double>(words[15]);
    meam_information.t3[el_ind] = utils::parse_str<double>(words[16]);
    meam_information.rho0[el_ind] = utils::parse_str<double>(words[17]);
    meam_information.ibar[el_ind] = utils::parse_str<int>(words[18]);
  }

  // Now read the MEAM parameter file
  auto par_file = std::ifstream(parameter_file_path);
  if (!par_file.is_open())
    throw exception::QCException{"Could not open " +
                                 std::string{parameter_file_path}};
  while (par_file.peek() != EOF) {
    // skip line if it is a comment or a blank line
    getline(par_file, line);
    if (line[0] == '#' || line.empty())
      continue;
    auto [raw_key, val] =
        utils::string_split<std::string_view, std::string_view>(
            line, '=', utils::AnyOf{" \t\n\r\f"});
    const auto key = utils::parse_str<IndexedKey>(raw_key);

    const auto parse_int = [](const auto &s) {
      return static_cast<int>(utils::parse_str<double>(s));
    };
    // take action depending on which parameter type appears in the line
    if (key.name == "rc") {
      meam_information.rc = utils::parse_str<double>(val);
    } else if (key.name == "delr") {
      meam_information.delr = utils::parse_str<double>(val);
    } else if (key.name == "augt1") {
      meam_information.augt1 = parse_int(val);
    } else if (key.name == "ialloy") {
      meam_information.ialloy = parse_int(val);
    } else if (key.name == "erose_form") {
      meam_information.erose_form = parse_int(val);
    } else if (key.name == "gsmooth_factor") {
      meam_information.gsmooth_factor = utils::parse_str<double>(val);
    } else if (key.name == "mixture_ref_t") {
      meam_information.mix_ref_t = parse_int(val);
    } else if (key.name == "emb_lin_neg") {
      meam_information.emb_lin_neg = parse_int(val);
    } else if (key.name == "bkgd_dyn") {
      meam_information.bkgd_dyn = parse_int(val);
    } else if (key.name == "rho0") {
      const auto [i] = indices<1>(key);
      meam_information.rho0[i] = utils::parse_str<double>(val);
    } else if (key.name == "Ec") {
      const auto [i, j] = indices<2>(key);
      meam_information.ec[i][j] = meam_information.ec[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "delta") {
      const auto [i, j] = indices<2>(key);
      meam_information.delta[i][j] = meam_information.delta[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "alpha") {
      const auto [i, j] = indices<2>(key);
      meam_information.alpha[i][j] = meam_information.alpha[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "re") {
      const auto [i, j] = indices<2>(key);
      meam_information.re0[i][j] = meam_information.re0[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "attrac") {
      const auto [i, j] = indices<2>(key);
      meam_information.attrac[i][j] = meam_information.attrac[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "repuls") {
      const auto [i, j] = indices<2>(key);
      meam_information.repuls[i][j] = meam_information.repuls[j][i] =
          utils::parse_str<double>(val);
    } else if (key.name == "nn2") {
      const auto [i, j] = indices<2>(key);
      meam_information.nn2[j][i] = meam_information.nn2[i][j] = parse_int(val);
    } else if (key.name == "zbl") {
      const auto [i, j] = indices<2>(key);
      meam_information.zbl[i][j] = meam_information.zbl[j][i] = parse_int(val);
    } else if (key.name == "Cmin") {
      const auto [i, j, k] = indices<3>(key);
      meam_information.cmin[i][j][k] = meam_information.cmin[j][i][k] =
          utils::parse_str<double>(val);
    } else if (key.name == "Cmax") {
      const auto [i, j, k] = indices<3>(key);
      meam_information.cmax[i][j][k] = meam_information.cmax[j][i][k] =
          utils::parse_str<double>(val);
    } else if (key.name == "lattce") {
      const auto lattice_structure =
          parse_lattice_structure(utils::string_strip(val, '\''));
      const auto [i, j] = indices<2>(key);
      meam_information.lattice_structure[i][j] = lattice_structure;
      meam_information.lattice_structure[j][i] = lattice_structure;
    } else if (key.name == "theta") {
      continue;
    } else
      throw exception::QCException{
          "Keyword " + std::string{key.name} +
          " does not match any MEAM parameter. Check the MEAM "
          "parameter file"};
  }

  return meam_information;
}

} // namespace materials::meam
