// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors S. Saxena, M. Spinola
 */

#include "materials/meam/reference_structure_calculations.hpp"

namespace materials::meam {

double calc_g(const double gamma, const int ibar, const double gsmooth_factor) {
  switch (ibar) {
  case 0:
  case 4: {
    const auto gsmooth_switchpoint = -gsmooth_factor / (gsmooth_factor + 1);
    if (gamma < gsmooth_switchpoint)
      return std::sqrt(1 / (gsmooth_factor + 1) *
                       pow((gsmooth_switchpoint / gamma), gsmooth_factor));
    return std::sqrt(1.0 + gamma);
  }
  case 1:
    return std::exp(gamma / 2.0);
  case 3:
    return 2.0 / (1.0 + std::exp(-gamma));
  case -5:
    if ((1.0 + gamma) >= 0)
      return std::sqrt(1.0 + gamma);
    return -std::sqrt(-1.0 - gamma);
  default:
    throw exception::QCException{
        "Function meam::calc_g: i_bar value not supported"};
  }
  return 0.;
}

std::tuple<std::array<double, 4>, std::array<double, 4>>
get_density_of_ref_struct(const MEAMInformation &meam_information,
                          const double r, const std::size_t a,
                          const std::size_t b) {
  auto rho_a = std::array<double, 4>{};
  auto rho_b = std::array<double, 4>{};
  const auto lattice = meam_information.lattice_structure[a][b];
  const auto a1 = r / meam_information.re0[a][a] - 1.0;
  const auto a2 = r / meam_information.re0[b][b] - 1.0;

  const auto rhoa0_a =
      meam_information.rho0[a] * exp(-meam_information.beta0[a] * a1);
  const auto rhoa2_a =
      meam_information.rho0[a] * exp(-meam_information.beta2[a] * a1);
  const auto rhoa3_a =
      meam_information.rho0[a] * exp(-meam_information.beta3[a] * a1);
  const auto rhoa0_b =
      meam_information.rho0[b] * exp(-meam_information.beta0[b] * a2);
  const auto rhoa2_b =
      meam_information.rho0[b] * exp(-meam_information.beta2[b] * a2);
  const auto rhoa3_b =
      meam_information.rho0[b] * exp(-meam_information.beta3[b] * a2);

  switch (lattice) {
  case lattice::LatticeStructure::FCC:
    rho_a[0] = 12 * rhoa0_b;
    rho_b[0] = 12 * rhoa0_a;
    break;
  case lattice::LatticeStructure::BCC:
    rho_a[0] = 8 * rhoa0_b;
    rho_b[0] = 8 * rhoa0_a;
    break;
  case lattice::LatticeStructure::B1:
    rho_a[0] = 6 * rhoa0_b;
    rho_b[0] = 6 * rhoa0_a;
    break;
  case lattice::LatticeStructure::B2:
    rho_a[0] = 8 * rhoa0_b;
    rho_b[0] = 8 * rhoa0_a;
    break;
  case lattice::LatticeStructure::L12:
    rho_a[0] = 8 * rhoa0_a + 4 * rhoa0_b;
    rho_b[0] = 12 * rhoa0_a;
    if (meam_information.ialloy == 1) {
      rho_a[2] =
          8. / 3. *
          (rhoa2_a * meam_information.t2[a] -
           rhoa2_b * meam_information.t2[b]) *
          (rhoa2_a * meam_information.t2[a] - rhoa2_b * meam_information.t2[b]);
      const auto denom =
          8 * rhoa0_a * meam_information.t2[a] * meam_information.t2[a] +
          4 * rhoa0_b * meam_information.t2[b] * meam_information.t2[b];
      if (denom > 0.)
        rho_a[2] = rho_a[2] / denom * rho_a[0];
    } else
      rho_a[2] = 8. / 3. * (rhoa2_a - rhoa2_b) * (rhoa2_a - rhoa2_b);
    break;
  case lattice::LatticeStructure::HCP:
    rho_a[0] = 12 * rhoa0_b;
    rho_b[0] = 12 * rhoa0_a;
    rho_a[3] = (1.0 / 3.0) * rhoa3_b * rhoa3_b;
    rho_b[3] = (1.0 / 3.0) * rhoa3_a * rhoa3_a;
    break;
  case lattice::LatticeStructure::Diamond:
    rho_a[0] = 4 * rhoa0_b;
    rho_b[0] = 4 * rhoa0_a;
    rho_a[3] = (32.0 / 9.0) * rhoa3_b * rhoa3_b;
    rho_b[3] = (32.0 / 9.0) * rhoa3_a * rhoa3_a;
    break;
  default:
    throw exception::QCException{
        "Function getDensityOfRefStruct: Lattice type not supported"};
  }

  // Add contributions if second neighbor treatment is active
  if (meam_information.nn2[a][b] == 1) {
    double arat{};
    double scrn{};
    const auto z2 =
        meam::get_z2_s_a_r(lattice, meam_information.cmin[a][a][b],
                           meam_information.cmax[a][a][b], arat, scrn);
    const auto a1 = arat * r / meam_information.re0[a][a] - 1.0;
    const auto a2 = arat * r / meam_information.re0[b][b] - 1.0;
    const auto rhoa0_a_nn2 =
        meam_information.rho0[a] * exp(-meam_information.beta0[a] * a1);
    const auto rhoa0_b_nn2 =
        meam_information.rho0[b] * exp(-meam_information.beta0[b] * a2);
    if (lattice == lattice::LatticeStructure::L12) {
      // L12 crystal structure is different
      const auto c = 1.0;
      const auto s_aaa = meam::f_cut(
          (c - meam_information.cmin[a][a][a]) /
          (meam_information.cmax[a][a][a] - meam_information.cmin[a][a][a]));
      const auto s_aab = meam::f_cut(
          (c - meam_information.cmin[a][a][b]) /
          (meam_information.cmax[a][a][b] - meam_information.cmin[a][a][b]));
      const auto s_bba = meam::f_cut(
          (c - meam_information.cmin[b][b][a]) /
          (meam_information.cmax[b][b][a] - meam_information.cmin[b][b][a]));
      const auto s_aa = s_aaa * s_aaa * s_aab * s_aab;
      const auto s_bb = s_bba * s_bba * s_bba * s_bba;
      rho_a[0] += 6 * s_aa * rhoa0_a_nn2;
      rho_b[0] += 6 * s_bb * rhoa0_b_nn2;
    } else {
      rho_a[0] += static_cast<double>(z2) * scrn * rhoa0_a_nn2;
      const auto z2 =
          meam::get_z2_s_a_r(lattice, meam_information.cmin[b][b][a],
                             meam_information.cmax[b][b][a], arat, scrn);
      rho_b[0] += static_cast<double>(z2) * scrn * rhoa0_b_nn2;
    }
  }
  return std::make_tuple(rho_a, rho_b);
}

std::array<std::array<double, 4>, 4> get_density_and_derivative_of_ref_struct(
    const meam::MEAMInformation &meam_information, const double r,
    const std::size_t a, const std::size_t b) {
  auto rho_a = std::array<double, 4>{};
  auto rho_b = std::array<double, 4>{};
  auto drho_a = std::array<double, 4>{};
  auto drho_b = std::array<double, 4>{};
  const auto lattice = meam_information.lattice_structure[a][b];
  const auto a1 = r / meam_information.re0[a][a] - 1.0;
  const auto a2 = r / meam_information.re0[b][b] - 1.0;

  const auto rhoa0_a =
      meam_information.rho0[a] * exp(-meam_information.beta0[a] * a1);
  const auto drhoa0_a =
      -meam_information.beta0[a] * rhoa0_a / meam_information.re0[a][a];

  const auto rhoa2_a =
      meam_information.rho0[a] * exp(-meam_information.beta2[a] * a1);
  const auto drhoa2_a =
      -meam_information.beta2[a] * rhoa2_a / meam_information.re0[a][a];

  const auto rhoa3_a =
      meam_information.rho0[a] * exp(-meam_information.beta3[a] * a1);
  const auto drhoa3_a =
      -meam_information.beta3[a] * rhoa3_a / meam_information.re0[a][a];
  const auto rhoa0_b =
      meam_information.rho0[b] * exp(-meam_information.beta0[b] * a2);
  const auto drhoa0_b =
      -meam_information.beta0[b] * rhoa0_b / meam_information.re0[b][b];

  const auto rhoa2_b =
      meam_information.rho0[b] * exp(-meam_information.beta2[b] * a2);
  const auto drhoa2_b =
      -meam_information.beta2[b] * rhoa2_b / meam_information.re0[b][b];

  const auto rhoa3_b =
      meam_information.rho0[b] * exp(-meam_information.beta3[b] * a2);
  const auto drhoa3_b =
      -meam_information.beta3[b] * rhoa3_b / meam_information.re0[b][b];

  switch (lattice) {
  case lattice::LatticeStructure::FCC:
    rho_a[0] = 12. * rhoa0_b;
    rho_b[0] = 12. * rhoa0_a;
    drho_a[0] = 12. * drhoa0_b;
    drho_b[0] = 12. * drhoa0_a;
    break;
  case lattice::LatticeStructure::BCC:
    rho_a[0] = 8. * rhoa0_b;
    rho_b[0] = 8. * rhoa0_a;
    drho_a[0] = 8. * drhoa0_b;
    drho_b[0] = 8. * drhoa0_a;
    break;
  case lattice::LatticeStructure::B1:
    rho_a[0] = 6. * rhoa0_b;
    rho_b[0] = 6. * rhoa0_a;
    drho_a[0] = 6. * drhoa0_b;
    drho_b[0] = 6. * drhoa0_a;
    break;
  case lattice::LatticeStructure::B2:
    rho_a[0] = 8. * rhoa0_b;
    rho_b[0] = 8. * rhoa0_a;
    drho_a[0] = 8. * drhoa0_b;
    drho_b[0] = 8. * drhoa0_a;
    break;
  case lattice::LatticeStructure::L12:
    rho_a[0] = 8. * rhoa0_a + 4. * rhoa0_b;
    rho_b[0] = 12. * rhoa0_a;
    drho_a[0] = 8. * drhoa0_a + 4. * drhoa0_b;
    drho_b[0] = 12. * drhoa0_a;
    if (meam_information.ialloy == 1) {
      rho_a[2] =
          8. / 3. *
          (rhoa2_a * meam_information.t2[a] -
           rhoa2_b * meam_information.t2[b]) *
          (rhoa2_a * meam_information.t2[a] - rhoa2_b * meam_information.t2[b]);
      drho_a[2] =
          16. / 3. *
          (drhoa2_a * meam_information.t2[a] -
           drhoa2_b * meam_information.t2[b]) *
          (rhoa2_a * meam_information.t2[a] - rhoa2_b * meam_information.t2[b]);
      const auto denom =
          8. * rhoa0_a * meam_information.t2[a] * meam_information.t2[a] +
          4. * rhoa0_b * meam_information.t2[b] * meam_information.t2[b];
      if (denom > 0.) {
        drho_a[2] = drho_a[2] / denom * rho_a[0];
        drho_a[2] += rho_a[2] * drho_a[0] / denom;
        drho_a[2] -=
            (rho_a[2] / (denom * denom) * rho_a[0]) *
            (8 * drhoa0_a * meam_information.t2[a] * meam_information.t2[a] +
             4 * drhoa0_b * meam_information.t2[b] * meam_information.t2[b]);

        rho_a[2] = rho_a[2] / denom * rho_a[0];
      }
    } else
      rho_a[2] = 8. / 3. * (rhoa2_a - rhoa2_b) * (rhoa2_a - rhoa2_b);
    drho_a[2] = 16. / 3. * (rhoa2_a - rhoa2_b) * (drhoa2_a - drhoa2_b);
    break;
  case lattice::LatticeStructure::HCP:
    rho_a[0] = 12. * rhoa0_b;
    rho_b[0] = 12. * rhoa0_a;
    drho_a[0] = 12. * drhoa0_b;
    drho_b[0] = 12. * drhoa0_a;
    rho_a[3] = 1.0 / 3.0 * rhoa3_b * rhoa3_b;
    rho_b[3] = 1.0 / 3.0 * rhoa3_a * rhoa3_a;
    drho_a[3] = 2.0 / 3.0 * rhoa3_b * drhoa3_b;
    drho_b[3] = 2.0 / 3.0 * rhoa3_a * drhoa3_a;
    break;
  case lattice::LatticeStructure::Diamond:
    rho_a[0] = 4. * rhoa0_b;
    rho_b[0] = 4. * rhoa0_a;
    drho_a[0] = 4. * drhoa0_b;
    drho_b[0] = 4. * drhoa0_a;
    rho_a[3] = 32.0 / 9.0 * rhoa3_b * rhoa3_b;
    rho_b[3] = 32.0 / 9.0 * rhoa3_a * rhoa3_a;
    drho_a[3] = 64.0 / 9.0 * rhoa3_b * drhoa3_b;
    drho_b[3] = 64.0 / 9.0 * rhoa3_a * drhoa3_a;
    break;
  default:
    throw exception::QCException{
        "Function getDensityAndDerOfRefStruct: Lattice type not "
        "supported"};
  }

  // Add contributions if second neighbor treatment is active
  if (meam_information.nn2[a][b] == 1) {
    double arat{};
    double scrn{};
    const auto z2 =
        meam::get_z2_s_a_r(lattice, meam_information.cmin[a][a][b],
                           meam_information.cmax[a][a][b], arat, scrn);
    const auto a1 = arat * r / meam_information.re0[a][a] - 1.0;
    const auto rhoa0_a_nn2 =
        meam_information.rho0[a] * exp(-meam_information.beta0[a] * a1);
    const auto drhoa0_a_nn2 = -meam_information.beta0[a] * arat * rhoa0_a_nn2 /
                              meam_information.re0[a][a];

    const auto a2 = arat * r / meam_information.re0[b][b] - 1.0;
    const auto rhoa0_b_nn2 =
        meam_information.rho0[b] * exp(-meam_information.beta0[b] * a2);
    const auto drhoa0_b_nn2 = -meam_information.beta0[b] * arat * rhoa0_b_nn2 /
                              meam_information.re0[b][b];

    if (lattice == lattice::LatticeStructure::L12) {
      // L12 crystal structure is different
      const auto c = 1.0;
      const auto s_aaa = meam::f_cut(
          (c - meam_information.cmin[a][a][a]) /
          (meam_information.cmax[a][a][a] - meam_information.cmin[a][a][a]));
      const auto s_aab = meam::f_cut(
          (c - meam_information.cmin[a][a][b]) /
          (meam_information.cmax[a][a][b] - meam_information.cmin[a][a][b]));
      const auto s_bba = meam::f_cut(
          (c - meam_information.cmin[b][b][a]) /
          (meam_information.cmax[b][b][a] - meam_information.cmin[b][b][a]));
      const auto s_aa = s_aaa * s_aaa * s_aab * s_aab;
      const auto s_bb = s_bba * s_bba * s_bba * s_bba;
      rho_a[0] += 6. * s_aa * rhoa0_a_nn2;
      rho_b[0] += 6. * s_bb * rhoa0_b_nn2;
      drho_a[0] += 6. * s_aa * drhoa0_a_nn2;
      drho_b[0] += 6. * s_bb * drhoa0_b_nn2;
    } else {
      rho_a[0] += static_cast<double>(z2) * scrn * rhoa0_a_nn2;
      drho_a[0] += static_cast<double>(z2) * scrn * drhoa0_a_nn2;

      const auto z2 =
          meam::get_z2_s_a_r(lattice, meam_information.cmin[b][b][a],
                             meam_information.cmax[b][b][a], arat, scrn);

      rho_b[0] += static_cast<double>(z2) * scrn * rhoa0_b_nn2;
      drho_b[0] += static_cast<double>(z2) * scrn * drhoa0_b_nn2;
    }
  }
  return std::array{rho_a, rho_b, drho_a, drho_b};
}

} // namespace materials::meam
