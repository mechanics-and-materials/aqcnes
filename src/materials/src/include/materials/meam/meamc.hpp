// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief File parsing and basic functions to use
 *        the modified embedded atom method (MEAM) potential
 * @author S. Saxena, M. Spinola
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "materials/cluster_model.hpp"
#include "materials/material.hpp"
#include "materials/meam/cluster_energy.hpp"
#include "materials/meam/cluster_forces.hpp"
#include "materials/meam/meam_information.hpp"
#include "materials/meam/read_library_file.hpp"
#include <string>

namespace materials {

template <std::size_t DIM, std::size_t Noi,
          std::size_t NIDOF = materials::Material<DIM>::THERMAL_DOF>
class MEAMC : public ClusterModel<DIM, Noi> {
  using Point = std::array<double, DIM>;
  using NodalDataPoint = meshing::NodalDataMultispecies<Noi>;
  using ConcentrationPoint = std::array<double, Noi>;
  using EdgeVector = std::array<std::size_t, 2>;
  using ThermalPoint = std::array<double, NIDOF>;
  using AtomicElectronDensityPoint = std::array<double, 4>;
  using Drho1Point = std::array<Point, 3>;
  using Drho2Point = std::array<Point, 6>;
  using Drho3Point = std::array<Point, 10>;
  using TPoint = std::array<double, 3>;
  using DtPoint = std::array<Point, 3>;

public:
  meam::MEAMInformation meam_information{};

private:
  std::size_t noof_elements{};
  std::string name{};
  std::size_t start_idx{}, idx_offset{};
  std::vector<std::vector<double>> embedding_interpolating_polynomial_coeffs{};

public:
  MEAMC(const std::string &library_file_name,
        const std::string &parameter_file_name, std::string lattice_name,
        const std::vector<std::string> &element_names,
        const bool simulating_vacancy, const bool output_potential_log,
        const std::vector<std::vector<double>> &embedding_interpolation_coeffs)
      : meam_information{meam::read_library_file(
            library_file_name, parameter_file_name, element_names, &std::cout)},
        noof_elements{element_names.size()}, name{std::move(lattice_name)},
        start_idx{simulating_vacancy ? std::size_t{1} : 0},
        idx_offset{simulating_vacancy ? std::size_t{1} : 0} {
    this->output_potential_log = output_potential_log;
    if constexpr (DIM == 2)
      for (auto &element : this->meam_information.lattice_structure)
        for (auto &lattice_structure : element)
          if (lattice_structure == lattice::LatticeStructure::FCC)
            lattice_structure = lattice::LatticeStructure::HCP;
    this->atomic_masses = this->meam_information.atomic_mass;
    this->neighbor_cutoff_radius = this->meam_information.rc;

    // Compute a bounding value for the screening function
    for (std::size_t i = 0; i < noof_elements; i++) {
      for (std::size_t j = 0; j < noof_elements; j++) {
        for (std::size_t k = 0; k < noof_elements; k++) {
          const auto eb =
              std::sqrt((meam_information.cmax[i][j][k] *
                         meam_information.cmax[i][j][k]) /
                        (4.0 * (meam_information.cmax[i][j][k] - 1.0)));
          meam_information.ebound[i][j] =
              std::max(meam_information.ebound[i][j], eb);
        }
      }
    }

    // augument t1 values
    for (std::size_t m = 0; m < noof_elements; m++) {
      if (meam_information.augt1 == 1)
        meam_information.t1[m] += (3.0 / 5.0) * meam_information.t3[m];
      for (std::size_t g = m + 1; g < noof_elements; g++) {
        if (meam_information.ec[m][g] == 0) {
          if (meam_information.lattice_structure[m][g] ==
              lattice::LatticeStructure::L12)
            meam_information.ec[m][g] =
                (3. * meam_information.ec[m][m] + meam_information.ec[g][g]) /
                    4. -
                meam_information.delta[m][g];
          else
            meam_information.ec[m][g] =
                (meam_information.ec[m][m] + meam_information.ec[g][g]) / 2. -
                meam_information.delta[m][g];
          meam_information.ec[g][m] = meam_information.ec[m][g];
        }
      }
    }

    // Compute reference electron densities for the elements read
    meam::compute_ref_elec_density<DIM>(this->meam_information);

    // initialize embedding interpolation polynomials
    const auto expected_n_interpolation_coefs =
        simulating_vacancy ? Noi : Noi + 1;
    if (embedding_interpolation_coeffs.size() != expected_n_interpolation_coefs)
      throw exception::QCException("Incorrect number of interpolation "
                                   "coefficients provided in user input");

    embedding_interpolating_polynomial_coeffs.clear();
    embedding_interpolating_polynomial_coeffs.resize(
        embedding_interpolation_coeffs.size(), std::vector<double>{});
    for (std::size_t si = 0; si < embedding_interpolation_coeffs.size(); si++)
      std::copy(embedding_interpolation_coeffs[si].begin(),
                embedding_interpolation_coeffs[si].end(),
                back_inserter(embedding_interpolating_polynomial_coeffs[si]));
  }

  [[nodiscard]] double get_cluster_energy(
      const Point &atom, const std::vector<Point> &neighbors,
      const NodalDataPoint &data_point_atom,                  // the atom
      const std::vector<NodalDataPoint> &data_point_neighbor) // the neighbors
      const override {
    const auto [u, embedding] = meam::cluster_energy(
        atom, neighbors, data_point_atom, data_point_neighbor,
        this->embedding_interpolating_polynomial_coeffs, this->meam_information,
        this->start_idx, this->idx_offset);
    if (this->output_potential_log) {
      std::ofstream f_potential_log;
      f_potential_log.open("./potential_log.dat", std::ios::app);
      f_potential_log << (u - embedding) << " " << u << '\n';
      f_potential_log.close();
    }
    return u;
  }

  void get_cluster_forces(
      std::vector<Point> &forces, const Point &atom,
      const std::vector<Point> &neighbors,
      const NodalDataPoint &data_point_atom,
      const std::vector<NodalDataPoint> &data_point_neighbor) const override {
    meam::cluster_forces(
        forces, atom, neighbors, data_point_atom, data_point_neighbor,
        this->embedding_interpolating_polynomial_coeffs, this->meam_information,
        this->start_idx, this->idx_offset);
  }

  void
  get_concentration_forces(ConcentrationPoint &,
                           std::vector<ConcentrationPoint> &, const Point &,
                           const std::vector<Point> &, const NodalDataPoint &,
                           const std::vector<NodalDataPoint> &) const override {
  }
};

} // namespace materials
