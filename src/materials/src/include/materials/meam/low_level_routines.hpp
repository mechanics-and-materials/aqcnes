// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author S. Saxena
 */

#pragma once
#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include <cmath>
#include <tuple>

namespace materials::meam {

/**
 * @brief cutoff function used in screening calculation
 */
double f_cut(const double x);

/**
 * @brief derivative of the cutoff function
 */
//
double df_cut(const double x);

/**
 * @brief get number of nearest first neighbors
 */
std::size_t get_z(const lattice::LatticeStructure lattice);

/**
 * @brief get number of second-nearest neighbors, screening values for them and ratio
 * of distances
*/
std::size_t get_z2_s_a_r(const lattice::LatticeStructure lattice,
                         const double cmin, const double cmax, double &a_r,
                         double &s);

/**
 * @brief zbl potential (blend with MEAM pair potential).
 */
double zbl_potential(const double r, const int atm_n_m, const int atm_n_g);

std::tuple<double, double> zbl_potential_and_derivative(const double r,
                                                        const int atm_n_m,
                                                        const int atm_n_g);

/**
 * @brief derivative of C_ikj wrt r_ij r_jk r_ik
 */
std::tuple<double, double, double> d_cikj(const double r_ij, const double r_ik,
                                          const double r_kj);

double comp_embedding_func(const double rhob, const double a, const double ec,
                           const int emb_lin_neg);

std::tuple<double, double>
comp_embedding_func_and_derivative(const double rhob, const double a,
                                   const double ec, const int emb_lin_neg);

/**
 * @brief Rose empirical energy formula
 */
double calc_rose_embedding_energy(const double r, const int form,
                                  const double attrac, const double repuls,
                                  const double r0, const double alpha,
                                  const double ec);

/**
 * @brief Rose empirical energy formula and derivative
 */
std::tuple<double, double> calc_rose_embedding_energy_and_derivative(
    const double r, const int form, const double attrac, const double repuls,
    const double r0, const double alpha, const double ec);

std::tuple<double, double> calc_g_d_g(const double gamma, const int ibar,
                                      const double gsmooth_factor);

} // namespace materials::meam
