// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Function implementation to compute reference structure energy and forces
 *
 * @authors S. Saxena, M. Spinola
 */

#pragma once
#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include "materials/meam/low_level_routines.hpp"
#include "materials/meam/meam_information.hpp"
#include <array>
#include <cmath>
#include <tuple>

namespace materials::meam {
/**
 * @brief Get shape factors assuming reference configuration. Used for ref elec density calculation
 */
template <std::size_t DIM>
std::array<double, DIM>
get_ref_shape_factors(const lattice::LatticeStructure lattice) {
  auto factors = std::array<double, DIM>{};
  switch (lattice) {
  case lattice::LatticeStructure::FCC:
  case lattice::LatticeStructure::BCC:
  case lattice::LatticeStructure::B1:
  case lattice::LatticeStructure::B2:
  case lattice::LatticeStructure::L12:
    break;
  case lattice::LatticeStructure::HCP:
    factors[2] = 1.0 / 3.0;
    break;
  case lattice::LatticeStructure::Diamond:
    factors[2] = 32.0 / 9.0;
    break;
  default:
    throw exception::QCException{"Function meam::get_ref_shape_factors: "
                                 "lattice structure not supported"};
  }
  return factors;
}

double calc_g(const double gamma, const int ibar, const double gsmooth_factor);

/**
 * @brief find and store electron densities for reference configurations with reference lattice spacing
 */
template <std::size_t DIM>
void compute_ref_elec_density(meam::MEAMInformation &meam_information) {
  for (std::size_t i = 0; i < meam_information.rho_ref.size(); i++) {
    meam_information.rho_ref[i] = 0.0;
    const auto z1 = get_z(meam_information.lattice_structure[i][i]);
    if (meam_information.bkgd_dyn == 1) {
      meam_information.rho_ref[i] =
          meam_information.rho0[i] * static_cast<double>(z1);
    } else {
      double gbar{};
      if (meam_information.ibar[i] <= 0) {
        gbar = 1.0;
      } else {
        const auto sf = meam::get_ref_shape_factors<DIM>(
            meam_information.lattice_structure[i][i]);
        const auto gamma =
            (meam_information.t1[i] * sf[0] + meam_information.t2[i] * sf[1] +
             meam_information.t3[i] * sf[2]) /
            (z1 * z1);
        gbar = meam::calc_g(gamma, meam_information.ibar[i],
                            meam_information.gsmooth_factor);
      }
      auto rho0 = meam_information.rho0[i] * static_cast<double>(z1);

      if (meam_information.nn2[i][i] == 1) {
        auto a_r = 1.0;
        auto s = double{};
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[i][i],
                               meam_information.cmin[i][i][i],
                               meam_information.cmax[i][i][i], a_r, s);
        rho0 += s * static_cast<double>(z2) * meam_information.rho0[i] *
                std::exp(-meam_information.beta0[i] * (a_r - 1));
      }
      meam_information.rho_ref[i] = rho0 * gbar;
    }
  }
}

/**
 * @brief Compute electron density of a reference structure with given lattice spacing r (needed for pair potential calc)
 */
std::tuple<std::array<double, 4>, std::array<double, 4>>
get_density_of_ref_struct(const MEAMInformation &meam_information,
                          const double r, const std::size_t a,
                          const std::size_t b);

std::array<std::array<double, 4>, 4> get_density_and_derivative_of_ref_struct(
    const meam::MEAMInformation &meam_information, const double r,
    const std::size_t a, const std::size_t b);

} // namespace materials::meam
