// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author S. Saxena
 */

#pragma once

#include "materials/meam/meam_information.hpp"
#include <filesystem>
#include <optional>
#include <vector>

namespace materials::meam {

meam::MEAMInformation create_meam_information(const std::size_t n_elements);
lattice::LatticeStructure parse_lattice_structure(const std::string_view &name);
/**
 * @brief Read parameters from a library file.
 *
 * if (DIM == 3) {
 *   fcc_lattice_structure = lattice::LatticeStructure::FCC;
 * else
 *   fcc_lattice_structure = lattice::LatticeStructure::HCP;
 *
 this->atomic_masses[el_ind] = this->meam_information.atomic_mass[el_ind];
 this->neighbor_cutoff_radius = meam_information.rc;
 */
meam::MEAMInformation read_library_file(
    const std::filesystem::path &library_file_path,
    const std::filesystem::path &parameter_file_path,
    const std::vector<std::string> &element_names,
    const std::optional<std::ostream *const> logger = std::nullopt);

} // namespace materials::meam
