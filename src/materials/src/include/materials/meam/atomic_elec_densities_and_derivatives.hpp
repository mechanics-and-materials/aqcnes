// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors S. Saxena, A. Wang, M. Spinola
 */

#pragma once

#include "materials/meam/meam_information.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <cmath>

namespace materials::meam {

namespace detail {

template <std::size_t Dimension> auto unit_vector_array(const std::size_t i) {
  auto vec = std::array<double, Dimension>{};
  vec[i] = 1.0;
  return vec;
}

} // namespace detail

template <std::size_t DIM, std::size_t Noi> struct AtomicElectronDensity {
  const meam::MEAMInformation &meam_information;
  std::size_t start_idx{}, idx_offset{};

  using Point = std::array<double, DIM>;
  using NodalDataPoint =
      meshing::NodalDataMultispecies<Noi>; // find probabilities here
  using TPoint = std::array<double, 3>;
  using DtPoint = std::array<Point, 3>;
  using AtomicElectronDensityPoint = std::array<double, 4>;
  using Drho1Point = std::array<Point, 3>;
  using Drho2Point = std::array<Point, 6>;
  using Drho3Point = std::array<Point, 10>;
  /**
   * @brief compute the interpolated atomic electron densities from every neighbor,
   * and their derivatives needed for the forces
   */
  void get_neighbor_atomic_electron_density_and_derivatives(
      const std::vector<Point> &relative_neighbor_unit_vectors,
      const std::vector<double> &neighbor_distances,
      const std::vector<std::vector<std::vector<double>>> &screening_values,
      const std::vector<double> &cutoff_screening_values,
      const std::vector<std::vector<std::vector<Point>>> &dscr_fncs_drj,
      const std::vector<std::vector<std::vector<std::vector<Point>>>>
          &dscr_fncs_drk,
      const std::vector<double> &dcutoff_scr_drij,
      const std::size_t species_type_i,
      const std::vector<NodalDataPoint> &data_point_neighbor,
      const std::vector<double> &conc_sum,
      const std::vector<TPoint> &conc_averaged_t_values,
      std::vector<AtomicElectronDensityPoint>
          &interpolated_atomic_electron_densities, // output
      std::vector<Point> &drho0_drj,
      std::vector<Drho1Point> &drho1_drj_componentwise,
      std::vector<Drho2Point> &drho2_drj_componentwise,
      std::vector<Drho3Point> &drho3_drj_componentwise,
      std::vector<Point> &drho2sub_drj,
      std::vector<Drho1Point> &drho3sub_drj_componentwise,
      std::vector<DtPoint> &dt_avg_values_drj,
      std::vector<DtPoint> &dtsq_avg_values_drj) const {
    namespace array_ops = qcmesh::array_ops;

    const auto noof_elements = this->meam_information.a.size();
    std::vector<std::vector<AtomicElectronDensityPoint>>
        all_atomic_electron_densities;
    all_atomic_electron_densities.resize(
        neighbor_distances.size(),
        std::vector<AtomicElectronDensityPoint>(noof_elements,
                                                AtomicElectronDensityPoint{}));

    Point temporary_derivative_variable;

    // clear output vectors
    interpolated_atomic_electron_densities.clear();
    interpolated_atomic_electron_densities.resize(neighbor_distances.size(),
                                                  AtomicElectronDensityPoint{});

    drho0_drj.clear();
    drho2sub_drj.clear();
    drho1_drj_componentwise.clear();
    drho3sub_drj_componentwise.clear();
    drho2_drj_componentwise.clear();
    drho3_drj_componentwise.clear();
    dt_avg_values_drj.clear();
    dtsq_avg_values_drj.clear();

    drho0_drj.resize(neighbor_distances.size(), Point{});
    drho2sub_drj.resize(neighbor_distances.size(), Point{});
    drho1_drj_componentwise.resize(neighbor_distances.size(), Drho1Point{});
    drho3sub_drj_componentwise.resize(neighbor_distances.size(), Drho1Point{});
    drho2_drj_componentwise.resize(neighbor_distances.size(), Drho2Point{});
    drho3_drj_componentwise.resize(neighbor_distances.size(), Drho3Point{});
    dt_avg_values_drj.resize(neighbor_distances.size(), DtPoint{});
    dtsq_avg_values_drj.resize(neighbor_distances.size(), DtPoint{});

    // loop for finding atomic elec densities
    for (std::size_t neighbour_idx = 0;
         neighbour_idx < neighbor_distances.size(); neighbour_idx++) {

      if (neighbor_distances[neighbour_idx] > meam_information.rc)
        continue;

      for (std::size_t m = start_idx; m < Noi + 1; m++) {
        const auto concentration =
            (m < Noi)
                ? data_point_neighbor[neighbour_idx].impurity_concentrations[m]
                : (1 - conc_sum[neighbour_idx]);

        const auto a =
            neighbor_distances[neighbour_idx] /
                meam_information.re0[m - idx_offset][m - idx_offset] -
            1.0;

        all_atomic_electron_densities[neighbour_idx][m - idx_offset][0] =
            meam_information.rho0[m - idx_offset] *
            std::exp(-meam_information.beta0[m - idx_offset] * a);
        all_atomic_electron_densities[neighbour_idx][m - idx_offset][1] =
            meam_information.rho0[m - idx_offset] *
            std::exp(-meam_information.beta1[m - idx_offset] * a);
        all_atomic_electron_densities[neighbour_idx][m - idx_offset][2] =
            meam_information.rho0[m - idx_offset] *
            std::exp(-meam_information.beta2[m - idx_offset] * a);
        all_atomic_electron_densities[neighbour_idx][m - idx_offset][3] =
            meam_information.rho0[m - idx_offset] *
            std::exp(-meam_information.beta3[m - idx_offset] * a);

        if (meam_information.ialloy == 1) {
          all_atomic_electron_densities[neighbour_idx][m - idx_offset][1] *=
              conc_averaged_t_values[neighbour_idx][0];
          all_atomic_electron_densities[neighbour_idx][m - idx_offset][2] *=
              conc_averaged_t_values[neighbour_idx][1];
          all_atomic_electron_densities[neighbour_idx][m - idx_offset][3] *=
              conc_averaged_t_values[neighbour_idx][2];
        }

        // compute the interpolated atomic electron density from atom j
        interpolated_atomic_electron_densities[neighbour_idx][0] +=
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][0] *
            screening_values[neighbour_idx][species_type_i][m - idx_offset];
        interpolated_atomic_electron_densities[neighbour_idx][1] +=
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][1] *
            screening_values[neighbour_idx][species_type_i][m - idx_offset];
        interpolated_atomic_electron_densities[neighbour_idx][2] +=
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][2] *
            screening_values[neighbour_idx][species_type_i][m - idx_offset];
        interpolated_atomic_electron_densities[neighbour_idx][3] +=
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][3] *
            screening_values[neighbour_idx][species_type_i][m - idx_offset];

      } // loop over species of j

    } // loop over j

    // compute the force components on every neighbor
    for (std::size_t neighbour_idx = 0;
         neighbour_idx < neighbor_distances.size(); neighbour_idx++) {

      if (neighbor_distances[neighbour_idx] > meam_information.rc)
        continue;

      for (std::size_t m = start_idx; m < Noi + 1; m++) {
        // add contributions from other neighbors
        for (std::size_t screening_neighbour_idx = 0;
             screening_neighbour_idx < neighbor_distances.size();
             screening_neighbour_idx++) {
          if (screening_neighbour_idx == neighbour_idx)
            continue;

          const auto concentration =
              (m < Noi) ? data_point_neighbor[screening_neighbour_idx]
                              .impurity_concentrations[m]
                        : (1 - conc_sum[screening_neighbour_idx]);

          auto c2 = std::size_t{0};
          auto c3 = std::size_t{0};

          array_ops::as_eigen(temporary_derivative_variable) =
              concentration *
              all_atomic_electron_densities[screening_neighbour_idx]
                                           [m - idx_offset][0] *
              cutoff_screening_values[screening_neighbour_idx] *
              array_ops::as_eigen(
                  dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                               [species_type_i][m - idx_offset]);

          // add derivatives of avg t_values due to dSik_drj according to ialloy value in the MEAM file
          if (meam_information.ialloy != 2) {
            array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][0]) +=
                conc_averaged_t_values[screening_neighbour_idx][0] *
                array_ops::as_eigen(temporary_derivative_variable);
            array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][1]) +=
                conc_averaged_t_values[screening_neighbour_idx][1] *
                array_ops::as_eigen(temporary_derivative_variable);
            array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][2]) +=
                conc_averaged_t_values[screening_neighbour_idx][2] *
                array_ops::as_eigen(temporary_derivative_variable);
            if (meam_information.ialloy == 1) {
              array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][0]) +=
                  conc_averaged_t_values[screening_neighbour_idx][0] *
                  conc_averaged_t_values[screening_neighbour_idx][0] *
                  array_ops::as_eigen(temporary_derivative_variable);
              array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][1]) +=
                  conc_averaged_t_values[screening_neighbour_idx][1] *
                  conc_averaged_t_values[screening_neighbour_idx][1] *
                  array_ops::as_eigen(temporary_derivative_variable);
              array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][2]) +=
                  conc_averaged_t_values[screening_neighbour_idx][2] *
                  conc_averaged_t_values[screening_neighbour_idx][2] *
                  array_ops::as_eigen(temporary_derivative_variable);
            }
          }

          // add force on j due to dSik_drj for all k's
          array_ops::as_eigen(drho0_drj[neighbour_idx]) +=
              array_ops::as_eigen(temporary_derivative_variable);

          array_ops::as_eigen(drho2sub_drj[neighbour_idx]) +=
              concentration *
              all_atomic_electron_densities[screening_neighbour_idx]
                                           [m - idx_offset][2] *
              cutoff_screening_values[screening_neighbour_idx] *
              array_ops::as_eigen(
                  dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                               [species_type_i][m - idx_offset]);

          for (std::size_t p = 0; p < DIM; p++) {
            array_ops::as_eigen(drho1_drj_componentwise[neighbour_idx][p]) +=
                concentration *
                all_atomic_electron_densities[screening_neighbour_idx]
                                             [m - idx_offset][1] *
                cutoff_screening_values[screening_neighbour_idx] *
                relative_neighbor_unit_vectors[screening_neighbour_idx][p] *
                array_ops::as_eigen(
                    dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                                 [species_type_i][m - idx_offset]);

            array_ops::as_eigen(drho3sub_drj_componentwise[neighbour_idx][p]) +=
                concentration *
                all_atomic_electron_densities[screening_neighbour_idx]
                                             [m - idx_offset][3] *
                cutoff_screening_values[screening_neighbour_idx] *
                relative_neighbor_unit_vectors[screening_neighbour_idx][p] *
                array_ops::as_eigen(
                    dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                                 [species_type_i][m - idx_offset]);

            for (std::size_t q = p; q < DIM; q++) {
              array_ops::as_eigen(drho2_drj_componentwise[neighbour_idx][c2]) +=
                  concentration *
                  all_atomic_electron_densities[screening_neighbour_idx]
                                               [m - idx_offset][2] *
                  cutoff_screening_values[screening_neighbour_idx] *
                  relative_neighbor_unit_vectors[screening_neighbour_idx][p] *
                  relative_neighbor_unit_vectors[screening_neighbour_idx][q] *
                  array_ops::as_eigen(
                      dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                                   [species_type_i][m - idx_offset]);
              c2++;

              for (std::size_t r = q; r < DIM; r++) {
                array_ops::as_eigen(
                    drho3_drj_componentwise[neighbour_idx][c3]) +=
                    concentration *
                    all_atomic_electron_densities[screening_neighbour_idx]
                                                 [m - idx_offset][3] *
                    cutoff_screening_values[screening_neighbour_idx] *
                    relative_neighbor_unit_vectors[screening_neighbour_idx][p] *
                    relative_neighbor_unit_vectors[screening_neighbour_idx][q] *
                    relative_neighbor_unit_vectors[screening_neighbour_idx][r] *
                    array_ops::as_eigen(
                        dscr_fncs_drk[screening_neighbour_idx][neighbour_idx]
                                     [species_type_i][m - idx_offset]);
                c3++;
              }
            }
          }

        } // loop over k (screening neighbors)

        // add contributions from j itself
        const auto concentration =
            (m < Noi)
                ? data_point_neighbor[neighbour_idx].impurity_concentrations[m]
                : (1 - conc_sum[neighbour_idx]);

        array_ops::as_eigen(temporary_derivative_variable) =
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][0] *
            cutoff_screening_values[neighbour_idx] *
            (array_ops::as_eigen(
                 dscr_fncs_drj[neighbour_idx][species_type_i][m - idx_offset]) +
             screening_values[neighbour_idx][species_type_i][m - idx_offset] *
                 (-meam_information.beta0[m - idx_offset] /
                  meam_information.re0[m - idx_offset][m - idx_offset]) *
                 array_ops::as_eigen(
                     relative_neighbor_unit_vectors[neighbour_idx]));

        // add force component to avg t_values
        if (meam_information.ialloy != 2) {
          array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][0]) +=
              conc_averaged_t_values[neighbour_idx][0] *
              array_ops::as_eigen(temporary_derivative_variable);
          array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][1]) +=
              conc_averaged_t_values[neighbour_idx][1] *
              array_ops::as_eigen(temporary_derivative_variable);
          array_ops::as_eigen(dt_avg_values_drj[neighbour_idx][2]) +=
              conc_averaged_t_values[neighbour_idx][2] *
              array_ops::as_eigen(temporary_derivative_variable);
          if (meam_information.ialloy == 1) {
            array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][0]) +=
                conc_averaged_t_values[neighbour_idx][0] *
                conc_averaged_t_values[neighbour_idx][0] *
                array_ops::as_eigen(temporary_derivative_variable);
            array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][1]) +=
                conc_averaged_t_values[neighbour_idx][1] *
                conc_averaged_t_values[neighbour_idx][1] *
                array_ops::as_eigen(temporary_derivative_variable);
            array_ops::as_eigen(dtsq_avg_values_drj[neighbour_idx][2]) +=
                conc_averaged_t_values[neighbour_idx][2] *
                conc_averaged_t_values[neighbour_idx][2] *
                array_ops::as_eigen(temporary_derivative_variable);
          }
        }

        // add force components of partial electron densities
        array_ops::as_eigen(drho0_drj[neighbour_idx]) +=
            array_ops::as_eigen(temporary_derivative_variable);

        array_ops::as_eigen(drho2sub_drj[neighbour_idx]) +=
            concentration *
            all_atomic_electron_densities[neighbour_idx][m - idx_offset][2] *
            cutoff_screening_values[neighbour_idx] *
            (array_ops::as_eigen(
                 dscr_fncs_drj[neighbour_idx][species_type_i][m - idx_offset]) +
             screening_values[neighbour_idx][species_type_i][m - idx_offset] *
                 (-meam_information.beta2[m - idx_offset] /
                  meam_information.re0[m - idx_offset][m - idx_offset]) *
                 array_ops::as_eigen(
                     relative_neighbor_unit_vectors[neighbour_idx]));

        auto c2 = std::size_t{0};
        auto c3 = std::size_t{0};

        for (std::size_t p = 0; p < DIM; p++) {

          array_ops::as_eigen(drho1_drj_componentwise[neighbour_idx][p]) +=
              concentration *
              all_atomic_electron_densities[neighbour_idx][m - idx_offset][1] *
              cutoff_screening_values[neighbour_idx] *
              (relative_neighbor_unit_vectors[neighbour_idx][p] *
                   (array_ops::as_eigen(
                        dscr_fncs_drj[neighbour_idx][species_type_i]
                                     [m - idx_offset]) +
                    screening_values[neighbour_idx][species_type_i]
                                    [m - idx_offset] *
                        (-meam_information.beta1[m - idx_offset] /
                         meam_information.re0[m - idx_offset][m - idx_offset]) *
                        array_ops::as_eigen(
                            relative_neighbor_unit_vectors[neighbour_idx])) +
               screening_values[neighbour_idx][species_type_i][m - idx_offset] *
                   (array_ops::as_eigen(detail::unit_vector_array<DIM>(p)) -
                    (relative_neighbor_unit_vectors[neighbour_idx][p] *
                     array_ops::as_eigen(
                         relative_neighbor_unit_vectors[neighbour_idx]))) /
                   neighbor_distances[neighbour_idx]);

          array_ops::as_eigen(drho3sub_drj_componentwise[neighbour_idx][p]) +=
              concentration *
              all_atomic_electron_densities[neighbour_idx][m - idx_offset][3] *
              cutoff_screening_values[neighbour_idx] *
              (relative_neighbor_unit_vectors[neighbour_idx][p] *
                   (array_ops::as_eigen(
                        dscr_fncs_drj[neighbour_idx][species_type_i]
                                     [m - idx_offset]) +
                    screening_values[neighbour_idx][species_type_i]
                                    [m - idx_offset] *
                        (-meam_information.beta3[m - idx_offset] /
                         meam_information.re0[m - idx_offset][m - idx_offset]) *
                        array_ops::as_eigen(
                            relative_neighbor_unit_vectors[neighbour_idx])) +
               screening_values[neighbour_idx][species_type_i][m - idx_offset] *
                   (array_ops::as_eigen(detail::unit_vector_array<DIM>(p)) -
                    (relative_neighbor_unit_vectors[neighbour_idx][p] *
                     array_ops::as_eigen(
                         relative_neighbor_unit_vectors[neighbour_idx]))) /
                   neighbor_distances[neighbour_idx]);

          for (std::size_t q = p; q < DIM; q++) {
            array_ops::as_eigen(drho2_drj_componentwise[neighbour_idx][c2]) +=
                concentration *
                all_atomic_electron_densities[neighbour_idx][m - idx_offset]
                                             [2] *
                cutoff_screening_values[neighbour_idx] *
                (relative_neighbor_unit_vectors[neighbour_idx][p] *
                     relative_neighbor_unit_vectors[neighbour_idx][q] *
                     (array_ops::as_eigen(
                          dscr_fncs_drj[neighbour_idx][species_type_i]
                                       [m - idx_offset]) +
                      screening_values[neighbour_idx][species_type_i]
                                      [m - idx_offset] *
                          (-meam_information.beta2[m - idx_offset] /
                           meam_information
                               .re0[m - idx_offset][m - idx_offset]) *
                          array_ops::as_eigen(
                              relative_neighbor_unit_vectors[neighbour_idx])) +
                 screening_values[neighbour_idx][species_type_i]
                                 [m - idx_offset] *
                     (relative_neighbor_unit_vectors[neighbour_idx][q] *
                          (array_ops::as_eigen(
                               detail::unit_vector_array<DIM>(p)) -
                           (relative_neighbor_unit_vectors[neighbour_idx][p] *
                            array_ops::as_eigen(relative_neighbor_unit_vectors
                                                    [neighbour_idx]))) +
                      relative_neighbor_unit_vectors[neighbour_idx][p] *
                          (array_ops::as_eigen(
                               detail::unit_vector_array<DIM>(q)) -
                           (relative_neighbor_unit_vectors[neighbour_idx][q] *
                            array_ops::as_eigen(relative_neighbor_unit_vectors
                                                    [neighbour_idx])))) /
                     neighbor_distances[neighbour_idx]);

            c2++;

            for (std::size_t r = q; r < DIM; r++) {
              array_ops::as_eigen(drho3_drj_componentwise[neighbour_idx][c3]) +=
                  concentration *
                  all_atomic_electron_densities[neighbour_idx][m - idx_offset]
                                               [3] *
                  cutoff_screening_values[neighbour_idx] *
                  (relative_neighbor_unit_vectors[neighbour_idx][p] *
                       relative_neighbor_unit_vectors[neighbour_idx][q] *
                       relative_neighbor_unit_vectors[neighbour_idx][r] *
                       (array_ops::as_eigen(
                            dscr_fncs_drj[neighbour_idx][species_type_i]
                                         [m - idx_offset]) +
                        screening_values[neighbour_idx][species_type_i]
                                        [m - idx_offset] *
                            (-meam_information.beta3[m - idx_offset] /
                             meam_information
                                 .re0[m - idx_offset][m - idx_offset]) *
                            array_ops::as_eigen(relative_neighbor_unit_vectors
                                                    [neighbour_idx])) +
                   screening_values[neighbour_idx][species_type_i]
                                   [m - idx_offset] *
                       (relative_neighbor_unit_vectors[neighbour_idx][r] *
                            relative_neighbor_unit_vectors[neighbour_idx][q] *
                            (array_ops::as_eigen(
                                 detail::unit_vector_array<DIM>(p)) -
                             (relative_neighbor_unit_vectors[neighbour_idx][p] *
                              array_ops::as_eigen(relative_neighbor_unit_vectors
                                                      [neighbour_idx]))) +
                        relative_neighbor_unit_vectors[neighbour_idx][r] *
                            relative_neighbor_unit_vectors[neighbour_idx][p] *
                            (array_ops::as_eigen(
                                 detail::unit_vector_array<DIM>(q)) -
                             (relative_neighbor_unit_vectors[neighbour_idx][q] *
                              array_ops::as_eigen(relative_neighbor_unit_vectors
                                                      [neighbour_idx]))) +
                        relative_neighbor_unit_vectors[neighbour_idx][p] *
                            relative_neighbor_unit_vectors[neighbour_idx][q] *
                            (array_ops::as_eigen(
                                 detail::unit_vector_array<DIM>(r)) -
                             (relative_neighbor_unit_vectors[neighbour_idx][r] *
                              array_ops::as_eigen(relative_neighbor_unit_vectors
                                                      [neighbour_idx])))) /
                       neighbor_distances[neighbour_idx]);
              c3++;
            } // loop over r
          }   // loop over q
        }     // loop over p

      } // loop over species of j/k

      // add contributions from the derivative of screening cutoff
      array_ops::as_eigen(drho0_drj[neighbour_idx]) +=
          interpolated_atomic_electron_densities[neighbour_idx][0] *
          dcutoff_scr_drij[neighbour_idx] *
          array_ops::as_eigen(relative_neighbor_unit_vectors[neighbour_idx]);

      array_ops::as_eigen(drho2sub_drj[neighbour_idx]) +=
          interpolated_atomic_electron_densities[neighbour_idx][2] *
          dcutoff_scr_drij[neighbour_idx] *
          array_ops::as_eigen(relative_neighbor_unit_vectors[neighbour_idx]);

      auto c2 = std::size_t{0};
      auto c3 = std::size_t{0};

      for (std::size_t p = 0; p < DIM; p++) {
        array_ops::as_eigen(drho1_drj_componentwise[neighbour_idx][p]) +=
            interpolated_atomic_electron_densities[neighbour_idx][1] *
            relative_neighbor_unit_vectors[neighbour_idx][p] *
            dcutoff_scr_drij[neighbour_idx] *
            array_ops::as_eigen(relative_neighbor_unit_vectors[neighbour_idx]);

        array_ops::as_eigen(drho3sub_drj_componentwise[neighbour_idx][p]) +=
            interpolated_atomic_electron_densities[neighbour_idx][3] *
            relative_neighbor_unit_vectors[neighbour_idx][p] *
            dcutoff_scr_drij[neighbour_idx] *
            array_ops::as_eigen(relative_neighbor_unit_vectors[neighbour_idx]);

        for (std::size_t q = p; q < DIM; q++) {
          array_ops::as_eigen(drho2_drj_componentwise[neighbour_idx][c2]) +=
              interpolated_atomic_electron_densities[neighbour_idx][2] *
              relative_neighbor_unit_vectors[neighbour_idx][p] *
              relative_neighbor_unit_vectors[neighbour_idx][q] *
              dcutoff_scr_drij[neighbour_idx] *
              array_ops::as_eigen(
                  relative_neighbor_unit_vectors[neighbour_idx]);

          c2++;

          for (std::size_t r = q; r < DIM; r++) {
            array_ops::as_eigen(drho3_drj_componentwise[neighbour_idx][c3]) +=
                interpolated_atomic_electron_densities[neighbour_idx][3] *
                relative_neighbor_unit_vectors[neighbour_idx][p] *
                relative_neighbor_unit_vectors[neighbour_idx][q] *
                relative_neighbor_unit_vectors[neighbour_idx][r] *
                dcutoff_scr_drij[neighbour_idx] *
                array_ops::as_eigen(
                    relative_neighbor_unit_vectors[neighbour_idx]);

            c3++;
          } // loop over r
        }   // loop over q
      }     // loop over p

    } // loop over j
  }   // end of function

  void get_neighbor_atomic_electron_density(
      double &rhoa0, double &rhoa1, double &rhoa2, double &rhoa3,
      const double r_ij, const NodalDataPoint &data_point_neighbor,
      const double &conc_sum_j,
      const std::vector<double> &screening_values) const {

    // these rhoa's are only for this neighbor, different neighbors have different screening factors
    std::vector<double> rhoa0_vec;
    std::vector<double> rhoa1_vec;
    std::vector<double> rhoa2_vec;
    std::vector<double> rhoa3_vec;

    const auto noof_elements = this->meam_information.a.size();
    // compute rhoa array for the current neighbor
    for (std::size_t m = 0; m < noof_elements; m++) {
      const auto a = r_ij / meam_information.re0[m][m] - 1.0;
      rhoa0_vec.push_back(meam_information.rho0[m] *
                          std::exp(-meam_information.beta0[m] * a));
      rhoa1_vec.push_back(meam_information.rho0[m] *
                          std::exp(-meam_information.beta1[m] * a));
      rhoa2_vec.push_back(meam_information.rho0[m] *
                          std::exp(-meam_information.beta2[m] * a));
      rhoa3_vec.push_back(meam_information.rho0[m] *
                          std::exp(-meam_information.beta3[m] * a));
    }

    rhoa0 = 0.0;
    rhoa1 = 0.0;
    rhoa2 = 0.0;
    rhoa3 = 0.0;

    // interpolate rhoa's to one value, first with the solute concentrations
    for (std::size_t m = start_idx; m < Noi; m++) {
      rhoa0 += data_point_neighbor.impurity_concentrations[m] *
               rhoa0_vec[m - idx_offset] * screening_values[m - idx_offset];
      rhoa1 += data_point_neighbor.impurity_concentrations[m] *
               rhoa1_vec[m - idx_offset] * screening_values[m - idx_offset];
      rhoa2 += data_point_neighbor.impurity_concentrations[m] *
               rhoa2_vec[m - idx_offset] * screening_values[m - idx_offset];
      rhoa3 += data_point_neighbor.impurity_concentrations[m] *
               rhoa3_vec[m - idx_offset] * screening_values[m - idx_offset];
    }

    // solvent
    rhoa0 += (1 - conc_sum_j) * rhoa0_vec[noof_elements - 1] *
             screening_values[noof_elements - 1];
    rhoa1 += (1 - conc_sum_j) * rhoa1_vec[noof_elements - 1] *
             screening_values[noof_elements - 1];
    rhoa2 += (1 - conc_sum_j) * rhoa2_vec[noof_elements - 1] *
             screening_values[noof_elements - 1];
    rhoa3 += (1 - conc_sum_j) * rhoa3_vec[noof_elements - 1] *
             screening_values[noof_elements - 1];
  }
};

} // namespace materials::meam
