// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to compute concentation interpolated forces
 *        using the modified embedded atom method (MEAM) potential
 * @authors S. Saxena, A. Wang, M. Spinola
 */

#pragma once
#include "exception/qc_exception.hpp"
#include "materials/meam/atomic_elec_densities_and_derivatives.hpp"
#include "materials/meam/cluster.hpp"
#include "materials/meam/low_level_routines.hpp"
#include "materials/meam/meam_information.hpp"
#include "materials/meam/pair_potential_and_derivatives.hpp"
#include "materials/meam/reference_structure_calculations.hpp"
#include "materials/meam/screening_and_derivatives.hpp"
#include "utils/evaluate_polynomial.hpp"

namespace materials::meam {

template <std::size_t DIM, std::size_t Noi>
void cluster_forces(
    std::vector<std::array<double, DIM>> &forces,
    const std::array<double, DIM> &atom,
    const std::vector<std::array<double, DIM>> &neighbors,
    const meshing::NodalDataMultispecies<Noi> &data_point_atom,
    const std::vector<meshing::NodalDataMultispecies<Noi>> &data_point_neighbor,
    const std::vector<std::vector<double>>
        &embedding_interpolating_polynomial_coeffs,
    const meam::MEAMInformation &meam_information, const std::size_t start_idx,
    const std::size_t idx_offset) {
  namespace array_ops = qcmesh::array_ops;
  using Point = std::array<double, DIM>;
  using TPoint = std::array<double, 3>;
  using DtPoint = std::array<Point, 3>;
  using AtomicElectronDensityPoint = std::array<double, 4>;
  using Drho1Point = std::array<Point, 3>;
  using Drho2Point = std::array<Point, 6>;
  using Drho3Point = std::array<Point, 10>;
  forces.clear();
  forces.resize(neighbors.size(), Point{});

  std::vector<std::vector<std::vector<double>>> s_values;
  std::vector<std::vector<std::vector<Point>>> d_s_drj_values; // NxMxMx3
  std::vector<std::vector<std::vector<std::vector<Point>>>>
      d_s_drk_values;                  // dimension: NxNxMxMx3
  std::vector<double> cutoff_s_values; // independent of species
  std::vector<double> dcutoff_s_drij_values;
  std::vector<TPoint> conc_averaged_t_values;

  std::vector<AtomicElectronDensityPoint>
      interpolated_atomic_electron_densities;
  std::vector<Point> relative_neighbor_unit_vectors;
  std::vector<std::vector<Point>> relative_inter_neighbor_unit_vectors;
  relative_inter_neighbor_unit_vectors.clear();
  relative_inter_neighbor_unit_vectors.resize(
      neighbors.size(), std::vector<Point>(neighbors.size(), Point{}));

  std::vector<double> neighbor_distances;
  std::vector<std::vector<double>> inter_neighbor_distances;
  inter_neighbor_distances.clear();
  inter_neighbor_distances.resize(neighbors.size(),
                                  std::vector<double>(neighbors.size(), 0.0));

  std::vector<Point> drho0_drj_vec; // Nx3
  std::vector<Drho1Point>
      drho1_drj_componentwise; // Nx3x3, all derivatives for all neighbors, for
  // one species of center atom
  std::vector<Drho2Point> drho2_drj_componentwise;    // Nx6x3
  std::vector<Drho3Point> drho3_drj_componentwise;    // Nx10x3
  std::vector<Point> drho2sub_drj_vec;                // Nx3
  std::vector<Drho1Point> drho3sub_drj_componentwise; // Nx3x3

  Point r_ij_vec;
  Point r_kj_vec;
  Point dgamma_drj;
  Point drhobar_rj;
  Point drho1_drj;
  Point drho2_drj;
  Point drho3_drj;
  Point d_t1_ave_drj{};
  Point d_t2_ave_drj{};
  Point d_t3_ave_drj{};
  Point dgamma_bkgd_drj;
  Point drho_bkgd_drj;

  std::vector<DtPoint> dt_avg_drj;
  std::vector<DtPoint> dtsq_avg_drj;

  double rho_bkgd{};
  double g_bkgd{};
  double d_g_bkgd{};

  std::vector<double> rho1_vec(3, 0.0);
  std::vector<double> rho3_sub_vec(3, 0.0);
  std::vector<double> rho2_vec(6, 0.0);
  std::vector<double> rho3_vec(10, 0.0);
  std::vector<Point> d_rho1_rj(3, Point{});
  std::vector<Point> d_rho3sub_rj(3, Point{}); // 3x3
  std::vector<Point> d_rho2_rj(6, Point{});
  std::vector<Point> d_rho3_rj(10, Point{});

  // find total solute concs at the central atom and all neighbors
  auto conc_sum_i = 0.0;
  std::vector<double> conc_sum_j = std::vector<double>(neighbors.size(), 0.0);
  for (std::size_t k = 0; k < Noi; k++) {
    conc_sum_i += data_point_atom.impurity_concentrations[k];
    for (std::size_t j = 0; j < neighbors.size(); j++) {
      conc_sum_j[j] += data_point_neighbor[j].impurity_concentrations[k];
    }
  }

  // get t values at each neighbor
  get_concentration_averaged_t_values(data_point_neighbor, conc_sum_j,
                                      conc_averaged_t_values, meam_information,
                                      start_idx, idx_offset);

  // get all neighbor vectors and distances and store in vectors
  for (std::size_t j = 0; j < neighbors.size(); j++) {
    const auto neighbor = neighbors[j];
    array_ops::as_eigen(r_ij_vec) =
        array_ops::as_eigen(neighbor) - array_ops::as_eigen(atom);

    const auto r_ij = array_ops::norm(r_ij_vec);
    if (r_ij < 1e-6)
      throw exception::QCException{
          "Coinciding neighbor to central atom detected"};

    array_ops::as_eigen(r_ij_vec) = array_ops::as_eigen(r_ij_vec) / r_ij;

    relative_neighbor_unit_vectors.push_back(r_ij_vec);
    neighbor_distances.push_back(r_ij);

    for (std::size_t k = j + 1; k < neighbors.size(); k++) {
      const auto screening_neighbor = neighbors[k];
      array_ops::as_eigen(r_kj_vec) = array_ops::as_eigen(neighbor) -
                                      array_ops::as_eigen(screening_neighbor);

      const auto r_kj = array_ops::norm(r_kj_vec);
      if (r_kj < 1e-6)
        throw exception::QCException{"Coinciding neighbors detected"};
      inter_neighbor_distances[j][k] = r_kj;
      inter_neighbor_distances[k][j] = r_kj;
      array_ops::as_eigen(relative_inter_neighbor_unit_vectors[j][k]) =
          array_ops::as_eigen(r_kj_vec) / r_kj;
      array_ops::as_eigen(relative_inter_neighbor_unit_vectors[k][j]) =
          -array_ops::as_eigen(relative_inter_neighbor_unit_vectors[j][k]);
    }
  }

  // First compute all screening factors and derivatives
  meam::compute_screening_and_derivatives(
      neighbor_distances, inter_neighbor_distances,
      relative_neighbor_unit_vectors, relative_inter_neighbor_unit_vectors,
      data_point_neighbor, conc_sum_j, meam_information, start_idx, idx_offset,
      s_values, cutoff_s_values, d_s_drj_values, d_s_drk_values,
      dcutoff_s_drij_values);

  // a loop for all i-species, get all rhoa's and derivatives first, loop needs
  // a vector of all ij vectors
  for (std::size_t i_g = start_idx; i_g < Noi + 1; i_g++) {

    const auto concentration_i =
        (i_g < Noi) ? data_point_atom.impurity_concentrations[i_g]
                    : (1 - conc_sum_i);

    const auto t1_i = meam_information.t1[i_g - idx_offset];
    const auto t2_i = meam_information.t2[i_g - idx_offset];
    const auto t3_i = meam_information.t3[i_g - idx_offset];
    const auto ro_0 = meam_information.rho0[i_g - idx_offset];
    const auto ibar = meam_information.ibar[i_g - idx_offset];

    // reset these vectors
    rho1_vec.assign(rho1_vec.size(), 0.0);
    rho2_vec.assign(rho2_vec.size(), 0.0);
    rho3_vec.assign(rho3_vec.size(), 0.0);
    rho3_sub_vec.assign(rho3_sub_vec.size(), 0.0);

    auto t1_ave = 0.0;
    auto t2_ave = 0.0;
    auto t3_ave = 0.0;
    auto t1sq_ave = 0.0;
    auto t2sq_ave = 0.0;
    auto t3sq_ave = 0.0;

    auto rho0 = 0.0;
    auto rho2_sub = 0.0;

    // all outputs below are sepccific to the current g
    AtomicElectronDensity<DIM, Noi>{meam_information, start_idx, idx_offset}
        .get_neighbor_atomic_electron_density_and_derivatives(
            relative_neighbor_unit_vectors, neighbor_distances, s_values,
            cutoff_s_values, d_s_drj_values, d_s_drk_values,
            dcutoff_s_drij_values, i_g - idx_offset, data_point_neighbor,
            conc_sum_j, conc_averaged_t_values,
            // all outputs:
            interpolated_atomic_electron_densities, // has 4 components in each
            // point, which correspond to
            // rhoa0-3, recalculated for
            // each i-species
            drho0_drj_vec, drho1_drj_componentwise, drho2_drj_componentwise,
            drho3_drj_componentwise, drho2sub_drj_vec,
            drho3sub_drj_componentwise, dt_avg_drj, dtsq_avg_drj);

    // Now loop over all atoms to find rho0,1,2,3 and atomic electron densities
    for (std::size_t j = 0; j < neighbors.size(); j++) {

      if (*std::max_element(s_values[j][i_g - idx_offset].begin(),
                            s_values[j][i_g - idx_offset].end()) < 1e-6)
        continue;

      const auto r_ij = neighbor_distances[j];
      r_ij_vec = relative_neighbor_unit_vectors[j];

      if (r_ij > meam_information.rc)
        continue;

      const auto rhoa0 =
          interpolated_atomic_electron_densities[j][0] * cutoff_s_values[j];
      auto rhoa1 =
          interpolated_atomic_electron_densities[j][1] * cutoff_s_values[j];
      auto rhoa2 =
          interpolated_atomic_electron_densities[j][2] * cutoff_s_values[j];
      auto rhoa3 =
          interpolated_atomic_electron_densities[j][3] * cutoff_s_values[j];

      if (meam_information.ialloy == 1) {
        rhoa1 *= conc_averaged_t_values[j][0];
        rhoa2 *= conc_averaged_t_values[j][1];
        rhoa3 *= conc_averaged_t_values[j][2];
        t1sq_ave +=
            conc_averaged_t_values[j][0] * conc_averaged_t_values[j][0] * rhoa0;
        t2sq_ave +=
            conc_averaged_t_values[j][1] * conc_averaged_t_values[j][1] * rhoa0;
        t3sq_ave +=
            conc_averaged_t_values[j][2] * conc_averaged_t_values[j][2] * rhoa0;
      }

      rho0 += rhoa0;
      rho2_sub += rhoa2;

      if (meam_information.ialloy != 2) {
        t1_ave += conc_averaged_t_values[j][0] * rhoa0;
        t2_ave += conc_averaged_t_values[j][1] * rhoa0;
        t3_ave += conc_averaged_t_values[j][2] * rhoa0;
      }

      auto c2 = std::size_t{0};
      auto c3 = std::size_t{0};
      for (std::size_t p = 0; p < DIM; p++) {
        rho1_vec[p] += r_ij_vec[p] * rhoa1;
        rho3_sub_vec[p] += r_ij_vec[p] * rhoa3;
        for (std::size_t q = p; q < DIM; q++) {
          rho2_vec[c2] += r_ij_vec[p] * r_ij_vec[q] * rhoa2;
          c2++;
          for (std::size_t r = q; r < DIM; r++) {
            rho3_vec[c3] += r_ij_vec[p] * r_ij_vec[q] * r_ij_vec[r] * rhoa3;
            c3++;
          }
        }
      }
    } // loop over j

    auto rho1 = 0.0;
    auto rho2 = -(1.0 / 3.0) * rho2_sub * rho2_sub;
    auto rho3 = 0.0;

    for (std::size_t p = 0; p < DIM; p++) {
      rho1 += rho1_vec[p] * rho1_vec[p];
      rho3 -= (3.0 / 5.0) * rho3_sub_vec[p] * rho3_sub_vec[p];
    }
    for (std::size_t p = 0; p < 6; p++) {
      rho2 += static_cast<double>(V2[p]) * rho2_vec[p] * rho2_vec[p];
    }
    for (std::size_t p = 0; p < 10; p++) {
      rho3 += static_cast<double>(V3[p]) * rho3_vec[p] * rho3_vec[p];
    }

    if (rho0 > 0.0) {
      if (meam_information.ialloy == 1) {
        t1_ave = t1_ave / t1sq_ave;
        t2_ave = t2_ave / t2sq_ave;
        t3_ave = t3_ave / t3sq_ave;
      }
      if (meam_information.ialloy == 0) {
        t1_ave = t1_ave / rho0;
        t2_ave = t2_ave / rho0;
        t3_ave = t3_ave / rho0;
      }
      if (meam_information.ialloy == 2) {
        t1_ave = t1_i;
        t2_ave = t2_i;
        t3_ave = t3_i;
      }
    }

    auto gamma = t1_ave * rho1 + t2_ave * rho2 + t3_ave * rho3;
    if (rho0 > 1e-6) {
      gamma /= (rho0 * rho0);
    }

    // find G and its derivative
    const auto [g, d_g] =
        meam::calc_g_d_g(gamma, ibar, meam_information.gsmooth_factor);
    const auto rho = rho0 * g;

    // finding background electron density
    const auto z1 = meam::get_z(
        meam_information.lattice_structure[i_g - idx_offset][i_g - idx_offset]);
    const auto ref_shape_factors = meam::get_ref_shape_factors<DIM>(
        meam_information.lattice_structure[i_g - idx_offset][i_g - idx_offset]);

    if (meam_information.mix_ref_t == 0)
      rho_bkgd = meam_information.rho_ref[i_g - idx_offset];
    else {
      if (ibar <= 0) {
        g_bkgd = 1.0;
        d_g_bkgd = 0.0;
      } else {
        const auto gamma_bkgd =
            (t1_ave * ref_shape_factors[0] + t2_ave * ref_shape_factors[1] +
             t3_ave * ref_shape_factors[2]) /
            (z1 * z1);
        std::tie(g_bkgd, d_g_bkgd) =
            meam::calc_g_d_g(gamma_bkgd, ibar, meam_information.gsmooth_factor);
      }
      rho_bkgd = ro_0 * g_bkgd * static_cast<double>(z1);
    }

    // find embedding function and its derivative
    const auto rho_bar = rho / rho_bkgd;

    //  dF_vec has dimension Mx1, M is species of center atom,
    // get a dF for each element of rho_bar_vec, no interpolation at this point,
    // since dF will be multiplied with drhobar_rj to get forces later,
    // and drhobar_rj will be a vector of Mx3
    const auto [_, d_f] = meam::comp_embedding_func_and_derivative(
        rho_bar, meam_information.a[i_g - idx_offset],
        meam_information.ec[i_g - idx_offset][i_g - idx_offset],
        meam_information.emb_lin_neg);

    // second loop to find forces
    for (std::size_t j = 0; j < neighbors.size();
         j++) { // add force for each neighbor

      dgamma_drj.fill(0.0);
      drho1_drj.fill(0.0);
      drho2_drj.fill(0.0);
      drho3_drj.fill(0.0);

      const auto r_ij = neighbor_distances[j];
      if (r_ij > meam_information.rc)
        continue;

      // combine summation terms, all calculations below correspond to one
      // specific g and j
      array_ops::as_eigen(drho2_drj) =
          -(2.0 / 3.0) * rho2_sub * array_ops::as_eigen(drho2sub_drj_vec[j]);

      for (std::size_t p = 0; p < DIM; p++) {
        array_ops::as_eigen(drho1_drj) +=
            2 * rho1_vec[p] *
            array_ops::as_eigen(drho1_drj_componentwise[j][p]);
        array_ops::as_eigen(drho3_drj) -=
            (6.0 / 5.0) * rho3_sub_vec[p] *
            array_ops::as_eigen(drho3sub_drj_componentwise[j][p]);
      }
      for (std::size_t p = 0; p < 6; p++) {
        array_ops::as_eigen(drho2_drj) +=
            static_cast<double>(V2[p]) * 2 * rho2_vec[p] *
            array_ops::as_eigen(drho2_drj_componentwise[j][p]);
      }
      for (std::size_t p = 0; p < 10; p++) {
        array_ops::as_eigen(drho3_drj) +=
            static_cast<double>(V3[p]) * 2 * rho3_vec[p] *
            array_ops::as_eigen(drho3_drj_componentwise[j][p]);
      }

      if (rho0 > 1e-8) {
        array_ops::as_eigen(
            dgamma_drj) = // derivative of gamma, all squares already included
            (t1_ave *
                 (array_ops::as_eigen(drho1_drj) -
                  2 * rho1 * array_ops::as_eigen(drho0_drj_vec[j]) / rho0) +
             t2_ave *
                 (array_ops::as_eigen(drho2_drj) -
                  2 * rho2 * array_ops::as_eigen(drho0_drj_vec[j]) / rho0) +
             t3_ave *
                 (array_ops::as_eigen(drho3_drj) -
                  2 * rho3 * array_ops::as_eigen(drho0_drj_vec[j]) / rho0)) /
            (rho0 * rho0);

        // add derivative of t-avg values if ialloy != 2
        if (meam_information.ialloy == 0) {
          array_ops::as_eigen(d_t1_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][0]) -
               t1_ave * array_ops::as_eigen(drho0_drj_vec[j])) /
              rho0;
          array_ops::as_eigen(d_t2_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][1]) -
               t2_ave * array_ops::as_eigen(drho0_drj_vec[j])) /
              rho0;
          array_ops::as_eigen(d_t3_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][2]) -
               t3_ave * array_ops::as_eigen(drho0_drj_vec[j])) /
              rho0;
        } else if (meam_information.ialloy == 1) {
          array_ops::as_eigen(d_t1_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][0]) -
               t1_ave * array_ops::as_eigen(dtsq_avg_drj[j][0])) /
              t1sq_ave;
          array_ops::as_eigen(d_t2_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][1]) -
               t2_ave * array_ops::as_eigen(dtsq_avg_drj[j][1])) /
              t2sq_ave;
          array_ops::as_eigen(d_t3_ave_drj) =
              (array_ops::as_eigen(dt_avg_drj[j][2]) -
               t3_ave * array_ops::as_eigen(dtsq_avg_drj[j][2])) /
              t3sq_ave;
        }

        if (meam_information.ialloy != 2) {
          array_ops::as_eigen(dgamma_drj) +=
              (rho1 * array_ops::as_eigen(d_t1_ave_drj) +
               rho2 * array_ops::as_eigen(d_t2_ave_drj) +
               rho3 * array_ops::as_eigen(d_t3_ave_drj)) /
              (rho0 * rho0);
        }

        array_ops::as_eigen(drhobar_rj) =
            (g * array_ops::as_eigen(drho0_drj_vec[j]) +
             rho0 * d_g * array_ops::as_eigen(dgamma_drj)) /
            rho_bkgd;

        if (meam_information.mix_ref_t == 1) {
          array_ops::as_eigen(dgamma_bkgd_drj) =
              (array_ops::as_eigen(d_t1_ave_drj) * ref_shape_factors[0] +
               array_ops::as_eigen(d_t2_ave_drj) * ref_shape_factors[1] +
               array_ops::as_eigen(d_t3_ave_drj) * ref_shape_factors[2]) /
              (z1 * z1);

          array_ops::as_eigen(drho_bkgd_drj) =
              ro_0 * d_g_bkgd * static_cast<double>(z1) *
              array_ops::as_eigen(dgamma_bkgd_drj);

          array_ops::as_eigen(drhobar_rj) -=
              (rho_bar / rho_bkgd) * array_ops::as_eigen(drho_bkgd_drj);
        }

      } else
        drhobar_rj = {0.0, 0.0, 0.0};

      array_ops::as_eigen(forces[j]) -=
          utils::evaluate_polynomial(
              embedding_interpolating_polynomial_coeffs[i_g - start_idx],
              concentration_i) *
          d_f * array_ops::as_eigen(drhobar_rj);

    } // j-loop

  } // g-loop

  double dphi_one_pair = 0.0;
  std::vector<std::vector<std::vector<double>>> phi_values;
  std::vector<double> interpolated_phi_values;
  std::vector<double> interpolated_dphi_values;
  phi_values.clear();
  interpolated_dphi_values.clear();
  interpolated_phi_values.clear();
  const auto noof_elements = meam_information.a.size();
  phi_values.resize(
      neighbors.size(),
      std::vector<std::vector<double>>(
          noof_elements, std::vector<double>(noof_elements, 0.0)));
  interpolated_phi_values.resize(neighbors.size(), 0.0);
  interpolated_dphi_values.resize(neighbors.size(), 0.0);

  // pair potential forces
  // first loop over all neighbors to store the phi and dphi values for all
  // species combinations
  for (std::size_t j = 0; j < neighbors.size(); j++) {
    const auto r_ij = neighbor_distances[j];

    if (r_ij < meam_information.rc) {
      // interpolation over the species of i and j
      for (std::size_t a = start_idx; a < Noi; a++) { // center atom species
        if (s_values[j][a - idx_offset][a - idx_offset] > 1e-6) {
          phi_values[j][a - idx_offset][a - idx_offset] =
              meam::calc_total_pair_potential_and_derivative<DIM>(
                  r_ij, a - idx_offset, a - idx_offset, dphi_one_pair,
                  meam_information);

          interpolated_phi_values[j] +=
              data_point_atom.impurity_concentrations[a] *
              data_point_neighbor[j].impurity_concentrations[a] *
              phi_values[j][a - idx_offset][a - idx_offset] *
              s_values[j][a - idx_offset][a - idx_offset];

          interpolated_dphi_values[j] +=
              data_point_atom.impurity_concentrations[a] *
              data_point_neighbor[j].impurity_concentrations[a] *
              dphi_one_pair * s_values[j][a - idx_offset][a - idx_offset];
        }
        for (std::size_t b = a + 1; b < Noi; b++) { // neighbor atom species
          if (s_values[j][a - idx_offset][b - idx_offset] > 1e-6) {
            phi_values[j][a - idx_offset][b - idx_offset] =
                meam::calc_total_pair_potential_and_derivative<DIM>(
                    r_ij, a - idx_offset, b - idx_offset, dphi_one_pair,
                    meam_information);

            interpolated_phi_values[j] +=
                (data_point_atom.impurity_concentrations[a] *
                     data_point_neighbor[j].impurity_concentrations[b] +
                 data_point_atom.impurity_concentrations[b] *
                     data_point_neighbor[j].impurity_concentrations[a]) *
                phi_values[j][a - idx_offset][b - idx_offset] *
                s_values[j][a - idx_offset][b - idx_offset];

            interpolated_dphi_values[j] +=
                (data_point_atom.impurity_concentrations[a] *
                     data_point_neighbor[j].impurity_concentrations[b] +
                 data_point_atom.impurity_concentrations[b] *
                     data_point_neighbor[j].impurity_concentrations[a]) *
                dphi_one_pair * s_values[j][a - idx_offset][b - idx_offset];
          }
        }
        if (s_values[j][a - idx_offset][noof_elements - 1] > 1e-6) {
          phi_values[j][a - idx_offset][noof_elements - 1] =
              meam::calc_total_pair_potential_and_derivative<DIM>(
                  r_ij, a - idx_offset, noof_elements - 1, dphi_one_pair,
                  meam_information);

          interpolated_phi_values[j] +=
              (data_point_atom.impurity_concentrations[a] *
                   (1 - conc_sum_j[j]) +
               (1 - conc_sum_i) *
                   data_point_neighbor[j].impurity_concentrations[a]) *
              phi_values[j][a - idx_offset][noof_elements - 1] *
              s_values[j][a - idx_offset][noof_elements - 1];

          interpolated_dphi_values[j] +=
              (data_point_atom.impurity_concentrations[a] *
                   (1 - conc_sum_j[j]) +
               (1 - conc_sum_i) *
                   data_point_neighbor[j].impurity_concentrations[a]) *
              dphi_one_pair * s_values[j][a - idx_offset][noof_elements - 1];
        }
      }
      if (s_values[j][noof_elements - 1][noof_elements - 1] > 1e-6) {
        phi_values[j][noof_elements - 1][noof_elements - 1] =
            meam::calc_total_pair_potential_and_derivative<DIM>(
                r_ij, noof_elements - 1, noof_elements - 1, dphi_one_pair,
                meam_information);

        interpolated_phi_values[j] +=
            (1 - conc_sum_j[j]) * (1 - conc_sum_i) *
            phi_values[j][noof_elements - 1][noof_elements - 1] *
            s_values[j][noof_elements - 1][noof_elements - 1];

        interpolated_dphi_values[j] +=
            (1 - conc_sum_j[j]) * (1 - conc_sum_i) * dphi_one_pair *
            s_values[j][noof_elements - 1][noof_elements - 1];
      }
    }
  } // j-loop

  // another loop to assemble the pair potential forces
  for (std::size_t j = 0; j < neighbors.size(); j++) {
    const auto r_ij = neighbor_distances[j];

    if (r_ij < meam_information.rc) {
      array_ops::as_eigen(forces[j]) -=
          0.5 * (interpolated_dphi_values[j] * cutoff_s_values[j] *
                     array_ops::as_eigen(relative_neighbor_unit_vectors[j]) +
                 interpolated_phi_values[j] * dcutoff_s_drij_values[j] *
                     array_ops::as_eigen(relative_neighbor_unit_vectors[j]));

      // interpolation over the species of i and j
      for (std::size_t a = start_idx; a < Noi; a++) { // center atom species
        if (s_values[j][a - idx_offset][a - idx_offset] > 1e-6) {
          array_ops::as_eigen(forces[j]) -=
              0.5 * data_point_atom.impurity_concentrations[a] *
              data_point_neighbor[j].impurity_concentrations[a] *
              phi_values[j][a - idx_offset][a - idx_offset] *
              cutoff_s_values[j] *
              array_ops::as_eigen(
                  d_s_drj_values[j][a - idx_offset][a - idx_offset]);
        }
        for (std::size_t b = a + 1; b < Noi; b++) { // neighbor atom species
          if (s_values[j][a - idx_offset][b - idx_offset] > 1e-6) {
            array_ops::as_eigen(forces[j]) -=
                0.5 *
                (data_point_atom.impurity_concentrations[a] *
                     data_point_neighbor[j].impurity_concentrations[b] +
                 data_point_atom.impurity_concentrations[b] *
                     data_point_neighbor[j].impurity_concentrations[a]) *
                phi_values[j][a - idx_offset][b - idx_offset] *
                cutoff_s_values[j] *
                array_ops::as_eigen(
                    d_s_drj_values[j][a - idx_offset][b - idx_offset]);
          }
        }
        if (s_values[j][a - idx_offset][noof_elements - 1] > 1e-6) {
          array_ops::as_eigen(forces[j]) -=
              0.5 *
              (data_point_atom.impurity_concentrations[a] *
                   (1 - conc_sum_j[j]) +
               (1 - conc_sum_i) *
                   data_point_neighbor[j].impurity_concentrations[a]) *
              phi_values[j][a - idx_offset][noof_elements - 1] *
              cutoff_s_values[j] *
              array_ops::as_eigen(
                  d_s_drj_values[j][a - idx_offset][noof_elements - 1]);
        }
      }
      if (s_values[j][noof_elements - 1][noof_elements - 1] > 1e-6) {
        array_ops::as_eigen(forces[j]) -=
            0.5 * (1 - conc_sum_j[j]) * (1 - conc_sum_i) *
            phi_values[j][noof_elements - 1][noof_elements - 1] *
            cutoff_s_values[j] *
            array_ops::as_eigen(
                d_s_drj_values[j][noof_elements - 1][noof_elements - 1]);
      }
    } // if statement

    // another loop to add the forces due to dSik_drj in pair potential term
    for (std::size_t k = 0; k < neighbors.size(); k++) {
      if (k == j)
        continue;

      const auto r_ik = neighbor_distances[k];

      if (r_ik < meam_information.rc) {
        // interpolation over the species of i and k
        for (std::size_t a = start_idx; a < Noi; a++) { // center atom species
          if (s_values[k][a - idx_offset][a - idx_offset] > 1e-6) {
            array_ops::as_eigen(forces[j]) -=
                0.5 * data_point_atom.impurity_concentrations[a] *
                data_point_neighbor[j].impurity_concentrations[a] *
                phi_values[k][a - idx_offset][a - idx_offset] *
                cutoff_s_values[k] *
                array_ops::as_eigen(
                    d_s_drk_values[k][j][a - idx_offset][a - idx_offset]);
          }
          for (std::size_t b = a + 1; b < Noi; b++) { // neighbor atom species
            if (s_values[k][a - idx_offset][b - idx_offset] > 1e-6) {
              array_ops::as_eigen(forces[j]) -=
                  0.5 *
                  (data_point_atom.impurity_concentrations[a] *
                       data_point_neighbor[j].impurity_concentrations[b] +
                   data_point_atom.impurity_concentrations[b] *
                       data_point_neighbor[j].impurity_concentrations[a]) *
                  phi_values[k][a - idx_offset][b - idx_offset] *
                  cutoff_s_values[k] *
                  array_ops::as_eigen(
                      d_s_drk_values[k][j][a - idx_offset][b - idx_offset]);
            }
          }
          if (s_values[k][a - idx_offset][noof_elements - 1] > 1e-6) {
            array_ops::as_eigen(forces[j]) -=
                0.5 *
                (data_point_atom.impurity_concentrations[a] *
                     (1 - conc_sum_j[j]) +
                 (1 - conc_sum_i) *
                     data_point_neighbor[j].impurity_concentrations[a]) *
                phi_values[k][a - idx_offset][noof_elements - 1] *
                cutoff_s_values[k] *
                array_ops::as_eigen(
                    d_s_drk_values[k][j][a - idx_offset][noof_elements - 1]);
          }
        }
        if (s_values[k][noof_elements - 1][noof_elements - 1] > 1e-6) {
          array_ops::as_eigen(forces[j]) -=
              0.5 * (1 - conc_sum_j[j]) * (1 - conc_sum_i) *
              phi_values[k][noof_elements - 1][noof_elements - 1] *
              cutoff_s_values[k] *
              array_ops::as_eigen(
                  d_s_drk_values[k][j][noof_elements - 1][noof_elements - 1]);
        }
      } // if inside cutoff
    }   // k loop
  }     // j loop
}
} // namespace materials::meam
