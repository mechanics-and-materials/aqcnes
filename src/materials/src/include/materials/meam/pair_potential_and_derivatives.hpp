// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to compute concentation interpolated pair potential and derivatives
 *        using the modified embedded atom method (MEAM) potential
 * @authors S. Saxena, A. Wang, M. Spinola
 */

#pragma once
#include "materials/meam/low_level_routines.hpp"
#include "materials/meam/meam_information.hpp"
#include "materials/meam/reference_structure_calculations.hpp"

namespace materials::meam {

/**
 * @brief compute additional terms in series for phi (if NN2 activated)
 */
template <std::size_t DIM>
double phi_series_addition(const double r, const int a, const int b,
                           const double b2, const double arat,
                           const meam::MEAMInformation &meam_information);

/**
 * @brief Find pair potential given distance and atom types
 */
template <std::size_t DIM>
double calc_pair_potential(const double r, const std::size_t a,
                           const std::size_t b,
                           const meam::MEAMInformation &meam_information) {
  const auto lattice = meam_information.lattice_structure[a][b];
  double phi{};
  double ta1_ave{};
  double ta2_ave{};
  double ta3_ave{};
  double tb1_ave{};
  double tb2_ave{};
  double tb3_ave{};
  double rho_bkgd_a{};
  double rho_bkgd_b{};
  double g_bkgd_a{};
  double g_bkgd_b{};
  double rho_bar_b{};
  const auto [rho_a, rho_b] =
      meam::get_density_of_ref_struct(meam_information, r, a, b);

  const auto za1 = meam::get_z(meam_information.lattice_structure[a][a]);
  const auto zb1 = meam::get_z(meam_information.lattice_structure[b][b]);
  const auto zab1 = meam::get_z(lattice);

  if (meam_information.ialloy == 2) {
    ta1_ave = meam_information.t1[a];
    ta2_ave = meam_information.t2[a];
    ta3_ave = meam_information.t3[a];
    tb1_ave = meam_information.t1[b];
    tb2_ave = meam_information.t2[b];
    tb3_ave = meam_information.t3[b];
  } else {
    ta1_ave = meam_information.t1[b];
    ta2_ave = meam_information.t2[b];
    ta3_ave = meam_information.t3[b];
    tb1_ave = meam_information.t1[a];
    tb2_ave = meam_information.t2[a];
    tb3_ave = meam_information.t3[a];
  }

  // finding background electron density
  if (meam_information.mix_ref_t == 0) {
    rho_bkgd_a = meam_information.rho_ref[a];
    rho_bkgd_b = meam_information.rho_ref[b];
  } else {
    if (meam_information.ibar[a] <= 0)
      g_bkgd_a = 1.0;
    else {
      // element a
      const auto ref_shape_factors_a = meam::get_ref_shape_factors<DIM>(
          meam_information.lattice_structure[a][a]);
      const auto gamma_bkgd_a =
          (ta1_ave * ref_shape_factors_a[0] + ta2_ave * ref_shape_factors_a[1] +
           ta3_ave * ref_shape_factors_a[2]) /
          (za1 * za1);
      g_bkgd_a = meam::calc_g(gamma_bkgd_a, meam_information.ibar[a],
                              meam_information.gsmooth_factor);
    }
    rho_bkgd_a = meam_information.rho0[a] * g_bkgd_a * static_cast<double>(za1);
    // element b
    if (meam_information.ibar[b] <= 0)
      g_bkgd_b = 1.0;
    else {
      const auto ref_shape_factors_b = meam::get_ref_shape_factors<DIM>(
          meam_information.lattice_structure[b][b]);
      const auto gamma_bkgd_b =
          (tb1_ave * ref_shape_factors_b[0] + tb2_ave * ref_shape_factors_b[1] +
           tb3_ave * ref_shape_factors_b[2]) /
          (zb1 * zb1);
      g_bkgd_b = meam::calc_g(gamma_bkgd_b, meam_information.ibar[b],
                              meam_information.gsmooth_factor);
    }
    rho_bkgd_b = meam_information.rho0[b] * g_bkgd_b * static_cast<double>(zb1);
  }

  auto gamma_a = ta1_ave * rho_a[1] + ta2_ave * rho_a[2] + ta3_ave * rho_a[3];
  if (rho_a[0] < 1e-10)
    gamma_a = 0.0;
  else
    gamma_a /= rho_a[0] * rho_a[0];
  const auto rho_bar_a = rho_a[0] *
                         meam::calc_g(gamma_a, meam_information.ibar[a],
                                      meam_information.gsmooth_factor) /
                         rho_bkgd_a;
  const auto fa = meam::comp_embedding_func(rho_bar_a, meam_information.a[a],
                                            meam_information.ec[a][a],
                                            meam_information.emb_lin_neg);

  auto gamma_b = tb1_ave * rho_b[1] + tb2_ave * rho_b[2] + tb3_ave * rho_b[3];
  if (rho_b[0] < 1e-10)
    gamma_b = 0.0;
  else
    gamma_b /= rho_b[0] * rho_b[0];
  rho_bar_b = rho_b[0] *
              meam::calc_g(gamma_b, meam_information.ibar[b],
                           meam_information.gsmooth_factor) /
              rho_bkgd_b;
  const auto fb = meam::comp_embedding_func(rho_bar_b, meam_information.a[b],
                                            meam_information.ec[b][b],
                                            meam_information.emb_lin_neg);

  const auto eu = meam::calc_rose_embedding_energy(
      r, meam_information.erose_form, meam_information.attrac[a][b],
      meam_information.repuls[a][b], meam_information.re0[a][b],
      meam_information.alpha[a][b], meam_information.ec[a][b]);

  if (lattice == lattice::LatticeStructure::L12) {
    // just check that entering this function, we always have a < b
    if (a > b)
      throw exception::QCException{"meamc::calc_pair_potential: a < b not "
                                   "satisfied for element type indices"};
    double arat{};
    double scrn{};
    auto phiaa = calc_pair_potential<DIM>(r, a, a, meam_information);
    const auto z1 = meam::get_z(meam_information.lattice_structure[a][a]);
    const auto z2 =
        meam::get_z2_s_a_r(meam_information.lattice_structure[a][a],
                           meam_information.cmin[a][a][a],
                           meam_information.cmax[a][a][a], arat, scrn);
    const auto bnn2 = -scrn * static_cast<double>(z2) / static_cast<double>(z1);
    phiaa += phi_series_addition<DIM>(r, a, a, bnn2, arat, meam_information);
    phi = eu / 3.0 - fa / 4.0 - fb / 12.0 - phiaa;
  } else {
    phi = (2 * eu - fa - fb) / static_cast<double>(zab1);
  }
  return phi;
}

template <std::size_t DIM>
double phi_series_addition(const double r, const int a, const int b,
                           const double b2, const double arat,
                           const meam::MEAMInformation &meam_information) {
  double phi_sum = 0.0;
  double phi = 0.0;
  for (int n = 1; n <= 10; n++) {
    const auto factor = pow(b2, n);
    const auto ratio = pow(arat, n);
    phi = factor * calc_pair_potential<DIM>(r * ratio, a, b, meam_information);
    if (std::abs(phi) < 1e-10)
      break;
    phi_sum += phi;
  }
  return phi_sum;
}

template <std::size_t DIM>
double phi_and_derivative_series_addition(
    const double r, const int a, const int b, const double b2,
    const double arat, double &dphisum,
    const meam::MEAMInformation &meam_information);

/**
 * @brief Find pair potential and its derivative given distance and atom types
 */
template <std::size_t DIM>
double calc_pair_potential_and_derivative(
    const double r, const std::size_t a, const std::size_t b, double &dphi,
    const meam::MEAMInformation &meam_information) {
  const auto lattice = meam_information.lattice_structure[a][b];
  double phi{};
  double ta1_ave{};
  double ta2_ave{};
  double ta3_ave{};
  double tb1_ave{};
  double tb2_ave{};
  double tb3_ave{};
  double rho_bkgd_a{};
  double rho_bkgd_b{};
  double g_bkgd_a{};
  double g_bkgd_b{};
  double dgamma_a{};
  double dgamma_b{};
  double rho_bar_b{};
  const auto [rho_a, rho_b, drho_a, drho_b] =
      meam::get_density_and_derivative_of_ref_struct(meam_information, r, a, b);

  const auto za1 = meam::get_z(meam_information.lattice_structure[a][a]);
  const auto zb1 = meam::get_z(meam_information.lattice_structure[b][b]);
  const auto zab1 = meam::get_z(lattice);

  if (meam_information.ialloy == 2) {
    ta1_ave = meam_information.t1[a];
    ta2_ave = meam_information.t2[a];
    ta3_ave = meam_information.t3[a];
    tb1_ave = meam_information.t1[b];
    tb2_ave = meam_information.t2[b];
    tb3_ave = meam_information.t3[b];
  } else {
    ta1_ave = meam_information.t1[b];
    ta2_ave = meam_information.t2[b];
    ta3_ave = meam_information.t3[b];
    tb1_ave = meam_information.t1[a];
    tb2_ave = meam_information.t2[a];
    tb3_ave = meam_information.t3[a];
  }

  // finding background electron density
  if (meam_information.mix_ref_t == 0) {
    rho_bkgd_a = meam_information.rho_ref[a];
    rho_bkgd_b = meam_information.rho_ref[b];
  } else {
    if (meam_information.ibar[a] <= 0)
      g_bkgd_a = 1.0;
    else {
      // element a
      const auto ref_shape_factors_a = meam::get_ref_shape_factors<DIM>(
          meam_information.lattice_structure[a][a]);
      const auto gamma_bkgd_a =
          (ta1_ave * ref_shape_factors_a[0] + ta2_ave * ref_shape_factors_a[1] +
           ta3_ave * ref_shape_factors_a[2]) /
          (za1 * za1);
      g_bkgd_a = meam::calc_g(gamma_bkgd_a, meam_information.ibar[a],
                              meam_information.gsmooth_factor);
    }
    rho_bkgd_a = meam_information.rho0[a] * g_bkgd_a * static_cast<double>(za1);
    // element b
    if (meam_information.ibar[b] <= 0)
      g_bkgd_b = 1.0;
    else {
      const auto ref_shape_factors_b = meam::get_ref_shape_factors<DIM>(
          meam_information.lattice_structure[b][b]);
      const auto gamma_bkgd_b =
          (tb1_ave * ref_shape_factors_b[0] + tb2_ave * ref_shape_factors_b[1] +
           tb3_ave * ref_shape_factors_b[2]) /
          (zb1 * zb1);
      g_bkgd_b = meam::calc_g(gamma_bkgd_b, meam_information.ibar[b],
                              meam_information.gsmooth_factor);
    }
    rho_bkgd_b = meam_information.rho0[b] * g_bkgd_b * static_cast<double>(zb1);
  }

  auto gamma_a = ta1_ave * rho_a[1] + ta2_ave * rho_a[2] + ta3_ave * rho_a[3];

  if (rho_a[0] < 1e-10) {
    gamma_a = 0.0;
    dgamma_a = 0.0;
  } else {
    gamma_a /= rho_a[0] * rho_a[0];
    dgamma_a = (ta1_ave * (drho_a[1] - 2 * rho_a[1] * drho_a[0] / rho_a[0]) +
                ta2_ave * (drho_a[2] - 2 * rho_a[2] * drho_a[0] / rho_a[0]) +
                ta3_ave * (drho_a[3] - 2 * rho_a[3] * drho_a[0] / rho_a[0])) /
               (rho_a[0] * rho_a[0]);
  }
  const auto [ga, d_ga] = meam::calc_g_d_g(gamma_a, meam_information.ibar[a],
                                           meam_information.gsmooth_factor);
  const auto rho_bar_a = rho_a[0] * ga / rho_bkgd_a;
  const auto [fa, d_fa] = meam::comp_embedding_func_and_derivative(
      rho_bar_a, meam_information.a[a], meam_information.ec[a][a],
      meam_information.emb_lin_neg);

  auto gamma_b = tb1_ave * rho_b[1] + tb2_ave * rho_b[2] + tb3_ave * rho_b[3];
  if (rho_b[0] < 1e-10) {
    gamma_b = 0.0;
    dgamma_b = 0.0;
  } else {
    gamma_b /= rho_b[0] * rho_b[0];
    dgamma_b = (tb1_ave * (drho_b[1] - 2 * rho_b[1] * drho_b[0] / rho_b[0]) +
                tb2_ave * (drho_b[2] - 2 * rho_b[2] * drho_b[0] / rho_b[0]) +
                tb3_ave * (drho_b[3] - 2 * rho_b[3] * drho_b[0] / rho_b[0])) /
               (rho_b[0] * rho_b[0]);
  }
  const auto [gb, d_gb] = meam::calc_g_d_g(gamma_b, meam_information.ibar[b],
                                           meam_information.gsmooth_factor);
  rho_bar_b = rho_b[0] * gb / rho_bkgd_b;
  const auto [fb, d_fb] = meam::comp_embedding_func_and_derivative(
      rho_bar_b, meam_information.a[b], meam_information.ec[b][b],
      meam_information.emb_lin_neg);

  const auto [eu, d_eu] = meam::calc_rose_embedding_energy_and_derivative(
      r, meam_information.erose_form, meam_information.attrac[a][b],
      meam_information.repuls[a][b], meam_information.re0[a][b],
      meam_information.alpha[a][b], meam_information.ec[a][b]);

  if (lattice == lattice::LatticeStructure::L12) {
    // just check that entering this function, we always have a < b
    if (a > b)
      throw exception::QCException{"meamc::calc_pair_potential: a < b not "
                                   "satisfied for element type indices"};
    double phiaa{};
    double arat{};
    double scrn{};
    double dphiaa{};
    double dphiaa_sum{};
    phiaa = calc_pair_potential_and_derivative<DIM>(r, a, a, dphiaa,
                                                    meam_information);
    const auto z1 = meam::get_z(meam_information.lattice_structure[a][a]);
    const auto z2 =
        meam::get_z2_s_a_r(meam_information.lattice_structure[a][a],
                           meam_information.cmin[a][a][a],
                           meam_information.cmax[a][a][a], arat, scrn);
    const auto bnn2 = -scrn * static_cast<double>(z2) / static_cast<double>(z1);
    phiaa += phi_and_derivative_series_addition<DIM>(
        r, a, a, bnn2, arat, dphiaa_sum, meam_information);
    dphiaa += dphiaa_sum;
    phi = eu / 3.0 - fa / 4.0 - fb / 12.0 - phiaa;
    dphi = d_eu / 3.0 -
           (d_fa * (drho_a[0] * ga + rho_a[0] * d_ga * dgamma_a) / rho_bkgd_a) /
               4.0 -
           (d_fb * (drho_b[0] * gb + rho_b[0] * d_gb * dgamma_b) / rho_bkgd_b) /
               12.0 -
           dphiaa;
  } else {
    phi = (2 * eu - fa - fb) / static_cast<double>(zab1);
    dphi =
        (2 * d_eu -
         (d_fa * (drho_a[0] * ga + rho_a[0] * d_ga * dgamma_a) / rho_bkgd_a) -
         (d_fb * (drho_b[0] * gb + rho_b[0] * d_gb * dgamma_b) / rho_bkgd_b)) /
        static_cast<double>(zab1);
  }
  return phi;
}

template <std::size_t DIM>
double phi_and_derivative_series_addition(
    const double r, const int a, const int b, const double b2,
    const double arat, double &dphisum,
    const meam::MEAMInformation &meam_information) {
  dphisum = 0.0;
  double phi_sum = 0.0;
  double dphi = 0.0;
  for (int n = 1; n <= 10; n++) {
    const auto factor = pow(b2, n);
    const auto ratio = pow(arat, n);
    const auto phi = factor * calc_pair_potential_and_derivative<DIM>(
                                  r * ratio, a, b, dphi, meam_information);
    dphi = factor * ratio * dphi;
    if (abs(phi) < 1e-10)
      break;
    phi_sum += phi;
    dphisum += dphi;
  }
  return phi_sum;
}

template <std::size_t DIM>
double
calc_total_pair_potential(const double r_ij, const int a, const int b,
                          const meam::MEAMInformation &meam_information) {
  double phi_one_pair = 0.0; // reset the phi value for the new pair
  double arat{};
  double scrn{};
  phi_one_pair += calc_pair_potential<DIM>(
      r_ij, a, b,
      meam_information); // the 2nd and third inputs are for atom types
  if (meam_information.nn2[a][b] == 1) {
    const auto z1_ab = meam::get_z(meam_information.lattice_structure[a][b]);
    const auto z2_ab =
        meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                           meam_information.cmin[a][a][b],
                           meam_information.cmax[a][a][b], arat, scrn);
    if (meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::B1 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::B2 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::L12 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::Diamond) {

      // phi_aa
      const auto r_times_arat = r_ij * arat;
      auto phiaa =
          calc_pair_potential<DIM>(r_times_arat, a, a, meam_information);
      {
        const auto z1 = meam::get_z(meam_information.lattice_structure[a][a]);
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[a][a],
                               meam_information.cmin[a][a][a],
                               meam_information.cmax[a][a][a], arat, scrn);
        const auto bnn2 =
            -scrn * static_cast<double>(z2) / static_cast<double>(z1);
        phiaa += phi_series_addition<DIM>(r_times_arat, a, a, bnn2, arat,
                                          meam_information);
      }

      // phi_bb
      auto phibb =
          calc_pair_potential<DIM>(r_times_arat, b, b, meam_information);
      {
        const auto z1 = meam::get_z(meam_information.lattice_structure[b][b]);
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[b][b],
                               meam_information.cmin[b][b][b],
                               meam_information.cmax[b][b][b], arat, scrn);
        const auto bnn2 =
            -scrn * static_cast<double>(z2) / static_cast<double>(z1);
        phibb += phi_series_addition<DIM>(r_times_arat, b, b, bnn2, arat,
                                          meam_information);
      }

      if (meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::B1 ||
          meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::B2 ||
          meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::Diamond) {
        {
          const auto z1 = meam::get_z(meam_information.lattice_structure[a][b]);
          const auto z2 =
              meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                                 meam_information.cmin[a][a][b],
                                 meam_information.cmax[a][a][b], arat, scrn);
          phi_one_pair -= scrn * static_cast<double>(z2) /
                          static_cast<double>(2 * z1) * phiaa;
        }
        const auto z1 = meam::get_z(meam_information.lattice_structure[a][b]);
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                               meam_information.cmin[b][b][a],
                               meam_information.cmax[b][b][a], arat, scrn);
        phi_one_pair -= scrn * static_cast<double>(z2) /
                        static_cast<double>(2 * z1) * phibb;
      } else if (meam_information.lattice_structure[a][b] ==
                 lattice::LatticeStructure::L12) {
        // check out the L12 crystal structure to understand this
        const auto c = 1.0;
        const auto s_aaa = meam::f_cut(
            (c - meam_information.cmin[a][a][a]) /
            (meam_information.cmax[a][a][a] - meam_information.cmin[a][a][a]));
        const auto s_aab = meam::f_cut(
            (c - meam_information.cmin[a][a][b]) /
            (meam_information.cmax[a][a][b] - meam_information.cmin[a][a][b]));
        const auto s_bba = meam::f_cut(
            (c - meam_information.cmin[b][b][a]) /
            (meam_information.cmax[b][b][a] - meam_information.cmin[b][b][a]));
        const auto s_aa = s_aaa * s_aaa * s_aab * s_aab;
        const auto s_bb = s_bba * s_bba * s_bba * s_bba;
        phi_one_pair -= (0.75 * s_aa * phiaa + 0.25 * s_bb * phibb);
      }
    } else {
      const auto bnn2 =
          -scrn * static_cast<double>(z2_ab) / static_cast<double>(z1_ab);
      phi_one_pair +=
          phi_series_addition<DIM>(r_ij, a, b, bnn2, arat, meam_information);
    }
  }

  if (meam_information.zbl[a][b] == 1) {
    const auto astar = meam_information.alpha[a][b] *
                       (r_ij / meam_information.re0[a][b] - 1.0);
    if (astar <= -3.0)
      phi_one_pair =
          meam::zbl_potential(r_ij, meam_information.atomic_number[a],
                              meam_information.atomic_number[b]);
    else if (astar > -3.0 && astar < -1.0) {
      const auto frac = meam::f_cut(1 - (astar + 1.0) / (-3.0 + 1.0));
      const auto phizbl =
          meam::zbl_potential(r_ij, meam_information.atomic_number[a],
                              meam_information.atomic_number[b]);
      phi_one_pair = frac * phi_one_pair + (1 - frac) * phizbl;
    }
  }

  return phi_one_pair;
}

template <std::size_t DIM>
double calc_total_pair_potential_and_derivative(
    const double r_ij, const int a, const int b, double &dphi_one_pair,
    const meam::MEAMInformation &meam_information) {
  double phi_one_pair = 0.0; // reset the phi value for the new pair
  dphi_one_pair = 0.0;
  double arat{};
  double scrn{};
  double dphiaa{};
  double dphibb{};
  double dphisum{};

  phi_one_pair += calc_pair_potential_and_derivative<DIM>(
      r_ij, a, b, dphi_one_pair,
      meam_information); // the 2nd and third inputs are for atom types

  if (meam_information.nn2[a][b] == 1) {
    const auto z1_ab = meam::get_z(meam_information.lattice_structure[a][b]);
    const auto z2_ab =
        meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                           meam_information.cmin[a][a][b],
                           meam_information.cmax[a][a][b], arat, scrn);
    if (meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::B1 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::B2 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::L12 ||
        meam_information.lattice_structure[a][b] ==
            lattice::LatticeStructure::Diamond) {
      // phi_aa
      const auto r_times_arat = r_ij * arat;
      auto phiaa = calc_pair_potential_and_derivative<DIM>(
          r_times_arat, a, a, dphiaa, meam_information);
      {
        const auto z1 = meam::get_z(meam_information.lattice_structure[a][a]);
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[a][a],
                               meam_information.cmin[a][a][a],
                               meam_information.cmax[a][a][a], arat, scrn);
        const auto bnn2 =
            -scrn * static_cast<double>(z2) / static_cast<double>(z1);
        phiaa += phi_and_derivative_series_addition<DIM>(
            r_times_arat, a, a, bnn2, arat, dphisum, meam_information);
      }
      dphiaa += dphisum;

      // phi_bb
      auto phibb = calc_pair_potential_and_derivative<DIM>(
          r_times_arat, b, b, dphibb, meam_information);
      const auto z1 = meam::get_z(meam_information.lattice_structure[b][b]);
      const auto z2 =
          meam::get_z2_s_a_r(meam_information.lattice_structure[b][b],
                             meam_information.cmin[b][b][b],
                             meam_information.cmax[b][b][b], arat, scrn);
      const auto bnn2 =
          -scrn * static_cast<double>(z2) / static_cast<double>(z1);
      phibb += phi_and_derivative_series_addition<DIM>(
          r_times_arat, b, b, bnn2, arat, dphisum, meam_information);
      dphibb += dphisum;

      if (meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::B1 ||
          meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::B2 ||
          meam_information.lattice_structure[a][b] ==
              lattice::LatticeStructure::Diamond) {
        {
          const auto z1 = meam::get_z(meam_information.lattice_structure[a][b]);
          const auto z2 =
              meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                                 meam_information.cmin[a][a][b],
                                 meam_information.cmax[a][a][b], arat, scrn);
          phi_one_pair -= scrn * static_cast<double>(z2) /
                          static_cast<double>(2 * z1) * phiaa;
          dphi_one_pair -= scrn * static_cast<double>(z2) /
                           static_cast<double>(2 * z1) * dphiaa;
        }

        const auto z1 = meam::get_z(meam_information.lattice_structure[a][b]);
        const auto z2 =
            meam::get_z2_s_a_r(meam_information.lattice_structure[a][b],
                               meam_information.cmin[b][b][a],
                               meam_information.cmax[b][b][a], arat, scrn);
        phi_one_pair -= scrn * static_cast<double>(z2) /
                        static_cast<double>(2 * z1) * phibb;
        dphi_one_pair -= scrn * static_cast<double>(z2) /
                         static_cast<double>(2 * z1) * dphibb;
      } else if (meam_information.lattice_structure[a][b] ==
                 lattice::LatticeStructure::L12) {
        // check out the L12 crystal structure to understand this
        const auto c = 1.0;
        const auto s_aaa = meam::f_cut(
            (c - meam_information.cmin[a][a][a]) /
            (meam_information.cmax[a][a][a] - meam_information.cmin[a][a][a]));
        const auto s_aab = meam::f_cut(
            (c - meam_information.cmin[a][a][b]) /
            (meam_information.cmax[a][a][b] - meam_information.cmin[a][a][b]));
        const auto s_bba = meam::f_cut(
            (c - meam_information.cmin[b][b][a]) /
            (meam_information.cmax[b][b][a] - meam_information.cmin[b][b][a]));
        const auto s_aa = s_aaa * s_aaa * s_aab * s_aab;
        const auto s_bb = s_bba * s_bba * s_bba * s_bba;
        phi_one_pair -= (0.75 * s_aa * phiaa + 0.25 * s_bb * phibb);
        dphi_one_pair -= (0.75 * s_aa * dphiaa + 0.25 * s_bb * dphibb);
      }
    } else {
      const auto bnn2 =
          -scrn * static_cast<double>(z2_ab) / static_cast<double>(z1_ab);
      phi_one_pair += phi_and_derivative_series_addition<DIM>(
          r_ij, a, b, bnn2, arat, dphisum, meam_information);
      dphi_one_pair += dphisum;
    }
  }

  if (meam_information.zbl[a][b] == 1) {
    const auto astar = meam_information.alpha[a][b] *
                       (r_ij / meam_information.re0[a][b] - 1.0);
    if (astar <= -3.0)
      std::tie(phi_one_pair, dphi_one_pair) =
          meam::zbl_potential_and_derivative(r_ij,
                                             meam_information.atomic_number[a],
                                             meam_information.atomic_number[b]);
    else if (astar > -3.0 && astar < -1.0) {
      const auto frac = meam::f_cut(1 - (astar + 1.0) / (-3.0 + 1.0));
      const auto dfrac =
          meam::df_cut(1 - (astar + 1.0) / (-3.0 + 1.0)) *
          (meam_information.alpha[a][b] / (2 * meam_information.re0[a][b]));
      const auto [phizbl, dphizbl] = meam::zbl_potential_and_derivative(
          r_ij, meam_information.atomic_number[a],
          meam_information.atomic_number[b]);
      dphi_one_pair = frac * dphi_one_pair + dfrac * phi_one_pair +
                      (1 - frac) * dphizbl - dfrac * phizbl;
      phi_one_pair = frac * phi_one_pair + (1 - frac) * phizbl;
    }
  }

  return phi_one_pair;
}
} // namespace materials::meam
