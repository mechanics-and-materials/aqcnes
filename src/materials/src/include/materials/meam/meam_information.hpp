// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author S. Saxena
 */

#pragma once

#include "lattice/lattice_structure.hpp"
#include <vector>

namespace materials::meam {

struct MEAMInformation {
  std::vector<double> a{}, beta0{}, beta1{}, beta2{}, beta3{}, t0{}, t1{}, t2{},
      t3{}, rho0{}, atomic_mass{}, rho_ref{};
  std::vector<std::vector<double>> alpha{}, re0{}, ec{}, delta{}, alat{},
      ebound{};
  std::vector<std::vector<std::vector<double>>> cmin{}, cmax{};
  std::vector<int> ibar{}, atomic_number{}, z{};
  std::vector<std::vector<int>> nn2{}, zbl{};
  std::vector<std::vector<double>> attrac{}, repuls{};
  std::vector<std::vector<lattice::LatticeStructure>> lattice_structure{};
  double rc{}, delr{}, gsmooth_factor{};
  int erose_form = 0;
  int augt1 = 1;
  int mix_ref_t = 0;
  int ialloy = 0;
  int emb_lin_neg = 0;
  int bkgd_dyn = 0;
};

} // namespace materials::meam
