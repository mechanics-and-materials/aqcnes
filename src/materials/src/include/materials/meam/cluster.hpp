// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors S. Saxena, M. Spinola
 */

#pragma once
#include "materials/meam/meam_information.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include <vector>

namespace materials::meam {

static constexpr std::array<std::size_t, 6> V2{
    1, 2, 2, 1, 2, 1}; // coefficients to simplify tensors to vectors
static constexpr std::array<std::size_t, 10> V3{1, 3, 3, 3, 6, 3, 1, 3, 3, 1};

/**
 * @brief calculate average t values for all atoms.
 */
template <std::size_t Noi>
void get_concentration_averaged_t_values(
    const std::vector<meshing::NodalDataMultispecies<Noi>> &data_point_neighbor,
    const std::vector<double> &conc_sum,
    std::vector<std::array<double, 3>> &conc_averaged_t_values,
    const meam::MEAMInformation &meam_information, const std::size_t start_idx,
    const std::size_t idx_offset) {
  conc_averaged_t_values.clear();
  conc_averaged_t_values.resize(data_point_neighbor.size(), {});
  for (std::size_t j = 0; j < data_point_neighbor.size(); j++) {
    for (std::size_t m = start_idx; m < Noi + 1; m++) {
      auto concentration =
          (m < Noi) ? data_point_neighbor[j].impurity_concentrations[m]
                    : (1 - conc_sum[j]);

      conc_averaged_t_values[j][0] +=
          meam_information.t1[m - idx_offset] * concentration;
      conc_averaged_t_values[j][1] +=
          meam_information.t2[m - idx_offset] * concentration;
      conc_averaged_t_values[j][2] +=
          meam_information.t3[m - idx_offset] * concentration;
    } // concentration loop
    if (start_idx == 1) {
      conc_averaged_t_values[j][0] =
          conc_averaged_t_values[j][0] /
          (1 - data_point_neighbor[j].impurity_concentrations[0]);
      conc_averaged_t_values[j][1] =
          conc_averaged_t_values[j][1] /
          (1 - data_point_neighbor[j].impurity_concentrations[0]);
      conc_averaged_t_values[j][2] =
          conc_averaged_t_values[j][2] /
          (1 - data_point_neighbor[j].impurity_concentrations[0]);
    }
  } // loop over j
}
} // namespace materials::meam
