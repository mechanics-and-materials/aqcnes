// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to compute concentation interpolated screening functions and derivatives
 *        for the modified embedded atom method (MEAM) potential
 * @author S. Saxena, M. Spinola
 */

#pragma once
#include "materials/meam/low_level_routines.hpp"
#include "materials/meam/meam_information.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "qcmesh/array_ops/array_ops.hpp"

namespace materials::meam {

/**
 * @brief calculate and store screening factors for all atom pairs
 */
template <std::size_t DIM, std::size_t Noi>
void compute_screening(
    const std::array<double, DIM> &atom,
    const std::vector<std::array<double, DIM>> &neighbors,
    const std::vector<meshing::NodalDataMultispecies<Noi>> &data_point_neighbor,
    const std::vector<double> &total_solute_concs,
    const meam::MEAMInformation &meam_information, const std::size_t start_idx,
    const std::size_t idx_offset,
    std::vector<std::vector<std::vector<double>>>
        &scr_fncs, // vector of matrices
    std::vector<double> &cutoff_scr_fncs) {
  namespace array_ops = qcmesh::array_ops;
  const auto noof_elements = meam_information.a.size();
  scr_fncs.clear();
  cutoff_scr_fncs.clear();
  scr_fncs.resize(neighbors.size(),
                  std::vector<std::vector<double>>(
                      noof_elements, std::vector<double>(noof_elements, 1.0)));
  cutoff_scr_fncs.resize(neighbors.size(), 0.0);

  for (std::size_t j = 0; j < neighbors.size(); j++) {
    const auto neighbor = neighbors[j];
    const auto r_ij = array_ops::distance(atom, neighbor);
    if (r_ij > meam_information.rc) {
      continue;
    }
    // loop for calculating screening factors
    for (std::size_t k = 0; k < neighbors.size(); k++) {
      if (k == j)
        continue;
      const auto screening_atom = neighbors[k];
      const auto r_jk = array_ops::distance(neighbor, screening_atom);
      const auto r_ik = array_ops::distance(atom, screening_atom);

      // geometrically if k is aoutside any ellipse passing through i and j, then continue
      const auto xik = (r_ik * r_ik) / (r_ij * r_ij);
      const auto xjk = (r_jk * r_jk) / (r_ij * r_ij);
      const auto dist_ratio_sq = 1 - (xik - xjk) * (xik - xjk);
      if (dist_ratio_sq <= 0.0)
        continue;

      // for every neighbor, find the screening factors for all possible combinations of elements at i and j
      for (std::size_t a = 0; a < noof_elements; a++) {
        for (std::size_t b = a; b < noof_elements; b++) {
          if (scr_fncs[j][a][b] > 1e-10) {
            auto s_ijk =
                (start_idx == 0)
                    ? 0.0
                    : data_point_neighbor[k].impurity_concentrations[0];
            // for every species combination, interpolate over the concs of the k-th atom
            for (std::size_t c = start_idx; c < Noi + 1; c++) {
              const auto r_bound_ab = meam_information.ebound[a][b] * r_ij;
              if (r_jk > r_bound_ab || r_ik > r_bound_ab) {
                s_ijk += (c < Noi)
                             ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]);
                continue;
              }
              const auto c_ikj =
                  (2.0 * (xik + xjk) + dist_ratio_sq - 2.0) / dist_ratio_sq;
              const auto cmin = meam_information.cmin[a][b][c - idx_offset];
              const auto cmax = meam_information.cmax[a][b][c - idx_offset];
              if (c_ikj < cmin) {
                s_ijk += 0.0;
                continue;
              }
              const auto x = (c_ikj - cmin) / (cmax - cmin);
              s_ijk +=
                  meam::f_cut(x) *
                  ((c < Noi) ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]));
            } // for loop over species of k
            scr_fncs[j][a][b] *= s_ijk;
            if (a != b)
              scr_fncs[j][b][a] *= s_ijk;
          }
        } // for loop over species of j
      }   // for loop over species of i
    }     // loop over k

    const auto r_norm = (meam_information.rc - r_ij) / meam_information.delr;
    cutoff_scr_fncs[j] = meam::f_cut(r_norm);
  }
}

/**
 * @brief calculate and store screening factors and their derivatives for all atom pairs
 */
template <std::size_t DIM, std::size_t Noi>
void compute_screening_and_derivatives(
    const std::vector<double> &neighbor_distances,
    const std::vector<std::vector<double>> &inter_neighbor_distances,
    const std::vector<std::array<double, DIM>> &relative_neighbor_unit_vectors,
    const std::vector<std::vector<std::array<double, DIM>>>
        &relative_inter_neighbor_unit_vectors,
    const std::vector<meshing::NodalDataMultispecies<Noi>> &data_point_neighbor,
    const std::vector<double> &total_solute_concs,
    const meam::MEAMInformation &meam_information, const std::size_t start_idx,
    const std::size_t idx_offset,
    std::vector<std::vector<std::vector<double>>> &scr_fncs,
    std::vector<double> &cutoff_scr_fncs,
    std::vector<std::vector<std::vector<std::array<double, DIM>>>>
        &dscr_fncs_drj,
    std::vector<std::vector<std::vector<std::vector<std::array<double, DIM>>>>>
        &dscr_fncs_drk,
    std::vector<double> &dcutoff_scr_fncs_drij) {
  namespace array_ops = qcmesh::array_ops;
  const auto noof_elements = meam_information.a.size();
  std::vector<std::vector<std::vector<double>>> sikjs;
  scr_fncs.clear();
  cutoff_scr_fncs.clear();
  scr_fncs.resize(neighbor_distances.size(),
                  std::vector<std::vector<double>>(
                      noof_elements, std::vector<double>(noof_elements, 1.0)));
  cutoff_scr_fncs.resize(neighbor_distances.size(), 0.0);

  using Point = std::array<double, DIM>;
  dscr_fncs_drj.clear();
  dscr_fncs_drj.resize(
      neighbor_distances.size(),
      std::vector<std::vector<Point>>(
          noof_elements, std::vector<Point>(noof_elements, Point{})));

  dcutoff_scr_fncs_drij.clear();
  dcutoff_scr_fncs_drij.resize(neighbor_distances.size(), 0.0);

  dscr_fncs_drk.clear();
  dscr_fncs_drk.resize(
      neighbor_distances.size(),
      std::vector<std::vector<std::vector<Point>>>(
          neighbor_distances.size(),
          std::vector<std::vector<Point>>(
              noof_elements, std::vector<Point>(noof_elements, Point{}))));

  for (std::size_t j = 0; j < neighbor_distances.size(); j++) {
    sikjs.resize(neighbor_distances.size(),
                 std::vector<std::vector<double>>(
                     noof_elements, std::vector<double>(noof_elements, 1.0)));
    const auto r_ij = neighbor_distances[j];
    if (r_ij > meam_information.rc) {
      continue;
    }

    // loop for calculating screening factors
    for (std::size_t k = 0; k < neighbor_distances.size(); k++) {
      if (k == j)
        continue;

      const auto r_jk = inter_neighbor_distances[j][k];
      const auto r_ik = neighbor_distances[k];

      // geometrically if k is aoutside any ellipse passing through i and j, then continue
      const auto xik = (r_ik * r_ik) / (r_ij * r_ij);
      const auto xjk = (r_jk * r_jk) / (r_ij * r_ij);
      const auto dist_ratio_sq = 1 - (xik - xjk) * (xik - xjk);
      if (dist_ratio_sq <= 0.0)
        continue;
      const auto c_ikj =
          (2.0 * (xik + xjk) + dist_ratio_sq - 2.0) / dist_ratio_sq;

      // for every neighbor, find the screening factors for all possible combinations of elements at i and j
      for (std::size_t a = 0; a < noof_elements; a++) {
        for (std::size_t b = a; b < noof_elements; b++) {
          if (scr_fncs[j][a][b] > 1e-8) {
            const auto r_bound_ab = meam_information.ebound[a][b] * r_ij;
            if (r_jk > r_bound_ab || r_ik > r_bound_ab) {
              continue;
            }
            sikjs[k][a][b] =
                (start_idx == 0)
                    ? 0.0
                    : data_point_neighbor[k].impurity_concentrations[0];

            // for every species combination, interpolate over the concs of the k-th atom
            for (std::size_t c = start_idx; c < Noi + 1; c++) {
              const auto cmin = meam_information.cmin[a][b][c - idx_offset];
              const auto cmax = meam_information.cmax[a][b][c - idx_offset];
              if (c_ikj < cmin) {
                sikjs[k][a][b] += 0.0;
                continue;
              }
              const auto x = (c_ikj - cmin) / (cmax - cmin);
              sikjs[k][a][b] +=
                  meam::f_cut(x) *
                  ((c < Noi) ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]));
            } // for loop over species of k
            scr_fncs[j][a][b] *= sikjs[k][a][b];
            if (a != b)
              scr_fncs[j][b][a] *= sikjs[k][a][b];
          }
        } // for loop over species of j
      }   // for loop over species of i
    }     // loop over k

    const auto r_norm = (meam_information.rc - r_ij) / meam_information.delr;
    cutoff_scr_fncs[j] = meam::f_cut(r_norm);

    // loop for calculating derivatives
    for (std::size_t k = 0; k < neighbor_distances.size(); k++) {
      if (k == j)
        continue;

      const auto r_jk = inter_neighbor_distances[j][k];
      const auto r_ik = neighbor_distances[k];

      const auto xik = (r_ik * r_ik) / (r_ij * r_ij);
      const auto xjk = (r_jk * r_jk) / (r_ij * r_ij);
      const auto dist_ratio_sq = 1 - (xik - xjk) * (xik - xjk);
      if (dist_ratio_sq <= 0.0)
        continue;
      const auto c_ikj =
          (2.0 * (xik + xjk) + dist_ratio_sq - 2.0) / dist_ratio_sq;
      const auto [d_c_ikj_ij, d_c_ikj_ik, d_c_ikj_jk] =
          meam::d_cikj(r_ij, r_ik, r_jk);

      for (std::size_t a = 0; a < noof_elements; a++) {
        for (std::size_t b = a; b < noof_elements; b++) {
          if (sikjs[k][a][b] > (1.0 - 1e-8) || sikjs[k][a][b] < 1e-8)
            continue;
          if (scr_fncs[j][a][b] > 1e-8) {
            const auto r_bound_ab = meam_information.ebound[a][b] * r_ij;
            if (r_jk > r_bound_ab || r_ik > r_bound_ab) {
              continue;
            }

            auto d_sikj_drij = 0.0;
            auto d_sikj_drik = 0.0;
            auto d_sikj_drjk = 0.0;
            for (std::size_t c = start_idx; c < Noi + 1; c++) {
              const auto cmin = meam_information.cmin[a][b][c - idx_offset];
              const auto cmax = meam_information.cmax[a][b][c - idx_offset];

              const auto x = (c_ikj - cmin) / (cmax - cmin);
              const auto df_ikj = meam::df_cut(x);

              d_sikj_drij +=
                  (scr_fncs[j][a][b] * df_ikj * d_c_ikj_ij /
                   ((cmax - cmin) * sikjs[k][a][b])) *
                  ((c < Noi) ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]));

              d_sikj_drik +=
                  (scr_fncs[j][a][b] * df_ikj * d_c_ikj_ik /
                   ((cmax - cmin) * sikjs[k][a][b])) *
                  ((c < Noi) ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]));

              d_sikj_drjk +=
                  (scr_fncs[j][a][b] * df_ikj * d_c_ikj_jk /
                   ((cmax - cmin) * sikjs[k][a][b])) *
                  ((c < Noi) ? data_point_neighbor[k].impurity_concentrations[c]
                             : (1.0 - total_solute_concs[k]));

            } // for loop over species of k

            array_ops::as_eigen(dscr_fncs_drj[j][a][b]) +=
                d_sikj_drij *
                    array_ops::as_eigen(relative_neighbor_unit_vectors[j]) +
                d_sikj_drjk * array_ops::as_eigen(
                                  relative_inter_neighbor_unit_vectors[j][k]);
            array_ops::as_eigen(dscr_fncs_drj[j][b][a]) =
                array_ops::as_eigen(dscr_fncs_drj[j][a][b]);

            array_ops::as_eigen(dscr_fncs_drk[j][k][a][b]) =
                d_sikj_drjk * array_ops::as_eigen(
                                  relative_inter_neighbor_unit_vectors[k][j]) +
                d_sikj_drik *
                    array_ops::as_eigen(relative_neighbor_unit_vectors[k]);

            array_ops::as_eigen(dscr_fncs_drk[j][k][b][a]) =
                array_ops::as_eigen(dscr_fncs_drk[j][k][a][b]);

          } // if statement

        } // for loop over species of j
      }   // for loop over species of i
    }     // loop over k
    if (cutoff_scr_fncs[j] < 1e-8 || cutoff_scr_fncs[j] > (1.0 - 1e-8))
      continue;
    dcutoff_scr_fncs_drij[j] = -meam::df_cut(r_norm) / (meam_information.delr);

  } // loop over j
} // end of function
} // namespace materials::meam
