// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to compute concentation interpolated energy
 *        using the modified embedded atom method (MEAM) potential
 * @authors S. Saxena, A. Wang, M. Spinola
 */

#pragma once
#include "exception/qc_exception.hpp"
#include "materials/meam/atomic_elec_densities_and_derivatives.hpp"
#include "materials/meam/cluster.hpp"
#include "materials/meam/low_level_routines.hpp"
#include "materials/meam/meam_information.hpp"
#include "materials/meam/pair_potential_and_derivatives.hpp"
#include "materials/meam/reference_structure_calculations.hpp"
#include "materials/meam/screening_and_derivatives.hpp"
#include "utils/evaluate_polynomial.hpp"
#include <fstream>

namespace materials::meam {

/**
 * @brief embedding function for electron density
 */
template <std::size_t Noi>
double comp_embedding_func_with_interpolation(
    const double solvent_ctr, const std::vector<double> &rhob_vec,
    const std::vector<double> &a_vec,
    const std::vector<std::vector<double>> &ec_vec,
    const meshing::NodalDataMultispecies<Noi> &data_point_atom,
    const std::vector<std::vector<double>>
        &embedding_interpolating_polynomial_coeffs,
    const std::size_t start_idx, const std::size_t idx_offset,
    const int emb_lin_neg, const bool output_potential_log) {
  std::ofstream f_potential_log;
  if (output_potential_log)
    f_potential_log.open("./potential_log.dat", std::ios::app);

  auto f = 0.0;

  for (std::size_t g = start_idx; g < Noi + 1; g++) {
    const auto embedding_value =
        rhob_vec[g - idx_offset] > 0.0
            ? (a_vec[g - idx_offset] * ec_vec[g - idx_offset][g - idx_offset] *
               rhob_vec[g - idx_offset] * log(rhob_vec[g - idx_offset]))
            : (emb_lin_neg == 0 ? 0.0
                                : (-a_vec[g - idx_offset] *
                                   ec_vec[g - idx_offset][g - idx_offset] *
                                   rhob_vec[g - idx_offset]));

    f += embedding_value *
         (g != Noi
              ? utils::evaluate_polynomial(
                    embedding_interpolating_polynomial_coeffs[g - start_idx],
                    data_point_atom.impurity_concentrations[g])
              : utils::evaluate_polynomial(
                    embedding_interpolating_polynomial_coeffs[g - start_idx],
                    solvent_ctr));

    if (output_potential_log)
      f_potential_log << embedding_value << " ";
  }

  if (output_potential_log)
    f_potential_log.close();

  return f;
}

template <std::size_t DIM, std::size_t Noi>
std::tuple<double, double> cluster_energy(
    const std::array<double, DIM> &atom,
    const std::vector<std::array<double, DIM>> &neighbors,
    const meshing::NodalDataMultispecies<Noi> &data_point_atom, // the atom
    const std::vector<meshing::NodalDataMultispecies<Noi>> &data_point_neighbor,
    const std::vector<std::vector<double>>
        &embedding_interpolating_polynomial_coeffs,
    const meam::MEAMInformation &meam_information, const std::size_t start_idx,
    const std::size_t idx_offset) // the neighbors
{

  namespace array_ops = qcmesh::array_ops;

  double u = 0.0;
  std::vector<std::vector<std::vector<double>>> s_values;
  std::vector<double> rho_bar_vec;
  std::vector<double> cutoff_s_values;
  std::vector<std::array<double, 3>> conc_averaged_t_values;

  std::array<double, 3> rho1_vec{};
  std::array<double, 3> rho3_sub_vec{};
  std::array<double, 6> rho2_vec{};
  std::array<double, 10> rho3_vec{};

  // find total solute concs at the central atom and all neighbors
  auto conc_sum_i = 0.0;
  std::vector<double> conc_sum_j = std::vector<double>(neighbors.size(), 0.0);
  for (std::size_t k = 0; k < Noi; k++) {
    conc_sum_i += data_point_atom.impurity_concentrations[k];
    for (std::size_t j = 0; j < neighbors.size(); j++) {
      conc_sum_j[j] += data_point_neighbor[j].impurity_concentrations[k];
    }
  }

  // get t values at each neighbor
  get_concentration_averaged_t_values(data_point_neighbor, conc_sum_j,
                                      conc_averaged_t_values, meam_information,
                                      start_idx, idx_offset);

  // First compute all screening factors
  meam::compute_screening(atom, neighbors, data_point_neighbor, conc_sum_j,
                          meam_information, start_idx, idx_offset, s_values,
                          cutoff_s_values);

  // interpolation loops to get rho_bar and Ec
  const auto noof_elements = meam_information.a.size();
  for (std::size_t i_g = 0; i_g < noof_elements; i_g++) {

    const auto t1_i = meam_information.t1[i_g];
    const auto t2_i = meam_information.t2[i_g];
    const auto t3_i = meam_information.t3[i_g];
    const auto ro_0 = meam_information.rho0[i_g];
    const auto ibar = meam_information.ibar[i_g];

    // reset these vectors
    rho1_vec.fill(0.0);
    rho2_vec.fill(0.0);
    rho3_vec.fill(0.0);
    rho3_sub_vec.fill(0.0);

    auto t1_ave = 0.0;
    auto t2_ave = 0.0;
    auto t3_ave = 0.0;
    auto t1sq_ave = 0.0;
    auto t2sq_ave = 0.0;
    auto t3sq_ave = 0.0;

    auto rho0 = 0.0;
    auto rho2_sub = 0.0;

    // compute the total electron density at sampling atom
    for (std::size_t j = 0; j < neighbors.size(); j++) {
      if (*std::max_element(s_values[j][i_g].begin(), s_values[j][i_g].end()) <
          1e-6)
        continue;
      const auto neighbor = neighbors[j];
      std::array<double, 3> r_ij_vec{};
      array_ops::as_eigen(r_ij_vec) =
          array_ops::as_eigen(neighbor) - array_ops::as_eigen(atom);
      const auto r_ij = array_ops::norm(r_ij_vec);
      if (r_ij > meam_information.rc)
        continue;
      if (r_ij < 1e-6)
        throw exception::QCException{"Coinciding atoms detected"};

      double rhoa0{};
      double rhoa1{};
      double rhoa2{};
      double rhoa3{};
      AtomicElectronDensity<DIM, Noi>{meam_information, start_idx, idx_offset}
          .get_neighbor_atomic_electron_density(
              rhoa0, rhoa1, rhoa2, rhoa3, r_ij, data_point_neighbor[j],
              conc_sum_j[j],
              s_values
                  [j]
                  [i_g]); // S_values[j][i_g] is a vector of all possible species
                          // of the current neighbor

      rhoa0 *= cutoff_s_values[j];
      rhoa1 *= cutoff_s_values[j];
      rhoa2 *= cutoff_s_values[j];
      rhoa3 *= cutoff_s_values[j];

      // alternative averaging (matches ialloy=1 in DYNAMO)
      if (meam_information.ialloy == 1) {
        rhoa1 *= conc_averaged_t_values[j][0];
        rhoa2 *= conc_averaged_t_values[j][1];
        rhoa3 *= conc_averaged_t_values[j][2];
        t1sq_ave +=
            conc_averaged_t_values[j][0] * conc_averaged_t_values[j][0] * rhoa0;
        t2sq_ave +=
            conc_averaged_t_values[j][1] * conc_averaged_t_values[j][1] * rhoa0;
        t3sq_ave +=
            conc_averaged_t_values[j][2] * conc_averaged_t_values[j][2] * rhoa0;
      }

      rho0 += rhoa0;
      rho2_sub += rhoa2;

      // if == 2: no averaging of t (use single-element values), so in this case
      // some interpolation is done
      if (meam_information.ialloy != 2) {
        t1_ave += conc_averaged_t_values[j][0] * rhoa0;
        t2_ave += conc_averaged_t_values[j][1] * rhoa0;
        t3_ave += conc_averaged_t_values[j][2] * rhoa0;
      }

      auto c2 = std::size_t{0}; // keeps track of dimension indices
      auto c3 = std::size_t{0};
      for (std::size_t p = 0; p < DIM; p++) { // p for x,y,z

        rho1_vec[p] += r_ij_vec[p] * rhoa1 / r_ij;
        rho3_sub_vec[p] += r_ij_vec[p] * rhoa3 / r_ij;

        for (std::size_t q = p; q < DIM; q++) {

          rho2_vec[c2] += r_ij_vec[p] * r_ij_vec[q] * rhoa2 / (r_ij * r_ij);

          c2++;
          for (std::size_t r = q; r < DIM; r++) {

            rho3_vec[c3] += r_ij_vec[p] * r_ij_vec[q] * r_ij_vec[r] * rhoa3 /
                            (r_ij * r_ij * r_ij);

            c3++;
          }
        }
      }
    }

    auto rho1 = 0.0;
    auto rho2 = -(1.0 / 3.0) * rho2_sub * rho2_sub;
    auto rho3 = 0.0;

    for (std::size_t p = 0; p < DIM; p++) {
      rho1 += rho1_vec[p] * rho1_vec[p];
      rho3 -= (3.0 / 5.0) * rho3_sub_vec[p] * rho3_sub_vec[p];
    }

    for (std::size_t p = 0; p < 6; p++) {
      rho2 += static_cast<double>(V2[p]) * rho2_vec[p] * rho2_vec[p];
    }

    for (std::size_t p = 0; p < 10; p++) {
      rho3 += static_cast<double>(V3[p]) * rho3_vec[p] * rho3_vec[p];
    }

    if (rho0 > 0.0) {
      if (meam_information.ialloy == 1) {
        t1_ave = t1_ave / t1sq_ave;
        t2_ave = t2_ave / t2sq_ave;
        t3_ave = t3_ave / t3sq_ave;
      }
      if (meam_information.ialloy == 0) {
        t1_ave = t1_ave / rho0;
        t2_ave = t2_ave / rho0;
        t3_ave = t3_ave / rho0;
      }
      if (meam_information.ialloy == 2) {
        t1_ave = t1_i;
        t2_ave = t2_i;
        t3_ave = t3_i;
      }
    }
    auto gamma = t1_ave * rho1 + t2_ave * rho2 + t3_ave * rho3;

    if (rho0 > 1e-6) {
      gamma /= (rho0 * rho0);
    }

    const auto g = meam::calc_g(gamma, ibar, meam_information.gsmooth_factor);

    const auto rho = rho0 * g;

    // finding background electron density
    const auto z1 = meam::get_z(meam_information.lattice_structure[i_g][i_g]);
    const auto ref_shape_factors = meam::get_ref_shape_factors<DIM>(
        meam_information.lattice_structure[i_g][i_g]);

    auto rho_bkgd = double{};
    auto g_bkgd = double{};
    if (meam_information.mix_ref_t == 0)
      rho_bkgd = meam_information.rho_ref[i_g];
    else {
      if (ibar <= 0)
        g_bkgd = 1.0;
      else {
        const auto gamma_bkgd =
            (t1_ave * ref_shape_factors[0] + t2_ave * ref_shape_factors[1] +
             t3_ave * ref_shape_factors[2]) /
            (z1 * z1);
        g_bkgd =
            meam::calc_g(gamma_bkgd, ibar, meam_information.gsmooth_factor);
      }
      rho_bkgd = ro_0 * g_bkgd * static_cast<double>(z1);
    }

    const auto rho_bar = rho / rho_bkgd;

    rho_bar_vec.push_back(rho_bar);
  }

  // use rho_bar_vec, A and Ec are vecs too
  const auto embedding = comp_embedding_func_with_interpolation(
      (1 - conc_sum_i), rho_bar_vec, meam_information.a, meam_information.ec,
      data_point_atom, embedding_interpolating_polynomial_coeffs, start_idx,
      idx_offset, meam_information.emb_lin_neg, false);
  u = embedding;

  // Now computing the pair potential
  double phi_one_pair = 0;
  for (std::size_t j = 0; j < neighbors.size(); j++) {
    auto phi = 0.0;

    const auto neighbor = neighbors[j];
    const auto r_ij =
        (array_ops::as_eigen(neighbor) - array_ops::as_eigen(atom)).norm();

    if (r_ij < meam_information.rc) {
      // interpolation over the species of i and j
      for (std::size_t a = start_idx; a < Noi; a++) { // center atom species
        if (s_values[j][a - idx_offset][a - idx_offset] > 1e-6) {

          phi_one_pair = meam::calc_total_pair_potential<DIM>(
              r_ij, a - idx_offset, a - idx_offset, meam_information);
          phi += data_point_atom.impurity_concentrations[a] *
                 data_point_neighbor[j].impurity_concentrations[a] *
                 phi_one_pair * s_values[j][a - idx_offset][a - idx_offset];
        }
        for (std::size_t b = a + 1; b < Noi; b++) { // neighbor atom species
          if (s_values[j][a - idx_offset][b - idx_offset] > 1e-6) {
            phi_one_pair = meam::calc_total_pair_potential<DIM>(
                r_ij, a - idx_offset, b - idx_offset, meam_information);
            phi += (data_point_atom.impurity_concentrations[a] *
                        data_point_neighbor[j].impurity_concentrations[b] +
                    data_point_atom.impurity_concentrations[b] *
                        data_point_neighbor[j].impurity_concentrations[a]) *
                   phi_one_pair * s_values[j][a - idx_offset][b - idx_offset];
          }
        }
        if (s_values[j][a - idx_offset][noof_elements - 1] > 1e-6) {
          phi_one_pair = meam::calc_total_pair_potential<DIM>(
              r_ij, a - idx_offset, noof_elements - 1, meam_information);
          phi += (data_point_atom.impurity_concentrations[a] *
                      (1 - conc_sum_j[j]) +
                  (1 - conc_sum_i) *
                      data_point_neighbor[j].impurity_concentrations[a]) *
                 phi_one_pair * s_values[j][a - idx_offset][noof_elements - 1];
        }
      }
      if (s_values[j][noof_elements - 1][noof_elements - 1] > 1e-6) {
        phi_one_pair = meam::calc_total_pair_potential<DIM>(
            r_ij, noof_elements - 1, noof_elements - 1, meam_information);
        phi += (1 - conc_sum_j[j]) * (1 - conc_sum_i) * phi_one_pair *
               s_values[j][noof_elements - 1][noof_elements - 1];
      }
    }

    u += cutoff_s_values[j] * phi / 2;
  }
  return {u, embedding};
}
} // namespace materials::meam
