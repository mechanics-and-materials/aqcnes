// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief When creating the materials in these functions, there are some
 * attibutes of the Material class we have to set:
 * - Some of them are first set in the ClusterModel and we need to pass them
 * to the material object.
 * - Some of them are set from userinput.
 * - Some of them we can directly set them in "create_specific_material.
 *
 * These are 9 parameters:
 *
 * Set in "create_specific_material" coming from each ClusterModel:
 * - material_name
 * - atomic_masses
 * - lattice_structure (except in MEAM which is set to that of lattice file)
 * - neighbor_cutoff_radius: Set inside ClusterModel
 *
 * Set in "create_materials" they are set in material or from userinput:
 * - lattice_parameter_vec
 * - lattice_type
 * - offset_coeffs
 * - initial_rotation
 * - basis_vectors
 * @authors A. Wang, G. Bräunlich, M. Spinola
 */

#pragma once

#include "input/userinput.hpp"
#include "lattice/lattice_cache.hpp"
#include "materials/cluster_model.hpp"
#include "materials/copper/copper_extended_finnis_sinclair.hpp"
#include "materials/cubic_spline.hpp"
#include "materials/material.hpp"
#include "materials/meam/meamc.hpp"
#include "materials/setfl_tabulated_eam.hpp"
#include "materials/silica.hpp"
#include <string_view>

namespace array_ops = qcmesh::array_ops;

namespace materials {

namespace traits {
/**
 * @brief Material property type -> string mapping.
 */
template <class P> struct MaterialName;
template <> struct MaterialName<input::NNMaterialProperties> {
  constexpr static std::string_view name = "NN material";
};
template <> struct MaterialName<input::KimFccProperties> {
  constexpr static std::string_view name = "KIM material";
};
} // namespace traits

/**
 * @brief Unspecialized fallback material loader.
 *
 * If kim / torch is disabled, no specialization will be defined
 * for KIM material / NN material and the compiler will fallback to this.
 */
template <std::size_t Dimension, std::size_t Noi, class T>
Material<Dimension, Noi>
create_specific_material(const T &, const input::Userinput &,
                         const lattice::LatticeStructure) {
  throw exception::QCException{"Binary not built with " +
                               std::string{traits::MaterialName<T>::name} +
                               " support"};
}

template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::SilicaYukawaProperties &,
                         const input::Userinput &,
                         const lattice::LatticeStructure) {
  Material<Dimension, Noi> silica_yukawa;

  silica_yukawa.cluster_model =
      std::make_unique<SilicaYukawa<Dimension, Noi>>();

  silica_yukawa.material_name = "SilicaYukawa";
  silica_yukawa.lattice_structure =
      silica_yukawa.cluster_model->lattice_structure;
  return silica_yukawa;
}

template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::EFSCopperProperties &,
                         const input::Userinput &,
                         const lattice::LatticeStructure) {
  Material<Dimension, Noi> efs_copper;

  efs_copper.cluster_model =
      std::make_unique<ExtendedFinnisSinclairCopper<Dimension, Noi>>();

  efs_copper.material_name = "ExtendedFinnisSinclairCopper";
  efs_copper.lattice_structure = efs_copper.cluster_model->lattice_structure;
  return efs_copper;
}

template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::SetflEAMProperties &material_properties,
                         const input::Userinput &input,
                         const lattice::LatticeStructure) {

  Material<Dimension, Noi> setfl_mat;

  // create cluster model (retrieves info from setfl file
  // which is needed for material so we need to pass it to material)
  const auto vacancy = simulating_vacancies(input);

  setfl_mat.cluster_model = std::make_unique<
      SetflTabulatedEAM<materials::CubicSpline, Dimension, Noi>>(
      material_properties.potential_file_path,
      material_properties.potential_material_name,
      material_properties.funcfl_file, vacancy, input.potential_log,
      input.embedding_interpolation_coeffs);

  setfl_mat.material_name = material_properties.potential_material_name;
  setfl_mat.lattice_structure = setfl_mat.cluster_model->lattice_structure;
  return setfl_mat;
}

template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::MeamProperties &material_properties,
                         const input::Userinput &input,
                         const lattice::LatticeStructure lattice_type) {

  Material<Dimension, Noi> meam_mat;

  const auto vacancy = simulating_vacancies(input);
  meam_mat.cluster_model = std::make_unique<MEAMC<Dimension, Noi>>(
      material_properties.library_file_path,
      material_properties.parameter_file_path,
      material_properties.potential_material_name, material_properties.species,
      vacancy, input.potential_log, input.embedding_interpolation_coeffs);

  meam_mat.material_name = material_properties.potential_material_name;

  // set lattice_structure to that of the lattice file generated by preprocessor
  switch (lattice_type) {
  case lattice::LatticeStructure::B1:
  case lattice::LatticeStructure::B2:
  case lattice::LatticeStructure::L12:
  case lattice::LatticeStructure::FCC:
  case lattice::LatticeStructure::BCC:
  case lattice::LatticeStructure::HCP:
  case lattice::LatticeStructure::Diamond:
  case lattice::LatticeStructure::SimpleCubic:
  case lattice::LatticeStructure::None:
    meam_mat.lattice_structure = lattice_type;
    break;
  default:
    throw exception::QCException{
        "Unhandled lattice structure: " +
        std::string{serialize_lattice_structure(lattice_type)}};
  }
  return meam_mat;
}
} // namespace materials

#ifdef USE_KIM
#include "materials/kim_fcc.hpp"

namespace materials {

template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::KimFccProperties &material_properties,
                         const input::Userinput &,
                         const lattice::LatticeStructure) {

  auto kim_fcc_mat = Material<Dimension, Noi>{};

  kim_fcc_mat.cluster_model = std::make_unique<KimFcc<Dimension, Noi>>(
      material_properties.model_name, material_properties.atomic_mass,
      material_properties.species);

  kim_fcc_mat.material_name = "KIM_FCC";
  kim_fcc_mat.lattice_type = kim_fcc_mat.cluster_model->lattice_structure;

  return kim_fcc_mat;
}
} // namespace materials
#endif

#ifdef USE_TORCH

#include "materials/nn_material.hpp"

namespace materials {
template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_specific_material(const input::NNMaterialProperties &material_properties,
                         const input::Userinput &,
                         const lattice::LatticeStructure lattice_type) {
  if constexpr (Noi == 1) {
    auto nn_mat = Material<Dimension, 1>{};

    nn_mat.cluster_model = std::make_unique<NNMaterial>(
        material_properties.model_path,
        material_properties.standardization_file_path,
        material_properties.atomic_mass, lattice_type);

    nn_mat.material_name = material_properties.potential_material_name;
    nn_mat.lattice_structure = nn_mat.cluster_model->lattice_structure;
    return nn_mat;
  } else {
    throw exception::QCException{
        "NNMaterial does not Noi=" + std::to_string(Noi) +
        ". Currently supported: Noi=1"};
  }
}
} // namespace materials
#endif

namespace materials {
template <std::size_t Dimension, std::size_t Noi>
Material<Dimension, Noi>
create_material(const input::Userinput &input,
                const lattice::LatticeCache<Dimension> &cache) {

  Material<Dimension, Noi> material = std::visit(
      [&](const auto &material_properties) {
        return create_specific_material<Dimension, Noi>(
            material_properties, input, cache.lattice_type);
      },
      input.material);

  material.atomic_masses = material.cluster_model->atomic_masses;
  material.neighbor_cutoff_radius =
      material.cluster_model->neighbor_cutoff_radius;

  // Set the rest off material attributtes from input (userinput or lattice
  // file)
  material.lattice_parameter_vec = cache.lattice_parameters;
  material.basis_vectors = cache.rotated_basis_matrix;
  material.offset_coeffs = cache.seed_point_offsets;
  material.initial_rotation = array_ops::as_eigen_matrix(cache.rotation);

  // BasisVectors are generated with lattice_type
  // element_densities are computed with lattice_structure
  material.lattice_type = cache.lattice_type;

  std::cout << "Using material: " << material.material_name << std::endl;
  return material;
}

} // namespace materials
