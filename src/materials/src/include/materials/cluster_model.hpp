// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Class with pure virtual functions for the force and energy computation
 * of central atom in a cluster Derived material classes will implement these
 * functions for their corresponding potential.
 * @authors P. Gupta, S. Saxena, M. Spinola
 */
#pragma once

#include "geometry/point.hpp"
#include "lattice/lattice_structure.hpp"
#include "meshing/nodal_data_interproc.hpp"
#include <array>
#include <string>
#include <vector>

namespace materials {

template <std::size_t Dimension, std::size_t Noi> class ClusterModel {

public:
  bool output_potential_log = false;
  std::vector<double> atomic_masses = {63.546};
  double neighbor_cutoff_radius{};
  lattice::LatticeStructure lattice_structure =
      (Dimension == 3) ? lattice::LatticeStructure::FCC
                       : lattice::LatticeStructure::HCP;

  // needs to be public so we can call these functions from inside
  // the material class

  [[nodiscard]] virtual double get_cluster_energy(
      const geometry::Point<Dimension> &,
      const std::vector<geometry::Point<Dimension>> &,
      const meshing::NodalDataMultispecies<Noi> &,
      const std::vector<meshing::NodalDataMultispecies<Noi>> &) const = 0;

  virtual void get_cluster_forces(
      std::vector<geometry::Point<Dimension>> &,
      const geometry::Point<Dimension> &,
      const std::vector<geometry::Point<Dimension>> &,
      const meshing::NodalDataMultispecies<Noi> &,
      const std::vector<meshing::NodalDataMultispecies<Noi>> &) const = 0;

  virtual void get_concentration_forces(
      std::array<double, Noi> &, std::vector<std::array<double, Noi>> &,
      const geometry::Point<Dimension> &,
      const std::vector<geometry::Point<Dimension>> &,
      const meshing::NodalDataMultispecies<Noi> &,
      const std::vector<meshing::NodalDataMultispecies<Noi>> &) const = 0;

  ClusterModel() = default;
  ClusterModel(const ClusterModel &) = default;
  ClusterModel(ClusterModel &&) noexcept = default;
  ClusterModel &operator=(const ClusterModel &) = default;
  ClusterModel &operator=(ClusterModel &&) noexcept = default;
  // Add virtual destructor. When using a std::vector<Material>, at some point
  // the destuctor of the vector will delete each Material's attributes.
  // One of the attributes is an std::unique_ptr<ClusterModel>. The vector
  // destructor will call the std::unique_ptr destructor which will call
  // delete on ClusterModel. But since ClusterModel is an interface object
  // we need to provide a virtual destructor so that it can be resolved which
  // actual derived class from ClusterModel has to be deleted
  virtual ~ClusterModel() = default;
};

} // namespace materials
