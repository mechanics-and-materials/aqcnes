// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Contains a material class
 * which holds a specific type of material
 * (chemical composition) and specific
 * potential in a `cluster_model` member.
 * @authors P. Gupta, S. Saxena, M. Spinola
 */
#pragma once

#include "lattice/generate_basis.hpp"
#include "lattice/generate_centro_symmetry_data.hpp"
#include "lattice/lattice_structure.hpp"
#include "materials/cluster_model.hpp"
#include "materials/material_base.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "utils/string_split.hpp"
#include <Eigen/Core>
#include <fstream>
#include <memory>
#include <random>
#include <sstream>

namespace array_ops = qcmesh::array_ops;

namespace materials {

namespace detail {
template <std::size_t DIM, std::size_t NIDOF>
exception::QCException format_error(
    const std::array<double, NIDOF> &neighbor_thermal_coordinates,
    const std::array<double, DIM> &neighbour_shift,
    const std::array<double, DIM> &neighbour_mean_location,
    const std::array<double, DIM> &sampling_atom_shift,
    const std::array<double, DIM> &sampling_atom_mean_location,
    const std::array<double, NIDOF> &sampling_atom_thermal_coordinates) {
  const auto to_str = [](const auto &arr) {
    std::stringstream buf{};
    buf << array_ops::as_streamable(arr);
    return buf.str();
  };
  return exception::QCException{
      "nan distance detected:\n\tneighbor_thermal_coordinates=" +
      to_str(neighbor_thermal_coordinates) +
      "\n\tneighbour_shift=" + to_str(neighbour_shift) +
      "\n\tneighbour_mean_location=" + to_str(neighbour_mean_location) +
      "\n\tsampling_atom_shift=" + to_str(sampling_atom_shift) +
      "\n\tsampling_atom_mean_location=" + to_str(sampling_atom_mean_location) +
      "\n\tsampling_atom_thermal_coordinates=" +
      to_str(sampling_atom_thermal_coordinates)};
}
} // namespace detail

template <std::size_t DIM, std::size_t NOI = 1, std::size_t NIDOF = 2>
struct Material : MaterialBase<DIM> {
  using MaterialBase<DIM>::DIMENSION;
  constexpr static std::size_t THERMAL_DOF = NIDOF;
  constexpr static std::size_t NOOF_IMPURITIES = NOI;
  using EdgeVector = std::array<std::size_t, 2>;
  using Point = std::array<double, DIM>;
  using ThermalPoint = std::array<double, NIDOF>;
  using NodalDataPoint = meshing::NodalDataMultispecies<NOI>;
  using ConcentrationPoint = std::array<double, NOI>;
  using Matrix = Eigen::Matrix<double, DIM, DIM>;

  using MaterialBase<DIM>::basis_vectors;
  using MaterialBase<DIM>::lattice_parameter_vec;
  using MaterialBase<DIM>::offset_coeffs;
  using MaterialBase<DIM>::atomic_masses;
  using MaterialBase<DIM>::neighbor_cutoff_radius;
  using MaterialBase<DIM>::lattice_type;
  using MaterialBase<DIM>::lattice_structure;
  Matrix initial_rotation{};
  std::string material_name{};
  std::unique_ptr<ClusterModel<DIM, NOI>> cluster_model{};

  [[nodiscard]] std::array<double, DIM> get_cell_dimensions() const {
    return this->lattice_parameter_vec;
  }

  void assign_basis_vectors(const Matrix m) {
    std::array<double, DIM> unit_cell = this->get_cell_dimensions();
    std::array<std::array<double, DIM>, DIM> basis_matrix{};

    if (DIM == 2) {
      this->basis_vectors[0][0] = unit_cell[0];
      this->basis_vectors[1][0] = 0.0;

      this->basis_vectors[0][1] = cos(M_PI / 3.0) * unit_cell[0];
      this->basis_vectors[1][1] = sin(M_PI / 3.0) * unit_cell[0];
    } else if (DIM == 3) {

      lattice::generate_basis(unit_cell, this->lattice_type, basis_matrix);

      const auto basis_prime =
          array_ops::as_eigen_matrix(basis_matrix).transpose() * m;

      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          this->basis_vectors[i][j] = basis_prime(j, i);
        }
      }
    }
  }

  [[nodiscard]] double
  get_cluster_energy(const Point &atom,
                     const std::vector<Point> &neighbours_coords,
                     const NodalDataPoint &atom_data,
                     const std::vector<NodalDataPoint> &neighbours_data) const {
    return this->cluster_model->get_cluster_energy(atom, neighbours_coords,
                                                   atom_data, neighbours_data);
  }

  void
  get_cluster_forces(std::vector<Point> &atom_forces, const Point &atom,
                     const std::vector<Point> &neighbours_coords,
                     const NodalDataPoint &atom_data,
                     const std::vector<NodalDataPoint> &neighbours_data) const {
    return this->cluster_model->get_cluster_forces(
        atom_forces, atom, neighbours_coords, atom_data, neighbours_data);
  }

  void get_concentration_forces(
      ConcentrationPoint &forces_atom,
      std::vector<ConcentrationPoint> &forces_neighbours, const Point &atom,
      const std::vector<Point> &neighbours,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {
    return this->cluster_model->get_concentration_forces(
        forces_atom, forces_neighbours, atom, neighbours, atom_data_point,
        neighbours_data_points);
  }

  [[nodiscard]] double get_thermalized_energy_3(
      const Point &atom, const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {

    double u = 0.0;

    Point quad_point;
    Point quad_point_frame;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();
    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples = 2 * noof_scalars;
    const auto r_quad = sqrt(0.5 * double(noof_scalars));
    const auto w_quad = 1.0 / double(noof_quadrature_samples);

    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);
    Point zero_point;
    zero_point.fill(0.0);

    for (std::size_t quadrature_idx = 0;
         quadrature_idx < noof_quadrature_samples; quadrature_idx++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);

      std::fill(neighbour_shifts.begin(), neighbour_shifts.end(), zero_point);
      std::fill(shifted_neighbours.begin(), shifted_neighbours.end(),
                zero_point);

      quad_point.fill(0.0);

      const auto r_quad_sign = (quadrature_idx < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx % DIM] = r_quad_sign * r_quad;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
            neighbour_shifts[neighbour_idx] = neighbour_shift;
          }
        }

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++)
        array_ops::as_eigen(shifted_neighbours[neighbour_idx]) =
            array_ops::as_eigen(neighbours[neighbour_idx]) +
            array_ops::as_eigen(neighbour_shifts[neighbour_idx]);
      const auto quad_energy =
          this->get_cluster_energy(shifted_sampling_atom, shifted_neighbours,
                                   atom_data_point, neighbours_data_points);
      u += w_quad * quad_energy;
    }
    return u;
  }

  void get_thermalized_forces_3(
      std::vector<Point> &forces,
      std::vector<double> &thermal_forces_sampling_atom,
      std::vector<double> &thermal_forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {

    forces.clear();
    thermal_forces_sampling_atom.clear();
    thermal_forces_neighbours.clear();
    forces.resize(neighbours.size(), Point{});
    thermal_forces_sampling_atom.resize(neighbours.size(), 0.0);
    thermal_forces_neighbours.resize(neighbours.size(), 0.0);

    Point quad_point;
    Point quad_point_frame;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();
    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples = 2 * noof_scalars;
    const auto r_quad = sqrt(0.5 * double(noof_scalars));
    const auto w_quad = 1.0 / double(noof_quadrature_samples);

    std::vector<Point> quad_forces(noof_neighbours + 1);
    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);

    for (std::size_t quadrature_idx = 0;
         quadrature_idx < noof_quadrature_samples; quadrature_idx++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);

      neighbour_shifts.clear();

      quad_point.fill(0.0);

      const auto r_quad_sign = (quadrature_idx < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx % DIM] = r_quad_sign * r_quad;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
          }
        }

        neighbour_shifts.push_back(neighbour_shift);

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++)
        array_ops::as_eigen(shifted_neighbours[neighbour_idx]) =
            array_ops::as_eigen(neighbours[neighbour_idx]) +
            array_ops::as_eigen(neighbour_shifts[neighbour_idx]);
      this->get_cluster_forces(quad_forces, shifted_sampling_atom,
                               shifted_neighbours, atom_data_point,
                               neighbours_data_points);

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        array_ops::as_eigen(forces[neighbour_idx]) +=
            w_quad * array_ops::as_eigen(quad_forces[neighbour_idx]);
        thermal_forces_sampling_atom[neighbour_idx] +=
            w_quad *
            array_ops::dot(quad_forces[neighbour_idx], sampling_atom_shift) /
            double(DIM);
        thermal_forces_neighbours[neighbour_idx] +=
            w_quad *
            array_ops::dot(quad_forces[neighbour_idx],
                           neighbour_shifts[neighbour_idx]) /
            double(DIM);
      }
    }
  }

  // Fifth order quadrature energy sampling
  [[nodiscard]] double get_thermalized_energy_5(
      const Point &atom, const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {
    double u = 0.0;

    Point quad_point;
    Point quad_point_1;
    Point quad_point_2;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point quad_point_frame;
    Point quad_point_frame_1;
    Point quad_point_frame_2;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();

    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);
    Point zero_point;
    zero_point.fill(0.0);

    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples_p1 = 2 * noof_scalars;
    const auto noof_quadrature_samples_p2 =
        2 * noof_scalars * (noof_scalars - 1);
    const auto r_quad_1 = sqrt(0.5 * double(noof_scalars + 2));
    const auto r_quad_2 = sqrt(0.25 * double(noof_scalars + 2));
    const auto w_quad_0 = 2.0 / double(noof_scalars + 2);
    const auto w_quad_1 =
        double(4.0 - noof_scalars) /
        (2.0 * double((noof_scalars + 2) * (noof_scalars + 2)));
    const auto w_quad_2 = 1.0 / double((noof_scalars + 2) * (noof_scalars + 2));

    // Contribution of central point
    const auto u0 = this->get_cluster_energy(atom, neighbours, atom_data_point,
                                             neighbours_data_points);
    u += u0 * w_quad_0;

    // Contribution of single permutations
    for (std::size_t quadrature_idx_p1 = 0;
         quadrature_idx_p1 < noof_quadrature_samples_p1; quadrature_idx_p1++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx_p1 % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);

      std::fill(neighbour_shifts.begin(), neighbour_shifts.end(), zero_point);
      std::fill(shifted_neighbours.begin(), shifted_neighbours.end(),
                zero_point);

      quad_point.fill(0.0);

      const auto r1_quad_sign = (quadrature_idx_p1 < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx_p1 % DIM] = r1_quad_sign * r_quad_1;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
            neighbour_shifts[neighbour_idx] = neighbour_shift;
          }
        }

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++)
        array_ops::as_eigen(shifted_neighbours[neighbour_idx]) =
            array_ops::as_eigen(neighbours[neighbour_idx]) +
            array_ops::as_eigen(neighbour_shifts[neighbour_idx]);
      const auto quad_energy =
          this->get_cluster_energy(shifted_sampling_atom, shifted_neighbours,
                                   atom_data_point, neighbours_data_points);
      u += w_quad_1 * quad_energy;
    }

    // Contribution of double permutations
    std::size_t total_p2_count = 0;
    for (std::size_t c = 0; c < 4; c++) {
      const auto r2_quad_sign_1 = ((c % 2) < 1) ? 1 : -1;
      const auto r2_quad_sign_2 = (c < 2) ? 1 : -1;
      for (std::size_t c1 = 0; c1 < noof_scalars - 1; c1++) {
        for (std::size_t c2 = c1 + 1; c2 < noof_scalars; c2++) {
          total_p2_count++;
          const auto displaced_point_index_1 = std::size_t(c1 / DIM);
          const auto displaced_point_index_2 = std::size_t(c2 / DIM);
          sampling_atom_shift.fill(0.0);
          neighbour_shifts.clear();
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          quad_point_1[c1 % DIM] = r2_quad_sign_1 * r_quad_2;
          quad_point_2[c2 % DIM] = r2_quad_sign_2 * r_quad_2;

          // Added to make energy calculation objective
          quad_point_frame_1 = quad_point_1;
          quad_point_frame_2 = quad_point_2;
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          for (std::size_t i = 0; i < DIM; i++) {
            for (std::size_t j = 0; j < DIM; j++) {
              quad_point_1[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_1[j];
              quad_point_2[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_2[j];
            }
          }

          if (displaced_point_index_1 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
          }
          if (displaced_point_index_2 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
          }

          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++) {
            neighbour_shift.fill(0.0);

            if (displaced_point_index_1 < noof_neighbours) {
              if (displaced_point_index_1 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
              }
            }

            if (displaced_point_index_2 < noof_neighbours) {
              if (displaced_point_index_2 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
              }
            }

            const auto r_ij_mag =
                (array_ops::as_eigen(neighbour_shift) +
                 array_ops::as_eigen(neighbours[neighbour_idx]) -
                 array_ops::as_eigen(sampling_atom_shift) -
                 array_ops::as_eigen(atom))
                    .norm();

            if (std::isnan(r_ij_mag)) {
              throw detail::format_error(
                  neighbours_thermal_coordinates[neighbour_idx],
                  neighbour_shift, neighbours[neighbour_idx],
                  sampling_atom_shift, atom, atom_thermal_coordinates);
            }
            neighbour_shifts.push_back(neighbour_shift);
          }

          array_ops::as_eigen(shifted_sampling_atom) =
              array_ops::as_eigen(atom) +
              array_ops::as_eigen(sampling_atom_shift);
          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++)
            array_ops::as_eigen(shifted_neighbours[neighbour_idx]) =
                array_ops::as_eigen(neighbours[neighbour_idx]) +
                array_ops::as_eigen(neighbour_shifts[neighbour_idx]);
          const auto quad_energy = this->get_cluster_energy(
              shifted_sampling_atom, shifted_neighbours, atom_data_point,
              neighbours_data_points);
          u += w_quad_2 * quad_energy;
        }
      }
    }
    if (total_p2_count != noof_quadrature_samples_p2)
      throw exception::QCException{
          "MEAM.c: total_P2_count != noof_quadrature_samples_P2"};

    return u;
  }

  // Fifth order quadrature force sampling
  void get_thermalized_forces_5(
      std::vector<Point> &forces,
      std::vector<double> &thermal_forces_sampling_atom,
      std::vector<double> &thermal_forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {

    forces.clear();
    thermal_forces_sampling_atom.clear();
    thermal_forces_neighbours.clear();
    forces.resize(neighbours.size(), Point{});
    thermal_forces_sampling_atom.resize(neighbours.size(), 0.0);
    thermal_forces_neighbours.resize(neighbours.size(), 0.0);

    Point quad_point;
    Point quad_point_1;
    Point quad_point_2;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point quad_point_frame;
    Point quad_point_frame_1;
    Point quad_point_frame_2;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();

    std::vector<Point> quad_forces(noof_neighbours + 1);
    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);
    std::vector<Point> forces0(noof_neighbours);

    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples_p1 = 2 * noof_scalars;
    const auto noof_quadrature_samples_p2 =
        2 * noof_scalars * (noof_scalars - 1);
    const auto r_quad_1 = sqrt(0.5 * double(noof_scalars + 2));
    const auto r_quad_2 = sqrt(0.25 * double(noof_scalars + 2));
    const auto w_quad_0 = 2.0 / double(noof_scalars + 2);
    const auto w_quad_1 =
        double(4.0 - noof_scalars) /
        (2.0 * double((noof_scalars + 2) * (noof_scalars + 2)));
    const auto w_quad_2 = 1.0 / double((noof_scalars + 2) * (noof_scalars + 2));

    // Contribution of central point
    this->get_cluster_forces(forces0, atom, neighbours, atom_data_point,
                             neighbours_data_points);
    for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      array_ops::as_eigen(forces[neighbour_idx]) +=
          w_quad_0 * array_ops::as_eigen(forces0[neighbour_idx]);
    }

    // Contribution of single permutations
    for (std::size_t quadrature_idx_p1 = 0;
         quadrature_idx_p1 < noof_quadrature_samples_p1; quadrature_idx_p1++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx_p1 % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);

      neighbour_shifts.clear();

      quad_point.fill(0.0);

      const auto r1_quad_sign = (quadrature_idx_p1 < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx_p1 % DIM] = r1_quad_sign * r_quad_1;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
          }
        }

        neighbour_shifts.push_back(neighbour_shift);

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t i = 0; i < noof_neighbours; i++)
        array_ops::as_eigen(shifted_neighbours[i]) =
            array_ops::as_eigen(neighbours[i]) +
            array_ops::as_eigen(neighbour_shifts[i]);
      this->get_cluster_forces(quad_forces, shifted_sampling_atom,
                               shifted_neighbours, atom_data_point,
                               neighbours_data_points);

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        array_ops::as_eigen(forces[neighbour_idx]) +=
            w_quad_1 * array_ops::as_eigen(quad_forces[neighbour_idx]);
        thermal_forces_sampling_atom[neighbour_idx] +=
            w_quad_1 *
            array_ops::dot(quad_forces[neighbour_idx], sampling_atom_shift) /
            double(DIM);
        thermal_forces_neighbours[neighbour_idx] +=
            w_quad_1 *
            array_ops::dot(quad_forces[neighbour_idx],
                           neighbour_shifts[neighbour_idx]) /
            double(DIM);
      }
    }

    // Contribution of double permutations
    std::size_t total_p2_count = 0;
    for (std::size_t c = 0; c < 4; c++) {
      const auto r2_quad_sign_1 = ((c % 2) < 1) ? 1 : -1;
      const auto r2_quad_sign_2 = (c < 2) ? 1 : -1;
      for (std::size_t c1 = 0; c1 < noof_scalars - 1; c1++) {
        for (std::size_t c2 = c1 + 1; c2 < noof_scalars; c2++) {
          total_p2_count++;
          const auto displaced_point_index_1 = std::size_t(c1 / DIM);
          const auto displaced_point_index_2 = std::size_t(c2 / DIM);
          sampling_atom_shift.fill(0.0);
          neighbour_shifts.clear();
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          quad_point_1[c1 % DIM] = r2_quad_sign_1 * r_quad_2;
          quad_point_2[c2 % DIM] = r2_quad_sign_2 * r_quad_2;

          // Added to make energy calculation objective
          quad_point_frame_1 = quad_point_1;
          quad_point_frame_2 = quad_point_2;
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          for (std::size_t i = 0; i < DIM; i++) {
            for (std::size_t j = 0; j < DIM; j++) {
              quad_point_1[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_1[j];
              quad_point_2[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_2[j];
            }
          }

          if (displaced_point_index_1 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
          }
          if (displaced_point_index_2 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
          }

          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++) {
            neighbour_shift.fill(0.0);

            if (displaced_point_index_1 < noof_neighbours) {
              if (displaced_point_index_1 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
              }
            }

            if (displaced_point_index_2 < noof_neighbours) {
              if (displaced_point_index_2 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
              }
            }

            const auto r_ij_mag =
                (array_ops::as_eigen(neighbour_shift) +
                 array_ops::as_eigen(neighbours[neighbour_idx]) -
                 array_ops::as_eigen(sampling_atom_shift) -
                 array_ops::as_eigen(atom))
                    .norm();

            if (std::isnan(r_ij_mag)) {
              throw detail::format_error(
                  neighbours_thermal_coordinates[neighbour_idx],
                  neighbour_shift, neighbours[neighbour_idx],
                  sampling_atom_shift, atom, atom_thermal_coordinates);
            }

            neighbour_shifts.push_back(neighbour_shift);
          }

          array_ops::as_eigen(shifted_sampling_atom) =
              array_ops::as_eigen(atom) +
              array_ops::as_eigen(sampling_atom_shift);
          for (std::size_t i = 0; i < noof_neighbours; i++)
            array_ops::as_eigen(shifted_neighbours[i]) =
                array_ops::as_eigen(neighbours[i]) +
                array_ops::as_eigen(neighbour_shifts[i]);
          this->get_cluster_forces(quad_forces, shifted_sampling_atom,
                                   shifted_neighbours, atom_data_point,
                                   neighbours_data_points);

          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++) {
            array_ops::as_eigen(forces[neighbour_idx]) +=
                w_quad_2 * array_ops::as_eigen(quad_forces[neighbour_idx]);
            thermal_forces_sampling_atom[neighbour_idx] +=
                w_quad_2 *
                array_ops::dot(quad_forces[neighbour_idx],
                               sampling_atom_shift) /
                double(DIM);
            thermal_forces_neighbours[neighbour_idx] +=
                w_quad_2 *
                array_ops::dot(quad_forces[neighbour_idx],
                               neighbour_shifts[neighbour_idx]) /
                double(DIM);
          }
        }
      }
    }
    if (total_p2_count != noof_quadrature_samples_p2)
      throw exception::QCException{
          "MEAM.c: total_P2_count != noof_quadrature_samples_P2"};
  }

  // Metropolis energy and force sampling
  double get_thermalized_energy_and_forces_metropolis(
      std::vector<Point> &forces,
      std::vector<double> &thermal_forces_sampling_atom,
      std::vector<double> &thermal_forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) {
    forces.clear();
    thermal_forces_sampling_atom.clear();
    thermal_forces_neighbours.clear();
    forces.resize(neighbours.size(), Point{});
    thermal_forces_sampling_atom.resize(neighbours.size(), 0.0);
    thermal_forces_neighbours.resize(neighbours.size(), 0.0);
    auto u = 0.0;
    Point shift;
    Point atom_realization;
    double acc_prob{};
    double new_shift_sqnorm{};
    double old_shift_sqnorm{};
    double energy{};
    double sigma{};
    std::size_t noof_neighbours = neighbours.size();
    std::vector<Point> neighbours_realization;
    std::vector<Point> forces_realization;
    double atom_shift_sqnorm{};
    std::vector<double> shift_sqnorms;
    neighbours_realization.reserve(noof_neighbours);
    shift_sqnorms.reserve(noof_neighbours);
    for (const auto &neighbour : neighbours) {
      neighbours_realization.push_back(neighbour);
      shift_sqnorms.push_back(0.0);
    }
    atom_realization = atom;
    atom_shift_sqnorm = 0.0;
    std::default_random_engine generator;
    auto rd = std::random_device{};
    generator.seed(rd());
    std::normal_distribution<double> std_normal_dist(0.0, 1.0);
    std::uniform_real_distribution<double> uniform_dist(0.0, 1.0);
    int nsubsweeps = (DIM * (noof_neighbours + 1));
    int nsamplingpoints = 1e6;
    int noof_same_points = 0;
    bool changed = false;

    for (int i = 0; i < nsamplingpoints; i++) {
      changed = false;
      for (int j = 0; j < nsubsweeps; j++) {
        // choose a neighbor index at random
        std::size_t index = rd() % (noof_neighbours + 1);
        if (index == noof_neighbours) {
          sigma = exp(-atom_thermal_coordinates[1]);
          old_shift_sqnorm = atom_shift_sqnorm;
        } else {
          sigma = exp(-neighbours_thermal_coordinates[index][1]);
          old_shift_sqnorm = shift_sqnorms[index];
        }
        // displace the neighbour by choosing numbers from a std normal dist.
        for (std::size_t d = 0; d < DIM; d++) {
          shift[d] = sqrt(sigma) * std_normal_dist(generator);
        }
        // Accept or reject the move according to metropolis algorithm
        if (index == noof_neighbours) {
          new_shift_sqnorm =
              (array_ops::as_eigen(atom_realization) +
               array_ops::as_eigen(shift) - array_ops::as_eigen(atom))
                  .norm();
        } else {
          new_shift_sqnorm =
              (array_ops::as_eigen(neighbours_realization[index]) +
               array_ops::as_eigen(shift) -
               array_ops::as_eigen(neighbours[index]))
                  .norm();
        }
        new_shift_sqnorm *= new_shift_sqnorm;
        if (old_shift_sqnorm < new_shift_sqnorm) {
          // the new config is at a higher energy so calc. acceptance prob.
          acc_prob = exp((old_shift_sqnorm - new_shift_sqnorm) / (2 * sigma));
          // accept the move with this prob.
          if (uniform_dist(generator) < acc_prob) {
            if (!changed)
              changed = true;
            if (index == noof_neighbours) {
              array_ops::as_eigen(atom_realization) +=
                  array_ops::as_eigen(shift);
              atom_shift_sqnorm = new_shift_sqnorm;
            } else {
              array_ops::as_eigen(neighbours_realization[index]) +=
                  array_ops::as_eigen(shift);
              shift_sqnorms[index] = new_shift_sqnorm;
            }
          }
        } else {
          // accept the move
          if (!changed)
            changed = true;
          if (index == noof_neighbours) {
            array_ops::as_eigen(atom_realization) += array_ops::as_eigen(shift);
            atom_shift_sqnorm = new_shift_sqnorm;
          } else {
            array_ops::as_eigen(neighbours_realization[index]) +=
                array_ops::as_eigen(shift);
            shift_sqnorms[index] = new_shift_sqnorm;
          }
        }
      }
      if (changed) {
        energy = get_cluster_energy(atom_realization, neighbours_realization,
                                    atom_data_point, neighbours_data_points);
        this->get_cluster_forces(forces_realization, atom_realization,
                                 neighbours_realization, atom_data_point,
                                 neighbours_data_points);
        for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
             neighbour_idx++) {
          const auto r = array_ops::as_eigen(forces_realization[neighbour_idx]);
          array_ops::as_eigen(forces[neighbour_idx]) += r;

          thermal_forces_sampling_atom[neighbour_idx] +=
              r.dot((array_ops::as_eigen(atom_realization) -
                     array_ops::as_eigen(atom))) /
              double(DIM);

          thermal_forces_neighbours[neighbour_idx] +=
              r.dot(
                  (array_ops::as_eigen(neighbours_realization[neighbour_idx]) -
                   array_ops::as_eigen(neighbours[neighbour_idx]))) /
              double(DIM);
        }
        u += energy;
      } else {
        noof_same_points += 1;
      }
    }
    for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      array_ops::as_eigen(forces[neighbour_idx]) /=
          (nsamplingpoints - noof_same_points);
      thermal_forces_sampling_atom[neighbour_idx] /=
          (nsamplingpoints - noof_same_points);
      thermal_forces_neighbours[neighbour_idx] /=
          (nsamplingpoints - noof_same_points);
    }
    u /= (nsamplingpoints - noof_same_points);
    return u;
  }

  // thermalization of concentration based forces
  void get_concentration_forces_3(
      ConcentrationPoint &forces_atom,
      std::vector<ConcentrationPoint> &forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {

    forces_neighbours.clear();
    forces_neighbours.resize(neighbours.size());
    forces_atom.fill(0.0);

    Point quad_point;
    Point quad_point_frame;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();
    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples = 2 * noof_scalars;
    const auto r_quad = sqrt(0.5 * double(noof_scalars));
    const auto w_quad = 1.0 / double(noof_quadrature_samples);

    ConcentrationPoint quad_forces_atom;
    std::vector<ConcentrationPoint> quad_forces_neighbors(noof_neighbours);
    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);

    for (std::size_t quadrature_idx = 0;
         quadrature_idx < noof_quadrature_samples; quadrature_idx++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);

      neighbour_shifts.clear();

      quad_point.fill(0.0);

      const auto r_quad_sign = (quadrature_idx < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx % DIM] = r_quad_sign * r_quad;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
          }
        }

        neighbour_shifts.push_back(neighbour_shift);

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++)
        array_ops::as_eigen(shifted_neighbours[neighbour_idx]) =
            array_ops::as_eigen(neighbours[neighbour_idx]) +
            array_ops::as_eigen(neighbour_shifts[neighbour_idx]);
      this->get_concentration_forces(quad_forces_atom, quad_forces_neighbors,
                                     shifted_sampling_atom, shifted_neighbours,
                                     atom_data_point, neighbours_data_points);

      array_ops::as_eigen(forces_atom) +=
          w_quad * array_ops::as_eigen(quad_forces_atom);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        array_ops::as_eigen(forces_neighbours[neighbour_idx]) +=
            w_quad * array_ops::as_eigen(quad_forces_neighbors[neighbour_idx]);
      }
    }
  }

  void get_concentration_forces_5(
      ConcentrationPoint &forces_atom,
      std::vector<ConcentrationPoint> &forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points) const {

    forces_neighbours.clear();
    forces_neighbours.resize(neighbours.size());
    forces_atom.fill(0.0);

    Point quad_point;
    Point quad_point_1;
    Point quad_point_2;
    Point sampling_atom_shift;
    Point neighbour_shift;
    Point quad_point_frame;
    Point quad_point_frame_1;
    Point quad_point_frame_2;
    Point shifted_sampling_atom;

    const auto noof_neighbours = neighbours.size();

    ConcentrationPoint quad_forces_atom;
    std::vector<ConcentrationPoint> quad_forces_neighbors(noof_neighbours);

    std::vector<Point> neighbour_shifts(noof_neighbours);
    std::vector<Point> shifted_neighbours(noof_neighbours);

    const auto noof_scalars = DIM * (noof_neighbours + 1);
    const auto noof_quadrature_samples_p1 = 2 * noof_scalars;
    const auto noof_quadrature_samples_p2 =
        2 * noof_scalars * (noof_scalars - 1);
    const auto r_quad_1 = sqrt(0.5 * double(noof_scalars + 2));
    const auto r_quad_2 = sqrt(0.25 * double(noof_scalars + 2));
    const auto w_quad_0 = 2.0 / double(noof_scalars + 2);
    const auto w_quad_1 =
        double(4.0 - noof_scalars) /
        (2.0 * double((noof_scalars + 2) * (noof_scalars + 2)));
    const auto w_quad_2 = 1.0 / double((noof_scalars + 2) * (noof_scalars + 2));

    // Contribution of central point
    this->get_concentration_forces(quad_forces_atom, quad_forces_neighbors,
                                   atom, neighbours, atom_data_point,
                                   neighbours_data_points);

    array_ops::as_eigen(forces_atom) +=
        w_quad_0 * array_ops::as_eigen(quad_forces_atom);
    for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      array_ops::as_eigen(forces_neighbours[neighbour_idx]) +=
          w_quad_0 * array_ops::as_eigen(quad_forces_neighbors[neighbour_idx]);
    }

    // Contribution of single permutations
    for (std::size_t quadrature_idx_p1 = 0;
         quadrature_idx_p1 < noof_quadrature_samples_p1; quadrature_idx_p1++) {
      const auto displaced_point_index =
          std::size_t((quadrature_idx_p1 % noof_scalars) / DIM);
      sampling_atom_shift.fill(0.0);
      neighbour_shifts.clear();
      quad_point.fill(0.0);

      const auto r1_quad_sign = (quadrature_idx_p1 < noof_scalars) ? 1 : -1;
      quad_point[quadrature_idx_p1 % DIM] = r1_quad_sign * r_quad_1;

      // Added to make energy calculation objective
      quad_point_frame = quad_point;
      quad_point.fill(0.0);
      for (std::size_t i = 0; i < DIM; i++) {
        for (std::size_t j = 0; j < DIM; j++) {
          quad_point[i] += this->initial_rotation(i, j) * quad_point_frame[j];
        }
      }

      if (displaced_point_index == noof_neighbours) {
        const auto sigma = exp(-atom_thermal_coordinates[1]);
        array_ops::as_eigen(sampling_atom_shift) =
            sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
      }

      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        neighbour_shift.fill(0.0);

        if (displaced_point_index < noof_neighbours) {
          if (displaced_point_index == neighbour_idx) {
            const auto sigma =
                exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
            array_ops::as_eigen(neighbour_shift) =
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point);
          }
        }

        neighbour_shifts.push_back(neighbour_shift);

        const auto r_ij_mag = (array_ops::as_eigen(neighbour_shift) +
                               array_ops::as_eigen(neighbours[neighbour_idx]) -
                               array_ops::as_eigen(sampling_atom_shift) -
                               array_ops::as_eigen(atom))
                                  .norm();

        if (std::isnan(r_ij_mag)) {
          throw detail::format_error(
              neighbours_thermal_coordinates[neighbour_idx], neighbour_shift,
              neighbours[neighbour_idx], sampling_atom_shift, atom,
              atom_thermal_coordinates);
        }
      }

      array_ops::as_eigen(shifted_sampling_atom) =
          array_ops::as_eigen(atom) + array_ops::as_eigen(sampling_atom_shift);
      for (std::size_t i = 0; i < noof_neighbours; i++)
        array_ops::as_eigen(shifted_neighbours[i]) =
            array_ops::as_eigen(neighbours[i]) +
            array_ops::as_eigen(neighbour_shifts[i]);
      this->get_concentration_forces(quad_forces_atom, quad_forces_neighbors,
                                     shifted_sampling_atom, shifted_neighbours,
                                     atom_data_point, neighbours_data_points);

      array_ops::as_eigen(forces_atom) +=
          w_quad_1 * array_ops::as_eigen(quad_forces_atom);
      for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
           neighbour_idx++) {
        array_ops::as_eigen(forces_neighbours[neighbour_idx]) +=
            w_quad_1 *
            array_ops::as_eigen(quad_forces_neighbors[neighbour_idx]);
      }
    }

    // Contribution of double permutations
    std::size_t total_p2_count = 0;
    for (std::size_t c = 0; c < 4; c++) {
      const auto r2_quad_sign_1 = ((c % 2) < 1) ? 1 : -1;
      const auto r2_quad_sign_2 = (c < 2) ? 1 : -1;
      for (std::size_t c1 = 0; c1 < noof_scalars - 1; c1++) {
        for (std::size_t c2 = c1 + 1; c2 < noof_scalars; c2++) {
          total_p2_count++;
          const auto displaced_point_index_1 = std::size_t(c1 / DIM);
          const auto displaced_point_index_2 = std::size_t(c2 / DIM);
          sampling_atom_shift.fill(0.0);
          neighbour_shifts.clear();
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          quad_point_1[c1 % DIM] = r2_quad_sign_1 * r_quad_2;
          quad_point_2[c2 % DIM] = r2_quad_sign_2 * r_quad_2;

          // Added to make energy calculation objective
          quad_point_frame_1 = quad_point_1;
          quad_point_frame_2 = quad_point_2;
          quad_point_1.fill(0.0);
          quad_point_2.fill(0.0);
          for (std::size_t i = 0; i < DIM; i++) {
            for (std::size_t j = 0; j < DIM; j++) {
              quad_point_1[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_1[j];
              quad_point_2[i] +=
                  this->initial_rotation(i, j) * quad_point_frame_2[j];
            }
          }

          if (displaced_point_index_1 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
          }
          if (displaced_point_index_2 == noof_neighbours) {
            const auto sigma = exp(-atom_thermal_coordinates[1]);
            array_ops::as_eigen(sampling_atom_shift) +=
                sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
          }

          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++) {
            neighbour_shift.fill(0.0);

            if (displaced_point_index_1 < noof_neighbours) {
              if (displaced_point_index_1 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_1);
              }
            }

            if (displaced_point_index_2 < noof_neighbours) {
              if (displaced_point_index_2 == neighbour_idx) {
                const auto sigma =
                    exp(-neighbours_thermal_coordinates[neighbour_idx][1]);
                array_ops::as_eigen(neighbour_shift) +=
                    sqrt(2.0 * sigma) * array_ops::as_eigen(quad_point_2);
              }
            }

            const auto r_ij_mag =
                (array_ops::as_eigen(neighbour_shift) +
                 array_ops::as_eigen(neighbours[neighbour_idx]) -
                 array_ops::as_eigen(sampling_atom_shift) -
                 array_ops::as_eigen(atom))
                    .norm();

            if (std::isnan(r_ij_mag)) {
              throw detail::format_error(
                  neighbours_thermal_coordinates[neighbour_idx],
                  neighbour_shift, neighbours[neighbour_idx],
                  sampling_atom_shift, atom, atom_thermal_coordinates);
            }

            neighbour_shifts.push_back(neighbour_shift);
          }

          array_ops::as_eigen(shifted_sampling_atom) =
              array_ops::as_eigen(atom) +
              array_ops::as_eigen(sampling_atom_shift);
          for (std::size_t i = 0; i < noof_neighbours; i++)
            array_ops::as_eigen(shifted_neighbours[i]) =
                array_ops::as_eigen(neighbours[i]) +
                array_ops::as_eigen(neighbour_shifts[i]);
          this->get_concentration_forces(
              quad_forces_atom, quad_forces_neighbors, shifted_sampling_atom,
              shifted_neighbours, atom_data_point, neighbours_data_points);

          array_ops::as_eigen(forces_atom) +=
              w_quad_2 * array_ops::as_eigen(quad_forces_atom);
          for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
               neighbour_idx++) {
            array_ops::as_eigen(forces_neighbours[neighbour_idx]) +=
                w_quad_2 *
                array_ops::as_eigen(quad_forces_neighbors[neighbour_idx]);
          }
        }
      }
    }
    if (total_p2_count != noof_quadrature_samples_p2)
      throw exception::QCException{
          "MEAM.c: total_P2_count != noof_quadrature_samples_P2"};
  }
};

extern template struct Material<3, 1>;
extern template struct Material<3, 2>;
extern template struct Material<3, 3>;
extern template struct Material<3, 4>;
extern template struct Material<3, 5>;

} // namespace materials
