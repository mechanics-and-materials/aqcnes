// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Class to use potentials available in the openKIM repository
 * https://kim-api.readthedocs.io/en/devel/ex__test___ar__fcc__cluster__cpp_8cpp_source.html
 * @authors S. Saxena, P. Gupta, M. Spinola
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "materials/cluster_model.hpp"
#include <string>

namespace materials {

class KimFccBase {

public:
  struct Model;
  struct ComputeArguments;

protected:
  constexpr static std::size_t DIMENSION = 3;
  Model *kim_cluster_model{};
  ComputeArguments *compute_arguments{};
  double min_rho{};
  std::vector<int> species_codes{};

  using EdgeVector = std::array<std::size_t, 2>;
  using Point = std::array<double, DIMENSION>;
  using Matrix = Eigen::Matrix<double, DIMENSION, DIMENSION>;

public:
  std::vector<std::string> species{};

  KimFccBase(const std::string &kim_model_name,
             std::vector<std::string> species_value);

  KimFccBase(const KimFccBase &) = default;
  KimFccBase(KimFccBase &&) = default;
  [[nodiscard]] KimFccBase &operator=(const KimFccBase &) = default;
  [[nodiscard]] KimFccBase &operator=(KimFccBase &&) = default;
  ~KimFccBase();

  void set_kim_object(const std::string &model_name);

  void kim_species_support();

  [[nodiscard]] double get_neighbor_interaction_cutoff_radius() const;
  [[nodiscard]] double
  get_cluster_energy(const Point &atom,
                     const std::vector<Point> &neighbours) const;

  void get_cluster_forces(std::vector<Point> &forces, const Point &atom,
                          const std::vector<Point> &neighbours) const;

  static int
  get_cluster_neigh(void *const data_object, int const number_of_neighbor_lists,
                    double const *const cutoffs, int const neighbor_list_index,
                    int const particle_number, int *const number_of_neighbors,
                    int const **const neighbors_of_particle);
};

template <std::size_t Dimension, std::size_t Noi>
class KimFcc : public ClusterModel<Dimension, Noi> {
  static_assert(Dimension == 3);
  KimFccBase base;

  using Point = std::array<double, Dimension>;
  using NodalDataPoint = meshing::NodalDataMultispecies<Noi>;
  using ConcentrationPoint = std::array<double, Noi>;

public:
  KimFcc(const std::string &kim_model_name, const double mass,
         const std::vector<std::string> &species_value)
      : base{kim_model_name, species_value} {
    this->atomic_masses = std::vector<double>{mass};
    this->neighbor_cutoff_radius =
        this->base.get_neighbor_interaction_cutoff_radius();
    if constexpr (Dimension == 2) {
      this->lattice_structure = lattice::LatticeStructure::HCP;
    } else if constexpr (Dimension == 3) {
      this->lattice_structure = lattice::LatticeStructure::FCC;
    }
  }
  void
  get_concentration_forces(ConcentrationPoint &,
                           std::vector<ConcentrationPoint> &, const Point &,
                           const std::vector<Point> &, const NodalDataPoint &,
                           const std::vector<NodalDataPoint> &) const override {
  }
  [[nodiscard]] double
  get_cluster_energy(const Point &atom, const std::vector<Point> &neighbours,
                     const NodalDataPoint &,
                     const std::vector<NodalDataPoint> &) const override {
    return this->base.get_cluster_energy(atom, neighbours);
  }
  void get_cluster_forces(
      std::vector<geometry::Point<3>> &forces, const geometry::Point<3> &atom,
      const std::vector<geometry::Point<3>> &neighbours,
      const meshing::NodalDataMultispecies<Noi> &,
      const std::vector<meshing::NodalDataMultispecies<Noi>> &) const override {
    this->base.get_cluster_forces(forces, atom, neighbours);
  }
};
} // namespace materials
