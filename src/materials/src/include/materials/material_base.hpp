// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation for a material base struct
 * @author M. Spinola, G. Bräunlich
 */

#pragma once

#include "lattice/lattice_structure.hpp"
#include <array>
#include <vector>

namespace materials {

/**
 * @brief Unspecialized material class which only holds generic
 * material properties.
 *
 * @ref meshing::QCMesh needs not more information than available in this class.
 */
template <std::size_t Dimension> struct MaterialBase {
  constexpr static std::size_t DIMENSION = Dimension;

  std::array<std::array<double, Dimension>, Dimension> basis_vectors{};
  std::array<double, Dimension> lattice_parameter_vec{};
  std::vector<std::array<double, Dimension>> offset_coeffs{};
  std::vector<double> atomic_masses{};
  double neighbor_cutoff_radius{};
  lattice::LatticeStructure lattice_type = lattice::LatticeStructure::FCC;
  lattice::LatticeStructure lattice_structure = lattice::LatticeStructure::FCC;
};

} // namespace materials
