// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief a material class based on Yukawa potential for silica glasses
 * @authors M. Spinola, S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "materials/cluster_model.hpp"
#include "materials/material.hpp"

namespace materials {

namespace array_ops = qcmesh::array_ops;

// species index Silicon - 0 ; Oxygen - 1
template <std::size_t DIM, std::size_t Noi>
class SilicaYukawa : public ClusterModel<DIM, Noi> {

public:
  using Point = std::array<double, DIM>;
  using NodalDataPoint = meshing::NodalDataMultispecies<Noi>;
  using ConcentrationPoint = std::array<double, Noi>;

  const std::array<double, 3> sigs{{2.250, 1.075, 0.900}};
  const std::array<double, 3> qs{{1.500, -1.000, 0.670}};
  std::array<double, 3> values_at_cutoff{};
  std::array<double, 3> derivatives_at_cutoff{};
  const double kappa = 1.0 / 9.8;

  SilicaYukawa() {
    this->neighbor_cutoff_radius = 9.8;
    this->atomic_masses = std::vector<double>{1.0, 0.57};

    for (std::size_t i = 0; i < 3; i++) {
      values_at_cutoff[i] =
          get_pair_potential(this->neighbor_cutoff_radius, sigs[i], qs[i]);
      derivatives_at_cutoff[i] = get_pair_potential_derivative(
          this->neighbor_cutoff_radius, sigs[i], qs[i]);
    }
  }

  [[nodiscard]] double get_pair_potential(const double r, const double sig,
                                          const double q) const {
    return (pow((sig / r), 12.0) + q / r * exp(-kappa * r));
  }

  [[nodiscard]] double get_pair_potential_derivative(const double r,
                                                     const double sig,
                                                     const double q) const {

    return (-12.0) / sig * pow((sig / r), 13.0) -
           kappa * q / r * exp(-kappa * r) - q / (r * r) * exp(-kappa * r);
  }

  void
  get_concentration_forces(ConcentrationPoint &,
                           std::vector<ConcentrationPoint> &, const Point &,
                           const std::vector<Point> &, const NodalDataPoint &,
                           const std::vector<NodalDataPoint> &) const override {
  }

  [[nodiscard]] double
  get_cluster_energy(const Point &atom, const std::vector<Point> &neighbours,
                     const NodalDataPoint &atom_data_point,
                     const std::vector<NodalDataPoint> &neighbours_data_points)
      const override {
    auto energy = 0.0;
    auto shifting_value = 0.0;
    for (std::size_t neigh_idx = 0; neigh_idx < neighbours.size();
         neigh_idx++) {
      auto a_value = 0.0;
      auto q_value = 0.0;

      if (atom_data_point.impurity_concentrations[0] == 1.0) {
        if (neighbours_data_points[neigh_idx].impurity_concentrations[0] ==
            1.0) {
          a_value = 2.25;
          q_value = 1.50;
          shifting_value = values_at_cutoff[0];
        } else if (neighbours_data_points[neigh_idx]
                       .impurity_concentrations[0] == 0.0) {

          a_value = 1.075;
          q_value = -1.00;
          shifting_value = values_at_cutoff[1];
        }
      } else if (atom_data_point.impurity_concentrations[0] == 0.0) {

        if (neighbours_data_points[neigh_idx].impurity_concentrations[0] ==
            1.0) {
          a_value = 1.075;
          q_value = -1.00;
          shifting_value = values_at_cutoff[1];
        } else if (neighbours_data_points[neigh_idx]
                       .impurity_concentrations[0] == 0.0) {
          a_value = 0.9;
          q_value = 0.67;
          shifting_value = values_at_cutoff[2];
        }
      }
      if (a_value == 0.0)
        throw exception::QCException(
            "Detected non 0 or 1 concentration in Silica potential. Atoms can "
            "either be Si or O.");

      energy +=
          (get_pair_potential(array_ops::distance(atom, neighbours[neigh_idx]),
                              a_value, q_value) -
           shifting_value);
    }
    return energy;
  }

  void get_cluster_forces(std::vector<Point> &forces, const Point &atom,
                          const std::vector<Point> &neighbours,
                          const NodalDataPoint &atom_data_point,
                          const std::vector<NodalDataPoint>
                              &neighbours_data_points) const override {
    forces.clear();
    forces.reserve(neighbours.size());
    auto derivative_shifting_value = 0.0;

    for (std::size_t neigh_idx = 0; neigh_idx < neighbours.size();
         neigh_idx++) {
      auto a_value = 0.0;
      auto q_value = 0.0;

      if (atom_data_point.impurity_concentrations[0] == 1.0) {
        if (neighbours_data_points[neigh_idx].impurity_concentrations[0] ==
            1.0) {
          a_value = 2.25;
          q_value = 1.50;
          derivative_shifting_value = derivatives_at_cutoff[0];
        } else if (neighbours_data_points[neigh_idx]
                       .impurity_concentrations[0] == 0.0) {
          a_value = 1.075;
          q_value = -1.00;
          derivative_shifting_value = derivatives_at_cutoff[1];
        }
      } else if (atom_data_point.impurity_concentrations[0] == 0.0) {
        if (neighbours_data_points[neigh_idx].impurity_concentrations[0] ==
            1.0) {
          a_value = 1.075;
          q_value = -1.00;
          derivative_shifting_value = derivatives_at_cutoff[1];
        } else if (neighbours_data_points[neigh_idx]
                       .impurity_concentrations[0] == 0.0) {
          a_value = 0.9;
          q_value = 0.67;
          derivative_shifting_value = derivatives_at_cutoff[2];
        }
      }
      if (a_value == 0.0)
        throw exception::QCException(
            "Detected non 0 or 1 concentration in Silica potential. Atoms can "
            "either be Si or O.");

      const auto r_ij = array_ops::distance(atom, neighbours[neigh_idx]);
      const auto f_ij =
          -(get_pair_potential_derivative(r_ij, a_value, q_value) -
            derivative_shifting_value);

      auto temp = Point{};
      array_ops::as_eigen(temp) = f_ij *
                                  (array_ops::as_eigen(neighbours[neigh_idx]) -
                                   array_ops::as_eigen(atom)) /
                                  r_ij;
      forces.push_back(temp);
    }
  }
};

} // namespace materials
