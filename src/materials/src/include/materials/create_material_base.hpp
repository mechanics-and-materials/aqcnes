// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation for creating the material base struct
 */

#pragma once

#include "lattice/lattice_cache.hpp"
#include "materials/material.hpp"

namespace materials {

/**
 * @brief Create a @ref MaterialBase from a @ref Material instance.
 *
 * This is used wherever either a @ref Material or @ref MaterialBase are possible.
 */
template <std::size_t Dimension, std::size_t Noi>
materials::MaterialBase<Dimension>
create_material_base(const materials::Material<Dimension, Noi> &material) {
  return *static_cast<const materials::MaterialBase<Dimension> *>(&material);
}

/**
 * @brief Create a @ref MaterialBase from a @ref MaterialBase instance.
 *
 * This is used wherever either a @ref Material or @ref MaterialBase are possible.
 */
template <std::size_t Dimension>
materials::MaterialBase<Dimension>
create_material_base(const materials::MaterialBase<Dimension> &material) {
  return material;
}

/**
 * @brief Create a @ref MaterialBase from a @ref LatticeCache instance and parameters which usually are set by a @ref ClusterModel instance.
 */
template <std::size_t Dimension>
materials::MaterialBase<Dimension>
create_material_base(const lattice::LatticeCache<Dimension> &cache,
                     const double neighbor_cutoff_radius,
                     const std::vector<double> &atomic_masses,
                     const lattice::LatticeStructure lattice_structure) {
  return {
      .basis_vectors = cache.rotated_basis_matrix,
      .lattice_parameter_vec = cache.lattice_parameters,
      .offset_coeffs = cache.seed_point_offsets,
      .atomic_masses = atomic_masses,
      .neighbor_cutoff_radius = neighbor_cutoff_radius,
      .lattice_type = cache.lattice_type,
      .lattice_structure = lattice_structure,
  };
}

} // namespace materials
