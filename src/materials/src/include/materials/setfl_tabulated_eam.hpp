// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief a material class based on tabulated SETFL or FUNCFL EAM potentials
 * @authors S. Saxena, P. Gupta, M. Spinola
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "materials/cluster_model.hpp"
#include "materials/material.hpp"
#include "utils/evaluate_polynomial.hpp"
#include <random>

namespace materials {

namespace array_ops = qcmesh::array_ops;

struct TabulatedEmbeddedAtomInformation {
  std::vector<std::pair<double, double>> embedding_function;
  std::vector<std::pair<double, double>> pair_potential;
  std::vector<std::pair<double, double>> density_function;
};

template <typename Interpolator, std::size_t DIM, std::size_t Noi>
class SetflTabulatedEAM : public ClusterModel<DIM, Noi> {

public:
  using Point = std::array<double, DIM>;
  using NodalDataPoint = meshing::NodalDataMultispecies<Noi>;
  using ConcentrationPoint = std::array<double, Noi>;

  std::vector<Interpolator> pair_potential_interpolators;
  std::vector<Interpolator> embedding_function_interpolators;
  std::vector<Interpolator> density_function_interpolators;
  std::vector<double> cell_side_lengths;
  std::vector<lattice::LatticeStructure> lattice_structures;
  std::vector<double> pair_potential_input_min;
  std::vector<double> pair_potential_input_max;
  std::vector<double> embedding_function_input_min;
  std::vector<double> embedding_function_input_max;
  std::vector<double> density_function_input_min;
  std::vector<double> density_function_input_max;
  std::vector<std::string> lattice_type_names;

  std::size_t number_of_species, number_of_pair_interactions, start_idx;

  TabulatedEmbeddedAtomInformation tabulated_embedded_atom_information;
  std::string material_name;

  std::vector<std::vector<double>> embedding_interpolating_polynomial_coeffs;

  SetflTabulatedEAM(
      const std::string &filename, const std::string &, const bool if_funcfl,
      const bool simulating_vacancy, const bool output_potential_log,
      const std::vector<std::vector<double>> &embedding_interpolation_coeffs)
      : number_of_species(simulating_vacancy ? Noi : (Noi + 1)),
        number_of_pair_interactions(number_of_species *
                                    (number_of_species + 1) / 2),
        start_idx(simulating_vacancy ? 1 : 0) {

    this->output_potential_log = output_potential_log;
    this->atomic_masses.resize(number_of_species);

    // resize member vectors according to the number of species
    pair_potential_interpolators.resize(number_of_pair_interactions);
    embedding_function_interpolators.resize(number_of_species);
    density_function_interpolators.resize(number_of_species);
    cell_side_lengths.resize(number_of_species);
    lattice_structures.resize(number_of_species);
    pair_potential_input_min.resize(number_of_pair_interactions);
    pair_potential_input_max.resize(number_of_pair_interactions);
    embedding_function_input_min.resize(number_of_species);
    embedding_function_input_max.resize(number_of_species);
    density_function_input_min.resize(number_of_species);
    density_function_input_max.resize(number_of_species);
    lattice_type_names.resize(number_of_species);

    // parse file, populate data structures

    std::ifstream file;
    file.open(filename, std::ios::in);
    if (!file)
      throw exception::QCException{"Could not open " + filename};

    std::string line;

    unsigned int noof_embedding_entries = 0;
    unsigned int noof_density_and_potential_entries = 0;
    double rho_spacing{};
    double r_spacing{};

    if (!if_funcfl) {
      // skip the first three lines
      std::array<std::string, 3> lines;
      for (auto &l : lines)
        getline(file, l);
      material_name = lines[0] + " " + lines[1] + " " + lines[2];

      // make sure number of elements/species are consistent
      getline(file, line);

      const auto [numberOfElements, _unused] =
          utils::string_split<unsigned int, std::string_view>(
              line, utils::AnyOf{" \t"}, utils::AnyOf{" \t"});

      if (numberOfElements != number_of_species)
        throw exception::QCException{"material.h: got wrong numberOfElements"};

      // parse Nrho, drho, Nr, dr, cutoff
      getline(file, line);

      std::tie(noof_embedding_entries, rho_spacing,
               noof_density_and_potential_entries, r_spacing,
               this->neighbor_cutoff_radius) =
          utils::string_split<unsigned int, double, unsigned int, double,
                              double>(line, ' ', ' ');

      const unsigned int number_of_embedding_entries = noof_embedding_entries;
      const unsigned int number_of_density_and_potential_entries =
          noof_density_and_potential_entries;
      const double embedding_spacing = rho_spacing;
      const double density_and_potential_spacing = r_spacing;

      for (std::size_t si = 0; si < number_of_species; si++) {

        // parse Atomic number, mass, lattice constant, lattice type
        getline(file, line);
        auto unused = std::string{};
        std::tie(unused, this->atomic_masses[si], cell_side_lengths[si],
                 lattice_type_names[si]) =
            utils::string_split<std::string, double, double, std::string>(
                line, ' ', ' ');

        // convert lattice structure to upper case
        std::transform(lattice_type_names[si].begin(),
                       lattice_type_names[si].end(),
                       lattice_type_names[si].begin(), ::toupper);
        if (strcmp(lattice_type_names[si].c_str(), "FCC") == 0) {
          if (DIM == 3) {
            lattice_structures[si] = lattice::LatticeStructure::FCC;
          } else {
            lattice_structures[si] = lattice::LatticeStructure::HCP;
          }
        } else if (strcmp(lattice_type_names[si].c_str(), "BCC") == 0) {
          lattice_structures[si] = lattice::LatticeStructure::BCC;
        } else if (strcmp(lattice_type_names[si].c_str(), "HCP") == 0) {
          lattice_structures[si] = lattice::LatticeStructure::HCP;
        } else {
          throw exception::QCException{"Invalid lattice type: " +
                                       lattice_type_names[si]};
        }

        // parse electron density and embedding function values into storage;
        std::vector<double> numbers{};
        while (numbers.size() < (number_of_embedding_entries +
                                 number_of_density_and_potential_entries)) {
          getline(file, line);
          utils::string_split_many_to_vec(line, numbers, ' ', ' ');
        }

        if (numbers.size() != (number_of_embedding_entries +
                               number_of_density_and_potential_entries))
          throw exception::QCException{
              "Wrong number of entries in SETFL file."};

        // parse the embedding function values
        unsigned int number_index = 0;
        for (unsigned int index = 0; index < number_of_embedding_entries;
             ++index, ++number_index) {
          const double rho = double(index) * embedding_spacing;
          const std::pair<double, double> data_pair =
              std::make_pair(rho, numbers[number_index]);
          tabulated_embedded_atom_information.embedding_function.push_back(
              data_pair);
        }

        // parse the electron density values
        for (unsigned int index = 0;
             index < number_of_density_and_potential_entries;
             ++index, ++number_index) {
          const double radius = double(index) * density_and_potential_spacing;
          const std::pair<double, double> data_pair =
              std::make_pair(radius, numbers[number_index]);
          tabulated_embedded_atom_information.density_function.push_back(
              data_pair);
        }
      } // end for loop over different species

      // Now parse pair interaction values
      std::vector<double> numbers{};
      while (numbers.size() < (number_of_pair_interactions *
                               number_of_density_and_potential_entries)) {
        getline(file, line);
        utils::string_split_many_to_vec(line, numbers, ' ', ' ');
      }
      if (numbers.size() !=
          number_of_pair_interactions * number_of_density_and_potential_entries)
        throw exception::QCException{"Wrong number of entries in SETFL file."};
      unsigned int number_index = 0;
      for (std::size_t pi = 0; pi < number_of_pair_interactions; pi++) {
        // parse the pair potential values
        for (unsigned int index = 0;
             index < number_of_density_and_potential_entries;
             ++index, ++number_index) {
          const double radius = double(index) * density_and_potential_spacing;
          if (std::abs(radius) > 1e-6) {
            const double pair_potential_value =
                0.5 * numbers[number_index] / radius;
            const std::pair<double, double> data_pair =
                std::make_pair(radius, pair_potential_value);
            tabulated_embedded_atom_information.pair_potential.push_back(
                data_pair);
          }
        }

      } // end loop over pair interactions
    } else {
      if (number_of_species > 1)
        throw exception::QCException{
            "material.h: Multi-element FUNCFL files not supported"};
      // skip the first line
      getline(file, material_name);

      // parse Atomic number, mass, lattice constant, lattice type
      getline(file, line);
      auto unused = std::string_view{};
      std::tie(unused, this->atomic_masses[0], cell_side_lengths[0],
               lattice_type_names[0]) =
          utils::string_split<std::string_view, double, double, std::string>(
              line, ' ', ' ');
      // parse Nrho, drho, Nr, dr, cutoff
      getline(file, line);
      std::tie(noof_embedding_entries, rho_spacing,
               noof_density_and_potential_entries, r_spacing,
               this->neighbor_cutoff_radius) =
          utils::string_split<unsigned int, double, unsigned int, double,
                              double>(line, ' ', ' ');

      const unsigned int number_of_embedding_entries = noof_embedding_entries;
      const unsigned int number_of_density_and_potential_entries =
          noof_density_and_potential_entries;
      const double embedding_spacing = rho_spacing;
      const double density_and_potential_spacing = r_spacing;

      // convert lattice structure to upper case
      std::transform(lattice_type_names[0].begin(), lattice_type_names[0].end(),
                     lattice_type_names[0].begin(), ::toupper);
      if (strcmp(lattice_type_names[0].c_str(), "FCC") == 0) {
        if (DIM == 3) {
          lattice_structures[0] = lattice::LatticeStructure::FCC;
        } else {
          lattice_structures[0] = lattice::LatticeStructure::HCP;
        }
      } else if (strcmp(lattice_type_names[0].c_str(), "BCC") == 0) {
        lattice_structures[0] = lattice::LatticeStructure::BCC;
      } else if (strcmp(lattice_type_names[0].c_str(), "HCP") == 0) {
        lattice_structures[0] = lattice::LatticeStructure::HCP;
      } else {
        throw exception::QCException{"Invalid lattice type: " +
                                     lattice_type_names[0]};
      }

      // parse all of the numbers into storage;
      std::vector<double> numbers;
      numbers.reserve(number_of_embedding_entries +
                      2 * number_of_density_and_potential_entries);

      while (getline(file, line))
        utils::string_split_many_to_vec(line, numbers, ' ', ' ');
      file.close();

      if (numbers.size() != number_of_embedding_entries +
                                2 * number_of_density_and_potential_entries)
        throw exception::QCException{"Wrong number of entries in FUNCFL file."};

      // parse the embedding function values
      unsigned int number_index = 0;
      for (unsigned int index = 0; index < number_of_embedding_entries;
           ++index, ++number_index) {
        const double rho = double(index) * embedding_spacing;
        const std::pair<double, double> data_pair =
            std::make_pair(rho, numbers[number_index]);
        tabulated_embedded_atom_information.embedding_function.push_back(
            data_pair);
      }

      // parse the effective charge function values
      for (unsigned int index = 0;
           index < number_of_density_and_potential_entries;
           ++index, ++number_index) {
        const double radius = double(index) * density_and_potential_spacing;
        if (std::abs(radius) > 1e-6) {
          const double pair_potential_value = 0.5 * 27.2 * 0.529 *
                                              numbers[number_index] *
                                              numbers[number_index] / radius;
          const std::pair<double, double> data_pair =
              std::make_pair(radius, pair_potential_value);
          tabulated_embedded_atom_information.pair_potential.push_back(
              data_pair);
        }
      }

      // parse the electron density values
      for (unsigned int index = 0;
           index < number_of_density_and_potential_entries;
           ++index, ++number_index) {
        const double radius = double(index) * density_and_potential_spacing;
        const std::pair<double, double> data_pair =
            std::make_pair(radius, numbers[number_index]);
        tabulated_embedded_atom_information.density_function.push_back(
            data_pair);
      }
    } // end of if-else for SETFL/FUNFL files

    this->lattice_structure = lattice_structures[0];
    const unsigned int number_of_embedding_entries = noof_embedding_entries;
    const unsigned int number_of_density_and_potential_entries =
        noof_density_and_potential_entries;

    // initialize vectors of interpolators
    for (std::size_t si = 0; si < number_of_species; si++) {
      // initialize embedding function interpolator(s)
      std::vector<std::pair<double, double>> paired_values_embedding_function(
          0);
      std::copy(tabulated_embedded_atom_information.embedding_function.begin() +
                    static_cast<int64_t>(si) * number_of_embedding_entries,
                tabulated_embedded_atom_information.embedding_function.begin() +
                    static_cast<int64_t>(si + 1) * number_of_embedding_entries,
                std::back_inserter(paired_values_embedding_function));
      embedding_function_interpolators[si].initialize(
          paired_values_embedding_function);
      embedding_function_input_min[si] =
          embedding_function_interpolators[si].get_domain_min();
      embedding_function_input_max[si] =
          embedding_function_interpolators[si].get_domain_max();

      // initialize electron density interpolator(s)
      std::vector<std::pair<double, double>> paired_values_electron_density(0);
      std::copy(tabulated_embedded_atom_information.density_function.begin() +
                    static_cast<int64_t>(si) *
                        number_of_density_and_potential_entries,
                tabulated_embedded_atom_information.density_function.begin() +
                    static_cast<int64_t>(si + 1) *
                        number_of_density_and_potential_entries,
                std::back_inserter(paired_values_electron_density));
      density_function_interpolators[si].initialize(
          paired_values_electron_density);
      density_function_input_min[si] =
          density_function_interpolators[si].get_domain_min();
      density_function_input_max[si] =
          density_function_interpolators[si].get_domain_max();
    }

    // initialize pair potential interpolator(s)
    for (std::size_t pi = 0; pi < number_of_pair_interactions; pi++) {
      std::vector<std::pair<double, double>> paired_values_pair_potential(0);
      std::copy(tabulated_embedded_atom_information.pair_potential.begin() +
                    static_cast<int64_t>(pi) *
                        (number_of_density_and_potential_entries - 1),
                tabulated_embedded_atom_information.pair_potential.begin() +
                    static_cast<int64_t>(pi + 1) *
                        (number_of_density_and_potential_entries - 1),
                std::back_inserter(paired_values_pair_potential));
      pair_potential_interpolators[pi].initialize(paired_values_pair_potential);
      pair_potential_input_min[pi] =
          pair_potential_interpolators[pi].get_domain_min();
      pair_potential_input_max[pi] =
          pair_potential_interpolators[pi].get_domain_max();
    }

    // initialize embedding interpolation polynomials
    if (simulating_vacancy) {
      if (embedding_interpolation_coeffs.size() != Noi)
        throw exception::QCException("Incorrect number of interpolation "
                                     "coefficients provided in user input");
    } else {
      if (embedding_interpolation_coeffs.size() != (Noi + 1))
        throw exception::QCException("Incorrect number of interpolation "
                                     "coefficients provided in user input");
    }

    embedding_interpolating_polynomial_coeffs.clear();
    embedding_interpolating_polynomial_coeffs.resize(
        embedding_interpolation_coeffs.size(), std::vector<double>{});
    for (std::size_t si = 0; si < embedding_interpolation_coeffs.size(); si++)
      std::copy(embedding_interpolation_coeffs[si].begin(),
                embedding_interpolation_coeffs[si].end(),
                back_inserter(embedding_interpolating_polynomial_coeffs[si]));
  }

  [[nodiscard]] TabulatedEmbeddedAtomInformation
  get_tabulated_embedded_atom_information() const {
    return tabulated_embedded_atom_information;
  }

  [[nodiscard]] double
  compute_pair_potential(const double r, const NodalDataPoint &data_point_atom,
                         const NodalDataPoint &data_point_neighbor) const {
    for (std::size_t pi = 0; pi < number_of_pair_interactions; pi++)
      if (r < pair_potential_input_min[pi])
        throw exception::QCException{"SETFL: r out of interpolator bounds"};

    double conc_sum_i = data_point_atom.impurity_concentrations[0];
    double conc_sum_j = data_point_neighbor.impurity_concentrations[0];

    for (std::size_t i = 1; i < Noi; i++)
      conc_sum_i += data_point_atom.impurity_concentrations[i];
    for (std::size_t i = 1; i < Noi; i++)
      conc_sum_j += data_point_neighbor.impurity_concentrations[i];

    double value = 0.0;
    std::size_t ctr = 0;
    for (std::size_t i = start_idx; i < Noi; i++) {
      value += (data_point_atom.impurity_concentrations[i] *
                data_point_neighbor.impurity_concentrations[i] *
                ((r > pair_potential_input_max[ctr])
                     ? 0
                     : pair_potential_interpolators[ctr].evaluate(r)));
      ctr++;
      for (std::size_t j = i + 1; j < Noi; j++) {
        value += ((data_point_atom.impurity_concentrations[i] *
                       data_point_neighbor.impurity_concentrations[j] +
                   data_point_atom.impurity_concentrations[j] *
                       data_point_neighbor.impurity_concentrations[i]) *
                  ((r > pair_potential_input_max[ctr])
                       ? 0
                       : pair_potential_interpolators[ctr].evaluate(r)));
        ctr++;
      }
      value +=
          ((data_point_atom.impurity_concentrations[i] * (1 - conc_sum_j) +
            (1 - conc_sum_i) * data_point_neighbor.impurity_concentrations[i]) *
           ((r > pair_potential_input_max[ctr])
                ? 0
                : pair_potential_interpolators[ctr].evaluate(r)));
      ctr++;
    }
    value += ((1 - conc_sum_i) * (1 - conc_sum_j) *
              ((r > pair_potential_input_max[ctr])
                   ? 0
                   : pair_potential_interpolators[ctr].evaluate(r)));
    return value;
  }

  [[nodiscard]] inline double compute_pair_potential_derivative(
      const double r, const NodalDataPoint &data_point_atom,
      const NodalDataPoint &data_point_neighbor) const {
    for (std::size_t pi = 0; pi < number_of_pair_interactions; pi++)
      if (r < pair_potential_input_min[pi])
        throw exception::QCException{"SETFL: r out of interpolator bounds"};
    double conc_sum_i = data_point_atom.impurity_concentrations[0];
    double conc_sum_j = data_point_neighbor.impurity_concentrations[0];

    for (std::size_t i = 1; i < Noi; i++)
      conc_sum_i += data_point_atom.impurity_concentrations[i];
    for (std::size_t i = 1; i < Noi; i++)
      conc_sum_j += data_point_neighbor.impurity_concentrations[i];

    double value = 0.0;
    std::size_t ctr = 0;
    for (std::size_t i = start_idx; i < Noi; i++) {
      value +=
          (data_point_atom.impurity_concentrations[i] *
           data_point_neighbor.impurity_concentrations[i] *
           ((r > pair_potential_input_max[ctr])
                ? 0
                : pair_potential_interpolators[ctr].evaluate_derivative(r)));
      ctr++;
      for (std::size_t j = i + 1; j < Noi; j++) {
        value +=
            ((data_point_atom.impurity_concentrations[i] *
                  data_point_neighbor.impurity_concentrations[j] +
              data_point_atom.impurity_concentrations[j] *
                  data_point_neighbor.impurity_concentrations[i]) *
             ((r > pair_potential_input_max[ctr])
                  ? 0
                  : pair_potential_interpolators[ctr].evaluate_derivative(r)));
        ctr++;
      }
      value +=
          ((data_point_atom.impurity_concentrations[i] * (1 - conc_sum_j) +
            (1 - conc_sum_i) * data_point_neighbor.impurity_concentrations[i]) *
           ((r > pair_potential_input_max[ctr])
                ? 0
                : pair_potential_interpolators[ctr].evaluate_derivative(r)));
      ctr++;
    }
    value += ((1 - conc_sum_i) * (1 - conc_sum_j) *
              ((r > pair_potential_input_max[ctr])
                   ? 0
                   : pair_potential_interpolators[ctr].evaluate_derivative(r)));
    return value;
  }

  [[nodiscard]] inline double
  compute_electron_density_function(const double r,
                                    const NodalDataPoint &data_point) const {
    for (std::size_t si = 0; si < number_of_species; si++)
      if (r < density_function_input_min[si])
        throw exception::QCException{"SETFL: r out of interpolator bounds"};

    double value = 0.0;
    double conc_sum = 0.0;
    std::size_t ctr = 0;
    for (std::size_t si = 0; si < Noi; si++)
      conc_sum += data_point.impurity_concentrations[si];

    for (std::size_t si = start_idx; si < Noi; si++) {
      value += ((r > density_function_input_max[ctr])
                    ? 0.0
                    : density_function_interpolators[ctr].evaluate(r)) *
               data_point.impurity_concentrations[si];
      ctr++;
    }

    value += ((r > density_function_input_max[ctr])
                  ? 0.0
                  : density_function_interpolators[ctr].evaluate(r)) *
             (1 - conc_sum);
    return value;
  }

  [[nodiscard]] inline double compute_electron_density_function_derivative(
      const double r, const NodalDataPoint &data_point) const {
    for (std::size_t si = 0; si < number_of_species; si++)
      if (r < density_function_input_min[si])
        throw exception::QCException{"SETFL: r out of interpolator bounds"};

    double value = 0.0;
    double conc_sum = 0.0;
    std::size_t ctr = 0;
    for (std::size_t si = 0; si < Noi; si++)
      conc_sum += data_point.impurity_concentrations[si];

    for (std::size_t si = start_idx; si < Noi; si++) {
      value +=
          ((r > density_function_input_max[ctr])
               ? 0.0
               : density_function_interpolators[ctr].evaluate_derivative(r)) *
          data_point.impurity_concentrations[si];
      ctr++;
    }

    value +=
        ((r > density_function_input_max[ctr])
             ? 0.0
             : density_function_interpolators[ctr].evaluate_derivative(r)) *
        (1 - conc_sum);
    return value;
  }

  [[nodiscard]] inline double
  compute_embedding_function(const double rho,
                             const NodalDataPoint &data_point) const {

    std::ofstream f_potential_log;
    if (this->output_potential_log)
      f_potential_log.open("./potential_log.dat", std::ios::app);

    for (std::size_t si = 0; si < number_of_species; si++)
      if (rho < embedding_function_input_min[si])
        throw exception::QCException{"SETFL: rho out of interpolator bounds"};
    double conc_sum = 0.0;
    double value = 0.0;
    std::size_t ctr = 0;
    for (std::size_t si = 0; si < Noi; si++)
      conc_sum += data_point.impurity_concentrations[si];

    for (std::size_t si = start_idx; si < Noi; si++) {
      const auto embedding =
          ((rho > embedding_function_input_max[ctr])
               ? 0.0
               : embedding_function_interpolators[ctr].evaluate(rho));
      value += embedding * utils::evaluate_polynomial(
                               embedding_interpolating_polynomial_coeffs[ctr],
                               data_point.impurity_concentrations[si]);
      ctr++;
      if (this->output_potential_log)
        f_potential_log << embedding << " ";
    }

    const auto embedding =
        ((rho > embedding_function_input_max[ctr])
             ? 0.0
             : embedding_function_interpolators[ctr].evaluate(rho));
    value += embedding * utils::evaluate_polynomial(
                             embedding_interpolating_polynomial_coeffs[ctr],
                             (1 - conc_sum));

    if (this->output_potential_log) {
      f_potential_log << embedding << " ";
      f_potential_log.close();
    }

    return value;
  }

  [[nodiscard]] inline double compute_embedding_function_derivative(
      const double rho, const NodalDataPoint &data_point) const {
    for (std::size_t si = 0; si < number_of_species; si++)
      if (rho < embedding_function_input_min[si])
        throw exception::QCException{"SETFL: rho out of interpolator bounds"};

    double value = 0.0;
    double conc_sum = 0.0;
    std::size_t ctr = 0;
    for (std::size_t si = 0; si < Noi; si++)
      conc_sum += data_point.impurity_concentrations[si];

    for (std::size_t si = start_idx; si < Noi; si++) {
      value += ((rho > embedding_function_input_max[ctr])
                    ? 0.0
                    : embedding_function_interpolators[ctr].evaluate_derivative(
                          rho)) *
               utils::evaluate_polynomial(
                   embedding_interpolating_polynomial_coeffs[ctr],
                   data_point.impurity_concentrations[si]);
      ctr++;
    }

    value +=
        ((rho > embedding_function_input_max[ctr])
             ? 0.0
             : embedding_function_interpolators[ctr].evaluate_derivative(rho)) *
        utils::evaluate_polynomial(
            embedding_interpolating_polynomial_coeffs[ctr], (1 - conc_sum));
    return value;
  }

  [[nodiscard]] double
  get_cluster_energy(const Point &atom, const std::vector<Point> &neighbours,
                     const NodalDataPoint &atom_data_point,
                     const std::vector<NodalDataPoint> &neighbours_data_points)
      const override {
    double u = 0.0;
    int noof_neighbours = neighbours.size();
    double f_rho_e{};
    double phi_ij{};
    double r_ij_mag{};
    auto rho_e = double{0.0};

    std::ofstream f_potential_log;
    if (this->output_potential_log) {
      f_potential_log.open("./potential_log.dat", std::ios::app);
    }

    for (int neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      rho_e += compute_electron_density_function(
          array_ops::distance(atom, neighbours[neighbour_idx]),
          neighbours_data_points[neighbour_idx]);
    }

    if (this->output_potential_log)
      f_potential_log << rho_e << " ";

    f_rho_e = compute_embedding_function(rho_e, atom_data_point);
    u = f_rho_e;

    for (int neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      r_ij_mag = array_ops::distance(atom, neighbours[neighbour_idx]);
      phi_ij = compute_pair_potential(r_ij_mag, atom_data_point,
                                      neighbours_data_points[neighbour_idx]);
      u += phi_ij;
    }
    if (this->output_potential_log) {
      f_potential_log << (u - f_rho_e) << " " << u << '\n';
      f_potential_log.close();
    }
    return u;
  }

  void get_cluster_forces(std::vector<Point> &forces, const Point &atom,
                          const std::vector<Point> &neighbours,
                          const NodalDataPoint &atom_data_point,
                          const std::vector<NodalDataPoint>
                              &neighbours_data_points) const override {
    forces.clear();
    forces.reserve(neighbours.size());

    double rho_e{};
    double dfrho_e{};
    double r_ij_mag{};
    double f_ij_mag{};
    auto temp = Point{};
    int noof_neighbours = neighbours.size();

    rho_e = 0.0;
    dfrho_e = 0.0;

    for (int neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      rho_e += compute_electron_density_function(
          array_ops::distance(atom, neighbours[neighbour_idx]),
          neighbours_data_points[neighbour_idx]);
    }
    dfrho_e = compute_embedding_function_derivative(rho_e, atom_data_point);

    for (int neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      r_ij_mag = array_ops::distance(atom, neighbours[neighbour_idx]);

      f_ij_mag =
          -(compute_pair_potential_derivative(
                r_ij_mag, atom_data_point,
                neighbours_data_points[neighbour_idx]) +
            dfrho_e * compute_electron_density_function_derivative(
                          r_ij_mag, neighbours_data_points[neighbour_idx]));

      if (std::isnan(f_ij_mag))
        throw exception::QCException{"SETFL: Nan force detected"};
      array_ops::as_eigen(temp) =
          f_ij_mag *
          (array_ops::as_eigen(neighbours[neighbour_idx]) -
           array_ops::as_eigen(atom)) /
          r_ij_mag;

      forces.push_back(temp);
    }
  }

  void get_concentration_forces(
      ConcentrationPoint &forces_atom,
      std::vector<ConcentrationPoint> &forces_neighbours, const Point &atom,
      const std::vector<Point> &neighbours,
      const NodalDataPoint &atom_data_point,
      const std::vector<NodalDataPoint> &neighbours_data_points)
      const override {

    forces_neighbours.clear();
    forces_neighbours.resize(neighbours.size());
    forces_atom.fill(0.0);

    const auto noof_neighbours = neighbours.size();

    std::vector<std::vector<double>> vector_of_densities;
    vector_of_densities.resize(noof_neighbours, std::vector<double>(Noi, 0.0));

    auto value = 0.0;
    for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      auto conc_sum = 0.0;
      for (std::size_t si = 0; si < Noi; si++)
        conc_sum +=
            neighbours_data_points[neighbour_idx].impurity_concentrations[si];

      const auto r = array_ops::distance(atom, neighbours[neighbour_idx]);

      auto ctr = 0;
      for (std::size_t si = start_idx; si < Noi; si++) {
        const auto electron_density =
            (r > density_function_input_max[ctr])
                ? 0.0
                : density_function_interpolators[ctr].evaluate(r);
        vector_of_densities[neighbour_idx][ctr] = electron_density;
        value +=
            electron_density *
            neighbours_data_points[neighbour_idx].impurity_concentrations[si];
        ctr++;
      }
      const auto electron_density =
          (r > density_function_input_max[ctr])
              ? 0.0
              : density_function_interpolators[ctr].evaluate(r);
      value += electron_density * (1 - conc_sum);
      if (start_idx == 1)
        vector_of_densities[neighbour_idx][ctr] = electron_density;
    }

    auto dfrho = compute_embedding_function_derivative(value, atom_data_point);

    auto ctr = 0;
    auto conc_sum_i = 0.0;
    for (std::size_t si = 0; si < Noi; si++)
      conc_sum_i += atom_data_point.impurity_concentrations[si];
    for (std::size_t si = start_idx; si < Noi; si++) {
      forces_atom[ctr] +=
          ((value > embedding_function_input_max[ctr])
               ? 0.0
               : embedding_function_interpolators[ctr].evaluate(value)) *
          utils::evaluate_polynomial_derivative(
              embedding_interpolating_polynomial_coeffs[ctr],
              atom_data_point.impurity_concentrations[si]);
      ctr++;
    }
    if (start_idx == 1)
      forces_atom[ctr] +=
          ((value > embedding_function_input_max[ctr])
               ? 0.0
               : embedding_function_interpolators[ctr].evaluate(value)) *
          utils::evaluate_polynomial_derivative(
              embedding_interpolating_polynomial_coeffs[ctr], 1 - conc_sum_i);

    for (std::size_t neighbour_idx = 0; neighbour_idx < noof_neighbours;
         neighbour_idx++) {
      for (std::size_t si = 0; si < Noi; si++) {
        forces_neighbours[neighbour_idx][si] +=
            dfrho * vector_of_densities[neighbour_idx][si];
      }
      // add the pair potential parts of the force now
      // compute and store all pair interactions first
      auto pair_potential_values =
          std::vector<double>(this->number_of_pair_interactions, 0.0);

      double conc_sum_j =
          neighbours_data_points[neighbour_idx].impurity_concentrations[0];
      for (std::size_t i = 1; i < Noi; i++)
        conc_sum_j +=
            neighbours_data_points[neighbour_idx].impurity_concentrations[i];

      const auto r = array_ops::distance(atom, neighbours[neighbour_idx]);

      for (std::size_t interpolator_ctr = 0;
           interpolator_ctr < number_of_pair_interactions; interpolator_ctr++) {
        pair_potential_values[interpolator_ctr] =
            (r > pair_potential_input_max[interpolator_ctr])
                ? 0
                : pair_potential_interpolators[interpolator_ctr].evaluate(r);
      }

      // now add force contributions
      auto ctri = 0;
      for (std::size_t i = start_idx; i < Noi; i++) {
        auto ctrj = 0;
        for (std::size_t j = start_idx; j < Noi; j++) {
          const auto pp_idx =
              get_interpolater_idx(i - start_idx, j - start_idx);
          forces_atom[ctri] +=
              neighbours_data_points[neighbour_idx].impurity_concentrations[j] *
              pair_potential_values[pp_idx];
          forces_neighbours[neighbour_idx][ctrj] +=
              atom_data_point.impurity_concentrations[i] *
              pair_potential_values[pp_idx];
          ctrj++;
        }
        const auto pp_idx =
            get_interpolater_idx(i - start_idx, Noi - start_idx);
        forces_atom[ctri] += (1 - conc_sum_j) * pair_potential_values[pp_idx];
        if (start_idx == 1)
          forces_neighbours[neighbour_idx][ctrj] +=
              atom_data_point.impurity_concentrations[i] *
              pair_potential_values[pp_idx];
        ctri++;
      }

      auto ctrj = 0;
      for (std::size_t j = start_idx; j < Noi; j++) {
        const auto pp_idx =
            get_interpolater_idx(Noi - start_idx, j - start_idx);
        if (start_idx == 1)
          forces_atom[ctri] +=
              neighbours_data_points[neighbour_idx].impurity_concentrations[j] *
              pair_potential_values[pp_idx];

        forces_neighbours[neighbour_idx][ctrj] +=
            (1 - conc_sum_i) * pair_potential_values[pp_idx];
        ctrj++;
      }

      const auto pp_idx =
          get_interpolater_idx(Noi - start_idx, Noi - start_idx);
      if (start_idx == 1) {
        forces_atom[ctri] += (1 - conc_sum_j) * pair_potential_values[pp_idx];
        forces_neighbours[neighbour_idx][ctrj] +=
            (1 - conc_sum_i) * pair_potential_values[pp_idx];
      }
    }
  }

  [[nodiscard]] std::size_t get_interpolater_idx(const std::size_t i,
                                                 const std::size_t j) const {
    const auto i1 = std::min(i, j);
    const auto j1 = std::max(i, j);
    const std::size_t v1 = i1 * this->number_of_species - (i1 * (i1 - 1)) / 2;
    return (v1 + j1 - i1);
  }
};

} // namespace materials
