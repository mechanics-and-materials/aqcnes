// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Extended FinnisSinclair copper implementation based on
 *        [Dai et al. 2006 (J. Phys.:Condensed Matter)]
 * @author P. Gupta, M. Spinola
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "materials/cluster_model.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <boost/mpi.hpp>

namespace materials {
namespace array_ops = qcmesh::array_ops;

struct ExtendedFinnisSinclairInformation {
  double c_0, c_1, c_2, c_3, c_4;
  double c, d;
  double a, b;
};

template <std::size_t DIM, std::size_t Noi>
class ExtendedFinnisSinclairCopper : public ClusterModel<DIM, Noi> {
  double min_rho = 1.0e-5;

  using Point = std::array<double, DIM>;
  using NodalDataPoint = meshing::NodalDataMultispecies<Noi>;
  using ConcentrationPoint = std::array<double, Noi>;

public:
  ExtendedFinnisSinclairCopper() {
    this->neighbor_cutoff_radius = 4.32;
    this->atomic_masses = std::vector<double>{63.546};
  }

  void
  get_concentration_forces(ConcentrationPoint &,
                           std::vector<ConcentrationPoint> &, const Point &,
                           const std::vector<Point> &, const NodalDataPoint &,
                           const std::vector<NodalDataPoint> &) const override {
  }

  double get_equilibrium_energy() {
    if (DIM == 2) {
      return -2.6162487985897;
    }
    if (DIM == 3) {
      return -3.49029024191736;
    }
    throw exception::QCException{
        "Invalid spatial dimension. Should be 2 or 3, but got " +
        std::to_string(DIM)};
  }

  [[nodiscard]] ExtendedFinnisSinclairInformation
  get_extended_finnis_sinclair_information() const {
    ExtendedFinnisSinclairInformation extended_finnis_sinclair_information{};
    extended_finnis_sinclair_information.c = 4.2900000000;
    extended_finnis_sinclair_information.d = 4.3200000000;
    extended_finnis_sinclair_information.c_0 = 10.1872400000;
    extended_finnis_sinclair_information.c_1 = -12.8203300000;
    extended_finnis_sinclair_information.c_2 = 6.1765870000;
    extended_finnis_sinclair_information.c_3 = -1.3413910000;
    extended_finnis_sinclair_information.c_4 = 0.1098420000;
    extended_finnis_sinclair_information.a = 0.3918650000;
    extended_finnis_sinclair_information.b = -0.8810960000;
    return extended_finnis_sinclair_information;
  }

  [[nodiscard]] double compute_pair_potential(double r) const {
    ExtendedFinnisSinclairInformation info =
        get_extended_finnis_sinclair_information();

    double r_c = r - info.c;
    return 0.5 * static_cast<double>(r_c < 0.0) *
           (r_c * r_c *
            (info.c_0 +
             r * (info.c_1 + r * (info.c_2 + r * (info.c_3 + r * info.c_4)))));
  }

  [[nodiscard]] double compute_pair_potential_derivative(double r) const {
    ExtendedFinnisSinclairInformation info =
        get_extended_finnis_sinclair_information();

    double r_c = r - info.c;
    return 0.5 * static_cast<double>(r_c < 0.0) *
           (2.0 * r_c *
                (info.c_0 +
                 r * (info.c_1 +
                      r * (info.c_2 + r * (info.c_3 + r * info.c_4)))) +
            r_c * r_c *
                (info.c_1 + r * (2.0 * info.c_2 +
                                 r * (3.0 * info.c_3 + 4.0 * r * info.c_4))));
  }

  [[nodiscard]] double
  compute_electron_density_function_derivative(double r) const {
    ExtendedFinnisSinclairInformation info =
        get_extended_finnis_sinclair_information();

    double r_d = r - info.d;
    double r_d2 = (r - info.d) * (r - info.d);

    double phi_prime =
        static_cast<double>(r_d < 0.0) *
        (info.a * info.a * r_d * (2.0 + 4.0 * info.b * info.b * r_d2));

    return phi_prime;
  }

  [[nodiscard]] double compute_electron_density_function(double r) const {
    ExtendedFinnisSinclairInformation info =
        get_extended_finnis_sinclair_information();

    double r_d = r - info.d;
    double r_d2 = (r - info.d) * (r - info.d);

    double phi = static_cast<double>(r_d < 0.0) * info.a * info.a * r_d2 *
                 (1.0 + info.b * info.b * r_d2);

    return phi;
  }

  [[nodiscard]] double compute_embedding_function(double rho) const {
    return -1.0 * std::sqrt(rho);
  }

  [[nodiscard]] double compute_embedding_function_derivative(double rho) const {
    return (rho > min_rho ? -0.5 * std::sqrt(1.0 / rho) : 0.0);
  }

  [[nodiscard]] double
  get_cluster_energy(const Point &atom, const std::vector<Point> &neighbours,
                     const NodalDataPoint &,
                     const std::vector<NodalDataPoint> &) const override {
    double u = 0.0;
    double rho_e{};
    double f_rho_e{};
    double phi_ij{};
    double r_ij_mag{};

    rho_e = 0.0;

    for (const auto &neighbour : neighbours) {
      rho_e += compute_electron_density_function(
          array_ops::distance(atom, neighbour));
    }

    f_rho_e = compute_embedding_function(rho_e);

    u = f_rho_e;

    for (const auto &neighbour : neighbours) {
      r_ij_mag = array_ops::distance(atom, neighbour);
      phi_ij = compute_pair_potential(r_ij_mag);

      u += phi_ij;
    }
    return u;
  }

  void get_cluster_forces(std::vector<Point> &forces, const Point &atom,
                          const std::vector<Point> &neighbours,
                          const NodalDataPoint &,
                          const std::vector<NodalDataPoint> &) const override {
    forces.clear();

    forces.reserve(neighbours.size());

    double rho_e{};
    double dfrho_e{};
    double r_ij_mag{};
    double f_ij_mag{};

    rho_e = 0.0;
    dfrho_e = 0.0;

    for (const auto &neighbour : neighbours) {
      rho_e += compute_electron_density_function(
          array_ops::distance(atom, neighbour));
    }
    dfrho_e = compute_embedding_function_derivative(rho_e);

    for (const auto &neighbour : neighbours) {
      r_ij_mag = array_ops::distance(atom, neighbour);

      f_ij_mag =
          -(compute_pair_potential_derivative(r_ij_mag) +
            dfrho_e * compute_electron_density_function_derivative(r_ij_mag));

      if (std::isnan(f_ij_mag))
        throw exception::QCException{"Copper EFS: Nan force detected"};

      if (r_ij_mag <= this->neighbor_cutoff_radius) {
        auto temp = Point{};
        array_ops::as_eigen(temp) =
            f_ij_mag *
            (array_ops::as_eigen(neighbour) - array_ops::as_eigen(atom)) /
            r_ij_mag;
        forces.push_back(temp);
      } else {
        forces.push_back(Point{});
      }
    }
  }
};
} // namespace materials
