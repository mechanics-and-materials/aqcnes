// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation for a GSL cubic spline to interpolate tabulated SETFL EAM energy values
 * @author P. Gupta
 */

#pragma once

#include <memory>
#include <vector>

namespace materials {

/**
 * @brief Wrapper around GSL cubic spline interpolator
 *
 * Warning: The use of the CubicSpline class as an interpolator may not
 *          be thread-safe. The threads are all sharing a gsl accelerator,
 *          and i'm not sure if it affects correctness
 
 * its fine if we create the interpolators using firstPrivate initially. So
 * doing multi-threading over the sampling atoms is fine as long as
 * potential is used with firstPrivate
 
 * check out
 * https://stackoverflow.com/questions/32347008/confused-about-firstprivate-and-threadprivate-in-openmp-context
 */
class CubicSpline {

private:
  struct InterpAccelerator;
  struct Spline;
  struct InterpAcceleratorDeleter {
    void operator()(InterpAccelerator *) const;
  };
  struct SplineDeleter {
    void operator()(Spline *) const;
  };

  std::unique_ptr<InterpAccelerator, InterpAcceleratorDeleter> accelerator{};
  std::unique_ptr<Spline, SplineDeleter> spline{};

  double domain_min{}, domain_max{}, last_range_value{};

public:
  CubicSpline() = default;
  void initialize(const std::vector<std::pair<double, double>> &data);
  [[nodiscard]] double evaluate(const double input) const;
  [[nodiscard]] double evaluate_derivative(const double input) const;
  [[nodiscard]] inline double get_domain_min() const { return domain_min; }
  [[nodiscard]] inline double get_domain_max() const { return domain_max; }
  [[nodiscard]] inline double get_last_range_value() const {
    return last_range_value;
  }
};
} // namespace materials
