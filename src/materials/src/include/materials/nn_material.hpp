// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief a material class based on trained GNN models
 * @author S. Saxena, M. Spinola
 */

#pragma once
#include "materials/cluster_model.hpp"

namespace materials {

class NNMaterial : public ClusterModel<3, 1> {

  constexpr static std::size_t NIDOF = 2;
  using Point = std::array<double, 3>;
  using NodalDataPoint = meshing::NodalDataMultispecies<1>;
  using EdgeVector = std::array<std::size_t, 2>;
  using ThermalPoint = std::array<double, 2>;
  using ConcentrationPoint = std::array<double, 1>;

protected:
  struct torch_info *torch_variables;
  double neighbour_radius{}, sigma_dataset_rms{};

public:
  NNMaterial(const std::string &path_to_model,
             const std::string &path_to_std_file,
             const double atomic_mass_input,
             const lattice::LatticeStructure lattice_type_name);
  NNMaterial(const NNMaterial &) = default;
  NNMaterial(NNMaterial &&) = default;
  NNMaterial &operator=(const NNMaterial &) = default;
  NNMaterial &operator=(NNMaterial &&) = default;
  ~NNMaterial() override;

  /**
   * @brief load a trained GNN
   */
  void
  load_torch_gnn(const std::string &model_name,
                 [[maybe_unused]] const std::string &standardization_file_name);

  // Use a trained GNN to get thermal energy and forces
  void get_thermalized_energy_and_forces_nn(
      double &energy, std::vector<Point> &forces,
      std::vector<double> &thermal_forces_sampling_atom,
      std::vector<double> &thermal_forces_neighbours, const Point &atom,
      const ThermalPoint &atom_thermal_coordinates,
      const std::vector<Point> &neighbours,
      const std::vector<ThermalPoint> &neighbours_thermal_coordinates) const;

  void get_batched_thermalized_energy_and_forces_gnn(
      const std::vector<Point> &positions,
      const std::vector<double> &phi_values,
      const std::vector<EdgeVector> &edges,
      const std::vector<int64_t> &graph_numbers, std::vector<double> &energies,
      std::vector<Point> &forces, std::vector<double> &thermal_forces) const;

  // unused functions for the NN class
  [[nodiscard]] double
  get_cluster_energy(const Point &, const std::vector<Point> &,
                     const NodalDataPoint &,
                     const std::vector<NodalDataPoint> &) const override;

  void get_cluster_forces(std::vector<Point> &, const Point &,
                          const std::vector<Point> &, const NodalDataPoint &,
                          const std::vector<NodalDataPoint> &) const override;

  void
  get_concentration_forces(ConcentrationPoint &,
                           std::vector<ConcentrationPoint> &, const Point &,
                           const std::vector<Point> &, const NodalDataPoint &,
                           const std::vector<NodalDataPoint> &) const override;
};

} // namespace materials
