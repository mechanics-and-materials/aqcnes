// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation get material density based on the lattice structure
 * @author P. Gupta, S. Saxena, M. Spinola
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include <array>
#include <cmath>

namespace materials {

inline double
get_material_density(const std::array<double, 2> &lattice_parameter_vector,
                     const lattice::LatticeStructure &lattice_structure) {

  const auto cell_volume =
      lattice_parameter_vector[0] * lattice_parameter_vector[1];

  switch (lattice_structure) {
  case lattice::LatticeStructure::SimpleCubic:
    return 1.0 / cell_volume;
  case lattice::LatticeStructure::HCP:
    return 2.0 / cell_volume;
  case lattice::LatticeStructure::FCC:
    throw exception::QCException{
        "Cannot do fcc in 2d. See materials/materials.h and the "
        "driver"};
  case lattice::LatticeStructure::BCC:
    throw exception::QCException{
        "Cannot do bcc in 2d. See materials/materials.h and the "
        "driver"};
  case lattice::LatticeStructure::None:
    throw exception::QCException{
        "Lattice structure not defined. See materials/materials.h and "
        "the driver"};
  default:
    throw exception::QCException{
        "Unrecognized structure. See materials/materials.h and the "
        "driver"};
  }
}

inline double
get_material_density(const std::array<double, 3> &lattice_parameter_vector,
                     const lattice::LatticeStructure &lattice_structure) {

  const auto cell_volume = lattice_parameter_vector[0] *
                           lattice_parameter_vector[1] *
                           lattice_parameter_vector[2];

  switch (lattice_structure) {
  case lattice::LatticeStructure::SimpleCubic:
    return 1.0 / cell_volume;
  case lattice::LatticeStructure::FCC:
    return 4.0 / cell_volume;
  case lattice::LatticeStructure::BCC:
    return 2.0 / cell_volume;
  case lattice::LatticeStructure::HCP:
    return 4.0 / (cell_volume * std::sqrt(3));
  case lattice::LatticeStructure::None:
    throw exception::QCException{
        "Lattice structure not defined. See materials/materials.h and "
        "the driver"};
  default:
    throw exception::QCException{
        "Unrecognized structure. See materials/materials.h and the "
        "driver"};
  }
}

} // namespace materials
