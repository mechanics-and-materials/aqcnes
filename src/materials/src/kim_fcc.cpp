// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementations to use potentials available in the openKIM repository
 * https://kim-api.readthedocs.io/en/devel/ex__test___ar__fcc__cluster__cpp_8cpp_source.html
 * @authors S. Saxena, P. Gupta, M. Spinola
 */

#include "materials/kim_fcc.hpp"
#include "exception/qc_exception.hpp"
#include "kim-api/KIM_SimulatorHeaders.hpp"
#include "kim-api/KIM_SupportedExtensions.hpp"
#include "qcmesh/array_ops/array_ops.hpp"

namespace materials {

namespace {
struct NeighList {
  double cutoff{};
  std::size_t number_of_particles{};
  std::vector<int> n_neighbors{};
  std::vector<int> neighbor_list{};
};

} // namespace

struct KimFccBase::Model : KIM::Model {};
struct KimFccBase::ComputeArguments : KIM::ComputeArguments {};

KIM::Model *as_kim(KimFccBase::Model *model) { return model; }
KIM::ComputeArguments *as_kim(KimFccBase::ComputeArguments *args) {
  return args;
}

KimFccBase::KimFccBase(const std::string &kim_model_name,
                       std::vector<std::string> species_value)
    : min_rho(1.0e-5), species{std::move(species_value)} {
  set_kim_object(kim_model_name);
  kim_species_support();
}
KimFccBase::~KimFccBase() {
  auto *kim_args = as_kim(this->compute_arguments);
  this->kim_cluster_model->ComputeArgumentsDestroy(&kim_args);
  auto *kim_model = as_kim(this->kim_cluster_model);
  KIM::Model::Destroy(&kim_model);
}

void KimFccBase::set_kim_object(const std::string &model_name) {
  const std::string &kim_model_name = model_name;
  int requested_units_accepted = 0;
  KIM::Model *kim_model = nullptr;
  if (KIM::Model::Create(
          KIM::NUMBERING::zeroBased, KIM::LENGTH_UNIT::A, KIM::ENERGY_UNIT::eV,
          KIM::CHARGE_UNIT::e, KIM::TEMPERATURE_UNIT::K, KIM::TIME_UNIT::ps,
          kim_model_name, &requested_units_accepted, &kim_model) != 0)
    throw exception::QCException{"KIM::Model::Create()"};
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-static-cast-downcast)
  this->kim_cluster_model = static_cast<KimFccBase::Model *>(kim_model);
  if (requested_units_accepted == 0)
    throw exception::QCException{"Must Adapt to model units"};

  KIM::ComputeArguments *kim_args = nullptr;
  if (this->kim_cluster_model->ComputeArgumentsCreate(&kim_args) != 0)
    throw exception::QCException{"Unable to create a ComputeArguments object."};
  this->compute_arguments =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-static-cast-downcast)
      static_cast<KimFccBase::ComputeArguments *>(kim_args);
}

void KimFccBase::kim_species_support() {
  species_codes.resize(this->species.size(), 0);
  int is_species_supported = 0;
  for (std::size_t i = 0; i < this->species.size(); i++) {
    KIM::SpeciesName sp(this->species[i]);
    if ((this->kim_cluster_model->GetSpeciesSupportAndCode(
             sp, &is_species_supported, &(this->species_codes[i])) != 0) ||
        (is_species_supported == 0))
      throw exception::QCException{"get_species_code"};
  }
}

double KimFccBase::get_neighbor_interaction_cutoff_radius() const {
  int number_of_neighbor_lists = 0;
  double const *cutoff = nullptr;
  int const *model_will_not_request_neighbors_of_noncontributing_particles =
      nullptr;
  this->kim_cluster_model->GetNeighborListPointers(
      &number_of_neighbor_lists, &cutoff,
      &model_will_not_request_neighbors_of_noncontributing_particles);
  return (*cutoff);
}

double
KimFccBase::get_cluster_energy(const Point &atom,
                               const std::vector<Point> &neighbours) const {
  double u = 0.0;

  const auto total_atoms = neighbours.size() + 1;
  auto coords_cluster =
      std::vector<Point>(total_atoms, std::array<double, 3>{});
  auto atom_species = std::vector<int>(total_atoms);
  auto particle_contributing = std::vector<int>(total_atoms, 0);

  for (std::size_t i = 0; i < total_atoms; ++i)
    atom_species[i] = this->species_codes[0];

  // Only the sampling atom contributes to the energy
  particle_contributing[0] = 1;

  // coords_cluster has first row as the sampling atom and other rows as its
  // neighbors
  coords_cluster[0] = atom;

  for (std::size_t i = 1; i < total_atoms; ++i) {
    coords_cluster[i] = neighbours[i - 1];
  }

  // allocate memory for the neighbor list
  auto neighbor_list = NeighList{};
  neighbor_list.number_of_particles = total_atoms;
  neighbor_list.n_neighbors.resize(total_atoms);
  neighbor_list.neighbor_list.resize(total_atoms * total_atoms);

  neighbor_list.n_neighbors[0] = static_cast<int>(total_atoms - 1);
  for (std::size_t i = 1; i < total_atoms; ++i) {
    neighbor_list.neighbor_list[i - 1] = static_cast<int>(i);
  }

  // Link the ComputeArguments parameters to our variables
  const auto kim_total_atoms = static_cast<int>(total_atoms);
  if ((this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::numberOfParticles, &kim_total_atoms) !=
       0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::particleSpeciesCodes,
           atom_species.data()) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::particleContributing,
           particle_contributing.data()) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::partialEnergy,
           static_cast<double *>(&u)) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::coordinates,
           (double *)coords_cluster[0].data()) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::partialForces,
           static_cast<double *>(nullptr)) != 0))
    throw exception::QCException{"KIM_API_set_data"};

  if (this->compute_arguments->SetCallbackPointer(
          KIM::COMPUTE_CALLBACK_NAME::GetNeighborList, KIM::LANGUAGE_NAME::cpp,
          // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
          reinterpret_cast<KIM::Function *>(&KimFccBase::get_cluster_neigh),
          &neighbor_list) != 0)
    throw exception::QCException{"set_call_back"};

  // call compute functions
  if (this->kim_cluster_model->Compute(this->compute_arguments) != 0)
    throw exception::QCException{"Can't compute energy"};

  // free memory of neighbor lists

  return u;
}

void KimFccBase::get_cluster_forces(
    std::vector<Point> &forces, const Point &atom,
    const std::vector<Point> &neighbours) const {
  forces.clear();

  const auto total_atoms = neighbours.size() + 1;
  auto forces_kim = std::vector<Point>(total_atoms, std::array<double, 3>{});
  auto coords_cluster =
      std::vector<Point>(total_atoms, std::array<double, 3>{});
  auto atom_species = std::vector<int>(total_atoms);

  auto particle_contributing = std::vector<int>(total_atoms, 0);
  auto neighbor_list = NeighList{};

  for (std::size_t i = 0; i < total_atoms; ++i)
    atom_species[i] = this->species_codes[0];

  // Only the sampling atom contributes to the energy
  particle_contributing[0] = 1;

  // coords_cluster has first row as the sampling atom and other rows as its
  // neighbors
  coords_cluster[0] = atom;

  for (std::size_t i = 1; i < total_atoms; ++i) {
    coords_cluster[i] = neighbours[i - 1];
  }

  // allocate memory for the neighbor list
  neighbor_list.number_of_particles = total_atoms;
  neighbor_list.n_neighbors.resize(total_atoms);
  neighbor_list.neighbor_list.resize(total_atoms * total_atoms);

  neighbor_list.n_neighbors[0] = static_cast<int>(total_atoms - 1);
  for (std::size_t i = 1; i < total_atoms; ++i) {
    neighbor_list.neighbor_list[i - 1] = static_cast<int>(i);
  }

  // Link the ComputeArguments parameters to our variables
  const auto kim_total_atoms = static_cast<int>(total_atoms);
  if ((this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::numberOfParticles, &kim_total_atoms) !=
       0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::particleSpeciesCodes,
           atom_species.data()) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::particleContributing,
           particle_contributing.data()) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::partialForces,
           static_cast<double *>(forces_kim[0].data())) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::coordinates,
           static_cast<double *>(coords_cluster[0].data())) != 0) ||
      (this->compute_arguments->SetArgumentPointer(
           KIM::COMPUTE_ARGUMENT_NAME::partialEnergy,
           static_cast<double *>(nullptr)) != 0))
    throw exception::QCException{"KIM_API_set_data"};

  if (this->compute_arguments->SetCallbackPointer(
          KIM::COMPUTE_CALLBACK_NAME::GetNeighborList, KIM::LANGUAGE_NAME::cpp,
          // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
          reinterpret_cast<KIM::Function *>(&KimFccBase::get_cluster_neigh),
          &neighbor_list) != 0)
    throw exception::QCException{"set_call_back"};

  // call compute functions
  if (this->kim_cluster_model->Compute(this->compute_arguments) != 0)
    throw exception::QCException{"Can't compute force"};

  forces.insert(forces.begin(), forces_kim.begin() + 1, forces_kim.end());
}

int KimFccBase::get_cluster_neigh(void *const data_object,
                                  const int number_of_neighbor_lists,
                                  const double *const cutoffs,
                                  const int neighbor_list_index,
                                  const int particle_number,
                                  int *const number_of_neighbors,
                                  const int **const neighbors_of_particle) {
  int error = 1;
  auto *nl = static_cast<NeighList *>(data_object);
  const auto number_of_particles = nl->number_of_particles;

  if ((number_of_neighbor_lists < 1) || (*cutoffs < 0))
    return error;

  if (neighbor_list_index != 0)
    return error;

  *number_of_neighbors = 0;

  if (particle_number < 0 || static_cast<std::size_t>(particle_number) >=
                                 number_of_particles) // invalid id
    throw exception::QCException{"Invalid part ID in get_cluster_neigh"};

  // set the returned number of neighbors for the returned part
  *number_of_neighbors = nl->n_neighbors[particle_number];

  // set the location for the returned neighbor list
  *neighbors_of_particle =
      &(nl->neighbor_list[static_cast<std::size_t>(particle_number) *
                          number_of_particles]);
  // this asigns to neighborsOfParticle the neighbors of particleNumber=0
  // stored in nl.neighborList created in get_cluster_forces
  return 0;
}

} // namespace materials
