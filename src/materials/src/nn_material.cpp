// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Function implementations to use trained GNN models for avg energy and force computation
 * @author S. Saxena, M. Spinola
 */

#include "materials/nn_material.hpp"
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/variable.h>
#include <torch/csrc/jit/runtime/graph_executor.h>
#include <torch/script.h>
#include <torch/torch.h>

namespace materials {

struct torch_info {
  torch::Device device = torch::kCPU;
  torch::jit::script::Module module;
};

NNMaterial::NNMaterial(const std::string &path_to_model,
                       const std::string &path_to_std_file,
                       const double atomic_mass_input,
                       const lattice::LatticeStructure lattice_type)
    : torch_variables(new torch_info) {

  load_torch_gnn(path_to_model, path_to_std_file);
  this->atomic_masses = std::vector<double>{atomic_mass_input};
  switch (lattice_type) {
  case lattice::LatticeStructure::FCC:
  case lattice::LatticeStructure::BCC:
  case lattice::LatticeStructure::HCP:
    this->lattice_structure = lattice_type;
    break;
  default:
    throw exception::QCException{
        "Unsupported lattice type: " +
        std::string{serialize_lattice_structure(lattice_type)}};
  }
}

NNMaterial::~NNMaterial() { delete torch_variables; }

// load a trained GNN
void NNMaterial::load_torch_gnn(
    const std::string &model_name,
    [[maybe_unused]] const std::string &standardization_file_name) {
  // loading standardization info
  std::string line;
  std::string word;
  std::fstream stdfile(standardization_file_name);
  if (stdfile.is_open()) {
    getline(stdfile, line); // skip first line
    getline(
        stdfile,
        line); // take the first word of the next line as the sigma RMS value
    std::stringstream str(line);
    getline(str, word, ',');
    sigma_dataset_rms = std::stod(word);
  } else
    std::cout << "Could not open the standardization file.\n";

  // checking device
  if (torch::cuda::is_available()) {
    torch_variables->device = torch::kCUDA;
  } else {
    torch_variables->device = torch::kCPU;
  }
  std::cout << "Device: " << torch_variables->device << "\n";

  // load torch module
  std::unordered_map<std::string, std::string> metadata = {
      {"config", ""},
      {"nequip_version", ""},
      {"r_max", ""},
      {"n_species", ""},
      {"type_names", ""},
      {"_jit_bailout_depth", ""},
      {"_jit_fusion_strategy", ""},
      {"allow_tf32", ""}};
  torch_variables->module =
      torch::jit::load(model_name, torch_variables->device, metadata);
  torch_variables->module.eval();

  this->neighbor_cutoff_radius = std::stod(metadata["r_max"]);
  std::cout << "cutoff radius: " << this->neighbor_cutoff_radius << '\n';

  // Check if model is a NequIP model
  if (metadata["nequip_version"].empty()) {
    std::cout
        << "The indicated TorchScript file does not appear to be a deployed "
           "NequIP model; did you forget to run `nequip-deploy`? \n";
  }

  std::cout << "NEQUIP GNN model loaded \n";

  if (torch_variables->module.hasattr("training")) {
    std::cout << "Freezing TorchScript model...\n";
    torch_variables->module = torch::jit::freeze(torch_variables->module);
  }
}

// Use a trained GNN to get thermal energy and forces
void NNMaterial::get_thermalized_energy_and_forces_nn(
    double &energy, std::vector<Point> &forces,
    std::vector<double> &thermal_forces_sampling_atom,
    std::vector<double> &thermal_forces_neighbours, const Point &atom,
    const ThermalPoint &atom_thermal_coordinates,
    const std::vector<Point> &neighbours,
    const std::vector<ThermalPoint> &neighbours_thermal_coordinates) const {

  constexpr std::size_t DIMENSION = 3;
  forces.clear();
  thermal_forces_sampling_atom.clear();
  thermal_forces_neighbours.clear();
  forces.resize(neighbours.size(), Point{});
  thermal_forces_sampling_atom.resize(neighbours.size(), 0.0);
  thermal_forces_neighbours.resize(neighbours.size(), 0.0);

  const auto natoms = static_cast<int64_t>(neighbours.size() + 1);
  // building input arrays for the GNN
  torch::Tensor pos_tensor = torch::zeros({natoms, 3});
  torch::Tensor tag2type_tensor =
      torch::zeros({natoms}, torch::TensorOptions().dtype(torch::kInt64));
  torch::Tensor sigma_tensor = torch::zeros({natoms, 1});

  auto pos = pos_tensor.accessor<float, 2>();
  auto sigmas = sigma_tensor.accessor<float, 2>();

  pos[0][0] = static_cast<float>(atom[0]);
  pos[0][1] = static_cast<float>(atom[1]);
  pos[0][2] = static_cast<float>(atom[2]);
  sigmas[0][0] =
      static_cast<float>(exp(-atom_thermal_coordinates[1]) / sigma_dataset_rms);
  std::vector<int64_t> edge_info;

  for (int64_t idx = 0; static_cast<std::size_t>(idx) < neighbours.size();
       idx++) {
    pos[idx + 1][0] = static_cast<float>(neighbours[idx][0]);
    pos[idx + 1][1] = static_cast<float>(neighbours[idx][1]);
    pos[idx + 1][2] = static_cast<float>(neighbours[idx][2]);
    sigmas[idx + 1][0] = static_cast<float>(
        exp(-neighbours_thermal_coordinates[idx][1]) / sigma_dataset_rms);

    // graph edge creation for a STAR graph
    edge_info.push_back(0);
    edge_info.push_back(idx + 1);
    edge_info.push_back(idx + 1);
    edge_info.push_back(0);
  }

  const auto n_edges = edge_info.size() / 2;
  torch::Tensor edges_tensor =
      torch::zeros({2, static_cast<int64_t>(n_edges)},
                   torch::TensorOptions().dtype(torch::kInt64));
  auto new_edges = edges_tensor.accessor<int64_t, 2>();

  for (int64_t edge_idx = 0; static_cast<std::size_t>(edge_idx) < n_edges;
       edge_idx++) {
    new_edges[0][edge_idx] = edge_info[2 * edge_idx];
    new_edges[1][edge_idx] = edge_info[2 * edge_idx + 1];
  }

  c10::Dict<std::string, torch::Tensor> input;
  input.insert("pos", pos_tensor.to(torch_variables->device));
  input.insert("edge_index", edges_tensor.to(torch_variables->device));
  input.insert("atom_types", tag2type_tensor.to(torch_variables->device));
  input.insert("node_features", sigma_tensor.to(torch_variables->device));

  std::vector<torch::IValue> input_vector{{input}};

  auto output_raw = torch_variables->module.forward(input_vector);
  auto output = output_raw.toGenericDict();

  // equate outputs
  torch::Tensor forces_tensor = output.at("forces").toTensor().cpu();
  torch::Tensor thermal_forces_tensor =
      output.at("node_feature_forces").toTensor().cpu() *
      (sigma_tensor * 2 / static_cast<int>(DIMENSION));
  torch::Tensor total_energy_tensor =
      output.at("total_energy").toTensor().cpu();

  energy = total_energy_tensor[0].item<float>();
  thermal_forces_sampling_atom[0] = -thermal_forces_tensor[0].item<float>();
  for (int64_t n_idx = 0; static_cast<std::size_t>(n_idx) < neighbours.size();
       n_idx++) {
    forces[n_idx][0] = forces_tensor[n_idx + 1][0].item<float>();
    forces[n_idx][1] = forces_tensor[n_idx + 1][1].item<float>();
    forces[n_idx][2] = forces_tensor[n_idx + 1][2].item<float>();
    thermal_forces_neighbours[n_idx] =
        thermal_forces_tensor[n_idx + 1].item<float>();
  }
}

void NNMaterial::get_batched_thermalized_energy_and_forces_gnn(
    const std::vector<Point> &positions, const std::vector<double> &phi_values,
    const std::vector<EdgeVector> &edges,
    const std::vector<int64_t> &graph_numbers, std::vector<double> &energies,
    std::vector<Point> &forces, std::vector<double> &thermal_forces) const {
  energies.clear();
  forces.clear();
  thermal_forces.clear();
  const auto noof_positions = static_cast<int64_t>(positions.size());
  const auto n_edges = static_cast<int64_t>(edges.size());

  energies.resize(noof_positions, 0.0);
  thermal_forces.resize(noof_positions, 0.0);
  forces.resize(positions.size(), Point{});

  // building input arrays for the GNN
  torch::Tensor pos_tensor = torch::zeros({noof_positions, 3});
  torch::Tensor tag2type_tensor = torch::zeros(
      {noof_positions}, torch::TensorOptions().dtype(torch::kInt64));
  torch::Tensor sigma_tensor = torch::zeros({noof_positions, 1});
  torch::Tensor edges_tensor =
      torch::zeros({2, n_edges}, torch::TensorOptions().dtype(torch::kInt64));
  torch::Tensor batch_tensor = torch::zeros(
      {noof_positions}, torch::TensorOptions().dtype(torch::kInt64));

  auto pos = pos_tensor.accessor<float, 2>();
  auto sigmas = sigma_tensor.accessor<float, 2>();
  auto t_edges = edges_tensor.accessor<int64_t, 2>();
  auto batch = batch_tensor.accessor<int64_t, 1>();

  for (int64_t idx = 0; idx < noof_positions; idx++) {
    pos[idx][0] = static_cast<float>(positions[idx][0]);
    pos[idx][1] = static_cast<float>(positions[idx][1]);
    pos[idx][2] = static_cast<float>(positions[idx][2]);
    sigmas[idx][0] =
        static_cast<float>(exp(-phi_values[idx]) / sigma_dataset_rms);
    batch[idx] = graph_numbers[idx];
  }
  for (int64_t edge_idx = 0; edge_idx < n_edges; edge_idx++) {
    t_edges[0][edge_idx] = static_cast<int64_t>(edges[edge_idx][0]);
    t_edges[1][edge_idx] = static_cast<int64_t>(edges[edge_idx][1]);
  }

  c10::Dict<std::string, torch::Tensor> input;
  input.insert("pos", pos_tensor.to(torch_variables->device));
  input.insert("edge_index", edges_tensor.to(torch_variables->device));
  input.insert("atom_types", tag2type_tensor.to(torch_variables->device));
  input.insert("node_features", sigma_tensor.to(torch_variables->device));
  input.insert("batch", batch_tensor.to(torch_variables->device));

  std::vector<torch::IValue> input_vector{{input}};

  auto output_raw = torch_variables->module.forward(input_vector);
  auto output = output_raw.toGenericDict();

  // equate outputs
  torch::Tensor forces_tensor = output.at("forces").toTensor().cpu();
  constexpr std::size_t DIMENSION = 3;
  torch::Tensor thermal_forces_tensor =
      output.at("node_feature_forces").toTensor().cpu() *
      (sigma_tensor * 2 / static_cast<int>(DIMENSION));
  torch::Tensor atomic_energy_tensor =
      output.at("atomic_energy").toTensor().cpu();

  for (int64_t n_idx = 0; static_cast<std::size_t>(n_idx) < positions.size();
       n_idx++) {
    energies[n_idx] = atomic_energy_tensor[n_idx].item<float>();
    forces[n_idx][0] = forces_tensor[n_idx][0].item<float>();
    forces[n_idx][1] = forces_tensor[n_idx][1].item<float>();
    forces[n_idx][2] = forces_tensor[n_idx][2].item<float>();
    thermal_forces[n_idx] = thermal_forces_tensor[n_idx].item<float>();
  }
}

// unused functions for the NN class
double
NNMaterial::get_cluster_energy(const Point &, const std::vector<Point> &,
                               const NodalDataPoint &,
                               const std::vector<NodalDataPoint> &) const {
  return 0.0;
}

void NNMaterial::get_cluster_forces(std::vector<Point> &, const Point &,
                                    const std::vector<Point> &,
                                    const NodalDataPoint &,
                                    const std::vector<NodalDataPoint> &) const {
}

void NNMaterial::get_concentration_forces(
    ConcentrationPoint &, std::vector<ConcentrationPoint> &, const Point &,
    const std::vector<Point> &, const NodalDataPoint &,
    const std::vector<NodalDataPoint> &) const {}

} // namespace materials
