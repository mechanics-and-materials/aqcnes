// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author P. Gupta
 */

#include "materials/cubic_spline.hpp"
#include "exception/qc_exception.hpp"
#include <gsl/gsl_spline.h>

namespace materials {

struct CubicSpline::InterpAccelerator : public gsl_interp_accel {};
struct CubicSpline::Spline : public gsl_spline {};

void CubicSpline::SplineDeleter::operator()(CubicSpline::Spline *spline) const {
  gsl_spline_free(spline);
}
void CubicSpline::InterpAcceleratorDeleter::operator()(
    CubicSpline::InterpAccelerator *accelerator) const {
  gsl_interp_accel_free(accelerator);
}

void CubicSpline::initialize(
    const std::vector<std::pair<double, double>> &data) {
  if (data.empty())
    throw exception::QCException{"Empty data vector"};
  auto domain = std::vector<double>{};
  domain.reserve(data.size());
  auto range = std::vector<double>{};
  range.reserve(data.size());

  for (const auto &[d, r] : data) {
    domain.push_back(d);
    range.push_back(r);
  }
  auto *raw_accelerator = gsl_interp_accel_alloc();
  auto *raw_spline = gsl_spline_alloc(gsl_interp_cspline, domain.size());
  gsl_spline_init(raw_spline, domain.data(), range.data(), range.size());
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-static-cast-downcast)
  this->accelerator.reset(static_cast<InterpAccelerator *>(raw_accelerator));
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-static-cast-downcast)
  this->spline.reset(static_cast<Spline *>(raw_spline));

  this->domain_min = data.front().first;
  std::tie(this->domain_max, this->last_range_value) = data.back();
}

double CubicSpline::evaluate(const double input) const {
  return gsl_spline_eval(
      static_cast<gsl_spline *>(this->spline.get()), input,
      static_cast<gsl_interp_accel *>(this->accelerator.get()));
}

double CubicSpline::evaluate_derivative(const double input) const {
  return gsl_spline_eval_deriv(
      static_cast<gsl_spline *>(this->spline.get()), input,
      static_cast<gsl_interp_accel *>(this->accelerator.get()));
}

} // namespace materials
