// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load boundary conditions from restart file
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/fixedboundary.hpp"
#include "boundarycondition/freeboundary.hpp"
#include "boundarycondition/slipboundary.hpp"
#include "netcdf_io/load_boundary_conditions.hpp"

namespace boundarycondition {

template <std::size_t Dimension>
std::vector<Boundarycondition<Dimension>>
load_boundary_condition(const std::filesystem::path &path) {
  const auto defs = netcdf_io::load_boundary_conditions<Dimension>(path);
  auto boundary_conditions = std::vector<Boundarycondition<Dimension>>{};
  boundary_conditions.reserve(defs.size());
  for (const auto &def : defs) {
    auto boundary_condition =
        create_boundary_condition<Dimension>(def.zone_type);
    boundary_condition.coordinate = def.coordinate;
    boundary_condition.bound = def.bound;
    boundary_condition.normal = def.normal;
    boundary_conditions.emplace_back(boundary_condition);
  }
  return boundary_conditions;
}
} // namespace boundarycondition
