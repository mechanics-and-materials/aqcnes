// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief External force applicator types and struct
 * @authors P. Gupta
 */

#pragma once

#include <array>
#include <vector>

namespace boundarycondition {
/**
 * @brief Force applicator type.
 */
enum struct ForceApplicatorType : std::size_t { Null, Sphere };

/**
 * @brief List of all supported force applicator types.
 *
 * Can be used for serialization / deserialization.
 */
constexpr static std::array FORCE_APPLICATOR_TYPES =
    std::array{ForceApplicatorType::Null, ForceApplicatorType::Sphere};

/**
 * @brief Abstraction over external force applicators.
 */
template <std::size_t Dimension> struct ExternalForceApplicator {
  const boundarycondition::ForceApplicatorType type =
      boundarycondition::ForceApplicatorType::Null;
  double radius{};
  double force_constant{};
  double delta{};
  std::array<double, Dimension> center{};
  std::array<double, Dimension> external_force{};

  std::vector<std::array<double, Dimension>> points_in_range{};
  std::vector<std::array<double, Dimension>> internal_force_points_in_range{};
  std::vector<int> indices_points_in_range{};

  ExternalForceApplicator(const boundarycondition::ForceApplicatorType type,
                          const double radius, const double force_constant,
                          const double delta,
                          const std::array<double, Dimension> &center)
      : type{type}, radius{radius},
        force_constant{force_constant}, delta{delta}, center{center} {}
  virtual ~ExternalForceApplicator() = default;
  ExternalForceApplicator(const ExternalForceApplicator &) = default;
  ExternalForceApplicator(ExternalForceApplicator &&) noexcept = default;
  ExternalForceApplicator &operator=(const ExternalForceApplicator &) = default;
  ExternalForceApplicator &
  operator=(ExternalForceApplicator &&) noexcept = default;

  virtual void update_position() = 0;

  virtual void apply_force(const std::vector<std::array<double, Dimension>> &,
                           const std::vector<double> &, const std::size_t,
                           std::vector<std::array<double, Dimension>> &) = 0;

  virtual void record_force(const double) = 0;
};

} // namespace boundarycondition
