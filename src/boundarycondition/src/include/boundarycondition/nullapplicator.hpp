// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation of the null force applicator
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/external_force_applicator.hpp"

namespace boundarycondition {

template <std::size_t Dimension>
struct NullApplicator : ExternalForceApplicator<Dimension> {
  NullApplicator(const double radius, const double force_constant,
                 const double delta,
                 const std::array<double, Dimension> &center)
      : ExternalForceApplicator<Dimension>{
            boundarycondition::ForceApplicatorType::Null, radius,
            force_constant, delta, center} {}
  NullApplicator(const NullApplicator &) = default;
  NullApplicator(NullApplicator &&) noexcept = default;
  NullApplicator &operator=(const NullApplicator &) = default;
  NullApplicator &operator=(NullApplicator &&) noexcept = default;
  ~NullApplicator() override = default;

  void update_position() override {}

  void apply_force(const std::vector<std::array<double, Dimension>> &,
                   const std::vector<double> &, const std::size_t,
                   std::vector<std::array<double, Dimension>> &) override {}

  void record_force(const double) override {}
};

} // namespace boundarycondition
