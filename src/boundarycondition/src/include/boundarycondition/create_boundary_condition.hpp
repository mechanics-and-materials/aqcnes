// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function to make a boundary conditions vector as per user input
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "boundarycondition/fixedboundary.hpp"
#include "boundarycondition/freeboundary.hpp"
#include "boundarycondition/slipboundary.hpp"
#include "exception/qc_exception.hpp"
#include "qcmesh/geometry/box.hpp"

namespace boundarycondition {

template <std::size_t Dimension>
Boundarycondition<Dimension>
create_boundary_condition(const BoundaryConditionType type) {
  switch (type) {
  case BoundaryConditionType::Free:
    return free::create<Dimension>();
  case BoundaryConditionType::Slip:
    return slip::create<Dimension>();
  case BoundaryConditionType::Fixed:
    return fixed::create<Dimension>();
  default:
    throw exception::QCException{
        "Invalid boundary condition type: " +
        std::to_string(static_cast<std::size_t>(type))};
  }
}

std::vector<Boundarycondition<3>> create_boundary_conditions(
    const std::array<TwoSidedBoundaryConditions, 3> &types,
    const qcmesh::geometry::Box<3> &bounding_box);

} // namespace boundarycondition
