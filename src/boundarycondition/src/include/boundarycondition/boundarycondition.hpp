// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Boundary conditions types and struct
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "qcmesh/array_ops/array_ops.hpp"
#include <array>
#include <string>
#include <vector>

namespace boundarycondition {

/**
 * @brief Supported boundary condition types.
 */
enum struct BoundaryConditionType : std::size_t { Free, Slip, Fixed };

struct TwoSidedBoundaryConditions {
  BoundaryConditionType lower = BoundaryConditionType::Free;
  BoundaryConditionType upper = BoundaryConditionType::Free;
};

/**
 * @brief List of all supported boundary condition types.
 *
 * Can be used for serialization / deserialization.
 */
constexpr static std::array BOUNDARY_CONDITION_TYPES =
    std::array{BoundaryConditionType::Free, BoundaryConditionType::Slip,
               BoundaryConditionType::Fixed};

/**
 * @brief Abstraction over boundary conditions.
 *
 * Design desicion: Instead of virtual functions / classical C++ polymorphism, function references are used (semi C style polymorphism).
 * This is to avoid raw C pointers or propagating `std::vector<std::unique_ptr<_>>` throughout the code.
 */
template <std::size_t Dimension> struct Boundarycondition {
  const BoundaryConditionType zone_type = BoundaryConditionType::Free;
  std::array<double, Dimension> recorded_force{};
  std::vector<std::array<double, Dimension>> bounds =
      std::vector<std::array<double, Dimension>>(Dimension);
  std::array<double, Dimension> normal{};
  std::size_t coordinate{};
  double bound{};

  void (&update_vectors_impl)(
      const Boundarycondition<Dimension> &,
      const std::vector<std::array<double, Dimension>> &,
      std::vector<std::array<double, Dimension>> &atom_vectors);

  void (&record_force_impl)(
      Boundarycondition<Dimension> &, const std::size_t noof_ghosts,
      const std::vector<std::array<double, Dimension>> &,
      const std::vector<std::array<double, Dimension>> &atom_forces);

  void update_vectors(
      const std::vector<std::array<double, Dimension>> &atom_locations,
      std::vector<std::array<double, Dimension>> &atom_vectors) {
    this->update_vectors_impl(*this, atom_locations, atom_vectors);
  }
  void
  record_force(const std::size_t noof_ghosts,
               const std::vector<std::array<double, Dimension>> &atom_locations,
               const std::vector<std::array<double, Dimension>> &atom_forces) {
    this->record_force_impl(*this, noof_ghosts, atom_locations, atom_forces);
  }
  [[nodiscard]] bool
  point_in_boundary(const std::array<double, Dimension> &p) const {
    return qcmesh::array_ops::dot(p, this->normal) > std::abs(this->bound);
  }
};

} // namespace boundarycondition
