// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Load external force applicator from restart file
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/external_force_applicator.hpp"
#include "boundarycondition/nanoindenter_sphere.hpp"
#include "boundarycondition/nullapplicator.hpp"
#include "exception/qc_exception.hpp"
#include "input/userinput.hpp"
#include "netcdf_io/load_external_force_applicator.hpp"
#include <memory>

namespace boundarycondition {

template <std::size_t Dimension>
std::unique_ptr<ExternalForceApplicator<Dimension>>
load_external_force_applicator(const std::filesystem::path &path,
                               const double min_indenter_increment) {
  const auto def = netcdf_io::load_external_force_applicator<Dimension>(path);
  const auto indenter_increment = std::max(def.delta, min_indenter_increment);
  switch (def.type) {
  case boundarycondition::ForceApplicatorType::Sphere:
    return std::make_unique<NanoindenterSphere<Dimension>>(
        def.radius, def.force_constant, indenter_increment, def.center);
  case boundarycondition::ForceApplicatorType::Null:
    return std::make_unique<NullApplicator<Dimension>>(
        def.radius, def.force_constant, indenter_increment, def.center);
  default:
    throw exception::QCException{
        "Invalid force applicator type: " +
        std::to_string(static_cast<std::size_t>(def.type))};
  }
}

template <std::size_t Dimension, class ApplicatorType>
std::unique_ptr<ExternalForceApplicator<Dimension>>
create_external_force_applicator(const input::Userinput &input) {
  auto indenter_center = std::array<double, Dimension>{};
  indenter_center[Dimension - 1] = input.indenter_radius + input.initial_offset;
  return std::make_unique<ApplicatorType>(
      input.indenter_radius, input.force_constant, input.indenter_increment,
      indenter_center);
}

template <std::size_t Dimension>
std::unique_ptr<ExternalForceApplicator<Dimension>>
load_external_force_applicator(const input::Userinput &input,
                               const std::size_t restartfile_iter) {
  if (input.intialize_force_applicator && input.restart_file_name.has_value() &&
      restartfile_iter > 2)
    return load_external_force_applicator<Dimension>(
        input.restart_file_name.value(), input.indenter_increment);
  if (input.intialize_force_applicator)
    return create_external_force_applicator<Dimension,
                                            NanoindenterSphere<Dimension>>(
        input);
  return create_external_force_applicator<Dimension, NullApplicator<Dimension>>(
      input);
}

} // namespace boundarycondition
