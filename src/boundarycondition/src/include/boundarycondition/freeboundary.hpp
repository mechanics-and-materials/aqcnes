// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation of the free boundary condition
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "geometry/point.hpp"
#include "qcmesh/array_ops/array_ops.hpp"

namespace boundarycondition::free {

namespace array_ops = qcmesh::array_ops;

template <std::size_t Dimension>
void update_vectors(const Boundarycondition<Dimension> &,
                    const std::vector<geometry::Point<Dimension>> &,
                    std::vector<geometry::Point<Dimension>> &) {}

template <std::size_t Dimension>
void record_force(Boundarycondition<Dimension> &, const std::size_t,
                  const std::vector<geometry::Point<Dimension>> &,
                  const std::vector<geometry::Point<Dimension>> &) {}

template <std::size_t Dimension> Boundarycondition<Dimension> create() {
  return Boundarycondition<Dimension>{
      .zone_type = BoundaryConditionType::Free,
      .update_vectors_impl = update_vectors<Dimension>,
      .record_force_impl = record_force<Dimension>};
}

} // namespace boundarycondition::free
