// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation of the nano-indenter force applicator
 * @authors P. Gupta
 */

#pragma once

#include "boundarycondition/external_force_applicator.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <boost/mpi.hpp>
#include <fstream>

namespace boundarycondition {

template <std::size_t Dimension>
struct NanoindenterSphere : ExternalForceApplicator<Dimension> {
  NanoindenterSphere(const double radius, const double force_constant,
                     const double delta,
                     const std::array<double, Dimension> &center)
      : ExternalForceApplicator<Dimension>{
            boundarycondition::ForceApplicatorType::Sphere, radius,
            force_constant, delta, center} {}

  NanoindenterSphere(const NanoindenterSphere &) = default;
  NanoindenterSphere(NanoindenterSphere &&) noexcept = default;
  NanoindenterSphere &operator=(const NanoindenterSphere &) = default;
  NanoindenterSphere &operator=(NanoindenterSphere &&) noexcept = default;
  ~NanoindenterSphere() override = default;

  [[nodiscard]] bool
  if_in_range(const std::array<double, Dimension> &location) const {
    return (qcmesh::array_ops::distance(location, this->center) < this->radius);
  }
  void update_position() override {
    namespace array_ops = qcmesh::array_ops;

    this->center[Dimension - 1] += this->delta;

    if (boost::mpi::communicator{}.rank() == 0) {
      std::cout << "updated indenter center position "
                << array_ops::as_streamable(this->center) << std::endl;
      auto displacement = this->center;
      displacement[2] -= this->radius;
      std::cout << "indenter lowest point at "
                << array_ops::as_streamable(displacement) << std::endl;
    }
  }

  void
  apply_force(const std::vector<std::array<double, Dimension>> &locations,
              const std::vector<double> &weights, const std::size_t noof_ghosts,
              std::vector<std::array<double, Dimension>> &forces) override {

    namespace array_ops = qcmesh::array_ops;
    this->external_force.fill(0.0);

    this->points_in_range.clear();
    this->internal_force_points_in_range.clear();
    this->indices_points_in_range.clear();

    for (std::size_t node_index = 0;
         node_index < locations.size() - noof_ghosts; node_index++) {
      if (if_in_range(locations[node_index])) {
        const auto r = array_ops::as_eigen(locations[node_index]) -
                       array_ops::as_eigen(this->center);
        const auto r_mag = r.norm();
        const auto f = (r / r_mag) * 3.0 * weights[node_index] *
                       this->force_constant * (this->radius - r_mag) *
                       (this->radius - r_mag);

        this->points_in_range.push_back(locations[node_index]);
        this->internal_force_points_in_range.push_back(forces[node_index]);
        this->indices_points_in_range.push_back(node_index);
        array_ops::as_eigen(forces[node_index]) += f;
        array_ops::as_eigen(this->external_force) += f;
      }
    }
  }

  void record_force(const double t) override {
    namespace array_ops = qcmesh::array_ops;
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    auto f = std::array<double, Dimension>{};
    boost::mpi::all_reduce(
        boost::mpi::communicator{}, this->external_force.data(),
        static_cast<int>(Dimension), f.data(), std::plus<>());

    if (mpi_rank == 0) {
      std::ofstream f_external;
      const auto filename = "./ExternalForce_sphere_" +
                            std::to_string(2.0 * this->radius) + "_" +
                            std::to_string(t) + ".dat";

      f_external.open(filename, std::ios::app);
      f_external << array_ops::as_streamable(this->center) << " "
                 << array_ops::as_streamable(f) << std::endl;
      f_external.close();

      std::cout << array_ops::as_streamable(this->center) << std::endl;
    }

    std::ofstream f_points;
    if (this->points_in_range.size() > 0) {
      std::ofstream f_points;
      const auto filename =
          "./External_spherePoints_" + std::to_string(2.0 * this->radius) +
          "_" + std::to_string(t) + "_" + std::to_string(mpi_rank) + ".dat";
      f_points.open(filename, std::ios::app);
      f_points << "*****************************" << std::endl;
      f_points << array_ops::as_streamable(this->center) << " "
               << array_ops::as_streamable(f) << std::endl;
      for (std::size_t i = 0; i < this->points_in_range.size(); i++) {
        f_points << i << " " << this->indices_points_in_range[i] << " "
                 << array_ops::as_streamable(this->points_in_range[i]) << " "
                 << array_ops::as_streamable(
                        this->internal_force_points_in_range[i])
                 << std::endl;
      }

      f_points.close();
    }
  }
};

} // namespace boundarycondition
