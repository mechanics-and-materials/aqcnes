// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function to make a boundary conditions vector as per user input
 * @authors P. Gupta, S. Saxena
 */

#include "boundarycondition/create_boundary_condition.hpp"

namespace boundarycondition {

std::vector<Boundarycondition<3>> create_boundary_conditions(
    const std::array<TwoSidedBoundaryConditions, 3> &types,
    const qcmesh::geometry::Box<3> &bounding_box) {

  auto boundary_conditions = std::vector<Boundarycondition<3>>{};
  boundary_conditions.reserve(6);
  for (std::size_t zone_ctr = 0; zone_ctr < types.size(); zone_ctr++) {
    // lower boundary condition
    auto boundary_condition_lower =
        create_boundary_condition<3>(types[zone_ctr].lower);
    boundary_condition_lower.coordinate = zone_ctr;
    boundary_condition_lower.bound = bounding_box.lower[zone_ctr] + 0.01;
    auto normal = geometry::Point<3>{};
    normal[zone_ctr] = -1.0;
    boundary_condition_lower.normal = normal;
    boundary_conditions.emplace_back(boundary_condition_lower);

    // upper boundary condition
    auto boundary_condition_upper =
        create_boundary_condition<3>(types[zone_ctr].upper);
    boundary_condition_upper.coordinate = zone_ctr;
    boundary_condition_upper.bound = bounding_box.upper[zone_ctr] - 0.01;
    normal[zone_ctr] = 1.0;
    boundary_condition_upper.normal = normal;
    boundary_conditions.emplace_back(boundary_condition_upper);
  }

  return boundary_conditions;
}

} // namespace boundarycondition
