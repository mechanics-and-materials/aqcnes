#!/usr/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Coarse mesh relaxation."""

from pathlib import Path
from typing import Any

import numpy as np
from test_workflows import Base, main

MATERIAL = "20x20x20_coarse_only"


class TestCoarseMeshRelaxationFiniteTemperature(Base.TestWorkflow):
    """Coarse mesh relaxation for finite temperature."""

    input_data = (
        Path("data") / "coarse_mesh_relaxation" / "finite",
        Path("data") / "coarse_mesh_relaxation" / "mesh_files",
        Path("../../data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
    )
    binaries = ("Relaxation3D_FINITE_K",)
    computed_data_file = f"energy_volume_{MATERIAL}_300.000000.dat"

    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            300.0,
            377829.9081,
            -98615.26157,
            -111592.3364,
            1240.922221,
            32000.68744,
            0.0,
            0.0,
            0.0,
            0.0,
        ]
    )


class TestCoarseMeshRelaxationZeroTemperature(Base.TestWorkflow):
    """Coarse mesh relaxation for zero temperature."""

    input_data = (
        Path("data") / "coarse_mesh_relaxation" / "zero",
        Path("data") / "coarse_mesh_relaxation" / "mesh_files",
        Path("../../data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
    )
    binaries = ("Relaxation3D_0K",)
    computed_data_file = f"energy_volume_{MATERIAL}_0.dat"

    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            0,
            377599.6649,
            -112849.1786,
            -112849.1786,
            0,
            32000.68744,
            0,
            0,
            0,
            0,
        ]
    )


if __name__ == "__main__":
    main()
