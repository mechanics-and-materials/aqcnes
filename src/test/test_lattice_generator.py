#!/usr/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Integration tests for lattice generator."""

from pathlib import Path
from typing import Any

import numpy as np
from test_workflows import Base, main


class TestLatticeGenerator(Base.TestWorkflow):
    """Test suite for lattice generator."""

    input_data = (Path("data") / "lattice_generator/latticegenerator_parameters.json",)
    binaries = ("mesh_generator",)
    computed_data_file = "mesh_files/periodic_info.txt"

    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            [-22.8633, -4.44089e-16, 0.0],
            [45.72653497, 22.86326748, 7.23],
        ]
    )

    def test_workflow(self) -> None:
        """Test correctnes of periodic_info.txt and mesh_names.txt."""
        super().test_workflow()
        with open(
            self.workdir / "mesh_files" / "mesh_names.txt", encoding="utf-8"
        ) as stream:
            mesh_file = stream.read().strip()
        self.assertEqual(mesh_file, '"./mesh_files/sigma5_2x2x1_000823.nc"')


if __name__ == "__main__":
    main()
