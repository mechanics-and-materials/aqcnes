// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check if an openKIM based relaxation can do 10 iterations
 * @authors S. Saxena, M. Spinola
 */

#include "create_test_mesh.hpp"
#include "geometry/point.hpp"
#include "materials/create_material_base.hpp"
#include "materials/kim_fcc.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "solver/relaxation.hpp"
#include "solver/solver.hpp"
#include "test_relaxation.hpp"
#include <Eigen/Core>
#include <gmock/gmock.h>
#include <memory>

namespace array_ops = qcmesh::array_ops;

namespace kim {
namespace {

constexpr std::size_t DIMENSION = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

#ifdef FINITE_TEMPERATURE
constexpr bool FINITE_T = true;
#else
constexpr bool FINITE_T = false;
#endif

using QCMesh = meshing::QCMesh<DIMENSION, DIMENSION, NIDOF,
                               meshing::NodalDataMultispecies<NOI>>;

/**
 * @brief Simple configuration for a 4x4x4 lattice for a grid parameter
 * of 3.615.
 */
auto config_4x4x4(const lattice::NanoIndentation<3> &config) {
  return lattice::LatticeGeneratorParameters<DIMENSION>{
      .n_coarsening_levels = 1,
      .atomistic_domain = {{7.3, 7.3, 7.3}},
      .save_lattice_vectors = false,
      .lattice_parameter = 3.615,
      .lattice_structure = lattice::LatticeStructure::FCC,
      .name = "NanoIndentation",
      .coarsening_factors = {{1.0, 4.0, 16.0, 50.0, 100.0}},
      .domain_expansion_factors = {{15.0, 40.0, 50.0, 100.0, 500}},
      .lattice_file_name = "",
      .rotation = {{{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}}},
      .seed_point_offset_coeffs = {},
      .method = config};
}

template <std::size_t Dimension>
materials::Material<Dimension> kim_copper(const double atomic_mass,
                                          const double lattice_parameter,
                                          const std::string &model_name) {

  materials::Material<Dimension> kim_fcc_mat;
  kim_fcc_mat.cluster_model =
      std::make_unique<materials::KimFcc<Dimension, NOI>>(
          model_name, atomic_mass, std::vector<std::string>{"Cu"});

  kim_fcc_mat.lattice_parameter_vec = std::array<double, 3>{
      {lattice_parameter, lattice_parameter, lattice_parameter}};

  const auto r = Eigen::Matrix<double, Dimension, Dimension>::Identity(
      Dimension, Dimension);
  kim_fcc_mat.initial_rotation = r;
  kim_fcc_mat.atomic_masses = kim_fcc_mat.cluster_model->atomic_masses;
  kim_fcc_mat.neighbor_cutoff_radius =
      kim_fcc_mat.cluster_model->neighbor_cutoff_radius;
  kim_fcc_mat.lattice_structure = kim_fcc_mat.cluster_model->lattice_structure;
  kim_fcc_mat.lattice_type = lattice::LatticeStructure::FCC;
  kim_fcc_mat.assign_basis_vectors(r.transpose());
  return kim_fcc_mat;
}

input::KimFccProperties kim_material_properties() {
  return input::KimFccProperties{
      .atomic_mass = 63.546,
      .model_name = "EAM_NN_Johnson_1988_Cu__MO_887933271505_002",
      .species = std::vector<std::string>{"Cu"}};
}

input::Userinput kim_user_input() {
  auto input = input::Userinput{
      // SimulationSettings
      .mesh_file = "./mesh_files/Box_4x4x4_000365.nc",
      .restart_file_name = std::nullopt,
      .output_initial_partition = true,
      .output_centro_symmetry = true,
      .node_effective_radius = 2.5,
      .minimum_barycenter_weight = 1.0,
      .verlet_buffer_factor = 1.75,
      .neighbour_update_displacement = 0.35,
      .mesh_overlap_buffer_radius = 1.5,
      .constrained_direction = 1,
      .neighbour_update_style = input::NeighbourUpdateStyle::ADAPTIVE,
      .remove_previous_neighbours = true,
      // ThermalSettings
      .T_ref = 300.0,
      .T_min = 0.01,
      .T_max = 5000.0,
      .Phi_ref = 5.545,
      .isentropic = true,
      .onsager_coefficient = 15.926734,
      .heat_rate_tolerance_per_atom = 1.0e-3,
      // Potentials
      .material = kim_material_properties(),
      .intialize_force_applicator = true,
      .indenter_radius = 25.0,
      .force_constant = 1000.0,
      .indenter_increment = 0.0001,
      .strain_increment_time_step = 0.01,
      .indenter_center = {{0.0, 0.0, 0.0}},
      .initial_offset = -0.3,
      .external_displacement_gradient = {{{1.0, 0.0, 0.0},
                                          {0.0, 1.0, 0.0},
                                          {0.0, 0.0, 1.0}}},
      .external_iterations = 40,
      // FIRERelaxation
      .fire_time_step = 5.0,
      .fire_initial_alpha = 0.1,
      .fire_f_alpha = 0.99,
      .fire_f_increase_t = 1.01,
      .fire_f_decrease_t = 0.5,
      .fire_min_steps_reset = 5,
      .fire_steps_post_reset = 0,
      .fire_max_time_step = 11.0,
      .fire_dt_output_iter = 9999,
      .fire_max_iter = 10,
      .fire_min_residual = 0.001,
      // PETSC
      .abs_tolerance = 5e-05,
      .rel_tolerance = 5e-05,
      .s_tolerance = 5e-05,
      .max_iter = 200,
      .max_feval = 200,
      // TimeStepping
      .safety_factor = 0.75,
      .t_initial = 0.0,
      .t_final = 1.0,
      .time_step = 3.9e-7,
      .max_time_steps = 9999,
      // Periodicity
      .periodicity =
          {{meshing::PeriodicityParameters{.min_coordinate = -7.23,
                                           .length = 14.46,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -7.23,
                                           .length = 14.46,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -14.46,
                                           .length = 14.46,
                                           .relaxation_constraint = false}}},
      .a_ref = 3.615,
  };
  //const auto properties = kim_material_properties();
  return input;
}

TEST(test_kim, feature_can_do_10_iterations) {
  solver::SolverVariables<DIMENSION, 1> solver_variables;
  solver_variables.input = kim_user_input();
  const auto properties = kim_material_properties();
  solver_variables.materials.resize(1);
  solver_variables.materials[0] = kim_copper<DIMENSION>(
      properties.atomic_mass, 3.615, properties.model_name);

  solver_variables.relaxation_fire_parameters =
      solver::fire_parameters_from_userinput(solver_variables.input);
  const auto nano_intendation_config = lattice::NanoIndentation<3>{
      .domain_boundaries = lattice::Domain<DIMENSION>{
          .lower = {{-5000.0, -5000.0, -10000.0}},
          .upper = {{5000.0, 5000.0, 0.1}},
      }};
  const auto config = config_4x4x4(nano_intendation_config);
  const auto mesh = create_test_mesh(
      solver_variables.input.Phi_ref, solver_variables.input.T_ref,
      solver_variables.input.periodicity, nano_intendation_config, config,
      std::vector{
          materials::create_material_base(solver_variables.materials[0])});

  auto out_mesh = do_relaxation<DIMENSION, NIDOF, NOI>(solver_variables, mesh);

  const auto eps = 1.E-6;
  const auto expected_mesh_volume = FINITE_T ? 3054.55972 : 3022.684416;
  const auto mesh_volume = out_mesh.volume();
  EXPECT_NEAR(mesh_volume, expected_mesh_volume, eps);

  const auto expected_e = FINITE_T ? -896.7475022 : -906.2399985;
  const auto e_global =
      qcmesh::mpi::all_reduce_sum(solver::local_energy(out_mesh));
  EXPECT_NEAR(e_global, expected_e, eps);
  if constexpr (FINITE_T) {
    const auto k_global =
        qcmesh::mpi::all_reduce_sum(solver::local_k(out_mesh));
    EXPECT_NEAR(k_global, 9.927165, eps);
  }
  const auto w_global =
      qcmesh::mpi::all_reduce_sum(solver::local_weight(out_mesh));
  EXPECT_DOUBLE_EQ(w_global, 256.);
}

TEST(test_kim, energy_is_correct) {
  const auto lattice_parameter = 3.615;

  // Initialize material for building lattice
  const auto *const model_name =
      "EAM_Dynamo_MishinMehlPapaconstantopoulos_2001_Cu__MO_346334655118_005";

  const auto mat = kim_copper<DIMENSION>(0., lattice_parameter, model_name);

  const auto center = geometry::Point<DIMENSION>{{0.0, 0.0, 0.0}};
  const auto basis = mat.basis_vectors;

  const auto px =
      geometry::Point<DIMENSION>{{basis[0][0], basis[1][0], basis[2][0]}};
  const auto py =
      geometry::Point<DIMENSION>{{basis[0][1], basis[1][1], basis[2][1]}};
  const auto pz =
      geometry::Point<DIMENSION>{{basis[0][2], basis[1][2], basis[2][2]}};

  std::vector<geometry::Point<DIMENSION>> lattice;
  std::vector<geometry::Point<DIMENSION>> neighbours;
  geometry::Point<DIMENSION> lattice_point;

  auto center_nodal_data = meshing::NodalDataMultispecies<NOI>{};
  std::vector<meshing::NodalDataMultispecies<NOI>> neighbours_nodal_data;

  for (int l = -2; l < 3; l++)
    for (int m = -2; m < 3; m++)
      for (int n = -2; n < 3; n++) {
        array_ops::as_eigen(lattice_point) =
            array_ops::as_eigen(center) + l * array_ops::as_eigen(px) +
            m * array_ops::as_eigen(py) + n * array_ops::as_eigen(pz);
        lattice.push_back(lattice_point);
      }
  for (const auto &p : lattice) {
    if (array_ops::distance(center, p) > 1.0e-3 &&
        array_ops::distance(center, p) <= (mat.neighbor_cutoff_radius)) {
      neighbours.push_back(p);
      neighbours_nodal_data.push_back(center_nodal_data);
    }
  }

  // Compute forces and energy
  // double energy = 0.0;
  const auto energy = mat.get_cluster_energy(
      center, neighbours, center_nodal_data, neighbours_nodal_data);

  const auto eps = 1.E-6;
  EXPECT_NEAR(energy, -3.54, eps);
}

} // namespace
} // namespace kim
