# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.


"""Test template to test pipelines (generate mesh, run simulation, ...).

Used by other python files which define test cases derived from
`Base.TestWorkflow`.

For all options including defaults, run `./test_workflows.py --help`.

"""

from __future__ import annotations

import argparse
import os
import shutil
import subprocess
import sys
import unittest
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import TYPE_CHECKING, Any

import numpy as np

if TYPE_CHECKING:
    from collections.abc import Sequence

    from numpy.typing import NDArray

AQCNES_SRC_TEST = Path(os.path.realpath(__file__)).parent
BINARY_DIR = AQCNES_SRC_TEST.parent.parent / "build" / "main"
N_PROCESSES = 2
TOLERANCE = 1.0e-6
WORKDIR = None


def main() -> None:
    """Entry point for the tests."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--bin",
        dest="BINARY_DIR",
        default=BINARY_DIR,
        type=lambda s: Path(os.path.realpath(s)),
        help="Folder containing the workflow binaries, "
        "e.g. Relaxation3D binary. Will be guessed by default.",
    )
    parser.add_argument(
        "--n-processes",
        dest="N_PROCESSES",
        type=int,
        default=N_PROCESSES,
        help="Number of MPI processes.",
    )
    parser.add_argument(
        "--tolerance",
        dest="TOLERANCE",
        type=float,
        default=TOLERANCE,
        help="The tolerance for the residual of the simulation.",
    )
    parser.add_argument(
        "--workdir",
        "-w",
        dest="WORKDIR",
        default=WORKDIR,
        type=Path,
        help="Working directory. If not specified, it will be a temporary folder.",
    )

    args, unittest_args = parser.parse_known_args()
    globals().update(vars(args))
    unittest.main(argv=sys.argv[:1] + unittest_args)


class Base:  # pylint: disable=too-few-public-methods
    """A namespace which prevents discovery of its contents."""

    class TestWorkflow(unittest.TestCase):
        """Base class for workflow tests.

        A workflow consists of
        * Input data, a set of files which will be copied to a working directory
        * A sequence of binaries called in order, which will process the input data
        * The name of the final output file

        The test will compare the contents of output file
        to the reference data.
        """

        input_data: Sequence[Path]
        binaries: Sequence[str]
        computed_data_file: str
        reference_data: np.ndarray[Any, np.dtype[np.float64]]
        workdir: Path = Path()
        extra_dirs: Sequence[Path] = ()
        _workdir_obj: TemporaryDirectory[str] | None = None

        @classmethod
        def setUpClass(cls) -> None:
            """Setup for all tests."""
            if WORKDIR is None:
                cls._workdir_obj = (
                    TemporaryDirectory()  # pylint: disable=consider-using-with
                )
                cls.workdir = Path(cls._workdir_obj.name)
            else:
                cls.workdir = WORKDIR

            (cls.workdir / "initial_partition").mkdir(parents=True, exist_ok=True)
            (cls.workdir / "solution_files").mkdir(parents=True, exist_ok=True)
            (cls.workdir / "restart_files").mkdir(parents=True, exist_ok=True)
            for folder in cls.extra_dirs:
                (cls.workdir / folder).mkdir(parents=True, exist_ok=True)

            for path in cls.input_data:
                if (AQCNES_SRC_TEST / path).is_dir():
                    shutil.copytree(
                        AQCNES_SRC_TEST / path, cls.workdir, dirs_exist_ok=True
                    )
                else:
                    shutil.copy(AQCNES_SRC_TEST / path, cls.workdir)

            missing_binaries = [
                b for b in cls.binaries if not (BINARY_DIR / b).is_file()
            ]
            if missing_binaries:
                msg = f"Binaries missing {', '.join(missing_binaries)}"
                raise unittest.SkipTest(msg)

        @classmethod
        def tearDownClass(cls) -> None:
            """Cleanup workdir."""
            if cls._workdir_obj is not None:
                cls._workdir_obj.cleanup()

        def load_computed_data(self, path: Path) -> NDArray[np.float64]:
            """Load tabulated data from a text file.

            Can be overridden by derived classes to load different formats.
            """
            return np.loadtxt(path)

        def test_workflow(self) -> None:
            """Run a workflow and check output."""
            for binary in self.binaries:
                cmd = ("mpirun", "-np", str(N_PROCESSES), str(BINARY_DIR / binary))
                sys.stdout.write(f"\n>>> {' '.join(cmd)}\n")
                subprocess.check_call(cmd, cwd=self.workdir)
            computed_data = self.load_computed_data(
                self.workdir / self.computed_data_file
            )
            np.testing.assert_allclose(
                computed_data, self.reference_data, atol=TOLERANCE
            )
