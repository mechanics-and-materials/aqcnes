// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check if a MEAM based infinite model relaxation works well
 * @authors S. Saxena, M. Spinola
 */

#include "lattice/lattice_cache.hpp"
#include "materials/create_material.hpp"
#include "solver/infinite_model_relaxation.hpp"
#include "testing/find_path.hpp"
#include "testing/matchers.hpp"
#include <gmock/gmock.h>
#include <memory>

namespace {

input::MeamProperties meam_material_properties() {
  return input::MeamProperties{
      .library_file_path = testing::io::find_path(
          "data/potentials_NIST/Al-Cu/library.AlCu.meam"),
      .parameter_file_path =
          testing::io::find_path("data/potentials_NIST/Al-Cu/AlCu.meam"),
      .potential_material_name = "AlCuMEAM",
      .species = std::vector<std::string>{"Al", "Cu"}};
}

input::Userinput test_user_input(const double concentration) {
  return input::Userinput{
      // SolverTolerances
      .max_force_per_atom = 1.0e-6,
      .max_thermal_force_per_atom = 1.0e-6,
      .max_periodic_force_residue_per_atom = 5.0e-07,
      // SimulationSettings
      .restart_file_name = std::nullopt,
      .output_initial_partition = true,
      .output_centro_symmetry = true,
      .node_effective_radius = 2.5,
      .minimum_barycenter_weight = 1.0,
      .verlet_buffer_factor = 1.75,
      .neighbour_update_displacement = 0.05,
      .mesh_overlap_buffer_radius = 1.5,
      .constrained_direction = 0,
      .neighbour_update_style = input::NeighbourUpdateStyle::ADAPTIVE,
      .remove_previous_neighbours = true,
      .quad_type = 3,
      // ThermalSettings
      .T_ref = 0.0,
      .T_min = 0.01,
      .T_max = 5000.0,
      .Phi_ref = 5.545,
      .isentropic = true,
      .onsager_coefficient = 15.926734,
      .heat_rate_tolerance_per_atom = 1.0e-3,
      // Potentials
      .material = meam_material_properties(),
      // Multispecies
      .impurity_atomic_masses = {27.0},
      .initial_impurity_concentrations = {concentration},
      .embedding_interpolation_coeffs = {{1.0}, {1.0}},
      // ExternalIndenter
      .intialize_force_applicator = true,
      .indenter_radius = 25.0,
      .force_constant = 1000.0,
      .indenter_increment = 0.0001,
      .strain_increment_time_step = 0.01,
      .indenter_center = {{0.0, 0.0, 0.0}},
      .initial_offset = -0.3,
      .external_displacement_gradient = {{{1.0, 0.0, 0.0},
                                          {0.0, 1.0, 0.0},
                                          {0.0, 0.0, 1.0}}},
      .external_iterations = 40,
      // FIRERelaxation
      .fire_time_step = 5.0,
      .fire_initial_alpha = 0.1,
      .fire_f_alpha = 0.99,
      .fire_f_increase_t = 1.01,
      .fire_f_decrease_t = 0.5,
      .fire_min_steps_reset = 5,
      .fire_steps_post_reset = 0,
      .fire_max_time_step = 11.0,
      .fire_max_iter = 1000,
      .fire_min_residual = 0.001,
      // PETSC
      .abs_tolerance = 5e-05,
      .rel_tolerance = 5e-05,
      .s_tolerance = 5e-05,
      .max_iter = 200,
      .max_feval = 200,
      // TimeStepping
      .safety_factor = 0.75,
      .t_initial = 0.0,
      .t_final = 1.0,
      .time_step = 3.9e-7,
      .max_time_steps = 9999,
      // Periodicity
      .periodicity =
          {{meshing::PeriodicityParameters{.min_coordinate = -14.44,
                                           .length = 28.88,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -14.44,
                                           .length = 28.88,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -10.83,
                                           .length = 21.66,
                                           .relaxation_constraint = false}}},
      .a_ref = 3.615,
  };
}

struct ParameterSet {
  double concentration{};
  double expected_lattice_spacing{};
  double expected_energy{};
};

std::ostream &operator<<(std::ostream &out, const ParameterSet &parameter_set) {
  return out << "{concentration: " << parameter_set.concentration
             << ", expected_lattice_spacing: "
             << parameter_set.expected_lattice_spacing
             << ", expected_energy: " << parameter_set.expected_energy << '}';
}

struct InfiniteModelRelaxationTest : ::testing::TestWithParam<ParameterSet> {};

auto create_test_lattice_cache() {
  return lattice::LatticeCache<3>{
      .n_sub_lattice = 1,
      .lattice_type = lattice::LatticeStructure::FCC,
      .suffix = "000365",
      .lattice_parameters = {{3.6, 3.6, 3.6}},
      .rotated_basis_matrix = {{{1.8, 0, 1.8}, {1.8, 1.8, 0}, {0, 1.8, 1.8}}},
      .seed_point_offsets = {{0.0, 0.0, 0.0}},
      .rotation = {{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}}};
}

TEST_P(InfiniteModelRelaxationTest,
       produces_correct_energy_and_lattice_spacing) {
  const auto parameter_set = GetParam();
  const auto input = test_user_input(parameter_set.concentration);
  const auto properties = meam_material_properties();
  const auto material =
      materials::create_material<3, 1>(input, create_test_lattice_cache());
  const auto t = 0.0;
  const auto results = solver::relax_infinite<3, 1, 2>(
      input, material, material.basis_vectors, t);
  const auto eps = 1.E-5;
  EXPECT_NEAR(2.0 * results.deformed_basis[2][2],
              parameter_set.expected_lattice_spacing, eps);
  EXPECT_NEAR(results.e, parameter_set.expected_energy, eps);
}
INSTANTIATE_TEST_CASE_P(
    InfiniteModelRelaxation, InfiniteModelRelaxationTest,
    testing::Values(ParameterSet{.concentration = 0.0,
                                 .expected_lattice_spacing = 3.62002,
                                 .expected_energy = -3.54},
                    ParameterSet{.concentration = 0.1,
                                 .expected_lattice_spacing = 3.67041,
                                 .expected_energy = -3.55045},
                    ParameterSet{.concentration = 0.2,
                                 .expected_lattice_spacing = 3.71817,
                                 .expected_energy = -3.56327},
                    ParameterSet{.concentration = 0.3,
                                 .expected_lattice_spacing = 3.76421,
                                 .expected_energy = -3.57416},
                    ParameterSet{.concentration = 0.4,
                                 .expected_lattice_spacing = 3.80894,
                                 .expected_energy = -3.57974},
                    ParameterSet{.concentration = 0.5,
                                 .expected_lattice_spacing = 3.85245,
                                 .expected_energy = -3.57715},
                    ParameterSet{.concentration = 0.6,
                                 .expected_lattice_spacing = 3.89454,
                                 .expected_energy = -3.56382},
                    ParameterSet{.concentration = 0.7,
                                 .expected_lattice_spacing = 3.93487,
                                 .expected_energy = -3.53742},
                    ParameterSet{.concentration = 0.8,
                                 .expected_lattice_spacing = 3.97303,
                                 .expected_energy = -3.49583},
                    ParameterSet{.concentration = 0.9,
                                 .expected_lattice_spacing = 4.00862,
                                 .expected_energy = -3.4372},
                    ParameterSet{.concentration = 1.0,
                                 .expected_lattice_spacing = 4.04135,
                                 .expected_energy = -3.35998}),
    [](const testing::TestParamInfo<InfiniteModelRelaxationTest::ParamType>
           &info) {
      return std::string{"c_"} +
             std::to_string(static_cast<int>(10. * info.param.concentration));
    });

} // namespace
