// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Tests to check if netcdf_io can load-write-load the mesh and recover the same force residual
 * @authors S. Saxena
 */

#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "input/userinput.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "meshing/write_mesh_to_nc.hpp"
#include "solver/solver.hpp"
#include "testing/find_path.hpp"
#include <boost/mpi.hpp>
#include <gmock/gmock.h>

namespace {

constexpr std::size_t DIMENSION = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

input::Userinput test_user_input() {
  return input::Userinput{
      // SolverTolerances
      .max_force_per_atom = 1e-04,
      .max_thermal_force_per_atom = 1e-04,
      .max_periodic_force_residue_per_atom = 1e-04,
      // SimulationSettings
      .mesh_file = "./mesh_files/sigma5_000213.nc",
      .lattice_file = "./mesh_files/sigma5.txt",
      .restart_file_name = std::nullopt,
      .output_initial_partition = false,
      .output_centro_symmetry = false,
      .node_effective_radius = 2.5,
      .minimum_barycenter_weight = 1.0,
      .verlet_buffer_factor = 1.75,
      .neighbour_update_displacement = 0.05,
      .mesh_overlap_buffer_radius = 1.5,
      .constrained_direction = std::nullopt,
      .neighbour_update_style = input::NeighbourUpdateStyle::ADAPTIVE,
      .remove_previous_neighbours = true,
      .quad_type = 3,
      // ThermalSettings
      .T_ref = 300.0,
      .T_min = 0.01,
      .T_max = 5000.0,
      .Phi_ref = 5.545,
      .isentropic = true,
      .onsager_coefficient = 15.926734,
      .heat_rate_tolerance_per_atom = 1.0e-3,
      // NEBSettings
      .number_of_replicas = 9,
      .neb_cutoff = 0.01,
      .parallel_band_stiffness = 1.0,
      .perpendicular_band_stiffness = 0.0,
      .reference_energy_barrier = 0.25,
      .do_thermal_relaxation = true,
      .fix_initial_final = false,
      .turn_on_nudging = true,
      .neb_verbose = false,
      // Potentials
      .material =
          input::SetflEAMProperties{
              .potential_file_path = testing::io::find_path(
                  "data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
              .potential_material_name = "CuEAM",
              .funcfl_file = false},
      // Multispecies
      .impurity_atomic_masses = {0.0},
      .initial_impurity_concentrations = {0.0},
      .concentration_rate_coefficients = {1.0},
      .concentration_update_cutoff_radius = 3.0,
      .concentration_update_style =
          input::ConcentrationUpdateStyle::NEB_master_equation,
      .embedding_interpolation_coeffs = {{1.0}},
      // ExternalIndenter
      .intialize_force_applicator = true,
      .indenter_radius = 25.0,
      .force_constant = 1000.0,
      .indenter_increment = 0.0001,
      .strain_increment_time_step = 0.01,
      .indenter_center = {{0.0, 0.0, 0.0}},
      .initial_offset = -0.3,
      .external_displacement_gradient = {{{1.0, 0.0, 0.0},
                                          {0.0, 1.0, 0.0},
                                          {0.0, 0.0, 1.0}}},
      .external_iterations = 40,
      // FIRERelaxation
      .fire_time_step = 5.0,
      .fire_initial_alpha = 0.1,
      .fire_f_alpha = 0.99,
      .fire_f_increase_t = 1.01,
      .fire_f_decrease_t = 0.5,
      .fire_min_steps_reset = 5,
      .fire_steps_post_reset = 0,
      .fire_max_time_step = 11.0,
      .fire_dt_output_iter = 9999,
      .fire_max_iter = 20,
      .fire_min_residual = 0.001,
      // PETSC
      .abs_tolerance = 5e-05,
      .rel_tolerance = 5e-05,
      .s_tolerance = 5e-05,
      .max_iter = 200,
      .max_feval = 200,
      // TimeStepping
      .safety_factor = 0.75,
      .t_initial = 0.0,
      .t_final = 1.0,
      .time_step = 100000.0,
      .max_time_steps = 5,
      .enable_vn_stable_time_stepping = true,
      // Periodicity
      .periodicity =
          {{meshing::PeriodicityParameters{.min_coordinate = -11.43163374,
                                           .length = 22.86326748,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -11.43163374,
                                           .length = 22.86326748,
                                           .relaxation_constraint = false},
            std::nullopt}},
      .a_ref = 3.61,
      // Datagen for GNN Training
      .clusters_compilation_file_name =
          "./solution_files/datagen_clusters_0.dat",
      .mean_pos_std_dev = 0.015,
      .phi_std_dev = 0.2,
      .noof_samples = 280,
      .dataset_stride = 1,
  };
}

auto create_test_lattice_cache() {
  return lattice::LatticeCache<3>{
      .n_sub_lattice = 1,
      .lattice_type = lattice::LatticeStructure::FCC,
      .suffix = "000213",
      .lattice_parameters = {{3.615, 3.615, 3.615}},
      .rotated_basis_matrix = {{{1.14316, -0.571582, 1.71475},
                                {2.28633, 1.71475, 0.571582},
                                {0.0, 1.8075, 1.8075}}},
      .seed_point_offsets = {{0.0, 0.0, 0.0}},
      .rotation = {{{0.948683, -0.316228, 0.0},
                    {0.316228, 0.948683, 0.0},
                    {0.0, 0.0, 1.0}}}};
}

meshing::QCMesh<DIMENSION, DIMENSION> read_restart_file(
    const std::filesystem::path &path,
    const std::vector<materials::Material<DIMENSION>> &materials,
    const std::array<std::optional<meshing::PeriodicityParameters>, DIMENSION>
        &periodicity) {
  auto [_, mesh] =
      meshing::load_restart_mesh_from_nc(path, materials, periodicity);
  meshing::redistribute(mesh);
  mesh.reinitialize_exchange_vectors();
  return mesh;
}

double
compute_residual(solver::SolverVariables<DIMENSION, NOI> &solver_variables) {
  solver::Solver::set_solver_dofs<DIMENSION, NIDOF>(solver_variables);
  solver::Solver::create_solution_vectors<DIMENSION>(solver_variables);
  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<NOI>,
                                      DIMENSION, NIDOF>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      static_cast<int>(solver_variables.input.max_iter),
      static_cast<int>(solver_variables.input.max_feval), solver_variables);
  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<NOI>, DIMENSION, NIDOF>(solver_variables);
  solver_variables.residual =
      solver_variables.local_mesh.repatoms.force_residual();
  return solver_variables.residual;
}

solver::SolverVariables<DIMENSION, NOI>
setup_solver_variables(const input::Userinput &input,
                       const std::filesystem::path &restart_file) {
  solver::SolverVariables<DIMENSION, NOI> solver_variables;
  solver_variables.input = input;
  solver_variables.materials.emplace_back(materials::create_material<3, 1>(
      solver_variables.input, create_test_lattice_cache()));
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIMENSION>(
          solver_variables.input, 0);
  solver_variables.relaxation_fire_parameters =
      solver::fire_parameters_from_userinput(solver_variables.input);
  solver_variables.apply_external_force = false;
  solver_variables.apply_boundary_conditions = false;
  solver_variables.isentropic = false;
  // First read of the restart file
  solver_variables.local_mesh =
      read_restart_file(restart_file, solver_variables.materials,
                        solver_variables.input.periodicity);
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          solver_variables.input.boundary_types,
          solver_variables.local_mesh.domain_bound);
  return solver_variables;
}

TEST(test_restart_io, write_restart_can_be_recovered_by_read_restart) {
  const auto restart_file =
      testing::io::find_path("src/test/data/restart/restart_test_file.nc");
  auto solver_variables =
      setup_solver_variables(test_user_input(), restart_file);

  // First computation of the residual
  const auto residual = compute_residual(solver_variables);
  const auto tolerance = 1.0e-9;
#ifdef FINITE_TEMPERATURE
  const auto expected_residual = 0.000994995;
#else
  const auto expected_residual = 0.00043645437194953938;
#endif
  EXPECT_NEAR(residual, expected_residual, tolerance);

  // Write the restart file
  const auto *const written_restart_file = "/tmp/restart.nc";
  meshing::write_restart_mesh_to_nc(
      written_restart_file, solver_variables.local_mesh,
      netcdf_io::RestartData<DIMENSION>{}, solver_variables.boundary_conditions,
      *solver_variables.external_force_applicator);

  // Second read of the written restart file and comparison
  auto solver_variables_after_write =
      setup_solver_variables(test_user_input(), written_restart_file);

  const auto residual_after_write =
      compute_residual(solver_variables_after_write);
  EXPECT_NEAR(residual_after_write, expected_residual, tolerance);
}

} // namespace
