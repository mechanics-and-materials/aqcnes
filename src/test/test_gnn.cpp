// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check if a GNN based infinite model relaxation works well
 * @authors S. Saxena, M. Spinola
 */

#include "exception/qc_exception.hpp"
#include "geometry/point.hpp"
#include "lattice/lattice_cache.hpp"
#include "materials/create_material.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include "solver/infinite_model_relaxation.hpp"
#include "testing/find_path.hpp"
#include <Eigen/Core>
#include <gmock/gmock.h>
#include <memory>

namespace {

input::NNMaterialProperties nn_material_properties() {
  return input::NNMaterialProperties{
      .model_path =
          testing::io::find_path("src/test/data/gnn/Al_Mishin_v4.pth"),
      .standardization_file_path =
          testing::io::find_path("src/test/data/gnn/standardization.csv"),
      .atomic_mass = 26.981539,
      .potential_material_name = "Al_mishin"};
}

input::Userinput test_user_input() {
  return input::Userinput{
      // SimulationSettings
      .mesh_file = "./Al_4x4x4_000365.nc",
      .restart_file_name = std::nullopt,
      .output_initial_partition = false,
      .output_centro_symmetry = false,
      .node_effective_radius = 2.5,
      .minimum_barycenter_weight = 1.0,
      .verlet_buffer_factor = 1.75,
      .neighbour_update_displacement = 0.35,
      .mesh_overlap_buffer_radius = 1.5,
      .constrained_direction = std::nullopt,
      .neighbour_update_style = input::NeighbourUpdateStyle::ADAPTIVE,
      .remove_previous_neighbours = true,
      .quad_type = 3,
      // ThermalSettings
      .T_ref = 300.0,
      .T_min = 0.01,
      .T_max = 5000.0,
      .Phi_ref = 5.0,
      .isentropic = false,
      .onsager_coefficient = 15.926734,
      .heat_rate_tolerance_per_atom = 1.0e-3,
      // Potentials
      .material = nn_material_properties(),
      // ExternalIndenter
      .intialize_force_applicator = false,
      .indenter_radius = 25.0,
      .force_constant = 1000.0,
      .indenter_increment = 0.0,
      .strain_increment_time_step = 0.01,
      .indenter_center = {{0.0, 0.0, 0.0}},
      .initial_offset = -0.3,
      .external_displacement_gradient = {{{1.0, 0.0, 0.0},
                                          {0.0, 1.0, 0.0},
                                          {0.0, 0.0, 0.0}}},
      .external_iterations = 750,
      // FIRERelaxation
      .fire_time_step = 5.0,
      .fire_initial_alpha = 0.1,
      .fire_f_alpha = 0.99,
      .fire_f_increase_t = 1.01,
      .fire_f_decrease_t = 0.5,
      .fire_min_steps_reset = 5,
      .fire_steps_post_reset = 0,
      .fire_max_time_step = 11.0,
      .fire_dt_output_iter = 9999,
      .fire_max_iter = 100,
      .fire_min_residual = 0.001,
      // PETSC
      .abs_tolerance = 5e-05,
      .rel_tolerance = 5e-05,
      .s_tolerance = 5e-05,
      .max_iter = 200,
      .max_feval = 200,
      // TimeStepping
      .safety_factor = 0.75,
      .t_initial = 0.0,
      .t_final = 1.0,
      .time_step = 3.9e-7,
      .max_time_steps = 9999,
      // Periodicity
      .periodicity =
          {{meshing::PeriodicityParameters{.min_coordinate = -8.1,
                                           .length = 16.2,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -8.1,
                                           .length = 16.2,
                                           .relaxation_constraint = false},
            meshing::PeriodicityParameters{.min_coordinate = -16.2,
                                           .length = 16.2,
                                           .relaxation_constraint = false}}},
      .a_ref = 4.05,
      // Datagen for GNN Training
      .clusters_compilation_file_name =
          "./solution_files/datagen_clusters_0.dat",
      .mean_pos_std_dev = 0.015,
      .phi_std_dev = 0.2,
      .noof_samples = 280,
      .dataset_stride = 1

  };
}

auto create_test_lattice_cache() {
  return lattice::LatticeCache<3>{
      .n_sub_lattice = 1,
      .lattice_type = lattice::LatticeStructure::FCC,
      .suffix = "000365",
      .lattice_parameters = {{4.05, 4.05, 4.05}},
      .rotated_basis_matrix = {{{2.025, 0, 2.025},
                                {2.025, 2.025, 0},
                                {0, 2.025, 2.025}}},
      .seed_point_offsets = {{0.0, 0.0, 0.0}},
      .rotation = {{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}}};
}

TEST(test_gnn, works_for_infinite_relaxation) {
  const auto input = test_user_input();
  const input::NNMaterialProperties properties = nn_material_properties();
  const auto material =
      materials::create_material<3, 1>(input, create_test_lattice_cache());

  const auto original_basis = material.basis_vectors;

  const auto results = solver::relax_infinite<3, 1, 2>(
      input, material, original_basis, input.T_ref);

  const auto eps = 5.E-6;
  const auto expected_phi = 5.20373;
  EXPECT_NEAR(results.phi, expected_phi, eps);
  const auto expected_energy = -3.323028;
  EXPECT_NEAR(results.e, expected_energy, eps);
  const auto s = 2.03243;
  const auto expected_basis = std::array{std::array<double, 3>{s, 0., s},
                                         std::array<double, 3>{s, s, 0.},
                                         std::array<double, 3>{0., s, s}};
  EXPECT_THAT(results.deformed_basis,
              qcmesh::testing::DoubleMatrixNear(expected_basis, eps));
}

} // namespace
