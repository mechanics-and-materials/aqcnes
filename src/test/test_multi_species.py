#!/usr/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Relaxation workflow with multispecies."""

from pathlib import Path
from typing import Any

import numpy as np
from test_workflows import Base, main


class TestMultiSpecies(Base.TestWorkflow):
    """Multispecies test suite."""

    input_data = (
        Path("data") / "multi_species",
        Path("../../data/potentials_NIST/nickel_palladium/NiPd_Johnson.eam.alloy"),
    )
    binaries = ("mesh_generator", "Relaxation3D_FINITE_K")
    computed_data_file = "energy_volume_NiPd_300.000000.dat"
    extra_dirs = (Path("mesh_files"),)
    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            300,  # Temperature
            3411.631203,  # MeshVolume
            -931.577125,  # FreeEnergy
            -1034.715147,  # InternalEnergy
            9.927164506,  # KineticEnergy
            256,  # TotalWeight
            128,  # TotalSoluteAtoms
            15.054073,  # PeriodX
            15.054073,  # PeriodY
            15.054073,  # PeriodZ
        ]
    )


if __name__ == "__main__":
    main()
