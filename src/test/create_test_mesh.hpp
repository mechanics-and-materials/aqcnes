// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "lattice/lattice_generators/nano_indentation.hpp"
#include "materials/material.hpp"
#include "meshing/periodicity_parameters.hpp"
#include "qcmesh/triangulation/triangulation.hpp"
#include "testing/create_mesh.hpp"
#include <vector>

inline meshing::QCMesh<3, 3> create_test_mesh(
    const double phi_ref, const double t_ref,
    const std::array<std::optional<meshing::PeriodicityParameters>, 3>
        &periodic_parameters_ref,
    const lattice::NanoIndentation<3> &method_parameters,
    const lattice::LatticeGeneratorParameters<3> &config,
    const std::vector<materials::MaterialBase<3>> &materials) {
  auto atomistic_domain = qcmesh::geometry::Box<3>{};
  auto material_ids = std::vector<int>{};
  const auto lattice = lattice::generate_lattice<3>(
      method_parameters, config, material_ids, atomistic_domain);
  auto simplices = qcmesh::triangulation::triangulate(lattice);
  qcmesh::triangulation::clean_0_vol_cells_at_boundary(
      simplices, lattice, constants::geometric_tolerance);
  auto mesh = testing::create_mesh(
      testing::BareMesh<std::array<double, 3>>{lattice, simplices}, materials,
      phi_ref, t_ref);
  mesh.initialize_periodic_parameters(periodic_parameters_ref);
  mesh.initialize_periodic_offsets();
  mesh.initialize_bravais_lattice(atomistic_domain, materials);
  mesh.initialize_elements_atomistic_flag();
  mesh.atomistic_domain = atomistic_domain;

  return mesh;
}
