#!/usr/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Relaxation workflow test."""

from pathlib import Path
from typing import Any

import numpy as np
from test_workflows import Base, main

MATERIAL = "ExtendedFinnisSinclairCopper"


class TestRelaxationFiniteTemperature(Base.TestWorkflow):
    """Relaxation test suite for finite temperature."""

    input_data = (
        Path("data") / "relaxation" / "finite",
        Path("data") / "relaxation" / "MyLattice.txt",
        Path("data") / "3DBox_007813_partially_relaxed.nc",
    )
    binaries = ("Relaxation3D_FINITE_K",)
    computed_data_file = f"energy_volume_{MATERIAL}_300.000000.dat"
    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            300,  # Temperature
            81534.51742,  # MeshVolume
            -23003.7459,  # FreeEnergy
            -26073.96979,  # InternalEnergy
            302.9724074,  # KineticEnergy
            7813,  # TotalWeight
            0,  # TotalVacancies
            0,  # PeriodX
            0,  # PeriodY
            0,  # PeriodZ
        ]
    )


class TestRelaxationZeroTemperature(Base.TestWorkflow):
    """Relaxation test suite for zero temperature."""

    input_data = (
        Path("data") / "relaxation" / "zero",
        Path("data") / "relaxation" / "MyLattice.txt",
        Path("data") / "3DBox_007813.nc",
    )
    binaries = ("Relaxation3D_0K",)
    computed_data_file = f"energy_volume_{MATERIAL}_0.dat"
    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            0,  # Temperature
            80245.57123,  # MeshVolume
            -26374.61897,  # FreeEnergy
            -26374.61897,  # InternalEnergy
            0,  # KineticEnergy
            7813,  # TotalWeight
            0,  # TotalVacancies
            0,  # PeriodX
            0,  # PeriodY
            0,  # PeriodZ
        ]
    )


if __name__ == "__main__":
    main()
