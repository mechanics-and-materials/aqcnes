// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check if an infinite model relaxation works well for multiple temperatures
 * @authors S. Saxena, M. Spinola
 */

#include "lattice/lattice_cache.hpp"
#include "materials/create_material.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include "solver/infinite_model_relaxation.hpp"
#include "testing/find_path.hpp"
#include <gmock/gmock.h>
#include <memory>

namespace {

input::SetflEAMProperties setfleam_material_properties() {
  return input::SetflEAMProperties{
      .potential_file_path = testing::io::find_path(
          "data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
      .potential_material_name = "MendelevCopper",
      .funcfl_file = false};
}

input::Userinput test_user_input() {
  return input::Userinput{
      // SolverTolerances
      .max_force_per_atom = 1.0e-6,
      .max_thermal_force_per_atom = 1.0e-6,
      .max_periodic_force_residue_per_atom = 1.0e-6,
      // SimulationSettings
      .mesh_file = "",
      .restart_file_name = std::nullopt,
      .output_initial_partition = true,
      .output_centro_symmetry = true,
      .node_effective_radius = 2.5,
      .minimum_barycenter_weight = 1.0,
      .verlet_buffer_factor = 1.75,
      .neighbour_update_displacement = 0.05,
      .mesh_overlap_buffer_radius = 1.5,
      .constrained_direction = 1,
      .neighbour_update_style = input::NeighbourUpdateStyle::ADAPTIVE,
      .remove_previous_neighbours = true,
      .quad_type = 3,
      // ThermalSettings
      .T_ref = 100.0,
      .T_min = 0.01,
      .T_max = 5000.0,
      .Phi_ref = 5.545,
      .isentropic = false,
      .onsager_coefficient = 15.926734,
      .heat_rate_tolerance_per_atom = 1.0e-3,
      // Potentials
      .material = setfleam_material_properties(),
      // ExternalIndenter
      .intialize_force_applicator = false,
      .indenter_radius = 25.0,
      .force_constant = 1000.0,
      .indenter_increment = 0.0,
      .strain_increment_time_step = 0.01,
      .indenter_center = {{0.0, 0.0, 0.0}},
      .initial_offset = -0.3,
      .external_displacement_gradient = {{{1.0, 0.0, 0.0},
                                          {0.0, 1.0, 0.0},
                                          {0.0, 0.0, 1.0}}},
      .external_iterations = 40,
      // FIRERelaxation
      .fire_time_step = 5.0,
      .fire_initial_alpha = 0.1,
      .fire_f_alpha = 0.99,
      .fire_f_increase_t = 1.01,
      .fire_f_decrease_t = 0.5,
      .fire_min_steps_reset = 5,
      .fire_steps_post_reset = 0,
      .fire_max_time_step = 11.0,
      .fire_dt_output_iter = 9999,
      .fire_max_iter = 5000,
      .fire_min_residual = 0.001,
      // PETSC
      .abs_tolerance = 5e-05,
      .rel_tolerance = 5e-05,
      .s_tolerance = 5e-05,
      .max_iter = 200,
      .max_feval = 200,
      // TimeStepping
      .safety_factor = 0.75,
      .t_initial = 0.0,
      .t_final = 1.0,
      .time_step = 3.9e-7,
      .max_time_steps = 9999,
      // Periodicity
      .periodicity = {{std::nullopt, std::nullopt, std::nullopt}},
      .a_ref = 3.615,
  };
}

struct ParameterSet {
  double t;
  double expected_phi;
  double expected_energy;
  std::array<std::array<double, 3>, 3> expected_basis;
};
struct InfiniteModelRelaxationTest : ::testing::TestWithParam<ParameterSet> {};

auto create_test_lattice_cache() {
  return lattice::LatticeCache<3>{
      .n_sub_lattice = 1,
      .lattice_type = lattice::LatticeStructure::FCC,
      .suffix = "000365",
      .lattice_parameters = {{3.615, 3.615, 3.615}},
      .rotated_basis_matrix = {{{1.8075, 0, 1.8075},
                                {1.8075, 1.8075, 0},
                                {0, 1.8075, 1.8075}}},
      .seed_point_offsets = {{0.0, 0.0, 0.0}},
      .rotation = {{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}}};
}

TEST_P(InfiniteModelRelaxationTest, produces_correct_phi_and_lattice_vectors) {
  const auto parameter_set = GetParam();
  const auto input = test_user_input();
  const input::SetflEAMProperties properties = setfleam_material_properties();
  const auto material =
      materials::create_material<3, 1>(input, create_test_lattice_cache());
  const auto results = solver::relax_infinite<3, 1, 2>(
      input, material, material.basis_vectors, parameter_set.t);
  const auto eps = 1.E-5;
  EXPECT_NEAR(results.phi, parameter_set.expected_phi, eps);
  EXPECT_NEAR(results.e, parameter_set.expected_energy, eps);
  EXPECT_THAT(results.deformed_basis, qcmesh::testing::DoubleMatrixNear(
                                          parameter_set.expected_basis, eps));
}
std::array<std::array<double, 3>, 3> m(const double x) {
  return {{{x, 0., x}, {x, x, 0.}, {0., x, x}}};
}
INSTANTIATE_TEST_CASE_P(
    InfiniteModelRelaxation, InfiniteModelRelaxationTest,
    testing::Values(ParameterSet{.t = 0.,
                                 .expected_phi = 0.,
                                 .expected_energy = -3.540000,
                                 .expected_basis = m(1.8075)},
                    ParameterSet{.t = 100.,
                                 .expected_phi = 6.86577,
                                 .expected_energy = -3.5277718,
                                 .expected_basis = m(1.8104)},
                    ParameterSet{.t = 200.,
                                 .expected_phi = 6.24072,
                                 .expected_energy = -3.5161248,
                                 .expected_basis = m(1.81312)},
                    ParameterSet{.t = 300.,
                                 .expected_phi = 5.87442,
                                 .expected_energy = -3.5044387154353949,
                                 .expected_basis = m(1.81574)},
                    ParameterSet{.t = 400.,
                                 .expected_phi = 5.60682,
                                 .expected_energy = -3.4924049114535078,
                                 .expected_basis = m(1.81828)},
                    ParameterSet{.t = 500.,
                                 .expected_phi = 5.38971,
                                 .expected_energy = -3.4798189974377367,
                                 .expected_basis = m(1.82081)}),
    [](const testing::TestParamInfo<InfiniteModelRelaxationTest::ParamType>
           &info) {
      return std::string{"T_"} + std::to_string(int(info.param.t));
    });

} // namespace
