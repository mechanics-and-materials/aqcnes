// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test mesh relaxation for test_kim
 * @authors M. Spinola
 */

#pragma once

#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"

template <std::size_t Dimension, std::size_t Nidof, std::size_t Noi>
meshing::QCMesh<Dimension, Dimension, Nidof,
                meshing::NodalDataMultispecies<Noi>> &
do_relaxation(
    solver::SolverVariables<Dimension, Noi> &solver_variables,
    const meshing::QCMesh<Dimension, Dimension, Nidof,
                          meshing::NodalDataMultispecies<Noi>> &mesh) {

  solver_variables.local_mesh = mesh;

  const auto periodic_flags = [](const auto &periodic_parameters) {
    std::array<bool, 3> periodic_flags{};
    for (std::size_t i = 0; i < periodic_parameters.size(); i++)
      periodic_flags[i] = bool(periodic_parameters[i]);
    return periodic_flags;
  };

  solver_variables.local_mesh.repatoms.box_displacement_gradient =
      repatoms::diag_displacement_gradient(
          periodic_flags(solver_variables.input.periodicity));

  meshing::redistribute(solver_variables.local_mesh);
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<Dimension>(
          solver_variables.input, 0);
  // initial repair after triangulation
  meshing::repair_mesh(solver_variables.local_mesh);
  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  // update boundaries
  solver_variables.local_mesh.local_domain_bound =
      qcmesh::geometry::bounding_box(
          solver_variables.local_mesh.repatoms.locations);
  solver_variables.local_mesh.domain_bound = geometry::distributed_box_hull(
      solver_variables.local_mesh.local_domain_bound);
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          solver_variables.input.boundary_types,
          solver_variables.local_mesh.domain_bound);
  solver_variables.apply_external_force = false;
  solver_variables.apply_boundary_conditions = false;
  solver_variables.isentropic = false;

  solver::Solver::set_solver_dofs<Dimension, Nidof>(solver_variables);

  solver::Solver::create_solution_vectors<Dimension>(solver_variables);

  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<Noi>,
                                      Dimension, Nidof>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      solver_variables.input.max_iter, solver_variables.input.max_feval,
      solver_variables);

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<Noi>, Dimension, Nidof>(solver_variables);

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  solver::Solver::composite_relax<
      solver::NullDump, meshing::NodalDataMultispecies<Noi>, Dimension, Nidof>(
      solver_variables.input.a_ref,
      solver_variables.input.max_thermal_force_per_atom,
      solver_variables.input.max_force_per_atom, solver_variables,
      solver::NullDump{});
  solver::Solver::update_sampling_atom_energies<
      meshing::NodalDataMultispecies<Noi>, Dimension, Nidof>(solver_variables);
  solver::Solver::destroy_snes<Dimension>(solver_variables);
  return solver_variables.local_mesh;
}
