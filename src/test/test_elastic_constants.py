#!/usr/bin/env python3

# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

"""Elastic constants workflow test."""

from pathlib import Path
from typing import Any

import numpy as np
from test_workflows import Base, main


class TestElasticConstantsFiniteTemperature(Base.TestWorkflow):
    """ElasticConstants test suite for finite temperature."""

    input_data = (
        Path("data") / "elastic_constants" / "finite",
        Path("../../data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
    )
    extra_dirs = (Path("mesh_files"),)
    binaries = ("mesh_generator", "ElasticConstants_FINITE_K")
    computed_data_file = "energy_volume_MishinCopper_300.000000.dat"
    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            [
                0,
                300,
                3064.98010431858,
                -792.606074974245,
                -897.137057885337,
                9.92716450559999,
                256,
                14.5258833835479,
                14.5258833835479,
                14.5258833835479,
            ],
            [
                0.0001,
                300,
                3065.89969030233,
                -792.605952988757,
                -897.136935899849,
                9.92716450559999,
                256,
                14.5273359718863,
                14.5273359718863,
                14.5273359718863,
            ],
            [
                0.0002,
                300,
                3066.81946020329,
                -792.605596444979,
                -897.131899839536,
                9.92716450559999,
                256,
                14.5287885602246,
                14.5287885602246,
                14.5287885602246,
            ],
            [
                0.0003,
                300,
                3067.73941403984,
                -792.605014247964,
                -897.121767686325,
                9.92716450559999,
                256,
                14.530241148563,
                14.530241148563,
                14.530241148563,
            ],
            [
                0.0004,
                300,
                3068.65955183036,
                -792.604199581709,
                -897.111277244968,
                9.92716450559999,
                256,
                14.5316937369013,
                14.5316937369013,
                14.5316937369013,
            ],
            [
                0.0005,
                300,
                3069.57987359325,
                -792.603152460348,
                -897.100475923399,
                9.92716450559999,
                256,
                14.5331463252397,
                14.5331463252397,
                14.5331463252397,
            ],
        ]
    )


class TestElasticConstantsZeroTemperature(Base.TestWorkflow):
    """ElasticConstants test suite for zero temperature."""

    input_data = (
        Path("data") / "elastic_constants" / "zero",
        Path("../../data/potentials_NIST/copper/mishin/Cu.eam.alloy"),
    )
    extra_dirs = (Path("mesh_files"),)
    binaries = ("mesh_generator", "ElasticConstants_0K")
    computed_data_file = "energy_volume_MishinCopper_0.dat"
    reference_data: np.ndarray[Any, np.dtype[np.float64]] = np.array(
        [
            [
                0,
                0,
                3023.46481349523,
                -906.23999190198,
                -906.23999190198,
                0,
                256,
                14.4600004423822,
                14.4600004423822,
                14.4600004423822,
            ],
            [
                0.0001,
                0,
                3024.37194364624,
                -906.239874397544,
                -906.239874397544,
                0,
                256,
                14.4614464424265,
                14.4614464424265,
                14.4614464424265,
            ],
            [
                0.0002,
                0,
                3025.27925522329,
                -906.239522011844,
                -906.239522011844,
                0,
                256,
                14.4628924424707,
                14.4628924424707,
                14.4628924424707,
            ],
            [
                0.0003,
                0,
                3026.1867482445,
                -906.238934797729,
                -906.238934797729,
                0,
                256,
                14.4643384425149,
                14.4643384425149,
                14.4643384425149,
            ],
            [
                0.0004,
                0,
                3027.09442272804,
                -906.238112800478,
                -906.238112800478,
                0,
                256,
                14.4657844425592,
                14.4657844425592,
                14.4657844425592,
            ],
            [
                0.0005,
                0,
                3028.00227869201,
                -906.237056074155,
                -906.237056074155,
                0,
                256,
                14.4672304426034,
                14.4672304426034,
                14.4672304426034,
            ],
        ]
    )


if __name__ == "__main__":
    main()
