// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "lattice/lattice_generator_node.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

/**
 * @brief Provides serialization of qcmesh::mesh::SimplexCell.
 */
template <typename Archive>
void serialize(Archive &ar, lattice::LatticeGeneratorNodalData &data,
               const unsigned int) {
  ar &data.material_index;
  ar &data.type;
}
} // namespace boost::serialization

/**
 * See https://www.boost.org/doc/libs/1_82_0/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
BOOST_CLASS_IMPLEMENTATION(lattice::LatticeGeneratorNodalData,
                           object_serializable)
BOOST_CLASS_TRACKING(lattice::LatticeGeneratorNodalData, track_never)
BOOST_IS_MPI_DATATYPE(lattice::LatticeGeneratorNodalData)
