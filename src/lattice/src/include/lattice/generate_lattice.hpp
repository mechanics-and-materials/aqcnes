// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief high level lattice generation function
 * @authors P. Gupta, S. Saxena, G. Bräunlich
 */

#include "exception/qc_exception.hpp"
#include "lattice/lattice_generator_node.hpp"
#include "lattice/lattice_generators/nano_indentation.hpp"
#include "lattice/lattice_generators/read_coordinates.hpp"
#include "lattice/lattice_generators/symmetric_gb.hpp"

namespace lattice {

template <std::size_t Dimension>
using LatticeGenerator =
    std::function<void(std::vector<std::array<double, Dimension>> &,
                       std::vector<int> &, std::vector<int> &,
                       qcmesh::geometry::Box<Dimension> &, std::string &,
                       qcmesh::geometry::Box<Dimension> &, bool &)>;

using nano_indentation::generate_lattice_from_method;
using read_coordinates::generate_lattice_from_method;
using symmetric_gb::generate_lattice_from_method;

/**
 * @brief Data needed to build a mesh after generating the lattice.
 */
template <std::size_t Dimension> struct LatticeData {
  std::vector<LatticeGeneratorNode<Dimension>> lattice_nodes{};
  qcmesh::geometry::Box<Dimension> domain_bounds;
  qcmesh::geometry::Box<Dimension> atomistic_domain;
};

/**
 * @brief Calls lattice generator depending on chosen method in json file.
 */
template <std::size_t Dimension>
LatticeData<Dimension>
generate_lattice(const LatticeGeneratorParameters<Dimension> &config) {
  auto data = LatticeData<Dimension>{};
  auto lattice_points = std::vector<std::array<double, Dimension>>{};
  auto material_ids = std::vector<int>{};
  auto lattice_site_types = std::vector<int>{};
  std::visit(
      [&](const auto &method) {
        generate_lattice_from_method<Dimension>(
            method, config, lattice_points, material_ids, lattice_site_types,
            data.domain_bounds, data.atomistic_domain);
      },
      config.method);
  if (lattice_points.size() != material_ids.size() ||
      lattice_points.size() != lattice_site_types.size())
    throw exception::QCException{
        "Size mismatch between lattice_points (" +
        std::to_string(lattice_points.size()) +
        "), material_ids, lattice_site_types: (" +
        std::to_string(material_ids.size()) + "), lattice_site_types (" +
        std::to_string(lattice_site_types.size()) + ")"};
  data.lattice_nodes.reserve(lattice_points.size());
  for (std::size_t i = 0; i < lattice_points.size(); i++)
    data.lattice_nodes.emplace_back(LatticeGeneratorNode<Dimension>{
        lattice_points[i],
        LatticeGeneratorNodalData{static_cast<std::size_t>(material_ids[i]),
                                  lattice_site_types[i]}});
  return data;
}

} // namespace lattice
