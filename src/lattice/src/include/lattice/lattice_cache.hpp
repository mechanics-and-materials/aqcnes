// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file 
 * @brief save and load lattice cache
 * @authors S. Saxena, S. Zimmermann
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include "utils/string_split.hpp"
#include <filesystem>
#include <fstream>
#include <string>

namespace lattice {

template <std::size_t Dimension> struct LatticeCache {
  std::size_t n_sub_lattice{};
  LatticeStructure lattice_type = LatticeStructure::FCC;
  std::string suffix;
  std::array<double, Dimension> lattice_parameters;
  std::array<std::array<double, Dimension>, Dimension> rotated_basis_matrix;
  std::vector<std::array<double, Dimension>> seed_point_offsets;
  std::array<std::array<double, Dimension>, Dimension> rotation;
};

template <std::size_t Dimension>
LatticeCache<Dimension> load_lattice_cache(std::istream &stream) {
  auto line = std::string{};
  std::getline(stream, line);
  auto cache = LatticeCache<Dimension>{};

  const auto [head, tail] = utils::string_split_once(line, ' ');
  cache.n_sub_lattice =
      utils::parse_str<std::size_t>(utils::string_strip(head, ' '));
  std::getline(stream, line);

  // Read Lattice structure and number of seed points
  std::getline(stream, line);
  const auto [lattice_type, n_seed_points, suffix] =
      utils::string_split<std::string, int, std::string>(line, '\t');
  cache.lattice_type = deserialize_lattice_structure(lattice_type);
  cache.suffix = suffix;

  // Read cell dimensions vector
  std::getline(stream, line);
  std::getline(stream, line);
  cache.lattice_parameters =
      utils::string_split<double, Dimension>(line, ' ', ' ');

  // Read initial basis vectors
  std::getline(stream, line);
  for (std::size_t i = 0; i < Dimension; i++) {
    std::getline(stream, line);
    cache.rotated_basis_matrix[i] =
        utils::string_split<double, Dimension>(line, ' ', ' ');
  }

  // Read Offset Coefficients for seed points
  std::getline(stream, line);
  for (std::size_t i = 0; i < static_cast<std::size_t>(n_seed_points); i++) {
    std::getline(stream, line);
    cache.seed_point_offsets.push_back(
        utils::string_split<double, Dimension>(line, ' ', ' '));
  }

  // Read initial rotation matrix
  std::getline(stream, line);
  for (std::size_t i = 0; i < Dimension; i++) {
    std::getline(stream, line);
    cache.rotation[i] = utils::string_split<double, Dimension>(line, ' ', ' ');
  }
  return cache;
}
template <std::size_t Dimension>
LatticeCache<Dimension> load_lattice_cache(const std::string &path) {
  auto file = std::ifstream(path.c_str());
  if (!file.is_open())
    throw exception::QCException{std::string("Could not read file ") + path};
  auto cache = load_lattice_cache<Dimension>(file);
  file.close();
  return cache;
}
template <std::size_t Dimension>
void save_lattice_cache(std::ostream &stream,
                        const LatticeCache<Dimension> &cache) {
  stream << cache.n_sub_lattice << std::endl << std::endl;

  stream << serialize_lattice_structure(cache.lattice_type) << '\t'
         << cache.seed_point_offsets.size() << '\t' << cache.suffix << std::endl
         << std::endl;

  for (std::size_t i = 0; i < Dimension; i++)
    stream << cache.lattice_parameters[i] << " ";

  stream << std::endl << std::endl;

  for (const auto &row : cache.rotated_basis_matrix) {
    for (const auto val : row)
      stream << val << " ";
    stream << std::endl;
  }
  stream << std::endl;

  for (const auto &coefs : cache.seed_point_offsets) {
    for (const auto val : coefs)
      stream << val << " ";
    stream << std::endl;
  }
  stream << std::endl;

  for (const auto &row : cache.rotation) {
    for (const auto val : row)
      stream << val << " ";
    stream << std::endl;
  }
  stream << std::endl;
}

template <std::size_t Dimension>
void save_lattice_cache(const std::filesystem::path &path,
                        const LatticeCache<Dimension> &cache) {
  std::filesystem::create_directories(path.parent_path());
  std::ofstream f_lattice;
  f_lattice.open(path, std::ios::app);
  save_lattice_cache(f_lattice, cache);
  f_lattice.close();
}

} // namespace lattice
