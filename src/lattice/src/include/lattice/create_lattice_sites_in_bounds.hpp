// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file 
 * @brief function implementation to create lattice sites in a cubic box
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/octree.hpp"
#include <vector>

namespace array_ops = qcmesh::array_ops;

namespace lattice {

namespace detail {
/**
 * @brief Adds an item to the end of a stl container if it is not already contained.
 */
template <class T>
typename std::vector<T>::iterator
add_to_container_unique(std::vector<T> &container, T item) {
  const auto iter = std::find(container.begin(), container.end(), item);
  if (iter == container.end()) {
    container.push_back(item);
    return container.end() - 1;
  }

  return container.end();
}
} // namespace detail

template <std::size_t N>
std::size_t get_number_of_lattice_sites(
    const std::array<std::array<double, N>, N> &bravais_vectors,
    const double rcut) {

  auto min_length = rcut;
  for (const auto &vec : bravais_vectors) {
    auto vec_length = array_ops::norm(vec);
    if (vec_length < min_length)
      min_length = vec_length;
  }
  return static_cast<std::size_t>(std::round(rcut / min_length) + 1);
}

inline void create_lattice_sites_in_bounds(
    qcmesh::geometry::Box<2> &box, const qcmesh::geometry::Box<2> &lower_box,
    const std::vector<std::array<double, 2>> &seed_points,
    const std::array<std::array<double, 2>, 2> &vectors,
    std::vector<std::array<double, 2>> &lattice_sites,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types) {
  double r_cut = array_ops::distance(box.upper, box.lower);

  std::array<std::size_t, 2> number_of_lattice_sites{};
  number_of_lattice_sites.fill(get_number_of_lattice_sites(vectors, r_cut));
  std::array<double, 2> px = {{vectors[0][0], vectors[0][1]}};
  std::array<double, 2> py = {{vectors[1][0], vectors[1][1]}};

  std::vector<std::array<double, 2>> box_sites;
  auto new_lower = std::array<double, 2>{};
  auto new_upper = std::array<double, 2>{};

  array_ops::as_eigen(new_lower) =
      (array_ops::as_eigen(box.lower) + array_ops::as_eigen(box.upper)) * 0.5;
  array_ops::as_eigen(new_upper) =
      (array_ops::as_eigen(box.lower) + array_ops::as_eigen(box.upper)) * 0.5;
  auto lattice_site = std::array<double, 2>{};

  for (std::size_t s_ctr = 0; s_ctr < seed_points.size(); s_ctr++) {
    for (int i = -static_cast<int>(number_of_lattice_sites[0]);
         i < static_cast<int>(number_of_lattice_sites[0]); i++) {
      for (int j = -static_cast<int>(number_of_lattice_sites[1]);
           j < static_cast<int>(number_of_lattice_sites[1]); j++) {
        array_ops::as_eigen(lattice_site) =
            array_ops::as_eigen(seed_points[s_ctr]) +
            array_ops::as_eigen(px) * i + array_ops::as_eigen(py) * j;

        if (qcmesh::geometry::contains(box, lattice_site) &&
            !qcmesh::geometry::contains(lower_box, lattice_site)) {
          const bool added_in = detail::add_to_container_unique(
                                    box_sites, lattice_site) != box_sites.end();

          if (added_in) {
            if (lattice_site <= new_lower)
              new_lower = lattice_site;

            if (lattice_site >= new_upper)
              new_upper = lattice_site;
          }
          lattice_site_types.push_back(static_cast<int>(s_ctr));
        }
      }
    }
  }

  box.lower = new_lower;
  box.upper = new_upper;

  for (std::array<double, 2> lattice_site : box_sites) {
    bool added_in = detail::add_to_container_unique(
                        lattice_sites, lattice_site) != lattice_sites.end();

    if (added_in) {
      material_ids.push_back(0);
    }
  }
}

inline void create_lattice_sites_in_bounds(
    qcmesh::geometry::Box<3> &box, const qcmesh::geometry::Box<3> &lower_box,
    const std::vector<std::array<double, 3>> &seed_points,
    const std::array<std::array<double, 3>, 3> &vectors,
    std::vector<std::array<double, 3>> &lattice_sites,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types) {

  double r_cut = array_ops::distance(box.upper, box.lower);

  std::array<std::size_t, 3> number_of_lattice_sites{};
  number_of_lattice_sites.fill(get_number_of_lattice_sites(vectors, r_cut));

  std::array<double, 3> px = {{vectors[0][0], vectors[0][1], vectors[0][2]}};
  std::array<double, 3> py = {{vectors[1][0], vectors[1][1], vectors[1][2]}};
  std::array<double, 3> pz = {{vectors[2][0], vectors[2][1], vectors[2][2]}};

  std::array<double, 3> lattice_site{};

  std::vector<int> site_type;
  site_type.clear();

  const int n = 2;

  constexpr double TOLERANCE = 0.1;
  auto unique_container =
      qcmesh::geometry::octree::Octree<std::array<double, 3>>(box);

  for (std::size_t s_ctr = 0; s_ctr < seed_points.size(); s_ctr++) {
    for (int i = -n * static_cast<int>(number_of_lattice_sites[0]);
         i <= static_cast<int>(n * number_of_lattice_sites[0]); i++) {
      for (int j = -n * static_cast<int>(number_of_lattice_sites[1]);
           j <= static_cast<int>(n * number_of_lattice_sites[1]); j++) {
        for (int k = -n * static_cast<int>(number_of_lattice_sites[2]);
             k <= static_cast<int>(n * number_of_lattice_sites[2]); k++) {
          array_ops::as_eigen(lattice_site) =
              array_ops::as_eigen(seed_points[s_ctr]) +
              array_ops::as_eigen(px) * i + array_ops::as_eigen(py) * j +
              array_ops::as_eigen(pz) * k;

          if (qcmesh::geometry::contains(box, lattice_site) &&
              !qcmesh::geometry::contains(lower_box, lattice_site)) {
            if (unique_container.insert_unique(lattice_site, TOLERANCE)) {
              lattice_sites.push_back(lattice_site);
              site_type.push_back(static_cast<int>(s_ctr));
            }
          }
        }
      }
    }
  }

  lattice_site_types.reserve(lattice_site_types.size() +
                             unique_container.size());

  lattice_sites.reserve(lattice_sites.size() + unique_container.size());

  material_ids.reserve(material_ids.size() + unique_container.size());

  lattice_site_types.insert(lattice_site_types.end(), site_type.begin(),
                            site_type.end());

  for (std::size_t ctr = 0; ctr < unique_container.size(); ctr++) {
    material_ids.push_back(0);
  }

  box = qcmesh::geometry::bounding_box(lattice_sites);
}

} // namespace lattice
