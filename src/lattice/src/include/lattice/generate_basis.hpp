// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief function implementation to get required basis for the lattice type
 * @author S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <array>

namespace lattice {

inline void generate_basis(const std::array<double, 3> &lattice_parameter,
                           const LatticeStructure lattice_structure,
                           std::array<std::array<double, 3>, 3> &basis) {
  switch (lattice_structure) {
  case LatticeStructure::FCC:
    basis[0][0] = lattice_parameter[0] / 2;
    basis[0][1] = 0.0;
    basis[0][2] = lattice_parameter[0] / 2;

    basis[1][0] = lattice_parameter[1] / 2;
    basis[1][1] = lattice_parameter[1] / 2;
    basis[1][2] = 0.0;

    basis[2][0] = 0.0;
    basis[2][1] = lattice_parameter[2] / 2;
    basis[2][2] = lattice_parameter[2] / 2;
    break;
  case LatticeStructure::BCC:
    basis[0][0] = lattice_parameter[0] / 2;
    basis[0][1] = -lattice_parameter[0] / 2;
    basis[0][2] = lattice_parameter[0] / 2;

    basis[1][0] = lattice_parameter[1] / 2;
    basis[1][1] = lattice_parameter[1] / 2;
    basis[1][2] = -lattice_parameter[1] / 2;

    basis[2][0] = -lattice_parameter[2] / 2;
    basis[2][1] = lattice_parameter[2] / 2;
    basis[2][2] = lattice_parameter[2] / 2;
    break;
  case LatticeStructure::SimpleCubic:
    basis[0][0] = lattice_parameter[0];
    basis[0][1] = 0.0;
    basis[0][2] = 0.0;
    basis[1][0] = 0.0;
    basis[1][1] = lattice_parameter[1];
    basis[1][2] = 0.0;
    basis[2][0] = 0.0;
    basis[2][1] = 0.0;
    basis[2][2] = lattice_parameter[2];
    break;
  case LatticeStructure::Diamond:
    basis[0][0] = lattice_parameter[0] / 2;
    basis[0][1] = 0.0;
    basis[0][2] = lattice_parameter[0] / 2;

    basis[1][0] = lattice_parameter[1] / 2;
    basis[1][1] = lattice_parameter[1] / 2;
    basis[1][2] = 0.0;

    basis[2][0] = 0.0;
    basis[2][1] = lattice_parameter[2] / 2;
    basis[2][2] = lattice_parameter[2] / 2;
    break;
  case LatticeStructure::HCP:
    basis[0][0] = lattice_parameter[0] / 2;
    basis[0][1] = lattice_parameter[0] / 2;
    basis[0][2] = 0.0;

    basis[1][0] = -lattice_parameter[1] * sqrt(3) / 2;
    basis[1][1] = lattice_parameter[1] * sqrt(3) / 2;
    basis[1][2] = 0.0;

    basis[2][0] = 0.0;
    basis[2][1] = 0.0;
    basis[2][2] = lattice_parameter[2];
    break;
  default:
    throw exception::QCException{
        std::string{"Unrecognized lattice structure: '"} +
        std::string{serialize_lattice_structure(lattice_structure)} +
        "'. Use BCC, FCC, HCP, Diamond or SimpleCubic."};
  }
}
inline void generate_basis(const std::array<double, 2> &lattice_parameter,
                           const LatticeStructure lattice_structure,
                           std::array<std::array<double, 2>, 2> &basis) {
  switch (lattice_structure) {
  case LatticeStructure::SimpleCubic:
    basis[0][0] = lattice_parameter[0];
    basis[0][1] = 0.0;
    basis[1][0] = 0.0;
    basis[1][1] = lattice_parameter[1];
    break;
  case LatticeStructure::FCC:
  case LatticeStructure::BCC:
  case LatticeStructure::Diamond:
  case LatticeStructure::HCP:
    throw exception::QCException{
        "Cannot do '" +
        std::string{serialize_lattice_structure(lattice_structure)} +
        "' in 2d."};
  default:
    throw exception::QCException{
        std::string{"Unrecognized lattice structure '"} +
        std::string{serialize_lattice_structure(lattice_structure)} +
        "'. Use BCC, FCC, HCP, Diamond or SimpleCubic."};
  }
}

} // namespace lattice
