// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief struct to hold lattice node information
 * @authors S. Zimmerman
 */

#pragma once

#include <array>

namespace lattice {

/**
 * @brief Nodal data type for @ref LatticeGeneratorNode.
 */
struct LatticeGeneratorNodalData {
  std::size_t material_index{};
  int type{};
};

/**
 * @brief Node type containing position, material index and type.
 */
template <std::size_t Dimension> struct LatticeGeneratorNode {
  std::array<double, Dimension> position{};
  LatticeGeneratorNodalData data{};
};

} // namespace lattice
