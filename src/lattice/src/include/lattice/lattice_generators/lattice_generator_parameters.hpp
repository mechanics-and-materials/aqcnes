// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief Struct to hold lattice generation user input parameters
 * @authors P.Gupta, S.Saxena, M. Spinola, S. Zimmerman
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include "qcmesh/geometry/box.hpp"
#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string_view>
#include <variant>
#include <vector>

namespace lattice {

template <std::size_t Dimension>
using Domain = qcmesh::geometry::Box<Dimension>;
constexpr Domain<3> DEFAULT_DOMAIN = {{-500.0, -300.0, -250.0},
                                      {500.0, 300.0, 250.0}};

/** 
 * @defgroup Different methods used for mesh generation. 
 * The structs are used to determine the executed method and
 *  contain the additional, method-specific parameters.
 *
 *  @{
 */

template <std::size_t Dimension> struct NanoIndentation {
  Domain<Dimension> domain_boundaries = DEFAULT_DOMAIN;
};

template <std::size_t Dimension> struct SymmetricGrainboundaries {
  Domain<Dimension> domain_boundaries = DEFAULT_DOMAIN;
  double overlap_deletion_distance = 1.2;
  bool delete_across_boundary = false;
};

struct ReadCoordinates {
  std::string coordinates_file_name{};
};

/** @} */

/**
 * @brief Parameter configuration used for lattice generation with
 * method-specific parameters stored in std::variant method.
 */

template <std::size_t Dimension> struct LatticeGeneratorParameters {

  /** 
   * @defgroup General parameters
   *
   *  @{
   */
  std::size_t n_sub_lattice = 1;
  std::size_t n_coarsening_levels = 2;
  std::array<double, Dimension> atomistic_domain = {{7.3, 7.3, 7.3}};

  std::array<double, Dimension> center = {{0.0, 0.0, 0.0}};
  bool save_lattice_vectors = false;

  double lattice_parameter = 3.615;
  LatticeStructure lattice_structure =
      LatticeStructure::FCC; //FCC BCC HCP Diamond SimpleCubic
  std::string name = "mesh_data";

  std::vector<double> coarsening_factors = {{1.0, 4.0}};
  std::vector<double> domain_expansion_factors = {{15., 40.}};
  double hcp_c_by_a = 1.632;

  bool make_atomistic_domain = true;

  std::filesystem::path lattice_file_name = "lattice_data";
  std::array<std::array<double, Dimension>, Dimension> rotation{};
  std::vector<std::array<double, Dimension>> seed_point_offset_coeffs{};
  /** @} */

  /**
  * @brief Lattice generator method with specific parameters.
  */
  std::variant<SymmetricGrainboundaries<Dimension>, NanoIndentation<Dimension>,
               ReadCoordinates>
      method = NanoIndentation<Dimension>{};
};

/**
 * @brief Load from a stream.
 */
template <std::size_t Dimension>
LatticeGeneratorParameters<Dimension>
load_lattice_parameters_from_json(std::istream &stream);

extern template LatticeGeneratorParameters<3>
load_lattice_parameters_from_json(std::istream &stream);

/**
 * @brief Load from a file specified by path.
 */
template <std::size_t Dimension>
LatticeGeneratorParameters<Dimension>
load_lattice_parameters_from_json(const std::filesystem::path &path);

extern template LatticeGeneratorParameters<3>
load_lattice_parameters_from_json(const std::filesystem::path &path);

/**
 * @brief Save to a stream.
 */
template <std::size_t Dimension>
void save_lattice_parameters_to_json(
    const LatticeGeneratorParameters<Dimension> &config, std::ostream &stream);

extern template void
save_lattice_parameters_to_json(const LatticeGeneratorParameters<3> &config,
                                std::ostream &stream);

/**
 * @brief Load latticegenerator_parameters.json from the current working directory.
 */
template <std::size_t Dimension>
LatticeGeneratorParameters<Dimension>
load_lattice_parameters_from_working_dir() {
  try {
    return load_lattice_parameters_from_json<Dimension>(
        "./latticegenerator_parameters.json");
  } catch (const std::ifstream::failure &) {
    throw exception::QCException{"latticegenerator_parameters.json not found "
                                 "in current working directory "};
  }
}

} // namespace lattice
