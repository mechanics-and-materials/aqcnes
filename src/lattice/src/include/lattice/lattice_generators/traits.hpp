// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @authors S. Zimmermann, G. Bräunlich
 */

#pragma once

#include <variant>

namespace lattice::traits {

/**
 * @brief Trait for lattice generators containing the information
 * if the lattice generator is able to generate lattice vectors.
 */
template <class T> struct CanSaveLatticeVectors;

/**
 * @brief Evaluates the trait for given instance.
 */
template <class T> bool can_save_lattice_vectors(const T &) {
  return CanSaveLatticeVectors<T>::value;
}
template <typename... V>
bool can_save_lattice_vectors(const std::variant<V...> &variant) {
  return std::visit(
      [](const auto &alternative) {
        return lattice::traits::can_save_lattice_vectors(alternative);
      },
      variant);
}

} // namespace lattice::traits
