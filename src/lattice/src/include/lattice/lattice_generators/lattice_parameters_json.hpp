// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief json serialization for lattice generation user input parameters
 * @authors S. Zimmerman
 */

#include "lattice/lattice_generators/lattice_generator_parameters.hpp"
#include "utils/json/nlohmann_define_type_non_intrusive_with_default.hpp"
#include "utils/json/variant_json_serialization.hpp"
#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <string_view>
#include <vector>

// std::stringview <-> Variant type mapping
template <>
struct utils::json::VariantName<lattice::SymmetricGrainboundaries<3>> {
  static constexpr std::string_view NAME =
      std::string_view{"SymmetricGrainboundaries"};
};

template <> struct utils::json::VariantName<lattice::ReadCoordinates> {
  static constexpr std::string_view NAME = std::string_view{"ReadCoordinates"};
};

template <> struct utils::json::VariantName<lattice::NanoIndentation<3>> {
  static constexpr std::string_view NAME = std::string_view{"NanoIndentation"};
};

namespace qcmesh::geometry {
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Box<3>, upper, lower)
} // namespace qcmesh::geometry

namespace lattice {

NLOHMANN_JSON_SERIALIZE_ENUM(LatticeStructure,
                             {
                                 {LatticeStructure::FCC, "FCC"},
                                 {LatticeStructure::BCC, "BCC"},
                                 {LatticeStructure::HCP, "HCP"},
                                 {LatticeStructure::Diamond, "Diamond"},
                                 {LatticeStructure::SimpleCubic, "SimpleCubic"},
                                 {LatticeStructure::None, "None"},
                             })

using Json = nlohmann::json;
using utils::json::from_json;
using utils::json::to_json;

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(NanoIndentation<3>,
                                                domain_boundaries)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(SymmetricGrainboundaries<3>,
                                                domain_boundaries,
                                                overlap_deletion_distance,
                                                delete_across_boundary)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ReadCoordinates,
                                                coordinates_file_name)

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(
    LatticeGeneratorParameters<3>, atomistic_domain, coarsening_factors,
    domain_expansion_factors, hcp_c_by_a, lattice_file_name, lattice_parameter,
    make_atomistic_domain, n_coarsening_levels, n_sub_lattice, name, rotation,
    save_lattice_vectors, seed_point_offset_coeffs, method, center,
    lattice_structure)

template <std::size_t Dimension>
LatticeGeneratorParameters<Dimension>
load_lattice_parameters_from_json(std::istream &stream) {
  LatticeGeneratorParameters<Dimension> config{};
  const auto j = Json::parse(stream);
  from_json(j, config);
  return config;
}

template <std::size_t Dimension>
LatticeGeneratorParameters<Dimension>
load_lattice_parameters_from_json(const std::filesystem::path &path) {
  std::ifstream f(path);
  f.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  return load_lattice_parameters_from_json<Dimension>(f);
}

constexpr std::string_view SCHEMA_VERSION = "0.1";
constexpr std::string_view SCHEMA_URL =
    "https://gitlab.ethz.ch/mechanics-and-materials/aqcnes/-/raw/main/data/"
    "lattice_gen_params/latticegenerator_parameters.schema.json";

template <std::size_t Dimension>
void save_lattice_parameters_to_json(
    const LatticeGeneratorParameters<Dimension> &config, std::ostream &stream) {
  Json j;
  to_json(j, config);

  j["$version"] = SCHEMA_VERSION;
  j["$schema"] = SCHEMA_URL;

  stream << j.dump(2);
}

} // namespace lattice
