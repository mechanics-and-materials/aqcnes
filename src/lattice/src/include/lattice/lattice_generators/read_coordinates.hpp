// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Lattice generator from a lammps coordinares file
 * @authors S. Saxena, S. Zimmerman
 */

#pragma once

#include "lattice/lattice_cache.hpp"
#include "lattice/lattice_generators/lattice_generator_parameters.hpp"
#include "lattice/lattice_generators/nano_indentation.hpp"
#include "lattice/lattice_generators/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "utils/string_split.hpp"
#include "utils/string_to_bool.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

namespace lattice {

namespace traits {
template <> struct CanSaveLatticeVectors<ReadCoordinates> {
  constexpr static bool value = false;
};
} // namespace traits

namespace read_coordinates {
template <std::size_t Dimension>
void generate_lattice_from_method(
    const lattice::ReadCoordinates &method_parameters,
    const lattice::LatticeGeneratorParameters<Dimension> &config,
    std::vector<std::array<double, Dimension>> &lattice_points,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types,
    qcmesh::geometry::Box<Dimension> &bounds,
    qcmesh::geometry::Box<Dimension> &atomistic_domain) {
  auto cache = LatticeCache<Dimension>{};

  // read config from txt file
  std::array<double, Dimension> point{};
  std::vector<std::string> header_keywords{"atoms", "atom types", "xlo xhi",
                                           "ylo yhi", "zlo zhi"};
  std::vector<std::string> section_keywords{"Atoms", "Masses"};
  std::vector<std::string> words;
  std::string line;
  std::size_t noof_coordinates = 0;
  std::size_t noof_types = 0;

  cache = lattice_cache_from_config(config);

  // read coordinates from coordinates file
  std::ifstream file(method_parameters.coordinates_file_name.c_str());
  if (!file.is_open())
    throw exception::QCException{
        "Lattice coordinates file could not be opened."};
  bool header_mode = true;
  bool found_header_keyword = false;
  auto domain = qcmesh::geometry::Box<Dimension>{};

  while (file.peek() != EOF) {
    words.clear();
    getline(file, line);
    if (line[0] == '#')
      continue;
    if (line.find_first_not_of(' ') == std::string::npos)
      continue;

    // if not skipped, search for the keyword present in the line
    found_header_keyword = false;
    std::size_t index = 10;
    if (header_mode) {
      for (std::size_t idx = 0; idx < header_keywords.size(); idx++) {
        if (line.find(header_keywords[idx]) != std::string::npos) {
          index = idx;
          found_header_keyword = true;
          break;
        }
      }
    }

    if (found_header_keyword) {
      words =
          utils::string_split_many<std::string>(line, utils::AnyOf{", "}, ' ');
      // take action depending on the keyword found
      if (index == 0) {
        noof_coordinates = static_cast<std::size_t>(std::stoi(words[0]));
      } else if (index == 1) {
        noof_types = static_cast<std::size_t>(std::stoi(words[0]));
      } else if (index == 2) {
        domain.lower[0] = std::stod(words[0]);
        domain.upper[0] = std::stod(words[1]);
      } else if (index == 3) {
        domain.lower[1] = std::stod(words[0]);
        domain.upper[1] = std::stod(words[1]);
      } else if (index == 4) {
        domain.lower[2] = std::stod(words[0]);
        domain.upper[2] = std::stod(words[1]);
      }
    }

    // find a section keyword if no header keyword is found
    if (!found_header_keyword) {
      for (std::size_t idx = 0; idx < section_keywords.size(); idx++) {
        if (line.find(section_keywords[idx]) != std::string::npos) {
          header_mode =
              false; // once a section keyword is found, no more headers should exist
          index = idx;
          break;
        }
      }

      // take action depending on the section keyword
      if (index == 0) {
        if (noof_coordinates == 0)
          throw exception::QCException{
              "Please provide number of atoms in the header."};

        getline(file, line); // skip the first line
        for (std::size_t atomidx = 0; atomidx < noof_coordinates; atomidx++) {
          getline(file, line);
          words = utils::string_split_many<std::string>(
              line, utils::AnyOf{",  "}, ' ');
          material_ids.push_back(0);
          lattice_site_types.push_back(std::stoi(words[1]) - 1);
          point[0] = std::stod(words[2]);
          point[1] = std::stod(words[3]);
          point[2] = std::stod(words[4]);
          lattice_points.push_back(point);
        }
      } else if (index == 1) {
        if (noof_types == 0)
          throw exception::QCException{
              "Please provide number of atom types in the header."};

        getline(file, line); // skip the first line
        for (std::size_t typeidx = 0; typeidx < noof_types; typeidx++) {
          getline(file, line);
        }
      } else
        continue;
    }
  }

  // Deleting points outside domain specified
  std::set<std::size_t> indices_to_delete;

  namespace array_ops = qcmesh::array_ops;

  std::cout << "Removing atoms outside "
            << array_ops::as_streamable(domain.upper) << " , "
            << array_ops::as_streamable(domain.lower) << std::endl;

  for (std::size_t idx = 0; idx < lattice_points.size(); idx++) {
    if (!qcmesh::geometry::contains(domain, lattice_points[idx])) {
      indices_to_delete.insert(idx);
    }
  }

  for (auto it = indices_to_delete.rbegin(); it != indices_to_delete.rend();
       ++it) {
    const auto d = static_cast<std::vector<int>::difference_type>(*it);
    lattice_points.erase(lattice_points.begin() + d);
    lattice_site_types.erase(lattice_site_types.begin() + d);
    material_ids.erase(material_ids.begin() + d);
  }

  std::cout << "Deleted " << indices_to_delete.size() << " sites" << std::endl;
  bounds = qcmesh::geometry::bounding_box(lattice_points);
  atomistic_domain = bounds;
  cache.suffix = utils::to_string_with_zeros(lattice_points.size(), 6);

  std::cout << "total lattice sites added  = " << lattice_points.size()
            << std::endl;

  save_lattice_cache(config.lattice_file_name, cache);

  std::cout << "Printed Lattice File: " << config.lattice_file_name
            << std::endl;
}
} // namespace read_coordinates
} // namespace lattice
