// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Cubic lattice generator with possible coarsening levels
 * @authors P. Gupta, S. Saxena, S. Zimmerman
 */

#pragma once

#include "lattice/create_lattice_sites_in_bounds.hpp"
#include "lattice/generate_basis.hpp"
#include "lattice/lattice_cache.hpp"
#include "lattice/lattice_generators/lattice_generator_parameters.hpp"
#include "lattice/lattice_generators/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "utils/string_to_bool.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

namespace lattice {
namespace array_ops = qcmesh::array_ops;

// generate the lattice points for initializing a lomer dipole
// sample input file. Builds lattice, displaces lattice points according to
// linear elastic dipole fields, and trims the lattice points

namespace detail {
template <std::size_t N>
void print_matrix(const std::array<std::array<double, N>, N> &matrix) {
  for (const auto &row : matrix) {
    for (const auto x : row)
      std::cout << x << " ";
    std::cout << std::endl;
  }
}
} // namespace detail

template <std::size_t Dimension>
auto rotate_basis(
    const std::array<std::array<double, Dimension>, Dimension> &rotation,
    const std::array<std::array<double, Dimension>, Dimension> &basis) {
  auto rotated_basis_matrix =
      std::array<std::array<double, Dimension>, Dimension>{};
  const auto rotation_matrix = array_ops::as_eigen_matrix(rotation);
  if ((rotation_matrix.transpose() * rotation_matrix -
       Eigen::Matrix<double, Dimension, Dimension>::Identity())
          .norm() > 1.E-4)
    throw exception::QCException{"nano indentation config got a rotation "
                                 "matrix which does not satisfy R^T R = 1."};

  array_ops::as_eigen_matrix(rotated_basis_matrix) =
      rotation_matrix * array_ops::as_eigen_matrix(basis);
  return rotated_basis_matrix;
}

template <std::size_t Dimension>
LatticeCache<Dimension>
lattice_cache_from_config(const LatticeGeneratorParameters<Dimension> &config,
                          const std::string &suffix = "") {
  auto lattice_parameters = std::array<double, Dimension>{};
  for (std::size_t iv = 0; iv < Dimension; iv++)
    lattice_parameters[iv] = config.lattice_parameter;

  if (config.lattice_structure == LatticeStructure::HCP)
    lattice_parameters[2] *= config.hcp_c_by_a;

  auto basis_vectors = std::array<std::array<double, Dimension>, Dimension>{};
  lattice::generate_basis(lattice_parameters, config.lattice_structure,
                          basis_vectors);

  auto seed_point_offsets = std::vector<std::array<double, 3>>{};
  seed_point_offsets.resize(config.seed_point_offset_coeffs.size() + 1);

  const auto rotated_basis_matrix =
      rotate_basis(config.rotation, basis_vectors);

  seed_point_offsets[0] = std::array<double, Dimension>{};
  for (std::size_t iv = 1; iv < seed_point_offsets.size(); iv++)
    array_ops::as_eigen(seed_point_offsets[iv]) =
        array_ops::as_eigen_matrix(rotated_basis_matrix) *
        array_ops::as_eigen(config.seed_point_offset_coeffs[iv - 1]);

  return LatticeCache<Dimension>{.n_sub_lattice = config.n_sub_lattice,
                                 .lattice_type = config.lattice_structure,
                                 .suffix = suffix,
                                 .lattice_parameters = lattice_parameters,
                                 .rotated_basis_matrix = rotated_basis_matrix,
                                 .seed_point_offsets = seed_point_offsets,
                                 .rotation = config.rotation};
}

template <std::size_t Dimension>
std::vector<std::array<double, Dimension>>
generate_lattice(const lattice::NanoIndentation<Dimension> &method_parameters,
                 const lattice::LatticeGeneratorParameters<Dimension> &config,
                 std::vector<int> &material_ids,
                 std::vector<int> &lattice_site_types,
                 qcmesh::geometry::Box<Dimension> &atomistic_domain,
                 LatticeCache<Dimension> &cache) {
  std::vector<std::array<double, Dimension>> lattice_points{};
  std::array<double, Dimension> upper{};
  std::array<double, Dimension> lower{};

  std::vector<qcmesh::geometry::Box<Dimension>> coarsening_levels(
      config.n_coarsening_levels + 1);

  // read the file here for lattice generation
  std::vector<std::array<double, Dimension>> coarsening_levels_domains(
      config.n_coarsening_levels);

  for (std::size_t i = 0; i < config.n_coarsening_levels; i++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      coarsening_levels_domains[i][d] = config.atomistic_domain[d];
      coarsening_levels_domains[i][d] += 2.0 * double(i) *
                                         config.domain_expansion_factors[i] *
                                         config.atomistic_domain[d];
    }
    std::cout << "coarsening level = " << i << " bounds : "
              << array_ops::as_streamable(coarsening_levels_domains[i])
              << std::endl;
  }

  auto offset = std::array<double, Dimension>{
      {0.0, 0.0, -coarsening_levels_domains[0][2]}};

  array_ops::as_eigen(upper) =
      array_ops::as_eigen(config.center) +
      array_ops::as_eigen(coarsening_levels_domains[0]) +
      array_ops::as_eigen(offset);
  array_ops::as_eigen(lower) =
      array_ops::as_eigen(config.center) -
      array_ops::as_eigen(coarsening_levels_domains[0]) +
      array_ops::as_eigen(offset);

  coarsening_levels[1] = qcmesh::geometry::Box<Dimension>{lower, upper};

  for (std::size_t i_box = 1; i_box < config.n_coarsening_levels; i_box++) {
    offset[2] = -coarsening_levels_domains[i_box][2];
    array_ops::as_eigen(upper) =
        array_ops::as_eigen(config.center) +
        array_ops::as_eigen(coarsening_levels_domains[i_box]) +
        array_ops::as_eigen(offset);
    array_ops::as_eigen(lower) =
        array_ops::as_eigen(config.center) -
        array_ops::as_eigen(coarsening_levels_domains[i_box]) +
        array_ops::as_eigen(offset);

    coarsening_levels[i_box + 1] =
        qcmesh::geometry::Box<Dimension>{lower, upper};
  }

  // 0th coarsening level is an inverted box
  // create_lattice_sites_in_bounds only adds points inside
  // a given box AND outside the previous-to-given box
  // so, by (inside if lower<geometry::Point<upper) logic, every point is always
  // outside the first rectangle
  coarsening_levels[0] = qcmesh::geometry::Box<Dimension>{
      coarsening_levels[1].upper, coarsening_levels[1].lower};
  if (!config.make_atomistic_domain)
    coarsening_levels[1] = qcmesh::geometry::Box<Dimension>{
        coarsening_levels[1].upper, coarsening_levels[1].lower};

  cache = lattice_cache_from_config(config);

  std::vector<std::array<double, Dimension>> seed_points;
  std::vector<int> boundary_points;
  seed_points.resize(cache.seed_point_offsets.size());
  for (std::size_t iv = 0; iv < cache.seed_point_offsets.size(); iv++)
    array_ops::as_eigen(seed_points[iv]) =
        array_ops::as_eigen(config.center) +
        array_ops::as_eigen(cache.seed_point_offsets[iv]);

  std::array<std::array<double, Dimension>, Dimension> adjusted_basis{};

  for (std::size_t i_box = 0; i_box < config.n_coarsening_levels; i_box++) {
    boundary_points.resize(0);

    if (!config.make_atomistic_domain && i_box == 0)
      continue;

    for (std::size_t iv = 0; iv < Dimension; iv++) {
      for (std::size_t d = 0; d < Dimension; d++) {
        adjusted_basis[iv][d] = config.coarsening_factors[i_box] *
                                cache.rotated_basis_matrix[d][iv];
      }
    }

    std::cout << "while creating lattice p=" << std::endl;
    detail::print_matrix(adjusted_basis);

    // If multiple seed points exist, create coarsened lattice using only one of them
    if (i_box > 0 && seed_points.size() > 1) {
      seed_points.erase(seed_points.begin() + 1, seed_points.end());
    }

    std::cout << "Seed points:" << std::endl;
    for (std::size_t iv = 0; iv < seed_points.size(); iv++) {
      std::cout << array_ops::as_streamable(seed_points[iv]) << std::endl;
    }

    create_lattice_sites_in_bounds(
        coarsening_levels[i_box + 1], coarsening_levels[i_box], seed_points,
        adjusted_basis, lattice_points, material_ids, lattice_site_types);

    if (abs(config.coarsening_factors[i_box] - 1.0) < 1.0e-3)
      atomistic_domain = coarsening_levels[i_box + 1];
  }

  if (!config.make_atomistic_domain)
    atomistic_domain =
        qcmesh::geometry::Box<Dimension>{config.center, config.center};

  // Deleting points outside domain specified
  std::set<std::size_t> indices_to_delete;

  const auto &domain = method_parameters.domain_boundaries;

  std::cout << "Removing atoms outside "
            << array_ops::as_streamable(domain.upper) << " , "
            << array_ops::as_streamable(domain.lower) << std::endl;

  for (std::size_t idx = 0; idx < lattice_points.size(); idx++) {
    if (!qcmesh::geometry::contains(domain, lattice_points[idx])) {
      indices_to_delete.insert(idx);
    }
  }

  for (auto it = indices_to_delete.rbegin(); it != indices_to_delete.rend();
       ++it) {
    const auto d = static_cast<std::vector<int>::difference_type>(*it);
    lattice_points.erase(lattice_points.begin() + d);
    lattice_site_types.erase(lattice_site_types.begin() + d);
    material_ids.erase(material_ids.begin() + d);
  }

  std::cout << "Deleted " << indices_to_delete.size() << " sites" << std::endl;

  return lattice_points;
}

template <std::size_t Dimension>
std::vector<std::array<double, Dimension>>
generate_lattice(const lattice::NanoIndentation<Dimension> &method_parameters,
                 const lattice::LatticeGeneratorParameters<Dimension> &config,
                 std::vector<int> &material_indices,
                 qcmesh::geometry::Box<Dimension> &atomistic_domain) {
  auto cache = LatticeCache<Dimension>{};
  auto lattice_site_types = std::vector<int>{};

  return generate_lattice(method_parameters, config, material_indices,
                          lattice_site_types, atomistic_domain, cache);
}

template <std::size_t Dimension>
std::vector<std::array<double, Dimension>>
generate_lattice(const lattice::NanoIndentation<Dimension> &method_parameters,
                 const lattice::LatticeGeneratorParameters<Dimension> &config) {
  auto material_indices = std::vector<int>{};
  auto atomistic_domain = qcmesh::geometry::Box<Dimension>{};

  return generate_lattice(method_parameters, config, material_indices,
                          atomistic_domain);
}

namespace traits {
template <std::size_t Dimension>
struct CanSaveLatticeVectors<NanoIndentation<Dimension>> {
  constexpr static bool value = true;
};
} // namespace traits

namespace nano_indentation {
template <std::size_t Dimension>
void generate_lattice_from_method(
    const lattice::NanoIndentation<Dimension> &method_parameters,
    const lattice::LatticeGeneratorParameters<Dimension> &config,
    std::vector<std::array<double, Dimension>> &lattice_points,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types,
    qcmesh::geometry::Box<Dimension> &bounds,
    qcmesh::geometry::Box<Dimension> &atomistic_domain) {

  auto cache = LatticeCache<Dimension>{};
  lattice_points =
      generate_lattice<Dimension>(method_parameters, config, material_ids,
                                  lattice_site_types, atomistic_domain, cache);
  bounds = qcmesh::geometry::bounding_box(lattice_points);

  cache.suffix = utils::to_string_with_zeros(lattice_points.size(), 6);

  std::cout << "total lattice sites added  = " << lattice_points.size()
            << std::endl;

  save_lattice_cache(config.lattice_file_name, cache);

  std::cout << "Printed Lattice File: " << config.lattice_file_name
            << std::endl;
}
} // namespace nano_indentation
} // namespace lattice
