// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Atomistic lattice generator for a bi-crystal with symmetric tilt GB's 
 * @authors M. Spinola, S. Zimmerman
 */

#pragma once

#include "constants/constants.hpp"
#include "lattice/lattice_cache.hpp"
#include "lattice/lattice_generators/nano_indentation.hpp"
#include "lattice/lattice_generators/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "utils/string_to_bool.hpp"
#include "utils/to_string_with_zeros.hpp"
#include <array>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

namespace lattice {
namespace array_ops = qcmesh::array_ops;

template <std::size_t Dimension>
std::vector<std::array<double, Dimension>> generate_lattice_symmetric_gb(
    const lattice::SymmetricGrainboundaries<Dimension> &method_parameters,
    const lattice::LatticeGeneratorParameters<Dimension> &config,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types,
    qcmesh::geometry::Box<Dimension> &atomistic_domain,
    LatticeCache<Dimension> &cache) {
  std::vector<std::array<double, Dimension>> lattice_points;
  std::array<double, Dimension> upper{};
  std::array<double, Dimension> lower{};
  std::array<double, Dimension> padding{1e-6, 1e-6, 1e-6};

  std::vector<qcmesh::geometry::Box<Dimension>> coarsening_levels(
      2 * config.n_coarsening_levels + 1);

  // read the file here for lattice generation
  std::vector<std::array<double, Dimension>> coarsening_levels_domains(
      config.n_coarsening_levels);

  for (std::size_t i = 0; i < config.n_coarsening_levels; i++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      coarsening_levels_domains[i][d] = config.atomistic_domain[d];
      if (d == 0) {
        coarsening_levels_domains[i][d] += double(i) *
                                           config.domain_expansion_factors[i] *
                                           config.atomistic_domain[d];
      }
    }
    std::cout << "coarsening level = " << i << " bounds : "
              << array_ops::as_streamable(coarsening_levels_domains[i])
              << std::endl;
  }

  array_ops::as_eigen(upper) =
      array_ops::as_eigen(config.center) +
      array_ops::as_eigen(coarsening_levels_domains[0]) +
      array_ops::as_eigen(padding);
  array_ops::as_eigen(lower) =
      array_ops::as_eigen(config.center) - array_ops::as_eigen(padding);

  int box_ctr = 0;

  coarsening_levels[1] = qcmesh::geometry::Box<Dimension>{lower, upper};
  box_ctr++;

  // right grain boxes
  for (std::size_t i_box = 1; i_box < config.n_coarsening_levels; i_box++) {
    array_ops::as_eigen(upper) =
        array_ops::as_eigen(config.center) +
        array_ops::as_eigen(coarsening_levels_domains[i_box]);
    array_ops::as_eigen(lower) = array_ops::as_eigen(config.center);

    coarsening_levels[i_box + 1] =
        qcmesh::geometry::Box<Dimension>{lower, upper};
    box_ctr++;
  }

  // 0th coarsening level is an inverted box
  coarsening_levels[0] = qcmesh::geometry::Box<Dimension>{
      coarsening_levels[1].upper, coarsening_levels[1].lower};
  if (!config.make_atomistic_domain)
    coarsening_levels[1] = qcmesh::geometry::Box<Dimension>{
        coarsening_levels[1].upper, coarsening_levels[1].lower};

  // left grain boxes
  auto offset = std::array<double, Dimension>{
      {0.0, coarsening_levels_domains[0][1], coarsening_levels_domains[0][2]}};
  for (std::size_t i_box = 0; i_box < config.n_coarsening_levels; i_box++) {
    upper = {{0.0, coarsening_levels_domains[0][1],
              coarsening_levels_domains[0][2]}};
    array_ops::as_eigen(upper) += array_ops::as_eigen(padding);
    array_ops::as_eigen(lower) =
        -1 * (array_ops::as_eigen(config.center) +
              array_ops::as_eigen(coarsening_levels_domains[i_box])) +
        array_ops::as_eigen(offset) - array_ops::as_eigen(padding);

    coarsening_levels[box_ctr + 1] =
        qcmesh::geometry::Box<Dimension>{lower, upper};

    box_ctr++;
  }

  cache = lattice_cache_from_config(config);

  std::vector<std::array<double, Dimension>> seed_points;
  std::vector<int> boundary_points;
  seed_points.resize(cache.seed_point_offsets.size());
  for (std::size_t iv = 0; iv < cache.seed_point_offsets.size(); iv++)
    array_ops::as_eigen(seed_points[iv]) =
        array_ops::as_eigen(config.center) +
        array_ops::as_eigen(cache.seed_point_offsets[iv]);

  std::array<std::array<double, Dimension>, Dimension> adjusted_basis{};

  // compute rotation matrix for left grained:
  //  - it is rotated_basis_matrix mirrored about yz plane by inverting x
  //  components
  // - and inverting first vector to have right handed basis
  auto mirrored_rotated_basis_matrix = cache.rotated_basis_matrix;
  for (std::size_t i = 0; i < Dimension; i++) {
    for (std::size_t j = 0; j < Dimension; j++) {
      if (j == 0)
        mirrored_rotated_basis_matrix[i][j] =
            -1 * cache.rotated_basis_matrix[i][j];
      if (i == 0)
        mirrored_rotated_basis_matrix[i][j] =
            -1 * mirrored_rotated_basis_matrix[i][j];
    }
  }

  for (std::size_t i_box = 0; i_box < 2 * config.n_coarsening_levels; i_box++) {
    boundary_points.resize(0);

    if (!config.make_atomistic_domain && i_box == 0)
      continue;

    for (std::size_t iv = 0; iv < Dimension; iv++) {
      for (std::size_t d = 0; d < Dimension; d++) {
        if (i_box < config.n_coarsening_levels) {
          adjusted_basis[iv][d] = config.coarsening_factors[i_box] *
                                  cache.rotated_basis_matrix[d][iv];
          // basis are stored as (each column being the vector)
        } else {
          adjusted_basis[iv][d] =
              config.coarsening_factors[i_box - config.n_coarsening_levels] *
              mirrored_rotated_basis_matrix[d][iv];
        }
      }
    }

    std::cout << "while creating lattice " << std::endl;
    detail::print_matrix(adjusted_basis);

    // If multiple seed points exist, create coarsened lattice using only one of them
    if (i_box > 0 && seed_points.size() > 1) {
      seed_points.erase(seed_points.begin() + 1, seed_points.end());
    }

    std::cout << "Seed points:" << std::endl;
    for (std::size_t iv = 0; iv < seed_points.size(); iv++) {
      std::cout << array_ops::as_streamable(seed_points[iv]) << std::endl;
    }

    lattice::create_lattice_sites_in_bounds(
        coarsening_levels[i_box + 1], coarsening_levels[i_box], seed_points,
        adjusted_basis, lattice_points, material_ids, lattice_site_types);
  }

  const auto half_length =
      std::array<double, Dimension>{constants::geometric_tolerance};

  array_ops::as_eigen(coarsening_levels[config.n_coarsening_levels + 1].lower) =
      array_ops::as_eigen(
          coarsening_levels[config.n_coarsening_levels + 1].lower) -
      array_ops::as_eigen(half_length);

  array_ops::as_eigen(coarsening_levels[1].upper) =
      array_ops::as_eigen(coarsening_levels[1].upper) +
      array_ops::as_eigen(half_length);

  atomistic_domain = qcmesh::geometry::Box<Dimension>{
      coarsening_levels[config.n_coarsening_levels + 1].lower,
      coarsening_levels[1].upper};

  if (!config.make_atomistic_domain)
    atomistic_domain =
        qcmesh::geometry::Box<Dimension>{config.center, config.center};

  // Deleting points outside domain specified
  std::set<std::size_t> indices_to_delete;
  double total_distx{};
  double periodic_distx{};

  const auto &domain = method_parameters.domain_boundaries;

  std::array<double, 3> periods{
      {2 * coarsening_levels[config.n_coarsening_levels].upper[0],
       coarsening_levels[config.n_coarsening_levels].upper[1],
       coarsening_levels[config.n_coarsening_levels].upper[2]}};

  std::cout << "Removing atoms outside "
            << array_ops::as_streamable(domain.upper) << " , "
            << array_ops::as_streamable(domain.lower) << std::endl;

  for (std::size_t idx = 0; idx < lattice_points.size(); idx++) {
    if (!qcmesh::geometry::contains(domain, lattice_points[idx])) {
      indices_to_delete.insert(idx);
    }

    // Get indices of points of the left grain closer than input tolerance
    // to other points in the second grain
    if (lattice_points[idx][0] <= 1.E-8 &&
        lattice_points[idx][0] > -1 * config.lattice_parameter) {
      for (std::size_t j = 0; j < lattice_points.size(); j++) {
        if (lattice_points[j][0] > 1.E-8 &&
            lattice_points[j][0] < 1 * config.lattice_parameter)
          if (array_ops::squared_distance(lattice_points[j],
                                          lattice_points[idx]) <=
                  method_parameters.overlap_deletion_distance *
                      method_parameters.overlap_deletion_distance &&
              j != idx) {
            indices_to_delete.insert(idx);
          }
      }
    }

    // delete points too close when assuming periodicity in x direction
    if (method_parameters.delete_across_boundary) {
      if (lattice_points[idx][0] >
              (coarsening_levels[config.n_coarsening_levels].upper[0] -
               config.lattice_parameter) &&
          lattice_points[idx][0] <
              (coarsening_levels[config.n_coarsening_levels].upper[0] -
               1.E-4)) {
        for (std::size_t j = 0; j < lattice_points.size(); j++) {
          if (lattice_points[j][0] <
                  (coarsening_levels[config.n_coarsening_levels * 2].lower[0] +
                   config.lattice_parameter) &&
              lattice_points[j][0] >
                  (coarsening_levels[config.n_coarsening_levels * 2].lower[0] +
                   1.E-4)) {
            total_distx = lattice_points[idx][0] - lattice_points[j][0];
            periodic_distx = total_distx - periods[0];
            if ((periodic_distx * periodic_distx) <=
                (method_parameters.overlap_deletion_distance *
                 method_parameters.overlap_deletion_distance))
              indices_to_delete.insert(idx);
          }
        }
      }
    }
  }

  for (auto it = indices_to_delete.rbegin(); it != indices_to_delete.rend();
       ++it) {
    const auto d = static_cast<std::vector<int>::difference_type>(*it);
    lattice_points.erase(lattice_points.begin() + d);
    lattice_site_types.erase(lattice_site_types.begin() + d);
    material_ids.erase(material_ids.begin() + d);
  }

  std::cout << "Deleted " << indices_to_delete.size() << " sites" << std::endl;

  return lattice_points;
}

namespace traits {
template <std::size_t Dimension>
struct CanSaveLatticeVectors<SymmetricGrainboundaries<Dimension>> {
  constexpr static bool value = true;
};
} // namespace traits

namespace symmetric_gb {
template <std::size_t Dimension>
void generate_lattice_from_method(
    const lattice::SymmetricGrainboundaries<Dimension> &method_parameters,
    const lattice::LatticeGeneratorParameters<Dimension> &config,
    std::vector<std::array<double, Dimension>> &lattice_points,
    std::vector<int> &material_ids, std::vector<int> &lattice_site_types,
    qcmesh::geometry::Box<Dimension> &bounds,
    qcmesh::geometry::Box<Dimension> &atomistic_domain) {

  auto cache = LatticeCache<Dimension>{};
  lattice_points = generate_lattice_symmetric_gb<Dimension>(
      method_parameters, config, material_ids, lattice_site_types,
      atomistic_domain, cache);
  bounds = qcmesh::geometry::bounding_box(lattice_points);

  cache.suffix = utils::to_string_with_zeros(lattice_points.size(), 6);

  std::cout << "total lattice sites added  = " << lattice_points.size()
            << std::endl;

  save_lattice_cache(config.lattice_file_name, cache);

  std::cout << "Printed Lattice File: " << config.lattice_file_name
            << std::endl;
}
} // namespace symmetric_gb
} // namespace lattice
