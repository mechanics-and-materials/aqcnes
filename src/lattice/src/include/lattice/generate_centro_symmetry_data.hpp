// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief function implementation to get centrosymmetry vectors for the lattice type
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "lattice/lattice_structure.hpp"
#include <array>
#include <iostream>
#include <vector>

namespace lattice {

template <std::size_t Dimension>
void generate_centro_symmetry_data(
    const std::array<double, Dimension> &lattice_parameter,
    const LatticeStructure lattice_structure, const bool constrained,
    std::vector<std::array<double, Dimension>> &centro_symmetries) {

  switch (lattice_structure) {
  case LatticeStructure::FCC: {
    const auto a = lattice_parameter[0] / 2.0;
    if (constrained) {
      centro_symmetries.reserve(2);
      centro_symmetries.push_back({{a, a, 0}});
      centro_symmetries.push_back({{-a, a, 0}});
    } else {
      centro_symmetries.reserve(6);
      centro_symmetries.push_back({{a, 0, a}});
      centro_symmetries.push_back({{a, a, 0}});
      centro_symmetries.push_back({{0, a, a}});

      centro_symmetries.push_back({{-a, 0, a}});
      centro_symmetries.push_back({{-a, a, 0}});
      centro_symmetries.push_back({{0, -a, a}});
    }
  } break;
  case LatticeStructure::BCC: {
    const auto a = lattice_parameter[0] / 2.0;
    if (constrained) {
      centro_symmetries.reserve(2);
      centro_symmetries.push_back({{a, a, 0}});
      centro_symmetries.push_back({{-a, a, 0}});
    } else {
      centro_symmetries.reserve(4);
      centro_symmetries.push_back({{a, a, a}});
      centro_symmetries.push_back({{a, a, -a}});

      centro_symmetries.push_back({{-a, a, a}});
      centro_symmetries.push_back({{-a, -a, a}});
    }
  } break;
  case LatticeStructure::SimpleCubic:
  case LatticeStructure::Diamond:
  case LatticeStructure::HCP:
    break;
  default:
    throw exception::QCException{
        "Unsupported lattice structure " +
        std::string{serialize_lattice_structure(lattice_structure)} +
        ". Use BCC, FCC, HCP, Diamond or "
        "SimpleCubic."};
  }
}

} // namespace lattice
