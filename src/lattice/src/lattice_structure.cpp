// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief allowed lattice types
 * @authors S. Saxena, S. Zimmerman
 */

#include "lattice/lattice_structure.hpp"
#include <array>
#include <string_view>

namespace lattice {
namespace {
constexpr std::array LATTICE_TYPE_NAMES =
    std::array{std::string_view{"SimpleCubic"}, std::string_view{"BCC"},
               std::string_view{"FCC"},         std::string_view{"HCP"},
               std::string_view{"Diamond"},     std::string_view{"B1"},
               std::string_view{"B2"},          std::string_view{"L12"},
               std::string_view{"None"}};
} // namespace

LatticeStructure deserialize_lattice_structure(const std::string &name) {
  for (std::size_t s = 0; s < LATTICE_TYPE_NAMES.size(); s++)
    if (LATTICE_TYPE_NAMES[s] == name)
      return static_cast<LatticeStructure>(s);
  throw exception::QCException{std::string{"Invalid lattice structure: "} +
                               name};
}

std::string_view serialize_lattice_structure(const LatticeStructure structure) {
  return LATTICE_TYPE_NAMES[static_cast<std::size_t>(structure)];
}

} // namespace lattice
