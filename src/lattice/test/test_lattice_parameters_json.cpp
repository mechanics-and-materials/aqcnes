// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check whether json serialization of LatticeGeneratorParameters works
 *
 * One config is tested for every method (NanoIndentation, SymmetricGrainboundaries, ReadCoordinates)
 */

#include "lattice/lattice_generators/lattice_generator_parameters.hpp"
#include <fstream>
#include <gmock/gmock.h>
#include <sstream>

namespace lattice {

TEST(test_lattice_parameters_json,
     load_after_save_is_identity_for_nano_indentation) {

  const auto config_nano = LatticeGeneratorParameters<3>{
      .lattice_file_name = "Hello",
      .rotation = {{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}},
      .seed_point_offset_coeffs = {{0, 1, 2}},
      .method = NanoIndentation<3>{}};

  auto stream = std::stringstream{};
  save_lattice_parameters_to_json<3>(config_nano, stream);
  const auto reloaded_config_nano =
      load_lattice_parameters_from_json<3>(stream);

  // Check some selected fields:
  EXPECT_EQ(reloaded_config_nano.lattice_file_name,
            config_nano.lattice_file_name);
  EXPECT_EQ(reloaded_config_nano.rotation, config_nano.rotation);
  EXPECT_EQ(reloaded_config_nano.seed_point_offset_coeffs,
            config_nano.seed_point_offset_coeffs);
  EXPECT_EQ(reloaded_config_nano.method.index(), config_nano.method.index());
}

TEST(test_lattice_parameters_json,
     load_after_save_is_identity_for_symmetric_grain_boundary) {

  const auto config_symmetric = LatticeGeneratorParameters<3>{
      .lattice_structure = LatticeStructure::BCC,
      .method = SymmetricGrainboundaries<3>{
          .domain_boundaries = Domain<3>{.upper = {{5.0, 10.0, 15.0}}},
          .overlap_deletion_distance = 7}};

  auto stream = std::stringstream{};
  save_lattice_parameters_to_json<3>(config_symmetric, stream);
  const auto reloaded_config_symmetric =
      load_lattice_parameters_from_json<3>(stream);

  EXPECT_EQ(
      std::get<SymmetricGrainboundaries<3>>(reloaded_config_symmetric.method)
          .domain_boundaries.upper,
      std::get<SymmetricGrainboundaries<3>>(config_symmetric.method)
          .domain_boundaries.upper);

  EXPECT_EQ(reloaded_config_symmetric.lattice_structure,
            config_symmetric.lattice_structure);

  EXPECT_DOUBLE_EQ(
      std::get<SymmetricGrainboundaries<3>>(reloaded_config_symmetric.method)
          .overlap_deletion_distance,
      std::get<SymmetricGrainboundaries<3>>(config_symmetric.method)
          .overlap_deletion_distance);
}

TEST(test_lattice_parameters_json,
     load_after_save_is_identity_for_read_coordinates) {

  const auto config_read_coord = LatticeGeneratorParameters<3>{
      .method =
          ReadCoordinates{.coordinates_file_name = "./nanocluster_lammps.txt"}};

  auto stream = std::stringstream{};
  save_lattice_parameters_to_json<3>(config_read_coord, stream);
  const auto reloaded_config_read_coord =
      load_lattice_parameters_from_json<3>(stream);

  EXPECT_EQ(std::get<ReadCoordinates>(reloaded_config_read_coord.method)
                .coordinates_file_name,
            std::get<ReadCoordinates>(config_read_coord.method)
                .coordinates_file_name);
}

} // namespace lattice
