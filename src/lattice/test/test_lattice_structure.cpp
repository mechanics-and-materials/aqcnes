// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Test to check whether LatticeStructure is serialized correctly
 */

#include "exception/qc_exception.hpp"
#include "lattice/lattice_structure.hpp"
#include <gmock/gmock.h>

namespace lattice {
namespace {

TEST(test_lattice_structure, enum_is_serialized_correctly) {
  const auto lattice_structure_repr =
      serialize_lattice_structure(LatticeStructure::FCC);
  const auto *const expected_repr = "FCC";
  EXPECT_EQ(lattice_structure_repr, expected_repr);
}

TEST(test_lattice_structure, enum_deserializes_FCC) {
  const auto lattice_structure = deserialize_lattice_structure("FCC");
  const auto expected_structure = LatticeStructure::FCC;
  EXPECT_EQ(lattice_structure, expected_structure);
}

TEST(test_lattice_structure, enum_does_not_deserialize_invalid_repr) {
  EXPECT_THROW(deserialize_lattice_structure("invalid"),
               exception::QCException);
}

} // namespace
} // namespace lattice
