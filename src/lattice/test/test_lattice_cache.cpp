// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "lattice/lattice_cache.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>
#include <sstream>

namespace lattice {
namespace {

MATCHER_P(LatticeCacheEq, reference,
          std::string(negation ? "not " : "") + "equal to " +
              ::testing::PrintToString(reference)) {
  return ::testing::ExplainMatchResult(::testing::Eq(reference.n_sub_lattice),
                                       arg.n_sub_lattice, result_listener) &&
         ::testing::ExplainMatchResult(::testing::Eq(reference.lattice_type),
                                       arg.lattice_type, result_listener) &&
         ::testing::ExplainMatchResult(::testing::Eq(reference.suffix),
                                       arg.suffix, result_listener) &&
         ::testing::ExplainMatchResult(
             qcmesh::testing::DoubleArrayEq(reference.lattice_parameters),
             arg.lattice_parameters, result_listener) &&
         ::testing::ExplainMatchResult(
             qcmesh::testing::DoubleMatrixEq(reference.rotated_basis_matrix),
             arg.rotated_basis_matrix, result_listener) &&
         ::testing::ExplainMatchResult(
             qcmesh::testing::DoubleVecArrayEq(reference.seed_point_offsets),
             arg.seed_point_offsets, result_listener) &&
         ::testing::ExplainMatchResult(
             qcmesh::testing::DoubleMatrixEq(reference.rotation), arg.rotation,
             result_listener);
}

TEST(test_lattice_cache, load_after_save_is_identity) {
  const auto cache = LatticeCache<3>{
      .n_sub_lattice = 2,
      .lattice_type = LatticeStructure::None,
      .suffix = "...",
      .lattice_parameters = {1., 2., 3.},
      .rotated_basis_matrix = {{{1., -1., 1.}, {-1., 1., 1.}, {1., 1., -1.}}},
      .seed_point_offsets = {{0.1, 0.1, 0.1}, {1., 1., 1.}},
      .rotation = {{{-1., -1., 1.}, {1., 1., 1.}, {1., -1., -1.}}}};
  auto stream = std::stringstream{};
  save_lattice_cache(stream, cache);
  const auto loaded_cache = load_lattice_cache<3>(stream);
  EXPECT_THAT(loaded_cache, LatticeCacheEq(cache));
}
} // namespace
} // namespace lattice
