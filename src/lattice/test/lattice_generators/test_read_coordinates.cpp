// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file 
 * @brief Tests for @ref lattice::ReadsCoordinates lattice generation.
 */

#include "lattice/lattice_generators/read_coordinates.hpp"
#include <filesystem>
#include <fstream>
#include <gmock/gmock.h>
#include <string>
#include <vector>

namespace lattice {

namespace {
/**
 * @brief Test whether coordinates file for @ref lattice::ReadsCoordinates lattice generation is correctly loaded.
 *
 * An example file with 9 points is written out and used as an input
 * The last of the 9 points is out of bounds and should be deleted
 */
TEST(test_read_coordinates, read_coordinates_from_file_works) {
  const auto filename =
      std::filesystem::temp_directory_path() / "test_lattice_input.txt";

  // write out file
  auto out = std::ofstream{};
  out.open(filename);

  // example coordinates file with 9 points (last point is out of bounds and should be removed)
  out << "9 atoms\n\n1 atom types\n\n-3.61 3.61 xlo xhi\n-3.61 3.61 ylo "
         "yhi\n-3.61 3.61 zlo zhi\n\nMasses\n\n1 63.546\n\nAtoms\n\n1 1 "
         "-3.61 -1.805 -1.805\n2 1 -1.805 -3.61 -1.805\n3 1 -1.805 "
         "-1.805 -3.61\n4 1 -3.61 -3.61 0.0\n5 1 -3.61 -1.805 "
         "1.8050000000000002\n6 1 -1.805 -3.61 1.8050000000000002\n7 1 "
         "-1.805 -1.805 0.0\n8 1 -1.805 -1.805 3.61\n9 1 -1.805 -1.805 10000";
  out.close();

  const auto method =
      lattice::ReadCoordinates{.coordinates_file_name = filename};
  const auto config = lattice::LatticeGeneratorParameters<3>{
      .atomistic_domain = {{7.3, 7.3, 7.3}},
      .center = {{0.0, 0.0, 0.0}},
      .lattice_parameter = 3.52,
      .lattice_structure = lattice::LatticeStructure::FCC,
      .name = "NanoIndentation",
      .hcp_c_by_a = 1.632,
      .make_atomistic_domain = true,
      .lattice_file_name = "./mesh_files/MyLattice.txt",
      .rotation = {{{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}},
      .seed_point_offset_coeffs = {{0.25, 0.25, 0.25}},
      .method = method};

  auto lattice_points = std::vector<std::array<double, 3>>{};
  auto material_ids = std::vector<int>{};
  auto lattice_site_types = std::vector<int>{};
  auto bounds = qcmesh::geometry::Box<3>{};
  auto atomistic_domain = qcmesh::geometry::Box<3>{};

  read_coordinates::generate_lattice_from_method<3>(
      method, config, lattice_points, material_ids, lattice_site_types, bounds,
      atomistic_domain);

  // remove file again
  std::filesystem::remove(filename);

  // check first and last lattice point
  EXPECT_DOUBLE_EQ(lattice_points[0][0], -3.61);
  EXPECT_DOUBLE_EQ(lattice_points[0][1], -1.805);
  EXPECT_DOUBLE_EQ(lattice_points[0][2], -1.805);

  EXPECT_DOUBLE_EQ(lattice_points[7][0], -1.805);
  EXPECT_DOUBLE_EQ(lattice_points[7][1], -1.805);
  EXPECT_DOUBLE_EQ(lattice_points[7][2], 3.61);

  // check atomistic domain
  EXPECT_DOUBLE_EQ(atomistic_domain.lower[0], -3.61);
  EXPECT_DOUBLE_EQ(atomistic_domain.upper[0], -1.805);
  EXPECT_DOUBLE_EQ(atomistic_domain.upper[2], 3.61);

  // check if last point was removed
  EXPECT_EQ(lattice_points.size(), 8);
}
} // namespace
} // namespace lattice
