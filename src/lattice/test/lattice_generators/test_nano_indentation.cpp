// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "lattice/lattice_generators/nano_indentation.hpp"
#include "testing/matchers.hpp"
#include <gmock/gmock.h>

namespace lattice {
namespace {

/**
 * @brief Test suite for nano indentation configs
 */
struct NanoIndentationTest
    : ::testing::TestWithParam<
          std::tuple<lattice::LatticeGeneratorParameters<3>,
                     std::vector<std::array<double, 3>>>> {};

TEST_P(NanoIndentationTest, generates_simple_mesh) {
  auto [config, expected_points] = GetParam();
  const lattice::NanoIndentation<3> method_parameters =
      std::get<lattice::NanoIndentation<3>>(config.method);
  const auto points = generate_lattice<3>(method_parameters, config);
  EXPECT_THAT(points, testing::UnorderedPointArrayEq(expected_points));
}

/**
 * @brief Simple configuration for a 4x4x4 lattice for a grid parameter of 3.615.
 *
 * The lattice parameter is 3.615, and the atomistic domain bounds is 7.3 in x,y,z,
 * which is slightly biger than 2x3.615 = 7.23.
 * So, the lattice formed will have 2 unit cells on each side of the origin in x, y and z.
 * Therefore, we expect a (4x4x4) unit cells simulation box.
 * The basis vectors turn the rectangular unit cells lattice into a
 * 3d checkerboard (5x5x5 points + one on the center of each square face):
 */
auto config_4x4x4() {
  return lattice::LatticeGeneratorParameters<3>{
      .n_coarsening_levels = 1,
      .atomistic_domain = {{7.3, 7.3, 7.3}},
      .save_lattice_vectors = false,
      .lattice_parameter = 3.615,
      .lattice_structure = LatticeStructure::FCC,
      .name = "NanoIndentation",
      .coarsening_factors = {{1.0, 4.0, 16.0, 50.0, 100.0}},
      .domain_expansion_factors = {{15.0, 40.0, 50.0, 100.0, 500}},
      .lattice_file_name = "",
      .rotation = {{{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}}},
      .seed_point_offset_coeffs = {},
      .method = lattice::NanoIndentation<3>{
          .domain_boundaries = lattice::Domain<3>{
              .lower = {{-5000.0, -5000.0, -10000.0}},
              .upper = {{5000.0, 5000.0, 0.1}},
          }}};
}

/**
 * Points for an nxnxn checkerboard lattice with grid parameter 2*g.
 */
auto expected_lattice(int n, double g) {
  auto expected_points = std::vector<std::array<double, 3>>{};
  for (int i = -n; i <= n; i++)
    for (int j = -n; j <= n; j++)
      for (int k = -n; k <= n; k++)
        if ((i + j + k + 3 * n) % 2 == 0)
          // All (i, j, k) in [-n,n]^3, such that either
          // i, j, k are even, in which case (x, y, z) is vertex of a unit cell,
          // or exactly 2 of i, j, k will be odd, in which case the point is the sum of a cell vertex and a basis vector.
          // z gets a negative offset, as nano indentation translates the lattice points such that the
          // max z-coordinate is 0.
          expected_points.emplace_back(
              std::array<double, 3>{i * g, j * g, (k - n) * g});
  return expected_points;
}

/**
 * @brief Simple configuration for a 2x2x2 lattice for a grid parameter of 2 with seed offsets.
 */
auto config_2x2x2_seed_offset() {
  return lattice::LatticeGeneratorParameters<3>{
      .n_coarsening_levels = 1,
      .atomistic_domain = {{2.1, 2.1, 2.1}},
      .save_lattice_vectors = false,
      .lattice_parameter = 2.0,
      .lattice_structure = LatticeStructure::Diamond,
      .name = "NanoIndentation",
      .coarsening_factors = {{1.0, 4.0, 16.0, 50.0, 100.0}},
      .domain_expansion_factors = {{15.0, 40.0, 50.0, 100.0, 500}},
      .lattice_file_name = "",
      .rotation = {{{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}}},
      .seed_point_offset_coeffs = {{0.25, 0.25, 0.25}},
      .method = lattice::NanoIndentation<3>{
          .domain_boundaries = lattice::Domain<3>{
              .lower = {{-2000.0, -2000.0, -5000.0}},
              .upper = {{2500.0, 2500.0, 0.1}},
          }}};
}

/**
 * Expected points for the 2x2x2 config with seed offsets.
 * For a Diamond lattice (see config above), the lattice vectors
 * are the same as that for FCC but there are two points to be translated
 * with respect to the lattice vectors (l1,l2,l3), one being the centre and the other
 * offset wrt the center by a vector v = 0.25*l1 + 0.25*l2 + 0.25*l3.
 * Hence, seed_point_offset_coeffs = {{0.25, 0.25, 0.25}} is added in the config
 * and additional points are expected, which have been explicitly written below.
 */
auto expected_lattice_2x2x2_seed_offset() {
  auto points = expected_lattice(2, 2.0 / 2);
  const auto additional_points = std::vector<std::array<double, 3>>{
      {{-1.5, -1.5, -3.5}, {-1.5, -1.5, -1.5}, {-1.5, -0.5, -2.5},
       {-1.5, -0.5, -0.5}, {-1.5, 0.5, -3.5},  {-1.5, 0.5, -1.5},
       {-1.5, 1.5, -2.5},  {-1.5, 1.5, -0.5},  {-0.5, -0.5, -3.5},
       {-0.5, -0.5, -1.5}, {-0.5, -1.5, -2.5}, {-0.5, -1.5, -0.5},
       {-0.5, 0.5, -2.5},  {-0.5, 0.5, -0.5},  {-0.5, 1.5, -3.5},
       {-0.5, 1.5, -1.5},  {0.5, -1.5, -3.5},  {0.5, -1.5, -1.5},
       {0.5, -0.5, -2.5},  {0.5, -0.5, -0.5},  {0.5, 0.5, -1.5},
       {0.5, 0.5, -3.5},   {0.5, 1.5, -0.5},   {0.5, 1.5, -2.5},
       {1.5, -1.5, -0.5},  {1.5, -1.5, -2.5},  {1.5, -0.5, -1.5},
       {1.5, -0.5, -3.5},  {1.5, 0.5, -0.5},   {1.5, 0.5, -2.5},
       {1.5, 1.5, -1.5},   {1.5, 1.5, -3.5}}};
  points.insert(points.end(), additional_points.begin(),
                additional_points.end());
  return points;
}

/**
 * @brief Rotation by 90° around the direction (nx, ny, nz).
 * R v = <n, v> n + n x v
 *
 * n x v = (n[1] v[2] - n[2] v[1], n[2] v[0] - n[0] v[2], n[0] v[1] - n[1] v[0])
 *        /   0    -n[2]  n[1] \
 * => R = |  n[2]   0    -n[0] | + (n n^T)
 *        \ -n[1]   n[0]   0   /
 */
auto rot_90(const double nx, const double ny, const double nz) {
  return std::array<std::array<double, 3>, 3>{
      {{nx * nx, nx * ny - nz, nx * nz + ny},
       {nx * ny + nz, ny * ny, ny * nz - nx},
       {nx * nz - ny, ny * nz + nx, nz * nz}}};
}

/**
 * @brief Simple configuration for a 2x1x1 lattice for a grid parameter of 2 with rotation.
 */
auto config_2x1x1_rotated(
    const std::array<std::array<double, 3>, 3> &rotation) {
  return lattice::LatticeGeneratorParameters<3>{
      .n_coarsening_levels = 1,
      .atomistic_domain = {{2.01, 1.01, 1.01}},
      .save_lattice_vectors = false,
      .lattice_parameter = 2.0,
      .lattice_structure = LatticeStructure::Diamond,
      .name = "NanoIndentation",
      .coarsening_factors = {{1.0, 4.0, 16.0, 50.0, 100.0}},
      .domain_expansion_factors = {{15.0, 40.0, 50.0, 100.0, 500}},
      .lattice_file_name = "",
      .rotation = rotation,
      .seed_point_offset_coeffs = {},
      .method = lattice::NanoIndentation<3>{
          .domain_boundaries = lattice::Domain<3>{
              .lower = {{-2000.0, -2000.0, -5000.0}},
              .upper = {{2500.0, 2500.0, 0.1}},
          }}};
}

/**
 * @brief The 2x2x1 lattice, rotated 30° around the z axis.
 *
 * All points contained in [-2, 2]x[-1,1]x[-2,0]
 */
auto expected_lattice_2x1x1_rotated_z() {
  const auto s = sqrt(3.) / 2;
  return std::vector<std::array<double, 3>>{{
      {0, 0, 0},               // {0, 0, 0}
      {0, 0, -2},              // {0, 0, -2}
      {s, 0.5, -1},            // {1, 0, -1}
      {-0.5, s, -1},           // {0, 1, -1}
      {-s, -0.5, -1},          // {-1, 0, -1}
      {0.5, -s, -1},           // {0, -1, -1}
      {s + 0.5, -s + 0.5, 0},  // {1, -1, 0}
      {s + 0.5, -s + 0.5, -2}, // {1, -1, -2}
      {-s - 0.5, s - 0.5, 0},  // {-1, 1, 0}
      {-s - 0.5, s - 0.5, -2}, // {-1, 1, -2}
      {2 * s, 1, 0},           // {2, 0, 0}
      {2 * s, 1, -2},          // {2, 0, -2}
      {-2 * s, -1, 0},         // {-2, 0, 0}
      {-2 * s, -1, -2},        // {-2, 0, -2}
  }};
}

/**
 * @brief The 2x2x1 lattice, rotated around the (1,1,0) axis by 90°.
 *
 * All points contained in [-2, 2]x[-1,1]x[-2,0]
 */
auto expected_lattice_2x1x1_rotated_xy() {
  const auto s = M_SQRT1_2;
  return std::vector<std::array<double, 3>>{{
      {-1., -1., 0.},           // {-1, -1, 0}
      {-0.5 - s, -0.5 + s, -s}, // {0, -1, -1}
      {0., 0., -2 * s},         // {1, -1, 0}
      {0., 0., 0.},             // {0, 0, 0}
      {1., 1., -2 * s},         // {2, 0, 0}
      {1., 1., 0.},             // {1, 1, 0}
      {-1., -1, -2 * s},        // {0, -2, 0}
      {0.5 + s, 0.5 - s, -s}    // {1, 0, 1}
  }};
}

INSTANTIATE_TEST_CASE_P(
    NanoIndentation, NanoIndentationTest,
    testing::Values(
        std::make_tuple(config_4x4x4(), expected_lattice(4, 3.615 / 2)),
        std::make_tuple(config_2x2x2_seed_offset(),
                        expected_lattice_2x2x2_seed_offset()),
        std::make_tuple(config_2x1x1_rotated({{{sqrt(3.) / 2., -0.5, 0.},
                                               {0.5, sqrt(3.) / 2., 0.},
                                               {0., 0., 1.}}}),
                        expected_lattice_2x1x1_rotated_z()),
        std::make_tuple(config_2x1x1_rotated(rot_90(M_SQRT1_2, M_SQRT1_2, 0.)),
                        expected_lattice_2x1x1_rotated_xy())));

TEST(test_nano_indentation, throws_for_invalid_rotation) {
  const auto config =
      config_2x1x1_rotated({{{1., 0., 0.}, {0., 2., 0.}, {0., 0., 1.}}});
  const lattice::NanoIndentation<3> method_parameters =
      std::get<lattice::NanoIndentation<3>>(config.method);
  EXPECT_THROW(generate_lattice<3>(method_parameters, config),
               exception::QCException);
}

} // namespace
} // namespace lattice
