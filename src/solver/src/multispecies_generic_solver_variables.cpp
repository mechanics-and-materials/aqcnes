// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "solver/multispecies_generic_solver_variables.hpp"

namespace solver {

template struct detail::NoiGenericSolverVariables<3, MAX_NOI>;

namespace {

template <std::size_t Dimension, std::size_t I = 1>
struct SolverVariableFactory {
  static NoiGenericSolverVariables<Dimension>
  create_solver_variables(input::Userinput &&userinput) {
    if constexpr (I > MAX_NOI)
      throw exception::QCException{
          "Unsupported number of impurities: " +
          std::to_string(userinput.impurity_atomic_masses.size())};
    else {
      if (I != userinput.impurity_atomic_masses.size())
        return SolverVariableFactory<Dimension, I + 1>::create_solver_variables(
            std::move(userinput));
      auto out = solver::SolverVariables<Dimension, I>{};
      out.input = userinput;
      return out;
    }
  };
};

} // namespace

NoiGenericSolverVariables<3>
create_solver_variables(input::Userinput &&userinput) {
  return SolverVariableFactory<3>::create_solver_variables(
      std::move(userinput));
}

} // namespace solver
