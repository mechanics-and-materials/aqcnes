// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "solver/solver_variables.hpp"

namespace solver {
FireParameters fire_parameters_from_userinput(const input::Userinput &input) {
  auto fire_parameters = FireParameters{};
  fire_parameters.time_step = input.fire_time_step;
  fire_parameters.alpha = input.fire_initial_alpha;
  fire_parameters.steps_post_reset = input.fire_steps_post_reset;

  fire_parameters.min_residual = input.fire_min_residual;
  return fire_parameters;
}

} // namespace solver
