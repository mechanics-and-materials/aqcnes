// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementations to relax an infinite bulk lattice
 * @authors S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "materials/material.hpp"
#include "meshing/array_from_vec.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/solver.hpp"
#include "solver/time_integrators.hpp"
#include <array>
#include <filesystem>
#include <tuple>
#include <vector>

#ifdef USE_TORCH
#include "materials/nn_material.hpp"
#endif

namespace solver {
namespace array_ops = qcmesh::array_ops;

template <std::size_t Dimension>
std::vector<geometry::Point<Dimension>> generate_lattice(
    const geometry::Point<Dimension> &center,
    const std::array<std::array<double, Dimension>, Dimension> &basis) {
  // Get lattice points
  std::vector<geometry::Point<Dimension>> lattice;
  geometry::Point<Dimension> lattice_point;
  for (int l = -8; l < 8; l++)
    for (int m = -8; m < 8; m++)
      for (int n = -8; n < 8; n++) {
        array_ops::as_eigen(lattice_point) =

            array_ops::as_eigen(center) +
            array_ops::as_eigen_matrix(basis) *
                Eigen::Vector3d(double(l), double(m), double(n));
        lattice.push_back(lattice_point);
      }
  return lattice;
}

template <std::size_t Dimension>
std::vector<geometry::Point<Dimension>>
collect_neighbours(const std::vector<geometry::Point<Dimension>> &lattice,
                   const double cutoff_radius,
                   const geometry::Point<Dimension> &center) {
  std::vector<geometry::Point<Dimension>> neighbours;
  for (const auto &point : lattice) {
    const auto d = array_ops::distance(center, point);
    if (1.0e-2 < d && d <= cutoff_radius)
      neighbours.push_back(point);
  }
  return neighbours;
}

inline double
delta_thermal_force(const std::vector<double> &thermal_forces_atom,
                    const std::vector<double> &thermal_forces_neigh) {
  auto thermal_force = double{0};
  for (const auto f : thermal_forces_atom)
    thermal_force -= f;
  for (const auto f : thermal_forces_neigh)
    thermal_force += f;
  return thermal_force;
}

template <std::size_t Dimension>
void update_periodic_forces(
    std::array<double, Dimension * 2> &periodic_forces,
    const std::vector<geometry::Point<Dimension>> &neighbours,
    const std::vector<geometry::Point<Dimension>> &forces,
    const std::array<std::array<bool, Dimension>, 2> &constrained) {
  periodic_forces.fill(0.);
  for (std::size_t neighbour_idx = 0; neighbour_idx < neighbours.size();
       neighbour_idx++)
    for (std::size_t d = 0; d < Dimension; d++) {
      if (!constrained[0][d])
        periodic_forces[d] +=
            forces[neighbour_idx][d] * neighbours[neighbour_idx][d];
      if (!constrained[1][d]) {
        const auto d_next = (d + 1 == Dimension) ? 0 : d + 1;
        periodic_forces[Dimension + d] +=
            (forces[neighbour_idx][d] * neighbours[neighbour_idx][d_next] +
             forces[neighbour_idx][d_next] * neighbours[neighbour_idx][d]) /
            2;
      }
    }
}

template <std::size_t Dimension>
void set_displacement_gradient(
    Eigen::Matrix<double, Dimension, Dimension> &displacement_gradient,
    const std::array<double, Dimension * 2> &periodic_strains) {
  displacement_gradient(0, 0) = periodic_strains[0];
  displacement_gradient(1, 1) = periodic_strains[1];
  displacement_gradient(2, 2) = periodic_strains[2];
  displacement_gradient(0, 1) = periodic_strains[3];
  displacement_gradient(1, 0) = periodic_strains[3];
  displacement_gradient(1, 2) = periodic_strains[4];
  displacement_gradient(2, 1) = periodic_strains[4];
  displacement_gradient(0, 2) = periodic_strains[5];
  displacement_gradient(2, 0) = periodic_strains[5];
}

template <std::size_t Dimension>
void update_positions(
    Eigen::Matrix<double, Dimension, Dimension> &displacement_gradient,
    Eigen::Matrix<double, Dimension, Dimension> &deformation_gradient,
    const std::array<double, Dimension * 2> &periodic_strains,
    std::vector<geometry::Point<Dimension>> &neighbours) {
  set_displacement_gradient<Dimension>(displacement_gradient, periodic_strains);

  deformation_gradient = displacement_gradient +
                         Eigen::Matrix<double, Dimension, Dimension>::Identity(
                             Dimension, Dimension);
  for (auto &neighbour : neighbours)
    array_ops::as_eigen(neighbour) =
        deformation_gradient * array_ops::as_eigen(neighbour);
}

template <const std::size_t Dimension>
void print_relaxed_cluster(
    const geometry::Point<Dimension> &center,
    const std::vector<geometry::Point<Dimension>> &neighbours,
    const std::optional<double> &energy = std::nullopt) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  std::filesystem::create_directories("./solution_files/");
  std::string file_name =
      "./solution_files/datagen_clusters_" + std::to_string(mpi_rank) + ".dat";
  std::ofstream f_print;
  f_print.open(file_name, std::ios::app);
  f_print << center[0] << "," << center[1] << "," << center[2];
  if (energy.has_value())
    f_print << "," << energy.value();
  for (std::size_t neighbour_idx = 0; neighbour_idx < neighbours.size();
       neighbour_idx++) {
    for (std::size_t d = 0; d < Dimension; d++)
      f_print << "," << neighbours[neighbour_idx][d];
    if (energy.has_value())
      f_print << "," << energy.value();
  }
  f_print << '\n';
  f_print.close();
}

template <std::size_t Dimension> struct RelaxationResults {
  std::array<geometry::Point<Dimension>, Dimension> deformed_basis{};
  double phi{};
  double e{};
};

template <std::size_t Dimension, std::size_t Noi, std::size_t Nidof>
RelaxationResults<Dimension>
relax_infinite(const input::Userinput &input,
               const materials::Material<Dimension, Noi, Nidof> &mat,
               const std::vector<geometry::Point<Dimension>> &lattice,
               const std::array<geometry::Point<Dimension>, Dimension> &basis,
               const double temperature,
               solver::SolverVariables<Dimension, Noi> &solver_variables,
               const std::array<std::array<bool, Dimension>, 2> &constrained) {

  geometry::Point<Dimension> center = {{0.0, 0.0, 0.0}};

  std::array<double, 2 * Dimension> periodic_forces{};
  std::array<double, 2 * Dimension> periodic_velocities{};
  std::array<double, 2 * Dimension> periodic_strains{};

  std::vector<geometry::Point<Dimension>> forces;
  Eigen::Matrix<double, Dimension, Dimension> displacement_gradient;
  Eigen::Matrix<double, Dimension, Dimension> deformation_gradient;

  auto neighbours =
      collect_neighbours(lattice, mat.neighbor_cutoff_radius, center);

  auto atom_nodal_data = meshing::NodalDataMultispecies<Noi>{
      array_from_vec<Noi>(input.initial_impurity_concentrations)};
  std::vector<meshing::NodalDataMultispecies<Noi>> neighbours_nodal_data =
      std::vector(neighbours.size(), atom_nodal_data);

  const auto mass = constants::G_TO_EV_FS2_A2 * mat.atomic_masses[0];

  bool fire_stop = false;
  solver_variables.relaxation_fire_parameters =
      solver::fire_parameters_from_userinput(input);
  solver_variables.apply_external_force = false;
  solver_variables.apply_boundary_conditions = false;

  auto deformed_basis = basis;
  double energy{};

  if (temperature > 0.) {
    std::vector<double> thermal_forces_atom;
    std::vector<double> thermal_forces_neigh;
    std::vector<std::array<double, Nidof>> neighbours_thermal_coordinates;
    double thermal_force = 0.0;
    double thermal_velocity = 0.0;

    auto atom_thermal_coordinates = std::array<double, Nidof>{
        {-log(constants::k_Boltzmann * temperature), input.Phi_ref}};
    neighbours_thermal_coordinates =
        std::vector(neighbours.size(), atom_thermal_coordinates);

    for (std::size_t fire_iter = 0;
         fire_iter < (std::size_t)input.fire_max_iter && !fire_stop;
         fire_iter++) {
      solver_variables.relaxation_fire_parameters.iter = fire_iter;
#ifdef USE_TORCH
      if (std::holds_alternative<input::NNMaterialProperties>(input.material)) {
        const auto *nn_material =
            dynamic_cast<materials::NNMaterial *>(mat.cluster_model.get());
        nn_material->get_thermalized_energy_and_forces_nn(
            energy, forces, thermal_forces_atom, thermal_forces_neigh, center,
            atom_thermal_coordinates, neighbours,
            neighbours_thermal_coordinates);
      } else
#endif
      {
        if (input.quad_type == 3) {
          mat.get_thermalized_forces_3(forces, thermal_forces_atom,
                                       thermal_forces_neigh, center,
                                       atom_thermal_coordinates, neighbours,
                                       neighbours_thermal_coordinates,
                                       atom_nodal_data, neighbours_nodal_data);
          energy = mat.get_thermalized_energy_3(
              center, atom_thermal_coordinates, neighbours,
              neighbours_thermal_coordinates, atom_nodal_data,
              neighbours_nodal_data);
        } else if (input.quad_type == 5) {
          mat.get_thermalized_forces_5(forces, thermal_forces_atom,
                                       thermal_forces_neigh, center,
                                       atom_thermal_coordinates, neighbours,
                                       neighbours_thermal_coordinates,
                                       atom_nodal_data, neighbours_nodal_data);
          energy = mat.get_thermalized_energy_5(
              center, atom_thermal_coordinates, neighbours,
              neighbours_thermal_coordinates, atom_nodal_data,
              neighbours_nodal_data);
        }
      }
      thermal_force =
          exp(-atom_thermal_coordinates[0]) +
          delta_thermal_force(thermal_forces_atom, thermal_forces_neigh);
      update_periodic_forces(periodic_forces, neighbours, forces, constrained);
      solver::velocity_verlet_fire_infmodel<true, Dimension>(
          periodic_forces, periodic_velocities, periodic_strains, input.a_ref,
          thermal_force, thermal_velocity, mass, fire_stop, solver_variables);
      const auto isentropic = false;
      solver::update_thermal_coordinates<Nidof>(
          atom_thermal_coordinates, thermal_velocity, isentropic,
          solver_variables.relaxation_fire_parameters.time_step);
      update_positions<Dimension>(displacement_gradient, deformation_gradient,
                                  periodic_strains, neighbours);
      for (auto &c : neighbours_thermal_coordinates)
        c = atom_thermal_coordinates;
      array_ops::as_eigen_matrix(deformed_basis) =
          deformation_gradient * array_ops::as_eigen_matrix(deformed_basis);
    }
    // Print the relaxed cluster if requested in the user input
    if (input.non_centrosymmetric_neighborhoods_frequency.has_value())
      print_relaxed_cluster(center, neighbours, atom_thermal_coordinates[1]);

    return RelaxationResults<Dimension>{deformed_basis,
                                        atom_thermal_coordinates[1], energy};
  }
  // 0 T case:
  for (std::size_t fire_iter = 0;
       fire_iter < (std::size_t)input.fire_max_iter && !fire_stop;
       fire_iter++) {
    solver_variables.relaxation_fire_parameters.iter = fire_iter;
    mat.get_cluster_forces(forces, center, neighbours, atom_nodal_data,
                           neighbours_nodal_data);
    energy = mat.get_cluster_energy(center, neighbours, atom_nodal_data,
                                    neighbours_nodal_data);
    update_periodic_forces(periodic_forces, neighbours, forces, constrained);
    auto unused = double{};
    solver::velocity_verlet_fire_infmodel<false, Dimension>(
        periodic_forces, periodic_velocities, periodic_strains, input.a_ref, 0.,
        unused, mass, fire_stop, solver_variables);
    update_positions<Dimension>(displacement_gradient, deformation_gradient,
                                periodic_strains, neighbours);
    array_ops::as_eigen_matrix(deformed_basis) =
        deformation_gradient * array_ops::as_eigen_matrix(deformed_basis);
  }
  // Print the relaxed cluster if requested in the user input
  if (input.non_centrosymmetric_neighborhoods_frequency.has_value())
    print_relaxed_cluster(center, neighbours);

  return RelaxationResults<Dimension>{deformed_basis, 0., energy};
}

template <std::size_t Dimension, std::size_t Noi, std::size_t Nidof>
RelaxationResults<Dimension> relax_infinite(
    const input::Userinput &input,
    const materials::Material<Dimension, Noi, Nidof> &mat,
    const std::array<std::array<double, Dimension>, Dimension> &basis,
    const double temperature,
    solver::SolverVariables<Dimension, Noi> &solver_variables,
    const std::array<std::array<bool, Dimension>, 2> &constrained =
        std::array<std::array<bool, Dimension>, 2>{}) {
  geometry::Point<Dimension> center = {{0.0, 0.0, 0.0}};

  //  Give an initial user imposed deformation to the lattice
  Eigen::Matrix<double, Dimension, Dimension> displacement_gradient;
  Eigen::Matrix<double, Dimension, Dimension> deformation_gradient;
  for (std::size_t i = 0; i < Dimension; i++) {
    for (std::size_t j = 0; j < Dimension; j++) {
      displacement_gradient(i, j) =
          input.indenter_increment * input.external_displacement_gradient[i][j];
    }
  }
  deformation_gradient = displacement_gradient +
                         Eigen::Matrix<double, Dimension, Dimension>::Identity(
                             Dimension, Dimension);

  auto deformed_basis = std::array<std::array<double, Dimension>, Dimension>{};
  array_ops::as_eigen_matrix(deformed_basis) =
      deformation_gradient * array_ops::as_eigen_matrix(basis);
  const auto lattice = generate_lattice(center, deformed_basis);

  return relax_infinite<Dimension, Noi, Nidof>(
      input, mat, lattice, basis, temperature, solver_variables, constrained);
}

template <std::size_t Dimension, std::size_t Noi, std::size_t Nidof>
RelaxationResults<Dimension> relax_infinite(
    const input::Userinput &input,
    const materials::Material<Dimension, Noi, Nidof> &mat,
    const std::array<std::array<double, Dimension>, Dimension> &basis,
    const double temperature,
    const std::array<std::array<bool, Dimension>, 2> &constrained =
        std::array<std::array<bool, Dimension>, 2>{}) {
  geometry::Point<Dimension> center = {{0.0, 0.0, 0.0}};

  //  Give an initial user imposed deformation to the lattice
  Eigen::Matrix<double, Dimension, Dimension> displacement_gradient;
  Eigen::Matrix<double, Dimension, Dimension> deformation_gradient;
  for (std::size_t i = 0; i < Dimension; i++) {
    for (std::size_t j = 0; j < Dimension; j++) {
      displacement_gradient(i, j) =
          input.indenter_increment * input.external_displacement_gradient[i][j];
    }
  }
  deformation_gradient = displacement_gradient +
                         Eigen::Matrix<double, Dimension, Dimension>::Identity(
                             Dimension, Dimension);

  auto deformed_basis = std::array<std::array<double, Dimension>, Dimension>{};
  array_ops::as_eigen_matrix(deformed_basis) =
      deformation_gradient * array_ops::as_eigen_matrix(basis);
  const auto lattice = generate_lattice(center, deformed_basis);

  solver::SolverVariables<Dimension, Noi> solver_variables =
      solver::SolverVariables<Dimension, Noi>{};

  return relax_infinite<Dimension, Noi, Nidof>(
      input, mat, lattice, basis, temperature, solver_variables, constrained);
}
} // namespace solver
