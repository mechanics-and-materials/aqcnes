// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Provides a `std::variant` of @ref solver::SolverVariables with different
 * `Noi` template arguments.
 */

#pragma once

#include "solver/solver_variables.hpp"
#include <variant>

namespace solver {

constexpr static std::size_t MAX_NOI = 2;

namespace detail {

/**
 * @brief Helper template to construct `std::variant<T<1>, T<2>, ...>`.
 */
template <typename T, typename... Args> struct ExtendVariant;

template <typename... Args, typename Arg>
struct ExtendVariant<std::variant<Args...>, Arg> {
  using Type = std::variant<Args..., Arg>;
};

/**
 * @brief Helper template to construct @ref NoiGenericSolverVariables using @ref ExtendVariant.
 */
template <std::size_t Dimension, std::size_t MaxNoi>
struct NoiGenericSolverVariables {
  using Type = typename ExtendVariant<
      typename NoiGenericSolverVariables<Dimension, MaxNoi - 1>::Type,
      typename solver::SolverVariables<Dimension, MaxNoi>>::Type;
};
template <std::size_t Dimension>
struct NoiGenericSolverVariables<Dimension, 1> {
  using Type = std::variant<solver::SolverVariables<Dimension, 1>>;
};

extern template struct NoiGenericSolverVariables<3, MAX_NOI>;

} // namespace detail

/**
 * @brief Expands to std::variant<SolverVariables<DIM, 1>, ..., SolverVariables<DIM, MAX_NOI>>.
 */
template <std::size_t Dimension>
using NoiGenericSolverVariables =
    typename detail::NoiGenericSolverVariables<Dimension, MAX_NOI>::Type;

/**
 * @brief Creates an instance of @ref NoiGenericSolverVariables holding a
 * @ref SolverVariables<3, Noi> where `Noi` is deduced by `userinput`.
 */
NoiGenericSolverVariables<3>
create_solver_variables(input::Userinput &&userinput);

} // namespace solver
