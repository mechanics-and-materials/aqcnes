// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementations to output relaxed thermodynamic quantities
 * @authors P.Gupta, S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "meshing/qc_mesh.hpp"
#include "solver/solver.hpp"
#include <boost/mpi.hpp>

namespace array_ops = qcmesh::array_ops;

namespace solver {

template <class Mesh> double local_energy(const Mesh &mesh) {
  double e_local{};
  for (std::size_t atom_index = 0;
       atom_index < mesh.sampling_atoms.nodal_locations.size() -
                        mesh.global_ghost_indices.size();
       atom_index++)
    e_local += mesh.sampling_atoms.nodal_energies[atom_index];
  for (std::size_t atom_index = 0;
       atom_index < mesh.sampling_atoms.central_locations.size(); atom_index++)
    e_local += mesh.sampling_atoms.central_energies[atom_index];
  return e_local;
}

template <class Mesh> double local_weight(const Mesh &mesh) {
  double w_local{};
  for (std::size_t atom_index = 0;
       atom_index < mesh.sampling_atoms.nodal_locations.size() -
                        mesh.global_ghost_indices.size();
       atom_index++)
    w_local += mesh.sampling_atoms.nodal_weights[atom_index];
  for (std::size_t atom_index = 0;
       atom_index < mesh.sampling_atoms.central_locations.size(); atom_index++)
    w_local += mesh.sampling_atoms.central_weights[atom_index];
  return w_local;
}

template <class Mesh> double local_k([[maybe_unused]] const Mesh &mesh) {
  double k_local{};
  if constexpr (std::is_same_v<typename Mesh::TemperatureMode,
                               meshing::FiniteT>) {
    for (std::size_t atom_index = 0;
         atom_index < mesh.sampling_atoms.nodal_locations.size() -
                          mesh.global_ghost_indices.size();
         atom_index++)
      k_local += exp(-mesh.repatoms.thermal_coordinates[atom_index][0]) *
                 mesh.sampling_atoms.nodal_weights[atom_index];
    for (std::size_t atom_index = 0;
         atom_index < mesh.sampling_atoms.central_locations.size();
         atom_index++)
      k_local += exp(-mesh.sampling_atoms.central_ido_fs[atom_index][0]) *
                 mesh.sampling_atoms.central_weights[atom_index];
  }
  return 1.50 * k_local;
}
template <class Mesh> double local_ts([[maybe_unused]] const Mesh &mesh) {
  double ts_local{};
  if constexpr (std::is_same_v<typename Mesh::TemperatureMode,
                               meshing::FiniteT>) {
    for (std::size_t atom_index = 0;
         atom_index < mesh.sampling_atoms.nodal_locations.size() -
                          mesh.global_ghost_indices.size();
         atom_index++)
      ts_local +=
          (-1.5 * exp(-mesh.repatoms.thermal_coordinates[atom_index][0]) *
           (mesh.repatoms.thermal_coordinates[atom_index][0] +
            mesh.repatoms.thermal_coordinates[atom_index][1])) *
          mesh.sampling_atoms.nodal_weights[atom_index];
    for (std::size_t atom_index = 0;
         atom_index < mesh.sampling_atoms.central_locations.size();
         atom_index++)
      ts_local +=
          (-1.5 * exp(-mesh.sampling_atoms.central_ido_fs[atom_index][0]) *
           (mesh.sampling_atoms.central_ido_fs[atom_index][0] +
            mesh.sampling_atoms.central_ido_fs[atom_index][1])) *
          mesh.sampling_atoms.central_weights[atom_index];
  }
  return ts_local;
}

template <class Mesh>
double total_local_impurity_concentration(const Mesh &mesh,
                                          const std::size_t species_idx) {
  auto total_conc = 0.0;
  for (std::size_t atom_index = 0;
       atom_index < mesh.repatoms.nodal_data_points.size() -
                        mesh.global_ghost_indices.size();
       atom_index++)
    total_conc += mesh.repatoms.nodal_weights[atom_index] *
                  mesh.repatoms.nodal_data_points[atom_index]
                      .impurity_concentrations[species_idx];
  return total_conc;
}

/**
 * @brief Print post-processed quantities
 */
template <std::size_t Dimension, std::size_t Noi>
void print_relaxed_info_to_files(
    solver::SolverVariables<Dimension, Noi> &solver_variables,
    const bool print_elapsed_time) {

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  constexpr bool FINITE_T = std::is_same_v<
      typename decltype(solver_variables.local_mesh)::TemperatureMode,
      meshing::FiniteT>;

  const auto t_str = FINITE_T ? std::to_string(solver_variables.input.T_ref)
                              : std::string{"0"};

  // printing boundary forces
  if (solver_variables.input.boundary_forces) {
    const auto file_boundary_forces =
        "./boundary_forces_" + solver_variables.materials[0].material_name +
        "_" + t_str + ".dat";

    std::ofstream f_boundary_forces;
    if (mpi_rank == 0) {
      f_boundary_forces.open(file_boundary_forces, std::ios::app);
    }

    for (const auto &boundary_condition :
         solver_variables.boundary_conditions) {
      auto global_boundary_force = geometry::Point<Dimension>{};
      for (std::size_t d = 0; d < Dimension; d++) {
        global_boundary_force[d] =
            qcmesh::mpi::all_reduce_sum(boundary_condition.recorded_force[d]);
      }

      if (mpi_rank == 0) {
        f_boundary_forces << array_ops::as_streamable(global_boundary_force)
                          << '\n';
      }
    }
    if (mpi_rank == 0) {
      f_boundary_forces << '\n';
      f_boundary_forces.close();
    }
  }

  // printing energy and volume info
  if (solver_variables.input.energy_volume) {

    // store thermalized energy in sampling atoms
    const auto e_global =
        qcmesh::mpi::all_reduce_sum(local_energy(solver_variables.local_mesh));
    const auto w_global =
        qcmesh::mpi::all_reduce_sum(local_weight(solver_variables.local_mesh));
    const auto k_global =
        qcmesh::mpi::all_reduce_sum(local_k(solver_variables.local_mesh));
    const auto ts_global =
        qcmesh::mpi::all_reduce_sum(local_ts(solver_variables.local_mesh));
    const auto f_global = e_global + k_global - ts_global;
    std::array<double, Noi> total_concs_global{};
    total_concs_global.fill(0.0);
    for (std::size_t si = 0; si < Noi; si++)
      total_concs_global[si] = qcmesh::mpi::all_reduce_sum(
          total_local_impurity_concentration(solver_variables.local_mesh, si));

    // store volume of the relaxed lattice
    const auto mesh_volume = solver_variables.local_mesh.volume();

    // Find new domain bounds
    solver_variables.local_mesh.domain_bound =
        geometry::distributed_bounding_box(
            solver_variables.local_mesh.repatoms.locations);

    const auto file_name_energy = "./energy_volume_" +
                                  solver_variables.materials[0].material_name +
                                  "_" + t_str + ".dat";

    if (mpi_rank == 0) {
      std::ofstream f_volume;
      f_volume.open(file_name_energy, std::ios::app);
      f_volume << std::setprecision(10);
      if (print_elapsed_time)
        f_volume << solver_variables.elapsed_conc_update_time << " ";

      f_volume << solver_variables.input.T_ref << " " << mesh_volume << " "
               << f_global << " " << e_global << " " << k_global << " "
               << w_global << " ";

      for (std::size_t si = 0; si < Noi; si++)
        f_volume << total_concs_global[si] << " ";

      f_volume << " " << solver_variables.local_mesh.periodic_lengths[0] << " "
               << solver_variables.local_mesh.periodic_lengths[1] << " "
               << solver_variables.local_mesh.periodic_lengths[2] << std::endl;
      f_volume.close();
    }
  }

  // printing final simulation domain size
  if (solver_variables.input.simulation_box_bounds) {
    const auto file_name_bounds = "./simulation_box_bounds_" +
                                  solver_variables.materials[0].material_name +
                                  "_" + t_str + ".dat";

    if (mpi_rank == 0) {
      std::ofstream f_bounds;
      f_bounds.open(file_name_bounds, std::ios::app);
      f_bounds << std::setprecision(10)
               << solver_variables.local_mesh.domain_bound.lower[0] << " "
               << solver_variables.local_mesh.domain_bound.lower[1] << " "
               << solver_variables.local_mesh.domain_bound.lower[2] << '\n'
               << (solver_variables.local_mesh.domain_bound.upper[0] -
                   solver_variables.local_mesh.domain_bound.lower[0])
               << " "
               << (solver_variables.local_mesh.domain_bound.upper[1] -
                   solver_variables.local_mesh.domain_bound.lower[1])
               << " "
               << (solver_variables.local_mesh.domain_bound.upper[2] -
                   solver_variables.local_mesh.domain_bound.lower[2])
               << std::endl;
      f_bounds.close();
    }
  }

  // ------------------------  Write per atom quantities in parallel ----------------------------
  if (solver_variables.input.per_atom_info) {

    const std::size_t n_local_nodal =
        solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
        solver_variables.local_mesh.global_ghost_indices.size();
    const auto n_local_central =
        solver_variables.local_mesh.sampling_atoms.central_locations.size();
    const auto n_local = n_local_nodal + n_local_central;

    constexpr std::size_t n_cols_atomic = FINITE_T ? (7 + 2 * Noi) : 5;

    std::vector<std::array<double, n_cols_atomic>> atomic_quantities;
    atomic_quantities.resize(n_local);
    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         atom_index++) {
      // load into vector
      atomic_quantities[atom_index][0] =
          solver_variables.local_mesh.sampling_atoms
              .nodal_locations[atom_index][0]; // x
      atomic_quantities[atom_index][1] =
          solver_variables.local_mesh.sampling_atoms
              .nodal_locations[atom_index][1]; // y
      atomic_quantities[atom_index][2] =
          solver_variables.local_mesh.sampling_atoms
              .nodal_locations[atom_index][2]; // z
      atomic_quantities[atom_index][3] =
          solver_variables.local_mesh.sampling_atoms
              .nodal_energies[atom_index]; // E
      if constexpr (FINITE_T) {
        atomic_quantities[atom_index][4] =
            1.50 *
            exp(-solver_variables.local_mesh.repatoms
                     .thermal_coordinates[atom_index][0]) *
            solver_variables.local_mesh.sampling_atoms
                .nodal_weights[atom_index]; // K atom
        atomic_quantities[atom_index][5] =
            (-1.5 *
             exp(-solver_variables.local_mesh.repatoms
                      .thermal_coordinates[atom_index][0]) *
             (solver_variables.local_mesh.repatoms
                  .thermal_coordinates[atom_index][0] +
              solver_variables.local_mesh.repatoms
                  .thermal_coordinates[atom_index][1])) *
            solver_variables.local_mesh.sampling_atoms
                .nodal_weights[atom_index]; // TS atom

        for (std::size_t impurity_idx = 0; impurity_idx < Noi; impurity_idx++) {
          atomic_quantities[atom_index][6 + impurity_idx] =
              solver_variables.local_mesh.repatoms.nodal_data_points[atom_index]
                  .chemical_potentials[impurity_idx];
        }
        for (std::size_t impurity_idx = 0; impurity_idx < Noi; impurity_idx++) {
          atomic_quantities[atom_index][7 + impurity_idx] =
              solver_variables.local_mesh.repatoms.nodal_data_points[atom_index]
                  .impurity_concentrations[impurity_idx];
        }

        atomic_quantities[atom_index][8] =
            solver_variables.local_mesh.sampling_atoms
                .nodal_weights[atom_index]; // w
      } else {
        atomic_quantities[atom_index][4] =
            solver_variables.local_mesh.sampling_atoms
                .nodal_weights[atom_index]; // w
      }
    }

    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.central_locations.size();
         atom_index++) {
      // load into vector
      const auto new_atom_index = atom_index + n_local_nodal;
      atomic_quantities[new_atom_index][0] =
          solver_variables.local_mesh.sampling_atoms
              .central_locations[atom_index][0];
      atomic_quantities[new_atom_index][1] =
          solver_variables.local_mesh.sampling_atoms
              .central_locations[atom_index][1];
      atomic_quantities[new_atom_index][2] =
          solver_variables.local_mesh.sampling_atoms
              .central_locations[atom_index][2];
      atomic_quantities[new_atom_index][3] =
          solver_variables.local_mesh.sampling_atoms
              .central_energies[atom_index];
      if constexpr (FINITE_T) {
        atomic_quantities[new_atom_index][4] =
            1.50 *
            exp(-solver_variables.local_mesh.sampling_atoms
                     .central_ido_fs[atom_index][0]) *
            solver_variables.local_mesh.sampling_atoms
                .central_weights[atom_index];
        // TS of atom
        atomic_quantities[new_atom_index][5] =
            (-1.5 *
             exp(-solver_variables.local_mesh.sampling_atoms
                      .central_ido_fs[atom_index][0]) *
             (solver_variables.local_mesh.sampling_atoms
                  .central_ido_fs[atom_index][0] +
              solver_variables.local_mesh.sampling_atoms
                  .central_ido_fs[atom_index][1])) *
            solver_variables.local_mesh.sampling_atoms
                .central_weights[atom_index];
        atomic_quantities[new_atom_index][6] =
            solver_variables.local_mesh.sampling_atoms
                .central_weights[atom_index];
      } else {
        atomic_quantities[new_atom_index][4] =
            solver_variables.local_mesh.sampling_atoms
                .central_weights[atom_index];
      }
    }

    boost::mpi::communicator local;

    std::vector<std::vector<std::array<double, n_cols_atomic>>> atomic_global;
    boost::mpi::gather<std::vector<std::array<double, n_cols_atomic>>>(
        local, atomic_quantities, atomic_global, 0);
    const auto file_per_atom =
        "./per_atom_quantities_" + solver_variables.materials[0].material_name +
        "_" + t_str + "_" +
        std::to_string(solver_variables.per_atom_info_file_counter) + ".dat";

    if (mpi_rank == 0) {
      std::ofstream f_peratom;
      f_peratom.open(file_per_atom, std::ios::app);
      f_peratom << "x_pos y_pos z_pos E ";
      if constexpr (FINITE_T)
        f_peratom
            << "K TS chemical_potential(s)[] impurity_concentration(s)[] ";
      f_peratom << "weight\n";
      for (const auto &atomic_global_i : atomic_global) {
        for (const auto &atomic_global_ij : atomic_global_i) {
          for (const auto val : atomic_global_ij)
            f_peratom << std::setprecision(10) << val << " ";
          f_peratom << std::endl;
        }
      }

      f_peratom.close();
    }
    solver_variables.per_atom_info_file_counter++;
  }
}

} // namespace solver
