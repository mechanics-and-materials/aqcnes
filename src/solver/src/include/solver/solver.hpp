// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Free function declarations for solver functions and solver class
  for PETSC. All variables are static variables of solver class
 * @authors P. Gupta, S. Saxena, M. Spinola
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "geometry/box.hpp"
#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "meshing/qc_mesh.hpp"
#include "neighbourhoods/get_entropy_rates.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "solver/calculate_deformation.hpp"
#include "solver/petsc_solver.hpp"
#include "solver/solver_variables.hpp"
#include "solver/time_integrators.hpp"
#include <boost/mpi.hpp>
#include <petscsnes.h>

namespace solver {

/**
 * @brief Can be used as the `dump_callback` argument for @ref composite_relax to dump nothing.
 */
class NullDump {};

class Solver {
public:
  template <std::size_t Dimension, std::size_t Nidof, std::size_t Noi>
  static void
  set_solver_dofs(SolverVariables<Dimension, Noi> &solver_variables) {

    solver_variables.n_var = Dimension;
// optimization is over sigma only
#ifdef FINITE_TEMPERATURE
    solver_variables.n_var += Nidof - 1;
#endif

    solver_variables.noof_local_dof =
        (solver_variables.local_mesh.nodes.size() -
         solver_variables.local_mesh.global_ghost_indices.size());

    solver_variables.noof_global_dof =
        meshing::count_repatoms(solver_variables.local_mesh);

    solver_variables.noof_ghost_dof =
        solver_variables.local_mesh.global_ghost_indices.size();

    // these are allocated again. We would have to
    // destroy the PetscVecs due to consecutive refining
    // and coarsening.

    solver_variables.noof_local_var =
        solver_variables.n_var * solver_variables.noof_local_dof;
    solver_variables.noof_global_var =
        solver_variables.n_var * solver_variables.noof_global_dof;
    solver_variables.noof_ghost_var =
        solver_variables.n_var * solver_variables.noof_ghost_dof;

    // non-dimensionalise the reference energy barrier
    solver_variables.input.reference_energy_barrier /=
        (constants::k_Boltzmann * solver_variables.input.T_ref);

    solver_variables.non_dimensionalizing_conc_rate =
        std::sqrt(constants::k_Boltzmann * solver_variables.input.T_ref /
                  (exp(-solver_variables.input.Phi_ref) *
                   solver_variables.materials[0].atomic_masses[0] *
                   constants::G_TO_EV_FS2_A2));
  }

  template <std::size_t Dimension, std::size_t Noi>
  static void
  create_solution_vectors(SolverVariables<Dimension, Noi> &solver_variables) {
    VecCreateGhost(MPI_COMM_WORLD, solver_variables.noof_local_var,
                   solver_variables.noof_global_var,
                   solver_variables.noof_ghost_var,
                   &solver_variables.local_mesh.repatoms.ghost_dof_indices[0],
                   &solver_variables.x_distributed);

    VecDuplicate(solver_variables.x_distributed,
                 &solver_variables.f_distributed);

#ifdef FINITE_TEMPERATURE
    VecCreateGhost(MPI_COMM_WORLD, solver_variables.noof_local_dof,
                   solver_variables.noof_global_dof,
                   solver_variables.noof_ghost_dof,
                   &solver_variables.local_mesh.global_ghost_indices[0],
                   &solver_variables.s_dot_distributed);
#endif
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof,
            std::size_t Noi>
  static void allocate_initial_neighbourhoods(
      SolverVariables<Dimension, Noi> &solver_variables) {
    // this function prepares all initial mesh overlaps and gets forces
    // initially, all sampling atoms are triggered for neighbourhoods.

    // we don't add neighbourhoods of ghost atoms (shared with a lesser rank)
    // so we can't trigger them locally. We have to exchange the triggers.
    // Use PETSC to do the exchange.

    solver_variables.triggered_nodal_sampling_atoms.clear();
    solver_variables.triggered_mesh_exchange_sampling_atoms.clear();
    solver_variables.triggered_periodic_mesh_exchange_sampling_atoms.clear();
    solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms
        .clear();
    solver_variables.triggered_cb_sampling_atoms.clear();

    solver_variables.triggered_nodal_sampling_atoms.reserve(
        solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
        solver_variables.local_mesh.global_ghost_indices.size());

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      solver_variables.triggered_nodal_sampling_atoms.push_back(node_index);
      solver_variables.local_mesh.sampling_atoms
          .nodal_sampling_atom_neighbours[node_index]
          .clear();
    }

    solver_variables.triggered_mesh_exchange_sampling_atoms.reserve(
        solver_variables.local_mesh.nodes.size());

    solver_variables.triggered_periodic_mesh_exchange_sampling_atoms.reserve(
        solver_variables.local_mesh.nodes.size());

    solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms
        .reserve(solver_variables.local_mesh.nodes.size());

    for (std::size_t node_index = 0;
         node_index < solver_variables.local_mesh.nodes.size(); node_index++) {
      if (solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .data.proc_boundary_flag) {
        solver_variables.triggered_mesh_exchange_sampling_atoms.push_back(
            node_index);
      }

      if (solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .data.periodic_boundary_flag) {
        solver_variables.triggered_periodic_mesh_exchange_sampling_atoms
            .push_back(node_index);
      }

      if (solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .data.proc_boundary_flag &&
          !solver_variables.local_mesh.nodes
               .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
               .data.periodic_boundary_flag) {
        solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms
            .push_back(node_index);
      }
    }

    solver_variables.triggered_cb_sampling_atoms.reserve(
        solver_variables.local_mesh.sampling_atoms.central_locations.size());

    for (std::size_t cb_index = 0;
         cb_index <
         solver_variables.local_mesh.sampling_atoms.central_locations.size();
         ++cb_index) {
      solver_variables.triggered_cb_sampling_atoms.push_back(cb_index);
      solver_variables.local_mesh.sampling_atoms
          .central_sampling_atom_neighbours[cb_index]
          .clear();
    }

    solver_variables.triggered_mesh_exchange_sampling_atoms.shrink_to_fit();
    solver_variables.triggered_periodic_mesh_exchange_sampling_atoms
        .shrink_to_fit();
    solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms
        .shrink_to_fit();
    update_repatom_forces<NodalData, Dimension, Nidof>(solver_variables);

    solver_variables.solver_iter = 0;
    solver_variables.last_output_iter = 0;
  }

  template <std::size_t Dimension, std::size_t Nidof, typename NodalData>
  static void update_mesh(
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables) {

    // deform the mesh from repatom positions
    // Make the marked elements zero volume. This
    // is required to make sure volume consistency is maintained
    // while repairing
    using QCMesh = meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>;

    deform_mesh_elements<QCMesh, Dimension>(solver_variables.local_mesh);

    // remesh/refine here. This might change
    // solver_variables.local_mesh_.repatoms, even global ghost indices, so
    // x_distributed_ and f_distributed_ will change.
    auto node_ctr = std::size_t{0};
    for (const auto &position : solver_variables.local_mesh.repatoms.locations)
      solver_variables.local_mesh.nodes
          .at(solver_variables.local_mesh.node_index_to_key[node_ctr++])
          .position = position;

    // update the domain bounds
    solver_variables.local_mesh.local_domain_bound =
        qcmesh::geometry::bounding_box(
            solver_variables.local_mesh.repatoms.locations);
    solver_variables.local_mesh.domain_bound = geometry::distributed_box_hull(
        solver_variables.local_mesh.local_domain_bound);

    // update mesh overlaps
    solver::deform_mesh_overlaps<QCMesh, Dimension, Nidof>(
        solver_variables.local_mesh);
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof>
  static void update_sampling_atoms(
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables) {

    // clear previously triggered nodal sampling atoms
    solver_variables.triggered_nodal_sampling_atoms.clear();

    // clear previously triggered cb sampling atomss
    solver_variables.triggered_cb_sampling_atoms.clear();

    solver_variables.triggered_mesh_exchange_sampling_atoms.clear();

    solver_variables.triggered_periodic_mesh_exchange_sampling_atoms.clear();

    solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms
        .clear();
    // deform neighbourhoods and check if some neighbourhoods need to be updated
    // update sampling atom locations and their neighbourhoods,
    // flag sampling atoms with neighbourhood relative displacements
    // for new neighbourhoods builds
    solver::deform_sampling_atoms_and_neighbourhoods<
        input::Userinput,
        meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension,
        Nidof, NodalData>(
        solver_variables.input, solver_variables.local_mesh,
        solver_variables.triggered_nodal_sampling_atoms,
        solver_variables.triggered_cb_sampling_atoms,
        solver_variables.triggered_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms,
        solver_variables.periodic_simulation_flag());

    if (solver_variables.input.regenerated_neighborhoods) {
      const auto mpi_rank = boost::mpi::communicator{}.rank();
      int global_neighbours_regenerated = 0;
      int global_cb_neighbours_regenerated = 0;
      int global_mesh_exchange_triggers = 0;
      int global_periodic_exchange_triggers = 0;

      global_neighbours_regenerated = qcmesh::mpi::all_reduce_sum(
          solver_variables.triggered_nodal_sampling_atoms.size());

      global_cb_neighbours_regenerated = qcmesh::mpi::all_reduce_sum(
          solver_variables.triggered_cb_sampling_atoms.size());

      global_mesh_exchange_triggers = qcmesh::mpi::all_reduce_sum(
          solver_variables.triggered_mesh_exchange_sampling_atoms.size());

      global_periodic_exchange_triggers = qcmesh::mpi::all_reduce_sum(
          solver_variables.triggered_periodic_mesh_exchange_sampling_atoms
              .size());

      if (mpi_rank == 0 && global_neighbours_regenerated > 0) {
        std::cout << "total nodal neighbourhoods regenerated "
                  << global_neighbours_regenerated << " , iter "
                  << solver_variables.relaxation_fire_parameters.iter
                  << std::endl;
      }

      if (mpi_rank == 0 && global_cb_neighbours_regenerated > 0) {
        std::cout << "total cb neighbourhoods generated "
                  << global_cb_neighbours_regenerated << " , iter "
                  << solver_variables.relaxation_fire_parameters.iter
                  << std::endl;
      }

      if (mpi_rank == 0 && global_mesh_exchange_triggers > 0) {
        std::cout << " total triggered mesh exchange atoms "
                  << global_mesh_exchange_triggers << " , iter "
                  << solver_variables.relaxation_fire_parameters.iter
                  << std::endl;
      }

      if (mpi_rank == 0 && global_periodic_exchange_triggers > 0) {
        std::cout << " total triggered periodic exchange atoms "
                  << global_periodic_exchange_triggers << " , iter "
                  << solver_variables.relaxation_fire_parameters.iter
                  << std::endl;
      }
    }
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof>
  static void update_repatom_forces(
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables) {
    neighbourhoods::get_required_neighbourhoods_add_forces<
        input::Userinput,
        meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>,
        boundarycondition::ExternalForceApplicator<Dimension>, Dimension, Nidof,
        NodalData>(
        solver_variables.input, solver_variables.local_mesh,
        solver_variables.triggered_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms,
        solver_variables.triggered_nodal_sampling_atoms,
        solver_variables.triggered_cb_sampling_atoms,
        solver_variables.external_force_applicator.get(),
        solver_variables.materials, solver_variables.apply_external_force,
        solver_variables.periodic_simulation_flag());

    exchange_interproc_forces<Dimension>(solver_variables.f_distributed,
                                         solver_variables);

    // put constrained direction forces to zero
    if (solver_variables.input.constrained_direction.has_value()) {
      for (auto &f : solver_variables.local_mesh.repatoms.forces)
        f[solver_variables.input.constrained_direction.value()] = 0.0;
    }

    // put fixed repatom forces to zero
    for (std::size_t repatom_idx = 0;
         repatom_idx < solver_variables.local_mesh.repatoms.locations.size();
         repatom_idx++)
      if (solver_variables.local_mesh.repatoms.if_fixed[repatom_idx])
        solver_variables.local_mesh.repatoms.velocities[repatom_idx].fill(0.0);

    // if fixed boundary force needs to be measured,
    // we have to store the force
    if (solver_variables.input.boundary_forces) {
      for (std::size_t boundary_ctr = 0;
           boundary_ctr < solver_variables.boundary_conditions.size();
           boundary_ctr++) {
        solver_variables.boundary_conditions[boundary_ctr].recorded_force.fill(
            0.0);
        solver_variables.boundary_conditions[boundary_ctr].record_force(
            solver_variables.local_mesh.repatoms.noof_ghosts,
            solver_variables.local_mesh.repatoms.locations,
            solver_variables.local_mesh.repatoms.forces);
      }
    }

    // boundary conditions modify F(x) making boundary normal force 0.
    // If all search steps have 0 component in normal direction,
    // no possible linear combination will have non-zero component
    for (std::size_t boundary_ctr = 0;
         boundary_ctr < solver_variables.boundary_conditions.size();
         boundary_ctr++) {
      // no update to thermal forces?
      solver_variables.boundary_conditions[boundary_ctr].update_vectors(
          solver_variables.local_mesh.repatoms.locations,
          solver_variables.local_mesh.repatoms.forces);
    }

    for (std::size_t iv = 0; iv < Dimension; iv++) {
      if (solver_variables.input.periodicity[iv].has_value() &&
          solver_variables.input.periodicity[iv]->relaxation_constraint) {
        solver_variables.local_mesh.repatoms.periodic_forces[iv] = 0.0;
      }
    }
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof>
  static void update_sampling_atom_energies(
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables) {
#ifdef FINITE_TEMPERATURE
    const auto use_gnn = std::holds_alternative<input::NNMaterialProperties>(
        solver_variables.input.material);
#endif
    neighbourhoods::compute_potential<
        meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension,
        Nidof, NodalData>(solver_variables.local_mesh,
                          solver_variables.materials
#ifdef FINITE_TEMPERATURE
                          ,
                          solver_variables.input.quad_type, use_gnn
#endif
    );

    double e_local = 0.0;
#ifdef FINITE_TEMPERATURE
    double k_local = 0.0;
    double k_global = 0.0;
#endif

    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         atom_index++) {
      e_local +=
          solver_variables.local_mesh.sampling_atoms.nodal_energies[atom_index];
#ifdef FINITE_TEMPERATURE
      k_local +=
          3.0 *
          exp(-solver_variables.local_mesh.repatoms
                   .thermal_coordinates[atom_index][0]) *
          solver_variables.local_mesh.sampling_atoms.nodal_weights[atom_index];
#endif
    }

    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.central_locations.size();
         atom_index++) {
      e_local += solver_variables.local_mesh.sampling_atoms
                     .central_energies[atom_index];
#ifdef FINITE_TEMPERATURE
      k_local += 1.5 *
                 exp(-solver_variables.local_mesh.sampling_atoms
                          .central_ido_fs[atom_index][0]) *
                 solver_variables.local_mesh.sampling_atoms
                     .central_weights[atom_index];
#endif
    }

    solver_variables.e_global = qcmesh::mpi::all_reduce_sum(e_local);
#ifdef FINITE_TEMPERATURE
    k_global = qcmesh::mpi::all_reduce_sum(k_local);
    solver_variables.e_global += k_global;
#endif
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof>
  static PetscErrorCode create_and_set_snes(
      const double abs_tolerance, const double rel_tolerance,
      const double s_tolerance, const std::size_t max_iter,
      const std::size_t max_f_eval,
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables) {

    solver_variables.reason = SNES_CONVERGED_ITERATING;
    solver_variables.petsc_abstol = abs_tolerance;
    solver_variables.petsc_rtol = rel_tolerance;
    solver_variables.petsc_stol = s_tolerance;

    solver_variables.petsc_max_iter = static_cast<int>(max_iter);
    solver_variables.petsc_max_f_eval = static_cast<int>(max_f_eval);

    PetscOptionsSetValue(nullptr, "-snes_type", "ngmres");
    // Number of stored previous solutions and residuals:
    PetscOptionsSetValue(nullptr, "-snes_ngmres_m", "2");
    // PetscOptionsSetValue(NULL, "-snes_monitor", "[ascii]");
    // difference based selection from candidate and combined iterates.
    // fast compared to others.
    PetscOptionsSetValue(nullptr, "-snes_ngmres_select_type", "difference");

    // Line search
    PetscOptionsSetValue(nullptr, "-snes_linesearch_type", "cp"); // cp, l2
    // The minimum step length:
    PetscOptionsSetValue(nullptr, "-snes_linesearch_minlambda", "0.01");
    // The linesearch damping parameter:
    PetscOptionsSetValue(nullptr, "-snes_linesearch_damping", "1.0");
    // The number of iterations for iterative line searches:
    PetscOptionsSetValue(nullptr, "-snes_linesearch_max_it", "3");

    // create petsc nonlinear solver context
    solver_variables.ierr = SNESCreate(MPI_COMM_WORLD, &solver_variables.snes);
    CHKERRQ(solver_variables.ierr);

    auto *solver_variables_ptr = &solver_variables;
    // set neighbourhood update and force calculation function
    solver_variables.ierr = SNESSetFunction(
        solver_variables.snes, solver_variables.f_distributed,
        &function_snes<NodalData, Dimension, Nidof>, solver_variables_ptr);
    // set line search options
    solver_variables.ierr =
        SNESGetLineSearch(solver_variables.snes, &solver_variables.linesearch);
    CHKERRQ(solver_variables.ierr);
    solver_variables.ierr = SNESSetFromOptions(solver_variables.snes);
    CHKERRQ(solver_variables.ierr);

    solver_variables.ierr = SNESSetTolerances(
        solver_variables.snes, solver_variables.petsc_abstol, PETSC_DEFAULT,
        PETSC_DEFAULT, solver_variables.petsc_max_iter,
        solver_variables.petsc_max_f_eval);
    CHKERRQ(solver_variables.ierr);

    return {};
  }

  template <std::size_t Dimension, std::size_t Noi>
  static PetscErrorCode
  destroy_snes(SolverVariables<Dimension, Noi> &solver_variables) {
    solver_variables.ierr = SNESDestroy(&solver_variables.snes);
    CHKERRQ(solver_variables.ierr);
    return 0;
  }

  template <std::size_t Dimension, std::size_t Noi>
  static void local_forces_to_distributed_vec(
      Vec &f_distributed, SolverVariables<Dimension, Noi> &solver_variables) {
    VecGhostGetLocalForm(f_distributed, &solver_variables.f_local);
    VecGetArray(solver_variables.f_local, &solver_variables.local_f_array);

    unwrap_repatom_forces_to_petsc_vec<Dimension>(solver_variables);

    VecRestoreArray(solver_variables.f_local, &solver_variables.local_f_array);
    VecGhostRestoreLocalForm(f_distributed, &solver_variables.f_local);

    VecGhostUpdateBegin(f_distributed, ADD_VALUES, SCATTER_REVERSE);
    VecGhostUpdateEnd(f_distributed, ADD_VALUES, SCATTER_REVERSE);

    VecGhostUpdateBegin(f_distributed, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(f_distributed, INSERT_VALUES, SCATTER_FORWARD);
  }

  template <std::size_t Dimension, std::size_t Noi>
  static void
  exchange_interproc_forces(Vec &f_distributed,
                            SolverVariables<Dimension, Noi> &solver_variables) {
    local_forces_to_distributed_vec<Dimension>(f_distributed, solver_variables);

    VecGhostUpdateBegin(f_distributed, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(f_distributed, INSERT_VALUES, SCATTER_FORWARD);

    VecGhostGetLocalForm(f_distributed, &solver_variables.f_local);
    VecGetArrayRead(solver_variables.f_local,
                    &solver_variables.local_f_array_read);

    wrap_petsc_vec_to_repatom_forces<Dimension>(solver_variables);

    VecRestoreArrayRead(solver_variables.f_local,
                        &(solver_variables.local_f_array_read));

    VecGhostRestoreLocalForm(f_distributed, &(solver_variables.f_local));
  }

  template <typename NodalData, std::size_t Dimension, std::size_t Nidof>
  static PetscErrorCode function_snes(SNES snes, Vec x_distributed,
                                      Vec f_distributed,
                                      void *solver_variables_void_ptr) {

    // from void ptr to SolverVariables ptr
    auto *solver_variables_ptr = static_cast<
        SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>> *>(
        solver_variables_void_ptr);

    auto &solver_variables = *solver_variables_ptr;

    /*!
            Do all of this only after first iteration.
            Use SNESGetIterationNumber to check
    */
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    SNESGetIterationNumber(snes, &solver_variables.petsc_nonlinear_iter);
    SNESGetLinearSolveIterations(snes, &solver_variables.petsc_linear_iter);

    // update x-vector locations (PETSC)
    VecGhostUpdateBegin(x_distributed, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(x_distributed, INSERT_VALUES, SCATTER_FORWARD);

    /****** <Get the values from distributed array> *********/
    // get local dofs from distributed vector
    VecGhostGetLocalForm(x_distributed, &solver_variables.x_local);
    VecGetArrayRead(solver_variables.x_local,
                    &solver_variables.local_x_array_read);

    // wrap them in repatom container (locations_ and thermal_coordinates_)
    // stores values in repatom locations from local_x_array
    wrap_petsc_vec_to_repatoms(solver_variables);

    // update Omega if isentropic relaxation and then just
    // operate using local_x_array_read. but what if we discover I have to
    // refine? So no!
    VecRestoreArrayRead(solver_variables.x_local,
                        &solver_variables.local_x_array_read);
    VecGhostRestoreLocalForm(x_distributed, &solver_variables.x_local);
    /****** <\Get the values from distributed array> *********/

    // update mesh
    // this is a big function. Besides computing deformations,
    // it can change the whole repatom container (positions and thermal DOFs)
    // It can add or delete nodes from mesh, which should
    // reflect in the repatom container and sample atom containers
    update_mesh<Dimension, Nidof, NodalData>(solver_variables);

    // If the update mesh remeshes across processors or refines,
    // then sampling atoms need to be created from scratch,
    // along with creation of repatom container. So in that case,
    // all nodal sampling atoms and central sampling atoms are
    // triggered for neighbours

    // if only local remesh happens, then repatom containers don't
    // need to change. Sampling atoms might need new neighbours if
    // remeshing happened within non-atomistic elements. So,
    // it would be best to remake all sampling atom and repatom
    // containers. Best thing to do would be, to make them as vectors.

    // deform neighbourhoods, and update sampling atoms.
    // this fills triggered_nodal_sampling_atoms_ with
    // sampling atoms markers that need new neighbours
    update_sampling_atoms<NodalData, Dimension, Nidof>(
        solver_variables); // updates triggered_nodal_sampling_atoms

    neighbourhoods::get_required_neighbourhoods_add_forces<
        input::Userinput,
        meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>,
        boundarycondition::ExternalForceApplicator<Dimension>, Dimension, Nidof,
        NodalData>(
        solver_variables.input, solver_variables.local_mesh,
        solver_variables.triggered_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_sampling_atoms,
        solver_variables.triggered_periodic_mesh_exchange_inner_sampling_atoms,
        solver_variables.triggered_nodal_sampling_atoms,
        solver_variables.triggered_cb_sampling_atoms,
        solver_variables.external_force_applicator.get(),
        solver_variables.materials, solver_variables.apply_external_force,
        solver_variables.periodic_simulation_flag());

    // boundary conditions modify F(x) making boundary normal force 0.
    // If all search steps have 0 component in normal direction,
    // no possible linear combination will have non-zero component
    // if fixed boundary force needs to be measured,
    // we have to store the force
    for (std::size_t boundary_ctr = 0;
         boundary_ctr < solver_variables.boundary_conditions.size();
         boundary_ctr++) {
      // no update to thermal forces?
      solver_variables.boundary_conditions[boundary_ctr].update_vectors(
          solver_variables.local_mesh.repatoms.locations,
          solver_variables.local_mesh.repatoms.forces);
    }

    /*!
            Unwrap repatom forces to distributed vector and add. Can't do this
       again
    */
    exchange_interproc_forces<Dimension>(f_distributed, solver_variables);

    // if we have fixed group of atoms, then we
    // only make forces in petscVec zero because
    // we have to store the forces

    solver_variables.residual =
        solver_variables.local_mesh.repatoms.force_residual();

    if (mpi_rank == 0) {
      std::cout << "petsc iter " << solver_variables.petsc_nonlinear_iter
                << ", Residual : " << solver_variables.residual << std::endl;
    }
    if ((solver_variables.solver_iter - solver_variables.last_output_iter) ==
        solver_variables.input.fire_dt_output_iter) {
      if (mpi_rank == 0) {
        std::cout << "written file at " << solver_variables.solver_iter
                  << std::endl;
      }
      solver_variables.last_output_iter = solver_variables.solver_iter;
    }

    solver_variables.solver_iter = solver_variables.input.fire_max_iter +
                                   solver_variables.petsc_nonlinear_iter;

    return 0;
  }

#ifdef FINITE_TEMPERATURE
  /**
   * @brief transport only till some time.
   */
  template <class DumpCallback, typename NodalData, std::size_t Dimension,
            std::size_t Nidof>
  static void dynamic_thermal_transport_ee(
      const double delta_t, const double a_ref,
      const double max_thermal_force_residue_per_atom,
      const double max_force_residue_per_atom,
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables,
      const DumpCallback dump_callback) {
    // before this function, the domain should be isentropically relaxed

    // _delta_t is the interval over which transport has to be considered
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    double ti_transport = 0.0;
    double time_transport{};
    double tf_transport{};

    time_transport = ti_transport;
    tf_transport = ti_transport + delta_t;

    // compute initial delta_t
    double dt_transport_limit{};
    double dt_transport{};

    double residual_heat_generation{};
    double residual_heat_tolerance{};
    residual_heat_generation = std::numeric_limits<double>::max();

    std::ofstream f_timeseries_transport;

    int transport_iter = 0;

    neighbourhoods::entropy_rate_onsager_kinetics<
        meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension,
        NodalData>(solver_variables.local_mesh, dt_transport_limit,
                   solver_variables.materials,
                   solver_variables.input.onsager_coefficient);

    exchange_interproc_scalars<Dimension>(
        solver_variables.local_mesh.repatoms.entropy_production_rates,
        solver_variables);

    solver_variables.local_mesh.repatoms.heat_rate_residual(
        residual_heat_generation);

    residual_heat_tolerance =
        solver_variables.input.heat_rate_tolerance_per_atom *
        double(meshing::count_repatoms(solver_variables.local_mesh));

    while (time_transport < tf_transport &&
           residual_heat_generation > residual_heat_tolerance &&
           (unsigned int)transport_iter <
               solver_variables.input.fire_max_iter) {

      dt_transport = solver_variables.input.safety_factor * dt_transport_limit;

      if (dt_transport >= delta_t) {
        dt_transport = delta_t;
      }

      if ((tf_transport - time_transport) < dt_transport)
        dt_transport = (tf_transport - time_transport);

      // one explicit step
      solver_variables.local_mesh.repatoms.update_entropy(dt_transport);

      if (mpi_rank == 0) {
        f_timeseries_transport.open("./TimeSeries.dat", std::ios::app);
        f_timeseries_transport
            << " time step : " << dt_transport
            << ", transport iter : " << transport_iter
            << ", residual heat generation : " << residual_heat_generation
            << ", residual heat tolerance : " << residual_heat_tolerance
            << std::endl;
        f_timeseries_transport.close();

        std::cout << "using time step : " << dt_transport << " , heat residual "
                  << residual_heat_generation << " ,heat tolerance "
                  << residual_heat_tolerance << std::endl;
      }

      // relaxation needs to be done after every entropy update so
      // Sigma needs to be readjusted using updated value of entropy
      // and Omega, so that relaxation can be triggered.
      update_mesh<Dimension, Nidof, NodalData>(solver_variables);

      update_sampling_atoms<NodalData, Dimension, Nidof>(solver_variables);

      update_repatom_forces<NodalData, Dimension, Nidof>(solver_variables);

      composite_relax<DumpCallback, NodalData, Dimension, Nidof>(
          a_ref, max_thermal_force_residue_per_atom, max_force_residue_per_atom,
          solver_variables, dump_callback);

      time_transport += dt_transport;
      transport_iter++;

      // calculates local entropy rate. Need to interchange for
      // shared nodes. Simply add the contributions
      neighbourhoods::entropy_rate_onsager_kinetics<
          meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension,
          NodalData>(solver_variables.local_mesh, dt_transport_limit,
                     solver_variables.materials,
                     solver_variables.input.onsager_coefficient);

      exchange_interproc_scalars<Dimension>(
          solver_variables.local_mesh.repatoms.entropy_production_rates,
          solver_variables);
      exchange_interproc_scalars<Dimension>(
          solver_variables.local_mesh.repatoms.dissipative_potential,
          solver_variables);

      solver_variables.local_mesh.repatoms.heat_rate_residual(
          residual_heat_generation);
    }
  }

#endif

  template <class DumpCallback, typename NodalData, std::size_t Dimension,
            std::size_t Nidof>
  static void fire_solve(
      const double a_ref, const double max_thermal_force_residue_per_atom,
      const double max_force_residue_per_atom,
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables,
      const DumpCallback dump_callback) {
    // if came in this function, then it will only leave after relaxing a bit

    solver_variables.relaxation_fire_parameters.stop = false;
    solver_variables.relaxation_fire_parameters.iter = 0;
    solver_variables.relaxation_fire_parameters.max_iterations =
        solver_variables.input.fire_max_iter;
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    solver_variables.local_mesh.repatoms.zero_velocities();
    update_sampling_atom_energies<NodalData, Dimension, Nidof>(
        solver_variables);
    while ((unsigned int)solver_variables.relaxation_fire_parameters.iter <
               solver_variables.relaxation_fire_parameters.max_iterations &&
           !solver_variables.relaxation_fire_parameters.stop) {
      // update repatom positions based on FIRE
      velocity_verlet_fire<
          meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension>(
          solver_variables.local_mesh,
#ifdef FINITE_TEMPERATURE
          solver_variables.isentropic,
#endif
          solver_variables.relaxation_fire_parameters.stop,
          max_thermal_force_residue_per_atom, max_force_residue_per_atom,
          solver_variables);

      const auto periodic_strains = detail::periodic_strains<Dimension>(
          a_ref, solver_variables.relaxation_fire_parameters.time_step,
          solver_variables.local_mesh.repatoms.periodic_velocities);
      if (mpi_rank == 0) {
        std::cout << " periodic strains were "
                  << array_ops::as_streamable(periodic_strains) << std::endl;
      }
#ifdef FINITE_TEMPERATURE
      if (!solver_variables.isentropic) {
        solver_variables.local_mesh.repatoms.update_equilibrium_entropy();
      }
#endif
      // adjust periods and repatom locations with periodic strains
      if (solver_variables.periodic_simulation_flag())
        deform_period_offsets<
            meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension,
            Nidof, NodalData>(solver_variables.local_mesh, periodic_strains);

      // update mesh using repatom locations
      update_mesh<Dimension, Nidof, NodalData>(solver_variables);

      // deform neighbourhoods, and update sampling atoms.
      // this fills triggered_nodal_sampling_atoms_ with
      // sampling atoms markers that need new neighbours
      update_sampling_atoms<NodalData, Dimension, Nidof>(
          solver_variables); // updates triggered_nodal_sampling_atoms_OLD

      // compute new required neighbours and forces
      update_repatom_forces<NodalData, Dimension, Nidof>(solver_variables);

      update_sampling_atom_energies<NodalData, Dimension, Nidof>(
          solver_variables);

      // Print non-centrosymmetric neighborhoods if requested
      if (solver_variables.input.non_centrosymmetric_neighborhoods_frequency
              .has_value() &&
          (solver_variables.relaxation_fire_parameters.iter %
               solver_variables.input
                   .non_centrosymmetric_neighborhoods_frequency.value() ==
           0)) {
        solver_variables.local_mesh.compute_nodal_centro_symmetry();
        neighbourhoods::save_non_centrosymmetric_neighborhoods<
            meshing::QCMesh<Dimension, Dimension, Nidof, NodalData>, Dimension>(
            solver_variables.local_mesh);
      }

      // Output mid fire solution files if requested
      if (solver_variables.input.mid_fire_solution_files_frequency
              .has_value() &&
          (solver_variables.relaxation_fire_parameters.iter %
               solver_variables.input.mid_fire_solution_files_frequency
                   .value() ==
           0)) {

        if (solver_variables.input.output_centro_symmetry) {
          solver_variables.local_mesh.compute_nodal_centro_symmetry();
        }

        if constexpr (std::is_constructible<
                          std::function<void(
                              SolverVariables<
                                  Dimension,
                                  meshing::traits::N_IMPURITIES<NodalData>> &)>,
                          DumpCallback>::value)
          dump_callback(solver_variables);
      }

      solver_variables.relaxation_fire_parameters.iter++;
      solver_variables.solver_iter++;
    }
  }

  /**
   * @brief starts with robust FIRE based relaxation, then calls petsc
   */
  template <class DumpCallback, typename NodalData, std::size_t Dimension,
            std::size_t Nidof>
  static void composite_relax(
      const double a_ref, const double max_thermal_force_residue_per_atom,
      const double max_force_residue_per_atom,
      SolverVariables<Dimension, meshing::traits::N_IMPURITIES<NodalData>>
          &solver_variables,
      const DumpCallback dump_callback) {
    solver_variables.solver_iter = 0;
    solver_variables.last_output_iter = 0;
    // make sure to store and add forces before this function is called.
    // also, come into this function with all the apply boundary/external force
    // flags sorted out.
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    solver_variables.residual =
        solver_variables.local_mesh.repatoms.force_residual();

    // first relax with FIRE.
    // compute ||r||.  If ||r|| > min_residual, FIRE relaxation
    // for fire_max_iter steps.

    double pre_fire_residual{};

    if (mpi_rank == 0) {
      std::cout << "Force residual is " << solver_variables.residual
                << std::endl;
    }

    // initialize FIRE parameters changed during previous relaxations
    solver_variables.relaxation_fire_parameters.time_step =
        solver_variables.input.fire_time_step;
    solver_variables.relaxation_fire_parameters.alpha =
        solver_variables.input.fire_initial_alpha;
    solver_variables.relaxation_fire_parameters.steps_post_reset =
        solver_variables.input.fire_steps_post_reset;

    pre_fire_residual = solver_variables.residual;
    {
      if (mpi_rank == 0)
        std::cout << "beginning FIRE. Residual is " << solver_variables.residual
                  << std::endl;

      fire_solve<DumpCallback, NodalData, Dimension, Nidof>(
          a_ref, max_thermal_force_residue_per_atom, max_force_residue_per_atom,
          solver_variables, dump_callback);

      solver_variables.residual =
          solver_variables.local_mesh.repatoms.force_residual();

      if (mpi_rank == 0)
        std::cout << "finished VERLET-FIRE. Force residual is "
                  << solver_variables.residual << std::endl;
    }
    /*!
   *      ||r|| should have decreased. Best case ||r|| < min_residual.
   *      worst case ||r||>||r||_0.
   *      Somethings might be wrong with relaxation.
   *       Don't try petscwith a warning. Most number of times (avg case),
   *       min_residual<||r||<||r||_0. When a dislocation is moving,
   *
   *       ||r|| > ||r||_0 can be observed due to insufficient number of FIRE
   *       iterations. Just move to next loadstep and eventually, it will
   *  converge
  */

    if (solver_variables.residual <= pre_fire_residual) {
      // do PETSC solve
      solve<Dimension>(solver_variables);

      solver_variables.residual =
          solver_variables.local_mesh.repatoms.force_residual();

      if (mpi_rank == 0)
        std::cout << "finished Petsc Solve. Force residual is "
                  << solver_variables.residual << std::endl;

      // update x-vector locations (PETSC)
      VecGhostUpdateBegin(solver_variables.x_distributed, INSERT_VALUES,
                          SCATTER_FORWARD);
      VecGhostUpdateEnd(solver_variables.x_distributed, INSERT_VALUES,
                        SCATTER_FORWARD);

      // **** <Get the final solution from distributed array> *******
      // get local dofs from distributed vector
      VecGhostGetLocalForm(solver_variables.x_distributed,
                           &solver_variables.x_local);
      VecGetArrayRead(solver_variables.x_local,
                      &solver_variables.local_x_array_read);

      wrap_petsc_vec_to_repatoms<Dimension>(solver_variables);

      VecRestoreArrayRead(solver_variables.x_local,
                          &solver_variables.local_x_array_read);
      VecGhostRestoreLocalForm(solver_variables.x_distributed,
                               &solver_variables.x_local);
      /****** <\Get the final solution from distributed array> *********/
    } else {
      if (mpi_rank == 0)
        std::cout
            << "Didn't do petsc Solve because FIRE made it worse"
            << " Possibly a dislocation was moving. We'll continue and hope"
            << " residual decreases next time " << solver_variables.residual
            << std::endl;
    }

    update_mesh<Dimension, Nidof, NodalData>(solver_variables);
    // get final neighbourhoods

    // deform neighbourhoods, and update sampling atoms.
    // this fills triggered_nodal_sampling_atoms_ with
    // sampling atoms markers that need new neighbours
    update_sampling_atoms<NodalData, Dimension, Nidof>(
        solver_variables); // updates triggered_nodal_sampling_atoms_OLD

    update_repatom_forces<NodalData, Dimension, Nidof>(solver_variables);

    solver_variables.residual =
        solver_variables.local_mesh.repatoms.force_residual();

    solver_variables.residual_after_last_relaxation = solver_variables.residual;

    if (mpi_rank == 0) {
      std::cout << "force residual before reinitializing all neighbours "
                << solver_variables.residual << std::endl;
    }

    // update periodic lengths from the relaxed periodic offsets
    if (solver_variables.periodic_simulation_flag()) {
      for (std::size_t d = 0; d < Dimension; d++)
        solver_variables.local_mesh.periodic_lengths[d] = std::abs(
            solver_variables.local_mesh.periodic_offsets
                [solver_variables.local_mesh.periodic_offsets.size() / 2][d]);
    }
  }
};

} // namespace solver
