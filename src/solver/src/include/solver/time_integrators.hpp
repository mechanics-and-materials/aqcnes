// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief function implementations for each iteration of the FIRE solver
 * @authors P. Gupta, S. Saxena
 */

#pragma once
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/solver_variables.hpp"
#include <fstream>
#include <iostream>

namespace solver {

namespace array_ops = qcmesh::array_ops;

template <bool FiniteTemperature, std::size_t Dimension, std::size_t Noi>
void velocity_verlet_fire_infmodel(
    const std::array<double, 2 * Dimension> &periodic_forces,
    std::array<double, 2 * Dimension> &periodic_velocities,
    std::array<double, 2 * Dimension> &periodic_strains, const double a_ref,
    [[maybe_unused]] const double thermal_force,
    [[maybe_unused]] double &thermal_velocity, const double total_mass,
    bool &to_stop, SolverVariables<Dimension, Noi> &solver_variables) {

  std::ofstream f_timeseries;
  std::cout << " periodic forces were "
            << array_ops::as_streamable(periodic_forces) << std::endl;

  if constexpr (FiniteTemperature)
    thermal_velocity += 0.5 *
                        solver_variables.relaxation_fire_parameters.time_step *
                        thermal_force / total_mass;

  for (std::size_t i = 0; i < (2 * Dimension); i++)
    periodic_velocities[i] +=
        0.5 * solver_variables.relaxation_fire_parameters.time_step *
        periodic_forces[i] / total_mass;

  std::array<double, 3> periodic_diagnostics{};
  periodic_diagnostics.fill(0.0);
  double sq_periodic_force = 0.0;

  bool thermal_condition_lower = false;
  bool thermal_condition_2 = false;
  [[maybe_unused]] auto thermal_diagnostics = std::array<double, 3>{};
  if constexpr (FiniteTemperature) {
    constexpr double max_thermal_force_residue_per_atom = 1.0e-6;
    thermal_diagnostics = std::array<double, 3>{
        thermal_force * thermal_velocity, std::abs(thermal_force),
        max_thermal_force_residue_per_atom};
    thermal_condition_lower =
        (thermal_diagnostics[1] <= thermal_diagnostics[2]);
    thermal_condition_2 = (thermal_diagnostics[0] >= -1.0e-8);
  } else {
    thermal_condition_lower = true;
    thermal_condition_2 = true;
  }

  for (std::size_t i = 0; i < (2 * Dimension); i++) {
    periodic_diagnostics[0] += periodic_forces[i] * periodic_velocities[i];
    sq_periodic_force += periodic_forces[i] * periodic_forces[i];
  }

  periodic_diagnostics[1] += sqrt(sq_periodic_force);

  periodic_diagnostics[2] +=
      solver_variables.input.max_periodic_force_residue_per_atom;

  double velocity_mag{};
  double force_mag{};
  //Check if cease FIRE, or continue
  if (periodic_diagnostics[1] <= periodic_diagnostics[2] &&
      thermal_condition_lower) {
    to_stop = true;
  } else {
    if constexpr (FiniteTemperature) {
      if (std::abs(thermal_force) > constants::force_tolerance)
        thermal_velocity =
            thermal_velocity *
                (1.0 - solver_variables.relaxation_fire_parameters.alpha) +
            (thermal_force / std::abs(thermal_force)) *
                solver_variables.relaxation_fire_parameters.alpha *
                std::abs(thermal_velocity);
      else
        thermal_velocity = 0.0;
    }

    for (std::size_t i = 0; i < (2 * Dimension); i++) {

      force_mag = abs(periodic_forces[i]);
      velocity_mag = abs(periodic_velocities[i]);

      if (force_mag > constants::force_tolerance) {
        periodic_velocities[i] =
            periodic_velocities[i] *
                (1.0 - solver_variables.relaxation_fire_parameters.alpha) +
            (periodic_forces[i] / force_mag) *
                solver_variables.relaxation_fire_parameters.alpha *
                velocity_mag;
      }
    }
  }

  // +=+=+=+=<adjust time step for changing things based on v(t)>
  // +=+=+=+=+=+//
  if (periodic_diagnostics[0] >= -1.0e-8 && thermal_condition_2) {
    if (solver_variables.relaxation_fire_parameters.time_step <
            solver_variables.input.fire_max_time_step &&
        solver_variables.relaxation_fire_parameters.steps_post_reset >=
            solver_variables.input.fire_min_steps_reset) {
      solver_variables.relaxation_fire_parameters.time_step =
          std::min(solver_variables.relaxation_fire_parameters.time_step *
                       solver_variables.input.fire_f_increase_t,
                   solver_variables.input.fire_max_time_step);
      solver_variables.relaxation_fire_parameters.alpha =
          solver_variables.relaxation_fire_parameters.alpha *
          solver_variables.input.fire_f_alpha;
    } else if (solver_variables.relaxation_fire_parameters.steps_post_reset <
               solver_variables.input.fire_min_steps_reset) {
      solver_variables.relaxation_fire_parameters.steps_post_reset++;
    }
  } else {
    solver_variables.relaxation_fire_parameters.time_step *=
        solver_variables.input.fire_f_decrease_t;
    solver_variables.relaxation_fire_parameters.alpha =
        solver_variables.input.fire_initial_alpha;
    solver_variables.relaxation_fire_parameters.steps_post_reset = 0;

    if constexpr (FiniteTemperature)
      thermal_velocity = 0.0;

    for (std::size_t i = 0; i < (2 * Dimension); i++)
      periodic_velocities[i] = 0.0;
  }

  if (solver_variables.input.relaxation_steps) {
    std::cout << "-------------------------------------------------"
              << std::endl;
    std::cout << "relaxation iteration : "
              << solver_variables.relaxation_fire_parameters.iter
              << " , time step : "
              << solver_variables.relaxation_fire_parameters.time_step
              << " , alpha = "
              << solver_variables.relaxation_fire_parameters.alpha << std::endl;

    if constexpr (FiniteTemperature)
      std::cout << "thermal power : " << thermal_diagnostics[0]
                << " , thermal error : " << thermal_diagnostics[1] << "/"
                << thermal_diagnostics[2] << std::endl;
    std::cout << "periodic power : " << periodic_diagnostics[0]
              << " , periodic error : " << periodic_diagnostics[1] << "/"
              << periodic_diagnostics[2] << std::endl;
  }

  f_timeseries.open("./TimeSeries.dat", std::ios::app);
  f_timeseries << solver_variables.relaxation_fire_parameters.iter << " "
               << solver_variables.relaxation_fire_parameters.t;
  if constexpr (FiniteTemperature)
    f_timeseries << " " << thermal_diagnostics[1] << " "
                 << thermal_diagnostics[2];
  f_timeseries << " " << periodic_diagnostics[1] << " "
               << periodic_diagnostics[2] << " "
               << solver_variables.relaxation_fire_parameters.time_step << " "
               << solver_variables.relaxation_fire_parameters.alpha << " "
               << std::endl;
  f_timeseries.close();

  if constexpr (FiniteTemperature)
    thermal_velocity += 0.5 *
                        solver_variables.relaxation_fire_parameters.time_step *
                        thermal_force / total_mass;

  for (std::size_t i = 0; i < (2 * Dimension); i++)
    periodic_velocities[i] +=
        0.5 * solver_variables.relaxation_fire_parameters.time_step *
        periodic_forces[i] / total_mass;

  // periodic strains are \gamma(t). New period length l(t) will be
  // l(t) = (1.0 + gamma(t))*l(t-1)
  for (std::size_t i = 0; i < (2 * Dimension); i++)
    periodic_strains[i] =
        periodic_velocities[i] *
        solver_variables.relaxation_fire_parameters.time_step / a_ref;

  std::cout << " periodic strains were "
            << array_ops::as_streamable(periodic_strains) << std::endl;
}

template <std::size_t Nidof>
void update_thermal_coordinates(std::array<double, Nidof> &thermal_coordinates,
                                const double thermal_velocity,
                                const bool isentropic,
                                const double fire_time_step) {
  // if sigma_old is too low (mean phi was too high,
  // then use sigma_ref to scale the phi force).
  // We need extra force against very large negative phi
  const auto sigma_old = exp(-thermal_coordinates[1]);
  const auto phi_new = thermal_coordinates[1] +
                       (-thermal_velocity / (sigma_old)) * fire_time_step;

  if (isentropic) {
    thermal_coordinates[0] =
        (thermal_coordinates[0] + thermal_coordinates[1]) - phi_new;
    thermal_coordinates[1] = phi_new;

  } else
    thermal_coordinates[1] = phi_new;
}

template <typename Mesh, std::size_t Dimension, std::size_t Noi>
void velocity_verlet_fire(Mesh &local_mesh,
#ifdef FINITE_TEMPERATURE
                          const bool &isentropic,
#endif
                          bool &to_stop,
                          const double
#ifdef FINITE_TEMPERATURE
                              max_thermal_force_residue_per_atom
#endif
                          ,
                          const double max_force_residue_per_atom,
                          SolverVariables<Dimension, Noi> &solver_variables) {
  std::ofstream f_timeseries;
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  const auto mpi_size = boost::mpi::communicator{}.size();

#ifdef FINITE_TEMPERATURE
  double sigma_old{};
  double phi_new{};
#endif
  if (mpi_rank == 0)
    std::cout << " periodic forces were "
              << array_ops::as_streamable(local_mesh.repatoms.periodic_forces)
              << std::endl;

  // +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
  // entering here at t = t_old + dt_old, the velocity we have is
  // v(t-dt/2).
  // 1. Calculate v(t) = v(t_old+dt_old/2) + (dt_old/2)*(F(t_old+dt_old)/m)
  // 2. Calculate P(t) = v(t).F(t) and |F(t)|_max
  // 3. Mix v(t) with F(t), and adjust dt to dt_new
  // 4. Get v(t+dt_new/2) and x(t + dt_new), t_new = t + dt_new
  // 5. Repeat
  // +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//

  // +=+=+=+=+=+=+=+=+=+=+=+=+ <Get v(t)> +=+=+=+=+=+=+=+=+=+=+=+=+//
  for (std::size_t node_index = 0;
       node_index < local_mesh.repatoms.forces.size(); node_index++) {
    if (solver_variables.input.nan_forces) {
      if (std::isnan(array_ops::norm(local_mesh.repatoms.forces[node_index]))
#ifdef FINITE_TEMPERATURE
          or std::isnan(local_mesh.repatoms.thermal_forces[node_index])
#endif
      ) {
        std::cout << "rank " << mpi_rank
                  << " detected NaN force on a repatom. Repatom id "
                  << node_index << " , current location : "
                  << array_ops::as_streamable(
                         local_mesh.repatoms.locations[node_index])
                  << " , " << local_mesh.repatoms.mass[node_index]
                  << " , physical force"
                  << array_ops::as_streamable(
                         local_mesh.repatoms.forces[node_index])
#ifdef FINITE_TEMPERATURE
                  << " , thermal force "
                  << local_mesh.repatoms.thermal_forces[node_index]
                  << " , thermal coordinates "
                  << array_ops::as_streamable(
                         local_mesh.repatoms.thermal_coordinates[node_index])
#endif
                  << std::endl;

        throw exception::QCException("Detected a Nan force. Exiting...");
      }
    }

    // v(t) = v(t-dt/2) + (dt/2)*(F/m)
    array_ops::as_eigen(local_mesh.repatoms.velocities[node_index]) +=
        0.5 * solver_variables.relaxation_fire_parameters.time_step *
        array_ops::as_eigen(local_mesh.repatoms.forces[node_index]) /
        local_mesh.repatoms.mass[node_index];

#ifdef FINITE_TEMPERATURE
    local_mesh.repatoms.thermal_velocities[node_index] +=
        0.5 * solver_variables.relaxation_fire_parameters.time_step *
        local_mesh.repatoms.thermal_forces[node_index] /
        local_mesh.repatoms.mass[node_index];
    ;
#endif
  }

#ifdef CHECK_FOR_NAN
  if (std::isnan(array_ops::norm(local_mesh.repatoms.periodic_forces))) {
    std::cout << "detected NaN periodic force (same by all ranks) "
              << array_ops::as_streamable(local_mesh.repatoms.periodic_forces)
              << std::endl;

    throw exception::QCException("Detected a Nan periodic force. Exiting...");
  }
#endif

  array_ops::as_eigen(local_mesh.repatoms.periodic_velocities) +=
      0.5 * solver_variables.relaxation_fire_parameters.time_step *
      array_ops::as_eigen(local_mesh.repatoms.periodic_forces) /
      local_mesh.repatoms.total_mass;

  // now we have v(t). We have to do mixing on v(t).

  // +=+=+=+=+=+=+=+=+=+=+=+=+ </Get v(t)> +=+=+=+=+=+=+=+=+=+=+=+=+//

  // +=+=+=+=<compute diagnostics (power and total force magnitude)>
  // +=+=+=+=+=+//
  std::array<double, 3> local_diagnostics{};
  local_diagnostics.fill(0.0);

#ifdef FINITE_TEMPERATURE
  std::array<double, 3> local_thermal_diagnostics{};
  local_thermal_diagnostics.fill(0.0);
#endif

  std::array<double, 3> local_periodic_diagnostics{};
  local_periodic_diagnostics.fill(0.0);

  for (std::size_t node_index = 0;
       node_index < local_mesh.repatoms.forces.size() -
                        local_mesh.global_ghost_indices.size();
       node_index++) {
    local_diagnostics[0] +=
        array_ops::dot(local_mesh.repatoms.forces[node_index],
                       local_mesh.repatoms.velocities[node_index]);

#ifdef FINITE_TEMPERATURE
    local_thermal_diagnostics[0] +=
        local_mesh.repatoms.thermal_forces[node_index] *
        local_mesh.repatoms.thermal_velocities[node_index];
#endif

    // add error due to all repatoms. This is free relaxation
    local_diagnostics[1] +=
        array_ops::norm(local_mesh.repatoms.forces[node_index]) /
        (local_mesh.repatoms.nodal_weights[node_index]);

    local_diagnostics[2] += max_force_residue_per_atom;

#ifdef FINITE_TEMPERATURE

    local_thermal_diagnostics[1] +=
        abs(local_mesh.repatoms.thermal_forces[node_index]) /
        (local_mesh.repatoms.nodal_weights[node_index]);

    local_thermal_diagnostics[2] += max_thermal_force_residue_per_atom;
#endif
  }

  // add period relaxation force to diagnostics as well
  local_periodic_diagnostics[0] +=
      array_ops::dot(local_mesh.repatoms.periodic_forces,
                     local_mesh.repatoms.periodic_velocities) /
      double(mpi_size);
  local_periodic_diagnostics[1] +=
      array_ops::norm(local_mesh.repatoms.periodic_forces) /
      (double(mpi_size) * local_mesh.repatoms.total_weights);

  local_periodic_diagnostics[2] +=
      solver_variables.input.max_periodic_force_residue_per_atom /
      double(mpi_size);

  std::array<double, 3> global_diagnostics{};
  global_diagnostics.fill(0.0);

  const auto world = boost::mpi::communicator{};
  boost::mpi::all_reduce(world, local_diagnostics.data(), 3,
                         global_diagnostics.data(), std::plus<>());

#ifdef FINITE_TEMPERATURE
  std::array<double, 3> global_thermal_diagnostics{};
  global_thermal_diagnostics.fill(0.0);
  boost::mpi::all_reduce(world, local_thermal_diagnostics.data(), 3,
                         global_thermal_diagnostics.data(), std::plus<>());
#endif
  std::array<double, 3> global_periodic_diagnostics{};
  global_periodic_diagnostics.fill(0.0);
  boost::mpi::all_reduce(world, local_periodic_diagnostics.data(), 3,
                         global_periodic_diagnostics.data(), std::plus<>());

  // +=+=+=+=</compute diagnostics (power and total force magnitude)>
  // +=+=+=+=+=+//

  // +=+=+=+=<Check if residue was lower than minimum. Nothing to do if yes>
  // +=+=+=+=+=+//

  if (global_diagnostics[1] <= global_diagnostics[2]
#ifdef FINITE_TEMPERATURE
      && global_thermal_diagnostics[1] <= global_thermal_diagnostics[2]
#endif
      && global_periodic_diagnostics[1] <= global_periodic_diagnostics[2]) {
    // CEASE FIRE! CEASE FIRE! CEASE FIRE!
    to_stop = true;
  } else {
    // +=+=+=+=<Fire based mixing of v(t)> +=+=+=+=+=+//
    double velocity_mag{};
    double force_mag{};

    // v(t) <- (1.0 - solver_variables.relaxation_fire_parameters.alpha)*v(t) + alpha *F(t)*|v(t)|/|F(t)|

    // for(repatom &atom : _repatoms)
    for (std::size_t node_index = 0;
         node_index < local_mesh.repatoms.forces.size(); node_index++) {
      velocity_mag =
          array_ops::norm(local_mesh.repatoms.velocities[node_index]);

      force_mag = array_ops::norm(local_mesh.repatoms.forces[node_index]);

      // Force tolerance for atoms. Forces below this magnitude
      // are considered 0
      if (force_mag > constants::force_tolerance) {
        array_ops::as_eigen(local_mesh.repatoms.velocities[node_index]) =
            array_ops::as_eigen(local_mesh.repatoms.velocities[node_index]) *
                (1.0 - solver_variables.relaxation_fire_parameters.alpha) +
            (array_ops::as_eigen(local_mesh.repatoms.forces[node_index]) /
             force_mag) *
                solver_variables.relaxation_fire_parameters.alpha *
                velocity_mag;
      } else {
        local_mesh.repatoms.velocities[node_index].fill(0.0);
      }

#ifdef FINITE_TEMPERATURE
      force_mag = abs(local_mesh.repatoms.thermal_forces[node_index]);
      velocity_mag = abs(local_mesh.repatoms.thermal_velocities[node_index]);

      if (abs(local_mesh.repatoms.thermal_forces[node_index]) >
          constants::force_tolerance) {
        local_mesh.repatoms.thermal_velocities[node_index] =
            local_mesh.repatoms.thermal_velocities[node_index] *
                (1.0 - solver_variables.relaxation_fire_parameters.alpha) +
            (local_mesh.repatoms.thermal_forces[node_index] / force_mag) *
                solver_variables.relaxation_fire_parameters.alpha *
                velocity_mag;
      } else {
        local_mesh.repatoms.thermal_velocities[node_index] = 0.0;
      }

#endif
    }

    // do periodic velocity modification for all directions separately for all directions
    for (std::size_t i_period = 0; i_period < Dimension; i_period++) {
      if (local_mesh.periodic_flags[i_period]) {
        force_mag = abs(local_mesh.repatoms.periodic_forces[i_period]);
        velocity_mag = abs(local_mesh.repatoms.periodic_velocities[i_period]);

        if (force_mag > constants::force_tolerance) {
          local_mesh.repatoms.periodic_velocities[i_period] =
              local_mesh.repatoms.periodic_velocities[i_period] *
                  (1.0 - solver_variables.relaxation_fire_parameters.alpha) +
              (local_mesh.repatoms.periodic_forces[i_period] / force_mag) *
                  solver_variables.relaxation_fire_parameters.alpha *
                  velocity_mag;
        }
      }
    }

    // forces in appropriate directions are always zero acc. to boundaries
    // hence velocity will never be in that direction. If not, check the Update
    // function in boundary conditions classes

    // +=+=+=+=</Fire based mixing of v(t)> +=+=+=+=+=+//

    // +=+=+=+=<adjust time step for changing things based on v(t)>
    // +=+=+=+=+=+//
    if (global_diagnostics[0] >= -1.0e-8
#ifdef FINITE_TEMPERATURE
        && global_thermal_diagnostics[0] >= -1.0e-8
#endif
        && global_periodic_diagnostics[0] >= -1.0e-8) {
      if (solver_variables.relaxation_fire_parameters.time_step <
              solver_variables.input.fire_max_time_step &&
          solver_variables.relaxation_fire_parameters.steps_post_reset >=
              solver_variables.input.fire_min_steps_reset) {
        solver_variables.relaxation_fire_parameters.time_step =
            std::min(solver_variables.relaxation_fire_parameters.time_step *
                         solver_variables.input.fire_f_increase_t,
                     solver_variables.input.fire_max_time_step);
        solver_variables.relaxation_fire_parameters.alpha =
            solver_variables.relaxation_fire_parameters.alpha *
            solver_variables.input.fire_f_alpha;
      } else if (solver_variables.relaxation_fire_parameters.steps_post_reset <
                 solver_variables.input.fire_min_steps_reset) {
        solver_variables.relaxation_fire_parameters.steps_post_reset++;
      }
    } else {
      solver_variables.relaxation_fire_parameters.time_step *=
          solver_variables.input.fire_f_decrease_t;
      solver_variables.relaxation_fire_parameters.alpha =
          solver_variables.input.fire_initial_alpha;
      solver_variables.relaxation_fire_parameters.steps_post_reset = 0;
      for (std::size_t node_index = 0;
           node_index < local_mesh.repatoms.velocities.size(); node_index++) {
        local_mesh.repatoms.velocities[node_index].fill(0.0);
#ifdef FINITE_TEMPERATURE
        local_mesh.repatoms.thermal_velocities[node_index] = 0.0;
#endif
      }
      local_mesh.repatoms.periodic_velocities.fill(0.0);
    }

    const auto force_residual = local_mesh.repatoms.force_residual();

    // ************** Write the time, power of atoms, maximum force per real
    // atom
    if (mpi_rank == 0) {
      if (solver_variables.input.relaxation_steps) {
        std::cout << "-------------------------------------------------"
                  << std::endl;
        std::cout << "relaxation iteration : "
                  << solver_variables.relaxation_fire_parameters.iter
                  << " , time step : "
                  << solver_variables.relaxation_fire_parameters.time_step
                  << ", global power : " << global_diagnostics[0]
                  << " , global error (repatom acceleration) : "
                  << global_diagnostics[1] << "/" << global_diagnostics[2]
                  << " , alpha = "
                  << solver_variables.relaxation_fire_parameters.alpha
                  << std::endl;

#ifdef FINITE_TEMPERATURE
        std::cout << "global thermal power : " << global_thermal_diagnostics[0]
                  << " , global thermal error : "
                  << global_thermal_diagnostics[1] << "/"
                  << global_thermal_diagnostics[2] << std::endl;
#endif
        std::cout << "global periodic power : "
                  << global_periodic_diagnostics[0]
                  << " , global periodic error : "
                  << global_periodic_diagnostics[1] << "/"
                  << global_periodic_diagnostics[2] << std::endl;
        std::cout << " force L2 residual " << force_residual << std::endl;
      }

      f_timeseries.open("./TimeSeries.dat", std::ios::app);
      f_timeseries << solver_variables.relaxation_fire_parameters.iter << " "
                   << solver_variables.relaxation_fire_parameters.t << " "
                   << force_residual << " " << global_diagnostics[0] << " "
                   << global_diagnostics[1] << " " << global_diagnostics[2]
                   << " "
                   << solver_variables.relaxation_fire_parameters.time_step
                   << " " << solver_variables.relaxation_fire_parameters.alpha
                   << " " << solver_variables.e_global << std::endl;
      f_timeseries.close();
    }

    // +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=
    // Actual beginning of iteration t+dt
    // +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=

#ifdef MULTI_THREADING
#pragma omp parallel default(shared)
#endif
    {
// +=+=+=+=<calculate v(t+dt/2), x(t+dt), t+dt> +=+=+=+=+=+//
#ifdef MULTI_THREADING
#pragma omp for schedule(static)
#endif
      for (std::size_t node_index = 0;
           node_index < local_mesh.repatoms.velocities.size(); node_index++) {
        // v(t+dt/2) = v(t) + (dt/2)*(F(t)/m)
        array_ops::as_eigen(local_mesh.repatoms.velocities[node_index]) +=
            array_ops::as_eigen(local_mesh.repatoms.forces[node_index]) *
            (0.5 * solver_variables.relaxation_fire_parameters.time_step /
             local_mesh.repatoms.mass[node_index]);

#ifdef FINITE_TEMPERATURE
        local_mesh.repatoms.thermal_velocities[node_index] +=
            local_mesh.repatoms.thermal_forces[node_index] * 0.5 *
            solver_variables.relaxation_fire_parameters.time_step /
            local_mesh.repatoms.mass[node_index];

#endif
      }
    }

    array_ops::as_eigen(local_mesh.repatoms.periodic_velocities) +=
        array_ops::as_eigen(local_mesh.repatoms.periodic_forces) *
        (0.5 * solver_variables.relaxation_fire_parameters.time_step /
         local_mesh.repatoms.total_mass);

#ifdef MULTI_THREADING
#pragma omp parallel default(shared) private(sigma_old, phi_new)
#endif
    {
// x(t+dt) = x(t) + dt*v(t+dt/2)
#ifdef MULTI_THREADING
#pragma omp for schedule(static)
#endif
      for (std::size_t node_index = 0;
           node_index < local_mesh.repatoms.velocities.size(); node_index++) {

        array_ops::as_eigen(local_mesh.repatoms.locations[node_index]) +=
            array_ops::as_eigen(local_mesh.repatoms.velocities[node_index]) *
            solver_variables.relaxation_fire_parameters.time_step;

        // d(Sigma)/dt = 2 A /m
        // dA/dt = \Omega/m + F.x/3

#ifdef FINITE_TEMPERATURE

        // if sigma_old is too low (mean phi was too high,
        // then use sigma_ref to scale the phi force).
        // We need extra force against very large negative phi

        sigma_old =
            exp(-local_mesh.repatoms.thermal_coordinates[node_index][1]);

        phi_new = local_mesh.repatoms.thermal_coordinates[node_index][1] +
                  (-local_mesh.repatoms.thermal_velocities[node_index] /
                   (sigma_old)) *
                      solver_variables.relaxation_fire_parameters.time_step;

        if (isentropic) {
          local_mesh.repatoms.isentropic_update(node_index, phi_new,
                                                solver_variables.input.T_min,
                                                solver_variables.input.T_max);
          // if the atoms are thermally fixed, then keep temperature fixed
        } else {

          if (sigma_old < 1.0e-8) {
            std::cout << "here " << sigma_old << " , "
                      << local_mesh.repatoms.thermal_coordinates[node_index][1]
                      << " , " << node_index << " , " << mpi_rank << " , "
                      << array_ops::as_streamable(
                             local_mesh.repatoms.locations[node_index])
                      << " , " << phi_new << std::endl;
          }

          local_mesh.repatoms.thermal_coordinates[node_index][1] = phi_new;
        }
#endif
      }
    }

    // v(t+dt) will be calculated when entering next step
    // such that v(t+dt) = v(t+dt/2) + (dt/2)*(F(t+dt)/m)
    solver_variables.relaxation_fire_parameters.t +=
        solver_variables.relaxation_fire_parameters.time_step;
    // +=+=+=+=</calculate v(t+dt/2), x(t+dt), t+dt> +=+=+=+=+=+//
  }
}
} // namespace solver
