// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief function implementations to update sampling atoms and neighborhoods after every iteration
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "meshing/qc_mesh.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"

namespace solver {

namespace array_ops = qcmesh::array_ops;

/**
 * @brief deforms the mesh elements and computes deformation gradient
 *
 * @param mesh The mesh
 *
 * @tparam MeshD mesh type
 * @tparam Dimension number of spatial dimensions
 */
template <typename MeshD, std::size_t Dimension>
void deform_mesh_elements(MeshD &mesh) {
  using Point = std::array<double, Dimension>;

  std::array<Point, Dimension> deformed_edges{};
  std::array<Point, Dimension> undeformed_edges{};

  // If an element is turned inside out, make it a zero
  // volume element, and then return with a message to
  // remesh/refine
  std::array<std::size_t, Dimension + 1> indexed_simplex{};

  for (auto &[cell_id, cell] : mesh.cells) {
    const auto simplex = qcmesh::mesh::simplex_cell_points(mesh, cell);

    indexed_simplex = meshing::indexed_simplex(mesh, cell_id);

    for (std::size_t d = 0; d < Dimension; d++) {
      array_ops::as_eigen(undeformed_edges[d]) =
          array_ops::as_eigen(simplex[d + 1]) - array_ops::as_eigen(simplex[0]);
      array_ops::as_eigen(deformed_edges[d]) =
          array_ops::as_eigen(mesh.repatoms.locations[indexed_simplex[d + 1]]) -
          array_ops::as_eigen(mesh.repatoms.locations[indexed_simplex[0]]);
    }

    // compute deformation gradient:
    const auto f = array_ops::as_eigen_matrix(deformed_edges) *
                   array_ops::as_eigen_matrix(undeformed_edges).inverse();

    const auto j = f.determinant();

    // change mesh element quantities.
    cell.data.jacobian *= j;

    array_ops::as_eigen_matrix(cell.data.lattice_vectors) =
        f * array_ops::as_eigen_matrix(cell.data.lattice_vectors);

    cell.data.offset_vectors.resize(mesh.noof_offset_vectors);
    for (std::size_t p = 0; p < mesh.noof_offset_vectors; p++)
      array_ops::as_eigen(cell.data.offset_vectors[p]) =
          f * array_ops::as_eigen(cell.data.offset_vectors[p]);

    cell.data.volume *= j;
    cell.data.density /= j;
    cell.data.quality =
        qcmesh::geometry::simplex_metric_residual<Dimension>(deformed_edges);
  }

  // if the jacobian turns too small or even negative,
  // its time to remesh or refine. Negative jacobian
  // implies deformation makes the element turn inside out

  // the best way is to project one of the nodes on the face in front of
  // it.
  // We can choose the node closest to its opposite face.
  // Now we have a degenerate element and qc mesh repair should
  // remove it. Use compressSimplex<Dimension>
}

/**
 * @brief      call mesh functions to exchange displacements
 *             of overlapping mesh
 *
 * @param      local_mesh  The local mesh
 *
 * @tparam     MeshD        mesh type
 * @tparam     Dimension          number of dimensions
 * @tparam     Nidof        number of internal degrees of freedom
 */
template <typename MeshD, std::size_t Dimension, std::size_t Nidof>
void deform_mesh_overlaps(MeshD &local_mesh) {
  local_mesh.exchange_overlapping_deformation();
}

/**
 * @brief      deform periodic offsets
 *
 * @param      local_mesh   The local mesh
 *
 * @tparam     MeshD        mesh type
 * @tparam     Dimension          number of dimensions
 */
template <typename MeshD, std::size_t Dimension, std::size_t Nidof,
          typename NodalData>
void deform_period_offsets(MeshD &local_mesh,
                           const std::array<double, 3> &periodic_strains) {
  // update repatom positions
  for (std::size_t node_idx = 0;
       node_idx < local_mesh.repatoms.locations.size(); node_idx++) {
    array_ops::as_eigen(local_mesh.repatoms.locations[node_idx]) =
        array_ops::as_eigen(local_mesh.repatoms.locations[node_idx])
            .cwiseProduct((1.00 + array_ops::as_eigen(periodic_strains).array())
                              .matrix());
  }

  // update periodic offsets
  for (auto &periodic_offset : local_mesh.periodic_offsets) {
    array_ops::as_eigen(periodic_offset) =
        array_ops::as_eigen(periodic_offset)
            .cwiseProduct((1.00 + array_ops::as_eigen(periodic_strains).array())
                              .matrix());
  }

  // update ghost offsets
  for (std::size_t ghost_index = 0;
       ghost_index < local_mesh.ghost_node_offsets.size(); ghost_index++) {
    array_ops::as_eigen(local_mesh.ghost_node_offsets[ghost_index]) =
        array_ops::as_eigen(local_mesh.ghost_node_offsets[ghost_index])
            .cwiseProduct((1.00 + array_ops::as_eigen(periodic_strains).array())
                              .matrix());
  }

  for (std::size_t node_index = 0;
       node_index < local_mesh.sampling_atoms.nodal_locations.size();
       node_index++) {
    for (auto n_it = local_mesh.sampling_atoms
                         .nodal_sampling_atom_neighbours[node_index]
                         .begin();
         n_it !=
         local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[node_index]
             .end();
         ++n_it) {
      if (n_it->periodic_flag) {
        array_ops::as_eigen(n_it->offset) =
            array_ops::as_eigen(n_it->offset)
                .cwiseProduct(
                    (1.00 + array_ops::as_eigen(periodic_strains).array())
                        .matrix());
      }
    }
  }
}

template <typename MeshD, std::size_t Dimension, std::size_t Nidof,
          typename Matrix, typename NodalData>
void deform_period_offsets(MeshD &local_mesh, Matrix &deformation_gradient) {
  // update repatom positions
  for (std::size_t node_idx = 0;
       node_idx < local_mesh.repatoms.locations.size(); node_idx++) {
    array_ops::as_eigen(local_mesh.repatoms.locations[node_idx]) =
        deformation_gradient *
        array_ops::as_eigen(local_mesh.repatoms.locations[node_idx]);
  }

  // update periodic offsets
  for (auto &periodic_offset : local_mesh.periodic_offsets) {
    array_ops::as_eigen(periodic_offset) =
        deformation_gradient * array_ops::as_eigen(periodic_offset);
  }

  // update ghost offsets
  for (std::size_t ghost_index = 0;
       ghost_index < local_mesh.ghost_node_offsets.size(); ghost_index++) {
    array_ops::as_eigen(local_mesh.ghost_node_offsets[ghost_index]) =
        deformation_gradient *
        array_ops::as_eigen(local_mesh.ghost_node_offsets[ghost_index]);
  }

  for (std::size_t node_index = 0;
       node_index < local_mesh.sampling_atoms.nodal_locations.size();
       node_index++) {
    for (auto n_it = local_mesh.sampling_atoms
                         .nodal_sampling_atom_neighbours[node_index]
                         .begin();
         n_it !=
         local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[node_index]
             .end();
         ++n_it) {
      if (n_it->periodic_flag) {
        array_ops::as_eigen(n_it->offset) =
            deformation_gradient * array_ops::as_eigen(n_it->offset);
      }
    }
  }
}

/**
 * @brief deform sampling atoms and their neighbourhoods
 *        and store the sampling atoms with too much deformation
 *
 * @param[in]  input_params                            The input parameters
 * @param      local_mesh                             The local mesh
 * @param      highly_deformed_nodal_sampling_atoms   The highly deformed nodal sampling atoms
 * @param      highly_deformed_cb_sampling_atoms      The highly deformed cb sampling atoms
 * @param      mesh_exchange_nodal_sampling_atoms      The mesh exchange nodal sampling atoms
 * @param      periodic_exchange_nodal_sampling_atoms  The periodic exchange nodal sampling atoms
 *
 * @tparam     InputClass                              input class
 * @tparam     Mesh                                    mesh type
 * @tparam     Dimension                                     number of dimensions
 * @tparam     Nidof                                   number of internal degrees of freedom
 */

template <typename InputClass, typename Mesh, std::size_t Dimension,
          std::size_t Nidof, typename NodalData>
void deform_sampling_atoms_and_neighbourhoods(
    const InputClass &input_params, Mesh &local_mesh,
    std::vector<std::size_t> &highly_deformed_nodal_sampling_atoms,
    std::vector<std::size_t> &highly_deformed_cb_sampling_atoms,
    std::vector<std::size_t> &mesh_exchange_nodal_sampling_atoms,
    std::vector<std::size_t> &periodic_exchange_nodal_sampling_atoms,
    std::vector<std::size_t> &periodic_exchange_nodal_inner_sampling_atoms,
    const bool periodic_simulation_flag) {

#ifdef MULTI_THREADING
#pragma omp declare reduction(                                                 \
        merge : std::std::vector<std::size_t> : omp_out.insert(                \
                omp_out.end(), omp_in.begin(), omp_in.end()))

#pragma omp declare reduction(                                                 \
        vec_int_plus : std::std::vector<int> : std::transform(                 \
                omp_out.begin(), omp_out.end(), omp_in.begin(),                \
                    omp_out.begin(), std::plus<int>()))                        \
    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))
#endif
  // after this, all nodal sampling atoms have their own
  // neighbours and their own temperatures correctly updated

  geometry::Point<Dimension> sampling_atom_location;
  geometry::Point<Dimension> neighbour_location;

  std::vector<int> sampling_atom_requiring_overlap_rebuild(
      local_mesh.nodes.size(), 0);

  double dx_nei{};
  double dx_nei_max{};
  double dx_nei_max_2{};
  double dx_sampling_atom{};

#ifdef MULTI_THREADING
#pragma omp parallel default(none) shared(local_mesh, input_params) private(   \
    sampling_atom_location, neighbour_location, dx_nei, dx_nei_max,            \
    dx_nei_max_2, dx_samplingAtom)                                             \
    reduction(merge                                                            \
              : highly_deformed_nodal_sampling_atoms)                          \
        reduction(vec_int_plus                                                 \
                  : samplingAtomRequiringOverlapRebuild)
#endif
  {
#ifdef FINITE_TEMPERATURE
    std::array<double, Nidof> sampling_atom_thermal_coordinate{};
    std::array<double, Nidof> neighbour_thermal_coordinate{};
#endif

#ifdef MULTI_THREADING
#pragma omp for schedule(static)
#endif
    // very important to go over all nodal sampling atoms (including ghosts)
    // because we have to deform them.
    for (std::size_t node_index = 0;
         node_index < local_mesh.sampling_atoms.nodal_locations.size();
         node_index++) {

      // get new mesh point location
      sampling_atom_location = mesh_point_from_index(local_mesh, node_index);

#ifdef FINITE_TEMPERATURE
      sampling_atom_thermal_coordinate =
          local_mesh.repatoms.thermal_coordinates[node_index];
#endif

      dx_nei_max = 0.0;
      dx_nei_max_2 = 0.0;
      dx_sampling_atom = 0.0;

      for (auto n_it = local_mesh.sampling_atoms
                           .nodal_sampling_atom_neighbours[node_index]
                           .begin();
           n_it !=
           local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[node_index]
               .end();
           ++n_it) {
        // compute new location
        neighbour_location.fill(0.0);

// compute new temperature
#ifdef FINITE_TEMPERATURE
        neighbour_thermal_coordinate.fill(0.0);
#endif

        if (n_it->interproc_flag && !n_it->periodic_flag) {
          for (std::size_t d = 0; d < n_it->repatom_dependencies.size(); d++) {
            array_ops::as_eigen(neighbour_location) +=
                array_ops::as_eigen(local_mesh.get_overlapping_mesh_point(
                    local_mesh.overlapping_mesh_data,
                    n_it->repatom_dependencies[d].first)) *
                n_it->repatom_dependencies[d].second;

#ifdef FINITE_TEMPERATURE
            array_ops::as_eigen(neighbour_thermal_coordinate) +=
                array_ops::as_eigen(local_mesh.get_overlapping_thermal_point(
                    local_mesh.overlapping_mesh_data,
                    n_it->repatom_dependencies[d].first)) *
                n_it->repatom_dependencies[d].second;
#endif
          }
        } else if (n_it->interproc_flag && n_it->periodic_flag) {
          for (std::size_t d = 0; d < n_it->repatom_dependencies.size(); d++) {
            array_ops::as_eigen(neighbour_location) +=
                (array_ops::as_eigen(local_mesh.get_overlapping_mesh_point(
                     local_mesh.periodic_mesh_data,
                     n_it->repatom_dependencies[d].first)) -
                 array_ops::as_eigen(n_it->offset)) *
                n_it->repatom_dependencies[d].second;

#ifdef FINITE_TEMPERATURE
            array_ops::as_eigen(neighbour_thermal_coordinate) +=
                array_ops::as_eigen(local_mesh.get_overlapping_thermal_point(
                    local_mesh.periodic_mesh_data,
                    n_it->repatom_dependencies[d].first)) *
                n_it->repatom_dependencies[d].second;
#endif
          }
        } else {
          for (std::size_t d = 0; d < n_it->repatom_dependencies.size(); d++) {
            array_ops::as_eigen(neighbour_location) +=
                (array_ops::as_eigen(mesh_point_from_index(
                     local_mesh, n_it->repatom_dependencies[d].first.second)) -
                 array_ops::as_eigen(n_it->offset)) *
                n_it->repatom_dependencies[d].second;

#ifdef FINITE_TEMPERATURE
            array_ops::as_eigen(neighbour_thermal_coordinate) +=
                array_ops::as_eigen(
                    local_mesh.repatoms.thermal_coordinates
                        [n_it->repatom_dependencies[d].first.second]) *
                n_it->repatom_dependencies[d].second;
#endif
          }
        }

        // add relative displacement of neighbourhood
        array_ops::as_eigen(n_it->relative_displ_neighbour_build) +=
            (array_ops::as_eigen(neighbour_location) -
             array_ops::as_eigen(n_it->location)) -
            (array_ops::as_eigen(sampling_atom_location) -
             array_ops::as_eigen(
                 local_mesh.sampling_atoms.nodal_locations[node_index]));

        if (input_params.neighbour_update_style ==
            input::NeighbourUpdateStyle::ADAPTIVE) {
          dx_nei = array_ops::norm(n_it->relative_displ_neighbour_build);

          if (dx_nei > dx_nei_max) {
            dx_nei_max = dx_nei;
          }
        } else {
          // compute relative displacement
          dx_nei =
              (array_ops::as_eigen(neighbour_location) -
               array_ops::as_eigen(n_it->location) -
               (array_ops::as_eigen(sampling_atom_location) -
                array_ops::as_eigen(
                    local_mesh.sampling_atoms.nodal_locations[node_index])))
                  .norm();

          if (dx_nei > dx_nei_max) {
            dx_nei_max_2 = dx_nei_max;
            dx_nei_max = dx_nei;
          } else {
            if (dx_nei > dx_nei_max_2) {
              dx_nei_max_2 = dx_nei;
            }
          }
        }

        n_it->location = neighbour_location;

#ifdef FINITE_TEMPERATURE
        n_it->thermal_coordinate = neighbour_thermal_coordinate;
#endif
      }

      array_ops::as_eigen(
          local_mesh.sampling_atoms
              .nodal_displacements_overlap_exchange[node_index]) +=
          array_ops::as_eigen(
              local_mesh.sampling_atoms.nodal_locations[node_index]) -
          array_ops::as_eigen(sampling_atom_location);

      dx_sampling_atom = array_ops::norm(
          local_mesh.sampling_atoms
              .nodal_displacements_overlap_exchange[node_index]);

      local_mesh.sampling_atoms.nodal_locations[node_index] =
          sampling_atom_location;

      if (local_mesh.nodes.at(local_mesh.node_index_to_key[node_index])
              .data.proc_boundary_flag ||
          local_mesh.nodes.at(local_mesh.node_index_to_key[node_index])
              .data.periodic_boundary_flag) {
        if (dx_sampling_atom >
            (input_params.mesh_overlap_buffer_radius -
             input_params.verlet_buffer_factor *
                 input_params.neighbour_update_displacement)) {
          sampling_atom_requiring_overlap_rebuild[node_index] = 1;
        }
      }

      if (input_params.neighbour_update_style ==
          input::NeighbourUpdateStyle::ADAPTIVE) {
        if (dx_nei_max > input_params.neighbour_update_displacement) {
          highly_deformed_nodal_sampling_atoms.push_back(node_index);
          if (input_params.remove_previous_neighbours) {
            local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[node_index]
                .clear();
          }
        }
      } else {
        // Verlet type neighbourhood update
        if (dx_nei_max_2 + dx_nei_max >
            input_params.neighbour_update_displacement) {
          // this sampling atom needs new neighbours. Mark it
          highly_deformed_nodal_sampling_atoms.push_back(node_index);

          if (input_params.remove_previous_neighbours) {
            // clear its neighbourlist. This still leaves
            // the capacity so push_back is easier
            local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[node_index]
                .clear();
          }
        }
      }
    }
  }

  for (std::size_t node_index = 0;
       node_index < sampling_atom_requiring_overlap_rebuild.size();
       node_index++) {
    if (local_mesh.nodes.at(local_mesh.node_index_to_key.at(node_index))
            .data.proc_boundary_flag &&
        sampling_atom_requiring_overlap_rebuild[node_index]) {
      mesh_exchange_nodal_sampling_atoms.push_back(node_index);
    }

    if (periodic_simulation_flag) {
      if (local_mesh.nodes.at(local_mesh.node_index_to_key.at(node_index))
              .data.periodic_boundary_flag &&
          sampling_atom_requiring_overlap_rebuild[node_index]) {
        periodic_exchange_nodal_sampling_atoms.push_back(node_index);
      }

      if (local_mesh.nodes.at(local_mesh.node_index_to_key.at(node_index))
              .data.proc_boundary_flag &&
          sampling_atom_requiring_overlap_rebuild[node_index] &&
          !(local_mesh.nodes.at(local_mesh.node_index_to_key.at(node_index))
                .data.periodic_boundary_flag)) {
        periodic_exchange_nodal_inner_sampling_atoms.push_back(node_index);
      }
    }
  }

  std::array<std::size_t, Dimension + 1> indexed_simplex{};

  {
#ifdef FINITE_TEMPERATURE
    std::array<double, Nidof> sampling_atom_thermal_coordinate{};
    std::array<double, Nidof> neighbour_thermal_coordinate{};
#endif

    for (std::size_t atom_index = 0;
         atom_index < local_mesh.sampling_atoms.central_locations.size();
         atom_index++) {
      const auto element_key_central_atom =
          local_mesh.sampling_atoms.cell_key_of_central_atoms[atom_index];

      const auto simplex = qcmesh::mesh::simplex_cell_points(
          local_mesh, element_key_central_atom);

      indexed_simplex =
          meshing::indexed_simplex(local_mesh, element_key_central_atom);

      sampling_atom_location = simplex[0];

#ifdef FINITE_TEMPERATURE
      sampling_atom_thermal_coordinate =
          local_mesh.repatoms.thermal_coordinates[indexed_simplex[0]];
#endif

      for (std::size_t d = 1; d < Dimension + 1; d++) {
        array_ops::as_eigen(sampling_atom_location) +=
            array_ops::as_eigen(simplex[d]);

#ifdef FINITE_TEMPERATURE
        array_ops::as_eigen(sampling_atom_thermal_coordinate) +=
            array_ops::as_eigen(
                local_mesh.repatoms.thermal_coordinates[indexed_simplex[d]]);
#endif
      }

      array_ops::as_eigen(sampling_atom_location) /= double(Dimension + 1);
#ifdef FINITE_TEMPERATURE
      array_ops::as_eigen(sampling_atom_thermal_coordinate) /=
          double(Dimension + 1);
#endif

      // check for neighbour deformations
      dx_nei_max = 0.0;
      dx_nei_max_2 = 0.0;

      for (auto n_it = local_mesh.sampling_atoms
                           .central_sampling_atom_neighbours[atom_index]
                           .begin();
           n_it != local_mesh.sampling_atoms
                       .central_sampling_atom_neighbours[atom_index]
                       .end();
           ++n_it) {
        // compute new location
        neighbour_location.fill(0.0);

// compute new temperature
#ifdef FINITE_TEMPERATURE
        neighbour_thermal_coordinate.fill(0.0);
#endif

        for (std::size_t d = 0; d < n_it->repatom_dependencies.size(); d++) {
          array_ops::as_eigen(neighbour_location) +=
              array_ops::as_eigen(mesh_point_from_index(
                  local_mesh, n_it->repatom_dependencies[d].first.second)) *
              n_it->repatom_dependencies[d].second;

#ifdef FINITE_TEMPERATURE
          array_ops::as_eigen(neighbour_thermal_coordinate) +=
              array_ops::as_eigen(
                  local_mesh.repatoms.thermal_coordinates
                      [n_it->repatom_dependencies[d].first.second]) *
              n_it->repatom_dependencies[d].second;
#endif
        }

        array_ops::as_eigen(n_it->relative_displ_neighbour_build) +=
            (array_ops::as_eigen(neighbour_location) -
             array_ops::as_eigen(n_it->location)) -
            (array_ops::as_eigen(sampling_atom_location) -
             array_ops::as_eigen(
                 local_mesh.sampling_atoms.central_locations[atom_index]));

        if (input_params.neighbour_update_style ==
            input::NeighbourUpdateStyle::ADAPTIVE) {
          dx_nei = array_ops::norm(n_it->relative_displ_neighbour_build);

          if (dx_nei > dx_nei_max)
            dx_nei_max = dx_nei;
        } else {
          // compute relative displacement
          dx_nei =
              (array_ops::as_eigen(neighbour_location) -
               array_ops::as_eigen(n_it->location) -
               (array_ops::as_eigen(sampling_atom_location) -
                array_ops::as_eigen(
                    local_mesh.sampling_atoms.central_locations[atom_index])))
                  .norm();

          if (dx_nei > dx_nei_max) {
            dx_nei_max_2 = dx_nei_max;
            dx_nei_max = dx_nei;
          } else {
            if (dx_nei > dx_nei_max_2) {
              dx_nei_max_2 = dx_nei;
            }
          }
        }

        n_it->location = neighbour_location;
#ifdef FINITE_TEMPERATURE
        n_it->thermal_coordinate = neighbour_thermal_coordinate;
#endif
      }

      local_mesh.sampling_atoms.central_locations[atom_index] =
          sampling_atom_location;
#ifdef FINITE_TEMPERATURE
      local_mesh.sampling_atoms.central_ido_fs[atom_index] =
          sampling_atom_thermal_coordinate;
#endif

      if (input_params.neighbour_update_style ==
          input::NeighbourUpdateStyle::ADAPTIVE) {
        if (dx_nei_max > input_params.neighbour_update_displacement) {
          highly_deformed_cb_sampling_atoms.push_back(atom_index);
          if (input_params.remove_previous_neighbours) {
            local_mesh.sampling_atoms
                .central_sampling_atom_neighbours[atom_index]
                .clear();
          }
        }
      } else {
        if (dx_nei_max_2 + dx_nei_max >
            input_params.neighbour_update_displacement) {
          // this cauchy born sampling atom needs new neighbours. Mark it
          highly_deformed_cb_sampling_atoms.push_back(atom_index);
          if (input_params.remove_previous_neighbours) {
            // clear its neighbourlist. This still leaves
            // the capacity so push_back is easier
            local_mesh.sampling_atoms
                .central_sampling_atom_neighbours[atom_index]
                .clear();
          }
        }
      }
    }
  }
}
} // namespace solver
