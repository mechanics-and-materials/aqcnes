// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief struct SolverVariables
 * @authors P.Gupta, S. Saxena, S. Zimmermann
 */

#pragma once

#include "boundarycondition/boundarycondition.hpp"
#include "boundarycondition/external_force_applicator.hpp"
#include "input/userinput.hpp"
#include "meshing/qc_mesh.hpp"
#include <memory>
#include <petscsnes.h>

namespace solver {

struct BandStiffness {
  double k_spring_parallel = 0.0;
  double k_spring_perp = 0.0;
};

// struct to store FIRE parameters
struct FireParameters {
  double t = 0.0;
  bool stop{};
  int iter{};
  double min_residual{}; //||F||
  double alpha{};
  double time_step{};
  std::size_t steps_post_reset{};
  std::size_t max_iterations{};
};

FireParameters fire_parameters_from_userinput(const input::Userinput &input);

template <std::size_t Dimension, std::size_t Noi> struct SolverVariables {

  // primary variables, set outside this class in main files
  input::Userinput input{};

  meshing::QCMesh<Dimension, Dimension, 2, meshing::NodalDataMultispecies<Noi>>
      local_mesh{};

  std::vector<boundarycondition::Boundarycondition<Dimension>>
      boundary_conditions{};
  std::unique_ptr<boundarycondition::ExternalForceApplicator<Dimension>>
      external_force_applicator{};
  std::vector<materials::Material<
      Dimension, Noi, meshing::QCMesh<Dimension, Dimension>::THERMAL_DOF>>
      materials{};

  double e_global{};

  // secondary variables, used while neighbourhood updates
  int n_var{};

  std::vector<std::size_t> triggered_nodal_sampling_atoms{};
  std::vector<std::size_t> triggered_cb_sampling_atoms{};
  std::vector<std::size_t> triggered_mesh_exchange_sampling_atoms{};
  std::vector<std::size_t> triggered_periodic_mesh_exchange_sampling_atoms{};
  std::vector<std::size_t>
      triggered_periodic_mesh_exchange_inner_sampling_atoms{};

  // petsc variables/settings
  SNES snes{};
  SNES snes_pc{};
  SNESLineSearch linesearch{};
  double petsc_abstol{};
  double petsc_rtol{};
  double petsc_stol{};

  // relaxation iteration variables
  int petsc_nonlinear_iter = 0;
  int petsc_linear_iter = 0;
  int petsc_max_iter{};
  int petsc_max_f_eval{};

  double residual{};
  double residual_after_last_relaxation{};
  std::size_t time_steps_post_last_decrease = 0;

  // FIRE mutable parameters
  FireParameters relaxation_fire_parameters{};
  FireParameters neb_fire_parameters{};

  std::size_t solver_iter{};
  std::size_t last_output_iter{};
  std::size_t concentration_update_iter{};
  std::size_t per_atom_info_file_counter{};

  std::array<double, Noi> max_concentration_update_time_steps{};
  double non_dimensionalizing_conc_rate = 1.0;
  double elapsed_conc_update_time = 0.0;

  // solver flags
  bool apply_boundary_conditions{};
  bool apply_external_force{};
  bool isentropic{}; // if this is false, then we are doing isothermal

  // previously private
  PetscErrorCode ierr{};
  PetscInt noof_local_dof{}, noof_global_dof{}, noof_ghost_dof{};
  PetscInt noof_local_var{}, noof_global_var{}, noof_ghost_var{};

  Vec x_distributed{}, x_local{};
  Vec f_distributed{}, f_local{};

  const PetscReal *local_x_array_read{};
  const PetscReal *local_f_array_read{};

#ifdef FINITE_TEMPERATURE
  Vec s_dot_distributed{}, s_dot_local{};
  const PetscReal *local_s_dot_read{};
  PetscReal *local_s_dot_array{};
#endif

  PetscReal *local_x_array{};
  PetscReal *local_f_array{};
  SNESConvergedReason reason{};

  [[nodiscard]] bool periodic_simulation_flag() const {
    return this->local_mesh.has_periodic_dimensions;
  }
};
} // namespace solver
