// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Functions to write solver variable to vtu files
 */

#pragma once

#include "meshing/write_mesh_to_nc.hpp"
#include "solver/solver_variables.hpp"
#include "vtkwriter/write_vtk_point_data_3d.hpp"

namespace solver {

/**
 * @brief Format a file name "{prefix}######.{ext}",
 * where ###### is output_file_counter padded with zeros (length 6).
 */
inline std::string fmt_filename(const std::string &prefix,
                                const std::size_t output_file_counter,
                                const std::string &extension) {
  constexpr std::size_t COUNTER_LEN = 6;
  return prefix +
         utils::to_string_with_zeros(output_file_counter, COUNTER_LEN) + "." +
         extension;
}

template <std::size_t Dimension, std::size_t Noi>
void dump_solver_variables_nc(
    const SolverVariables<Dimension, Noi> &solver_variables,
    const netcdf_io::RestartData<Dimension> &restart_data) {
  meshing::write_restart_mesh_to_nc(
      std::filesystem::path{"."} / "restart_files" /
          fmt_filename("restart_", restart_data.restartfile_iter, "nc"),
      solver_variables.local_mesh, restart_data,
      solver_variables.boundary_conditions,
      *solver_variables.external_force_applicator);
}

template <std::size_t Dimension, std::size_t Noi>
void dump_solver_variables_vtk(
    SolverVariables<Dimension, Noi> &solver_variables,
    const std::size_t counter, const bool display_zones,
    const bool display_element_qualities) {
  if (solver_variables.input.output_centro_symmetry) {
    solver_variables.local_mesh.compute_nodal_centro_symmetry();
  }
  // Write the final solution
  const auto filename = std::filesystem::path{"."} / "solution_files" /
                        fmt_filename("AQCNES_", counter, "pvtp");
  vtkwriter::write_vtk_point_data_3d(
      solver_variables.local_mesh,
      display_zones ? solver_variables.boundary_conditions
                    : decltype(solver_variables.boundary_conditions){},
      solver_variables.input.output_centro_symmetry, filename,
      display_element_qualities);
}

class DumpSolverVariablesVtk {
  std::size_t &counter;
  bool display_zones{};
  bool display_element_qualities{};

public:
  inline DumpSolverVariablesVtk(std::size_t &counter, const bool display_zones,
                                const bool display_element_qualities)
      : counter{counter}, display_zones{display_zones},
        display_element_qualities{display_element_qualities} {}
  template <std::size_t Dimension, std::size_t Noi>
  void operator()(SolverVariables<Dimension, Noi> &solver_variables) const {
    dump_solver_variables_vtk(solver_variables, this->counter,
                              this->display_zones,
                              this->display_element_qualities);
    this->counter++;
  }
};

} // namespace solver
