// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief function implementations to loop around FIRE or PetSc iterations
 * @authors P. Gupta, S. Saxena
 */

#pragma once
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/solver_variables.hpp"
#include <functional>
#include <petscsnes.h>

namespace solver {
// NOLINTBEGIN(cppcoreguidelines-pro-bounds-pointer-arithmetic)

namespace array_ops = qcmesh::array_ops;

/**
 * @brief wraps the local x to repatom container locations and thermal coordinates
 */
template <std::size_t Dimension, std::size_t Noi>
void wrap_petsc_vec_to_repatoms(
    SolverVariables<Dimension, Noi> &solver_variables) {
#ifdef FINITE_TEMPERATURE
  double phi_new{};
#endif
  std::size_t repatom_index = 0;

  // contains ghost repatom locations also
  for (std::size_t repatom_index = 0;
       repatom_index < solver_variables.local_mesh.repatoms.locations.size();
       repatom_index++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      solver_variables.local_mesh.repatoms.locations[repatom_index][d] =
          solver_variables
              .local_x_array_read[solver_variables.n_var * repatom_index + d];
    }

#ifdef FINITE_TEMPERATURE
    phi_new = solver_variables
                  .local_x_array_read[solver_variables.n_var * repatom_index +
                                      Dimension];

    if (solver_variables.isentropic) {
      solver_variables.local_mesh.repatoms.isentropic_update(
          repatom_index, phi_new, solver_variables.input.T_min,
          solver_variables.input.T_max);
    } else {
      solver_variables.local_mesh.repatoms
          .thermal_coordinates[repatom_index][1] = phi_new;
    }
#endif
  }

#ifdef FINITE_TEMPERATURE
  if (!solver_variables.isentropic)
    solver_variables.local_mesh.repatoms.update_equilibrium_entropy();
#endif

  for (std::size_t ghost_index = 0;
       ghost_index < solver_variables.local_mesh.ghost_node_offsets.size();
       ghost_index++) {
    repatom_index = solver_variables.local_mesh.repatoms.locations.size() -
                    solver_variables.local_mesh.ghost_node_offsets.size() +
                    static_cast<std::size_t>(ghost_index);

    array_ops::as_eigen(
        solver_variables.local_mesh.repatoms.locations[repatom_index]) +=
        array_ops::as_eigen(
            solver_variables.local_mesh.ghost_node_offsets[ghost_index]);
  }
}

template <std::size_t Dimension, std::size_t Noi>
void wrap_petsc_vec_to_repatom_forces(
    SolverVariables<Dimension, Noi> &solver_variables) {

// contains ghost repatom locations also
#ifdef FINITE_TEMPERATURE
  double sigma{};
#endif

  for (std::size_t repatom_index = 0;
       repatom_index < solver_variables.local_mesh.repatoms.forces.size();
       repatom_index++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      solver_variables.local_mesh.repatoms.forces[repatom_index][d] =
          -solver_variables
               .local_f_array_read[solver_variables.n_var * repatom_index + d] *
          solver_variables.local_mesh.repatoms.mass[repatom_index];
    }

// only sigma has the optimizing force
#ifdef FINITE_TEMPERATURE

    sigma = exp(-solver_variables.local_mesh.repatoms
                     .thermal_coordinates[repatom_index][1]);

    solver_variables.local_mesh.repatoms.thermal_forces[repatom_index] =
        solver_variables
            .local_f_array_read[solver_variables.n_var * repatom_index +
                                Dimension] *
        solver_variables.local_mesh.repatoms.mass[repatom_index] * sigma;

#endif
  }
}

template <std::size_t Dimension, std::size_t Noi>
void unwrap_repatom_forces_to_petsc_vec(
    SolverVariables<Dimension, Noi> &solver_variables) {

// contains ghost repatom locations also
#ifdef FINITE_TEMPERATURE
  double sigma{};
#endif

  for (std::size_t repatom_index = 0;
       repatom_index < solver_variables.local_mesh.repatoms.forces.size();
       repatom_index++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      solver_variables
          .local_f_array[solver_variables.n_var * repatom_index + d] =
          -solver_variables.local_mesh.repatoms.forces[repatom_index][d] /
          (solver_variables.local_mesh.repatoms.mass[repatom_index]);
    }

// only sigma has the optimizing force
#ifdef FINITE_TEMPERATURE

    sigma = exp(-solver_variables.local_mesh.repatoms
                     .thermal_coordinates[repatom_index][1]);

    solver_variables
        .local_f_array[solver_variables.n_var * repatom_index + Dimension] =
        solver_variables.local_mesh.repatoms.thermal_forces[repatom_index] /
        (solver_variables.local_mesh.repatoms.mass[repatom_index] * sigma);
#endif
  }
}

template <std::size_t Dimension, std::size_t Noi>
void unwrap_repatoms_to_petsc_vec(
    SolverVariables<Dimension, Noi> &solver_variables) {

  // contains ghost repatom locations also
  for (std::size_t repatom_index = 0;
       repatom_index < solver_variables.local_mesh.repatoms.locations.size();
       repatom_index++) {
    for (std::size_t d = 0; d < Dimension; d++) {
      solver_variables
          .local_x_array[solver_variables.n_var * repatom_index + d] =
          solver_variables.local_mesh.repatoms.locations[repatom_index][d];
    }

// only sigma has the optimizing force
#ifdef FINITE_TEMPERATURE
    solver_variables
        .local_x_array[solver_variables.n_var * repatom_index + Dimension] =
        solver_variables.local_mesh.repatoms
            .thermal_coordinates[repatom_index][1];
#endif
  }
}

namespace detail {
/**
 * @brief periodic strains are \gamma(t). New period length l(t) will be
 * l(t) = (1.0 + gamma(t))*l(t-1)
 * all repatoms locations need to be changed such that
 * q_i(t) = (1.0 + gamma(t))*q_i(t-1)
 * periodic forces are averaged over all sampling atoms since velocities
 * are computed using force/total_mass as the acceleration. So the force
 * is considered for a single bond of length a_ref. All
 */
template <std::size_t Dimension>
std::array<double, 3>
periodic_strains(const double a_ref, const double fire_time_step,
                 const geometry::Point<Dimension> &periodic_velocities) {
  auto strains = std::array<double, 3>{};
  array_ops::as_eigen(strains) =
      array_ops::as_eigen(periodic_velocities) * fire_time_step / a_ref;
  return strains;
}
} // namespace detail

template <std::size_t Dimension, std::size_t Noi>
PetscErrorCode solve(SolverVariables<Dimension, Noi> &solver_variables) {
  // neighbourhoods should be ready before this function

  // set the initial condition from repatom container

  VecGhostGetLocalForm(solver_variables.x_distributed,
                       &solver_variables.x_local);
  VecGetArray(solver_variables.x_local, &solver_variables.local_x_array);

  unwrap_repatoms_to_petsc_vec<Dimension>(solver_variables);

  VecRestoreArray(solver_variables.x_local, &solver_variables.local_x_array);
  VecGhostRestoreLocalForm(solver_variables.x_distributed,
                           &solver_variables.x_local);

  VecGhostUpdateBegin(solver_variables.x_distributed, INSERT_VALUES,
                      SCATTER_FORWARD);
  VecGhostUpdateEnd(solver_variables.x_distributed, INSERT_VALUES,
                    SCATTER_FORWARD);

  if (solver_variables.input.use_petsc) {
    const auto mpi_rank = boost::mpi::communicator{}.rank();
    // solve the system !
    solver_variables.ierr = SNESSolve(solver_variables.snes, nullptr,
                                      solver_variables.x_distributed);
    CHKERRQ(solver_variables.ierr);
    SNESGetConvergedReason(solver_variables.snes, &solver_variables.reason);

    if (mpi_rank == 0) {
      std::cout << "total iteration number "
                << solver_variables.petsc_nonlinear_iter << std::endl;
      std::cout << "Converged reason " << solver_variables.reason << std::endl;
    }
  }

  return 0;
}

#ifdef FINITE_TEMPERATURE
template <std::size_t Dimension, std::size_t Noi>
static void
exchange_interproc_scalars(std::vector<double> &scalars,
                           SolverVariables<Dimension, Noi> &solver_variables) {
  // transfer entropy rates from repatoms to sDot_distributed_
  VecGhostGetLocalForm(solver_variables.s_dot_distributed,
                       &solver_variables.s_dot_local);
  VecGetArray(solver_variables.s_dot_local,
              &solver_variables.local_s_dot_array);

  // unwrap repatomEntropyRates
  // unwrapRepatomEntropyRatesToPetscVec();
  for (std::size_t node_index = 0; node_index < scalars.size(); node_index++) {
    solver_variables.local_s_dot_array[node_index] = scalars[node_index];
  }

  VecRestoreArray(solver_variables.s_dot_local,
                  &solver_variables.local_s_dot_array);
  VecGhostRestoreLocalForm(solver_variables.s_dot_distributed,
                           &solver_variables.s_dot_local);

  // add values on ghost nodes
  VecGhostUpdateBegin(solver_variables.s_dot_distributed, ADD_VALUES,
                      SCATTER_REVERSE);
  VecGhostUpdateEnd(solver_variables.s_dot_distributed, ADD_VALUES,
                    SCATTER_REVERSE);

  // update the vector
  VecGhostUpdateBegin(solver_variables.s_dot_distributed, INSERT_VALUES,
                      SCATTER_FORWARD);
  VecGhostUpdateEnd(solver_variables.s_dot_distributed, INSERT_VALUES,
                    SCATTER_FORWARD);

  // get the read only local form
  VecGhostGetLocalForm(solver_variables.s_dot_distributed,
                       &solver_variables.s_dot_local);
  VecGetArrayRead(solver_variables.s_dot_local,
                  &solver_variables.local_s_dot_read);

  // wrap petscVec to repatom Entropy rates
  // wrapPetscVecToRepatomEntropyRates();
  for (std::size_t node_index = 0; node_index < scalars.size(); node_index++) {
    scalars[node_index] = solver_variables.local_s_dot_read[node_index];
  }

  VecRestoreArrayRead(solver_variables.s_dot_local,
                      &(solver_variables.local_s_dot_read));
  VecGhostRestoreLocalForm(solver_variables.s_dot_distributed,
                           &(solver_variables.s_dot_local));
}

#endif
// NOLINTEND(cppcoreguidelines-pro-bounds-pointer-arithmetic)
} // namespace solver
