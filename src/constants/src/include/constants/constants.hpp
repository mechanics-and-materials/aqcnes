// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Physical and geometric constants used throughout the code
 * @authors P. Gupta, S. Saxena
 */

#pragma once

namespace constants {
static constexpr double k_Boltzmann = 8.6173303e-5; //(ev/K)
static constexpr double geometric_tolerance = 1e-6; // Angstroms
static constexpr double shape_function_tolerance = 1e-6;
static constexpr double concentration_minimum_bound = 1e-10;
static constexpr double concentration_maximum_bound = 1.0 - 1e-10;
static constexpr double force_tolerance = 1.0e-9;

/// Factor converting g to eV*fs^2/A^2
/// needed for units to be consistent with most potentials (except LJ)
static constexpr double G_TO_EV_FS2_A2 = 103.642641;
} // namespace constants
