// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function declaration for generating neighbourhoods of non-atomistic simplices
 * @author P. Gupta, S. Saxena
 */

#include "neighbourhoods/generate_and_add_neighbourhood.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/ball.hpp"

bool qcmesh::geometry::traits::HaveOverlap<
    neighbourhoods::Annulus,
    std::array<double, 3>>::have_overlap(const neighbourhoods::Annulus &annulus,
                                         const std::array<double, 3> &point) {
  const auto rr = array_ops::squared_distance(annulus.center, point);
  return annulus.r_inner * annulus.r_inner < rr &&
         rr < annulus.r_outer * annulus.r_outer;
}

qcmesh::geometry::Box<3>
qcmesh::geometry::traits::BoundingBox<neighbourhoods::Annulus>::bounding_box(
    const neighbourhoods::Annulus &annulus) {
  using Ball = qcmesh::geometry::Ball<3>;
  return qcmesh::geometry::traits::BoundingBox<Ball>::bounding_box(
      Ball{annulus.center, annulus.r_outer});
}
