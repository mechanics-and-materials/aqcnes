// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Compute entropy rates for exchange of temperature
 * @author P. Gupta
 */

#pragma once

#include "constants/constants.hpp"
#include "geometry/point.hpp"
#include "materials/material.hpp"
#include "meshing/traits.hpp"
#include <boost/mpi.hpp>

namespace neighbourhoods {

/**
 * @brief compute entropy rate from diffusion
 */
template <typename Mesh, std::size_t DIM, typename NodalData>
void entropy_rate_onsager_kinetics(
    Mesh &local_mesh, double &dt_min,
    const std::vector<
        materials::Material<DIM, meshing::traits::N_IMPURITIES<NodalData>>> &,
    const double onsager_coefficient) {

  // onsager_coefficient for copper from conductivity at room temperature
  // kappa = 401 W/m.K

  // 1 J = 6.242e+18 eV
  // r\otimes r in A^2

  // onsager_coefficient = 	2.0*V0*kappa*(r \otimes r)^{-1} [W/m.K][10^{-30}
  // m^3]/10^{-20}m^{-2} = 2.0*(V0*kappa/lambda)[10^{-10}] W/K
  // = 2.0*1.0e-10*(V0*kappa/lambda)*6.242e+18 eV/s.K
  // = 2.0*6.242e+8*(V0*kappa/lambda) eV/s.K
  // = 2.0*6.242e-7*(V0*kappa/lambda) eV/fs.K
  // = 2.0*6.242e-1*(V0*kappa/lambda) eV/ns.K

  for (std::size_t i = 0; i < local_mesh.repatoms.locations.size(); i++) {

    local_mesh.repatoms.entropy_production_rates[i] = 0.0;
    local_mesh.repatoms.dissipative_potential[i] = 0.0;
  }

  double entropy_rate{};
  double dissipation_potential{};
  double theta_alpha{};
  double theta_j{};
  double theta_ij{};
  double p_ij{};
  std::size_t noof_neighbours = 0;

  double dt_inv_sampling_atom{};
  double dt_inv_max_local{};
  double dt_inv_max_global{};

  std::vector<geometry::Point<DIM>> cluster;
  std::vector<std::array<double, Mesh::THERMAL_DOF>>
      cluster_thermal_coordinates;
  std::vector<NodalData> cluster_nodal_data;
  std::vector<int> neighbour_indices;
  // T_i\dot{s}_i = \sum_{j}A_{ij} T^2_{ij} \left(1/_T{i} - 1/T_{j}\right)

  // use force based type entropy rates coarsening

  dt_inv_max_local = std::numeric_limits<double>::min();

  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.central_locations.size();
       atom_index++) {
    entropy_rate = 0.0;

    dissipation_potential = 0.0;

    local_mesh.sampling_atoms.get_central_neighbour_thermal_cluster(
        atom_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);

    noof_neighbours = cluster.size();

    theta_alpha =
        exp(-local_mesh.sampling_atoms.central_ido_fs[atom_index][0]) /
        constants::k_Boltzmann;

    // theta_alpha =
    // _local_mesh.samplingAtoms.centralIDOFs_[atomIndex][0]/constants::k_Boltzmann;

    dt_inv_sampling_atom = 0.0;

    for (std::size_t j = 0; j < noof_neighbours; j++) {
      theta_j =
          exp(-cluster_thermal_coordinates[j][0]) / constants::k_Boltzmann;

      theta_ij = 0.5 * (theta_alpha + theta_j);
      p_ij = (1.0 / theta_alpha - 1.0 / theta_j);

      dissipation_potential += 0.5 * onsager_coefficient *
                               pow(theta_ij * p_ij, 2.0) *
                               (theta_alpha * theta_alpha);

      entropy_rate +=
          onsager_coefficient * pow(theta_ij, 2.0) * p_ij / theta_alpha;

      dt_inv_sampling_atom += theta_ij * theta_ij / (theta_alpha * theta_j);
    }

    if (dt_inv_sampling_atom > dt_inv_max_local)
      dt_inv_max_local = dt_inv_sampling_atom;

    entropy_rate *= local_mesh.sampling_atoms.central_weights[atom_index];
    dissipation_potential *=
        local_mesh.sampling_atoms.central_weights[atom_index];

    const auto element_key_central_atom =
        local_mesh.sampling_atoms.cell_key_of_central_atoms[atom_index];

    const auto simplex = indexed_simplex(local_mesh, element_key_central_atom);

    for (std::size_t d = 0; d < DIM + 1; d++) {
      local_mesh.repatoms.entropy_production_rates[simplex[d]] +=
          entropy_rate / double(DIM + 1);

      local_mesh.repatoms.dissipative_potential[simplex[d]] +=
          dissipation_potential / double(DIM + 1);
    }
  }

  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.nodal_locations.size() -
                        local_mesh.global_ghost_indices.size();
       atom_index++) {
    entropy_rate = 0.0;
    dissipation_potential = 0.0;

    local_mesh.sampling_atoms.get_nodal_neighbour_thermal_cluster(
        atom_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);

    noof_neighbours = cluster.size();

    theta_alpha = exp(-local_mesh.repatoms.thermal_coordinates[atom_index][0]) /
                  constants::k_Boltzmann;
    // theta_alpha =
    // _local_mesh.repatoms.thermal_coordinates_[atomIndex][0]/constants::k_Boltzmann;

    dt_inv_sampling_atom = 0.0;

    for (std::size_t j = 0; j < noof_neighbours; j++) {
      theta_j =
          exp(-cluster_thermal_coordinates[j][0]) / constants::k_Boltzmann;

      theta_ij = 0.5 * (theta_alpha + theta_j);
      p_ij = (1.0 / theta_alpha - 1.0 / theta_j);

      dissipation_potential += 0.5 * onsager_coefficient *
                               pow(theta_ij * p_ij, 2.0) *
                               (theta_alpha * theta_alpha);

      entropy_rate +=
          onsager_coefficient * pow(theta_ij, 2.0) * p_ij / theta_alpha;

      dt_inv_sampling_atom += theta_ij * theta_ij / (theta_alpha * theta_j);

      // theta_j = _local_mesh.samplingAtoms.
      // nodalSamplingAtomNeighbours_[atomIndex][j].thermal_coordinate_[0]/constants::k_Boltzmann;
    }

    if (dt_inv_sampling_atom > dt_inv_max_local)
      dt_inv_max_local = dt_inv_sampling_atom;

    entropy_rate *= local_mesh.sampling_atoms.nodal_weights[atom_index];
    dissipation_potential *=
        local_mesh.sampling_atoms.nodal_weights[atom_index];

    local_mesh.repatoms.entropy_production_rates[atom_index] += entropy_rate;

    local_mesh.repatoms.dissipative_potential[atom_index] +=
        dissipation_potential;
  }

  const auto world = boost::mpi::communicator{};
  boost::mpi::all_reduce(world, dt_inv_max_local, dt_inv_max_global,
                         boost::mpi::maximum<double>());

  dt_min = 3.0 * constants::k_Boltzmann /
           (2.0 * onsager_coefficient * dt_inv_max_global);

  // normalize with repatom weights
  for (std::size_t i = 0; i < local_mesh.repatoms.locations.size(); i++) {

    local_mesh.repatoms.entropy_production_rates[i] /=
        local_mesh.repatoms.nodal_weights[i];
    local_mesh.repatoms.dissipative_potential[i] /=
        local_mesh.repatoms.nodal_weights[i];
    ;
  }
}
} // namespace neighbourhoods
