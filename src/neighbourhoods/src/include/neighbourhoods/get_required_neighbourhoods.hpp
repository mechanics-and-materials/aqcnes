// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Function implementation for getting required sampling atom neighbourhoods
 * @author P. Gupta, S. Saxena
 */

#pragma once

#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "neighbourhoods/generate_and_add_neighbourhood.hpp"
#include "neighbourhoods/overlapping_mesh_exchange.hpp"
#include "neighbourhoods/periodic_mesh_exchange.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/intersect_balls_with_simplex.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "utils/timer.hpp"
#include "vtkwriter/write_vtu_triangulation.hpp"
#include <tuple>

namespace neighbourhoods {

namespace array_ops = qcmesh::array_ops;

template <std::size_t Dimension>
void get_closest_lattice_site(
    const std::array<std::array<double, Dimension>, Dimension> &vectors,
    const std::array<double, Dimension> &reference,
    const std::array<double, Dimension> &target,
    std::array<double, Dimension> &result) {
  Eigen::Matrix<double, Dimension, 1> p;

  for (std::size_t d = 0; d < Dimension; d++) {
    p(d, 0) = target[d] - reference[d];
  }
  const auto lattice_vectors = array_ops::as_eigen_matrix(vectors);

  Eigen::Matrix<double, Dimension, 1> coeff;
  coeff = lattice_vectors.lu().solve(p);

  Eigen::Matrix<double, Dimension, 1> coeff_int;
  for (std::size_t d = 0; d < Dimension; d++) {
    coeff_int(d, 0) = int(round(coeff(d, 0)));
  }

  Eigen::Matrix<double, Dimension, 1> eigen_point = lattice_vectors * coeff_int;

  for (std::size_t d = 0; d < Dimension; d++) {
    result[d] = reference[d] + eigen_point(d, 0);
  }
}

/**
 * @brief build neighbourhoods of all (non-cauchy born) sampling atoms
 * and mark the sampling atoms that are near enough to the
 * processor boundaries.
 *
 *  get iterator locations of nodal sampling atoms with
 * large deforming neighbourhoods.
 */
template <typename InputClass, typename Mesh, std::size_t DIM,
          std::size_t NIDOF, typename NodalData>
void get_required_neighbourhoods(
    const InputClass &input_params, Mesh &local_mesh,
    std::vector<std::size_t> &marked_mesh_exchange_sampling_atoms,
    std::vector<std::size_t> &marked_periodic_mesh_exchange_sampling_atoms,
    std::vector<std::size_t>
        &marked_periodic_mesh_exchange_inner_sampling_atoms,
    std::vector<std::size_t> &marked_nodal_sampling_atoms,
    std::vector<std::size_t> &marked_cb_sampling_atoms,
    const bool periodic_simulation_flag) {

  // ---------- < nodal sampling atoms >-----------------//
  // determine which marked nodal sampling atoms
  // might need more mesh.
  using Simplex = std::array<geometry::Point<DIM>, DIM + 1>;
  using PairDataIndex = std::pair<int, int>;
  using TripletDataIndex = std::tuple<int, int, int>;

  using Ball = qcmesh::geometry::Ball<DIM>;

  constexpr std::size_t noi = meshing::traits::N_IMPURITIES<NodalData>;

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();
  // make the marked sampling atom balls
  std::vector<qcmesh::geometry::Indexed<Ball>> local_sampling_atom_balls;
  std::vector<Ball> interproc_balls;
  std::vector<Ball> periodic_balls;
  std::vector<Ball> periodic_inner_balls;

  std::vector<std::vector<std::size_t>> elements_of_sampling_atoms;

  std::vector<std::vector<PairDataIndex>> periodic_elements_of_sampling_atoms;
  std::vector<std::vector<PairDataIndex>> rcvd_elements_of_sampling_atoms;
  std::vector<std::vector<TripletDataIndex>>
      recv_periodic_elements_of_sampling_atoms;

  const auto verlet_buffer = input_params.verlet_buffer_factor *
                             input_params.neighbour_update_displacement;

  auto conc_update_buffer = 0.0;
  if (input_params.concentration_update_style ==
      input::ConcentrationUpdateStyle::NEB_master_equation)
    conc_update_buffer =
        verlet_buffer + input_params.concentration_update_cutoff_radius;

  auto neighbourhood_clock = utils::Timer{};
  if (input_params.neighborhood_and_force_time) {
    neighbourhood_clock.tic();
  }

  local_sampling_atom_balls.reserve(marked_nodal_sampling_atoms.size());

  elements_of_sampling_atoms.reserve(marked_nodal_sampling_atoms.size());
  rcvd_elements_of_sampling_atoms.reserve(marked_nodal_sampling_atoms.size());
  recv_periodic_elements_of_sampling_atoms.reserve(
      marked_nodal_sampling_atoms.size());
  periodic_elements_of_sampling_atoms.reserve(
      marked_nodal_sampling_atoms.size());

  interproc_balls.reserve(marked_mesh_exchange_sampling_atoms.size());

  if (periodic_simulation_flag) {
    periodic_balls.reserve(marked_periodic_mesh_exchange_sampling_atoms.size());

    periodic_inner_balls.reserve(
        marked_periodic_mesh_exchange_inner_sampling_atoms.size());
  }

  Ball atom_ball;
  for (const auto &node_index : marked_nodal_sampling_atoms) {

    atom_ball = {local_mesh.sampling_atoms.nodal_locations[node_index],
                 local_mesh.sampling_atoms.nodal_cut_offs[node_index] +
                     verlet_buffer + conc_update_buffer};

    local_sampling_atom_balls.emplace_back(qcmesh::geometry::Indexed<Ball>{
        local_sampling_atom_balls.size(), atom_ball});

    elements_of_sampling_atoms.emplace_back();

    rcvd_elements_of_sampling_atoms.emplace_back();

    recv_periodic_elements_of_sampling_atoms.emplace_back();

    periodic_elements_of_sampling_atoms.emplace_back();
  }

  for (const auto &node_index : marked_mesh_exchange_sampling_atoms) {
    atom_ball = {local_mesh.sampling_atoms.nodal_locations[node_index],
                 local_mesh.sampling_atoms.nodal_cut_offs[node_index] +
                     input_params.mesh_overlap_buffer_radius +
                     conc_update_buffer};

    interproc_balls.push_back(atom_ball);

    // reset nodal displacement since overlap exchange of this node
    local_mesh.sampling_atoms.nodal_displacements_overlap_exchange[node_index]
        .fill(0.0);
  }

  if (periodic_simulation_flag) {
    for (const auto &node_index :
         marked_periodic_mesh_exchange_sampling_atoms) {
      atom_ball = {local_mesh.sampling_atoms.nodal_locations[node_index],
                   local_mesh.sampling_atoms.nodal_cut_offs[node_index] +
                       input_params.mesh_overlap_buffer_radius +
                       conc_update_buffer};

      periodic_balls.push_back(atom_ball);

      // reset nodal displacement since periodic exchange of this node
      local_mesh.sampling_atoms.nodal_displacements_overlap_exchange[node_index]
          .fill(0.0);
    }

    for (const auto &node_index :
         marked_periodic_mesh_exchange_inner_sampling_atoms) {
      atom_ball = {local_mesh.sampling_atoms.nodal_locations[node_index],
                   local_mesh.sampling_atoms.nodal_cut_offs[node_index] +
                       input_params.mesh_overlap_buffer_radius +
                       conc_update_buffer};

      periodic_inner_balls.push_back(atom_ball);

      // reset nodal displacement since periodic exchange of this node
      local_mesh.sampling_atoms.nodal_displacements_overlap_exchange[node_index]
          .fill(0.0);
    }
  }

  if (input_params.neighborhood_and_force_time) {
    const auto elapsed_time = neighbourhood_clock.toc();
    std::cout << "rank - " << mpi_rank << " took " << elapsed_time
              << " in building sampling atom balls" << std::endl;
    neighbourhood_clock.tic();
  }
  // we have to enter so that ranks can communicate
  neighbourhoods::update_overlapping_mesh_elements_and_nodes<Mesh, DIM, NIDOF,
                                                             NodalData>(
      local_mesh, interproc_balls, input_params.halo_mesh_exchange);

  if (periodic_simulation_flag) {

    bool if_periodic_boundary_balls = true;
    neighbourhoods::update_periodic_mesh_elements_and_nodes<Mesh, DIM, NIDOF,
                                                            NodalData>(
        local_mesh, periodic_balls, if_periodic_boundary_balls,
        input_params.halo_mesh_exchange);

    if_periodic_boundary_balls = false;

    neighbourhoods::update_periodic_mesh_elements_and_nodes<Mesh, DIM, NIDOF,
                                                            NodalData>(
        local_mesh, periodic_inner_balls, if_periodic_boundary_balls,
        input_params.halo_mesh_exchange);
  }

  if (input_params.neighborhood_and_force_time) {
    const auto elapsed_time = neighbourhood_clock.toc();
    std::cout << "rank - " << mpi_rank << " took " << elapsed_time
              << " in getting overlapping mesh" << std::endl;
    neighbourhood_clock.tic();
  }

  auto sampling_atom_balls_tree =
      qcmesh::geometry::kd_tree::KDTree(local_sampling_atom_balls);
  const auto max_ball_radius =
      qcmesh::geometry::max_radius(local_sampling_atom_balls);

  Simplex offset_simplex;
  PairDataIndex index_pair;
  // collect local elements on sampling atom balls
  for (std::size_t i_el = 0; i_el != local_mesh.cells.size(); ++i_el) {
    const auto simplex = qcmesh::mesh::simplex_cell_points(
        local_mesh, local_mesh.simplex_index_to_key[i_el]);

    for (const auto &ball : qcmesh::geometry::intersect_balls_with_simplex(
             sampling_atom_balls_tree, simplex, max_ball_radius)) {
      elements_of_sampling_atoms[ball.index].push_back(i_el);
    }
  }

  if (periodic_simulation_flag) {
    // periodic elements in the local mesh
    if (local_mesh.periodic_mesh_data.overlap_simplex_idxs[mpi_rank].size() >
        0) {
      for (const auto i_el :
           local_mesh.periodic_mesh_data.overlap_simplex_idxs[mpi_rank]) {
        const auto simplex = qcmesh::mesh::simplex_cell_points(
            local_mesh, local_mesh.simplex_index_to_key[i_el]);

        for (std::size_t offset_index = 1;
             offset_index < local_mesh.periodic_offsets.size();
             offset_index++) {
          index_pair = std::make_pair(i_el, offset_index);

          for (std::size_t d = 0; d < DIM + 1; d++)
            array_ops::as_eigen(offset_simplex[d]) =
                array_ops::as_eigen(simplex[d]) -
                array_ops::as_eigen(local_mesh.periodic_offsets[offset_index]);

          for (const auto &ball :
               qcmesh::geometry::intersect_balls_with_simplex(
                   sampling_atom_balls_tree, offset_simplex, max_ball_radius)) {
            // this non cauchy born sampling atom has this received element as
            // neighbour
            periodic_elements_of_sampling_atoms[ball.index].push_back(
                index_pair);
          }
        }
      }
    }
  }

  Simplex simplex;

  // do simple mesh overlap simplices in the sampling atom balls
  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank &&
        local_mesh.overlapping_mesh_data.overlapping_simplices_recv[i_proc]
                .size() > 0) {
      for (std::size_t i_el_recv = 0;
           i_el_recv <
           local_mesh.overlapping_mesh_data.overlapping_simplices_recv[i_proc]
               .size();
           i_el_recv++) {
        index_pair = std::make_pair(i_proc, i_el_recv);

        local_mesh.get_overlapping_simplex(index_pair, simplex);

        for (const auto &ball : qcmesh::geometry::intersect_balls_with_simplex(
                 sampling_atom_balls_tree, simplex, max_ball_radius)) {
          // this non cauchy born sampling atom has this received element as
          // neighbour
          rcvd_elements_of_sampling_atoms[ball.index].push_back(index_pair);
        }
      }
    }
  }

  if (periodic_simulation_flag) {
    TripletDataIndex index_triplet;

    // periodic elements in other processors
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank &&
          local_mesh.periodic_mesh_data.overlapping_simplices_recv[i_proc]
                  .size() > 0) {
        for (std::size_t i_el_recv = 0;
             i_el_recv <
             local_mesh.periodic_mesh_data.overlapping_simplices_recv[i_proc]
                 .size();
             i_el_recv++) {
          index_pair = std::make_pair(i_proc, i_el_recv);

          local_mesh.get_periodic_simplex(index_pair, simplex);
          // check for the element's all offsets except for 0 offset

          for (std::size_t offset_index = 1;
               offset_index < local_mesh.periodic_offsets.size();
               offset_index++) {
            index_triplet = std::make_tuple(i_proc, i_el_recv, offset_index);

            for (std::size_t d = 0; d < DIM + 1; d++)
              array_ops::as_eigen(offset_simplex[d]) =
                  array_ops::as_eigen(simplex[d]) -
                  array_ops::as_eigen(
                      local_mesh.periodic_offsets[offset_index]);

            for (const auto &ball :
                 qcmesh::geometry::intersect_balls_with_simplex(
                     sampling_atom_balls_tree, offset_simplex,
                     max_ball_radius)) {
              // this non cauchy born sampling atom has this received element as
              // neighbour
              recv_periodic_elements_of_sampling_atoms[ball.index].push_back(
                  index_triplet);
            }
          }
        }
      }
    }
  }

#ifdef FINITE_TEMPERATURE
  std::array<std::array<double, NIDOF>, DIM + 1> thermal_simplex{};
#endif
  std::array<NodalData, DIM + 1> nodal_data_simplex;

  std::array<std::size_t, DIM + 1> indexed_cell{};
  geometry::Point<DIM> neighbour_offset;

  bool print = false;

  if (input_params.neighborhood_and_force_time) {
    neighbourhood_clock.tic();
  }

  if (!marked_nodal_sampling_atoms.empty()) {

    geometry::Point<DIM> neighbour_candidate;
    geometry::Point<DIM> closest_lattice_site;

    double distance_from_sampling_atom{};

    int local_offset_idx = 0;
    int local_simplex_idx = 0;

    int recv_rank_idx = 0;
    int recv_simplex_idx = 0;
    int recv_offset_idx = 0;
    int site_type_closest_lattice_site = 0;
    int site_type_sampling_atom = 0;

    bool added = false;

    std::vector<std::size_t>::const_iterator cell_index_it;
    std::vector<PairDataIndex>::const_iterator pair_index_it;
    std::vector<TripletDataIndex>::const_iterator triplet_index_it;

    std::size_t node_index = 0;
    std::size_t sampling_atom_index = 0;

    for (std::size_t ball_index = 0;
         ball_index < local_sampling_atom_balls.size(); ball_index++) {

      site_type_sampling_atom =
          local_mesh.repatoms.lattice_site_type[ball_index];

      print = false;

      atom_ball = local_sampling_atom_balls[ball_index].inner;

      constexpr double TOLERANCE = 1.0e-3;

      auto neighbour_set =
          NeighbourSet(Annulus{atom_ball.center, TOLERANCE, atom_ball.radius});
      const auto add_to_neighbourhood = [&neighbour_set](const auto &point) {
        return neighbour_set.insert_unique(point, neighbour_set.domain.r_inner);
      };

      // ball_index is the marker_iterator_index. get the sampling atom index
      sampling_atom_index = marked_nodal_sampling_atoms[ball_index];

      neighbour_offset.fill(0.0);

      // reserve for an approximate number of neighbour sites
      local_mesh.sampling_atoms
          .nodal_sampling_atom_neighbours[sampling_atom_index]
          .reserve(43);

      for (cell_index_it = elements_of_sampling_atoms[ball_index].cbegin();
           cell_index_it != elements_of_sampling_atoms[ball_index].cend();
           ++cell_index_it) {
        const auto cell_key = local_mesh.simplex_index_to_key[*cell_index_it];
        const auto &cell = local_mesh.cells.at(cell_key);
        const auto simplex =
            qcmesh::mesh::simplex_cell_points(local_mesh, cell);
        indexed_cell = indexed_simplex(local_mesh, cell_key);

#ifdef FINITE_TEMPERATURE
        for (std::size_t d = 0; d < DIM + 1; d++) {
          const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
          node_index = local_mesh.nodes.at(node_key).data.mesh_index;

          // omega/m is still in repatom_container
          thermal_simplex[d] =
              local_mesh.repatoms.thermal_coordinates[node_index];

          // every where its Omega/m, phi (we don't
          // want to interpolate the weights of repatoms)
        }
#endif

        for (std::size_t d = 0; d < DIM + 1; d++) {
          const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
          node_index = local_mesh.nodes.at(node_key).data.mesh_index;
          // nodal data is still in repatom_container
          nodal_data_simplex[d] =
              local_mesh.repatoms.nodal_data_points[node_index];
        }

        // local element index in the sampling atom balls
        if (cell.data.is_atomistic) {
          // just add all the repatoms/nodes of this element
          // neighbourhood_container makes sure to add uniquely
          for (std::size_t d = 0; d < DIM + 1; d++) {

            const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
            node_index = local_mesh.nodes.at(node_key).data.mesh_index;

            neighbour_candidate = local_mesh.repatoms.locations[node_index];

            added = add_to_neighbourhood(neighbour_candidate);

            if (added) {
              const auto n_j =
                  qcmesh::geometry::simplex_eval_shape_functions_interior(
                      simplex, neighbour_candidate);

              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[sampling_atom_index]
                  .emplace_back(neighbour_candidate, neighbour_offset, n_j,
#ifdef FINITE_TEMPERATURE
                                thermal_simplex,
#endif
                                nodal_data_simplex, indexed_cell, mpi_rank,
                                false, false);
            }
          }
        } else // non-atomistic simplex. Generate neighbourhood and add
        {
          // 1. get lattice point from simplex's bravais vectors closest
          // to the sampling atom by rounding the projections
          get_closest_lattice_site<DIM>(cell.data.lattice_vectors, simplex[0],
                                        atom_ball.center, closest_lattice_site);

          site_type_closest_lattice_site =
              local_mesh.repatoms.lattice_site_type
                  [local_mesh.nodes.at(local_mesh.cells.at(cell_key).nodes[0])
                       .data.mesh_index];

          // 2. generate neighbourhood within
          // radius + verlet_buffer + distance_from_sampling_atom
          // and simplex, and add uniquely
          distance_from_sampling_atom =
              array_ops::distance(closest_lattice_site, atom_ball.center);

          neighbourhoods::generate_and_add_neighbourhood<noi>(
              closest_lattice_site, site_type_closest_lattice_site,
              site_type_sampling_atom, neighbour_offset,
              distance_from_sampling_atom, atom_ball.radius + verlet_buffer,
              cell.data.lattice_vectors, cell.data.offset_vectors, simplex,
#ifdef FINITE_TEMPERATURE
              thermal_simplex,
#endif
              nodal_data_simplex, indexed_cell, mpi_rank, false, false, false,
              neighbour_set,
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[sampling_atom_index],
              print);
        }
      }

      for (const auto &pair_index :
           rcvd_elements_of_sampling_atoms[ball_index]) {
        const auto &[recv_rank_idx, recv_simplex_idx] = pair_index;
        local_mesh.get_overlapping_simplex(pair_index, simplex);

#ifdef FINITE_TEMPERATURE
        for (std::size_t d = 0; d < DIM + 1; d++)
          thermal_simplex[d] =
              local_mesh.overlapping_mesh_data.overlapping_thermal_dofs_recv
                  [recv_rank_idx]
                  [local_mesh.overlapping_mesh_data.overlapping_simplices_recv
                       [recv_rank_idx][recv_simplex_idx][d]];
#endif
        for (std::size_t d = 0; d < DIM + 1; d++)
          nodal_data_simplex[d] =
              local_mesh.overlapping_mesh_data.overlapping_nodal_data_recv
                  [recv_rank_idx]
                  [local_mesh.overlapping_mesh_data.overlapping_simplices_recv
                       [recv_rank_idx][recv_simplex_idx][d]];
        if (local_mesh.overlapping_mesh_data
                .overlapping_simplices_recv_atomistic_flag[recv_rank_idx]
                                                          [recv_simplex_idx]) {
          for (std::size_t d = 0; d < DIM + 1; d++) {
            neighbour_candidate =
                local_mesh.overlapping_mesh_data.overlapping_nodes_recv
                    [recv_rank_idx]
                    [local_mesh.overlapping_mesh_data.overlapping_simplices_recv
                         [recv_rank_idx][recv_simplex_idx][d]];

            added = add_to_neighbourhood(neighbour_candidate);
            // if added, then add its dependent node indices and
            // weights
            if (added) {

              // compute repatom dependents and add.
              const auto n_j =
                  qcmesh::geometry::simplex_eval_shape_functions_interior(
                      simplex, neighbour_candidate);

              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[sampling_atom_index]
                  .emplace_back(
                      neighbour_candidate, neighbour_offset, n_j,
#ifdef FINITE_TEMPERATURE
                      thermal_simplex,
#endif
                      nodal_data_simplex,
                      local_mesh.overlapping_mesh_data
                          .overlapping_simplices_recv[recv_rank_idx]
                                                     [recv_simplex_idx],
                      recv_rank_idx, true, false);
            }
          }
        } else // non atomistic interproc elements
        {
          // 1. get lattice point from simplex's bravais vectors closest
          // to the sampling atom by rounding the projections
          site_type_closest_lattice_site =
              local_mesh.overlapping_mesh_data.overlapping_node_site_types_recv
                  [recv_rank_idx]
                  [local_mesh.overlapping_mesh_data.overlapping_simplices_recv
                       [recv_rank_idx][recv_simplex_idx][0]];
          get_closest_lattice_site<DIM>(
              local_mesh.overlapping_mesh_data
                  .overlapping_element_lattice_vectors_recv[recv_rank_idx]
                                                           [recv_simplex_idx],
              simplex[0], atom_ball.center, closest_lattice_site);

          // 2. generate neighbourhood within
          // radius + verlet_buffer + distance_from_sampling_atom
          // and simplex, and add uniquely
          distance_from_sampling_atom =
              array_ops::distance(closest_lattice_site, atom_ball.center);

          neighbourhoods::generate_and_add_neighbourhood<noi>(
              closest_lattice_site, site_type_closest_lattice_site,
              site_type_sampling_atom, neighbour_offset,
              distance_from_sampling_atom, atom_ball.radius + verlet_buffer,
              local_mesh.overlapping_mesh_data
                  .overlapping_element_lattice_vectors_recv[recv_rank_idx]
                                                           [recv_simplex_idx],
              local_mesh.overlapping_mesh_data
                  .overlapping_element_offsets_recv[recv_rank_idx]
                                                   [recv_simplex_idx],
              simplex,
#ifdef FINITE_TEMPERATURE
              thermal_simplex,
#endif
              nodal_data_simplex,
              local_mesh.overlapping_mesh_data
                  .overlapping_simplices_recv[recv_rank_idx][recv_simplex_idx],
              recv_rank_idx, true, false, false, neighbour_set,
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[sampling_atom_index]);
        }
      }

      if (periodic_simulation_flag) {
        for (pair_index_it =
                 periodic_elements_of_sampling_atoms[ball_index].cbegin();
             pair_index_it !=
             periodic_elements_of_sampling_atoms[ball_index].cend();
             ++pair_index_it) {
          local_simplex_idx = pair_index_it->first;
          local_offset_idx = pair_index_it->second;

          const auto cell_key =
              local_mesh.simplex_index_to_key[local_simplex_idx];
          const auto &local_cell = local_mesh.cells.at(cell_key);
          const auto simplex =
              qcmesh::mesh::simplex_cell_points(local_mesh, local_cell);

          for (std::size_t d = 0; d < DIM + 1; d++)
            array_ops::as_eigen(offset_simplex[d]) =
                array_ops::as_eigen(simplex[d]) -
                array_ops::as_eigen(
                    local_mesh.periodic_offsets[local_offset_idx]);

          neighbour_offset = local_mesh.periodic_offsets[local_offset_idx];

          indexed_cell = indexed_simplex(local_mesh, cell_key);

#ifdef FINITE_TEMPERATURE
          for (std::size_t d = 0; d < DIM + 1; d++) {
            const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
            node_index = local_mesh.nodes.at(node_key).data.mesh_index;

            // omega/m is still in repatom_container
            thermal_simplex[d] =
                local_mesh.repatoms.thermal_coordinates[node_index];

            // every where its Omega/m, phi (we don't
            // want to interpolate the weights of repatoms)
          }
#endif
          for (std::size_t d = 0; d < DIM + 1; d++) {
            const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
            node_index = local_mesh.nodes.at(node_key).data.mesh_index;
            // impurity concentrations are still in repatom_container
            nodal_data_simplex[d] =
                local_mesh.repatoms.nodal_data_points[node_index];
          }
          // local element index in the sampling atom balls
          if (local_cell.data.is_atomistic) {
            // just add all the repatoms/nodes of this element
            // neighbourhood_container makes sure to add uniquely
            for (std::size_t d = 0; d < DIM + 1; d++) {
              const auto node_key = local_mesh.cells.at(cell_key).nodes[d];
              node_index = local_mesh.nodes.at(node_key).data.mesh_index;

              array_ops::as_eigen(neighbour_candidate) =
                  array_ops::as_eigen(
                      local_mesh.repatoms.locations[node_index]) -
                  array_ops::as_eigen(
                      local_mesh.periodic_offsets[local_offset_idx]);

              added = add_to_neighbourhood(neighbour_candidate);

              if (added) {
                const auto n_j =
                    qcmesh::geometry::simplex_eval_shape_functions_interior(
                        offset_simplex, neighbour_candidate);

                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[sampling_atom_index]
                    .emplace_back(neighbour_candidate, neighbour_offset, n_j,
#ifdef FINITE_TEMPERATURE
                                  thermal_simplex,
#endif
                                  nodal_data_simplex, indexed_cell, mpi_rank,
                                  false, true);
              }
            }
          } else // non-atomistic simplex. Generate neighbourhood and add
          {
            // 1. get lattice point from simplex's bravais vectors closest
            // to the sampling atom by rounding the projections
            site_type_closest_lattice_site =
                local_mesh.repatoms.lattice_site_type
                    [local_mesh.nodes.at(local_mesh.cells.at(cell_key).nodes[0])
                         .data.mesh_index];
            get_closest_lattice_site<DIM>(local_cell.data.lattice_vectors,
                                          offset_simplex[0], atom_ball.center,
                                          closest_lattice_site);

            // 2. generate neighbourhood within
            // radius + verlet_buffer + distance_from_sampling_atom
            // and simplex, and add uniquely
            distance_from_sampling_atom =
                array_ops::distance(closest_lattice_site, atom_ball.center);

            neighbourhoods::generate_and_add_neighbourhood<noi>(
                closest_lattice_site, site_type_closest_lattice_site,
                site_type_sampling_atom, neighbour_offset,
                distance_from_sampling_atom, atom_ball.radius + verlet_buffer,
                local_cell.data.lattice_vectors, local_cell.data.offset_vectors,
                offset_simplex,
#ifdef FINITE_TEMPERATURE
                thermal_simplex,
#endif
                nodal_data_simplex, indexed_cell, mpi_rank, false, true, false,
                neighbour_set,
                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[sampling_atom_index]);
          }
        }

        if (!recv_periodic_elements_of_sampling_atoms[ball_index].empty()) {
          for (triplet_index_it =
                   recv_periodic_elements_of_sampling_atoms[ball_index]
                       .cbegin();
               triplet_index_it !=
               recv_periodic_elements_of_sampling_atoms[ball_index].cend();
               ++triplet_index_it) {
            recv_rank_idx = std::get<0>(*triplet_index_it);
            recv_simplex_idx = std::get<1>(*triplet_index_it);
            recv_offset_idx = std::get<2>(*triplet_index_it);

            local_mesh.get_periodic_simplex(
                std::make_pair(recv_rank_idx, recv_simplex_idx), simplex);

            for (std::size_t d = 0; d < DIM + 1; d++)
              array_ops::as_eigen(offset_simplex[d]) =
                  array_ops::as_eigen(simplex[d]) -
                  array_ops::as_eigen(
                      local_mesh.periodic_offsets[recv_offset_idx]);

            neighbour_offset = local_mesh.periodic_offsets[recv_offset_idx];

#ifdef FINITE_TEMPERATURE
            for (std::size_t d = 0; d < DIM + 1; d++) {
              thermal_simplex[d] =
                  local_mesh.periodic_mesh_data.overlapping_thermal_dofs_recv
                      [recv_rank_idx]
                      [local_mesh.periodic_mesh_data.overlapping_simplices_recv
                           [recv_rank_idx][recv_simplex_idx][d]];
            }
#endif
            for (std::size_t d = 0; d < DIM + 1; d++) {
              nodal_data_simplex[d] =
                  local_mesh.periodic_mesh_data.overlapping_nodal_data_recv
                      [recv_rank_idx]
                      [local_mesh.periodic_mesh_data.overlapping_simplices_recv
                           [recv_rank_idx][recv_simplex_idx][d]];
            }
            if (local_mesh.periodic_mesh_data
                    .overlapping_simplices_recv_atomistic_flag
                        [recv_rank_idx][recv_simplex_idx]) {
              for (std::size_t d = 0; d < DIM + 1; d++) {
                // subtract the offset so that the neighbour candidate is within
                // the range
                neighbour_candidate = offset_simplex[d];

                added = add_to_neighbourhood(neighbour_candidate);
                // if added, then add its dependent node indices and
                // weights
                if (added) {
                  // compute repatom dependents and add.
                  const auto n_j =
                      qcmesh::geometry::simplex_eval_shape_functions_interior(
                          offset_simplex, neighbour_candidate);

                  local_mesh.sampling_atoms
                      .nodal_sampling_atom_neighbours[sampling_atom_index]
                      .emplace_back(
                          neighbour_candidate, neighbour_offset, n_j,
#ifdef FINITE_TEMPERATURE
                          thermal_simplex,
#endif
                          nodal_data_simplex,
                          local_mesh.periodic_mesh_data
                              .overlapping_simplices_recv[recv_rank_idx]
                                                         [recv_simplex_idx],
                          recv_rank_idx, true, true);
                }
              }
            } else // non atomistic interproc elements
            {
              // 1. get lattice point from simplex's bravais vectors closest
              // to the sampling atom by rounding the projections
              site_type_closest_lattice_site =
                  local_mesh.periodic_mesh_data.overlapping_node_site_types_recv
                      [recv_rank_idx]
                      [local_mesh.periodic_mesh_data.overlapping_simplices_recv
                           [recv_rank_idx][recv_simplex_idx][0]];
              get_closest_lattice_site<DIM>(
                  local_mesh.periodic_mesh_data
                      .overlapping_element_lattice_vectors_recv
                          [recv_rank_idx][recv_simplex_idx],
                  offset_simplex[0], atom_ball.center, closest_lattice_site);

              // 2. generate neighbourhood within
              // radius + verlet_buffer + distance_from_sampling_atom
              // and simplex, and add uniquely
              distance_from_sampling_atom =
                  array_ops::distance(closest_lattice_site, atom_ball.center);

              neighbourhoods::generate_and_add_neighbourhood<noi>(
                  closest_lattice_site, site_type_closest_lattice_site,
                  site_type_sampling_atom, neighbour_offset,
                  distance_from_sampling_atom, atom_ball.radius + verlet_buffer,
                  local_mesh.periodic_mesh_data
                      .overlapping_element_lattice_vectors_recv
                          [recv_rank_idx][recv_simplex_idx],
                  local_mesh.periodic_mesh_data
                      .overlapping_element_offsets_recv[recv_rank_idx]
                                                       [recv_simplex_idx],
                  offset_simplex,
#ifdef FINITE_TEMPERATURE
                  thermal_simplex,
#endif
                  nodal_data_simplex,
                  local_mesh.periodic_mesh_data
                      .overlapping_simplices_recv[recv_rank_idx]
                                                 [recv_simplex_idx],
                  recv_rank_idx, true, true, false, neighbour_set,
                  local_mesh.sampling_atoms
                      .nodal_sampling_atom_neighbours[sampling_atom_index],
                  print);
            }
          }
        }
      }

      local_mesh.sampling_atoms
          .nodal_sampling_atom_neighbours[sampling_atom_index]
          .shrink_to_fit();

      if (input_params.display_neighbourhoods_sampling_atoms.size() > 0) {
        std::filesystem::path neighbourhoods_folder = "neighbourhoods/";
        std::filesystem::create_directories(neighbourhoods_folder);
        std::array<std::size_t, 4> indexed_simplex{};

        for (const auto &p_test :
             input_params.display_neighbourhoods_sampling_atoms) {

          const auto epsilon = array_ops::distance(
              local_mesh.sampling_atoms.nodal_locations[sampling_atom_index],
              p_test);

          if (epsilon < 1.0) {

            std::vector<geometry::Point<DIM>> display_neighbourhoods{};

            display_neighbourhoods.push_back(
                local_mesh.sampling_atoms.nodal_locations[sampling_atom_index]);

            for (std::size_t n = 0;
                 n < local_mesh.sampling_atoms
                         .nodal_sampling_atom_neighbours[sampling_atom_index]
                         .size();
                 n++) {
              display_neighbourhoods.push_back(
                  local_mesh.sampling_atoms
                      .nodal_sampling_atom_neighbours[sampling_atom_index][n]
                      .location);
            }

            std::vector<std::array<std::size_t, DIM + 1>> tets{};
            std::vector<geometry::Point<DIM>> points{};

            std::size_t node_id = 0;
            std::size_t cell_id = 0;

            // add local elements
            for (const auto &cell_index :
                 elements_of_sampling_atoms[ball_index]) {
              const auto cell_key = local_mesh.simplex_index_to_key[cell_index];
              const auto simplex =
                  qcmesh::mesh::simplex_cell_points(local_mesh, cell_key);

              for (std::size_t d = 0; d < DIM + 1; d++) {
                points.push_back(simplex[d]);
                indexed_simplex[d] = node_id;
                node_id++;
              }
              tets.push_back(indexed_simplex);
              cell_id++;
            }

            // add local periodic elements
            for (const auto &pair_index :
                 periodic_elements_of_sampling_atoms[ball_index]) {
              const auto &[local_simplex_idx, local_offset_idx] = pair_index;

              const auto cell_key =
                  local_mesh.simplex_index_to_key[local_simplex_idx];
              const auto simplex =
                  qcmesh::mesh::simplex_cell_points(local_mesh, cell_key);

              for (std::size_t d = 0; d < DIM + 1; d++)
                array_ops::as_eigen(offset_simplex[d]) =
                    array_ops::as_eigen(simplex[d]) -
                    array_ops::as_eigen(
                        local_mesh.periodic_offsets[local_offset_idx]);

              for (std::size_t d = 0; d < DIM + 1; d++) {
                points.push_back(offset_simplex[d]);
                indexed_simplex[d] = node_id;
                node_id++;
              }

              tets.push_back(indexed_simplex);
              cell_id++;
            }

            for (const auto &pair_index :
                 rcvd_elements_of_sampling_atoms[ball_index]) {

              local_mesh.get_overlapping_simplex(pair_index, simplex);

              for (std::size_t d = 0; d < DIM + 1; d++) {
                points.push_back(simplex[d]);
                indexed_simplex[d] = node_id;
                node_id++;
              }

              tets.push_back(indexed_simplex);
              cell_id++;
            }

            for (triplet_index_it =
                     recv_periodic_elements_of_sampling_atoms[ball_index]
                         .cbegin();
                 triplet_index_it !=
                 recv_periodic_elements_of_sampling_atoms[ball_index].cend();
                 ++triplet_index_it) {

              recv_rank_idx = std::get<0>(*triplet_index_it);
              recv_simplex_idx = std::get<1>(*triplet_index_it);
              recv_offset_idx = std::get<2>(*triplet_index_it);

              local_mesh.get_periodic_simplex(
                  std::make_pair(recv_rank_idx, recv_simplex_idx), simplex);

              for (std::size_t d = 0; d < DIM + 1; d++) {
                array_ops::as_eigen(offset_simplex[d]) =
                    array_ops::as_eigen(simplex[d]) -
                    array_ops::as_eigen(
                        local_mesh.periodic_offsets[recv_offset_idx]);
              }

              for (std::size_t d = 0; d < DIM + 1; d++) {
                points.push_back(offset_simplex[d]);
                indexed_simplex[d] = node_id;
                node_id++;
              }

              tets.push_back(indexed_simplex);
              cell_id++;
            }

            vtkwriter::write_vtu_triangulation(
                display_neighbourhoods, tets, std::nullopt,
                std::filesystem::path{
                    "./neighbourhoods/neighbourhoods_display" +
                    std::to_string(mpi_rank) + ".vtu"});
          }
        }
      }
    }
  }
  // ---------- </nodal sampling atoms >-----------------//

  // ---------- <Cauchy Born sampling atoms neighbourhoods>-----------------//

  for (const auto central_index : marked_cb_sampling_atoms) {
    const auto element_key_central_atom =
        local_mesh.sampling_atoms.cell_key_of_central_atoms[central_index];
    const auto central_atom_cell =
        local_mesh.cells.at(element_key_central_atom);
    const auto simplex =
        qcmesh::mesh::simplex_cell_points(local_mesh, central_atom_cell);

    indexed_cell = indexed_simplex(local_mesh, element_key_central_atom);

    neighbour_offset.fill(0.0);

#ifdef FINITE_TEMPERATURE
    for (std::size_t d = 0; d < DIM + 1; d++) {
      thermal_simplex[d] =
          local_mesh.repatoms.thermal_coordinates[indexed_cell[d]];
    }
#endif
    for (std::size_t d = 0; d < DIM + 1; d++) {
      nodal_data_simplex[d] =
          local_mesh.repatoms.nodal_data_points[indexed_cell[d]];
    }

    local_mesh.sampling_atoms.central_sampling_atom_neighbours[central_index]
        .clear();

    auto dummy_neighbour_set = NeighbourSet(Annulus{});

    neighbourhoods::generate_and_add_neighbourhood<noi>(
        local_mesh.sampling_atoms.central_locations[central_index], 0, 0,
        neighbour_offset, 0.0,
        local_mesh.sampling_atoms.central_cut_offs[central_index] +
            input_params.verlet_buffer_factor *
                input_params.neighbour_update_displacement +
            verlet_buffer,
        central_atom_cell.data.lattice_vectors,
        central_atom_cell.data.offset_vectors, simplex,
#ifdef FINITE_TEMPERATURE
        thermal_simplex,
#endif
        nodal_data_simplex, indexed_cell, mpi_rank, false, false, true,
        dummy_neighbour_set,
        local_mesh.sampling_atoms
            .central_sampling_atom_neighbours[central_index]);
  }

  if (input_params.neighborhood_and_force_time) {
    const auto elapsed_time = neighbourhood_clock.toc();

    std::cout << "rank - " << mpi_rank << " took " << elapsed_time
              << " in generating neighbours" << std::endl;
  }

  // ---------- </Cauchy Born sampling atoms neighbourhoods>-----------------//
}
} // namespace neighbourhoods
