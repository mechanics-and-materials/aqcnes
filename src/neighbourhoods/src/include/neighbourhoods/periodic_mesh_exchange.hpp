// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Low level function defintions for periodic mesh data exchange
 * @author P. Gupta, S. Saxena
 */

#pragma once

#include "geometry/point.hpp"
#include "meshing/nodal_data_interproc.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/indexed.hpp"
#include "qcmesh/geometry/intersect_balls_with_simplex.hpp"
#include "qcmesh/geometry/octree.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi.hpp>

namespace neighbourhoods {

template <std::size_t DIM>
std::tuple<qcmesh::geometry::octree::Octree<
               qcmesh::geometry::Indexed<std::array<double, DIM>>>,
           std::vector<qcmesh::geometry::Indexed<qcmesh::geometry::Ball<DIM>>>,
           std::vector<std::vector<int>>>
build_unique_balls(const std::vector<std::vector<double>> &balls_data_all_proc,
                   const std::vector<std::size_t> &noof_periodic_balls_by_proc,
                   const qcmesh::geometry::Box<DIM> &domain) {
  auto unique_balls = qcmesh::geometry::octree::Octree<
      qcmesh::geometry::Indexed<std::array<double, DIM>>>(domain);
  auto balls_all_proc =
      std::vector<qcmesh::geometry::Indexed<qcmesh::geometry::Ball<DIM>>>{};
  auto ranks_of_unique_balls = std::vector<std::vector<int>>{};

  std::size_t noof_balls_all_proc = 0.0;
  for (const auto noof_periodic_balls : noof_periodic_balls_by_proc)
    noof_balls_all_proc += noof_periodic_balls;

  ranks_of_unique_balls.reserve(noof_balls_all_proc);
  balls_all_proc.reserve(noof_balls_all_proc);

  for (std::size_t ball_rank = 0; ball_rank < balls_data_all_proc.size();
       ball_rank++) {
    std::size_t ball_data_ctr = 0;
    for (std::size_t ball_ctr = 0;
         ball_ctr < noof_periodic_balls_by_proc[ball_rank]; ball_ctr++) {

      auto interproc_ball = qcmesh::geometry::Ball<DIM>{};

      for (auto &c : interproc_ball.center)
        c = balls_data_all_proc[ball_rank][ball_data_ctr++];
      interproc_ball.radius = balls_data_all_proc[ball_rank][ball_data_ctr++];

      // do all the periodic offsets of each ball. Store the offset
      // for each rank. The offset balls need to be tested if within the domain
      // also.

      // Or if I assume that each point's offset has an image, I can just
      // search for every ball's offset in the boundary point tree. Yes
      // this makes more sense.

      constexpr double TOLERANCE = 0.1;
      auto collisions = unique_balls.find_or_insert(
          qcmesh::geometry::Indexed<std::array<double, DIM>>{
              unique_balls.size(), interproc_ball.center},
          TOLERANCE);

      if (collisions.empty()) {
        balls_all_proc.push_back(
            qcmesh::geometry::Indexed<qcmesh::geometry::Ball<DIM>>{
                balls_all_proc.size(), interproc_ball});
        ranks_of_unique_balls.push_back(
            std::vector<int>{static_cast<int>(ball_rank)});
      } else
        for (const auto &collision : collisions)
          ranks_of_unique_balls[collision.index].push_back(
              static_cast<int>(ball_rank));
    }
  }
  return std::make_tuple(std::move(unique_balls), std::move(balls_all_proc),
                         std::move(ranks_of_unique_balls));
}

template <typename Mesh, std::size_t DIM>
void gather_periodic_element_periodic_nodes(
    const std::vector<std::vector<double>> &balls_data_all_proc,
    const std::vector<std::size_t> &noof_periodic_balls_by_proc,
    Mesh &local_mesh,
    std::vector<std::vector<std::size_t>> &added_simplex_idxs_for_sending) {

  namespace array_ops = qcmesh::array_ops;
  auto [unique_balls, balls_all_proc, ranks_of_unique_balls] =
      build_unique_balls<DIM>(balls_data_all_proc, noof_periodic_balls_by_proc,
                              local_mesh.domain_bound);

  auto interproc_balls = qcmesh::geometry::kd_tree::KDTree(balls_all_proc);

  const auto mpi_size = boost::mpi::communicator{}.size();

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    added_simplex_idxs_for_sending[i_proc].reserve(local_mesh.cells.size() /
                                                   mpi_size);
  }

  // store all elements that are in the neighbourhood of boundary balls
  std::vector<std::vector<std::size_t>> elements_of_unique_balls(
      unique_balls.size(), std::vector<std::size_t>{});

  for (std::size_t i_el = 0; i_el != local_mesh.cells.size(); ++i_el) {
    const auto simplex = qcmesh::mesh::simplex_cell_points(
        local_mesh, local_mesh.simplex_index_to_key[i_el]);

    for (const auto &ball : qcmesh::geometry::intersect_balls_with_simplex(
             interproc_balls, simplex,
             qcmesh::geometry::max_radius(balls_all_proc)))
      elements_of_unique_balls[ball.index].push_back(i_el);
  }

  geometry::Point<DIM> periodic_image;

  // for every boundary point, get its image, and
  // its image elements' nodes are the points to send. We apply -offset while
  // checking for neighbours and overlapping elements.
  for (std::size_t idx_ball = 0; idx_ball < balls_all_proc.size(); idx_ball++) {
    for (std::size_t offset_index = 1;
         offset_index < local_mesh.periodic_offsets.size(); offset_index++) {
      array_ops::as_eigen(periodic_image) =
          array_ops::as_eigen(balls_all_proc[idx_ball].inner.center) +
          array_ops::as_eigen(local_mesh.periodic_offsets[offset_index]);

      constexpr double TOLERANCE = 0.1;
      auto periodic_images = unique_balls.find(periodic_image, TOLERANCE);

      for (const auto &image : periodic_images) {
        // elements_of_unique_balls[imageIndex] are the elements to be sent
        // to ranks_of_unique_balls[idx_ball]

        // just send single offset. While detecting neighbours, we
        // check and subtract all offsets.

        for (const auto unique_ball_rank : ranks_of_unique_balls[idx_ball]) {
          for (const auto i_el : elements_of_unique_balls[image.index]) {
            // need to mark if this element has been sent to unique_ball_rank
            // earlier
            if (!local_mesh.periodic_mesh_data
                     .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el]) {
              // mark this element as sent, so we dont send again and again!
              local_mesh.periodic_mesh_data
                  .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el] =
                  true;

              added_simplex_idxs_for_sending[unique_ball_rank].push_back(i_el);
            }
          }
        }
      }
    }
  }
}

template <typename Mesh, std::size_t DIM>
void gather_periodic_element_inner_nodes(
    const std::vector<std::vector<double>> &balls_data_all_proc,
    const std::vector<std::size_t> &noof_periodic_balls_by_proc,
    Mesh &local_mesh,
    std::vector<std::vector<std::size_t>> &added_simplex_idxs_for_sending) {
  namespace array_ops = qcmesh::array_ops;
  using Simplex = std::array<geometry::Point<DIM>, DIM + 1>;

  auto [unique_balls, balls_all_proc, ranks_of_unique_balls] =
      build_unique_balls<DIM>(balls_data_all_proc, noof_periodic_balls_by_proc,
                              local_mesh.domain_bound);

  auto interproc_balls = qcmesh::geometry::kd_tree::KDTree(balls_all_proc);

  const auto mpi_size = boost::mpi::communicator{}.size();

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    added_simplex_idxs_for_sending[i_proc].reserve(local_mesh.cells.size() /
                                                   mpi_size);
  }

  Simplex offset_simplex;

  for (std::size_t i_el = 0; i_el != local_mesh.cells.size(); ++i_el) {
    const auto simplex = qcmesh::mesh::simplex_cell_points(
        local_mesh, local_mesh.simplex_index_to_key[i_el]);

    for (std::size_t offset_index = 1;
         offset_index < local_mesh.periodic_offsets.size(); offset_index++) {

      for (std::size_t d = 0; d < DIM + 1; d++) {
        array_ops::as_eigen(offset_simplex[d]) =
            array_ops::as_eigen(simplex[d]) -
            array_ops::as_eigen(local_mesh.periodic_offsets[offset_index]);
      }

      for (const auto &ball : qcmesh::geometry::intersect_balls_with_simplex(
               interproc_balls, offset_simplex,
               qcmesh::geometry::max_radius(balls_all_proc))) {
        for (const auto unique_ball_rank : ranks_of_unique_balls[ball.index]) {
          // need to mark if this element has been sent to unique_ball_rank
          // earlier
          if (!local_mesh.periodic_mesh_data
                   .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el]) {
            // mark this element as sent, so we dont send again and again!
            local_mesh.periodic_mesh_data
                .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el] = true;

            added_simplex_idxs_for_sending[unique_ball_rank].push_back(i_el);
          }
        }
      }
    }
  }
}

template <typename Mesh, std::size_t DIM, std::size_t NIDOF, typename NodalData>
void update_periodic_mesh_elements_and_nodes(
    Mesh &local_mesh,
    const std::vector<qcmesh::geometry::Ball<DIM>> &periodic_balls,
    const bool if_boundary_atoms, const bool print_to_console) {
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  // If refinements happened,
  // then we re-initialize the mesh overlaps. Otherwise there is too much book
  // keeping.
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  // -even if we end up sending duplicate points, its fine. We should not send
  // duplicate elements.
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  using Point = geometry::Point<DIM>;
  using Offset = std::vector<std::array<double, DIM>>;

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  constexpr std::size_t noi = meshing::traits::N_IMPURITIES<NodalData>;
#ifdef FINITE_TEMPERATURE
  constexpr std::size_t nidof = NIDOF;
#else
  constexpr std::size_t nidof = 0;
#endif

  std::vector<std::size_t> noof_periodic_balls_by_proc(mpi_size, 0);
  std::vector<double> balls_data_to_bcast;

  std::size_t noof_periodic_sampling_atoms = periodic_balls.size();

  balls_data_to_bcast.reserve(noof_periodic_sampling_atoms * (DIM + 1));
  // prepare balls to broadcast
  for (std::size_t i = 0; i < periodic_balls.size(); i++) {
    for (std::size_t d = 0; d < DIM; d++) {
      balls_data_to_bcast.push_back(periodic_balls[i].center[d]);
    }
    balls_data_to_bcast.push_back(periodic_balls[i].radius);
  }

  // all-gather amount of interproc balls-data to exchange
  boost::mpi::all_gather(world, noof_periodic_sampling_atoms,
                         noof_periodic_balls_by_proc);

  std::vector<std::vector<double>> balls_data_all_proc(mpi_size);
  boost::mpi::all_gather(world, balls_data_to_bcast, balls_data_all_proc);

  std::vector<std::vector<std::size_t>> added_simplex_idxs_for_sending(
      mpi_size);

  if (if_boundary_atoms) {
    gather_periodic_element_periodic_nodes<Mesh, DIM>(
        balls_data_all_proc, noof_periodic_balls_by_proc, local_mesh,
        added_simplex_idxs_for_sending);
  } else {
    gather_periodic_element_inner_nodes<Mesh, DIM>(
        balls_data_all_proc, noof_periodic_balls_by_proc, local_mesh,
        added_simplex_idxs_for_sending);
  }

  std::vector<std::vector<std::size_t>> added_node_idxs_for_sending(mpi_size);
  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    added_node_idxs_for_sending[i_proc].reserve(local_mesh.nodes.size() /
                                                mpi_size);
  }

  //=-=-=-=- Build a tree once for all the received balls-=-=-=-=-//
  // get the elements that intersect other's balls (not our own)
  // Try to make sure that the balls are mostly unique. If the KDTree
  // fails, then the leafsize of KDTree needs to be increased. Will happen
  // if too many points are being repeated. We need the unique container!

  // now prepare newly added simplices' data for sending
  // now we have simplex ids that need to be sent. We have to send
  // one by one and receive, with traditional non-blocking MPI_Isend
  // data to be sent is nodes (coordinates), lattice vectors,
  // atomistic flags.
  std::vector<std::vector<double>> all_send_double_data(mpi_size);
  std::vector<std::vector<int>> all_send_int_data(mpi_size);

  std::vector<int> noof_simplices_send(mpi_size, 0);
  std::vector<int> noof_nodes_send(mpi_size, 0);

  std::vector<double> meshnodes_send;
  std::vector<int> meshnodes_int_data_send;

  int noof_int_data_per_node = 2;

  meshnodes_send.reserve(local_mesh.nodes.size() * DIM +
                         nidof * local_mesh.nodes.size() +
                         noi * local_mesh.nodes.size());

  meshnodes_int_data_send.reserve(local_mesh.nodes.size() *
                                  noof_int_data_per_node);

  // we have to send only 2 quantities (omega and sigma)

  std::size_t node_index = 0;
  std::size_t sent_index = 0;
  geometry::Point<DIM> node_point;

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank && !added_simplex_idxs_for_sending[i_proc].empty()) {

      noof_simplices_send[i_proc] =
          static_cast<int>(added_simplex_idxs_for_sending[i_proc].size());

      all_send_double_data[i_proc].reserve(noof_simplices_send[i_proc] *
                                               (DIM * DIM) +
                                           local_mesh.nodes.size() * DIM);

      all_send_int_data[i_proc].reserve(
          noof_simplices_send[i_proc] * (DIM + 1 + 1) +
          local_mesh.nodes.size() * noof_int_data_per_node);

      meshnodes_send.clear();
      meshnodes_int_data_send.clear();

      for (const auto idx : added_simplex_idxs_for_sending[i_proc]) {
        const auto cell_key = local_mesh.simplex_index_to_key[idx];
        const auto &cell = local_mesh.cells.at(cell_key);

        for (std::size_t d = 0; d < DIM + 1; d++) {
          const auto node_key = cell.nodes[d];
          node_index = local_mesh.nodes.at(node_key).data.mesh_index;

          if (local_mesh.periodic_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .find(static_cast<int>(node_key)) ==
              local_mesh.periodic_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .end()) {
            sent_index = local_mesh.periodic_mesh_data
                             .overlap_nodes_sent_idx_recv_side[i_proc]
                             .size();

            local_mesh.periodic_mesh_data
                .overlap_nodes_sent_idx_recv_side[i_proc]
                .insert(std::make_pair(static_cast<int>(node_key),
                                       static_cast<int>(sent_index)));

            added_node_idxs_for_sending[i_proc].push_back(node_index);

            node_point = local_mesh.nodes.at(node_key).position;

            for (std::size_t l = 0; l < DIM; l++) {
              // DIM number of coordinates
              meshnodes_send.push_back(node_point[l]);
            }

            meshnodes_int_data_send.push_back(
                local_mesh.material_idx_of_nodes[node_index]);
            meshnodes_int_data_send.push_back(
                local_mesh.repatoms.lattice_site_type[node_index]);

#ifdef FINITE_TEMPERATURE
            // send omega/m and sigma

            meshnodes_send.push_back(
                local_mesh.repatoms.thermal_coordinates[node_index][0]);

            meshnodes_send.push_back(
                local_mesh.repatoms.thermal_coordinates[node_index][1]);
#endif

            // send impurity concentrations
            for (std::size_t l = 0; l < noi; l++)
              meshnodes_send.push_back(
                  local_mesh.repatoms.nodal_data_points[node_index]
                      .impurity_concentrations[l]);

            // we recieve back forces in this order and just add to these
            // repatoms

            noof_nodes_send[i_proc]++;
          }
          // DIM+1 number of vertex indices

          all_send_int_data[i_proc].push_back(
              local_mesh.periodic_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .at(static_cast<int>(node_key)));
        }

        all_send_int_data[i_proc].push_back(cell.data.is_atomistic);

        // DIM*DIM lattice vector components
        for (std::size_t m = 0; m < DIM; m++) {
          for (std::size_t n = 0; n < DIM; n++) {
            all_send_double_data[i_proc].push_back(
                cell.data.lattice_vectors[m][n]);
          }
        }
      }

      for (const auto &d : meshnodes_send) {
        all_send_double_data[i_proc].push_back(d);
      }

      for (const auto &d : meshnodes_int_data_send) {
        all_send_int_data[i_proc].push_back(d);
      }
    }
  }

  if (print_to_console) {
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank) {
        std::cout << "periodic mesh exchange: " << mpi_rank << " sending "
                  << noof_simplices_send[i_proc] << " simplices and "
                  << noof_nodes_send[i_proc] << " nodes to " << i_proc
                  << std::endl;
      }
    }
  }

  std::vector<double>().swap(meshnodes_send);
  std::vector<int> noof_simplices_recv(mpi_size, 0);
  std::vector<int> noof_nodes_recv(mpi_size, 0);

  noof_simplices_recv = qcmesh::mpi::all_scatter(noof_simplices_send);
  noof_nodes_recv = qcmesh::mpi::all_scatter(noof_nodes_send);

  if (print_to_console) {
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank) {
        std::cout << "periodic mesh exchange: " << mpi_rank << " receiving "
                  << noof_simplices_recv[i_proc] << " simplices and "
                  << noof_nodes_recv[i_proc] << " nodes from " << i_proc
                  << std::endl;
      }
    }
  }

  std::vector<std::vector<double>> all_recv_double_data(mpi_size);
  std::vector<std::vector<int>> all_recv_int_data(mpi_size);
  for (int i = 0; i < mpi_size; i++) {
    all_recv_double_data[i].resize(
        (DIM * DIM) * noof_simplices_recv[i] + (DIM)*noof_nodes_recv[i] +
        nidof * noof_nodes_recv[i] + noi * noof_nodes_recv[i]);

    all_recv_int_data[i].resize(
        (DIM + 1 + 1) * noof_simplices_recv[i] +
        static_cast<uint64_t>(noof_int_data_per_node * noof_nodes_recv[i]));
  }

  qcmesh::mpi::all_scatter(all_send_int_data, all_recv_int_data);
  qcmesh::mpi::all_scatter(all_send_double_data, all_recv_double_data);

  int simplex_data_offset = 0;
  int simplex_int_data_offset = 0;

  std::array<std::size_t, DIM + 1> recv_simplex{};
  Point recv_node;
  std::array<std::array<double, DIM>, DIM> recv_matrix{};
  Offset offset;

  int node_int_data_idx_offset = 2;

#ifdef FINITE_TEMPERATURE
  std::array<double, NIDOF> recv_thermal_dof{};
#endif

  constexpr std::size_t node_data_idx_rho_offset = DIM + nidof;
  constexpr std::size_t node_data_idx_offset = DIM + nidof + noi;

  NodalData recv_nodal_data;

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank && noof_simplices_recv[i_proc] > 0) {

      local_mesh.periodic_mesh_data.overlapping_simplices_recv[i_proc].reserve(
          local_mesh.periodic_mesh_data.overlapping_simplices_recv[i_proc]
              .size() +
          noof_simplices_recv[i_proc]);

      local_mesh.periodic_mesh_data
          .overlapping_element_lattice_vectors_recv[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_element_lattice_vectors_recv[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      local_mesh.periodic_mesh_data.overlapping_element_offsets_recv[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_element_offsets_recv[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      local_mesh.periodic_mesh_data
          .overlapping_simplices_recv_atomistic_flag[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_simplices_recv_atomistic_flag[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      // directly append into the relevant data vectors
      for (int i_el_recv = 0; i_el_recv < noof_simplices_recv[i_proc];
           i_el_recv++) {
        offset.clear();
        for (std::size_t d = 0; d < DIM; d++) {
          for (std::size_t l = 0; l < DIM; l++) {
            recv_matrix[d][l] =
                all_recv_double_data[i_proc]
                                    [DIM * DIM * i_el_recv + d * DIM + l];
          }
        }

        local_mesh.periodic_mesh_data
            .overlapping_element_lattice_vectors_recv[i_proc]
            .push_back(recv_matrix);

        //compute offsets from the received lattice vectors

        offset.resize(local_mesh.element_offset_coeffs.size());

        for (std::size_t p = 0; p < local_mesh.element_offset_coeffs.size();
             p++) {
          for (std::size_t d = 0; d < DIM; d++) {
            for (std::size_t e = 0; e < DIM; e++) {
              offset[p][d] +=
                  local_mesh.element_offset_coeffs[p][e] * recv_matrix[d][e];
            }
          }
        }
        local_mesh.periodic_mesh_data.overlapping_element_offsets_recv[i_proc]
            .push_back(offset);

        for (std::size_t d = 0; d < DIM + 1; d++) {
          recv_simplex[d] =
              all_recv_int_data[i_proc][(DIM + 1 + 1) * i_el_recv + d];
        }

        local_mesh.periodic_mesh_data.overlapping_simplices_recv[i_proc]
            .push_back(recv_simplex);

        local_mesh.periodic_mesh_data
            .overlapping_simplices_recv_atomistic_flag[i_proc]
            .push_back(
                all_recv_int_data[i_proc][(DIM + 1 + 1) * i_el_recv + DIM + 1]);
      }

      simplex_data_offset = DIM * DIM * noof_simplices_recv[i_proc];
      simplex_int_data_offset = (DIM + 1 + 1) * noof_simplices_recv[i_proc];

      local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].reserve(
          local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].size() +
          noof_nodes_recv[i_proc]);

      local_mesh.periodic_mesh_data.overlapping_node_material_ids_recv[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_node_material_ids_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);

      local_mesh.periodic_mesh_data.overlapping_node_site_types_recv[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_node_site_types_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);

#ifdef FINITE_TEMPERATURE
      local_mesh.periodic_mesh_data.overlapping_thermal_dofs_recv[i_proc]
          .reserve(local_mesh.periodic_mesh_data
                       .overlapping_thermal_dofs_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);
#endif

      local_mesh.periodic_mesh_data.overlapping_nodal_data_recv[i_proc].reserve(
          local_mesh.periodic_mesh_data.overlapping_nodal_data_recv[i_proc]
              .size() +
          noof_nodes_recv[i_proc]);

      for (int ind_recv = 0; ind_recv < noof_nodes_recv[i_proc]; ind_recv++) {
        recv_node.fill(0.0);
#ifdef FINITE_TEMPERATURE
        recv_thermal_dof.fill(0.0);
#endif

        for (std::size_t d = 0; d < DIM; d++) {
          recv_node[d] =
              all_recv_double_data[i_proc][simplex_data_offset +
                                           node_data_idx_offset * ind_recv + d];
        }

        local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].push_back(
            recv_node);

        local_mesh.periodic_mesh_data.overlapping_node_material_ids_recv[i_proc]
            .push_back(
                all_recv_int_data[i_proc][simplex_int_data_offset +
                                          node_int_data_idx_offset * ind_recv]);

        local_mesh.periodic_mesh_data.overlapping_node_site_types_recv[i_proc]
            .push_back(
                all_recv_int_data[i_proc]
                                 [simplex_int_data_offset +
                                  node_int_data_idx_offset * ind_recv + 1]);

#ifdef FINITE_TEMPERATURE
        for (std::size_t d = 0; d < NIDOF; d++) {
          recv_thermal_dof[d] =
              all_recv_double_data[i_proc]
                                  [simplex_data_offset +
                                   node_data_idx_offset * ind_recv + DIM + d];
        }
        local_mesh.periodic_mesh_data.overlapping_thermal_dofs_recv[i_proc]
            .push_back(recv_thermal_dof);
#endif
        for (std::size_t d = 0; d < noi; d++) {
          recv_nodal_data.impurity_concentrations[d] =
              all_recv_double_data[i_proc][simplex_data_offset +
                                           node_data_idx_offset * ind_recv +
                                           node_data_idx_rho_offset + d];
        }
        local_mesh.periodic_mesh_data.overlapping_nodal_data_recv[i_proc]
            .push_back(recv_nodal_data);
      }
    }
  }

  // append the newly added simplices for sending to owner rank map
  // append the newly added nodes for sending to owner rank map
  // Note again, its ok to add non-unique nodes, but we make sure that
  // only unique simplices are sent
  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (!added_simplex_idxs_for_sending[i_proc].empty()) {
      local_mesh.periodic_mesh_data.overlap_simplex_idxs[i_proc].reserve(
          local_mesh.periodic_mesh_data.overlap_simplex_idxs[i_proc].size() +
          added_simplex_idxs_for_sending[i_proc].size());

      local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].reserve(
          local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size() +
          added_node_idxs_for_sending[i_proc].size());

      local_mesh.periodic_mesh_data.overlap_simplex_idxs[i_proc].insert(
          local_mesh.periodic_mesh_data.overlap_simplex_idxs[i_proc].end(),
          make_move_iterator(added_simplex_idxs_for_sending[i_proc].begin()),
          make_move_iterator(added_simplex_idxs_for_sending[i_proc].end()));

      local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].insert(
          local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].end(),
          make_move_iterator(added_node_idxs_for_sending[i_proc].begin()),
          make_move_iterator(added_node_idxs_for_sending[i_proc].end()));
    }

    std::vector<std::size_t>().swap(added_simplex_idxs_for_sending[i_proc]);
    std::vector<std::size_t>().swap(added_node_idxs_for_sending[i_proc]);
  }
}
} // namespace neighbourhoods
