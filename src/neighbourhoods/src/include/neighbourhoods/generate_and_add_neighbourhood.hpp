// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation for generating neighbourhoods of non-atomistic simplices
 * @author P. Gupta, S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "geometry/point.hpp"
#include "meshing/lattice_site.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/octree.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include <iostream>
#include <vector>

namespace neighbourhoods {

struct Annulus {
  std::array<double, 3> center{};
  double r_inner{};
  double r_outer{};
};

} // namespace neighbourhoods

template <>
struct qcmesh::geometry::traits::HaveOverlap<neighbourhoods::Annulus,
                                             std::array<double, 3>> {
  static bool have_overlap(const neighbourhoods::Annulus &annulus,
                           const std::array<double, 3> &point);
};

template <>
struct qcmesh::geometry::traits::BoundingBox<neighbourhoods::Annulus> {
  constexpr static std::size_t DIMENSION = 3;
  static qcmesh::geometry::Box<DIMENSION>
  bounding_box(const neighbourhoods::Annulus &annulus);
};

namespace neighbourhoods {

using NeighbourSet =
    qcmesh::geometry::octree::Octree<std::array<double, 3>, Annulus>;

template <std::size_t Noi>
void generate_and_add_neighbourhood(
    const geometry::Point<3> &closest_lattice_site,
    const int &site_type_closest_site, const int &site_type_sampling_atom,
    const geometry::Point<3> &offset, const double &distance_from_sampling_atom,
    const double &radius, const std::array<std::array<double, 3>, 3> &vectors,
    const std::vector<std::array<double, 3>> &offset_vectors,
    const std::array<geometry::Point<3>, 3 + 1> &simplex,
#ifdef FINITE_TEMPERATURE
    const std::array<std::array<double, 2>, 3 + 1> &thermal_simplex,
#endif
    const std::array<meshing::NodalDataMultispecies<Noi>, 3 + 1>
        &nodal_data_simplex,
    const std::array<std::size_t, 3 + 1> &ind_simplex, const int &rank,
    const bool &interproc_neighbour_flag, const bool &periodic_neighbour_flag,
    const bool &doing_cauchy_born_sampling_atom, NeighbourSet &neighbour_set,
    std::vector<meshing::LatticeSite<3, 2, meshing::NodalDataMultispecies<Noi>>>
        &neighbour_lattice_sites,
    bool print = false) {

  namespace array_ops = qcmesh::array_ops;

  std::array<double, 3> u{};
  std::array<double, 3> v{};
  std::array<double, 3> w{};
  double extended_radius = radius + distance_from_sampling_atom;

  const auto unpack_matrix_3 = [](const std::array<std::array<double, 3>, 3> &m,
                                  std::array<double, 3> &a,
                                  std::array<double, 3> &b,
                                  std::array<double, 3> &c) {
    for (int i = 0; i != 3; ++i) {
      a[i] = m[i][0];
      b[i] = m[i][1];
      c[i] = m[i][2];
    }
  };

  unpack_matrix_3(vectors, u, v, w);

  geometry::Point<3> lattice_site;

  std::array<int, 3> bounds_cells_in_directions{};

  bounds_cells_in_directions[0] = int((extended_radius) / array_ops::norm(u));

  bounds_cells_in_directions[1] = int((extended_radius) / array_ops::norm(v));

  bounds_cells_in_directions[2] = int((extended_radius) / array_ops::norm(w));

  bool added = false;

  double distance_squared{};

  for (std::size_t p = 0; p < offset_vectors.size(); p++) {

    for (int i = -bounds_cells_in_directions[0];
         i < bounds_cells_in_directions[0] + 1; i++) {
      for (int j = -bounds_cells_in_directions[1];
           j < bounds_cells_in_directions[1] + 1; j++) {
        for (int k = -bounds_cells_in_directions[2];
             k < bounds_cells_in_directions[2] + 1; k++) {
          if (!(i == 0 && j == 0 && k == 0 &&
                int(p) == site_type_sampling_atom)) {
            array_ops::as_eigen(lattice_site) =
                array_ops::as_eigen(closest_lattice_site) +
                i * array_ops::as_eigen(u) + j * array_ops::as_eigen(v) +
                k * array_ops::as_eigen(w) +
                array_ops::as_eigen(offset_vectors[p]) -
                array_ops::as_eigen(offset_vectors[site_type_closest_site]);

            distance_squared =
                array_ops::squared_distance(closest_lattice_site, lattice_site);

            if (!doing_cauchy_born_sampling_atom) {

              auto shape_function_values =
                  qcmesh::geometry::simplex_eval_shape_functions(simplex,
                                                                 lattice_site);

              const auto inside_simplex = qcmesh::geometry::
                  shape_function_values_represent_contained_point(
                      shape_function_values,
                      constants::shape_function_tolerance);

              if (inside_simplex && distance_squared < radius * radius) {

                added = neighbour_set.insert_unique(
                    lattice_site, neighbour_set.domain.r_inner);

                if (added) {
                  if (print) {
                    {
                      std::cout << "simplex is " << std::endl;
                      for (int i = 0; i < 4; i++)
                        std::cout << array_ops::as_streamable(simplex[i])
                                  << std::endl;

                      std::cout
                          << "closest lattice site "
                          << array_ops::as_streamable(closest_lattice_site)
                          << std::endl;
                    }

                    std::cout
                        << "volume is :"
                        << std::abs(qcmesh::geometry::simplex_volume(simplex))
                        << std::endl;
                    std::cout << "adding generated " << i << " " << j << " "
                              << k << " : "
                              << array_ops::as_streamable(lattice_site)
                              << std::endl;
                    std::cout << "basis are " << std::endl;
                    std::cout << array_ops::as_streamable(u) << std::endl;
                    std::cout << array_ops::as_streamable(v) << std::endl;
                    std::cout << array_ops::as_streamable(w) << std::endl;
                  }
                  neighbour_lattice_sites.emplace_back(
                      lattice_site, offset, shape_function_values,
#ifdef FINITE_TEMPERATURE
                      thermal_simplex,
#endif
                      nodal_data_simplex, ind_simplex, rank,
                      interproc_neighbour_flag, periodic_neighbour_flag);
                }
              }
            } else {
              const auto shape_function_values =
                  qcmesh::geometry::simplex_eval_shape_functions(simplex,
                                                                 lattice_site);

              if (distance_squared < radius * radius &&
                  distance_squared > constants::geometric_tolerance) {
                // we are always going to have unique
                // sites for a cauchy born sampling atom
                neighbour_lattice_sites.emplace_back(
                    lattice_site, offset, shape_function_values,
#ifdef FINITE_TEMPERATURE
                    thermal_simplex,
#endif
                    nodal_data_simplex, ind_simplex, rank,
                    interproc_neighbour_flag, periodic_neighbour_flag);
              }
            }
          }
        }
      }
    }
  }
}

} // namespace neighbourhoods
