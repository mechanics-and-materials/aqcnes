// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Free function declarations for neighbourhood functions
 * @author P. Gupta, S. Saxena, M. Spinola
 */

#pragma once

#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "materials/material.hpp"
#include "meshing/traits.hpp"
#include "neighbourhoods/get_force_and_potential.hpp"
#include "neighbourhoods/get_required_neighbourhoods.hpp"
#include "qcmesh/geometry/ball.hpp"
#include "qcmesh/geometry/box.hpp"
#include "utils/timer.hpp"
#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>

namespace neighbourhoods {

/**
 * @brief Prints non centro-symmetric clusters if requested in the user input.
 *
 * Can be used later for training GNN's for phase space averaging
 */
template <typename Mesh, std::size_t Dimension>
void save_non_centrosymmetric_neighborhoods(Mesh &local_mesh) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  std::string file_name =
      "./solution_files/datagen_clusters_" + std::to_string(mpi_rank) + ".dat";
  std::filesystem::create_directories("./solution_files/");
  std::ofstream f_print;
  f_print.open(file_name, std::ios::app);
  for (std::size_t atom_idx = 0;
       atom_idx < local_mesh.sampling_atoms.nodal_locations.size() -
                      local_mesh.global_ghost_indices.size();
       atom_idx++) {
    if (local_mesh.sampling_atoms.nodal_centro_symmetry[atom_idx] > 1e-3) {
      f_print << local_mesh.sampling_atoms.nodal_centro_symmetry[atom_idx]
              << ",";
      f_print << local_mesh.sampling_atoms.nodal_locations[atom_idx][0] << ","
              << local_mesh.sampling_atoms.nodal_locations[atom_idx][1] << ","
              << local_mesh.sampling_atoms.nodal_locations[atom_idx][2];
#ifdef FINITE_TEMPERATURE
      f_print << "," << local_mesh.repatoms.thermal_coordinates[atom_idx][1];
#endif
      for (std::size_t neighbour_idx = 0;
           neighbour_idx <
           local_mesh.sampling_atoms.nodal_sampling_atom_neighbours[atom_idx]
               .size();
           neighbour_idx++) {
        for (std::size_t d = 0; d < Dimension; d++)
          f_print
              << ","
              << local_mesh.sampling_atoms
                     .nodal_sampling_atom_neighbours[atom_idx][neighbour_idx]
                     .location[d];
#ifdef FINITE_TEMPERATURE
        f_print << ","
                << local_mesh.sampling_atoms
                       .nodal_sampling_atom_neighbours[atom_idx][neighbour_idx]
                       .thermal_coordinate[1];
#endif
      }
      f_print << '\n';
    }
  }
  f_print.close();
}

template <typename Mesh, std::size_t Dimension, typename NodalData>
void add_forces_to_repatoms(
    Mesh &local_mesh,
    const std::vector<materials::Material<
        Dimension, meshing::traits::N_IMPURITIES<NodalData>>> &,
    bool constrain_positions = false);

template <typename Mesh, std::size_t Dimension, std::size_t NIDOF,
          typename NodalData>
void compute_potential(
    Mesh &local_mesh,
    const std::vector<materials::Material<
        Dimension, meshing::traits::N_IMPURITIES<NodalData>>> &materials
#ifdef FINITE_TEMPERATURE
    ,
    int quad_type, bool use_gnn
#endif
) {

  std::vector<geometry::Point<Dimension>> cluster;
  double e_s{};
  std::vector<int> neighbour_indices;
  std::vector<NodalData> cluster_nodal_data;

#ifdef FINITE_TEMPERATURE
  std::vector<std::array<double, NIDOF>> cluster_thermal_coordinates;
#endif

  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.central_locations.size();
       atom_index++) {

#ifdef FINITE_TEMPERATURE
    local_mesh.sampling_atoms.get_central_neighbour_thermal_cluster(
        atom_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);
    if (use_gnn) {
      // read energies from precomputed values
      e_s = local_mesh.central_cluster_energies_gnn[atom_index];
    } else {

      if (quad_type == 3) {
        e_s = materials[local_mesh.sampling_atoms
                            .central_material_ids[atom_index]]
                  .get_thermalized_energy_3(
                      local_mesh.sampling_atoms.central_locations[atom_index],
                      local_mesh.sampling_atoms.central_ido_fs[atom_index],
                      cluster, cluster_thermal_coordinates,
                      local_mesh.sampling_atoms.central_nodal_data[atom_index],
                      cluster_nodal_data);
      } else if (quad_type == 5) {
        e_s = materials[local_mesh.sampling_atoms
                            .central_material_ids[atom_index]]
                  .get_thermalized_energy_5(
                      local_mesh.sampling_atoms.central_locations[atom_index],
                      local_mesh.sampling_atoms.central_ido_fs[atom_index],
                      cluster, cluster_thermal_coordinates,
                      local_mesh.sampling_atoms.central_nodal_data[atom_index],
                      cluster_nodal_data);
      } else
        throw exception::QCException{
            "Only 3 and 5 order quadratures supported at the "
            "moment."};
    }
#else
    local_mesh.sampling_atoms.get_central_neighbour_cluster(
        atom_index, cluster, cluster_nodal_data, neighbour_indices);

    e_s = materials[local_mesh.sampling_atoms.central_material_ids[atom_index]]
              .get_cluster_energy(
                  local_mesh.sampling_atoms.central_locations[atom_index],
                  cluster,
                  local_mesh.sampling_atoms.central_nodal_data[atom_index],
                  cluster_nodal_data);
#endif

    local_mesh.sampling_atoms.central_energies[atom_index] =
        e_s * local_mesh.sampling_atoms.central_weights[atom_index];
  }

  for (std::size_t node_index = 0;
       node_index < local_mesh.sampling_atoms.nodal_locations.size() -
                        local_mesh.global_ghost_indices.size();
       node_index++) {
#ifdef FINITE_TEMPERATURE
    local_mesh.sampling_atoms.get_nodal_neighbour_thermal_cluster(
        node_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);
    if (use_gnn) {
      // read energies from precomputed values
      e_s = local_mesh.nodal_cluster_energies_gnn[node_index];
    } else {
      if (quad_type == 3) {
        e_s = materials[local_mesh.material_idx_of_nodes[node_index]]
                  .get_thermalized_energy_3(
                      local_mesh.sampling_atoms.nodal_locations[node_index],
                      local_mesh.repatoms.thermal_coordinates[node_index],
                      cluster, cluster_thermal_coordinates,
                      local_mesh.repatoms.nodal_data_points[node_index],
                      cluster_nodal_data);

      } else if (quad_type == 5) {
        e_s = materials[local_mesh.material_idx_of_nodes[node_index]]
                  .get_thermalized_energy_5(
                      local_mesh.sampling_atoms.nodal_locations[node_index],
                      local_mesh.repatoms.thermal_coordinates[node_index],
                      cluster, cluster_thermal_coordinates,
                      local_mesh.repatoms.nodal_data_points[node_index],
                      cluster_nodal_data);
      } else
        throw exception::QCException{
            "Only 3 and 5 order quadratures supported at the "
            "moment."};
    }
#else
    local_mesh.sampling_atoms.get_nodal_neighbour_cluster(
        node_index, cluster, cluster_nodal_data, neighbour_indices);

    e_s = materials[local_mesh.material_idx_of_nodes[node_index]]
              .get_cluster_energy(
                  local_mesh.sampling_atoms.nodal_locations[node_index],
                  cluster, local_mesh.repatoms.nodal_data_points[node_index],
                  cluster_nodal_data);
#endif
    local_mesh.sampling_atoms.nodal_energies[node_index] =
        e_s * local_mesh.sampling_atoms.nodal_weights[node_index];
  }
}

template <typename Mesh, std::size_t Dimension, typename NodalData>
void add_forces_to_repatoms(
    Mesh &local_mesh,
    const std::vector<materials::Material<
        Dimension, meshing::traits::N_IMPURITIES<NodalData>>> &materials,
    bool constrain_positions) {
  namespace array_ops = qcmesh::array_ops;

  const auto mpi_size = boost::mpi::communicator{}.size();

  std::vector<std::vector<double>> overlapping_nodes_forces_data_send(mpi_size);
  std::vector<std::vector<double>> overlapping_nodes_forces_data_recv(mpi_size);

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    overlapping_nodes_forces_data_send[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc].size() *
            Dimension,
        0.0);

    overlapping_nodes_forces_data_recv[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size() *
            Dimension,
        0.0);
  }

  std::vector<std::vector<double>> periodic_overlapping_nodes_forces_send(
      mpi_size);
  std::vector<std::vector<double>> periodic_overlapping_nodes_forces_recv(
      mpi_size);

  if (local_mesh.has_periodic_dimensions) {
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      periodic_overlapping_nodes_forces_send[i_proc].resize(
          local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].size() *
              Dimension,
          0.0);

      periodic_overlapping_nodes_forces_recv[i_proc].resize(
          local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size() *
              Dimension,
          0.0);
    }
  }

  // parallelize with omp.
  for (geometry::Point<Dimension> &force : local_mesh.repatoms.forces) {
    force.fill(0.0);
  }

  geometry::Point<Dimension> local_periodic_force;
  local_periodic_force.fill(0.0);

  geometry::Point<Dimension> f_ij;
  geometry::Point<Dimension> r_ij;

  // compute neighbour forces and add to repatoms
  std::size_t local_repatom_idx = 0;
  int dependent_rank = 0;
  int interproc_repatom_idx = 0;

  std::vector<geometry::Point<Dimension>> cluster_forces;
  std::vector<geometry::Point<Dimension>> cluster;
  std::vector<NodalData> cluster_nodal_data;
  std::vector<int> neighbour_indices;

  int noof_dependencies = 0;
  int neighbour_index = 0;

  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.central_locations.size();
       atom_index++) {
    local_mesh.sampling_atoms.get_central_neighbour_cluster(
        atom_index, cluster, cluster_nodal_data, neighbour_indices);

    materials[local_mesh.sampling_atoms.central_material_ids[atom_index]]
        .get_cluster_forces(
            cluster_forces,
            local_mesh.sampling_atoms.central_locations[atom_index], cluster,
            local_mesh.sampling_atoms.central_nodal_data[atom_index],
            cluster_nodal_data);

    for (std::size_t cluster_index = 0; cluster_index < cluster.size();
         cluster_index++) {
      neighbour_index = neighbour_indices[cluster_index];

      array_ops::as_eigen(f_ij) =
          array_ops::as_eigen(cluster_forces[cluster_index]) *
          local_mesh.sampling_atoms.central_weights[atom_index];

      noof_dependencies =
          local_mesh.sampling_atoms
              .central_sampling_atom_neighbours[atom_index][neighbour_index]
              .repatom_dependencies.size();

      for (int d = 0; d < noof_dependencies; d++) {
        local_repatom_idx =
            local_mesh.sampling_atoms
                .central_sampling_atom_neighbours[atom_index][neighbour_index]
                .repatom_dependencies[d]
                .first.second;

        array_ops::as_eigen(local_mesh.repatoms.forces[local_repatom_idx]) +=
            array_ops::as_eigen(f_ij) *
            (local_mesh.sampling_atoms
                 .central_sampling_atom_neighbours[atom_index][neighbour_index]
                 .repatom_dependencies[d]
                 .second -
             1.0 / double(Dimension + 1));
      }

      // add periodic forces
      for (std::size_t i_period = 0; i_period < Dimension; i_period++) {
        if (local_mesh.periodic_flags[i_period]) {
          local_periodic_force[i_period] +=
              f_ij[i_period] * (cluster[cluster_index][i_period] -
                                local_mesh.sampling_atoms
                                    .central_locations[atom_index][i_period]);
        }
      }
    }
  }

  bool interproc_neighbour_flag = false;
  bool periodic_neighbour_flag = false;

  for (std::size_t node_index = 0;
       node_index < local_mesh.sampling_atoms.nodal_locations.size() -
                        local_mesh.global_ghost_indices.size();
       node_index++) {
    local_mesh.sampling_atoms.get_nodal_neighbour_cluster(
        node_index, cluster, cluster_nodal_data, neighbour_indices);

    materials[local_mesh.material_idx_of_nodes[node_index]].get_cluster_forces(
        cluster_forces, local_mesh.sampling_atoms.nodal_locations[node_index],
        cluster, local_mesh.repatoms.nodal_data_points[node_index],
        cluster_nodal_data);

    r_ij.fill(0.0);

    for (std::size_t cluster_index = 0; cluster_index < cluster.size();
         cluster_index++) {
      neighbour_index = neighbour_indices[cluster_index];

      array_ops::as_eigen(f_ij) =
          array_ops::as_eigen(cluster_forces[cluster_index]) *
          local_mesh.sampling_atoms.nodal_weights[node_index];

      array_ops::as_eigen(r_ij) +=
          array_ops::as_eigen(
              local_mesh.sampling_atoms.nodal_locations[node_index]) -
          array_ops::as_eigen(cluster[cluster_index]);

      // add force to this node
      array_ops::as_eigen(local_mesh.repatoms.forces[node_index]) -=
          array_ops::as_eigen(f_ij);

      noof_dependencies =
          local_mesh.sampling_atoms
              .nodal_sampling_atom_neighbours[node_index][neighbour_index]
              .repatom_dependencies.size();

      interproc_neighbour_flag =
          local_mesh.sampling_atoms
              .nodal_sampling_atom_neighbours[node_index][neighbour_index]
              .interproc_flag;

      periodic_neighbour_flag =
          local_mesh.sampling_atoms
              .nodal_sampling_atom_neighbours[node_index][neighbour_index]
              .periodic_flag;

      if (!interproc_neighbour_flag) {
        for (int d = 0; d < noof_dependencies; d++) {
          local_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .first.second;

          array_ops::as_eigen(local_mesh.repatoms.forces[local_repatom_idx]) +=
              array_ops::as_eigen(f_ij) *
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .second;
        }
      } else if (interproc_neighbour_flag && !periodic_neighbour_flag) {
        for (int d = 0; d < noof_dependencies; d++) {
          dependent_rank =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .first.first;

          interproc_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .first.second;

          for (std::size_t l = 0; l < Dimension; l++) {
            overlapping_nodes_forces_data_send
                [dependent_rank][Dimension * interproc_repatom_idx + l] +=
                f_ij[l] *
                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                    .repatom_dependencies[d]
                    .second;
          }
        }
      } else if (local_mesh.has_periodic_dimensions &&
                 interproc_neighbour_flag && periodic_neighbour_flag) {
        for (int d = 0; d < noof_dependencies; d++) {
          dependent_rank =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .first.first;

          interproc_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                  .repatom_dependencies[d]
                  .first.second;

          for (std::size_t l = 0; l < Dimension; l++) {
            periodic_overlapping_nodes_forces_send
                [dependent_rank][Dimension * interproc_repatom_idx + l] +=
                f_ij[l] *
                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[node_index][neighbour_index]
                    .repatom_dependencies[d]
                    .second;
          }
        }
      }

      // add periodic forces
      for (std::size_t i_period = 0; i_period < Dimension; i_period++) {
        if (local_mesh.periodic_flags[i_period]) {
          local_periodic_force[i_period] +=
              f_ij[i_period] *
              (cluster[cluster_index][i_period] -
               local_mesh.sampling_atoms.nodal_locations[node_index][i_period]);
        }
      }
    }
  }

  if (mpi_size > 1) {
    // exchange the neighbour bonds and check

    // exchanged forces on overlapping nodes.
    qcmesh::mpi::all_scatter(overlapping_nodes_forces_data_send,
                             overlapping_nodes_forces_data_recv);

    // now add forces to overlapping nodes

    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      for (std::size_t i_nd = 0;
           i_nd <
           local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size();
           i_nd++) {
        interproc_repatom_idx =
            local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc][i_nd];

        for (std::size_t d = 0; d < Dimension; d++) {
          local_mesh.repatoms.forces[interproc_repatom_idx][d] +=
              overlapping_nodes_forces_data_recv[i_proc][Dimension * i_nd + d];
        }
      }
    }

    if (local_mesh.has_periodic_dimensions) {
      // exchanged forces on periodic overlapping nodes.
      qcmesh::mpi::all_scatter(periodic_overlapping_nodes_forces_send,
                               periodic_overlapping_nodes_forces_recv);

      // now add forces to overlapping nodes

      for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
        for (std::size_t i_nd = 0;
             i_nd <
             local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size();
             i_nd++) {
          interproc_repatom_idx =
              local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc][i_nd];

          for (std::size_t d = 0; d < Dimension; d++) {
            local_mesh.repatoms.forces[interproc_repatom_idx][d] +=
                periodic_overlapping_nodes_forces_recv[i_proc]
                                                      [Dimension * i_nd + d];
          }
        }
      }
    }
  }

  if (constrain_positions) {
    for (auto &f : local_mesh.repatoms.forces) {
      f.fill(0.0);
    }
  }

  // simply all-reduce periodic forces

  // make periodic domain force zero
  local_mesh.repatoms.periodic_forces.fill(0.0);

  const auto world = boost::mpi::communicator{};
  boost::mpi::all_reduce(world, local_periodic_force.data(), Dimension,
                         local_mesh.repatoms.periodic_forces.data(),
                         std::plus<>());
}

template <typename InputClass, typename Mesh, typename ExternalForceApplicator,
          std::size_t Dimension, std::size_t NIDOF, typename NodalData>
void get_required_neighbourhoods_add_forces(
    const InputClass &input_params, Mesh &local_mesh,
    std::vector<std::size_t> &triggered_mesh_exchange_sampling_atoms,
    std::vector<std::size_t> &triggered_periodic_mesh_exchange_sampling_atoms,
    std::vector<std::size_t>
        &triggered_periodic_mesh_exchange_inner_sampling_atoms,
    std::vector<std::size_t> &triggered_nodal_sampling_atoms,
    std::vector<std::size_t> &triggered_cb_sampling_atoms,
    ExternalForceApplicator *external_force_applicator,
    const std::vector<materials::Material<
        Dimension, meshing::traits::N_IMPURITIES<NodalData>>> &materials,
    const bool &apply_external_force, const bool periodic_simulation_flag) {
  // triggered sampling atoms have no neighbours. Update those

  get_required_neighbourhoods<InputClass, Mesh, Dimension, NIDOF, NodalData>(
      input_params, local_mesh, triggered_mesh_exchange_sampling_atoms,
      triggered_periodic_mesh_exchange_sampling_atoms,
      triggered_periodic_mesh_exchange_inner_sampling_atoms,
      triggered_nodal_sampling_atoms, triggered_cb_sampling_atoms,
      periodic_simulation_flag);

  if (input_params.non_centrosymmetric_neighborhoods_frequency.has_value() &&
      input_params.non_centrosymmetric_neighborhoods_frequency.value() == 0) {
    local_mesh.compute_nodal_centro_symmetry();
    neighbourhoods::save_non_centrosymmetric_neighborhoods<Mesh, Dimension>(
        local_mesh);
    throw exception::QCException{
        "Exiting after printing initially non-centrosymmteric "
        "neighborhoods."};
  }

  auto neighbourhood_clock = utils::Timer{};
  if (input_params.neighborhood_and_force_time) {
    neighbourhood_clock.tic();
  }

  // all neighbourhood sites are in place
  // compute forces and then add.
#ifdef FINITE_TEMPERATURE
  const auto use_gnn = std::holds_alternative<input::NNMaterialProperties>(
      input_params.material);

  add_thermalized_forces_to_repatoms<Mesh, Dimension, NIDOF, NodalData>(
      local_mesh, materials, input_params, periodic_simulation_flag, use_gnn);

#else
  add_forces_to_repatoms<Mesh, Dimension, NodalData>(local_mesh, materials);
#endif

  compute_potential<Mesh, Dimension, NIDOF, NodalData>(local_mesh, materials
#ifdef FINITE_TEMPERATURE
                                                       ,
                                                       input_params.quad_type,
                                                       use_gnn
#endif
  );

  if (apply_external_force) {
    external_force_applicator->apply_force(
        local_mesh.repatoms.locations, local_mesh.repatoms.nodal_weights,
        local_mesh.global_ghost_indices.size(), local_mesh.repatoms.forces);
  }

  if (input_params.neighborhood_and_force_time) {
    const auto mpi_rank = boost::mpi::communicator{}.rank();
    const auto elapsed_time = neighbourhood_clock.toc();
    std::cout << "rank - " << mpi_rank << " took " << elapsed_time
              << " in computing forces" << std::endl;
  }
}

} // namespace neighbourhoods
