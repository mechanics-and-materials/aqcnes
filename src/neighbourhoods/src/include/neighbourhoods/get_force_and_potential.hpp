// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Compute forces and potentials for 0K and Finite temperature cases
 * @author P. Gupta, S. Saxena, M. Spinola
 */

#pragma once

#include "constants/constants.hpp"
#include "geometry/point.hpp"
#include "input/userinput.hpp"
#include "materials/material.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi.hpp>

#ifdef USE_TORCH
#include "materials/nn_material.hpp"
#endif

namespace neighbourhoods {

namespace detail {
template <std::size_t Dim, std::size_t Nidof>
exception::QCException
format_error(const std::array<double, Nidof> &neighbor_thermal_coordinates,
             const std::array<double, Dim> &neighbour_mean_location,
             const std::array<double, Nidof> &sampling_atom_thermal_coordinates,
             const std::array<double, Dim> &sampling_atom_mean_location) {
  const auto to_str = [](const auto &arr) {
    std::stringstream buf{};
    buf << qcmesh::array_ops::as_streamable(arr);
    return buf.str();
  };
  return exception::QCException{
      "nan force detected:\n\tneighbor_thermal_coordinates=" +
      to_str(neighbor_thermal_coordinates) +
      "\n\tneighbour_mean_location=" + to_str(neighbour_mean_location) +
      "\n\tsampling_atom_mean_location=" + to_str(sampling_atom_mean_location) +
      "\n\tsampling_atom_thermal_coordinates=" +
      to_str(sampling_atom_thermal_coordinates)};
}
} // namespace detail

/**
 * @brief add phase averaged thermalized forces using
 * third order quadrature for phase-space integration
 */
template <typename Mesh, std::size_t DIM, std::size_t NIDOF, typename NodalData>
void add_thermalized_forces_to_repatoms(
    Mesh &local_mesh,
    const std::vector<
        materials::Material<DIM, meshing::traits::N_IMPURITIES<NodalData>>>
        &materials,
    const input::Userinput &input, const bool periodic_simulation_flag,
    const bool use_gnn, const bool constrain_positions = false,
    const bool freeze_thermal_relaxation_flag = false) {
  namespace array_ops = qcmesh::array_ops;
  const auto mpi_size = boost::mpi::communicator{}.size();

  std::vector<std::vector<double>> overlapping_nodes_forces_data_send(mpi_size);
  std::vector<std::vector<double>> overlapping_nodes_forces_data_recv(mpi_size);
  std::vector<std::vector<double>> periodic_overlapping_forces_send(mpi_size);
  std::vector<std::vector<double>> periodic_overlapping_forces_recv(mpi_size);

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    overlapping_nodes_forces_data_send[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc].size() *
            (DIM + 1),
        0.0);
    overlapping_nodes_forces_data_recv[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size() *
            (DIM + 1),
        0.0);

    if (periodic_simulation_flag) {
      periodic_overlapping_forces_send[i_proc].resize(
          local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].size() *
              (DIM + 1),
          0.0);
      periodic_overlapping_forces_recv[i_proc].resize(
          local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size() *
              (DIM + 1),
          0.0);
    }
  }

  // parallelize with omp.
  for (std::size_t i = 0; i < local_mesh.repatoms.locations.size(); i++) {
    local_mesh.repatoms.forces[i].fill(0.0);
    local_mesh.repatoms.thermal_forces[i] = 0.0;
  }

  geometry::Point<DIM> local_periodic_force;
  local_periodic_force.fill(0.0);

#ifdef MULTI_THREADING

#pragma omp declare reduction(                                                 \
        vec_point_plus : std::vector<geometry::Point<DIM>>:

        std::transform(omp_out.begin(), omp_out.end(), \
		omp_in.begin(), omp_out.begin(), std::plus<geometry::Point<DIM>>())) \
	  initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

#pragma omp declare reduction(point_plus : point<DIM> : omp_out += omp_in)     \
    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

#pragma omp declare reduction(                                                 \
        vec_double_plus : std::vector<double> : std::transform(                \
                omp_out.begin(), omp_out.end(), omp_in.begin(),                \
                    omp_out.begin(), std::plus<double>()))                     \
    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))
#endif

  std::vector<geometry::Point<DIM>> forces(local_mesh.repatoms.forces.size(),
                                           geometry::Point<DIM>{});

        std::vector<double> thermal_forces(local_mesh.repatoms.forces.size(),
                                           0.0);

        std::array<std::size_t, DIM + 1> indexed_cell{};

        // first private copies into variables local to threads
        // every thread will have its interpolator

#ifdef MULTI_THREADING
#pragma omp parallel default(none)                                             \
    shared(_local_mesh, cout) private(indexed_cell) firstprivate(materials_)   \
        reduction(vec_point_plus                                               \
                  : forces) reduction(point_plus                               \
                                      : local_periodic_force)                  \
            reduction(vec_double_plus                                          \
                      : thermal_forces)
#endif

        // inputs are batched and passed on the GNN module if using GNN's
        // make vectors to stack input info and receive output info from GNN's,
        // if using them
        std::vector<geometry::Point<DIM>> cluster;
        std::vector<std::array<double, NIDOF>> cluster_thermal_coordinates;
        std::size_t graph_idx = 0;
        using EdgeVector = std::array<std::size_t, 2>;
        std::vector<double> sigmas_gnn;
        std::vector<double> thermal_forces_gnn;
        std::vector<double> energy_gnn;
        std::vector<geometry::Point<DIM>> positions_gnn;
        std::vector<geometry::Point<DIM>> forces_gnn;
        std::vector<int64_t> graph_numbers_gnn;
        std::vector<EdgeVector> edges_gnn;
        std::vector<NodalData> cluster_nodal_data;

#ifdef USE_TORCH
        if (use_gnn) {
          local_mesh.central_cluster_energies_gnn.clear();
          local_mesh.nodal_cluster_energies_gnn.clear();
          local_mesh.central_cluster_energies_gnn.resize(
              local_mesh.sampling_atoms.central_locations.size(), 0.0);
          local_mesh.nodal_cluster_energies_gnn.resize(
              local_mesh.sampling_atoms.nodal_locations.size() -
                  local_mesh.global_ghost_indices.size(),
              0.0);
          std::vector<int> neighbour_indices;
          positions_gnn.clear();
          sigmas_gnn.clear();
          edges_gnn.clear();
          graph_numbers_gnn.clear();
          std::size_t noof_central_atoms =
              local_mesh.sampling_atoms.central_locations.size();
          // stack central sampling atom info
          for (std::size_t atom_index = 0; atom_index < noof_central_atoms;
               atom_index++) {
            local_mesh.sampling_atoms.get_central_neighbour_thermal_cluster(
                atom_index, cluster, cluster_thermal_coordinates,
                cluster_nodal_data, neighbour_indices);
            // noof_cluster_atoms = cluster.size() + 1;
            graph_numbers_gnn.push_back(static_cast<int64_t>(atom_index));
            positions_gnn.push_back(
                local_mesh.sampling_atoms.central_locations[atom_index]);
            sigmas_gnn.push_back(
                local_mesh.sampling_atoms.central_ido_fs[atom_index][1]);
            for (std::size_t cluster_index = 0; cluster_index < cluster.size();
                 cluster_index++) {
              positions_gnn.push_back(cluster[cluster_index]);
              sigmas_gnn.push_back(
                  cluster_thermal_coordinates[cluster_index][1]);
              edges_gnn.push_back({graph_idx, graph_idx + cluster_index + 1});
              edges_gnn.push_back({graph_idx + cluster_index + 1, graph_idx});
              graph_numbers_gnn.push_back(static_cast<int64_t>(atom_index));
            }
            graph_idx += cluster.size() + 1;
          }
          // stack nodal sampling atom info
          for (std::size_t atom_index = 0;
               atom_index < local_mesh.sampling_atoms.nodal_locations.size() -
                                local_mesh.global_ghost_indices.size();
               atom_index++) {
            local_mesh.sampling_atoms.get_nodal_neighbour_thermal_cluster(
                atom_index, cluster, cluster_thermal_coordinates,
                cluster_nodal_data, neighbour_indices);
            // noof_cluster_atoms = cluster.size() + 1;
            graph_numbers_gnn.push_back(
                static_cast<int64_t>(atom_index + noof_central_atoms));
            positions_gnn.push_back(
                local_mesh.sampling_atoms.nodal_locations[atom_index]);
            sigmas_gnn.push_back(
                local_mesh.repatoms.thermal_coordinates[atom_index][1]);
            for (std::size_t cluster_index = 0; cluster_index < cluster.size();
                 cluster_index++) {
              positions_gnn.push_back(cluster[cluster_index]);
              sigmas_gnn.push_back(
                  cluster_thermal_coordinates[cluster_index][1]);
              edges_gnn.push_back({graph_idx, graph_idx + cluster_index + 1});
              edges_gnn.push_back({graph_idx + cluster_index + 1, graph_idx});
              graph_numbers_gnn.push_back(
                  static_cast<int64_t>(atom_index + noof_central_atoms));
            }
            graph_idx += cluster.size() + 1;
          }
          // Forward pass on GNN model
          // cast a unique_ptr to a parent class (ClusterModel)
          // to a unique_ptr to its derived class (NNMaterial)
          const auto *nn_material = dynamic_cast<materials::NNMaterial *>(
              materials[0].cluster_model.get());
          nn_material->get_batched_thermalized_energy_and_forces_gnn(
              positions_gnn, sigmas_gnn, edges_gnn, graph_numbers_gnn,
              energy_gnn, forces_gnn, thermal_forces_gnn);
          graph_idx = 0;
        }
#endif

        {
          geometry::Point<DIM> f_ij;

          int noof_dependencies = 0;
          std::size_t local_repatom_idx = 0;

          std::vector<geometry::Point<DIM>> cluster_forces;
          std::vector<double> cluster_thermal_force_sampling_atom;
          std::vector<double> cluster_thermal_force_neighbour;

          std::vector<int> neighbour_indices;

          int neighbour_idx = 0;

#ifdef MULTI_THREADING
#pragma omp for schedule(static)
#endif

          for (std::size_t atom_index = 0;
               atom_index < local_mesh.sampling_atoms.central_locations.size();
               atom_index++) {
            local_mesh.sampling_atoms.get_central_neighbour_thermal_cluster(
                atom_index, cluster, cluster_thermal_coordinates,
                cluster_nodal_data, neighbour_indices);
            if (!use_gnn) {
              if (input.quad_type == 3) {
                materials[local_mesh.sampling_atoms
                              .central_material_ids[atom_index]]
                    .get_thermalized_forces_3(
                        cluster_forces, cluster_thermal_force_sampling_atom,
                        cluster_thermal_force_neighbour,
                        local_mesh.sampling_atoms.central_locations[atom_index],
                        local_mesh.sampling_atoms.central_ido_fs[atom_index],
                        cluster, cluster_thermal_coordinates,
                        local_mesh.sampling_atoms
                            .central_nodal_data[atom_index],
                        cluster_nodal_data);
              } else if (input.quad_type == 5) {
                materials[local_mesh.sampling_atoms
                              .central_material_ids[atom_index]]
                    .get_thermalized_forces_5(
                        cluster_forces, cluster_thermal_force_sampling_atom,
                        cluster_thermal_force_neighbour,
                        local_mesh.sampling_atoms.central_locations[atom_index],
                        local_mesh.sampling_atoms.central_ido_fs[atom_index],
                        cluster, cluster_thermal_coordinates,
                        local_mesh.sampling_atoms
                            .central_nodal_data[atom_index],
                        cluster_nodal_data);
              } else
                throw exception::QCException{
                    "Only 3 and 5 order quadratures supported at the "
                    "moment."};
            }

            const auto element_key_central_atom =
                local_mesh.sampling_atoms.cell_key_of_central_atoms[atom_index];

            indexed_cell =
                indexed_simplex(local_mesh, element_key_central_atom);

            // temperature part of the thermal force. thermal_coordinate_[0]
            // is k_B T

            for (std::size_t d = 0; d < DIM + 1; d++) {
              // ******************************************** //
              thermal_forces[indexed_cell[d]] +=
                  (1.0 / double(DIM + 1)) *
                  local_mesh.sampling_atoms.central_weights[atom_index] *
                  exp(-local_mesh.sampling_atoms.central_ido_fs[atom_index][0]);

              if (use_gnn) {
                thermal_forces[indexed_cell[d]] -=
                    thermal_forces_gnn[graph_idx] * (1.0 / double(DIM + 1)) *
                    local_mesh.sampling_atoms.central_weights[atom_index];
              }
              // ********************************************//
            }

            for (std::size_t cluster_index = 0; cluster_index < cluster.size();
                 cluster_index++) {
              neighbour_idx = neighbour_indices[cluster_index];

              if (use_gnn) {
                array_ops::as_eigen(f_ij) =
                    array_ops::as_eigen(
                        forces_gnn[graph_idx + cluster_index + 1]) *
                    local_mesh.sampling_atoms.central_weights[atom_index];
              } else {
                array_ops::as_eigen(f_ij) =
                    array_ops::as_eigen(cluster_forces[cluster_index]) *
                    local_mesh.sampling_atoms.central_weights[atom_index];
              }

              noof_dependencies =
                  local_mesh.sampling_atoms
                      .central_sampling_atom_neighbours[atom_index]
                                                       [neighbour_idx]
                      .repatom_dependencies.size();

              for (int d = 0; d < noof_dependencies; d++) {
                local_repatom_idx =
                    local_mesh.sampling_atoms
                        .central_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                        .repatom_dependencies[d]
                        .first.second;

                array_ops::as_eigen(forces[local_repatom_idx]) +=
                    array_ops::as_eigen(f_ij) *
                    (local_mesh.sampling_atoms
                         .central_sampling_atom_neighbours[atom_index]
                                                          [neighbour_idx]
                         .repatom_dependencies[d]
                         .second -
                     1.0 / double(DIM + 1));

                // ********************************************************** //
                if (use_gnn) {
                  thermal_forces[local_repatom_idx] +=
                      (local_mesh.sampling_atoms
                           .central_sampling_atom_neighbours[atom_index]
                                                            [neighbour_idx]
                           .repatom_dependencies[d]
                           .second *
                       thermal_forces_gnn[graph_idx + cluster_index + 1]) *
                      local_mesh.sampling_atoms.central_weights[atom_index];
                } else {

                  thermal_forces[local_repatom_idx] +=
                      (local_mesh.sampling_atoms
                               .central_sampling_atom_neighbours[atom_index]
                                                                [neighbour_idx]
                               .repatom_dependencies[d]
                               .second *
                           cluster_thermal_force_neighbour[cluster_index] -
                       cluster_thermal_force_sampling_atom[cluster_index] *
                           (1.0 / double(DIM + 1))) *
                      local_mesh.sampling_atoms.central_weights[atom_index];
                }
                // ********************************************************** //

                if (std::isnan(array_ops::norm(f_ij))) {
                  std::cout << "cb :" << array_ops::norm(f_ij) << " , "
                            << array_ops::as_streamable(
                                   local_mesh.sampling_atoms
                                       .central_sampling_atom_neighbours
                                           [atom_index][neighbour_idx]
                                       .thermal_coordinate)
                            << " , "
                            << array_ops::as_streamable(
                                   local_mesh.sampling_atoms
                                       .central_ido_fs[atom_index])
                            << std::endl;
                }
              }

              for (std::size_t i_period = 0; i_period < DIM; i_period++) {
                if (local_mesh.periodic_flags[i_period]) {
                  local_periodic_force[i_period] +=
                      f_ij[i_period] *
                      (cluster[cluster_index][i_period] -
                       local_mesh.sampling_atoms
                           .central_locations[atom_index][i_period]);
                }
              }
            }
            if (use_gnn) {
              local_mesh.central_cluster_energies_gnn[atom_index] =
                  energy_gnn[graph_idx];
              graph_idx += cluster.size() + 1;
            }
          }
        }

#ifdef MULTI_THREADING
        const auto mpi_rank = boost::mpi::communicator{}.rank();

#pragma omp parallel default(none)                                             \
    shared(_local_mesh, delimiters_send, periodic_delimiters_send, mpi_rank,   \
           cout) private(indexedSimplex) firstprivate(materials_)              \
        reduction(vec_point_plus                                               \
                  : forces) reduction(point_plus                               \
                                      : local_periodic_force)                  \
            reduction(vec_double_plus                                          \
                      : thermal_forces)                                        \
                reduction(vec_double_plus                                      \
                          : overlapping_nodes_forces_data_send_1D)             \
                    reduction(vec_double_plus                                  \
                              : periodic_overlapping_forces_send_1D)
#endif
        {
          geometry::Point<DIM> f_ij;

          int dependent_rank = 0;
          int interproc_repatom_idx = 0;
          int noof_dependencies = 0;
          std::size_t local_repatom_idx = 0;

          std::vector<geometry::Point<DIM>> cluster_forces;
          std::vector<double> cluster_thermal_force_sampling_atom;
          std::vector<double> cluster_thermal_force_neighbour;

          std::vector<int> neighbour_indices;

          int neighbour_idx = 0;
          double t_force_temp = 0.0;

#ifdef MULTI_THREADING
#pragma omp for schedule(static)
#endif

          for (std::size_t atom_index = 0;
               atom_index < local_mesh.sampling_atoms.nodal_locations.size() -
                                local_mesh.global_ghost_indices.size();
               atom_index++) {
            local_mesh.sampling_atoms.get_nodal_neighbour_thermal_cluster(
                atom_index, cluster, cluster_thermal_coordinates,
                cluster_nodal_data, neighbour_indices);

            if (!use_gnn) {
              if (input.quad_type == 3) {
                materials[local_mesh.material_idx_of_nodes[atom_index]]
                    .get_thermalized_forces_3(
                        cluster_forces, cluster_thermal_force_sampling_atom,
                        cluster_thermal_force_neighbour,
                        local_mesh.sampling_atoms.nodal_locations[atom_index],
                        local_mesh.repatoms.thermal_coordinates[atom_index],
                        cluster, cluster_thermal_coordinates,
                        local_mesh.repatoms.nodal_data_points[atom_index],
                        cluster_nodal_data);

              } else if (input.quad_type == 5) {
                materials[local_mesh.material_idx_of_nodes[atom_index]]
                    .get_thermalized_forces_5(
                        cluster_forces, cluster_thermal_force_sampling_atom,
                        cluster_thermal_force_neighbour,
                        local_mesh.sampling_atoms.nodal_locations[atom_index],
                        local_mesh.repatoms.thermal_coordinates[atom_index],
                        cluster, cluster_thermal_coordinates,
                        local_mesh.repatoms.nodal_data_points[atom_index],
                        cluster_nodal_data);

              } else
                throw exception::QCException{
                    "Only 3 and 5 order quadratures supported at the "
                    "moment."};
            }

            // *************************************************//
            thermal_forces[atom_index] +=
                local_mesh.sampling_atoms.nodal_weights[atom_index] *
                exp(-local_mesh.repatoms.thermal_coordinates[atom_index][0]);
            if (use_gnn) {
              thermal_forces[atom_index] +=
                  thermal_forces_gnn[graph_idx] *
                  local_mesh.sampling_atoms.nodal_weights[atom_index];
            }
            // *************************************************//

            for (std::size_t cluster_index = 0; cluster_index < cluster.size();
                 cluster_index++) {
              neighbour_idx = neighbour_indices[cluster_index];

              if (use_gnn) {
                array_ops::as_eigen(f_ij) =
                    array_ops::as_eigen(
                        forces_gnn[graph_idx + cluster_index + 1]) *
                    local_mesh.sampling_atoms.nodal_weights[atom_index];
              } else {
                array_ops::as_eigen(f_ij) =
                    array_ops::as_eigen(cluster_forces[cluster_index]) *
                    local_mesh.sampling_atoms.nodal_weights[atom_index];
              }

              // minus because the weight of nodal sampling atom itself is 1.0
              // and the force vector is directed away.

              array_ops::as_eigen(forces[atom_index]) -=
                  array_ops::as_eigen(f_ij);

              // minus because the weight of nodal sampling atom itself is -1.0
              // thermal_forces[atomIndex]-=
              // dot(f_ij, sampling_atom_shifts[neighbourIdx])/double(DIM);

              // *************************************************//
              if (!use_gnn) {
                thermal_forces[atom_index] -=
                    cluster_thermal_force_sampling_atom[cluster_index] *
                    local_mesh.sampling_atoms.nodal_weights[atom_index];
              }
              // *************************************************//

              noof_dependencies =
                  local_mesh.sampling_atoms
                      .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                      .repatom_dependencies.size();

              if (!local_mesh.sampling_atoms
                       .nodal_sampling_atom_neighbours[atom_index]
                                                      [neighbour_idx]
                       .interproc_flag) {
                for (int d = 0; d < noof_dependencies; d++) {
                  local_repatom_idx =
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .first.second;

                  array_ops::as_eigen(forces[local_repatom_idx]) +=
                      array_ops::as_eigen(f_ij) *
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .second;

                  // thermal force. From derivative of thermalized Hamiltonian
                  // w.r.t sigma (unknown variable)
                  // *************************************************//

                  if (use_gnn) {
                    thermal_forces[local_repatom_idx] +=
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .repatom_dependencies[d]
                            .second *
                        thermal_forces_gnn[graph_idx + cluster_index + 1] *
                        local_mesh.sampling_atoms.nodal_weights[atom_index];
                  } else {
                    thermal_forces[local_repatom_idx] +=
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .repatom_dependencies[d]
                            .second *
                        cluster_thermal_force_neighbour[cluster_index] *
                        local_mesh.sampling_atoms.nodal_weights[atom_index];
                  }
                  // *************************************************//

                  if (std::isnan(array_ops::norm(f_ij))) {
                    throw detail::format_error(
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .thermal_coordinate,
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .location,
                        local_mesh.repatoms.thermal_coordinates[atom_index],
                        local_mesh.sampling_atoms.nodal_locations[atom_index]);
                  }
                }
              } else if (local_mesh.sampling_atoms
                             .nodal_sampling_atom_neighbours[atom_index]
                                                            [neighbour_idx]
                             .interproc_flag &&
                         !local_mesh.sampling_atoms
                              .nodal_sampling_atom_neighbours[atom_index]
                                                             [neighbour_idx]
                              .periodic_flag) {
                for (int d = 0; d < noof_dependencies; d++) {

                  dependent_rank =
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .first.first;

                  interproc_repatom_idx =
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .first.second;

                  for (std::size_t l = 0; l < DIM; l++) {
                    overlapping_nodes_forces_data_send
                        [dependent_rank]
                        [(DIM + 1) * interproc_repatom_idx + l] +=
                        f_ij[l] *
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .repatom_dependencies[d]
                            .second;
                  }

                  // *************************************************//
                  if (use_gnn)
                    t_force_temp =
                        thermal_forces_gnn[graph_idx + cluster_index + 1];
                  else
                    t_force_temp =
                        cluster_thermal_force_neighbour[cluster_index];

                  overlapping_nodes_forces_data_send[dependent_rank]
                                                    [(DIM + 1) *
                                                         interproc_repatom_idx +
                                                     DIM] +=
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .second *
                      t_force_temp *
                      local_mesh.sampling_atoms.nodal_weights[atom_index];
                }
              } else if (local_mesh.sampling_atoms
                             .nodal_sampling_atom_neighbours[atom_index]
                                                            [neighbour_idx]
                             .interproc_flag &&
                         local_mesh.sampling_atoms
                             .nodal_sampling_atom_neighbours[atom_index]
                                                            [neighbour_idx]
                             .periodic_flag) {
                for (int d = 0; d < noof_dependencies; d++) {
                  dependent_rank =
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .first.first;

                  interproc_repatom_idx =
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .first.second;

                  for (std::size_t l = 0; l < DIM; l++) {

                    periodic_overlapping_forces_send[dependent_rank]
                                                    [(DIM + 1) *
                                                         interproc_repatom_idx +
                                                     l] +=
                        f_ij[l] *
                        local_mesh.sampling_atoms
                            .nodal_sampling_atom_neighbours[atom_index]
                                                           [neighbour_idx]
                            .repatom_dependencies[d]
                            .second;
                  }

                  // *************************************************//
                  if (use_gnn)
                    t_force_temp =
                        thermal_forces_gnn[graph_idx + cluster_index + 1];
                  else
                    t_force_temp =
                        cluster_thermal_force_neighbour[cluster_index];

                  periodic_overlapping_forces_send[dependent_rank]
                                                  [(DIM + 1) *
                                                       interproc_repatom_idx +
                                                   DIM] +=
                      local_mesh.sampling_atoms
                          .nodal_sampling_atom_neighbours[atom_index]
                                                         [neighbour_idx]
                          .repatom_dependencies[d]
                          .second *
                      t_force_temp *
                      local_mesh.sampling_atoms.nodal_weights[atom_index];

                  // *************************************************//
                }
              }

              for (std::size_t i_period = 0; i_period < DIM; i_period++) {
                if (local_mesh.periodic_flags[i_period]) {
                  local_periodic_force[i_period] +=
                      f_ij[i_period] *
                      (cluster[cluster_index][i_period] -
                       local_mesh.sampling_atoms
                           .nodal_locations[atom_index][i_period]);
                }
              }
            }
            if (use_gnn) {
              local_mesh.nodal_cluster_energies_gnn[atom_index] =
                  energy_gnn[graph_idx];
              graph_idx += cluster.size() + 1;
            }
          }
        }

        local_mesh.repatoms.forces = forces;
        local_mesh.repatoms.thermal_forces = thermal_forces;

        if (mpi_size > 1) {
          int interproc_repatom_idx = 0;

          qcmesh::mpi::all_scatter(overlapping_nodes_forces_data_send,
                                   overlapping_nodes_forces_data_recv);

          // now add forces to overlapping nodes

          for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
            for (std::size_t i_nd = 0; i_nd < local_mesh.overlapping_mesh_data
                                                  .overlap_node_idxs[i_proc]
                                                  .size();
                 i_nd++) {
              interproc_repatom_idx = local_mesh.overlapping_mesh_data
                                          .overlap_node_idxs[i_proc][i_nd];

              for (std::size_t d = 0; d < DIM; d++)
                local_mesh.repatoms.forces[interproc_repatom_idx][d] +=
                    overlapping_nodes_forces_data_recv[i_proc]
                                                      [(DIM + 1) * i_nd + d];

              // *************************************************//
              local_mesh.repatoms.thermal_forces[interproc_repatom_idx] +=
                  overlapping_nodes_forces_data_recv[i_proc]
                                                    [(DIM + 1) * i_nd + DIM];

              // *************************************************//
            }
          }

          if (periodic_simulation_flag) {
            // exchange periodic overlaps
            qcmesh::mpi::all_scatter(periodic_overlapping_forces_send,
                                     periodic_overlapping_forces_recv);

            for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
              for (std::size_t i_nd = 0; i_nd < local_mesh.periodic_mesh_data
                                                    .overlap_node_idxs[i_proc]
                                                    .size();
                   i_nd++) {
                interproc_repatom_idx = local_mesh.periodic_mesh_data
                                            .overlap_node_idxs[i_proc][i_nd];

                for (std::size_t d = 0; d < DIM; d++)
                  local_mesh.repatoms.forces[interproc_repatom_idx][d] +=
                      periodic_overlapping_forces_recv[i_proc]
                                                      [(DIM + 1) * i_nd + d];

                // *************************************************//
                local_mesh.repatoms.thermal_forces[interproc_repatom_idx] +=
                    periodic_overlapping_forces_recv[i_proc]
                                                    [(DIM + 1) * i_nd + DIM];

                // *************************************************//
              }
            }
          }
        }

        if (constrain_positions) {
          for (auto &f : local_mesh.repatoms.forces) {
            f.fill(0.0);
          }
        }

        if (freeze_thermal_relaxation_flag) {
          for (auto &tf : local_mesh.repatoms.thermal_forces) {
            tf = 0.0;
          }
        }

        // make periodic domain force zero
        local_mesh.repatoms.periodic_forces.fill(0.0);

        const auto world = boost::mpi::communicator{};
        boost::mpi::all_reduce(world, local_periodic_force.data(), DIM,
                               local_mesh.repatoms.periodic_forces.data(),
                               std::plus<>());
}

} // namespace neighbourhoods
