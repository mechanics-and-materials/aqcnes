// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Compute derivatives of the potential with respect to impurity concentrations
 * @author S. Saxena
 */

#pragma once

#include "input/userinput.hpp"
#include "materials/material.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi.hpp>

namespace neighbourhoods {

/**
 * @brief get derivatives of energy wrt concentration
 */
template <typename Mesh, std::size_t DIM, std::size_t NIDOF, typename NodalData>
void compute_concentration_forces(
    Mesh &local_mesh,
    const std::vector<
        materials::Material<DIM, meshing::traits::N_IMPURITIES<NodalData>>>
        &solver_materials,
    const input::Userinput &input, const bool periodic_simulation_flag) {
  namespace array_ops = qcmesh::array_ops;
  constexpr std::size_t NOI = meshing::traits::N_IMPURITIES<NodalData>;
  using ConcentrationPoint = std::array<double, NOI>;
  const auto mpi_size = boost::mpi::communicator{}.size();

  std::vector<std::vector<double>> overlapping_nodes_forces_data_send(mpi_size);
  std::vector<std::vector<double>> overlapping_nodes_forces_data_recv(mpi_size);
  std::vector<std::vector<double>> periodic_overlapping_forces_send(mpi_size);
  std::vector<std::vector<double>> periodic_overlapping_forces_recv(mpi_size);

  std::vector<geometry::Point<DIM>> cluster;
  std::vector<std::array<double, NIDOF>> cluster_thermal_coordinates;
  std::vector<NodalData> cluster_nodal_data;
  std::vector<int> neighbour_indices;

  // resize exchange vectors
  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    overlapping_nodes_forces_data_send[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc].size() *
            NOI,
        0.0);
    overlapping_nodes_forces_data_recv[i_proc].resize(
        local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size() * NOI,
        0.0);

    if (periodic_simulation_flag) {
      periodic_overlapping_forces_send[i_proc].resize(
          local_mesh.periodic_mesh_data.overlapping_nodes_recv[i_proc].size() *
              NOI,
          0.0);
      periodic_overlapping_forces_recv[i_proc].resize(
          local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size() * NOI,
          0.0);
    }
  }

  ConcentrationPoint forces_atom;
  std::vector<ConcentrationPoint> forces_neighbors;

  // go over nodal sampling atoms
  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.nodal_locations.size() -
                        local_mesh.global_ghost_indices.size();
       atom_index++) {

    local_mesh.sampling_atoms.get_nodal_neighbour_thermal_cluster(
        atom_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);

    if (input.quad_type == 3) {
      solver_materials[0].get_concentration_forces_3(
          forces_atom, forces_neighbors,
          local_mesh.sampling_atoms.nodal_locations[atom_index],
          local_mesh.repatoms.thermal_coordinates[atom_index], cluster,
          cluster_thermal_coordinates,
          local_mesh.repatoms.nodal_data_points[atom_index],
          cluster_nodal_data);
    } else if (input.quad_type == 5) {
      solver_materials[0].get_concentration_forces_5(
          forces_atom, forces_neighbors,
          local_mesh.sampling_atoms.nodal_locations[atom_index],
          local_mesh.repatoms.thermal_coordinates[atom_index], cluster,
          cluster_thermal_coordinates,
          local_mesh.repatoms.nodal_data_points[atom_index],
          cluster_nodal_data);
    } else {
      throw exception::QCException{
          "Only 3 and 5 order quadratures supported at the "
          "moment."};
    }

    // distribute forces to repatoms
    array_ops::as_eigen(local_mesh.repatoms.nodal_data_points[atom_index]
                            .chemical_potentials) +=
        local_mesh.sampling_atoms.nodal_weights[atom_index] *
        array_ops::as_eigen(forces_atom);

    for (std::size_t cluster_index = 0; cluster_index < cluster.size();
         cluster_index++) {
      const auto neighbour_idx = neighbour_indices[cluster_index];
      const auto noof_dependencies =
          local_mesh.sampling_atoms
              .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
              .repatom_dependencies.size();

      // if the neighbor isn't interproc or periodic, simply add the corresponding
      //components to local repatoms else, add the info to a vector of double
      // for interproc/perodic exchange, and then add the component to the
      // relevant repatom index in the dependent rank

      if (!local_mesh.sampling_atoms
               .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
               .interproc_flag) {
        for (std::size_t d = 0; d < noof_dependencies; d++) {
          const auto local_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .first.second;

          array_ops::as_eigen(
              local_mesh.repatoms.nodal_data_points[local_repatom_idx]
                  .chemical_potentials) +=
              local_mesh.sampling_atoms.nodal_weights[atom_index] *
              array_ops::as_eigen(forces_neighbors[cluster_index]) *
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .second;
        }
      } else if (local_mesh.sampling_atoms
                     .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                     .interproc_flag &&
                 !local_mesh.sampling_atoms
                      .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                      .periodic_flag) {
        for (std::size_t d = 0; d < noof_dependencies; d++) {
          const auto dependent_rank =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .first.first;

          const auto interproc_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .first.second;

          // add info to be communicated with dependent ranks
          for (std::size_t si = 0; si < NOI; si++)
            overlapping_nodes_forces_data_send[dependent_rank]
                                              [NOI * interproc_repatom_idx +
                                               si] +=
                local_mesh.sampling_atoms.nodal_weights[atom_index] *
                forces_neighbors[cluster_index][si] *
                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                    .repatom_dependencies[d]
                    .second;
        }
      } else if (local_mesh.sampling_atoms
                     .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                     .interproc_flag &&
                 local_mesh.sampling_atoms
                     .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                     .periodic_flag) {
        for (std::size_t d = 0; d < noof_dependencies; d++) {
          const auto dependent_rank =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .first.first;

          const auto interproc_repatom_idx =
              local_mesh.sampling_atoms
                  .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                  .repatom_dependencies[d]
                  .first.second;

          // add info to be communicated with dependent ranks
          for (std::size_t si = 0; si < NOI; si++)
            periodic_overlapping_forces_send[dependent_rank]
                                            [NOI * interproc_repatom_idx +
                                             si] +=
                local_mesh.sampling_atoms.nodal_weights[atom_index] *
                forces_neighbors[cluster_index][si] *
                local_mesh.sampling_atoms
                    .nodal_sampling_atom_neighbours[atom_index][neighbour_idx]
                    .repatom_dependencies[d]
                    .second;
        }
      }
    }
  } // loop over nodal sampling atoms

  // go over central sampling atoms
  for (std::size_t atom_index = 0;
       atom_index < local_mesh.sampling_atoms.central_locations.size();
       atom_index++) {
    local_mesh.sampling_atoms.get_central_neighbour_thermal_cluster(
        atom_index, cluster, cluster_thermal_coordinates, cluster_nodal_data,
        neighbour_indices);

    if (input.quad_type == 3) {
      solver_materials[0].get_concentration_forces_3(
          forces_atom, forces_neighbors,
          local_mesh.sampling_atoms.nodal_locations[atom_index],
          local_mesh.repatoms.thermal_coordinates[atom_index], cluster,
          cluster_thermal_coordinates,
          local_mesh.repatoms.nodal_data_points[atom_index],
          cluster_nodal_data);
    } else if (input.quad_type == 5) {
      solver_materials[0].get_concentration_forces_5(
          forces_atom, forces_neighbors,
          local_mesh.sampling_atoms.nodal_locations[atom_index],
          local_mesh.repatoms.thermal_coordinates[atom_index], cluster,
          cluster_thermal_coordinates,
          local_mesh.repatoms.nodal_data_points[atom_index],
          cluster_nodal_data);
    } else {
      throw exception::QCException{
          "Only 3 and 5 order quadratures supported at the "
          "moment."};
    }

    const auto element_key_central_atom =
        local_mesh.sampling_atoms.cell_key_of_central_atoms[atom_index];

    const auto indexed_cell =
        indexed_simplex(local_mesh, element_key_central_atom);

    for (std::size_t d = 0; d < DIM + 1; d++) {
      array_ops::as_eigen(local_mesh.repatoms.nodal_data_points[indexed_cell[d]]
                              .chemical_potentials) +=
          (1.0 / double(DIM + 1)) * array_ops::as_eigen(forces_atom) *
          local_mesh.sampling_atoms.central_weights[atom_index];
    }

    for (std::size_t cluster_index = 0; cluster_index < cluster.size();
         cluster_index++) {
      const auto neighbour_idx = neighbour_indices[cluster_index];

      const auto noof_dependencies =
          local_mesh.sampling_atoms
              .central_sampling_atom_neighbours[atom_index][neighbour_idx]
              .repatom_dependencies.size();

      for (std::size_t d = 0; d < noof_dependencies; d++) {
        const auto local_repatom_idx =
            local_mesh.sampling_atoms
                .central_sampling_atom_neighbours[atom_index][neighbour_idx]
                .repatom_dependencies[d]
                .first.second;

        array_ops::as_eigen(
            local_mesh.repatoms.nodal_data_points[local_repatom_idx]
                .chemical_potentials) +=
            array_ops::as_eigen(forces_neighbors[cluster_index]) *
            local_mesh.sampling_atoms
                .central_sampling_atom_neighbours[atom_index][neighbour_idx]
                .repatom_dependencies[d]
                .second *
            local_mesh.sampling_atoms.central_weights[atom_index];
      }
    }
  } // loop over central sampling atoms

  // exchange interproc and periodic force info
  if (mpi_size > 1) {
    int interproc_repatom_idx = 0;
    // exchange interproc overlaps

    qcmesh::mpi::all_scatter(overlapping_nodes_forces_data_send,
                             overlapping_nodes_forces_data_recv);

    // now add forces to overlapping nodes
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      for (std::size_t i_nd = 0;
           i_nd <
           local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size();
           i_nd++) {
        interproc_repatom_idx =
            local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc][i_nd];

        for (std::size_t si = 0; si < NOI; si++)
          local_mesh.repatoms.nodal_data_points[interproc_repatom_idx]
              .chemical_potentials[si] +=
              overlapping_nodes_forces_data_recv[i_proc][NOI * i_nd + si];
      }
    }

    if (periodic_simulation_flag) {
      // exchange periodic overlaps
      qcmesh::mpi::all_scatter(periodic_overlapping_forces_send,
                               periodic_overlapping_forces_recv);

      // now add forces to overlapping nodes
      for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
        for (std::size_t i_nd = 0;
             i_nd <
             local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc].size();
             i_nd++) {
          interproc_repatom_idx =
              local_mesh.periodic_mesh_data.overlap_node_idxs[i_proc][i_nd];

          for (std::size_t si = 0; si < NOI; si++)
            local_mesh.repatoms.nodal_data_points[interproc_repatom_idx]
                .chemical_potentials[si] +=
                periodic_overlapping_forces_recv[i_proc][NOI * i_nd + si];
        }
      }
    }
  }

  // normalize with repatom weights
  for (std::size_t i = 0; i < local_mesh.repatoms.locations.size(); i++) {
    array_ops::as_eigen(
        local_mesh.repatoms.nodal_data_points[i].chemical_potentials) =
        array_ops::as_eigen(
            local_mesh.repatoms.nodal_data_points[i].chemical_potentials) /
        local_mesh.repatoms.nodal_weights[i];
  }
}

} // namespace neighbourhoods
