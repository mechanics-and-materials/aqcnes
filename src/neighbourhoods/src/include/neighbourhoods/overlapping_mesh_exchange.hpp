// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Low level function defintions for overlap mesh data exchange
 * @author P. Gupta, S. Saxena
 */

#pragma once

#include "geometry/point.hpp"
#include "meshing/nodal_data_interproc.hpp"
#include "qcmesh/geometry/indexed.hpp"
#include "qcmesh/geometry/intersect_balls_with_simplex.hpp"
#include "qcmesh/geometry/octree.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi.hpp>

namespace neighbourhoods {

template <typename Mesh, std::size_t DIM, std::size_t NIDOF, typename NodalData>
void update_overlapping_mesh_elements_and_nodes(
    Mesh &local_mesh,
    const std::vector<qcmesh::geometry::Ball<DIM>> &interproc_atom_balls,
    const bool print_to_console) {
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  // -gather information about refinements. If refinements happened,
  // then we re-initialize the mesh overlaps. Otherwise there is too much book
  // keeping.
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  // -even if we end up sending duplicate points, its fine. We should not send
  // duplicate elements.
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  using Point = geometry::Point<DIM>;
  using Offset = std::vector<std::array<double, DIM>>;
  using Ball = qcmesh::geometry::Ball<DIM>;

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  constexpr std::size_t noi = meshing::traits::N_IMPURITIES<NodalData>;
#ifdef FINITE_TEMPERATURE
  constexpr std::size_t nidof = NIDOF;
#else
  constexpr std::size_t nidof = 0;
#endif

  std::vector<std::size_t> noof_interproc_balls_by_proc(mpi_size, 0);
  std::vector<double> balls_data_to_bcast;

  std::size_t noof_interproc_sampling_atoms = interproc_atom_balls.size();

  balls_data_to_bcast.reserve(noof_interproc_sampling_atoms * (DIM + 1));
  // prepare balls to broadcast
  for (std::size_t i = 0; i < interproc_atom_balls.size(); i++) {
    for (std::size_t d = 0; d < DIM; d++) {
      balls_data_to_bcast.push_back(interproc_atom_balls[i].center[d]);
    }

    balls_data_to_bcast.push_back(interproc_atom_balls[i].radius);
  }

  // all-gather amount of interproc balls-data to exchange
  boost::mpi::all_gather(world, noof_interproc_sampling_atoms,
                         noof_interproc_balls_by_proc);

  std::vector<std::vector<double>> balls_data_all_proc(mpi_size);
  boost::mpi::all_gather(world, balls_data_to_bcast, balls_data_all_proc);

  auto global_noof_balls_data = std::size_t{};
  for (const auto &balls_data : balls_data_all_proc)
    global_noof_balls_data += balls_data.size();

  std::vector<qcmesh::geometry::Indexed<Ball>> balls_all_proc;
  balls_all_proc.reserve(global_noof_balls_data / (DIM + 1));

  std::vector<std::vector<int>> ranks_of_unique_balls;
  ranks_of_unique_balls.reserve(global_noof_balls_data / (DIM + 1));

  auto unique_balls = qcmesh::geometry::octree::Octree<
      qcmesh::geometry::Indexed<std::array<double, DIM>>>(
      local_mesh.domain_bound);
  constexpr double TOLERANCE = 0.1;

  Ball interproc_ball;

  for (int ball_rank = 0; ball_rank < mpi_size; ball_rank++) {
    std::size_t ball_data_ctr = 0;
    for (std::size_t ball_ctr = 0;
         ball_ctr < noof_interproc_balls_by_proc[ball_rank]; ball_ctr++) {

      for (std::size_t d = 0; d < DIM; d++) {
        interproc_ball.center[d] =
            balls_data_all_proc[ball_rank][ball_data_ctr];
        ball_data_ctr++;
      }

      interproc_ball.radius = balls_data_all_proc[ball_rank][ball_data_ctr];
      ball_data_ctr++;

      auto collisions = unique_balls.find_or_insert(
          qcmesh::geometry::Indexed<std::array<double, DIM>>{
              unique_balls.size(), interproc_ball.center},
          TOLERANCE);

      if (collisions.empty()) {
        balls_all_proc.emplace_back(qcmesh::geometry::Indexed<Ball>{
            balls_all_proc.size(), interproc_ball});
        ranks_of_unique_balls.push_back(std::vector<int>{ball_rank});
      } else {
        // it was previously added
        for (const auto &collision : collisions)
          ranks_of_unique_balls[collision.index].push_back(ball_rank);
      }
    }
  }

  //=-=-=-=- Build a tree once for all the received balls-=-=-=-=-//
  // get the elements that intersect other's balls (not our own)
  // Try to make sure that the balls are mostly unique. If the KDTree
  // fails, then the leafsize of KDTree needs to be increased. Will happen
  // if too many points are being repeated. We need the unique container!

  auto interproc_balls = qcmesh::geometry::kd_tree::KDTree(balls_all_proc);

  std::vector<std::vector<std::size_t>> added_simplex_idxs_for_sending(
      mpi_size);
  std::vector<std::vector<std::size_t>> added_node_idxs_for_sending(mpi_size);

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    added_simplex_idxs_for_sending[i_proc].reserve(local_mesh.cells.size() /
                                                   mpi_size);

    added_node_idxs_for_sending[i_proc].reserve(local_mesh.nodes.size() /
                                                mpi_size);
  }

  for (std::size_t i_el = 0; i_el != local_mesh.cells.size(); ++i_el) {
    const auto simplex = qcmesh::mesh::simplex_cell_points(
        local_mesh, local_mesh.simplex_index_to_key[i_el]);

    for (const auto &ball : qcmesh::geometry::intersect_balls_with_simplex(
             interproc_balls, simplex,
             qcmesh::geometry::max_radius(balls_all_proc))) {
      // add omp with locks later
      //(https://computing.llnl.gov/tutorials/openMP/#OMP_SET_LOCK)
      for (const auto unique_ball_rank : ranks_of_unique_balls[ball.index]) {
        // need to mark if this element has been sent to unique_ball_rank
        // earlier
        if (!local_mesh.overlapping_mesh_data
                 .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el]) {
          // mark this element as sent, so we dont send again and again!
          local_mesh.overlapping_mesh_data
              .overlap_simplex_idxs_sent_flag[unique_ball_rank][i_el] = true;

          added_simplex_idxs_for_sending[unique_ball_rank].push_back(i_el);
        }
      }
    }
  }
  // now prepare newly added simplices' data for sending
  // now we have simplex ids that need to be sent. We have to send
  // one by one and receive, with traditional non-blocking MPI_Isend
  // data to be sent is nodes (coordinates), lattice vectors,
  // atomistic flags.
  std::vector<std::vector<double>> all_send_double_data(mpi_size);
  std::vector<std::vector<int>> all_send_int_data(mpi_size);

  std::vector<int> noof_simplices_send(mpi_size, 0);
  std::vector<int> noof_nodes_send(mpi_size, 0);

  std::vector<double> meshnodes_send;
  std::vector<int> meshnodes_int_data_send;

  int noof_int_data_per_node = 2;

  meshnodes_send.reserve(local_mesh.nodes.size() * DIM +
                         nidof * local_mesh.nodes.size() +
                         noi * local_mesh.nodes.size());

  meshnodes_int_data_send.reserve(local_mesh.nodes.size() *
                                  noof_int_data_per_node);

  // we have to send only 2 quantities (omega and sigma)

  std::size_t node_index = 0;
  std::size_t sent_index = 0;
  geometry::Point<DIM> node_point;

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank && !added_simplex_idxs_for_sending[i_proc].empty()) {

      noof_simplices_send[i_proc] =
          static_cast<int>(added_simplex_idxs_for_sending[i_proc].size());

      all_send_double_data[i_proc].reserve(noof_simplices_send[i_proc] *
                                               (DIM * DIM) +
                                           local_mesh.nodes.size() * DIM);

      all_send_int_data[i_proc].reserve(
          noof_simplices_send[i_proc] * (DIM + 1 + 1) +
          local_mesh.nodes.size() * noof_int_data_per_node);

      meshnodes_send.clear();
      meshnodes_int_data_send.clear();

      for (const auto idx : added_simplex_idxs_for_sending[i_proc]) {
        const auto cell_key = local_mesh.simplex_index_to_key[idx];
        const auto &cell = local_mesh.cells.at(cell_key);
        for (std::size_t d = 0; d < DIM + 1; d++) {
          const auto node_key = cell.nodes[d];

          node_index = local_mesh.nodes.at(node_key).data.mesh_index;

          if (local_mesh.overlapping_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .find(static_cast<int>(node_key)) ==
              local_mesh.overlapping_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .end()) {
            sent_index = local_mesh.overlapping_mesh_data
                             .overlap_nodes_sent_idx_recv_side[i_proc]
                             .size();

            local_mesh.overlapping_mesh_data
                .overlap_nodes_sent_idx_recv_side[i_proc]
                .insert(std::make_pair(static_cast<int>(node_key),
                                       static_cast<int>(sent_index)));

            added_node_idxs_for_sending[i_proc].push_back(node_index);

            node_point = local_mesh.nodes.at(node_key).position;

            for (std::size_t l = 0; l < DIM; l++) {
              // DIM number of coordinates
              meshnodes_send.push_back(node_point[l]);
            }

            meshnodes_int_data_send.push_back(
                local_mesh.material_idx_of_nodes[node_index]);
            meshnodes_int_data_send.push_back(
                local_mesh.repatoms.lattice_site_type[node_index]);

#ifdef FINITE_TEMPERATURE
            // send omega/m and sigma
            meshnodes_send.push_back(
                local_mesh.repatoms.thermal_coordinates[node_index][0]);

            meshnodes_send.push_back(
                local_mesh.repatoms.thermal_coordinates[node_index][1]);

#endif
            for (std::size_t l = 0; l < noi; l++)
              meshnodes_send.push_back(
                  local_mesh.repatoms.nodal_data_points[node_index]
                      .impurity_concentrations[l]);
            // we recieve back forces in this order and just add to these
            // repatoms

            noof_nodes_send[i_proc]++;
          }
          // DIM+1 number of vertex indices.
          // noof nodes is not the correct index, because some nodes
          // would already be there in the receiving rank. This
          // would work only one time

          // map_sent.at(nodeIndex) = sentIndex;
          // sentIndex is simply the size of map_sent.
          // noof_nodes_sent[i_proc] is only for
          // one time exchange. Always make the simplex
          // using sentIndex so we can easily access it
          // on the receiving side.

          // While receiving, I could have just offset the
          // index of nodes by existing size, but this would prevent
          // repeated exchange of nodes

          all_send_int_data[i_proc].push_back(
              local_mesh.overlapping_mesh_data
                  .overlap_nodes_sent_idx_recv_side[i_proc]
                  .at(static_cast<int>(node_key)));
        }

        all_send_int_data[i_proc].push_back(cell.data.is_atomistic);

        // DIM*DIM lattice vector components
        for (std::size_t m = 0; m < DIM; m++) {
          for (std::size_t n = 0; n < DIM; n++) {
            all_send_double_data[i_proc].push_back(
                cell.data.lattice_vectors[m][n]);
          }
        }
      }

      all_send_double_data[i_proc].insert(
          all_send_double_data[i_proc].end(),
          make_move_iterator(meshnodes_send.begin()),
          make_move_iterator(meshnodes_send.end()));

      all_send_int_data[i_proc].insert(
          all_send_int_data[i_proc].end(),
          make_move_iterator(meshnodes_int_data_send.begin()),
          make_move_iterator(meshnodes_int_data_send.end()));
    }
  }

  if (print_to_console) {
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank) {
        std::cout << "interproc mesh exchange: " << mpi_rank << " sending "
                  << noof_simplices_send[i_proc] << " simplices and "
                  << noof_nodes_send[i_proc] << " nodes to " << i_proc
                  << std::endl;
      }
    }
  }

  std::vector<double>().swap(meshnodes_send);
  std::vector<int> noof_simplices_recv(mpi_size, 0);
  std::vector<int> noof_nodes_recv(mpi_size, 0);

  noof_simplices_recv = qcmesh::mpi::all_scatter(noof_simplices_send);
  noof_nodes_recv = qcmesh::mpi::all_scatter(noof_nodes_send);

  if (print_to_console) {
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank) {
        std::cout << "interproc mesh exchange: " << mpi_rank << " receiving "
                  << noof_simplices_recv[i_proc] << " simplices and "
                  << noof_nodes_recv[i_proc] << " nodes from " << i_proc
                  << std::endl;
      }
    }
  }

  std::vector<std::vector<double>> all_recv_double_data(mpi_size);
  std::vector<std::vector<int>> all_recv_int_data(mpi_size);
  for (int i = 0; i < mpi_size; i++) {
    all_recv_double_data[i].resize(
        (DIM * DIM) * noof_simplices_recv[i] + (DIM)*noof_nodes_recv[i] +
        nidof * noof_nodes_recv[i] + noi * noof_nodes_recv[i]);

    all_recv_int_data[i].resize(
        (DIM + 1 + 1) * noof_simplices_recv[i] +
        static_cast<uint64_t>(noof_int_data_per_node * noof_nodes_recv[i]));
  }

  qcmesh::mpi::all_scatter(all_send_int_data, all_recv_int_data);
  qcmesh::mpi::all_scatter(all_send_double_data, all_recv_double_data);

  int simplex_data_offset = 0;
  int simplex_int_data_offset = 0;

  std::array<std::size_t, DIM + 1> recv_simplex{};
  Point recv_node;
  std::array<std::array<double, DIM>, DIM> recv_matrix{};
  Offset offset;

  int node_int_data_idx_offset = 2;

#ifdef FINITE_TEMPERATURE
  std::array<double, NIDOF> recv_thermal_dof{};
#endif

  constexpr std::size_t node_data_idx_rho_offset = DIM + nidof;
  constexpr std::size_t node_data_idx_offset = DIM + nidof + noi;

  NodalData recv_nodal_data{};

  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank && noof_simplices_recv[i_proc] > 0) {

      local_mesh.overlapping_mesh_data.overlapping_simplices_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_simplices_recv[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      local_mesh.overlapping_mesh_data
          .overlapping_element_lattice_vectors_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_element_lattice_vectors_recv[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      local_mesh.overlapping_mesh_data.overlapping_element_offsets_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_element_offsets_recv[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      local_mesh.overlapping_mesh_data
          .overlapping_simplices_recv_atomistic_flag[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_simplices_recv_atomistic_flag[i_proc]
                       .size() +
                   noof_simplices_recv[i_proc]);

      // directly append into the relevant data vectors
      for (int i_el_recv = 0; i_el_recv < noof_simplices_recv[i_proc];
           i_el_recv++) {
        offset.clear();
        for (std::size_t d = 0; d < DIM; d++) {
          for (std::size_t l = 0; l < DIM; l++) {
            recv_matrix[d][l] =
                all_recv_double_data[i_proc]
                                    [DIM * DIM * i_el_recv + d * DIM + l];
          }
        }

        local_mesh.overlapping_mesh_data
            .overlapping_element_lattice_vectors_recv[i_proc]
            .push_back(recv_matrix);

        //compute offsets from the received lattice vectors

        offset.resize(local_mesh.element_offset_coeffs.size());

        for (std::size_t p = 0; p < local_mesh.element_offset_coeffs.size();
             p++) {
          for (std::size_t d = 0; d < DIM; d++) {
            for (std::size_t e = 0; e < DIM; e++) {
              offset[p][d] +=
                  local_mesh.element_offset_coeffs[p][e] * recv_matrix[d][e];
            }
          }
        }
        local_mesh.overlapping_mesh_data
            .overlapping_element_offsets_recv[i_proc]
            .push_back(offset);

        for (std::size_t d = 0; d < DIM + 1; d++) {
          recv_simplex[d] = static_cast<std::size_t>(
              all_recv_int_data[i_proc][(DIM + 1 + 1) * i_el_recv + d]);
        }

        local_mesh.overlapping_mesh_data.overlapping_simplices_recv[i_proc]
            .push_back(recv_simplex);

        local_mesh.overlapping_mesh_data
            .overlapping_simplices_recv_atomistic_flag[i_proc]
            .push_back(
                all_recv_int_data[i_proc][(DIM + 1 + 1) * i_el_recv + DIM + 1]);
      }

      simplex_data_offset = DIM * DIM * noof_simplices_recv[i_proc];
      simplex_int_data_offset = (DIM + 1 + 1) * noof_simplices_recv[i_proc];

      local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc].reserve(
          local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc]
              .size() +
          noof_nodes_recv[i_proc]);

      local_mesh.overlapping_mesh_data
          .overlapping_node_material_ids_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_node_material_ids_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);

      local_mesh.overlapping_mesh_data.overlapping_node_site_types_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_node_site_types_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);

#ifdef FINITE_TEMPERATURE
      local_mesh.overlapping_mesh_data.overlapping_thermal_dofs_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_thermal_dofs_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);
#endif
      local_mesh.overlapping_mesh_data.overlapping_nodal_data_recv[i_proc]
          .reserve(local_mesh.overlapping_mesh_data
                       .overlapping_nodal_data_recv[i_proc]
                       .size() +
                   noof_nodes_recv[i_proc]);

      for (int ind_recv = 0; ind_recv < noof_nodes_recv[i_proc]; ind_recv++) {
        recv_node.fill(0.0);
#ifdef FINITE_TEMPERATURE
        recv_thermal_dof.fill(0.0);
#endif

        for (std::size_t d = 0; d < DIM; d++) {
          recv_node[d] =
              all_recv_double_data[i_proc][simplex_data_offset +
                                           node_data_idx_offset * ind_recv + d];
        }

        local_mesh.overlapping_mesh_data.overlapping_nodes_recv[i_proc]
            .push_back(recv_node);

        local_mesh.overlapping_mesh_data
            .overlapping_node_material_ids_recv[i_proc]
            .push_back(
                all_recv_int_data[i_proc][simplex_int_data_offset +
                                          node_int_data_idx_offset * ind_recv]);

        local_mesh.overlapping_mesh_data
            .overlapping_node_site_types_recv[i_proc]
            .push_back(
                all_recv_int_data[i_proc]
                                 [simplex_int_data_offset +
                                  node_int_data_idx_offset * ind_recv + 1]);

#ifdef FINITE_TEMPERATURE
        for (std::size_t d = 0; d < NIDOF; d++) {
          recv_thermal_dof[d] =
              all_recv_double_data[i_proc]
                                  [simplex_data_offset +
                                   node_data_idx_offset * ind_recv + DIM + d];
        }
        local_mesh.overlapping_mesh_data.overlapping_thermal_dofs_recv[i_proc]
            .push_back(recv_thermal_dof);

#endif
        for (std::size_t d = 0; d < noi; d++) {
          recv_nodal_data.impurity_concentrations[d] =
              all_recv_double_data[i_proc][simplex_data_offset +
                                           node_data_idx_offset * ind_recv +
                                           node_data_idx_rho_offset + d];
        }
        local_mesh.overlapping_mesh_data.overlapping_nodal_data_recv[i_proc]
            .push_back(recv_nodal_data);
      }
    }
  }

  // append the newly added simplices for sending to owner rank map
  // append the newly added nodes for sending to owner rank map
  // Note again, its ok to add non-unique nodes, but we make sure that
  // only unique simplices are sent
  for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
    if (i_proc != mpi_rank && !added_simplex_idxs_for_sending[i_proc].empty()) {
      local_mesh.overlapping_mesh_data.overlap_simplex_idxs[i_proc].reserve(
          local_mesh.overlapping_mesh_data.overlap_simplex_idxs[i_proc].size() +
          added_simplex_idxs_for_sending[i_proc].size());

      local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].reserve(
          local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].size() +
          added_node_idxs_for_sending[i_proc].size());

      local_mesh.overlapping_mesh_data.overlap_simplex_idxs[i_proc].insert(
          local_mesh.overlapping_mesh_data.overlap_simplex_idxs[i_proc].end(),
          make_move_iterator(added_simplex_idxs_for_sending[i_proc].begin()),
          make_move_iterator(added_simplex_idxs_for_sending[i_proc].end()));

      local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].insert(
          local_mesh.overlapping_mesh_data.overlap_node_idxs[i_proc].end(),
          make_move_iterator(added_node_idxs_for_sending[i_proc].begin()),
          make_move_iterator(added_node_idxs_for_sending[i_proc].end()));
    }

    std::vector<std::size_t>().swap(added_simplex_idxs_for_sending[i_proc]);
    std::vector<std::size_t>().swap(added_node_idxs_for_sending[i_proc]);
  }
}

} // namespace neighbourhoods
