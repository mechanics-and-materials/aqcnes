// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/nanoindenter_sphere.hpp"
#include "boundarycondition/nullapplicator.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/write_mesh_to_nc.hpp"
#include "netcdf_io/load_boundary_conditions.hpp"
#include "netcdf_io/load_external_force_applicator.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include "solver/solver.hpp"
#include "testing/create_mesh.hpp"
#include "testing/find_path.hpp"
#include <gmock/gmock.h>
#include <set>

namespace meshing {
namespace {

constexpr qcmesh::geometry::Box<3> ATOMISTIC_DOMAIN{{-1., -1., -1},
                                                    {1., 1., 1.}};

constexpr netcdf_io::ExternalForceApplicatorDef<3> REFERENCE_FORCE_APPLICATOR{
    boundarycondition::ForceApplicatorType::Sphere, 10., 11., 12.,
    std::array<double, 3>{13., 14., 15.}};

constexpr netcdf_io::BoundaryConditionDef<3> BOUNDARY_CONDITION_MIN{
    boundarycondition::BoundaryConditionType::Free,
    std::array<double, 3>{20., 21., 22.},
    23,
    24.,
};
constexpr netcdf_io::BoundaryConditionDef<3> BOUNDARY_CONDITION_MAX{
    boundarycondition::BoundaryConditionType::Free,
    std::array<double, 3>{25., 26., 27.},
    28,
    29.,
};

const QCCellData<3, 3> restart_cell_data = QCCellData<3, 3>{
    .jacobian = 4.0,
    .density = 5.0,
    // det = -2.0:
    .lattice_vectors = {std::array<double, 3>{1., 1., 0.},
                        std::array<double, 3>{1., 0., 1.},
                        std::array<double, 3>{0., 1., 1.}},
    .is_atomistic = true,
};

const QCCellData<3, 3> cell_data = QCCellData<3, 3>{
    .lattice_vectors = {std::array<double, 3>{1., 1., 0.},
                        std::array<double, 3>{1., 0., 1.},
                        std::array<double, 3>{0., 1., 1.}},
};

const netcdf_io::RestartData<3> restart_data = netcdf_io::RestartData<3>{
    .outputfile_iter = 1,
    .restartfile_iter = 2,
    .external_strain = 1.0,
    .external_strain_increment = 0.5,
    .external_strain_time_step = 0.25,
    .periodic_lengths = {1.0, 2.0, 3.0},
    .apply_fixed_box = true,
};

template <std::size_t Dimension>
void write_restart_mesh(const std::filesystem::path &path) {
  auto mesh = testing::create_mesh(qcmesh::mesh::testing::mesh_1b(), false);
  mesh.periodic_lengths = restart_data.periodic_lengths;
  for (auto &[node_id, node] : mesh.nodes) {
    node.data.type = static_cast<int>(node_id);
    node.data.periodic_boundary_flag = true;
  }
  for (auto &[_, cell] : mesh.cells)
    cell.data = restart_cell_data;
  auto solver_variables =
      solver::SolverVariables<Dimension,
                              meshing::traits::N_IMPURITIES<decltype(mesh)>>{};
  solver_variables.local_mesh.domain_bound = geometry::distributed_box_hull(
      solver_variables.local_mesh.local_domain_bound);
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          std::array<boundarycondition::TwoSidedBoundaryConditions, 3>{},
          mesh.domain_bound);
  solver_variables.boundary_conditions[0].normal =
      BOUNDARY_CONDITION_MIN.normal;
  solver_variables.boundary_conditions[0].coordinate =
      BOUNDARY_CONDITION_MIN.coordinate;
  solver_variables.boundary_conditions[0].bound = BOUNDARY_CONDITION_MIN.bound;
  solver_variables.boundary_conditions[1].normal =
      BOUNDARY_CONDITION_MAX.normal;
  solver_variables.boundary_conditions[1].coordinate =
      BOUNDARY_CONDITION_MAX.coordinate;
  solver_variables.boundary_conditions[1].bound = BOUNDARY_CONDITION_MAX.bound;
  const auto force_applicator = REFERENCE_FORCE_APPLICATOR;
  solver_variables.external_force_applicator =
      std::make_unique<boundarycondition::NanoindenterSphere<Dimension>>(
          force_applicator.radius, force_applicator.force_constant,
          force_applicator.delta, force_applicator.center);

  meshing::write_restart_mesh_to_nc(
      path, mesh, restart_data, solver_variables.boundary_conditions,
      *solver_variables.external_force_applicator);
  solver_variables.boundary_conditions.clear();
}

void write_mesh(const std::filesystem::path &path,
                const bool save_lattice_vectors) {
  auto mesh = testing::create_mesh(qcmesh::mesh::testing::mesh_1b(), false);
  mesh.atomistic_domain = ATOMISTIC_DOMAIN;
  for (auto &[node_id, node] : mesh.nodes)
    node.data.type = static_cast<int>(node_id);
  for (auto &[_, cell] : mesh.cells)
    cell.data = restart_cell_data;
  meshing::write_mesh_to_nc(path, mesh, save_lattice_vectors);
}

constexpr std::array MATERIAL_BASIS = std::array{
    std::array<double, 3>{2.0, 0.0, 0.0}, std::array<double, 3>{0.0, 2.0, 0.0},
    std::array<double, 3>{0.0, 0.0, 2.0}};

template <std::size_t Dimension>
materials::MaterialBase<Dimension> create_test_material() {
  return materials::MaterialBase<Dimension>{
      .basis_vectors = MATERIAL_BASIS,
      .lattice_parameter_vec = {{3.615, 3.615, 3.615}},
      .atomic_masses = std::vector<double>{63.546},
      .neighbor_cutoff_radius = 4.32,
      .lattice_type = lattice::LatticeStructure::FCC,
      .lattice_structure = lattice::LatticeStructure::FCC};
}

void check_mesh(const QCMesh<> &mesh, const QCCellData<3, 3> &cell_data) {
  auto node_types = std::set<int>{};
  for (const auto &[_, node] : mesh.nodes)
    node_types.emplace(node.data.type);
  EXPECT_THAT(qcmesh::mpi::all_union_set(node_types),
              testing::UnorderedElementsAre(0, 1, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ(qcmesh::mpi::all_reduce_sum(mesh.cells.size()), 9);
  for (const auto &[_, cell] : mesh.cells) {
    EXPECT_DOUBLE_EQ(cell.data.density, cell_data.density);
    EXPECT_THAT(cell.data.lattice_vectors,
                qcmesh::testing::DoubleMatrixEq(cell_data.lattice_vectors));
  }
}

void check_restart_mesh(const QCMesh<> &mesh) {
  for (const auto &[_, node] : mesh.nodes) {
    EXPECT_EQ(node.data.periodic_boundary_flag, true);
  }
  for (const auto &[_, cell] : mesh.cells) {
    EXPECT_EQ(cell.data.is_atomistic, restart_cell_data.is_atomistic);
  }
  EXPECT_EQ(mesh.periodic_lengths.size(), restart_data.periodic_lengths.size());
  for (std::size_t i = 0; i < mesh.periodic_lengths.size(); i++) {
    EXPECT_DOUBLE_EQ(mesh.periodic_lengths[i],
                     restart_data.periodic_lengths[i]);
  }
  check_mesh(mesh, restart_cell_data);
}

void check_atomistic_domain(const QCMesh<> &mesh) {
  for (std::size_t i = 0; i < ATOMISTIC_DOMAIN.upper.size(); i++) {
    EXPECT_DOUBLE_EQ(mesh.atomistic_domain.upper[i], ATOMISTIC_DOMAIN.upper[i]);
  }
  for (std::size_t i = 0; i < ATOMISTIC_DOMAIN.lower.size(); i++) {
    EXPECT_DOUBLE_EQ(mesh.atomistic_domain.lower[i], ATOMISTIC_DOMAIN.lower[i]);
  }
}

TEST(test_meshing, read_recovers_write_restart) {
  write_restart_mesh<3>("/tmp/restart.nc");
  const auto material = create_test_material<3>();
  const auto [data, mesh] = meshing::load_restart_mesh_from_nc(
      "/tmp/restart.nc", std::vector{material});
  EXPECT_EQ(data.outputfile_iter, restart_data.outputfile_iter);
  EXPECT_EQ(data.restartfile_iter, restart_data.restartfile_iter);
  EXPECT_DOUBLE_EQ(data.external_strain, restart_data.external_strain);
  EXPECT_DOUBLE_EQ(data.external_strain_increment,
                   restart_data.external_strain_increment);
  EXPECT_DOUBLE_EQ(data.external_strain_time_step,
                   restart_data.external_strain_time_step);
  EXPECT_EQ(data.apply_fixed_box, restart_data.apply_fixed_box);
  check_restart_mesh(mesh);
  for (const auto &[_, cell] : mesh.cells) {
    EXPECT_DOUBLE_EQ(cell.data.jacobian, restart_cell_data.jacobian);
  }
}

TEST(test_meshing, load_external_force_applicator_recovers_write_restart) {
  write_restart_mesh<3>("/tmp/restart-force_applicator.nc");
  const auto force_applicator = netcdf_io::load_external_force_applicator<3>(
      "/tmp/restart-force_applicator.nc");
  const auto expected_force_applicator = REFERENCE_FORCE_APPLICATOR;

  EXPECT_EQ(force_applicator.type, expected_force_applicator.type);
  EXPECT_DOUBLE_EQ(force_applicator.force_constant,
                   expected_force_applicator.force_constant);
  EXPECT_DOUBLE_EQ(force_applicator.delta, expected_force_applicator.delta);
  EXPECT_DOUBLE_EQ(force_applicator.center.size(),
                   expected_force_applicator.center.size());
  for (std::size_t i = 0; i < force_applicator.center.size(); i++) {
    EXPECT_DOUBLE_EQ(force_applicator.center[i],
                     expected_force_applicator.center[i]);
  }
}

void expect_boundary_condition_equal(
    const netcdf_io::BoundaryConditionDef<3> &a,
    const netcdf_io::BoundaryConditionDef<3> &b) {
  EXPECT_EQ(a.zone_type, b.zone_type);
  EXPECT_EQ(a.coordinate, b.coordinate);
  EXPECT_DOUBLE_EQ(a.bound, b.bound);
  for (std::size_t i = 0; i < a.normal.size(); i++) {
    EXPECT_DOUBLE_EQ(a.normal[i], b.normal[i]);
  }
}

TEST(test_meshing, load_boundary_conditions_recovers_write_restart) {
  write_restart_mesh<3>("/tmp/restart-boundary.nc");
  const auto boundary_conditions =
      netcdf_io::load_boundary_conditions<3>("/tmp/restart-boundary.nc");
  EXPECT_EQ(boundary_conditions.size(), 6);
  expect_boundary_condition_equal(boundary_conditions[0],
                                  BOUNDARY_CONDITION_MIN);
  expect_boundary_condition_equal(boundary_conditions[1],
                                  BOUNDARY_CONDITION_MAX);
}

TEST(test_meshing, read_recovers_write_with_lattice_vectors) {
  write_mesh("/tmp/mesh.nc", true);
  const auto material = create_test_material<3>();
  auto mesh = meshing::load_mesh_from_nc("/tmp/mesh.nc", std::vector{material});
  check_mesh(mesh, cell_data);
  check_atomistic_domain(mesh);
  const auto expected_jacobian =
      -0.25; // = det(basis)/det(material_basis) = -2 / 8
  for (const auto &[_, cell] : mesh.cells) {
    EXPECT_DOUBLE_EQ(cell.data.jacobian, expected_jacobian);
  }
}

TEST(test_meshing, read_recovers_write_without_lattice_vectors) {
  write_mesh("/tmp/mesh-nolattice.nc", false);
  const auto material = create_test_material<3>();
  auto mesh = meshing::load_mesh_from_nc("/tmp/mesh-nolattice.nc",
                                         std::vector{material});
  check_mesh(mesh, QCCellData<3, 3>{.lattice_vectors = MATERIAL_BASIS});
  check_atomistic_domain(mesh);
  const auto expected_jacobian = 1.0;
  for (const auto &[_, cell] : mesh.cells) {
    EXPECT_DOUBLE_EQ(cell.data.jacobian, expected_jacobian);
  }
}

} // namespace
} // namespace meshing
