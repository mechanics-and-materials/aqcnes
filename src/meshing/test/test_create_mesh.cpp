// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation for create_mesh_test 
 */

#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include "qcmesh/mpi/serialization/tuple.hpp"
#include "testing/create_mesh.hpp"
#include <gmock/gmock.h>

namespace meshing {
namespace {
using qcmesh::mesh::SimplexCellId;
using qcmesh::mesh::testing::mesh_lattice_4x4x4;

std::vector<std::tuple<SimplexCellId, double>>
collect_bad_jacobians(const QCMesh<> &mesh) {
  auto bad_jacobians = std::vector<std::tuple<SimplexCellId, double>>{};
  for (const auto &[_, cell] : mesh.cells)
    if (std::isnan(cell.data.jacobian) ||
        std::abs(cell.data.jacobian) < 1.0E-10)
      bad_jacobians.emplace_back(std::make_tuple(cell.id, cell.data.jacobian));
  return bad_jacobians;
}

TEST(create_mesh_test, repair_mesh_does_not_produce_0_or_nan_jacobians) {
  auto mesh = testing::create_mesh(mesh_lattice_4x4x4(), false);
  repair_mesh(mesh);
  const auto bad_jacobians =
      qcmesh::mpi::all_concat_vec(collect_bad_jacobians(mesh));
  EXPECT_THAT(bad_jacobians, testing::IsEmpty());
}

TEST(create_mesh_test,
     double_redistribute_mesh_does_not_produce_0_or_nan_jacobians) {
  auto mesh = testing::create_mesh(mesh_lattice_4x4x4(), false);
  // fix nan lattice bases:
  for (auto &[_, cell] : mesh.cells)
    cell.data.lattice_vectors = {std::array{1., 0., 1.}, std::array{1., 1., 0.},
                                 std::array{0., 1., 1.}};
  if (boost::mpi::communicator{}.size() > 1) {
    redistribute(mesh);
    redistribute(mesh);
  }
  repair_mesh(mesh);
  const auto bad_jacobians =
      qcmesh::mpi::all_concat_vec(collect_bad_jacobians(mesh));
  EXPECT_THAT(bad_jacobians, testing::IsEmpty());
}

} // namespace
} // namespace meshing
