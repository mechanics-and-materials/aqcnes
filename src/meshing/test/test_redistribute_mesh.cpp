// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Tests for mesh redistriubution.
 */

#include "meshing/bare_mesh.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/testing/canoncially_order_mesh.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include "qcmesh/mpi/all_union_unordered_map.hpp"
#include "string_view"
#include "testing/create_mesh.hpp"
#include "testing/find_path.hpp"
#include "testing/load_mesh.hpp"
#include <gmock/gmock.h>

template <class T> struct Approx {
  const T &val;
  double tolerance = 1.0e-10;
};

template <class T>
Approx<T> approx(const T &val, const double tolerance = 1.0e-10) {
  return {val, tolerance};
}

template <class T> struct Comparable { const T &val; };

template <class T> Comparable<T> comparable(const T &val) { return {val}; }

bool operator==(const Approx<double> &a, const Approx<double> &b) {
  if (std::isnan(a.val) && std::isnan(b.val))
    return true;
  if (std::isinf(a.val) && std::isinf(b.val) && (a.val < 0.) == (b.val < 0.))
    return true;
  return std::abs(a.val - b.val) < std::min(a.tolerance, b.tolerance);
}

template <class T, std::size_t N>
bool operator==(const Approx<std::array<T, N>> &a,
                const Approx<std::array<T, N>> &b) {
  for (std::size_t i = 0; i < a.val.size(); i++)
    if (!(approx(a.val[i], a.tolerance) == approx(b.val[i], b.tolerance)))
      return false;
  return true;
}

template <class T, std::size_t N>
bool operator==(const Comparable<std::array<T, N>> &a,
                const Comparable<std::array<T, N>> &b) {
  for (std::size_t i = 0; i < a.val.size(); i++)
    if (!(comparable(a.val[i]) == comparable(b.val[i])))
      return false;
  return true;
}

template <class T>
bool operator==(const Comparable<T> &a, const Comparable<T> &b) {
  return a.val == b.val;
}

template <class T> struct Fmt {
  static std::string fmt(const T &val) { return std::to_string(val); }
};
template <class T> std::string format(const T &val) { return Fmt<T>::fmt(val); }

template <class T> struct Fmt<Approx<T>> {
  static std::string fmt(const Approx<T> &val) { return format(val.val); }
};

template <class T> struct Fmt<Comparable<T>> {
  static std::string fmt(const Comparable<T> &val) { return format(val.val); }
};

template <> struct Fmt<qcmesh::mesh::NodeId> {
  static std::string fmt(const qcmesh::mesh::NodeId &val) {
    return format(static_cast<std::size_t>(val));
  }
};
template <> struct Fmt<qcmesh::mesh::SimplexCellId> {
  static std::string fmt(const qcmesh::mesh::SimplexCellId &val) {
    return format(static_cast<std::size_t>(val));
  }
};
template <class T> struct Fmt<std::optional<T>> {
  static std::string fmt(const std::optional<T> &val) {
    if (val.has_value())
      return format(val.value());
    return "-";
  }
};

template <class T, std::size_t N> struct Fmt<std::array<T, N>> {
  static std::string fmt(const std::array<T, N> &val) {
    if (val.size() == 0)
      return "[]";
    auto out = "[" + format(val[0]);
    for (std::size_t i = 1; i < val.size(); i++)
      out += "," + format(val[i]);
    out += "]";
    return out;
  }
};

struct Difference {
  std::string_view field{};
  std::string left_repr{};
  std::string right_repr{};
};

std::ostream &operator<<(std::ostream &os, const Difference &diff) {
  return os << diff.field << ": " << diff.left_repr
            << " != " << diff.right_repr;
}

struct Diffs {
  std::string id{};
  std::vector<Difference> diffs{};

  template <class T>
  Diffs &&add(const std::string_view &field, const T a, const T b) {
    if (!(a == b))
      this->diffs.push_back({field, format(a), format(b)});
    return std::move(*this);
  }
  Diffs &&add(Diffs &&diffs) {
    for (auto diff : diffs.diffs)
      this->diffs.emplace_back(std::move(diff));
    return std::move(*this);
  }
};

std::ostream &operator<<(std::ostream &os, const Diffs &diffs) {
  os << diffs.id << ": ";
  for (const auto &diff : diffs.diffs)
    os << diff << ", ";
  return os;
}

Diffs diff(const meshing::NodalThermalDataZeroT &,
           const meshing::NodalThermalDataZeroT &) {
  return Diffs{};
}

Diffs diff(const meshing::NodalThermalDataFiniteT &a,
           const meshing::NodalThermalDataFiniteT &b) {
  return Diffs{}
      .add("entropy", approx(a.entropy), approx(b.entropy))
      .add("thermal_point", approx(a.thermal_point), approx(b.thermal_point));
}

template <std::size_t NOI>
Diffs diff(const meshing::NodalDataMultispecies<NOI> &a,
           const meshing::NodalDataMultispecies<NOI> &b) {
  return Diffs{}
      .add("impurity_concentrations", approx(a.impurity_concentrations),
           approx(b.impurity_concentrations))
      .add("impurity_concentration_update_rates",
           approx(a.impurity_concentration_update_rates),
           approx(b.impurity_concentration_update_rates))
      .add("chemical_potentials", approx(a.chemical_potentials),
           approx(b.chemical_potentials))
      .add("impurity_concentration_update_rates", approx(a.gamma_values),
           approx(b.gamma_values));
}

template <class ThermalDataType, class MultiSpeciesDataType>
Diffs diff(
    const repatoms::RepatomData<ThermalDataType, MultiSpeciesDataType> &a,
    const repatoms::RepatomData<ThermalDataType, MultiSpeciesDataType> &b) {
  return Diffs{}
      .add("lattice_site_type", a.lattice_site_type, b.lattice_site_type)
      .add("fixed", a.fixed, b.fixed)
      .add(diff(a.thermal_data, b.thermal_data))
      .add(diff(a.multi_species_data, b.multi_species_data));
}

template <class ThermalData, class MultiSpeciesDataType>
Diffs diff(
    const meshing::PackedNodalData<ThermalData, MultiSpeciesDataType> &a,
    const meshing::PackedNodalData<ThermalData, MultiSpeciesDataType> &b) {
  return Diffs{}
      .add("material_id", a.material_id, b.material_id)
      .add("weight", approx(a.weight), approx(b.weight))
      .add("periodic_boundary_flag", a.periodic_boundary_flag,
           b.periodic_boundary_flag)
      .add(diff(a.repatom_data, b.repatom_data));
}

template <std::size_t K>
Diffs diff(const meshing::PackedCellData<K> &a,
           const meshing::PackedCellData<K> &b) {
  return Diffs{}
      .add("volume", approx(a.volume), approx(b.volume))
      .add("quality", approx(a.quality), approx(b.quality))
      .add("jacobian", approx(a.jacobian), approx(b.jacobian))
      .add("density", approx(a.density), approx(b.density))
      .add("weight", approx(a.weight), approx(b.weight))
      .add("lattice_vectors", approx(a.lattice_vectors),
           approx(b.lattice_vectors))
      .add("material_index", a.material_index, b.material_index)
      .add("is_atomistic", a.is_atomistic, b.is_atomistic)
      .add("is_mono_lattice", a.is_mono_lattice, b.is_mono_lattice);
}

template <std::size_t D, class NodalDataType>
Diffs diff(const qcmesh::mesh::Node<D, NodalDataType> &a,
           const qcmesh::mesh::Node<D, NodalDataType> &b) {
  return Diffs{"(" + std::to_string(static_cast<std::size_t>(a.id)) + ")"}
      .add("id", a.id, b.id)
      .add("process_rank", a.process_rank, b.process_rank)
      .add("position", approx(a.position), approx(b.position))
      .add(diff(a.data, b.data));
}

template <std::size_t K, class CellDataType>
Diffs diff(const qcmesh::mesh::SimplexCell<K, CellDataType> &a,
           const qcmesh::mesh::SimplexCell<K, CellDataType> &b) {
  return Diffs{"<" + std::to_string(static_cast<std::size_t>(a.id)) + ">"}
      .add("id", a.id, b.id)
      // process_rank is allowed to change
      .add("neighbors", comparable(a.neighbors), comparable(b.neighbors))
      .add(diff(a.data, b.data));
}

template <class T>
std::vector<Diffs> vec_diff(const std::vector<T> &a, const std::vector<T> &b) {
  if (a.size() != b.size())
    return {Diffs{}.add("size", a.size(), b.size())};
  auto out = std::vector<Diffs>{};
  for (std::size_t i = 0; i < a.size(); i++) {
    auto d = diff(a[i], b[i]);
    if (!d.diffs.empty())
      out.emplace_back(d);
  }
  return out;
}

namespace meshing {
namespace {

template <class NodalDataType, class CellDataType, class Mesh, class PackNode,
          class PackCell>
meshing::BareMesh<3, 3, NodalDataType, CellDataType>
gather_mesh(const Mesh &mesh, const PackNode pack_node,
            const PackCell pack_cell) {
  auto cells = std::vector<decltype(pack_cell(
      qcmesh::mesh::SimplexCell<3, QCCellData<3, 3>>{}))>{};
  cells.reserve(mesh.cells.size());
  for (const auto &[_, cell] : mesh.cells)
    cells.emplace_back(pack_cell(cell));
  using PackedNodeType =
      decltype(pack_node(qcmesh::mesh::Node<3, QCNodalData>{}));
  auto nodes = std::unordered_map<qcmesh::mesh::NodeId, PackedNodeType>{};
  for (const auto &[node_id, node] : mesh.nodes)
    nodes.emplace(node_id, pack_node(node));
  auto global_cells = qcmesh::mpi::all_concat_vec(cells);
  const auto global_nodes_map = qcmesh::mpi::all_union_unordered_map(nodes);
  auto global_nodes = std::vector<PackedNodeType>{};
  global_nodes.reserve(global_nodes_map.size());
  for (const auto &[_, node] : global_nodes_map)
    global_nodes.emplace_back(node);
  const auto &[ordered_nodes, ordered_cells] =
      qcmesh::mesh::testing::canonically_order_mesh(global_nodes, global_cells);
  return {ordered_nodes, ordered_cells};
}

TEST(test_redistribute_mesh, redistribute_mesh_does_not_change_basic_mesh) {
  using qcmesh::mesh::testing::mesh_lattice_4x4x4;
  auto mesh = testing::create_mesh(mesh_lattice_4x4x4(), false);
  using PackedNodalDataType = PackedNodalData<
      traits::NodalThermalDataType<typename QCMesh<>::TemperatureMode>,
      NodalDataMultispecies<QCMesh<>::NOI>>;
  const auto pack_node = [&mesh](const auto &node) {
    return meshing::pack_node(mesh, node);
  };
  const auto pack_cell = [&mesh](const auto &cell) {
    return meshing::pack_cell(mesh, cell);
  };
  const auto mesh_dump_before =
      gather_mesh<PackedNodalDataType, PackedCellData<3>>(mesh, pack_node,
                                                          pack_cell);
  redistribute(mesh);
  const auto mesh_dump_after =
      gather_mesh<PackedNodalDataType, PackedCellData<3>>(mesh, pack_node,
                                                          pack_cell);
  EXPECT_THAT(vec_diff(mesh_dump_before.nodes, mesh_dump_after.nodes),
              ::testing::IsEmpty());
  EXPECT_THAT(vec_diff(mesh_dump_before.cells, mesh_dump_after.cells),
              ::testing::IsEmpty());
}

TEST(test_redistribute_mesh, redistribute_mesh_does_not_change_mesh) {
  const auto periodicity =
      std::array<std::optional<meshing::PeriodicityParameters>, 3>{
          meshing::PeriodicityParameters{.min_coordinate = -11.43163374,
                                         .length = 22.86326748,
                                         .relaxation_constraint = false},
          meshing::PeriodicityParameters{.min_coordinate = -11.43163374,
                                         .length = 22.86326748,
                                         .relaxation_constraint = false},
          std::nullopt};
  const auto materials = std::vector{materials::MaterialBase<3>{
      std::array<std::array<double, 3>, 3>{{
          {1.0, 0.0, 0.0},
          {0.0, 1.0, 0.0},
          {0.0, 0.0, 1.0},
      }},                          // basis_vectors
      std::array{1.0, 1.0, 1.0},   // lattice_parameter_vec
      {},                          // offset_coeffs
      std::vector<double>{63.546}, // atomic_masses
      4.32,                        // neighbor_cutoff_radius
  }};
  auto mesh = std::get<1>(meshing::load_restart_mesh_from_nc(
      testing::io::find_path("src/test/data/restart/restart_test_file.nc"),
      materials, periodicity));
  using PackedNodalDataType = PackedNodalData<
      traits::NodalThermalDataType<typename QCMesh<>::TemperatureMode>,
      NodalDataMultispecies<QCMesh<>::NOI>>;
  const auto pack_node = [&mesh](const auto &node) {
    return meshing::pack_node(mesh, node);
  };
  const auto pack_cell = [&mesh](const auto &cell) {
    return meshing::pack_cell(mesh, cell);
  };
  const auto mesh_dump_before =
      gather_mesh<PackedNodalDataType, PackedCellData<3>>(mesh, pack_node,
                                                          pack_cell);
  redistribute(mesh);
  const auto mesh_dump_after =
      gather_mesh<PackedNodalDataType, PackedCellData<3>>(mesh, pack_node,
                                                          pack_cell);
  EXPECT_THAT(vec_diff(mesh_dump_before.nodes, mesh_dump_after.nodes),
              ::testing::IsEmpty());
  EXPECT_THAT(vec_diff(mesh_dump_before.cells, mesh_dump_after.cells),
              ::testing::IsEmpty());
}

TEST(test_redistribute_mesh,
     redistribute_mesh_ships_nodes_contained_in_shipped_cells) {
  using qcmesh::mesh::testing::mesh_lattice_4x4x4;
  auto mesh = testing::create_mesh(mesh_lattice_4x4x4(), false);
  redistribute(mesh);
  redistribute(mesh);
  auto referenced_node_ids = std::set<qcmesh::mesh::NodeId>{};
  for (const auto &[_, cell] : mesh.cells)
    for (const auto node_id : cell.nodes)
      referenced_node_ids.emplace(node_id);
  auto local_node_ids = std::set<qcmesh::mesh::NodeId>{};
  for (const auto &[node_id, _] : mesh.nodes)
    local_node_ids.emplace(node_id);
  EXPECT_THAT(referenced_node_ids, ::testing::IsSubsetOf(local_node_ids));
}

std::size_t total_number_of_nodes(const meshing::QCMesh<3, 3> &mesh) {
  auto node_ids = std::set<qcmesh::mesh::NodeId>{};
  for (const auto &[node_id, _] : mesh.nodes)
    node_ids.insert(node_id);
  const auto all_node_ids = qcmesh::mpi::all_union_set(node_ids);
  return all_node_ids.size();
}

struct TestRedistributeMesh
    : ::testing::TestWithParam<std::tuple<std::string_view, std::string_view>> {
};

TEST_P(TestRedistributeMesh, redistribute_mesh_distributes_nodes_reasonably) {
  // Reasonable: Number of local vertices n should not exceed 1.5 N/m,
  // where N is the total number of vertices and m the number of processes.
  const auto [mesh_file, lattice_file] = GetParam();
  auto mesh = testing::load_mesh(testing::io::find_path(mesh_file),
                                 testing::io::find_path(lattice_file));
  const auto n_nodes_total = total_number_of_nodes(mesh);

  redistribute(mesh);
  redistribute(mesh);

  const auto n_processes = boost::mpi::communicator{}.size();
  EXPECT_LT(mesh.nodes.size(), n_nodes_total / n_processes * 3 / 2);
}

INSTANTIATE_TEST_CASE_P(
    distribute_cases, TestRedistributeMesh,
    testing::Values(
        std::make_tuple("src/test/data/grain_boundary/sigma5_000823.nc",
                        "src/test/data/grain_boundary/sigma5.txt"),
        std::make_tuple("src/test/data/coarse_mesh/NanoIndentation_009551.nc",
                        "src/test/data/coarse_mesh/MyLattice.txt")));

} // namespace
} // namespace meshing
