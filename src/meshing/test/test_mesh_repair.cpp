// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Tests for mesh repairing.
 */

#include "geometry/point.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/repatomcontainer.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/mesh_repair/testing/boundary.hpp"
#include "qcmesh/mesh/mesh_repair/testing/cell_id_collisions.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/intersection_detection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/face_connection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/simplex_lattice_jacobian.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/serialization/empty_data.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "qcmesh/mpi/all_union_unordered_map.hpp"
#include "qcmesh/mpi/serialization/tuple.hpp"
#include "testing/create_mesh.hpp"
#include "testing/find_path.hpp"
#include "testing/load_mesh.hpp"
#include <algorithm>
#include <boost/mpi.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <gmock/gmock.h>
#include <string>
#include <unordered_map>

using testing::IsEmpty;

namespace meshing {
namespace {

using qcmesh::mesh::testing::mesh_1;
using qcmesh::mesh::testing::mesh_1b;
using qcmesh::mesh::testing::mesh_2;
using qcmesh::mesh::testing::mesh_lattice;
using qcmesh::mesh::testing::mesh_lattice_4x4x4;
using qcmesh::mesh::testing::mesh_lattice_4x4x4_boundary;

/**
 * @brief Test suite loading a mesh file specified via env var
 */
struct MeshRepairTest : ::testing::TestWithParam<QCMesh<>> {};

/**
 * @brief Returns the number of cells with quality residual above tolerance.
 */
std::size_t num_bad_cells(const QCMesh<> &mesh) {
  auto n_bad_cells = (std::size_t){0};
  for (const auto &[_, cell] : mesh.cells)
    if (cell.data.quality >= meshing::REMESH_TOLERANCE)
      n_bad_cells++;
  return n_bad_cells;
}

using Tetrahedron = std::array<qcmesh::mesh::NodeId, 4>;
using Face = std::array<qcmesh::mesh::NodeId, 3>;

TEST_P(MeshRepairTest, mesh_repair_does_not_change_boundary) {
  auto mesh = GetParam();
  const auto boundary_vertices_before =
      qcmesh::mesh::testing::boundary_vertices(mesh.cells);
  meshing::repair_mesh(mesh);
  const auto boundary_vertices_after =
      qcmesh::mesh::testing::boundary_vertices(mesh.cells);
  EXPECT_THAT(boundary_vertices_before,
              testing::ContainerEq(boundary_vertices_after));
}

TEST_P(MeshRepairTest, mesh_repair_removes_all_bad_cells) {
  auto mesh = GetParam();
  const auto n_bad_cells_before =
      qcmesh::mpi::all_reduce_sum(num_bad_cells(mesh));
  EXPECT_GT(n_bad_cells_before, (std::size_t){0});
  meshing::repair_mesh(mesh);
  const auto n_bad_cells = qcmesh::mpi::all_reduce_sum(num_bad_cells(mesh));
  EXPECT_EQ(n_bad_cells, (std::size_t){0});
}

TEST_P(MeshRepairTest, mesh_repair_does_not_create_overlappings) {
  auto mesh = GetParam();
  meshing::repair_mesh(mesh);
  const auto intersections = qcmesh::mesh::testing::cell_overlaps(mesh);
  EXPECT_THAT(intersections, IsEmpty());
}

TEST_P(MeshRepairTest, test_gather_bad_cells) {
  const auto mesh = GetParam();
  auto bad_cells =
      qcmesh::mesh::gather_bad_elements(mesh, meshing::REMESH_TOLERANCE);
  auto bad_cells_ids = std::unordered_set<qcmesh::mesh::SimplexCellId>{};
  for (const auto &[cell_rank, cell_id] : bad_cells) {
    bad_cells_ids.insert(cell_id); // Already prepare for next test
    if (mesh.cells.find(cell_id) == mesh.cells.end())
      continue;
    EXPECT_GE(qcmesh::geometry::simplex_metric_residual(
                  qcmesh::mesh::simplex_cell_points(mesh, cell_id)),
              meshing::REMESH_TOLERANCE);
  }
  // All other simplices should be below tolerance:
  for (const auto &[cell_id, cell] : mesh.cells) {
    if (mesh.cells.find(cell_id) == mesh.cells.end() ||
        bad_cells_ids.find(cell_id) != bad_cells_ids.end())
      continue;
    EXPECT_LT(qcmesh::geometry::simplex_metric_residual(
                  qcmesh::mesh::simplex_cell_points(mesh, cell)),
              meshing::REMESH_TOLERANCE);
  }
}

std::unordered_map<qcmesh::mesh::NodeId, geometry::Point<3>>
coordinates(const QCMesh<> &mesh) {
  auto out = std::unordered_map<qcmesh::mesh::NodeId, geometry::Point<3>>{};
  for (const auto &[node_id, node] : mesh.nodes)
    out.emplace(node_id, node.position);
  return out;
}
TEST_P(MeshRepairTest, mesh_repair_does_not_change_vertices) {
  auto mesh = GetParam();
  const auto vertices_before =
      qcmesh::mpi::all_union_unordered_map(coordinates(mesh));
  meshing::repair_mesh(mesh);
  const auto vertices_after =
      qcmesh::mpi::all_union_unordered_map(coordinates(mesh));
  EXPECT_EQ(vertices_before, vertices_after);
}

TEST_P(MeshRepairTest, mesh_repair_does_not_break_connectivity) {
  auto mesh = GetParam();
  ASSERT_THAT(
      qcmesh::mesh::testing::collect_intrinsic_face_connections(mesh.cells),
      testing::ContainerEq(
          qcmesh::mesh::testing::collect_extrinsic_face_connections(
              mesh.cells)));
  meshing::repair_mesh(mesh);
  ASSERT_THAT(
      qcmesh::mesh::testing::collect_intrinsic_face_connections(mesh.cells),
      testing::ContainerEq(
          qcmesh::mesh::testing::collect_extrinsic_face_connections(
              mesh.cells)));
}

TEST_P(MeshRepairTest, test_qc_mesh_repair_does_not_create_cell_id_collisions) {
  auto mesh = GetParam();
  repair_mesh(mesh);

  const auto collisions = qcmesh::mesh::testing::cell_id_collisions(mesh.cells);
  ASSERT_THAT(collisions, testing::IsEmpty());
}

INSTANTIATE_TEST_CASE_P(
    QcMesh, MeshRepairTest,
    testing::Values(
        testing::create_mesh(mesh_1()), testing::create_mesh(mesh_2()),
        testing::create_mesh(mesh_1b()),
        testing::create_mesh(mesh_lattice(), false),
        testing::create_mesh(mesh_lattice_4x4x4(), false),
        testing::create_mesh(mesh_lattice_4x4x4_boundary(), false),
        testing::load_mesh(
            testing::io::find_path("src/test/data/3DBox_007813.nc"),
            testing::io::find_path("src/test/data/relaxation/MyLattice.txt")),
        testing::load_mesh(
            testing::io::find_path(
                "src/test/data/coarse_mesh/NanoIndentation_009551.nc"),
            testing::io::find_path(
                "src/test/data/coarse_mesh/MyLattice.txt"))));

} // namespace
} // namespace meshing
