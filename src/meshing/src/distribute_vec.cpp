// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief PetSc-based vector distribution for ghosts
 * @authors P. Gupta
 */

#include "meshing/distribute_vec.hpp"
#include <petscvec.h>

namespace meshing {
namespace {
namespace implementation {
template <class T>
void distribute_vec(const std::size_t n_ghosts, const std::size_t n_locals,
                    const std::size_t n_repatoms,
                    const std::vector<int> &ghosts, std::vector<T> &vec) {

  // NOLINTBEGIN(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  Vec distributed_nodal_data_petsc = nullptr;
  Vec local_nodal_data_petsc = nullptr;
  PetscScalar *local_nodal_scalars = nullptr;
  const PetscScalar *local_nodal_scalars_read = nullptr;

  VecCreateGhost(MPI_COMM_WORLD, static_cast<PetscInt>(n_locals),
                 static_cast<PetscInt>(n_repatoms),
                 static_cast<PetscInt>(n_ghosts), ghosts.data(),
                 &distributed_nodal_data_petsc);

  // get local representation
  VecGhostGetLocalForm(distributed_nodal_data_petsc, &local_nodal_data_petsc);
  VecGetArray(local_nodal_data_petsc, &local_nodal_scalars);
  // copy from weights vector to PETSC C style array
  for (std::size_t idx = 0; idx < vec.size(); idx++)
    local_nodal_scalars[idx] = vec[idx];

  // copy from C style array to PETSC local representation
  VecRestoreArray(local_nodal_data_petsc, &local_nodal_scalars);
  VecGhostRestoreLocalForm(distributed_nodal_data_petsc,
                           &local_nodal_data_petsc);
  VecGhostUpdateBegin(distributed_nodal_data_petsc, ADD_VALUES,
                      SCATTER_REVERSE);
  VecGhostUpdateEnd(distributed_nodal_data_petsc, ADD_VALUES, SCATTER_REVERSE);
  VecGhostUpdateBegin(distributed_nodal_data_petsc, INSERT_VALUES,
                      SCATTER_FORWARD);
  VecGhostUpdateEnd(distributed_nodal_data_petsc, INSERT_VALUES,
                    SCATTER_FORWARD);
  VecGhostGetLocalForm(distributed_nodal_data_petsc, &local_nodal_data_petsc);
  VecGetArrayRead(local_nodal_data_petsc, &local_nodal_scalars_read);
  // assign to weights vector from PETSC local representation
  for (std::size_t idx = 0; idx < vec.size(); idx++)
    vec[idx] = local_nodal_scalars_read[idx];
  VecRestoreArrayRead(local_nodal_data_petsc, &local_nodal_scalars_read);
  VecGhostRestoreLocalForm(distributed_nodal_data_petsc,
                           &local_nodal_data_petsc);
  VecDestroy(&distributed_nodal_data_petsc);
  // NOLINTEND(cppcoreguidelines-pro-bounds-pointer-arithmetic)
}
} // namespace implementation
} // namespace

void distribute_vec(const std::size_t n_ghosts, const std::size_t n_locals,
                    const std::size_t n_repatoms,
                    const std::vector<int> &ghosts, std::vector<int> &vec) {
  implementation::distribute_vec(n_ghosts, n_locals, n_repatoms, ghosts, vec);
}
void distribute_vec(const std::size_t n_ghosts, const std::size_t n_locals,
                    const std::size_t n_repatoms,
                    const std::vector<int> &ghosts, std::vector<double> &vec) {
  implementation::distribute_vec(n_ghosts, n_locals, n_repatoms, ghosts, vec);
}

} // namespace meshing
