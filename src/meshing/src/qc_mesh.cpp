// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Class and function implementations for QCMesh and InterProcMeshData
 * @authors P. Gupta, S. Saxena, M. Spinola
 */

#include "meshing/qc_mesh.hpp"

namespace meshing {

template class QCMesh<3, 3, 2, NodalDataMultispecies<1>>;
template class QCMesh<3, 3, 2, NodalDataMultispecies<2>>;
template class QCMesh<3, 3, 2, NodalDataMultispecies<3>>;
template class QCMesh<3, 3, 2, NodalDataMultispecies<4>>;
template class QCMesh<3, 3, 2, NodalDataMultispecies<5>>;

} // namespace meshing
