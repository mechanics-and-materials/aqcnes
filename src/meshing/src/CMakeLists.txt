# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

include(FindPkgConfig)
find_package(PTScotchParMETIS MODULE REQUIRED)
find_package(AQCNES_qcmesh REQUIRED)
pkg_check_modules(PETSc PETSc IMPORTED_TARGET REQUIRED)

add_library(meshing
  array_from_vec.cpp
  bare_mesh.cpp
  build_interproc_connectivity.cpp
  build_periodic_connectivity.cpp
  build_repatoms.cpp
  concentration_to_gamma_values.cpp
  create_mesh.cpp
  distribute_vec.cpp
  lattice_site.cpp
  mesh_repair/exchange_halos.cpp
  mesh_repair/repair_mesh.cpp
  mesh_repair/serialization/vertex_draft.cpp
  mesh_repair/vertex_draft.cpp
  mesh_topology.cpp
  nodal_data_interproc.cpp
  nodal_data_multispecies.cpp
  nodal_thermal_data.cpp
  offset_vectors.cpp
  partition_ghost_indices.cpp
  periodicity_parameters.cpp
  qc_cell_data.cpp
  qc_mesh.cpp
  qc_nodal_data.cpp
  redistribute_mesh.cpp
  renumber_mesh.cpp
  repatomcontainer.cpp
  repatom_data.cpp
  samplingatoms.cpp
  serialization/nodal_data_multispecies.cpp
  serialization/nodal_thermal_data.cpp
  serialization/qc_cell_data.cpp
  serialization/qc_nodal_data.cpp
  temperature_mode.cpp
  traits.cpp
)

target_include_directories(meshing PUBLIC include)
target_link_libraries(meshing
  PUBLIC geometry
  PUBLIC constants
  PUBLIC qcmesh::array_ops
  PUBLIC qcmesh::geometry
  PUBLIC qcmesh::mesh
  PUBLIC qcmesh::mpi
  PUBLIC lattice
  PUBLIC materials
  PUBLIC app
  PUBLIC input
  PUBLIC utils
  PUBLIC exception
  PRIVATE aqcnes::external::ptscotch-parmetis
  PRIVATE PkgConfig::PETSc
  PUBLIC qc_compile_defines
)

add_library(meshing_nc
  load_mesh_from_nc.cpp
  write_mesh_to_nc.cpp
)
target_include_directories(meshing_nc PUBLIC include)
target_link_libraries(meshing_nc
  PUBLIC meshing
  PUBLIC netcdf_io
)
