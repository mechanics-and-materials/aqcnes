// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Traits for the QCMesh class
 * @author G. Bräunlich
 */

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <array>

namespace meshing::traits {

/**
 * @brief Abstraction trait to access the node ids belonging to a cell.
 *
 * An implementation must provide:
 *
 * - `constexpr static std::size_t DIMENSION`,
 * - `static const std::array<NodeId, DIMENSION + 1> & cell_nodes(const T &)`.
 *
 * Example implementors: std::array<NodeId, K>, @ref qcmesh::mesh::SimplexCell.
 */
template <class T, class Enable = void> struct CellNodes;
/**
 * @brief Access the node ids belonging to a cell via @ref CellNodes.
 */
template <class T>
std::array<qcmesh::mesh::NodeId, CellNodes<T>::DIMENSION + 1>
cell_nodes(const T &cell) {
  return CellNodes<T>::cell_nodes(cell);
}

/**
 * @brief Implementation of @ref CellNodes for std::array<qcmesh::mesh::NodeId, N>.
 */
template <std::size_t N> struct CellNodes<std::array<qcmesh::mesh::NodeId, N>> {
  constexpr static std::size_t DIMENSION = N - 1;
  static const std::array<qcmesh::mesh::NodeId, N> &
  cell_nodes(const std::array<qcmesh::mesh::NodeId, N> &cell) {
    return cell;
  }
};
/**
 * @brief Implementation of @ref CellNodes for @ref qcmesh::mesh::SimplexCell.
 */
template <std::size_t K, class CellDataType>
struct CellNodes<qcmesh::mesh::SimplexCell<K, CellDataType>> {
  constexpr static std::size_t DIMENSION = K;
  static const std::array<qcmesh::mesh::NodeId, K + 1> &
  cell_nodes(const qcmesh::mesh::SimplexCell<K, CellDataType> &cell) {
    return cell.nodes;
  }
};

/**
 * @brief Used to access the `weight` property of a node or cell.
 *
 * An implementation must provide:
 * - `static double weight(const T &)`
 */
template <class T, class Enable = void> struct Weight;
/**
 * @brief Access the `weight` property of a cell or node via @ref Weight.
 */
template <class T> double weight(const T &cell) {
  return Weight<T>::weight(cell);
}

/**
 * @brief Implementation of @ref Weight for @ref qcmesh::mesh::SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct Weight<qcmesh::mesh::SimplexCell<K, DataType>,
              std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                  meshing::traits::Weight<DataType>>>> {
  static double weight(const qcmesh::mesh::SimplexCell<K, DataType> &cell) {
    return Weight<DataType>::weight(cell.data);
  }
};

/**
 * @brief Implementation of @ref Weight for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct Weight<qcmesh::mesh::Node<D, DataType>,
              std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                  meshing::traits::Weight<DataType>>>> {
  static double weight(const qcmesh::mesh::Node<D, DataType> &node) {
    return Weight<DataType>::weight(node.data);
  }
};

/**
 * @brief Helper trait to automatically deduce NOI for the given type (mesh or nodal data).
 */
template <class T> struct NImpurities {
  constexpr static std::size_t VALUE = 0;
};

template <std::size_t D, class DataType>
struct NImpurities<qcmesh::mesh::Node<D, DataType>> {
  constexpr static std::size_t VALUE = NImpurities<DataType>::VALUE;
};

template <class Mesh>
inline constexpr std::size_t N_IMPURITIES = NImpurities<Mesh>::VALUE;

/**
 * @brief Used to access impurity concentrations of a node.
 *
 * An implementation must provide:
 * - `static const std::array<double, N_IMPURITIES<T>> &impurity_concentrations(const T &)`,
 * - `static std::array<double, N_IMPURITIES<T>> &impurity_concentrations(T &)`.
 */
template <class T, class Enable = void> struct ImpurityConcentrations;
/**
 * @brief Access the impurity concentrations of a node via @ref ImpurityConcentrations.
 */
template <class T>
std::array<double, N_IMPURITIES<T>> impurity_concentrations(const T &node) {
  return ImpurityConcentrations<T>::impurity_concentrations(node);
}
/**
 * @brief Access the impurity concentrations of a node via @ref ImpurityConcentrations.
 */
template <class T>
std::array<double, N_IMPURITIES<T>> &impurity_concentrations(T &node) {
  return ImpurityConcentrations<T>::impurity_concentrations(node);
}

/**
 * @brief Implementation of @ref ImpurityConcentrations for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct ImpurityConcentrations<
    qcmesh::mesh::Node<D, DataType>,
    std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
        meshing::traits::ImpurityConcentrations<DataType>>>> {
  static const std::array<double, N_IMPURITIES<DataType>> &
  impurity_concentrations(const qcmesh::mesh::Node<D, DataType> &node) {
    return ImpurityConcentrations<DataType>::impurity_concentrations(node.data);
  }
  static std::array<double, N_IMPURITIES<DataType>> &
  impurity_concentrations(qcmesh::mesh::Node<D, DataType> &node) {
    return ImpurityConcentrations<DataType>::impurity_concentrations(node.data);
  }
};

/**
 * @brief Used to access the density of a node or cell.
 *
 * An implementation must provide:
 * - `static double density(const T &)`.
 */
template <class T, class Enable = void> struct Density;
/**
 * @brief Access the density of a cell or node via @ref Density.
 */
template <class T> double density(const T &cell) {
  return Density<T>::density(cell);
}

/**
 * @brief Implementation of @ref Density for @ref qcmesh::mesh::SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct Density<qcmesh::mesh::SimplexCell<K, DataType>,
               std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                   meshing::traits::Density<DataType>>>> {
  static double density(const qcmesh::mesh::SimplexCell<K, DataType> &cell) {
    return Density<DataType>::density(cell.data);
  }
};

/**
 * @brief Implementation of @ref Density for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct Density<qcmesh::mesh::Node<D, DataType>,
               std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                   meshing::traits::Density<DataType>>>> {
  static double density(const qcmesh::mesh::Node<D, DataType> &node) {
    return Density<DataType>::density(node.data);
  }
};

/**
 * @brief Used to access the id of a node or cell.
 *
 * An implementation must provide:
 * - `using IdType = ...`,
 * - `static IdType id(const T &)`.
 */
template <class T, class Enable = void> struct Id;
/**
 * @brief Access the id of a cell or node via @ref Id.
 */
template <class T> typename Id<T>::IdType id(const T &obj) {
  return Id<T>::id(obj);
}

template <std::size_t D, class NodalDataType>
struct Id<qcmesh::mesh::Node<D, NodalDataType>> {
  using IdType = qcmesh::mesh::NodeId;
  static IdType id(const qcmesh::mesh::Node<D, NodalDataType> &node) {
    return node.id;
  }
};
template <std::size_t K, class CellDataType>
struct Id<qcmesh::mesh::SimplexCell<K, CellDataType>> {
  using IdType = qcmesh::mesh::SimplexCellId;
  static IdType id(const qcmesh::mesh::SimplexCell<K, CellDataType> &cell) {
    return cell.id;
  }
};

/**
 * @brief Used to access the lattice site type of nodes.
 *
 * When using @ref create_simplex_mesh to build the nodes of the mesh the `type`
 * field of the resulting node will be populated using the trait.
 * By default, 0 will be used for the `type` field.
 * An implementation must provide
 *  `static int node_type(const T&)`.
 */
template <class T, class Enable = void> struct NodeType {
  static int node_type(const T &) { return int{}; }
};
/**
 * @brief Access the node type of a node via @ref NodeType.
 */
template <class T> int node_type(const T &cell) {
  return NodeType<T>::node_type(cell);
}

/**
 * @brief Implementation of @ref NodeType for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct NodeType<qcmesh::mesh::Node<D, DataType>,
                std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                    meshing::traits::NodeType<DataType>>>> {
  static int node_type(const qcmesh::mesh::Node<D, DataType> &node) {
    return NodeType<DataType>::node_type(node.data);
  }
};

/**
 * @brief Used to access the property of a node whether it is at the periodic boundary.
 */
template <class T, class Enable = void> struct PeriodicBoundaryFlag {
  static bool periodic_boundary_flag(const T &) { return false; }
};
/**
 * @brief Access the property of a node whether it is at the periodic boundary via @ref PeriodicBoundaryFlag.
 */
template <class T> bool periodic_boundary_flag(const T &cell) {
  return PeriodicBoundaryFlag<T>::periodic_boundary_flag(cell);
}
/**
 * @brief Implementation of @ref PeriodicBoundaryFlag for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct PeriodicBoundaryFlag<
    qcmesh::mesh::Node<D, DataType>,
    std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
        meshing::traits::PeriodicBoundaryFlag<DataType>>>> {
  static bool
  periodic_boundary_flag(const qcmesh::mesh::Node<D, DataType> &node) {
    return PeriodicBoundaryFlag<DataType>::periodic_boundary_flag(node.data);
  }
};

/**
 * @brief Used to access the material id of a node or cell.
 */
template <class T, class Enable = void> struct MaterialId {
  static std::size_t material_id(const T &) { return 0; }
};
/**
 * @brief Access the material id of a cell or node via @ref MaterialId.
 */
template <class T> std::size_t material_id(const T &cell) {
  return MaterialId<T>::material_id(cell);
}

/**
 * @brief Implementation of @ref MaterialId for @ref qcmesh::mesh::SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct MaterialId<qcmesh::mesh::SimplexCell<K, DataType>,
                  std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                      meshing::traits::MaterialId<DataType>>>> {
  static std::size_t
  material_id(const qcmesh::mesh::SimplexCell<K, DataType> &cell) {
    return MaterialId<DataType>::material_id(cell.data);
  }
};
/**
 * @brief Implementation of @ref MaterialId for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct MaterialId<qcmesh::mesh::Node<D, DataType>,
                  std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                      meshing::traits::MaterialId<DataType>>>> {
  static std::size_t material_id(const qcmesh::mesh::Node<D, DataType> &node) {
    return MaterialId<DataType>::material_id(node.data);
  }
};

/**
 * @brief Used to access the property of a node whether it is fixed.
 */
template <class T, class Enable = void> struct IsFixed {
  static bool is_fixed(const T &) { return false; }
};
/**
 * @brief Access the property of a node whether it is fixed via @IsFixed.
 */
template <class T> bool is_fixed(const T &node) {
  return IsFixed<T>::is_fixed(node);
}

/**
 * @brief Implementation of @ref IsFixed for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct IsFixed<qcmesh::mesh::Node<D, DataType>,
               std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                   meshing::traits::IsFixed<DataType>>>> {
  static bool is_fixed(const qcmesh::mesh::Node<D, DataType> &node) {
    return IsFixed<DataType>::is_fixed(node.data);
  }
};

/**
 * @brief Used to access thermal coordinates of a node.
 *
 * An implementation must provide:
 * - `constexpr static std::size_t DIMENSION`,
 * - `static const std::array<double, DIMENSION> &thermal_coordinates(const T &)`.
 */
template <class T, class Enable = void> struct ThermalCoordinates;
/**
 * @brief Access the thermal coordinates of a node via @ref ThermalCoordinates.
 */
template <class T>
std::array<double, ThermalCoordinates<T>::DIMENSION>
thermal_coordinates(const T &node) {
  return ThermalCoordinates<T>::thermal_coordinates(node);
}

/**
 * @brief Implementation of @ref ThermalCoordinates for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct ThermalCoordinates<qcmesh::mesh::Node<D, DataType>,
                          std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                              meshing::traits::ThermalCoordinates<DataType>>>> {
  constexpr static std::size_t DIMENSION =
      ThermalCoordinates<DataType>::DIMENSION;

  static const std::array<double, DIMENSION> &
  thermal_coordinates(const qcmesh::mesh::Node<D, DataType> &node) {
    return ThermalCoordinates<DataType>::thermal_coordinates(node.data);
  }
};

/**
 * @brief Used to access the entropy of a node.
 *
 * An implementation must provide:
 * - `static double entropy(const T &)`.
 */
template <class T, class Enable = void> struct Entropy;
/**
 * @brief Access the entropy of a node via @ref Entropy.
 */
template <class T> double entropy(const T &cell) {
  return Entropy<T>::entropy(cell);
}

/**
 * @brief Implementation of @ref Entropy for @ref qcmesh::mesh::Node<D, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t D, class DataType>
struct Entropy<qcmesh::mesh::Node<D, DataType>,
               std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
                   meshing::traits::Entropy<DataType>>>> {
  static double entropy(const qcmesh::mesh::Node<D, DataType> &node) {
    return Entropy<DataType>::entropy(node.data);
  }
};

/**
 * @brief Used to partition the nodes of a mesh into ghost indices and normal indices.
 *
 * An implementation must provide:
 * - `static std::tuple<std::vector<std::size_t>, std::vector<std::size_t>>
  partition_ghost_indices(const T&)`.
 */
template <class Mesh> struct Partition;

/**
 * @brief Used to count the number of repatoms of a mesh.
 *
 * An implementation must provide:
 * - `static std::size_t count_repatoms(const T&)`.
 */
template <class Mesh> struct CountRepatoms;

} // namespace meshing::traits
