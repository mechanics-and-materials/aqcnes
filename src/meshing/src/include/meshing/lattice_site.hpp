// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct implementation for a generic neighborhood lattice site
 * @authors P. Gupta
 */

#pragma once

#include "meshing/nodal_data_interproc.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <array>
#include <utility>
#include <vector>

namespace array_ops = qcmesh::array_ops;

namespace meshing {
template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
struct LatticeSite {
  using PairIndex = std::pair<int, std::size_t>;

  std::array<double, Dimension> location;
  std::array<double, Dimension> offset;
  std::array<double, Dimension> relative_displ_neighbour_build;
  std::vector<std::pair<PairIndex, double>> repatom_dependencies;

#ifdef FINITE_TEMPERATURE
  std::array<double, NIDOF> thermal_coordinate;
#endif
  NodalData nodal_data;

  bool interproc_flag{};
  bool periodic_flag{};

  LatticeSite() {
    location.fill(0.);
    offset.fill(0.);
#ifdef FINITE_TEMPERATURE
    thermal_coordinate.fill(0.0);
#endif
    relative_displ_neighbour_build.fill(0.0);
  }
  LatticeSite(const std::array<double, Dimension> &l,
              const std::array<double, Dimension> &offset_value,
              const std::array<double, Dimension + 1> &n,
#ifdef FINITE_TEMPERATURE
              const std::array<std::array<double, NIDOF>, Dimension + 1>
                  &thermal_simplex,
#endif
              const std::array<NodalData, Dimension + 1> &nodal_data_value,
              const std::array<std::size_t, Dimension + 1> &i, const int &rank,
              const bool &flag, const bool &periodic_flag_value)
      : location(l), interproc_flag(flag), periodic_flag(periodic_flag_value) {
    // a new  neighbour has no relative displacement
    // from sampling atom. This is tested for
    // determining if neighbourhood build is required.
    relative_displ_neighbour_build.fill(0.0);

    offset.fill(0.0);

    PairIndex p;
    repatom_dependencies.reserve(Dimension + 1);

    if (periodic_flag)
      offset = offset_value;

#ifdef FINITE_TEMPERATURE
    thermal_coordinate.fill(0.0);
#endif

    for (std::size_t d = 0; d < Dimension + 1; d++) {
      p = std::make_pair(rank, i[d]);

      if (abs(n[d]) > 1.0e-4) {
        repatom_dependencies.emplace_back(p, n[d]);

#ifdef FINITE_TEMPERATURE
        array_ops::as_eigen(thermal_coordinate) +=
            array_ops::as_eigen(thermal_simplex[d]) * n[d];
#endif
        meshing::InterpolateNodalData<NodalData>::interpolate(
            nodal_data_value[d], nodal_data, n[d]);
      }
    }
  }

  double operator[](const int &d) { return location[d]; }
};

} // namespace meshing
