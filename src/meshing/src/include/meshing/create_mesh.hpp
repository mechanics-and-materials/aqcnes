// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Create a mesh based on a @ref BareMesh instance.
 * @authors G. Bräunlich, P. Gupta
 */

#pragma once

#include "geometry/box.hpp"
#include "materials/create_material_base.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "meshing/periodicity_parameters.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/repatomcontainer.hpp"
#include "meshing/samplingatoms.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/mesh/connect_global_cell_faces.hpp"
#include "qcmesh/mesh/orient_elements.hpp"
#include "qcmesh/mesh/traits/node.hpp"

namespace meshing {

template <std::size_t K, class CellData>
std::vector<qcmesh::mesh::SimplexCellId> build_simplex_index_key_maps(
    const std::unordered_map<qcmesh::mesh::SimplexCellId,
                             qcmesh::mesh::SimplexCell<K, CellData>> &cells) {
  auto simplex_index_to_key = std::vector<qcmesh::mesh::SimplexCellId>{};
  simplex_index_to_key.reserve(cells.size());

  for (const auto &[cell_id, _] : cells)
    simplex_index_to_key.push_back(cell_id);

  // erasing is easier if this is sorted
  std::sort(simplex_index_to_key.begin(), simplex_index_to_key.end());
  return simplex_index_to_key;
}

template <std::size_t D, std::size_t K>
void cell_assign_material(qcmesh::mesh::SimplexCell<K, QCCellData<D, K>> &cell,
                          const std::unordered_map<qcmesh::mesh::NodeId, int>
                              &material_index_by_node) {
  auto material_indices = std::array<int, K + 1>{};
  for (std::size_t d = 0; d < K + 1; d++)
    material_indices[d] = material_index_by_node.at(cell.nodes[d]);
  const auto is_mono_lattice =
      std::count(material_indices.begin(), material_indices.end(),
                 material_indices[0]) == K + 1;
  if (!is_mono_lattice)
    std::cout << static_cast<std::size_t>(cell.id)
              << ", detected a multi-lattice element. Just warning .... "
              << "rank : " << boost::mpi::communicator{}.rank() << std::endl;
  // all such multilattice elements should be atomistic
  else
    cell.data.material_index = material_indices[0];
  cell.data.is_mono_lattice = is_mono_lattice;
}

template <class NodeType, class CellType>
std::array<
    std::array<double, qcmesh::mesh::traits::Position<NodeType>::DIMENSION>,
    traits::CellNodes<CellType>::DIMENSION + 1>
simplex_cell_points(
    const CellType &cell,
    const std::unordered_map<qcmesh::mesh::NodeId, NodeType> &nodes) {
  constexpr std::size_t D = qcmesh::mesh::traits::Position<NodeType>::DIMENSION;
  constexpr std::size_t K = traits::CellNodes<CellType>::DIMENSION;
  auto out = std::array<std::array<double, D>, K + 1>{};
  for (std::size_t i = 0; i < cell.nodes.size(); i++)
    out[i] = qcmesh::mesh::traits::position(nodes.at(cell.nodes[i]));
  return out;
};

template <class NodeType, class CellType>
qcmesh::mesh::SimplexCell<
    qcmesh::mesh::traits::Position<NodeType>::DIMENSION,
    QCCellData<qcmesh::mesh::traits::Position<NodeType>::DIMENSION,
               traits::CellNodes<CellType>::DIMENSION>>
create_cell(
    const CellType &cell, const int process_rank,
    const std::unordered_map<qcmesh::mesh::NodeId, NodeType> &nodes,
    const std::vector<
        std::array<double, qcmesh::mesh::traits::Position<NodeType>::DIMENSION>>
        &element_offset_coeffs) {
  constexpr std::size_t D = qcmesh::mesh::traits::Position<NodeType>::DIMENSION;
  constexpr std::size_t K = traits::CellNodes<CellType>::DIMENSION;
  using LatticeBasisTrait =
      typename qcmesh::mesh::traits::LatticeBasis<CellType>;
  using qcmesh::mesh::traits::IS_IMPLEMENTED;

  auto new_cell = qcmesh::mesh::SimplexCell<D, QCCellData<D, K>>{
      .id = IS_IMPLEMENTED<traits::Id<CellType>>
                ? traits::Id<CellType>::id(cell)
                : qcmesh::mesh::SimplexCellId{},
      .process_rank = process_rank,
      .nodes = traits::CellNodes<CellType>::cell_nodes(cell)};

  new_cell.data.material_index = traits::material_id(cell);
  if constexpr (IS_IMPLEMENTED<qcmesh::mesh::traits::Jacobian<CellType>>)
    new_cell.data.jacobian = qcmesh::mesh::traits::jacobian(cell);
  if constexpr (IS_IMPLEMENTED<LatticeBasisTrait>) {
    const auto &lattice_basis = LatticeBasisTrait::lattice_basis(cell);
    new_cell.data.lattice_vectors = lattice_basis;
    new_cell.data.offset_vectors = prepare_offsets(
        array_ops::as_eigen_matrix(lattice_basis), element_offset_coeffs);
  }
  if constexpr (IS_IMPLEMENTED<traits::Density<CellType>>)
    new_cell.data.density = traits::Density<CellType>::density(cell);
  if constexpr (IS_IMPLEMENTED<qcmesh::mesh::traits::IsAtomistic<CellType>>)
    new_cell.data.is_atomistic = qcmesh::mesh::traits::is_atomistic(cell);
  if constexpr (IS_IMPLEMENTED<qcmesh::mesh::traits::Quality<CellType>> &&
                IS_IMPLEMENTED<qcmesh::mesh::traits::Volume<CellType>>) {
    new_cell.data.quality = qcmesh::mesh::traits::quality(cell);
    new_cell.data.volume = qcmesh::mesh::traits::volume(cell);
  } else
    qcmesh::mesh::set_cell_data(new_cell, simplex_cell_points(new_cell, nodes));
  return new_cell;
}

template <class NodeType>
qcmesh::mesh::Node<qcmesh::mesh::traits::Position<NodeType>::DIMENSION,
                   QCNodalData>
create_node(const NodeType &raw_node, const qcmesh::mesh::NodeId id,
            const int process_rank) {
  auto node = qcmesh::mesh::build_node<QCNodalData>(raw_node, id, process_rank);
  node.data.periodic_boundary_flag =
      traits::PeriodicBoundaryFlag<NodeType>::periodic_boundary_flag(raw_node);
  node.data.type = traits::NodeType<NodeType>::node_type(raw_node);
  return node;
}

template <class NodeType>
qcmesh::mesh::Node<qcmesh::mesh::traits::Position<NodeType>::DIMENSION,
                   QCNodalData>
create_node(const NodeType &raw_node, const int process_rank) {
  return create_node(raw_node, traits::Id<NodeType>::id(raw_node),
                     process_rank);
}
/**
 * @brief Create a mesh based on a @ref BareMesh instance.
 *
 * There must be an overload of @ref materials::create_material_base which accepts `MaterialType` which is the case for @ref materials::Material and @ref materials::MaterialBase.
 * Data of the node and cell types used in the @ref BareMesh instance
 * is transferred to the created mesh via the node traits:
 * - @ref traits::Id (if not implemented, nodes will be indexed automatically)
 * - @ref qcmesh::mesh::traits::Position
 * - @ref traits::NodeType (`node.data.type`)
 * - @ref traits::PeriodicBoundaryFlag (`node.data.periodic_boundary_flag`)
 * - @ref traits::MaterialId (`node.data.material_id`)
 * - @ref traits::ImpurityConcentrations
 * - @ref traits::IsFixed
 * - @ref traits::Weight
 * - @ref traits::ThermalCoordinates
 * - @ref traits::Entropy
 * And the cell traits:
 * - @ref traits::Id (if not implemented, cells will be indexed automatically)
 * - @ref traits::CellNodes (`cell.nodes`)
 * - @ref qcmesh::mesh::traits::LatticeBasis (`cell.data.lattice_vector`)
 * - @ref qcmesh::mesh::traits::Jacobian (`cell.data.jacobian`)
 * - @ref traits::Density (`cell.data.density`)
 * - @ref qcmesh::mesh::traits::IsAtomistic (`cell.data.is_atomistic`)
 * - @ref traits::Weight
 *
 * Furthermore, to support custom nodal data, the trait
 * @ref qcmesh::mesh::traits::TransferData can be used to inject nodal data.
 * Here, the `FromType` template argument must be the node type used in @ref BareMesh::nodes and the `ToType` must be @ref QCNodalData.
 */
template <class MultispeciesDataType = QCMesh<>::NodalDataType,
          std::size_t ThermalDof = QCMesh<>::THERMAL_DOF, class NodeType,
          class CellType, class MaterialType>
QCMesh<qcmesh::mesh::traits::Position<NodeType>::DIMENSION,
       traits::CellNodes<CellType>::DIMENSION, ThermalDof, MultispeciesDataType>
create_mesh(
    const std::vector<NodeType> &raw_nodes,
    const std::vector<CellType> &raw_cells,
    const std::vector<MaterialType> &materials,
    const std::vector<
        std::array<double, qcmesh::mesh::traits::Position<NodeType>::DIMENSION>>
        &element_offset_coeffs = std::vector<std::array<
            double, qcmesh::mesh::traits::Position<NodeType>::DIMENSION>>{},
    const std::array<std::optional<PeriodicityParameters>,
                     qcmesh::mesh::traits::Position<NodeType>::DIMENSION>
        &periodicity =
            std::array<std::optional<PeriodicityParameters>,
                       qcmesh::mesh::traits::Position<NodeType>::DIMENSION>{}) {
  constexpr std::size_t D = qcmesh::mesh::traits::Position<NodeType>::DIMENSION;
  constexpr std::size_t K = traits::CellNodes<CellType>::DIMENSION;
  constexpr std::size_t NIDOF = ThermalDof;
  auto mesh = QCMesh<D, K, NIDOF, MultispeciesDataType>{};
  mesh.initialize_periodic_parameters(periodicity);
  mesh.materials.reserve(materials.size());
  for (auto const &material : materials)
    mesh.materials.emplace_back(materials::create_material_base(material));
  mesh.element_offset_coeffs = element_offset_coeffs;

  mesh.cells.reserve(raw_cells.size());
  mesh.nodes.reserve(raw_nodes.size());
  mesh.material_idx_of_nodes.reserve(raw_nodes.size());

  auto material_ids = std::unordered_map<qcmesh::mesh::NodeId, int>{};
  const auto process_rank = boost::mpi::communicator{}.rank();
  auto node_id = qcmesh::mesh::NodeId{0};
  using qcmesh::mesh::traits::IS_IMPLEMENTED;
  for (const auto &raw_node : raw_nodes) {
    if constexpr (IS_IMPLEMENTED<traits::Id<NodeType>>)
      node_id = traits::id(raw_node);
    auto node = create_node(raw_node, node_id, process_rank);

    mesh.nodes.emplace(node_id, node);
    material_ids.emplace(node_id, traits::material_id(raw_node));
    if constexpr (!IS_IMPLEMENTED<traits::Id<NodeType>>)
      node_id = qcmesh::mesh::NodeId{static_cast<std::size_t>(node_id) + 1};
  }
  using LatticeBasisTrait =
      typename qcmesh::mesh::traits::LatticeBasis<CellType>;
  for (const auto &cell : raw_cells) {
    auto new_cell =
        create_cell(cell, process_rank, mesh.nodes, element_offset_coeffs);
    if constexpr (!IS_IMPLEMENTED<traits::Id<CellType>>)
      new_cell.id = qcmesh::mesh::SimplexCellId{mesh.cell_id_counter++};
    else if (static_cast<std::size_t>(new_cell.id) >= mesh.cell_id_counter)
      mesh.cell_id_counter = static_cast<std::size_t>(new_cell.id) + 1;
    if (IS_IMPLEMENTED<LatticeBasisTrait> &&
        !IS_IMPLEMENTED<qcmesh::mesh::traits::Jacobian<CellType>>) {
      const auto &material_basis =
          mesh.materials[new_cell.data.material_index].basis_vectors;
      new_cell.data.jacobian =
          array_ops::as_eigen_matrix(new_cell.data.lattice_vectors)
              .determinant() /
          array_ops::as_eigen_matrix(material_basis).determinant();
    }
    cell_assign_material(new_cell, material_ids);
    mesh.cells.emplace(new_cell.id, new_cell);
  }
  if constexpr (!IS_IMPLEMENTED<qcmesh::mesh::traits::IsAtomistic<CellType>>)
    mesh.initialize_elements_atomistic_flag();

  mesh.simplex_index_to_key = build_simplex_index_key_maps(mesh.cells);

  for (std::size_t index = 0; index < mesh.simplex_index_to_key.size(); index++)
    mesh.cells.at(mesh.simplex_index_to_key[index]).data.mesh_index = index;
  mesh.node_index_to_key = build_node_index_key_maps(mesh.nodes);
  for (std::size_t index = 0; index < mesh.node_index_to_key.size(); index++)
    mesh.nodes.at(mesh.node_index_to_key[index]).data.mesh_index = index;
  for (const auto &node_key : mesh.node_index_to_key)
    mesh.material_idx_of_nodes.push_back(material_ids.at(node_key));

  mesh.sampling_atoms_of_simplices.resize(mesh.cells.size(),
                                          std::numeric_limits<int>::max());

  renumber_cells(mesh);
  // build basic topology (nodes and face neighbours)
  qcmesh::mesh::connect_global_cell_faces(mesh.cells);

  // renumber mesh nodes (reorder indices according to ghost indices)
  renumber_mesh_nodes(mesh);

  const auto collect_vec = [](const auto &cells, const auto &f) {
    auto vec = std::vector<decltype(f(CellType{}))>{};
    vec.reserve(cells.size());
    for (const auto &cell : cells)
      vec.emplace_back(f(cell));
    return vec;
  };
  const auto collect_map = [](const auto &nodes, const auto &f) {
    auto map =
        std::unordered_map<qcmesh::mesh::NodeId, decltype(f(NodeType{}))>{};
    map.reserve(nodes.size());
    auto node_id = qcmesh::mesh::NodeId{0};
    for (const auto &node : nodes) {
      if constexpr (IS_IMPLEMENTED<traits::Id<NodeType>>)
        node_id = traits::id(node);
      map.emplace(node_id, f(node));
      if constexpr (!IS_IMPLEMENTED<traits::Id<NodeType>>)
        node_id = qcmesh::mesh::NodeId{static_cast<std::size_t>(node_id) + 1};
    }
    return map;
  };
  auto node_site_types = collect_map(
      raw_nodes, [](const auto &node) { return traits::node_type(node); });
  if constexpr (IS_IMPLEMENTED<traits::ImpurityConcentrations<NodeType>> &&
                IS_IMPLEMENTED<
                    traits::ImpurityConcentrations<MultispeciesDataType>> &&
                (std::is_same_v<typename QCMesh<>::TemperatureMode,
                                meshing::ZeroT> ||
                 (IS_IMPLEMENTED<traits::ThermalCoordinates<NodeType>> &&
                  IS_IMPLEMENTED<traits::Entropy<NodeType>>))) {
    auto node_flags = collect_map(raw_nodes, [](const auto &node) {
      return static_cast<int>(traits::is_fixed(node));
    });
    auto node_nodal_data = collect_map(raw_nodes, [](const auto &node) {
      auto data = MultispeciesDataType{};
      traits::impurity_concentrations(data) =
          traits::impurity_concentrations(node);
      return data;
    });
    mesh.build_repatoms(
        node_flags, node_site_types
#ifdef FINITE_TEMPERATURE
        ,
        collect_map(
            raw_nodes,
            [](const auto &node) { return traits::thermal_coordinates(node); }),
        collect_map(raw_nodes,
                    [](const auto &node) { return traits::entropy(node); })
#endif
            ,
        node_nodal_data);
  }
  if constexpr (IS_IMPLEMENTED<traits::Weight<NodeType>> &&
                IS_IMPLEMENTED<traits::Weight<CellType>>) {
    auto cell_weights = collect_vec(
        raw_cells, [](const auto &cell) { return traits::weight(cell); });
    auto node_weights = collect_map(
        raw_nodes, [](const auto &node) { return traits::weight(node); });

    mesh.build_sampling_atoms(cell_weights, node_weights);
    // restart mesh with same weights
    mesh.assign_repatom_weights();
  }
  auto zero_vol_cells = qcmesh::mesh::orient_elements(mesh);
  qcmesh::mesh::orient_local_0_vol_cells_using_neighbors(mesh, zero_vol_cells);
  mesh.initialize_element_vectors();
  mesh.cell_id_counter =
      boost::mpi::all_reduce(boost::mpi::communicator{}, mesh.cell_id_counter,
                             boost::mpi::maximum<std::size_t>{});
  mesh.local_domain_bound =
      qcmesh::geometry::bounding_box(mesh.repatoms.locations);
  mesh.domain_bound = geometry::distributed_box_hull(mesh.local_domain_bound);
  return mesh;
}

} // namespace meshing
