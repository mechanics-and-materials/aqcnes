// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief class and function implementation for RepatomContainer
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "exception/qc_exception.hpp"
#include "meshing/concentration_to_gamma_values.hpp"
#include "meshing/nodal_thermal_data.hpp"
#include "meshing/repatom_data.hpp"
#include "meshing/temperature_mode.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include <boost/mpi.hpp>
#include <type_traits>

namespace array_ops = qcmesh::array_ops;

namespace repatoms {

template <std::size_t Dimension>
std::array<std::array<double, Dimension>, Dimension>
diag_displacement_gradient(const std::array<bool, Dimension> &periodic_flags) {
  auto out = std::array<std::array<double, Dimension>, Dimension>{};
  for (std::size_t i = 0; i < Dimension; i++)
    out[i][i] = static_cast<double>(periodic_flags[i]);
  return out;
}

// no exchange of repatoms. Just exchange
// thermal point and location when exchanging nodes,
// packing of container will take care of rest, which happens
// after renumbering

template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
class RepatomContainer {
public:
#ifdef FINITE_TEMPERATURE
  using TemperatureMode = meshing::FiniteT;
#else
  using TemperatureMode = meshing::ZeroT;
#endif
  // we are using repatom container instead of a vector of repatom objects
  // so converting all the calculations to a C type petsc array is simple

  std::vector<std::array<double, Dimension>> locations;
  std::vector<std::array<double, Dimension>> initial_locations;
  std::vector<std::array<double, Dimension>> forces;
  std::vector<std::array<double, Dimension>> velocities;

  std::vector<bool> if_on_boundary;
  std::vector<bool> if_fixed;

  std::vector<int> ghost_dof_indices;
  int noof_ghosts{};

#ifdef FINITE_TEMPERATURE
  using ThermalPoint = std::array<double, NIDOF>;
  std::vector<bool> if_thermally_fixed;

  std::vector<ThermalPoint> thermal_coordinates; //\Omega, \Sigma
  std::vector<double> thermal_forces;            // d\alpha/dt
  std::vector<double> thermal_velocities;        // d\Sigma/dt
  std::vector<double> entropy;
  std::vector<double> entropy_production_rates;

  std::vector<double> dissipative_potential;
#endif

  std::vector<NodalData> nodal_data_points;

  std::vector<int> lattice_site_type;
  std::vector<int> material_indices;
  std::vector<double> nodal_weights;
  std::vector<double> mass;
  std::vector<int> force_distribution_factors;

  std::array<double, Dimension> periodic_forces{};
  std::array<double, Dimension> periodic_velocities{};
  std::array<std::array<double, Dimension>, Dimension>
      box_displacement_gradient{};

  double total_mass{};
  double total_weights{};

  void assign_ghost_do_fs(const std::vector<int> &ghost_node_indices) {

    noof_ghosts = static_cast<int>(ghost_node_indices.size());

#ifdef FINITE_TEMPERATURE
    constexpr std::size_t N_VAR = Dimension + 1;
#else
    constexpr std::size_t N_VAR = Dimension;
#endif

    // !!!!! VERY IMPORTANT TO CLEAR
    ghost_dof_indices.clear();
    ghost_dof_indices.reserve(N_VAR * ghost_node_indices.size());

    for (const auto &id : ghost_node_indices) {
      for (std::size_t d = 0; d < N_VAR; d++) {
        ghost_dof_indices.push_back(N_VAR * id + d);
      }
    }
  }

  void calc_total_mass() {

    double m_local = 0.0;
    double w_local = 0.0;
    for (std::size_t repatom_idx = 0; repatom_idx < forces.size() - noof_ghosts;
         repatom_idx++) {
      w_local += nodal_weights[repatom_idx];
      m_local += mass[repatom_idx];
    }
    total_mass = qcmesh::mpi::all_reduce_sum(m_local);
    total_weights = qcmesh::mpi::all_reduce_sum(w_local);
  }

  double force_residual() {
    const auto mpi_size = boost::mpi::communicator{}.size();
    std::fill(if_on_boundary.begin(), if_on_boundary.end(), false);
    auto r = 0.0;
    // we should not add boundary force residuals
    // by default, we add all forces
    double local_r = 0.0;

#ifdef FINITE_TEMPERATURE
    double sigma{};
    double phi_force{};
#endif

    for (std::size_t repatom_idx = 0; repatom_idx < forces.size() - noof_ghosts;
         repatom_idx++) {

      if (!if_fixed[repatom_idx]) {
        for (std::size_t d = 0; d < Dimension; d++) {
          local_r += forces[repatom_idx][d] * forces[repatom_idx][d] /
                     (mass[repatom_idx] * mass[repatom_idx]);
        }
      }

#ifdef FINITE_TEMPERATURE
      if (!if_thermally_fixed[repatom_idx]) {
        sigma = exp(-thermal_coordinates[repatom_idx][1]);
        phi_force = -thermal_forces[repatom_idx] / (sigma * mass[repatom_idx]);

        local_r += phi_force * phi_force;
      }
#endif
    }

    for (std::size_t i_period = 0; i_period < Dimension; i_period++) {
      local_r += periodic_forces[i_period] * periodic_forces[i_period] /
                 (total_mass * total_mass * double(mpi_size));
    }

    if (mpi_size > 1) {
      // sum across all processors. Notice we did not add ghosts
      r = qcmesh::mpi::all_reduce_sum(local_r);
    } else {
      r = local_r;
    }

    r = sqrt(r);
    return r;
  }

#ifdef FINITE_TEMPERATURE

  void heat_rate_residual(double &r) {
    const auto mpi_size = boost::mpi::communicator{}.size();
    double local_r = 0.0;
    double temperature{};
    double q_dot{};
    r = 0.0;

    for (std::size_t repatom_idx = 0;
         repatom_idx < entropy_production_rates.size() - noof_ghosts;
         repatom_idx++) {
      temperature =
          exp(-thermal_coordinates[repatom_idx][0]) / (constants::k_Boltzmann);
      q_dot = temperature * entropy_production_rates[repatom_idx];

      local_r += q_dot * q_dot;
    }

    if (mpi_size > 1) {
      // sum across all processors. Notice we did not add ghosts
      r = qcmesh::mpi::all_reduce_sum(local_r);
    } else {
      r = local_r;
    }

    r = sqrt(r);
  }

  void reset_temperature(const double &t0) {
    for (std::size_t repatom_idx = 0; repatom_idx < thermal_coordinates.size();
         repatom_idx++) {
      thermal_coordinates[repatom_idx][0] = -log(constants::k_Boltzmann * t0);
      // thermal_coordinates_[repatom_idx][0] =
      // k_Boltzmann*_T0;
    }
  }

  void isentropic_update(const std::size_t id, const double phi,
                         const double t_min, const double t_max) {
    // if marked thermally fixed,
    // the atom doesn't change its temperature,
    // correspondingly the entropy might increase
    const auto mpi_rank = boost::mpi::communicator{}.rank();

    if (if_thermally_fixed[id]) {
      thermal_coordinates[id][1] = phi;

      entropy[id] = -1.5 * constants::k_Boltzmann *
                    (thermal_coordinates[id][0] + thermal_coordinates[id][1]);
    } else {
      thermal_coordinates[id][1] = phi;

      thermal_coordinates[id][0] =
          -1.0 * entropy[id] / (1.5 * constants::k_Boltzmann) - phi;

      const double t =
          exp(-thermal_coordinates[id][0]) / constants::k_Boltzmann;

      if ((t < t_min) || (t > t_max))
        throw exception::QCException(
            "Rank " + std::to_string(mpi_rank) +
            "detected temperature out of provided bounds = " +
            std::to_string(t));
    }
  }

  void update_equilibrium_entropy() {
    for (std::size_t repatom_idx = 0; repatom_idx < entropy.size();
         repatom_idx++) {

      entropy[repatom_idx] = -1.5 * constants::k_Boltzmann *
                             (thermal_coordinates[repatom_idx][0] +
                              thermal_coordinates[repatom_idx][1]);
    }
  }

  // to be called only after deformation of the mesh and overlaps
  void update_entropy(const double &dt) {
    // after this update, we have to make sure to deform mesh overlaps
    // and update temperature
    for (std::size_t repatom_idx = 0;
         repatom_idx < entropy_production_rates.size(); repatom_idx++) {
      entropy[repatom_idx] += entropy_production_rates[repatom_idx] * dt;

      // update the Omega value using existing value of Sigma

      if (!if_thermally_fixed[repatom_idx]) {
        thermal_coordinates[repatom_idx][0] =
            -1.0 * entropy[repatom_idx] / (1.5 * constants::k_Boltzmann) -
            thermal_coordinates[repatom_idx][1];
      } else {
        // update the Sigma value using existing value of Omega

        thermal_coordinates[repatom_idx][1] =
            -1.0 * entropy[repatom_idx] / (1.5 * constants::k_Boltzmann) -
            thermal_coordinates[repatom_idx][0];
      }
    }
  }

  void zero_entropy_rates() {
    for (auto &er : entropy_production_rates) {
      er = 0.0;
    }
  }
#endif

  void initialize_gamma_values(bool simulating_vacancy) {
    for (std::size_t repatom_idx = 0; repatom_idx < nodal_data_points.size();
         repatom_idx++)
      nodal_data_points[repatom_idx].gamma_values = meshing::get_gamma_values(
          nodal_data_points[repatom_idx].impurity_concentrations,
          simulating_vacancy);
  }

  /**
   * @brief update solute concentration values while simultaneously checking
   * if they lies within bounds (0<c<1) and the sum lies within bounds
   * (sum(c)<1) and updating the time step to keep them within bounds
   */
  void update_concentrations(double &dt, const bool &simulating_vacancy) {
    auto max_local_excess_time = 0.0;
    for (std::size_t repatom_idx = 0; repatom_idx < nodal_data_points.size();
         repatom_idx++) {
      auto new_conc_sum = 0.0;
      auto conc_rate_sum =
          nodal_data_points[repatom_idx].impurity_concentration_update_rates[0];
      const auto noi = nodal_data_points[repatom_idx]
                           .impurity_concentration_update_rates.size();
      std::string output_string;
      for (std::size_t species_idx = 1; species_idx < noi; species_idx++) {
        conc_rate_sum += nodal_data_points[repatom_idx]
                             .impurity_concentration_update_rates[species_idx];

        nodal_data_points[repatom_idx].impurity_concentrations[species_idx] +=
            nodal_data_points[repatom_idx]
                .impurity_concentration_update_rates[species_idx] *
            dt;

        if (nodal_data_points[repatom_idx]
                .impurity_concentrations[species_idx] <
            constants::concentration_minimum_bound) {
          const auto bad_delta_t =
              (nodal_data_points[repatom_idx]
                   .impurity_concentrations[species_idx] -
               constants::concentration_minimum_bound) /
              nodal_data_points[repatom_idx]
                  .impurity_concentration_update_rates[species_idx];

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;

        } else if (nodal_data_points[repatom_idx]
                       .impurity_concentrations[species_idx] >
                   constants::concentration_maximum_bound) {

          const auto bad_delta_t =
              (nodal_data_points[repatom_idx]
                   .impurity_concentrations[species_idx] -
               constants::concentration_maximum_bound) /
              nodal_data_points[repatom_idx]
                  .impurity_concentration_update_rates[species_idx];

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;
        }

        new_conc_sum +=
            nodal_data_points[repatom_idx].impurity_concentrations[species_idx];
      }

      if (simulating_vacancy) {
        nodal_data_points[repatom_idx].impurity_concentrations[0] -=
            conc_rate_sum * dt;

        if (nodal_data_points[repatom_idx].impurity_concentrations[0] <
            constants::concentration_minimum_bound) {

          const auto bad_delta_t =
              -(nodal_data_points[repatom_idx].impurity_concentrations[0] -
                constants::concentration_minimum_bound) /
              conc_rate_sum;

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;

        } else if (nodal_data_points[repatom_idx].impurity_concentrations[0] >
                   constants::concentration_maximum_bound) {

          const auto bad_delta_t =
              -(nodal_data_points[repatom_idx].impurity_concentrations[0] -
                constants::concentration_maximum_bound) /
              conc_rate_sum;

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;
        }
      } else {
        nodal_data_points[repatom_idx].impurity_concentrations[0] +=
            nodal_data_points[repatom_idx]
                .impurity_concentration_update_rates[0] *
            dt;
        if (nodal_data_points[repatom_idx].impurity_concentrations[0] <
            constants::concentration_minimum_bound) {

          const auto bad_delta_t =
              (nodal_data_points[repatom_idx].impurity_concentrations[0] -
               constants::concentration_minimum_bound) /
              nodal_data_points[repatom_idx]
                  .impurity_concentration_update_rates[0];

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;

        } else if (nodal_data_points[repatom_idx].impurity_concentrations[0] >
                   constants::concentration_maximum_bound) {

          const auto bad_delta_t =
              (nodal_data_points[repatom_idx].impurity_concentrations[0] -
               constants::concentration_maximum_bound) /
              nodal_data_points[repatom_idx]
                  .impurity_concentration_update_rates[0];

          if (bad_delta_t > max_local_excess_time)
            max_local_excess_time = bad_delta_t;
        }
      }
      new_conc_sum += nodal_data_points[repatom_idx].impurity_concentrations[0];
    } // loop over repatom indices

    // get the global maximum excess time
    auto max_global_excess_time = 0.0;
    const auto world = boost::mpi::communicator{};
    boost::mpi::all_reduce(world, max_local_excess_time, max_global_excess_time,
                           boost::mpi::maximum<double>());

    // recall the function itself with a negative (corrective) dt
    // take only 0.1*(allowable_time_step) if concs out of bounds is encountered
    const auto good_delta_t = dt - max_global_excess_time;
    auto corrective_negative_dt =
        -(max_global_excess_time + 0.9 * good_delta_t);
    if (max_global_excess_time > constants::shape_function_tolerance) {
      dt += corrective_negative_dt;
      const auto mpi_rank = boost::mpi::communicator{}.rank();
      if (mpi_rank == 0)
        std::cout << "Reducing time step to " << dt
                  << " to ensure concentrations within bounds.\n";
      update_concentrations(corrective_negative_dt, simulating_vacancy);
    }

    for (std::size_t repatom_idx = 0; repatom_idx < nodal_data_points.size();
         repatom_idx++) {
      // recalc gamma based on new conc values
      nodal_data_points[repatom_idx].gamma_values = meshing::get_gamma_values(
          nodal_data_points[repatom_idx].impurity_concentrations,
          simulating_vacancy);
    }
  }

  /**
   * @brief Update gamma values followed by a computation of new concs
   * from the updated gamma values. This ensures that 0 < c < 1
   * but might lead to non-conservation of mass because the gamma-update
   * rules are not antisymmetric for i<--->j pairs
   */
  void update_concentrations_via_gamma(double &dt,
                                       const bool &simulating_vacancy) {
    for (std::size_t repatom_idx = 0; repatom_idx < nodal_data_points.size();
         repatom_idx++) {
      // update gamma values first
      auto conc_sum = 0.0;
      for (const auto conc :
           nodal_data_points[repatom_idx].impurity_concentrations)
        conc_sum += conc;
      const auto solvent_conc = 1.0 - conc_sum;

      if (simulating_vacancy) {
        auto to_add = 0.0;
        for (const auto conc_rate :
             nodal_data_points[repatom_idx].impurity_concentration_update_rates)
          to_add += conc_rate;
        to_add /= nodal_data_points[repatom_idx].impurity_concentrations[0];
        nodal_data_points[repatom_idx].gamma_values[0] +=
            dt * (nodal_data_points[repatom_idx]
                          .impurity_concentration_update_rates[0] /
                      solvent_conc +
                  to_add);
        for (std::size_t si = 1;
             si < nodal_data_points[repatom_idx].gamma_values.size(); si++)
          nodal_data_points[repatom_idx].gamma_values[si] +=
              dt *
              (nodal_data_points[repatom_idx]
                       .impurity_concentration_update_rates[si] /
                   nodal_data_points[repatom_idx].impurity_concentrations[si] +
               to_add);
      } else {
        auto to_subtract = 0.0;
        const auto noi = nodal_data_points[repatom_idx]
                             .impurity_concentration_update_rates.size();
        for (std::size_t si = 0; si < noi; si++) {
          to_subtract +=
              (nodal_data_points[repatom_idx]
                       .impurity_concentration_update_rates[si] /
                   nodal_data_points[repatom_idx].impurity_concentrations[si] -
               nodal_data_points[repatom_idx]
                       .impurity_concentration_update_rates[si] /
                   solvent_conc);
        }
        to_subtract /= (1 + noi);
        for (std::size_t si = 0;
             si < nodal_data_points[repatom_idx].gamma_values.size(); si++)
          nodal_data_points[repatom_idx].gamma_values[si] +=
              dt *
              (nodal_data_points[repatom_idx]
                       .impurity_concentration_update_rates[si] /
                   nodal_data_points[repatom_idx].impurity_concentrations[si] -
               to_subtract);
      }

      // recalc concs based on new gamma values
      // this ensures 0 < conc < 1 always
      nodal_data_points[repatom_idx].impurity_concentrations =
          meshing::get_concentration_values(
              nodal_data_points[repatom_idx].gamma_values, simulating_vacancy);
    }
  }

  void clear() {
    // we are using repatom container instead of a vector of repatom objects
    // so converting all the calculations to a C type petsc array is simple
    locations.clear();
    forces.clear();
    velocities.clear();
    if_on_boundary.clear();
    if_fixed.clear();
    lattice_site_type.clear();
    material_indices.clear();

    ghost_dof_indices.clear();
    noof_ghosts = 0;

#ifdef FINITE_TEMPERATURE
    dissipative_potential.clear();
    if_thermally_fixed.clear();
    thermal_coordinates.clear();
    thermal_forces.clear();
    thermal_velocities.clear();
    entropy.clear();
    entropy_production_rates.clear();
#endif
    nodal_data_points.clear();

    nodal_weights.clear();
    mass.clear();
  }

  void reserve(const std::size_t &n) {
    locations.reserve(n);
    forces.reserve(n);
    velocities.reserve(n);
    lattice_site_type.reserve(n);
    material_indices.reserve(n);

    if_on_boundary.reserve(n);
    if_fixed.reserve(n);

#ifdef FINITE_TEMPERATURE
    dissipative_potential.reserve(n);
    if_thermally_fixed.reserve(n);
    thermal_coordinates.reserve(n);
    thermal_forces.reserve(n);
    thermal_velocities.reserve(n);
    entropy.reserve(n);
    entropy_production_rates.reserve(n);
#endif
    nodal_data_points.reserve(n);
    nodal_weights.reserve(n);
    mass.reserve(n);
  }

  void resize(const std::size_t &n) {
    locations.resize(n, std::array<double, Dimension>{});
    forces.resize(n, std::array<double, Dimension>{});
    velocities.resize(n, std::array<double, Dimension>{});
    lattice_site_type.resize(n, 0);
    material_indices.resize(n, 0);

    if_on_boundary.resize(n, false);
    if_fixed.resize(n, false);

#ifdef FINITE_TEMPERATURE
    dissipative_potential.resize(n, 0.0);
    if_thermally_fixed.resize(n, false);
    thermal_coordinates.resize(n, std::array<double, NIDOF>{});
    thermal_forces.resize(n, 0.0);
    thermal_velocities.resize(n, 0.0);
    entropy.resize(n, 0.0);
    entropy_production_rates.resize(n, 0.0);
#endif
    nodal_data_points.resize(n);

    nodal_weights.resize(n, 0.0);
    mass.resize(n, 0.0);
  }

  // push_back empty quantities
  void push_back() {
    locations.push_back(std::array<double, Dimension>{});
    forces.push_back(std::array<double, Dimension>{});
    velocities.push_back(std::array<double, Dimension>{});
    lattice_site_type.push_back(0);
    material_indices.push_back(0);

    if_on_boundary.push_back(false);
    if_fixed.push_back(false);

#ifdef FINITE_TEMPERATURE
    dissipative_potential.push_back(0.0);
    if_thermally_fixed.push_back(false);
    thermal_coordinates.push_back(std::array<double, NIDOF>{});
    thermal_forces.push_back(0.0);
    thermal_velocities.push_back(0.0);
    entropy.push_back(0.0);
    entropy_production_rates.push_back(0.0);
#endif

    nodal_data_points.push_back(NodalData{});

    nodal_weights.push_back(0.0);
    mass.push_back(0.0);
  }

  void add_repatom(
      const std::array<double, Dimension> &position,
      const RepatomData<meshing::traits::NodalThermalDataType<TemperatureMode>,
                        NodalData> &repatom_data);
  void remove_repatom(const std::size_t index) {
    const auto del = [](auto &vec, const auto index) {
      vec.erase(vec.begin() + index);
    };
    del(this->lattice_site_type, index);
    del(this->locations, index);
    del(this->if_fixed, index);
    if constexpr (std::is_same_v<TemperatureMode, meshing::FiniteT>) {
      del(this->thermal_coordinates, index);
      del(this->entropy, index);
      del(this->entropy_production_rates, index);
      del(this->dissipative_potential, index);
      del(this->if_thermally_fixed, index);
      del(this->thermal_forces, index);
      del(this->thermal_velocities, index);
    }
    del(this->nodal_data_points, index);
    del(this->forces, index);
    del(this->velocities, index);
    del(this->material_indices, index);
    del(this->if_on_boundary, index);
    del(this->nodal_weights, index);
    del(this->mass, index);
  }

  [[nodiscard]] RepatomData<
      meshing::traits::NodalThermalDataType<TemperatureMode>, NodalData>
  pack_data(const std::size_t index) const {
    return RepatomData<meshing::traits::NodalThermalDataType<TemperatureMode>,
                       NodalData>{
        this->lattice_site_type[index], this->if_fixed[index],
        this->pack_thermal_data(index), this->nodal_data_points[index]};
  }
  [[nodiscard]] meshing::traits::NodalThermalDataType<TemperatureMode>
  pack_thermal_data(const std::size_t index) const {
    if constexpr (std::is_same_v<TemperatureMode, meshing::FiniteT>)
      return {this->thermal_coordinates[index], this->entropy[index]};
    else
      return {};
  }
  void permute(const std::vector<std::size_t> &permutation) {
    const auto p = [&permutation](auto &vec) {
      const auto old_values = vec;
      for (std::size_t i = 0; i < vec.size(); i++)
        //vec[i] = old_values[permutation[i]];
        vec[permutation[i]] = old_values[i];
    };
    p(this->lattice_site_type);
    p(this->locations);
    p(this->if_fixed);
    if constexpr (std::is_same_v<TemperatureMode, meshing::FiniteT>) {
      p(this->thermal_coordinates);
      p(this->entropy);
      p(this->entropy_production_rates);
      p(this->dissipative_potential);
      p(this->if_thermally_fixed);
      p(this->thermal_forces);
      p(this->thermal_velocities);
    }
    p(this->nodal_data_points);
    p(this->forces);
    p(this->velocities);
    p(this->material_indices);
    p(this->if_on_boundary);
    p(this->nodal_weights);
    p(this->mass);
  }
  void get_fixed_region_force(std::array<double, Dimension> &external_force) {

    std::array<double, Dimension> local_external_force{};
    local_external_force.fill(0.0);
    external_force.fill(0.0);

    for (std::size_t repatom_index = 0;
         repatom_index < locations.size() - noof_ghosts; repatom_index++) {
      if (if_fixed[repatom_index] && locations[repatom_index][1] > 0.0) {
        array_ops::as_eigen(local_external_force) +=
            array_ops::as_eigen(forces[repatom_index]);
      }
    }

    const auto world = boost::mpi::communicator{};
    boost::mpi::all_reduce(world, local_external_force.data(), Dimension,
                           external_force.data(), std::plus<>());
  }

  void zero_velocities() {
    fill(velocities.begin(), velocities.end(), std::array<double, Dimension>{});

#ifdef FINITE_TEMPERATURE
    fill(thermal_velocities.begin(), thermal_velocities.end(), 0.0);
#endif
    periodic_velocities.fill(0.0);
  }
};

template <class TemperatureMode> struct ModifyRepatoms;

template <> struct ModifyRepatoms<meshing::FiniteT> {
  template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
  static void add_thermal_data(
      class RepatomContainer<Dimension, NIDOF, NodalData> &repatoms,
      const meshing::NodalThermalDataFiniteT &thermal_data) {
    repatoms.thermal_coordinates.push_back(thermal_data.thermal_point);
    repatoms.entropy.push_back(thermal_data.entropy);
    repatoms.entropy_production_rates.emplace_back(0.0);
    repatoms.dissipative_potential.push_back(0.0);
    repatoms.if_thermally_fixed.push_back(false);
    repatoms.thermal_forces.push_back(0.0);
    repatoms.thermal_velocities.push_back(0.0);
  }
};
template <> struct ModifyRepatoms<meshing::ZeroT> {
  template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
  static void
  add_thermal_data(class RepatomContainer<Dimension, NIDOF, NodalData> &,
                   const meshing::NodalThermalDataZeroT &) {}
};

template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
void RepatomContainer<Dimension, NIDOF, NodalData>::add_repatom(
    const std::array<double, Dimension> &position,
    const RepatomData<meshing::traits::NodalThermalDataType<TemperatureMode>,
                      NodalData> &repatom_data) {
  this->lattice_site_type.push_back(repatom_data.lattice_site_type);
  this->locations.push_back(position);
  this->if_fixed.push_back(repatom_data.fixed);
  ModifyRepatoms<
      typename RepatomContainer<Dimension, NIDOF, NodalData>::TemperatureMode>::
      add_thermal_data(*this, repatom_data.thermal_data);
  this->nodal_data_points.push_back(repatom_data.multi_species_data);
  this->forces.push_back({});
  this->velocities.push_back({});
  this->material_indices.push_back(0);
  this->if_on_boundary.push_back(false);
  this->nodal_weights.push_back(0.0);
  this->mass.push_back(0.0);
}

} // namespace repatoms
