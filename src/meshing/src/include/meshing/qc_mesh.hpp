// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Class and function implementations for QCMesh and InterProcMeshData
 * @authors P. Gupta, S. Saxena, M. Spinola
 */

#pragma once

#include "constants/constants.hpp"
#include "geometry/point.hpp"
#include "lattice/generate_centro_symmetry_data.hpp"
#include "materials/get_material_density.hpp"
#include "materials/material_base.hpp"
#include "meshing/bare_mesh.hpp"
#include "meshing/distribute_vec.hpp"
#include "meshing/nodal_data_interproc.hpp"
#include "meshing/nodal_data_multispecies.hpp"
#include "meshing/offset_vectors.hpp"
#include "meshing/partition_ghost_indices.hpp"
#include "meshing/periodicity_parameters.hpp"
#include "meshing/qc_cell_data.hpp"
#include "meshing/qc_nodal_data.hpp"
#include "meshing/repatomcontainer.hpp"
#include "meshing/samplingatoms.hpp"
#include "meshing/temperature_mode.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <Eigen/Dense>
#include <unordered_set>

namespace array_ops = qcmesh::array_ops;

namespace meshing {

/// Minimum metric residue allowed
constexpr std::size_t REMESH_TOLERANCE = 1.0e1;

template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
struct InterProcMeshData {
public:
  using Offset = std::vector<std::array<double, D>>;
  using Matrix = Eigen::Matrix<double, K, K>;
  using ThermalPoint = geometry::Point<NIDOF>;

  std::vector<std::vector<bool>> overlap_simplex_idxs_sent_flag;
  std::vector<std::vector<int>> overlap_simplex_idxs;
  std::vector<std::vector<int>> overlap_node_idxs;

  // received nodes and simplices from other ranks.
  // to get neighbourhoods across processors
  std::vector<std::vector<geometry::Point<D>>> overlapping_nodes_recv;
  std::vector<std::vector<std::array<std::size_t, K + 1>>>
      overlapping_simplices_recv;
  std::vector<std::vector<std::array<std::array<double, K>, K>>>
      overlapping_element_lattice_vectors_recv;
  std::vector<std::vector<Offset>> overlapping_element_offsets_recv;

  std::vector<std::vector<bool>> overlapping_simplices_recv_atomistic_flag;
  std::vector<std::vector<int>> overlapping_node_material_ids_recv;
  std::vector<std::vector<int>> overlapping_node_site_types_recv;
#ifdef FINITE_TEMPERATURE
  std::vector<std::vector<ThermalPoint>> overlapping_thermal_dofs_recv;
#endif
  std::vector<std::vector<NodalData>> overlapping_nodal_data_recv;

  // we will try google maps later on
  std::vector<std::unordered_map<int, int>> overlap_nodes_sent_idx_recv_side;
};

// this is a mesh specific to QC.
// it has additional information for QC nodes and simplices
template <std::size_t D = 3, std::size_t K = 3, std::size_t NIDOF = 2,
          typename NodalData = NodalDataMultispecies<1>>
class QCMesh
    : public qcmesh::mesh::SimplexMesh<D, K, QCNodalData, QCCellData<D, K>> {
public:
  static constexpr std::size_t DIMENSION = D;
  static constexpr std::size_t SIMPLEX_DIMENSION = K;
  static constexpr std::size_t THERMAL_DOF = NIDOF;
  static constexpr std::size_t NOI = traits::N_IMPURITIES<NodalData>;
#ifdef FINITE_TEMPERATURE
  using TemperatureMode = FiniteT;
#else
  using TemperatureMode = ZeroT;
#endif
  using RepatomContainerType = repatoms::RepatomContainer<D, NIDOF, NodalData>;
  using NodalDataType = NodalData;
  using Base = qcmesh::mesh::SimplexMesh<D, K, QCNodalData, QCCellData<D, K>>;
  using Offset = std::vector<std::array<double, D>>;
  using Simplex = typename std::array<geometry::Point<D>, K + 1>;
  using Matrix = Eigen::Matrix<double, K, K>;
  using StlMatrix = std::array<double, K * K>;
  using OverlapIndex = std::pair<int, int>;
  using ThermalPoint = geometry::Point<NIDOF>;

  // needed to build central sampling atoms from scratch
  double node_effective_radius{0.0};
  double minimum_barycenter_weight{0.0};

  // domain bounds
  qcmesh::geometry::Box<D> domain_bound{geometry::Point<D>{0.0},
                                        geometry::Point<D>{0.0}};
  qcmesh::geometry::Box<D> local_domain_bound{geometry::Point<D>{0.0},
                                              geometry::Point<D>{0.0}};

  // periodic variables
  bool has_periodic_dimensions{};
  std::array<bool, 3> periodic_flags{};
  geometry::Point<D> periodic_lengths{0.0};
  std::vector<std::array<int, 3>> periodic_offset_directions;
  std::vector<geometry::Point<D>> periodic_offsets;

  std::vector<qcmesh::mesh::SimplexCellId> vertex_adjacency{};
  std::vector<std::vector<qcmesh::mesh::SimplexCellId>::iterator>
      vertex_adjacency_pointers{};

  std::vector<qcmesh::mesh::SimplexCellId> simplex_index_to_key{};
  std::vector<qcmesh::mesh::NodeId> node_index_to_key{};

  // connected nodes and faces (important for partitioned meshes)
  // indices in nodes
  std::vector<int> connected_node_procs{};
  std::vector<std::vector<std::size_t>> connected_nodes{};

  std::unordered_map<std::size_t, std::size_t>
      original_keys_of_periodic_images{};
  std::unordered_map<std::size_t, geometry::Point<D>>
      offsets_of_periodic_images{};

  std::vector<int> global_ghost_indices{};
  std::vector<geometry::Point<D>> ghost_node_offsets{};

  // use a MaterialBase object with the only necessary things for
  // the mesh. This is needed since we can't copy the initialized
  // materials vector to qcMaterials_ above in assignMaterials.
  // This is because Material has std::unique_ptr and therefore
  // compiler will add deleted copy constructor to material class
  // which will be called when assigning
  std::vector<materials::MaterialBase<D>> materials;

  // these stay constant unless an element is flipped.
  // No need to store lattice generators. Compute when needed

  std::vector<int> material_idx_of_nodes;
  std::vector<int> sampling_atoms_of_simplices;

  // element offset vectors
  Offset element_offset_coeffs;

  InterProcMeshData<D, K, NIDOF, NodalData> overlapping_mesh_data;
  InterProcMeshData<D, K, NIDOF, NodalData> periodic_mesh_data;

  /*
   * We should just make elementDataContainer and nodeDataContainer
   * Instead of naked containers here.
   */

  repatoms::RepatomContainer<D, NIDOF, NodalData> repatoms;
  sampling_atoms::SamplingAtomContainer<D, NIDOF, NodalData> sampling_atoms;
  std::unordered_map<qcmesh::mesh::SimplexCellId, std::size_t>
      cell_key_to_central_sampling_atom;

  // received nodes and cells from other ranks for mesh repair
#ifdef FINITE_TEMPERATURE
  std::unordered_map<qcmesh::mesh::NodeId, double> halo_node_entropy;
  std::unordered_map<qcmesh::mesh::NodeId, ThermalPoint>
      halo_node_thermal_points;
  std::vector<double> central_cluster_energies_gnn;
  std::vector<double> nodal_cluster_energies_gnn;
#endif
  std::unordered_map<qcmesh::mesh::NodeId, NodalData> halo_node_nodal_data;

  std::unordered_map<qcmesh::mesh::NodeId, int> halo_node_material_indices;

  // atomistic domains inside the mesh
  qcmesh::geometry::Box<D> atomistic_domain;

  std::size_t noof_offset_vectors{};

  QCMesh() : Base{}, repatoms(), sampling_atoms(), atomistic_domain() {}

  std::size_t initialize_periodic_parameters(
      const std::array<std::optional<PeriodicityParameters>, 3>
          &input_periodic_parameters) {

    // initialise periodic flags
    for (std::size_t i = 0; i < input_periodic_parameters.size(); i++)
      periodic_flags[i] = bool(input_periodic_parameters[i]);

    // count the number of periodic directions
    const auto n_periods = static_cast<std::size_t>(
        std::count(periodic_flags.begin(), periodic_flags.end(), true));
    this->has_periodic_dimensions = n_periods > 0;
    if (n_periods > D)
      throw exception::QCException("Number of periodic directions is greater "
                                   "than the simulation dimension! ");

    // set the initial domain bounds to be equal to input periodic parameters
    // get initial periodic lenghts. They are overwritten with stored data for restart-simulations
    for (std::size_t i = 0; i < input_periodic_parameters.size(); i++)
      if (input_periodic_parameters[i].has_value()) {
        const auto &prms = input_periodic_parameters[i].value();
        domain_bound.lower[i] = prms.min_coordinate;
        domain_bound.upper[i] = prms.min_coordinate + prms.length;
        periodic_lengths[i] = domain_bound.upper[i] - domain_bound.lower[i];
      }

    // initialize periodic_offset_directions.
    // periodic_offsets are initialised later after the mesh is initialized.
    const auto offset_order = std::array<int, 3>{{0, 1, -1}};

    const auto noof_all_offsets = static_cast<std::size_t>(pow(3, n_periods));
    periodic_offset_directions.resize(noof_all_offsets, std::array<int, 3>{});

    const auto periodic_dimension = [](const auto &periodic_flags,
                                       std::size_t start = 0) {
      for (std::size_t i = start; i < periodic_flags.size(); i++) {
        if (periodic_flags[i])
          return i;
      }
      return periodic_flags.size();
    };
    switch (n_periods) {
    case 1: {
      const auto d = periodic_dimension(periodic_flags);

      periodic_offset_directions[0][d] = offset_order[0];
      periodic_offset_directions[1][d] = offset_order[1];
      periodic_offset_directions[2][d] = offset_order[2];
      break;
    }
    case 2: {
      const auto d = periodic_dimension(periodic_flags);
      const auto e = periodic_dimension(periodic_flags, d + 1);

      // 0, +1, -1

      for (std::size_t i = 0; i < 3; i++) {
        periodic_offset_directions[i][d] = 0;
        periodic_offset_directions[i][e] = offset_order[i];

        periodic_offset_directions[i + 3][d] = 1;
        periodic_offset_directions[i + 3][e] = offset_order[i];

        periodic_offset_directions[i + 6][d] = -1;
        periodic_offset_directions[i + 6][e] = offset_order[i];
      }
      break;
    }
    case 3: {
      for (std::size_t i = 0; i < 3; i++) {
        for (std::size_t j = 0; j < 3; j++) {
          for (std::size_t k = 0; k < 3; k++) {
            periodic_offset_directions[i * 9 + j * 3 + k][0] = offset_order[i];
            periodic_offset_directions[i * 9 + j * 3 + k][1] = offset_order[j];
            periodic_offset_directions[i * 9 + j * 3 + k][2] = offset_order[k];
          }
        }
      }
      break;
    }
    default:
      break;
    }
    return n_periods;
  }

  void initialize_periodic_offsets() {
    periodic_offsets.resize(periodic_offset_directions.size());
    for (std::size_t i = 0; i < periodic_offset_directions.size(); i++) {
      periodic_offsets[i].fill(0.0);
      for (std::size_t d = 0; d < D; d++) {
        periodic_offsets[i][d] +=
            double(periodic_offset_directions[i][d]) * periodic_lengths[d];
      }
    }
  }

  template <typename ContainerType>
  geometry::Point<D>
  get_overlapping_mesh_point(const ContainerType &overlap_container,
                             const std::pair<int, std::size_t> &id) const {
    if ((id.first >=
         static_cast<int>(overlap_container.overlapping_nodes_recv.size())) ||
        (id.second >=
         overlap_container.overlapping_nodes_recv[id.first].size()))
      throw exception::QCException{
          "get_overlapping_mesh_point: Indiced out of bounds."};

    return overlap_container.overlapping_nodes_recv[id.first][id.second];
  }

  template <typename ContainerType>
  NodalData get_overlapping_nodal_data_point(
      const ContainerType &overlap_container,
      const std::pair<int, std::size_t> &id) const {
    return overlap_container.overlapping_nodal_data_recv[id.first][id.second];
  }

#ifdef FINITE_TEMPERATURE
  template <typename ContainerType>
  ThermalPoint
  get_overlapping_thermal_point(const ContainerType &overlap_container,
                                const std::pair<int, std::size_t> &id) const {
    return overlap_container.overlapping_thermal_dofs_recv[id.first][id.second];
  }
#endif

  // ! get the overlapping simplex with vertices
  void get_overlapping_simplex(const OverlapIndex &i, Simplex &s) {
    for (std::size_t d = 0; d < K + 1; d++) {
      s[d] =
          overlapping_mesh_data.overlapping_nodes_recv
              [i.first][overlapping_mesh_data
                            .overlapping_simplices_recv[i.first][i.second][d]];
    }
  }

  // ! get the periodic simplex with vertices
  void get_periodic_simplex(const OverlapIndex &i, Simplex &s) {
    for (std::size_t d = 0; d < K + 1; d++) {
      s[d] =
          periodic_mesh_data.overlapping_nodes_recv
              [i.first][periodic_mesh_data
                            .overlapping_simplices_recv[i.first][i.second][d]];
    }
  }

  void assign_mono_lattice_flag() {
    std::array<int, K + 1> array_material_idxs{};

    const auto mpi_rank = boost::mpi::communicator{}.rank();

    for (auto &[_, cell] : this->cells) {
      for (std::size_t d = 0; d < K + 1; d++) {
        const auto node_key = cell.nodes[d];

        array_material_idxs[d] =
            material_idx_of_nodes[Base::nodes.at(node_key).data.mesh_index];
      }
      const auto is_mono_lattice =
          std::count(array_material_idxs.begin(), array_material_idxs.end(),
                     array_material_idxs[0]) == K + 1;
      if (!is_mono_lattice)
        std::cout << static_cast<std::size_t>(cell.id)
                  << ", detected a multi-lattice element. Just warning .... "
                  << "rank : " << mpi_rank << std::endl;
      // all such multilattice elements should be atomistic
      else
        cell.data.material_index = array_material_idxs[0];
      cell.data.is_mono_lattice = is_mono_lattice;
    }
  }

  template <class MaterialType>
  void initialize_bravais_lattice(const std::vector<MaterialType> &materials) {

    assign_mono_lattice_flag();

    std::array<std::array<double, K>, K> lattice_vectors{};

    Offset offset_vectors;

    for (auto &[_, cell] : this->cells) {
      offset_vectors.clear();

      if (cell.data.is_mono_lattice) {
        for (std::size_t p = 0; p < K; p++) {
          for (std::size_t q = 0; q < K; q++) {
            lattice_vectors[q][p] =
                materials[cell.data.material_index].basis_vectors[q][p];
          }
        }
      }
      // TODO
      // element must have all lattice vectors for generating the neighbours
      // or if they are always atomistic, then we don't need lattice vectors for
      // these
      else {
        const auto simplex = qcmesh::mesh::simplex_cell_points(*this, cell);
        for (std::size_t p = 0; p < K; p++) {
          for (std::size_t q = 0; q < K; q++) {
            lattice_vectors[q][p] = simplex[q + 1][p] - simplex[0][p];
          }
        }
      }

      cell.data.lattice_vectors = lattice_vectors;

      offset_vectors.resize(element_offset_coeffs.size());
      for (std::size_t p = 0; p < element_offset_coeffs.size(); p++) {
        for (std::size_t d = 0; d < D; d++) {
          for (std::size_t e = 0; e < D; e++) {
            offset_vectors[p][d] +=
                element_offset_coeffs[p][e] * lattice_vectors[d][e];
          }
        }
      }

      cell.data.offset_vectors = offset_vectors;

      cell.data.jacobian = 1.0;

      // element density is material density/jacobian
    }
    noof_offset_vectors = element_offset_coeffs.size();
  }

  void initialize_bravais_lattice(
      const qcmesh::geometry::Box<D> &,
      const std::vector<materials::MaterialBase<D>> &materials) {

    assign_mono_lattice_flag();

    Matrix lattice_vectors;
    Offset offset_vectors;

    geometry::Point<D> centroid;

    centroid.fill(0.0);

    Matrix material_basis;
    std::array<geometry::Point<D>, K> material_bases_stl{};

    for (auto &[_, cell] : this->cells) {
      offset_vectors.clear();
      centroid.fill(0.0);

      for (std::size_t p = 0; p < K; p++) {
        for (std::size_t q = 0; q < K; q++) {
          material_basis(q, p) =
              materials[cell.data.material_index].basis_vectors[q][p];

          material_bases_stl[p][q] = material_basis(q, p);
        }
      }

      const auto simplex = qcmesh::mesh::simplex_cell_points(*this, cell);

      for (std::size_t d = 0; d < K + 1; d++) {
        array_ops::as_eigen(centroid) +=
            array_ops::as_eigen(simplex[d]) / (double(K + 1));
      }

      if (cell.data.is_mono_lattice) {
        // we can not assign the lattice vectors simply as edges
        // every edge needs to have a single unique projection (n1, n2, n3)
        // in the basis of all its incident elements. Otherwise, the remeshing
        // won't work.
        lattice_vectors = material_basis;
      }
      // TODO
      // element must have all lattice vectors for generating the neighbours
      // or if they are always atomistic, then we don't need lattice vectors for
      // these
      else {
        throw exception::QCException{
            "initialize_bravais_lattice: found a multi-lattice element"};

        for (std::size_t p = 0; p < K; p++) {
          for (std::size_t q = 0; q < K; q++) {
            lattice_vectors(q, p) = simplex[q + 1][p] - simplex[0][p];
          }
        }
      }

      // compute deformation gradient:
      const auto initial_deformation =
          lattice_vectors * material_basis.inverse();
      array_ops::as_eigen_matrix(cell.data.lattice_vectors) = lattice_vectors;

      element_offset_coeffs = materials[cell.data.material_index].offset_coeffs;
      offset_vectors.resize(element_offset_coeffs.size());
      for (std::size_t p = 0; p < element_offset_coeffs.size(); p++) {
        for (std::size_t d = 0; d < D; d++) {
          for (std::size_t e = 0; e < D; e++) {
            offset_vectors[p][d] +=
                element_offset_coeffs[p][e] * lattice_vectors(d, e);
          }
        }
      }

      cell.data.offset_vectors = offset_vectors;
      cell.data.jacobian = initial_deformation.determinant();

      // element density is material density/jacobian
    }
    noof_offset_vectors = element_offset_coeffs.size();
  }

  void initialize_elements_atomistic_flag() {
    for (auto &[_, cell] : this->cells) {
      auto centroid = geometry::Point<D>{};
      const auto simplex = qcmesh::mesh::simplex_cell_points(*this, cell);

      for (std::size_t d = 0; d < K + 1; d++)
        array_ops::as_eigen(centroid) += array_ops::as_eigen(simplex[d]);
      array_ops::as_eigen(centroid) /= static_cast<double>(K + 1);

      const auto jac = qcmesh::geometry::simplex_jacobian(simplex);
      const auto n_space_det =
          (array_ops::as_eigen_matrix(cell.data.lattice_vectors).inverse() *
           array_ops::as_eigen_matrix(jac))
              .transpose()
              .array()
              .round()
              .matrix()
              .template cast<int>()
              .determinant();

      cell.data.is_atomistic =
          (std::abs(n_space_det) <= 1 ||
           qcmesh::geometry::contains(atomistic_domain, centroid));
    }
  }

  void initialize_element_vectors() {
    sampling_atoms_of_simplices.clear();

    sampling_atoms_of_simplices.resize(Base::cells.size(),
                                       std::numeric_limits<int>::max());
  }

  double volume() const {
    double local_volume = 0.0;
    for (const auto &[_, cell] : Base::cells)
      local_volume += cell.data.volume;
    return qcmesh::mpi::all_reduce_sum(local_volume);
  }

  void build_repatoms(
      const std::unordered_map<qcmesh::mesh::NodeId, int> &element_node_flags,
      const std::unordered_map<qcmesh::mesh::NodeId, int>
          &element_lattice_site_types
#ifdef FINITE_TEMPERATURE
      ,
      const std::unordered_map<qcmesh::mesh::NodeId, ThermalPoint>
          &initialthermal_points,
      const std::unordered_map<qcmesh::mesh::NodeId, double> &initial_entropies
#endif
      ,
      const std::unordered_map<qcmesh::mesh::NodeId, NodalData>
          &initial_nodal_data) {
    // build repatoms.
    repatoms.clear();
    repatoms.resize(Base::nodes.size());

    for (std::size_t idx = 0; idx < this->node_index_to_key.size(); idx++) {
      repatoms.locations[idx] =
          Base::nodes.at(this->node_index_to_key[idx]).position;

      repatoms.if_fixed[idx] =
          element_node_flags.at(this->node_index_to_key[idx]);

      repatoms.lattice_site_type[idx] =
          element_lattice_site_types.at(this->node_index_to_key[idx]);

#ifdef FINITE_TEMPERATURE
      repatoms.thermal_coordinates[idx] =
          initialthermal_points.at(this->node_index_to_key[idx]);

      repatoms.entropy[idx] =
          initial_entropies.at(this->node_index_to_key[idx]);

      repatoms.entropy_production_rates[idx] = 0.0;
#endif

      repatoms.nodal_data_points[idx] =
          initial_nodal_data.at(this->node_index_to_key[idx]);
    }

    repatoms.assign_ghost_do_fs(this->global_ghost_indices);
  }

  void build_sampling_atoms() {
    sampling_atoms.clear();

    sampling_atoms.resize_nodal_sampling_atoms(Base::nodes.size());

    cell_key_to_central_sampling_atom.clear();

    for (std::size_t atom_index = 0; atom_index < repatoms.locations.size();
         atom_index++) {
      sampling_atoms.nodal_locations[atom_index] =
          repatoms.locations[atom_index];

      sampling_atoms.nodal_cut_offs[atom_index] =
          materials[material_idx_of_nodes[atom_index]].neighbor_cutoff_radius;
    }

    geometry::Point<D> simplex_bary_center;
#ifdef FINITE_TEMPERATURE
    ThermalPoint central_sampling_atom_thermal_dof;
#endif
    NodalData central_sampling_atom_nodal_data;

    double r_cut{};

    for (const auto &[cell_id, cell] : this->cells) {
      const auto element_volume = cell.data.volume;
      const auto element_density = cell.data.density;
      const auto element_jacobian = cell.data.jacobian;

      const auto node_radius =
          node_effective_radius * pow(element_jacobian, 1.0 / double(K));

      if (element_volume > 1.0e-6) {
        const auto simplex = qcmesh::mesh::simplex_cell_points(*this, cell);
        const auto metrics =
            qcmesh::geometry::simplex_metrics(simplex, node_radius);

        const auto barycenter_weight =
            metrics.barycenter_volume * element_density;

        for (std::size_t d = 0; d < K + 1; d++) {
          const auto node_index = Base::nodes.at(cell.nodes[d]).data.mesh_index;

          sampling_atoms.nodal_weights[node_index] +=
              metrics.vertex_volumes[d] * element_density;
        }

        if (barycenter_weight >= minimum_barycenter_weight) {

          r_cut = 0.0;

          simplex_bary_center.fill(0.0);

#ifdef FINITE_TEMPERATURE
          central_sampling_atom_thermal_dof.fill(0.0);
#endif

          for (std::size_t d = 0; d < K + 1; d++) {
            const auto node_index =
                Base::nodes.at(cell.nodes[d]).data.mesh_index;

            array_ops::as_eigen(simplex_bary_center) +=
                array_ops::as_eigen(simplex[d]) / double(K + 1);

            r_cut = std::max(r_cut, materials[material_idx_of_nodes[node_index]]
                                        .neighbor_cutoff_radius);

#ifdef FINITE_TEMPERATURE
            array_ops::as_eigen(central_sampling_atom_thermal_dof) +=
                array_ops::as_eigen(repatoms.thermal_coordinates[node_index]) /
                double(K + 1);
#endif
            InterpolateNodalData<NodalData>::interpolate(
                repatoms.nodal_data_points[node_index],
                central_sampling_atom_nodal_data, 1 / double(K + 1));
          }

          sampling_atoms.central_weights.push_back(barycenter_weight);

          sampling_atoms.central_locations.push_back(simplex_bary_center);

          sampling_atoms.cell_key_of_central_atoms.push_back(cell_id);

          sampling_atoms.central_cut_offs.push_back(r_cut);

#ifdef FINITE_TEMPERATURE
          sampling_atoms.central_ido_fs.push_back(
              central_sampling_atom_thermal_dof);
#endif

          sampling_atoms.central_nodal_data.push_back(
              central_sampling_atom_nodal_data);

          cell_key_to_central_sampling_atom.insert(std::make_pair(
              cell_id, sampling_atoms.central_locations.size() - 1));

          // add material ids of sampling atoms here.
          // for now adding only last node's material index
          const auto node_index = Base::nodes.at(cell.nodes[K]).data.mesh_index;
          sampling_atoms.central_material_ids.push_back(
              material_idx_of_nodes[node_index]);

        } else if (barycenter_weight > 0.0) {
          const auto total_angles =
              array_ops::as_eigen(metrics.solid_angles).sum();

          if (total_angles > 1.0e-6) {
            for (std::size_t d = 0; d < K + 1; d++) {
              const auto node_index =
                  Base::nodes.at(cell.nodes[d]).data.mesh_index;

              sampling_atoms.nodal_weights[node_index] +=
                  barycenter_weight * metrics.solid_angles[d] / total_angles;
            }
          }
        }
      }
    }

    exchange_weights(sampling_atoms);

    for (std::size_t atom_index = 0;
         atom_index < sampling_atoms.nodal_locations.size(); atom_index++) {

      if (std::isnan(sampling_atoms.nodal_weights[atom_index]) ||
          std::isinf(sampling_atoms.nodal_weights[atom_index]))
        throw exception::QCException{
            "build_sampling_atoms: nan or inf nodal sampling atom weight"};

      if (sampling_atoms.nodal_weights[atom_index] < 1.0) {
        sampling_atoms.nodal_weights[atom_index] = 1.0;
      }
      // make nodal weights 1.0 for atomistic sampling atoms
      if (qcmesh::geometry::contains(
              atomistic_domain, sampling_atoms.nodal_locations[atom_index])) {
        sampling_atoms.nodal_weights[atom_index] = 1.0;
      }
    }

    // resize the samplingAtoms.centralSamplingAtomNeighbours_
    sampling_atoms.central_sampling_atom_neighbours.clear();
    sampling_atoms.central_energies.clear();

    sampling_atoms.central_sampling_atom_neighbours.resize(
        sampling_atoms.central_locations.size());
    sampling_atoms.central_energies.resize(
        sampling_atoms.central_locations.size());
  }

  void
  build_sampling_atoms(const std::vector<double> &element_weights,
                       const std::unordered_map<qcmesh::mesh::NodeId, double>
                           &element_node_weights) {
    sampling_atoms.clear();

    sampling_atoms.resize_nodal_sampling_atoms(Base::nodes.size());
    cell_key_to_central_sampling_atom.clear();

    for (std::size_t atom_index = 0; atom_index < repatoms.locations.size();
         atom_index++) {
      sampling_atoms.nodal_locations[atom_index] =
          repatoms.locations[atom_index];

      sampling_atoms.nodal_cut_offs[atom_index] =
          materials[material_idx_of_nodes[atom_index]].neighbor_cutoff_radius;

      sampling_atoms.nodal_weights[atom_index] =
          element_node_weights.at(this->node_index_to_key[atom_index]);
    }

    geometry::Point<D> simplex_bary_center;
#ifdef FINITE_TEMPERATURE
    ThermalPoint central_sampling_atom_thermal_dof;
#endif

    if (element_weights.size() != this->simplex_index_to_key.size())
      throw exception::QCException{"build_sampling_atoms: mismatching noof "
                                   "element weights and noof mesh elements"};

    double r_cut{};

    for (std::size_t idx = 0; idx < this->simplex_index_to_key.size(); idx++) {
      if (element_weights[idx] > 0.0) {
        const auto cell_key = this->simplex_index_to_key[idx];
        const auto simplex = qcmesh::mesh::simplex_cell_points(*this, cell_key);

        r_cut = 0.0;

        simplex_bary_center.fill(0.0);

#ifdef FINITE_TEMPERATURE
        central_sampling_atom_thermal_dof.fill(0.0);
#endif

        NodalData central_sampling_atom_nodal_data;

        for (std::size_t d = 0; d < K + 1; d++) {
          const auto node_index =
              Base::nodes.at(Base::cells.at(cell_key).nodes[d]).data.mesh_index;

          array_ops::as_eigen(simplex_bary_center) +=
              array_ops::as_eigen(simplex[d]) / double(K + 1);

          r_cut = std::max(r_cut, materials[material_idx_of_nodes[node_index]]
                                      .neighbor_cutoff_radius);

#ifdef FINITE_TEMPERATURE
          array_ops::as_eigen(central_sampling_atom_thermal_dof) +=
              array_ops::as_eigen(repatoms.thermal_coordinates[node_index]) /
              double(K + 1);
#endif
          InterpolateNodalData<NodalData>::interpolate(
              repatoms.nodal_data_points[node_index],
              central_sampling_atom_nodal_data, 1 / double(K + 1));
        }

        sampling_atoms.central_weights.push_back(element_weights[idx]);

        sampling_atoms.central_locations.push_back(simplex_bary_center);

        sampling_atoms.cell_key_of_central_atoms.push_back(cell_key);

        sampling_atoms.central_cut_offs.push_back(r_cut);

#ifdef FINITE_TEMPERATURE
        sampling_atoms.central_ido_fs.push_back(
            central_sampling_atom_thermal_dof);
#endif
        sampling_atoms.central_nodal_data.push_back(
            central_sampling_atom_nodal_data);

        cell_key_to_central_sampling_atom.insert(std::make_pair(
            cell_key, sampling_atoms.central_locations.size() - 1));

        // add material ids of sampling atoms here.
        // for now adding only last node's material index
        const auto node_index =
            Base::nodes.at(Base::cells.at(cell_key).nodes[K]).data.mesh_index;
        sampling_atoms.central_material_ids.push_back(
            material_idx_of_nodes[node_index]);
      }
    }

    for (double &nodal_weight : sampling_atoms.nodal_weights) {
      if (std::isnan(nodal_weight) || std::isinf(nodal_weight))
        throw exception::QCException{
            "build_sampling_atoms: nan or inf nodal sampling atom weight"};

      if (nodal_weight < 1.0) {
        nodal_weight = 1.0;
      }
    }

    // resize the samplingAtoms.centralSamplingAtomNeighbours_
    sampling_atoms.central_sampling_atom_neighbours.clear();
    sampling_atoms.central_energies.clear();

    sampling_atoms.central_sampling_atom_neighbours.resize(
        sampling_atoms.central_locations.size());
    sampling_atoms.central_energies.resize(
        sampling_atoms.central_locations.size());
  }

  void assign_repatom_weights() {
    fill(repatoms.nodal_weights.begin(), repatoms.nodal_weights.end(), 0.0);

    for (std::size_t atom_index = 0;
         atom_index < sampling_atoms.central_locations.size(); atom_index++) {
      const auto cell_key =
          sampling_atoms.cell_key_of_central_atoms[atom_index];
      for (std::size_t d = 0; d < K + 1; d++) {
        const auto node_index =
            Base::nodes.at(Base::cells.at(cell_key).nodes[d]).data.mesh_index;

        repatoms.nodal_weights[node_index] +=
            sampling_atoms.central_weights[atom_index] / (double(K + 1));
      }
    }
    exchange_weights(repatoms);

    for (std::size_t atom_index = 0;
         atom_index < sampling_atoms.nodal_weights.size(); atom_index++) {
      repatoms.nodal_weights[atom_index] +=
          sampling_atoms.nodal_weights[atom_index];
    }

    for (std::size_t atom_index = 0; atom_index < repatoms.nodal_weights.size();
         atom_index++) {
      if (repatoms.nodal_weights[atom_index] < 1.0)
        repatoms.nodal_weights[atom_index] = 1.0;
    }

    assign_repatom_masses();

    repatoms.initial_locations.clear();
    repatoms.initial_locations = repatoms.locations;
    repatoms.calc_total_mass();
  }

  void assign_repatom_masses() {
    for (std::size_t atom_index = 0; atom_index < repatoms.nodal_weights.size();
         atom_index++)
      repatoms.mass[atom_index] =
          repatoms.nodal_weights[atom_index] * constants::G_TO_EV_FS2_A2 *
          materials[material_idx_of_nodes[atom_index]].atomic_masses[0];
  }

  void compute_nodal_centro_symmetry() {
    fill(sampling_atoms.nodal_centro_symmetry.begin(),
         sampling_atoms.nodal_centro_symmetry.end(), 0.0);

    for (std::size_t node_index = 0;
         node_index < Base::nodes.size() - this->global_ghost_indices.size();
         node_index++) {
      auto lattice_symmetries = std::vector<geometry::Point<D>>{};
      const auto &material = this->materials[material_idx_of_nodes[node_index]];
      const auto constrained = false;
      lattice::generate_centro_symmetry_data(material.lattice_parameter_vec,
                                             material.lattice_type, constrained,
                                             lattice_symmetries);

      sampling_atoms.nodal_centro_symmetry[node_index] =
          sampling_atoms.get_nodal_centro_symmetry(node_index,
                                                   lattice_symmetries);
    }

    exchange_nodal_values(sampling_atoms.nodal_centro_symmetry);
  }

  template <typename ValueType>
  void exchange_nodal_values(std::vector<ValueType> &nodal_values) {
    const auto n_ghosts = this->global_ghost_indices.size();
    const auto n_locals = this->nodes.size() - n_ghosts;
    const auto n_repatoms = meshing::count_repatoms(*this);
    distribute_vec(n_ghosts, n_locals, n_repatoms, this->global_ghost_indices,
                   nodal_values);
  }

  std::unordered_set<qcmesh::mesh::NodeId> find_unreferenced_nodes() {
    auto unreferenced_nodes = std::unordered_set<qcmesh::mesh::NodeId>{};
    for (const auto &[node_id, _] : this->nodes)
      unreferenced_nodes.emplace(node_id);

    for (const auto &[_, cell] : this->cells)
      for (const auto node_id : cell.nodes)
        unreferenced_nodes.erase(node_id);
    return unreferenced_nodes;
  }

  /**
   * @brief Remove multiple cells.
   *
   * Does not remove sampling atom data.
   */
  void remove_cells(
      const std::vector<qcmesh::mesh::SimplexCellId> &cells_to_delete) {
    if (cells_to_delete.empty())
      return;
    auto indices_to_delete = std::vector<std::size_t>{};
    indices_to_delete.reserve(cells_to_delete.size());
    for (const auto id : cells_to_delete) {
      if (this->cells.find(id) != this->cells.end())
        indices_to_delete.emplace_back(this->cells.at(id).data.mesh_index);
      this->cells.erase(id);
      this->halo_cells.erase(id);
    }
    std::sort(indices_to_delete.begin(), indices_to_delete.end(),
              std::greater<>());
    for (const auto idx : indices_to_delete)
      this->simplex_index_to_key.erase(this->simplex_index_to_key.begin() +
                                       idx);
    for (std::size_t idx = indices_to_delete.back();
         idx < this->simplex_index_to_key.size(); idx++)
      this->cells.at(this->simplex_index_to_key[idx]).data.mesh_index = idx;
  }

  /**
   * @brief Remove multiple nodes including repatom data.
   *
   * Clears the sampling atoms.
   */
  void remove_nodes(
      const std::unordered_set<qcmesh::mesh::NodeId> &nodes_to_delete) {
    if (nodes_to_delete.empty())
      return;
    auto indices_to_delete = std::vector<std::size_t>{};
    indices_to_delete.reserve(nodes_to_delete.size());
    for (const auto id : nodes_to_delete) {
      indices_to_delete.emplace_back(this->nodes.at(id).data.mesh_index);
      this->nodes.erase(id);
    }
    std::sort(indices_to_delete.begin(), indices_to_delete.end(),
              std::greater<>());
    for (const auto idx : indices_to_delete) {
      this->node_index_to_key.erase(this->node_index_to_key.begin() + idx);
      this->material_idx_of_nodes.erase(this->material_idx_of_nodes.begin() +
                                        idx);
      this->repatoms.remove_repatom(idx);
    }
    for (std::size_t idx = indices_to_delete.back();
         idx < this->node_index_to_key.size(); idx++)
      this->nodes.at(this->node_index_to_key[idx]).data.mesh_index = idx;
  }

  template <typename ContainerType>
  void exchange_weights(ContainerType &container) {
    this->exchange_nodal_values(container.nodal_weights);
  }

  int get_material_index_of_node(const qcmesh::mesh::NodeId &node_key) const {
    if (Base::nodes.find(node_key) != Base::nodes.end()) {
      return material_idx_of_nodes[Base::nodes.at(node_key).data.mesh_index];
    }
    if (halo_node_material_indices.find(node_key) !=
        halo_node_material_indices.end()) {
      return halo_node_material_indices.at(node_key);
    }
    throw exception::QCException{"get_material_index_of_node: node key found "
                                 "neither in local nor in halo mesh"};
  }

  int get_site_type_of_node(const qcmesh::mesh::NodeId &node_key) const {
    if (Base::nodes.find(node_key) != Base::nodes.end()) {
      return Base::nodes.at(node_key).data.type;
    }
    if (Base::halo_nodes.find(node_key) != Base::halo_nodes.end()) {
      return Base::halo_nodes.at(node_key).data.type;
    }
    throw exception::QCException{"get_site_type_of_node: node key found "
                                 "neither in local nor in halo mesh"};
  }

#ifdef FINITE_TEMPERATURE
  ThermalPoint
  get_thermal_point_of_node(const qcmesh::mesh::NodeId &node_key) const {
    if (Base::nodes.find(node_key) != Base::nodes.end()) {
      return repatoms
          .thermal_coordinates[Base::nodes.at(node_key).data.mesh_index];
    }
    if (halo_node_thermal_points.find(node_key) !=
        halo_node_thermal_points.end()) {
      return halo_node_thermal_points.at(node_key);
    }
    throw exception::QCException{"get_thermal_point_of_node: node key found "
                                 "neither in local nor in halo mesh"};
  }

  double get_entropy_of_node(const qcmesh::mesh::NodeId &node_key) const {
    if (Base::nodes.find(node_key) != Base::nodes.end()) {
      return repatoms.entropy[Base::nodes.at(node_key).data.mesh_index];
    }
    if (halo_node_entropy.find(node_key) != halo_node_entropy.end()) {
      return halo_node_entropy.at(node_key);
    }
    throw exception::QCException{"get_entropy_of_node: node key found "
                                 "neither in local nor in halo mesh"};
  }
#endif

  const NodalData &
  get_nodal_data_of_node(const qcmesh::mesh::NodeId &node_key) const {
    if (Base::nodes.find(node_key) != Base::nodes.end()) {
      return repatoms
          .nodal_data_points[Base::nodes.at(node_key).data.mesh_index];
    }
    if (halo_node_nodal_data.find(node_key) != halo_node_nodal_data.end()) {
      return halo_node_nodal_data.at(node_key);
    }
    throw exception::QCException{"get_nodal_data_of_node: node key found "
                                 "neither in local nor in halo mesh"};
  }

  void exchange_overlapping_deformation() {
    exchange_overlap_containers(overlapping_mesh_data, false);
    exchange_overlap_containers(periodic_mesh_data, false);
  }

  void exchange_overlapping_multispecies_info() {
    exchange_overlap_containers(overlapping_mesh_data, true);
    exchange_overlap_containers(periodic_mesh_data, true);
  }

  template <typename ContainerType>
  void exchange_overlap_containers(ContainerType &overlap_container,
                                   const bool exchange_concentration_info) {
    const auto world = boost::mpi::communicator{};
    const auto mpi_size = world.size();
    const auto mpi_rank = world.rank();

    std::vector<std::vector<double>> overlapping_double_send(mpi_size);

    std::array<double, D> mesh_point{};

#ifdef FINITE_TEMPERATURE
    const auto nidof = NIDOF;
#else
    const auto nidof = 0;
#endif

    constexpr std::size_t NOI = traits::N_IMPURITIES<NodalData>;

    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      if (i_proc != mpi_rank) {
        if (exchange_concentration_info) {
          const auto send_size =
              overlap_container.overlap_node_idxs[i_proc].size() * 3 * NOI;
          overlapping_double_send[i_proc].reserve(send_size);
        } else {
          const auto send_size =
              overlap_container.overlap_node_idxs[i_proc].size() * D +
              overlap_container.overlap_simplex_idxs[i_proc].size() * (D * D) +
              overlap_container.overlap_simplex_idxs[i_proc].size() *
                  (noof_offset_vectors * D) +
              overlap_container.overlap_node_idxs[i_proc].size() * nidof;
          overlapping_double_send[i_proc].reserve(send_size);
        }

        if (exchange_concentration_info) {
          for (const auto idx : overlap_container.overlap_node_idxs[i_proc]) {
            for (std::size_t k = 0; k < NOI; k++) {
              overlapping_double_send[i_proc].push_back(
                  repatoms.nodal_data_points[idx].impurity_concentrations[k]);
              overlapping_double_send[i_proc].push_back(
                  repatoms.nodal_data_points[idx]
                      .impurity_concentration_update_rates[k]);
              overlapping_double_send[i_proc].push_back(
                  repatoms.nodal_data_points[idx].chemical_potentials[k]);
            }
          }
        } else {
          for (const auto idx : overlap_container.overlap_node_idxs[i_proc]) {
            mesh_point = mesh_point_from_index(*this, idx);

            for (std::size_t d = 0; d < D; d++) {
              overlapping_double_send[i_proc].push_back(mesh_point[d]);
            }
#ifdef FINITE_TEMPERATURE
            for (std::size_t d = 0; d < NIDOF; d++) {
              overlapping_double_send[i_proc].push_back(
                  repatoms.thermal_coordinates[idx][d]);
            }
#endif
          }

          for (const auto idx :
               overlap_container.overlap_simplex_idxs[i_proc]) {
            const auto &cell = this->cells.at(this->simplex_index_to_key[idx]);
            for (std::size_t m = 0; m < D; m++) {
              for (std::size_t n = 0; n < D; n++) {
                const auto lattice_vectors = cell.data.lattice_vectors;
                overlapping_double_send[i_proc].push_back(
                    lattice_vectors[m][n]);
              }
            }
            for (std::size_t m = 0; m < noof_offset_vectors; m++) {
              for (std::size_t n = 0; n < D; n++) {
                overlapping_double_send[i_proc].push_back(
                    cell.data.offset_vectors[m][n]);
              }
            }
          }
        }
      }
    }

    const auto overlapping_double_recv =
        qcmesh::mpi::all_scatter(overlapping_double_send);

    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      std::size_t recvd_data_ctr = 0;

      if (i_proc != mpi_rank) {
        if (exchange_concentration_info) {
          for (std::size_t idx = 0;
               idx < overlap_container.overlapping_nodes_recv[i_proc].size();
               idx++) {
            for (std::size_t k = 0; k < NOI; k++) {
              overlap_container.overlapping_nodal_data_recv[i_proc][idx]
                  .impurity_concentrations[k] =
                  overlapping_double_recv[i_proc][recvd_data_ctr];
              recvd_data_ctr++;
              overlap_container.overlapping_nodal_data_recv[i_proc][idx]
                  .impurity_concentration_update_rates[k] =
                  overlapping_double_recv[i_proc][recvd_data_ctr];
              recvd_data_ctr++;
              overlap_container.overlapping_nodal_data_recv[i_proc][idx]
                  .chemical_potentials[k] =
                  overlapping_double_recv[i_proc][recvd_data_ctr];
              recvd_data_ctr++;
            }
          }
        } else {
          for (std::size_t idx = 0;
               idx < overlap_container.overlapping_nodes_recv[i_proc].size();
               idx++) {

            for (std::size_t d = 0; d < D; d++) {
              overlap_container.overlapping_nodes_recv[i_proc][idx][d] =
                  overlapping_double_recv[i_proc][recvd_data_ctr];
              recvd_data_ctr++;
            }
#ifdef FINITE_TEMPERATURE
            for (std::size_t d = 0; d < NIDOF; d++) {
              overlap_container.overlapping_thermal_dofs_recv[i_proc][idx][d] =
                  overlapping_double_recv[i_proc][recvd_data_ctr];
              recvd_data_ctr++;
            }
#endif
          }

          for (std::size_t idx = 0;
               idx <
               overlap_container.overlapping_simplices_recv[i_proc].size();
               idx++) {
            for (std::size_t m = 0; m < D; m++) {
              for (std::size_t n = 0; n < D; n++) {
                overlap_container
                    .overlapping_element_lattice_vectors_recv[i_proc][idx][m]
                                                             [n] =
                    overlapping_double_recv[i_proc][recvd_data_ctr];
                recvd_data_ctr++;
              }
            }
            for (std::size_t m = 0; m < noof_offset_vectors; m++) {
              for (std::size_t n = 0; n < D; n++) {
                overlap_container
                    .overlapping_element_offsets_recv[i_proc][idx][m][n] =
                    overlapping_double_recv[i_proc][recvd_data_ctr];
                recvd_data_ctr++;
              }
            }
          }
        }
      }
    }
  }

  void reinitialize_exchange_vectors() {
    reinitialize_overlap_vectors(overlapping_mesh_data);
    reinitialize_overlap_vectors(periodic_mesh_data);
  }
  // ! reinitialize the overlapping communication vectors
  template <typename ContainerType>
  void reinitialize_overlap_vectors(ContainerType &container) {
    const auto mpi_size = boost::mpi::communicator{}.size();

    container.overlap_simplex_idxs.clear();
    container.overlap_simplex_idxs_sent_flag.clear();
    container.overlap_node_idxs.clear();
    container.overlap_nodes_sent_idx_recv_side.clear();
    container.overlapping_nodes_recv.clear();
    container.overlapping_simplices_recv.clear();
    container.overlapping_element_lattice_vectors_recv.clear();
    container.overlapping_element_offsets_recv.clear();
    container.overlapping_simplices_recv_atomistic_flag.clear();
    container.overlapping_node_material_ids_recv.clear();
    container.overlapping_node_site_types_recv.clear();

    container.overlap_simplex_idxs.resize(mpi_size);
    container.overlap_simplex_idxs_sent_flag.resize(mpi_size);
    for (int i_proc = 0; i_proc < mpi_size; i_proc++) {
      container.overlap_simplex_idxs_sent_flag[i_proc].resize(
          Base::cells.size(), false);
      container.overlap_simplex_idxs[i_proc].reserve(Base::cells.size());
    }

    container.overlap_node_idxs.resize(mpi_size);
    container.overlapping_node_material_ids_recv.resize(mpi_size);
    container.overlapping_node_site_types_recv.resize(mpi_size);
    container.overlapping_nodes_recv.resize(mpi_size);
    container.overlap_nodes_sent_idx_recv_side.resize(mpi_size);
    container.overlapping_simplices_recv.resize(mpi_size);
    container.overlapping_element_lattice_vectors_recv.resize(mpi_size);
    container.overlapping_element_offsets_recv.resize(mpi_size);
    container.overlapping_simplices_recv_atomistic_flag.resize(mpi_size);

#ifdef FINITE_TEMPERATURE
    container.overlapping_thermal_dofs_recv.clear();
    container.overlapping_thermal_dofs_recv.resize(mpi_size);
#endif
    container.overlapping_nodal_data_recv.clear();
    container.overlapping_nodal_data_recv.resize(mpi_size);
  }
};

template <std::size_t D, std::size_t K, class Mesh>
double cell_density(const qcmesh::mesh::SimplexCell<K, QCCellData<D, K>> &cell,
                    const Mesh &mesh) {
  auto density = 0.0;
  for (const auto node_id : cell.nodes) {
    const auto material_idx =
        mesh.material_idx_of_nodes[mesh.nodes.at(node_id).data.mesh_index];
    const auto &material = mesh.materials[material_idx];
    density += materials::get_material_density(material.lattice_parameter_vec,
                                               material.lattice_structure);
  }
  return density / static_cast<double>(K + 1);
}

template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
geometry::Point<D>
mesh_point_from_index(const QCMesh<D, K, NIDOF, NodalData> &mesh,
                      const std::size_t index) {
  if (mesh.node_index_to_key.size() <= index)
    throw exception::QCException{"mesh_point_from_index: index out of bounds"};
  return mesh.nodes.at(mesh.node_index_to_key[index]).position;
}

/**
 * @brief get jth incident element for vertex as vid
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
qcmesh::mesh::SimplexCellId
incident_element(const QCMesh<D, K, NIDOF, NodalData> &mesh,
                 const std::size_t vid, const std::size_t j) {
  return *(mesh.vertex_adjacency_pointers[vid] + j);
}

/**
 * @brief get number of incident elements to vertex at vid
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
std::size_t n_incident_elements(const QCMesh<D, K, NIDOF, NodalData> &mesh,
                                const std::size_t vid) {
  return std::size_t(mesh.vertex_adjacency_pointers[vid + 1] -
                     mesh.vertex_adjacency_pointers[vid]);
}

/**
 * @brief return indexed simplex with node Indices from id
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
std::array<std::size_t, K + 1>
indexed_simplex(QCMesh<D, K, NIDOF, NodalData> &mesh,
                const qcmesh::mesh::SimplexCellId id) {
  std::array<std::size_t, K + 1> s{};
  for (std::size_t d = 0; d < K + 1; d++)
    s[d] = mesh.nodes.at(mesh.cells.at(id).nodes[d]).data.mesh_index;
  return s;
}

template <std::size_t D, class NodalData>
std::vector<qcmesh::mesh::NodeId> build_node_index_key_maps(
    const std::unordered_map<qcmesh::mesh::NodeId,
                             qcmesh::mesh::Node<D, NodalData>> &nodes) {
  auto node_index_to_key = std::vector<qcmesh::mesh::NodeId>{};
  node_index_to_key.reserve(nodes.size());

  for (const auto &[node_id, _] : nodes)
    node_index_to_key.emplace_back(node_id);

  // not much point sorting this because
  // nodes need to have a specific order
  // which is dictated by PETSC.
  std::sort(node_index_to_key.begin(), node_index_to_key.end());
  return node_index_to_key;
}

extern template class QCMesh<3, 3, 2, NodalDataMultispecies<1>>;
extern template class QCMesh<3, 3, 2, NodalDataMultispecies<2>>;
extern template class QCMesh<3, 3, 2, NodalDataMultispecies<3>>;
extern template class QCMesh<3, 3, 2, NodalDataMultispecies<4>>;
extern template class QCMesh<3, 3, 2, NodalDataMultispecies<5>>;

} // namespace meshing

template <std::size_t NIDOF, typename... MeshParameters>
struct meshing::traits::Partition<
    meshing::QCMesh<3, 3, NIDOF, MeshParameters...>> {

  static std::tuple<std::vector<std::size_t>, std::vector<std::size_t>>
  partition_ghost_indices(
      const meshing::QCMesh<3, 3, NIDOF, MeshParameters...> &mesh) {
    return ::meshing::partition_ghost_indices(mesh);
  }
};
template <std::size_t NIDOF, typename... MeshParameters>
struct meshing::traits::CountRepatoms<
    meshing::QCMesh<3, 3, NIDOF, MeshParameters...>> {
  static std::size_t
  count_repatoms(const meshing::QCMesh<3, 3, NIDOF, MeshParameters...> &mesh) {
    return ::meshing::count_repatoms(mesh);
  }
};
