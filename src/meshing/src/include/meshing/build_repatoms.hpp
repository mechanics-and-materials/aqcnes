// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to build repatoms
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/mesh/node.hpp"
#include <cmath>
#include <optional>
#include <unordered_map>

template <class Mesh>
auto initial_fixed_flags(
    const Mesh &mesh, const std::optional<std::array<std::array<double, 3>, 2>>
                          &fixed_box_boundaries) {
  auto flags = std::unordered_map<qcmesh::mesh::NodeId, int>{};
  if (!fixed_box_boundaries.has_value())
    for (const auto &[node_id, _] : mesh.nodes)
      flags.insert({node_id, false});
  else {
    const auto fixed_box = qcmesh::geometry::Box<Mesh::DIMENSION>{
        fixed_box_boundaries.value()[0], fixed_box_boundaries.value()[1]};
    for (const auto &[node_id, node] : mesh.nodes)
      flags.insert(
          {node_id, qcmesh::geometry::contains(fixed_box, node.position)});
  }
  return flags;
}

template <class Mesh> auto extract_site_types(Mesh &mesh) {
  auto lattice_site_types = std::unordered_map<qcmesh::mesh::NodeId, int>{};
  for (std::size_t i_nd = 0; i_nd < mesh.nodes.size(); i_nd++)
    lattice_site_types.insert(
        std::make_pair(mesh.node_index_to_key[i_nd],
                       mesh.nodes.at(mesh.node_index_to_key[i_nd]).data.type));
  return lattice_site_types;
}

template <typename> struct ArraySize;
template <typename T, std::size_t N> struct ArraySize<std::array<T, N>> {
  static std::size_t const size = N;
};

template <class Mesh, class NodalData>
void build_repatoms(Mesh &mesh, [[maybe_unused]] const double phi_ref,
                    [[maybe_unused]] const double t_ref,
                    const NodalData &data_ref,
                    const std::optional<std::array<std::array<double, 3>, 2>>
                        &fixed_box_boundaries = std::nullopt) {

#ifdef FINITE_TEMPERATURE
  constexpr std::size_t NIDOF = ArraySize<typename Mesh::ThermalPoint>::size;
  auto initial_thermal_conditions =
      std::unordered_map<qcmesh::mesh::NodeId, std::array<double, NIDOF>>{};
  auto initial_entropies = std::unordered_map<qcmesh::mesh::NodeId, double>{};
  for (std::size_t i_nd = 0; i_nd < mesh.nodes.size(); i_nd++) {
    const auto thermal_point = std::array<double, NIDOF>{
        -log(constants::k_Boltzmann * t_ref), phi_ref};

    initial_thermal_conditions.insert(
        std::make_pair(mesh.node_index_to_key[i_nd], thermal_point));

    const auto t = thermal_point[0] + thermal_point[1];
    const auto entropy = double{-1.5 * constants::k_Boltzmann * t};

    initial_entropies.insert(
        std::make_pair(mesh.node_index_to_key[i_nd], entropy));
  }
#endif

  auto initial_nodal_data =
      std::unordered_map<qcmesh::mesh::NodeId, NodalData>{};
  for (std::size_t i_nd = 0; i_nd < mesh.nodes.size(); i_nd++) {
    initial_nodal_data.insert(
        std::make_pair(mesh.node_index_to_key[i_nd], data_ref));
  }

  const auto lattice_site_types = extract_site_types(mesh);
  const auto fixed_flags = initial_fixed_flags(mesh, fixed_box_boundaries);

  mesh.build_repatoms(fixed_flags, lattice_site_types
#ifdef FINITE_TEMPERATURE
                      ,
                      initial_thermal_conditions, initial_entropies
#endif
                      ,
                      initial_nodal_data);
}
