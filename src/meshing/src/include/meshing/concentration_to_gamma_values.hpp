// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to get chemical potentials from concentrations
 *        and vice-versa
 * @author S. Saxena
 */

#pragma once
#include <array>
#include <cmath>

namespace meshing {

template <std::size_t NOI>
std::array<double, NOI>
get_gamma_values(const std::array<double, NOI> &concentration_values,
                 const bool simulating_vacancy) {
  auto gamma_values = std::array<double, NOI>{};
  auto conc_sum = 0.0;
  for (const auto concentration : concentration_values)
    conc_sum += concentration;
  const auto solvent_conc = 1.0 - conc_sum;

  if (simulating_vacancy) {
    gamma_values[0] = log(solvent_conc / concentration_values[0]);
    for (std::size_t si = 1; si < NOI; si++)
      gamma_values[si] =
          log(concentration_values[si] / concentration_values[0]);
  } else {
    auto conc_prod = 1.0;
    for (const auto concentration : concentration_values)
      conc_prod *= concentration;
    const auto to_subtract = (1.0 / (1 + NOI)) * std::log(conc_prod);
    for (std::size_t si = 0; si < NOI; si++)
      gamma_values[si] = std::log(concentration_values[si]) - to_subtract;
  }
  return gamma_values;
}

template <std::size_t NOI>
std::array<double, NOI>
get_concentration_values(const std::array<double, NOI> &gamma_values,
                         const bool simulating_vacancy) {
  auto concentration_values = std::array<double, NOI>{};
  auto denom = 0.0;
  for (const auto gamma : gamma_values)
    denom += exp(gamma);
  if (simulating_vacancy) {
    denom += 1.0;
    concentration_values[0] = 1.0 / denom;
    for (std::size_t si = 1; si < NOI; si++)
      concentration_values[si] =
          exp(gamma_values[si]) * concentration_values[0];
  } else {
    for (std::size_t si = 0; si < NOI; si++)
      concentration_values[si] = exp(gamma_values[si]) / denom;
  }
  return concentration_values;
}

} // namespace meshing
