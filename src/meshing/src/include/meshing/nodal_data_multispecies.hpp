// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct implementation to hold chemical information of multiple species
 * @author S. Saxena
 */

#pragma once
#include "meshing/traits.hpp"
#include <array>

namespace meshing {

template <std::size_t NOI> struct NodalDataMultispecies {
  using ConcentrationPoint = std::array<double, NOI>;

  ConcentrationPoint impurity_concentrations = ConcentrationPoint{};

  /*
  If the first impurity is a vacancy, the first entry of the impurity_concentration_update_rates
   and chemical_potentials corresponds to the solvent, else it corresponds to the impurities themselves.
   In the second case, the values for the solvent are just negative summations of the values for all solutes.
  */
  ConcentrationPoint impurity_concentration_update_rates = ConcentrationPoint{};
  ConcentrationPoint chemical_potentials = ConcentrationPoint{};
  ConcentrationPoint gamma_values = ConcentrationPoint{};
};

/**
 * @brief Implementation of @ref traits::NImpurities for @ref NodalDataMultispecies.
 */
template <std::size_t Noi>
struct traits::NImpurities<NodalDataMultispecies<Noi>> {
  constexpr static std::size_t VALUE = Noi;
};

/**
 * @brief Implementation of @ref traits::ImpurityConcentrations for @ref NodalDataMultispecies.
 */
template <std::size_t NOI>
struct traits::ImpurityConcentrations<NodalDataMultispecies<NOI>> {
  static const std::array<double,
                          traits::N_IMPURITIES<NodalDataMultispecies<NOI>>> &
  impurity_concentrations(const NodalDataMultispecies<NOI> &data) {
    return data.impurity_concentrations;
  }
  static std::array<double, traits::N_IMPURITIES<NodalDataMultispecies<NOI>>> &
  impurity_concentrations(NodalDataMultispecies<NOI> &data) {
    return data.impurity_concentrations;
  }
};

} // namespace meshing
