// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief PetSc-based vector distribution for ghosts
 * @authors P. Gupta
 */

#pragma once

#include <vector>

namespace meshing {

void distribute_vec(const std::size_t n_ghosts, const std::size_t n_locals,
                    const std::size_t n_repatoms,
                    const std::vector<int> &ghosts, std::vector<int> &vec);
void distribute_vec(const std::size_t n_ghosts, const std::size_t n_locals,
                    const std::size_t n_repatoms,
                    const std::vector<int> &ghosts, std::vector<double> &vec);
} // namespace meshing
