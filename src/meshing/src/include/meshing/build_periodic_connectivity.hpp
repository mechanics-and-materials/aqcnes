// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Connect cells to their images accross periodic boundaries
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "geometry/box.hpp"
#include "geometry/point.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/indexed.hpp"
#include "qcmesh/geometry/octree.hpp"
#include "qcmesh/mesh/node.hpp"
#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>

namespace meshing {

/**
 * @brief Build periodic connectivity
 */
template <std::size_t D, class Mesh>
void build_periodic_connectivity(Mesh &mesh) {
  namespace array_ops = qcmesh::array_ops;

  const auto world = boost::mpi::communicator{};
  const auto mpi_size = world.size();
  const auto mpi_rank = world.rank();
  constexpr double TOLERANCE = 0.1;

  auto node_periodic_images = std::unordered_map<
      std::size_t,
      std::vector<std::tuple<int, qcmesh::mesh::NodeId, geometry::Point<D>>>>{};

  mesh.original_keys_of_periodic_images.clear();
  mesh.offsets_of_periodic_images.clear();

  // affects
  // 1. calculation of weights
  // 2. calculation of neighbors and forces

  // build similar to interproc connectivity.

  auto periodic_boundary_node_ids = std::vector<qcmesh::mesh::NodeId>{};
  auto periodic_boundary_nodes = std::vector<geometry::Point<D>>{};

  periodic_boundary_node_ids.reserve(mesh.nodes.size());
  periodic_boundary_nodes.reserve(mesh.nodes.size());

  for (auto &[node_id, node] : mesh.nodes) {
    // determine which nodes lie on boundary

    for (std::size_t d = 0; d < D && !node.data.periodic_boundary_flag; d++) {
      // here periodic boundary nodes are identified for a new mesh. This works
      // only for flat boundaries right now. A smarter way is needed to deal with
      // curved periodic boundaries from the beginning
      if (mesh.periodic_flags[d]) {
        if ((abs(node.position[d] - mesh.domain_bound.lower[d]) < 1.0e-4) ||
            (abs(node.position[d] - mesh.domain_bound.upper[d]) < 1.0e-4)) {
          node.data.periodic_boundary_flag = true;
        }
      }
    }

    if (node.data.periodic_boundary_flag) {
      periodic_boundary_node_ids.push_back(node_id);
      periodic_boundary_nodes.push_back(node.position);
    }
  }

  auto periodic_boundary_ids_by_proc =
      std::vector<std::vector<qcmesh::mesh::NodeId>>(mpi_size);
  auto periodic_boundary_positions_by_proc =
      std::vector<std::vector<std::array<double, D>>>(mpi_size);

  boost::mpi::all_gather(world, periodic_boundary_node_ids,
                         periodic_boundary_ids_by_proc);
  boost::mpi::all_gather(world, periodic_boundary_nodes,
                         periodic_boundary_positions_by_proc);
  auto n_total_boundary_nodes = std::size_t{0};
  for (const auto &positions : periodic_boundary_positions_by_proc)
    n_total_boundary_nodes += positions.size();
  if (mpi_rank == 0 && n_total_boundary_nodes == 0) {
    throw exception::QCException{
        "Perodic simulation without any boundary nodes. Please check the "
        "periodic lengths provided"};
  }

  auto boundary_nodes_bounds =
      qcmesh::geometry::bounding_box(periodic_boundary_positions_by_proc[0]);
  for (int i = 1; i < mpi_size; i++)
    geometry::BoxUnion<D>{}(
        boundary_nodes_bounds,
        qcmesh::geometry::bounding_box(periodic_boundary_positions_by_proc[i]));

  auto unique_boundary_nodes = qcmesh::geometry::octree::Octree<
      qcmesh::geometry::Indexed<std::array<double, D>>>(boundary_nodes_bounds);

  auto unique_periodic_keys = std::vector<qcmesh::mesh::NodeId>{};
  unique_periodic_keys.reserve(n_total_boundary_nodes);

  // 1. Gather all nodes on boundaries.
  //  a. MPI_Gather(boundary nodes)
  //  b. get unique boundary nodes
  auto ranks_of_unique_nodes = std::vector<std::vector<int>>{};
  ranks_of_unique_nodes.reserve(n_total_boundary_nodes);

  if (periodic_boundary_ids_by_proc.size() !=
      periodic_boundary_positions_by_proc.size())
    throw exception::QCException{
        "Mismatching size of periodic_boundary_ids_by_proc and "
        "periodic_boundary_positions_by_proc"};

  for (int node_rank = 0; node_rank < mpi_size; node_rank++) {
    for (std::size_t node_idx = 0;
         node_idx < periodic_boundary_ids_by_proc[node_rank].size();
         node_idx++) {
      auto collisions = unique_boundary_nodes.find_or_insert(
          qcmesh::geometry::Indexed<std::array<double, D>>{
              unique_boundary_nodes.size(),
              periodic_boundary_positions_by_proc[node_rank][node_idx]},
          TOLERANCE);

      if (collisions.empty()) {
        unique_periodic_keys.push_back(
            periodic_boundary_ids_by_proc[node_rank][node_idx]);

        ranks_of_unique_nodes.emplace_back(std::vector<int>{node_rank});
      } else
        for (const auto &collision : collisions)
          ranks_of_unique_nodes[collision.index].push_back(node_rank);
    }
  }

  std::size_t node_index = 0;
  int image_index = 0;
  geometry::Point<D> periodic_offset;

  geometry::Point<D> node_periodic_point;

  int n_periods =
      std::count(mesh.periodic_flags.begin(), mesh.periodic_flags.end(), true);

  std::vector<std::array<int, D>> periodic_offsets;

  periodic_offsets.resize(0);

  if (n_periods == 1) {
    periodic_offsets.resize(1);
  } else if (n_periods == 2) {
    periodic_offsets.resize(3);
  } else if (n_periods == 3) {
    periodic_offsets.resize(7);
  }

  for (std::size_t n = 0; n < periodic_offsets.size(); n++) {
    periodic_offsets[n].fill(0);
  }

  if (n_periods == 1) {
    std::size_t d{};

    for (std::size_t i = 0; i < D; i++)
      if (mesh.periodic_flags[i]) {
        d = i;
        break;
      }

    periodic_offsets[0][d] = -1;
  } else if (n_periods == 2) {
    // total 3 cases
    std::size_t d{};
    std::size_t e{};

    for (std::size_t i = 0; i < D; i++) {
      if (mesh.periodic_flags[i]) {
        d = i;
        break;
      }
    }

    for (std::size_t i = 0; i < D; i++)
      if (mesh.periodic_flags[i] && i != d) {
        e = i;
        break;
      }

    periodic_offsets[0][d] = -1;
    periodic_offsets[1][e] = -1;

    periodic_offsets[2][d] = -1;
    periodic_offsets[2][e] = -1;

  }

  else if (n_periods == 3) {
    for (std::size_t d = 0; d < D; d++) {
      periodic_offsets[d][d] = -1;

      periodic_offsets[D + d][d] = -1;
      periodic_offsets[D + d][(d + 1) % D] = -1;
    }

    periodic_offsets[6].fill(-1);
  }

  // 2. To find the original node key, first find all images
  for (std::size_t boundary_idx = 0;
       boundary_idx < periodic_boundary_node_ids.size(); boundary_idx++) {
    periodic_offset.fill(0.0);

    const auto node_point = periodic_boundary_nodes[boundary_idx];
    const auto node_key = static_cast<qcmesh::mesh::NodeId>(
        periodic_boundary_node_ids[boundary_idx]);

    node_index = mesh.nodes.at(node_key).data.mesh_index;

    node_periodic_images.insert(
        {node_index,
         std::vector<
             std::tuple<int, qcmesh::mesh::NodeId, geometry::Point<D>>>{}});

    for (std::size_t offset_index = 0; offset_index < periodic_offsets.size();
         offset_index++) {
      for (std::size_t d = 0; d < D; d++) {
        periodic_offset[d] = double(periodic_offsets[offset_index][d]) *
                             mesh.periodic_lengths[d];
      }

      // always get the periodic points with lesser coordinate
      array_ops::as_eigen(node_periodic_point) =
          array_ops::as_eigen(node_point) +
          array_ops::as_eigen(periodic_offset);

      auto periodic_images =
          unique_boundary_nodes.find(node_periodic_point, TOLERANCE);

      for (const auto image : periodic_images) {
        image_index = image.index;

        // just add smallest rank now so we don't maintain multiple periodic
        // points with same coordinate
        std::size_t add_rank_index = 0;

        for (std::size_t rank_index = 1;
             rank_index < ranks_of_unique_nodes[image_index].size();
             rank_index++) {
          if (ranks_of_unique_nodes[image_index][rank_index] <
              ranks_of_unique_nodes[image_index][add_rank_index]) {
            add_rank_index = rank_index;
          }
        }

        node_periodic_images.at(node_index)
            .push_back({ranks_of_unique_nodes[image_index][add_rank_index],
                        unique_periodic_keys[image_index], image.inner});
      }
    }
  }

  // 3. find the original key from the images
  for (const auto &[node_idx, links] : node_periodic_images) {
    if (links.size() != 0) {
      auto max_image_distance = std::numeric_limits<double>::min();
      auto farthest_index = std::size_t{};
      auto found_farthest_index = false;

      const auto node_point = mesh_point_from_index(mesh, node_idx);

      for (std::size_t image_idx = 0; image_idx < links.size(); image_idx++) {
        const auto image_distance =
            array_ops::distance(node_point, std::get<2>(links[image_idx]));
        if ((image_distance + 1.0e-4) >= max_image_distance) {
          max_image_distance = image_distance;
          found_farthest_index = true;
          farthest_index = image_idx;
        }
      }

      if (!found_farthest_index)
        throw exception::QCException{
            "Original key not found from periodic images"};

      mesh.original_keys_of_periodic_images.insert(
          {node_idx,
           static_cast<std::size_t>(std::get<1>(links[farthest_index]))});
      auto temp = std::array<double, 3>{};
      array_ops::as_eigen(temp) =
          array_ops::as_eigen(node_point) -
          array_ops::as_eigen(std::get<2>(links[farthest_index]));
      mesh.offsets_of_periodic_images.insert(std::make_pair(node_idx, temp));
    }
  }
}

} // namespace meshing
