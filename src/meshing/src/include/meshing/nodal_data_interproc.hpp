// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Implementation of functions for concentration based interpolation
 * @author S. Saxena
 */

#pragma once
#include "meshing/nodal_data_multispecies.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mesh/empty_data.hpp"
#include <vector>

namespace array_ops = qcmesh::array_ops;

namespace meshing {

/**
 * @brief interpolate function for different types of nodal data
 */
template <typename NodalData> struct InterpolateNodalData {
  static void interpolate(const NodalData &, NodalData &, const double);
};

template <std::size_t NOI>
struct InterpolateNodalData<NodalDataMultispecies<NOI>> {
  static void interpolate(const NodalDataMultispecies<NOI> &data_vertex,
                          NodalDataMultispecies<NOI> &data_interpolated,
                          const double weight) {
    array_ops::as_eigen(data_interpolated.impurity_concentrations) +=
        array_ops::as_eigen(data_vertex.impurity_concentrations) * weight;
  }
};

template <> struct InterpolateNodalData<qcmesh::mesh::EmptyData> {
  static void interpolate(const qcmesh::mesh::EmptyData &,
                          qcmesh::mesh::EmptyData &, const double) {}
};

} // namespace meshing
