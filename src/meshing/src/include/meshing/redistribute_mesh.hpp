// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief repartitioning of mesh from a poor partitioning.
 *
 * Responsible for all the qc data movement
 * @authors P. Gupta, S. Saxena, G. Bräunlich
 */

#pragma once

#include "meshing/create_mesh.hpp"
#include "meshing/nodal_thermal_data.hpp"
#include "meshing/qc_cell_data.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/qc_nodal_data.hpp"
#include "meshing/renumber_mesh.hpp"
#include "meshing/repatom_data.hpp"
#include "meshing/serialization/nodal_thermal_data.hpp"
#include "meshing/serialization/repatom_data.hpp"
#include "qcmesh/mesh/empty_data.hpp"
#include "qcmesh/mesh/serialization/empty_data.hpp"
#include "qcmesh/mesh/serialization/node.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "qcmesh/mesh/traits/transfer_data.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <boost/serialization/vector.hpp>

namespace meshing {

/**
 * @brief Nodal data type for exchanging nodes between mpi processes.
 */
template <class ThermalData, class MultiSpeciesDataType>
struct PackedNodalData {
  std::size_t material_id{};
  double weight{};
  int type{};
  bool periodic_boundary_flag{};
  repatoms::RepatomData<ThermalData, MultiSpeciesDataType> repatom_data{};
};

} // namespace meshing

// Boost serialization for meshing::PackedNodalData
namespace boost::serialization {
template <typename Archive, class MultiSpeciesDataType, class ThermalData>
void serialize(
    Archive &ar,
    meshing::PackedNodalData<MultiSpeciesDataType, ThermalData> &data,
    const unsigned int) {
  ar &data.material_id;
  ar &data.weight;
  ar &data.type;
  ar &data.periodic_boundary_flag;
  ar &data.repatom_data;
}
} // namespace boost::serialization

template <class ThermalData, class MultiSpeciesDataType>
struct boost::serialization::tracking_level<
    meshing::PackedNodalData<ThermalData, MultiSpeciesDataType>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<meshing::PackedNodalData<
                        ThermalData, MultiSpeciesDataType>>,
                    mpl::int_<primitive_type>>::value));
};

template <class ThermalData, class MultiSpeciesDataType>
struct boost::serialization::implementation_level<
    meshing::PackedNodalData<ThermalData, MultiSpeciesDataType>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <class ThermalData, class MultiSpeciesDataType>
struct boost::mpi::is_mpi_datatype<
    meshing::PackedNodalData<ThermalData, MultiSpeciesDataType>>
    : public mpl::true_ {};

namespace meshing {

/**
 * @brief Cell data type for exchanging nodes between mpi processes.
 */
template <std::size_t K> struct PackedCellData {
  double volume = 0.;
  double quality = 1.0e9;
  double jacobian = 1.0;
  double density = 0.0;
  double weight{};
  std::array<std::array<double, K>, K> lattice_vectors{};
  std::size_t material_index{};
  bool is_atomistic = false;
  bool is_mono_lattice = false;
};

} // namespace meshing

// Boost serialization for meshing::PackedNodalData
namespace boost::serialization {
template <typename Archive, std::size_t K>
void serialize(Archive &ar, meshing::PackedCellData<K> &data,
               const unsigned int) {
  ar &data.volume;
  ar &data.quality;
  ar &data.jacobian;
  ar &data.density;
  ar &data.weight;
  ar &data.lattice_vectors;
  ar &data.material_index;
  ar &data.is_atomistic;
  ar &data.is_mono_lattice;
}
} // namespace boost::serialization

template <std::size_t K>
struct boost::serialization::tracking_level<meshing::PackedCellData<K>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<meshing::PackedCellData<K>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t K>
struct boost::serialization::implementation_level<meshing::PackedCellData<K>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <std::size_t K>
struct boost::mpi::is_mpi_datatype<meshing::PackedCellData<K>>
    : public mpl::true_ {};

/**
 * @brief Implementation of @ref qcmesh::mesh::traits::TransferData for @ref meshing::PackedCellData -> @ref meshing::QCCellData.
 */
template <std::size_t D, std::size_t K>
struct qcmesh::mesh::traits::TransferData<meshing::PackedCellData<K>,
                                          meshing::QCCellData<D, K>> {
  static void transfer_data(const meshing::PackedCellData<K> &from,
                            meshing::QCCellData<D, K> &to) {
    to.volume = from.volume;
    to.quality = from.quality;
    to.jacobian = from.jacobian;
    to.density = from.density;
    to.lattice_vectors = from.lattice_vectors;
    to.is_atomistic = from.is_atomistic;
    to.material_index = static_cast<int>(from.material_index);
    to.is_mono_lattice = from.is_mono_lattice;
  }
};

/**
 * @brief Implementation of @ref qcmesh::mesh::traits::TransferData for @ref meshing::QCCellData -> @ref meshing::PackedCellData.
 */
template <std::size_t D, std::size_t K>
struct qcmesh::mesh::traits::TransferData<meshing::QCCellData<D, K>,
                                          meshing::PackedCellData<K>> {
  static void transfer_data(const meshing::QCCellData<D, K> &from,
                            meshing::PackedCellData<K> &to) {
    to.volume = from.volume;
    to.quality = from.quality;
    to.jacobian = from.jacobian;
    to.density = from.density;
    to.lattice_vectors = from.lattice_vectors;
    to.is_atomistic = from.is_atomistic;
    to.material_index = from.material_index;
    to.is_mono_lattice = from.is_mono_lattice;
  }
};

namespace meshing {

/**
 * @brief C++ wrapper around the parmetis routine ParMETIS_V3_PartKway.
 *
 * Encapsulated in the cpp file.
 */
std::vector<int> repartition_cells_metis(
    std::vector<int> &&cell_adjacency, std::vector<int> &&cell_start_indices,
    const std::vector<std::size_t> &current_partition_sizes);

/**
 * @brief Prepare a reindexing of cells which can be used by @ref repartition_cells_metis.
 *
 * @return A map mapping cell ids to indices such that the indices are
 * - contiguous within each process,
 * - contiguous over all processes,
 * - unique over all processes.
 */
template <std::size_t K, class CellData>
std::unordered_map<qcmesh::mesh::SimplexCellId, std::size_t>
build_csr_index_map(
    const std::unordered_map<qcmesh::mesh::SimplexCellId,
                             qcmesh::mesh::SimplexCell<K, CellData>> &cells,
    const std::vector<std::size_t> &n_cells_per_process) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = static_cast<std::size_t>(world.rank());
  const auto mpi_size = n_cells_per_process.size();
  auto foreign_ids = std::unordered_set<qcmesh::mesh::SimplexCellId>{};
  for (const auto &[_, cell] : cells)
    for (const auto &n : cell.neighbors)
      if (n.has_value() && cells.find(n.value()) == cells.end())
        foreign_ids.emplace(n.value());
  auto cell_index_ctr = std::size_t{0};
  for (std::size_t i = 0; i < mpi_rank; i++)
    cell_index_ctr += n_cells_per_process[i];
  auto foreign_ids_per_process =
      std::vector<std::unordered_set<qcmesh::mesh::SimplexCellId>>(mpi_size);
  boost::mpi::all_gather(world, foreign_ids, foreign_ids_per_process);
  auto id_map = std::unordered_map<qcmesh::mesh::SimplexCellId, std::size_t>{};
  for (const auto &[cell_id, _] : cells)
    id_map.emplace(cell_id, cell_index_ctr++);
  auto ids_for_procs = std::vector<
      std::vector<std::tuple<qcmesh::mesh::SimplexCellId, std::size_t>>>(
      mpi_size);
  for (std::size_t i = 0; i < mpi_size; i++)
    if (i != mpi_rank)
      for (const auto id : foreign_ids_per_process[i])
        if (id_map.find(id) != id_map.end())
          ids_for_procs[i].push_back({id, id_map.at(id)});
  const auto ids_from_procs = qcmesh::mpi::all_scatter(ids_for_procs);
  for (const auto &ids : ids_from_procs)
    for (const auto &[id, idx] : ids)
      id_map.emplace(id, idx);
  return id_map;
}

/**
 * @brief Build a repartition table for distributed cells.
 *
 * Uses @ref build_csr_index_map and @ref repartition_cells_metis.
 *
 * @return A vector with target ranks for each cell in the map,
 * in iteration order through the map.
 */
template <std::size_t K, class CellData>
std::vector<int> repartition_cells(
    const std::unordered_map<qcmesh::mesh::SimplexCellId,
                             qcmesh::mesh::SimplexCell<K, CellData>> &cells) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = world.size();
  auto n_cells_per_process = std::vector<std::size_t>(mpi_size);
  boost::mpi::all_gather(world, cells.size(), n_cells_per_process);

  // build CSR adjacencies (for parmetis)
  auto cell_adjncy = std::vector<int>{};
  auto ixadj = std::vector<int>{};
  cell_adjncy.reserve(cells.size() * (K + 1));
  ixadj.reserve(cells.size() + 1);
  ixadj.emplace_back(0);
  const auto id_map = build_csr_index_map(cells, n_cells_per_process);
  for (const auto &[_, cell] : cells) {
    for (const auto &n : cell.neighbors)
      if (n.has_value())
        cell_adjncy.emplace_back(static_cast<int>(id_map.at(n.value())));
    ixadj.emplace_back(cell_adjncy.size());
  }
  return repartition_cells_metis(std::move(cell_adjncy), std::move(ixadj),
                                 n_cells_per_process);
}

/**
 * @brief Insert a node from exchanged data into a mesh.
 *
 * This includes "floating data": Data not stored in the node but in other
 * data structures managed by the mesh.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void mesh_insert_node(
    QCMesh<D, K, NIDOF, NodalData> &mesh,
    const qcmesh::mesh::Node<
        D, PackedNodalData<traits::NodalThermalDataType<typename QCMesh<
                               D, K, NIDOF, NodalData>::TemperatureMode>,
                           NodalData>> &packed_node,
    const int mpi_rank) {
  auto new_node = create_node(packed_node, mpi_rank);
  const auto mesh_index = mesh.node_index_to_key.size();
  new_node.data.mesh_index = mesh_index;
  new_node.data.type = packed_node.data.type;
  new_node.data.periodic_boundary_flag =
      packed_node.data.periodic_boundary_flag;
  mesh.node_index_to_key.emplace_back(new_node.id);
  mesh.material_idx_of_nodes.push_back(packed_node.data.material_id);
  mesh.repatoms.add_repatom(packed_node.position,
                            packed_node.data.repatom_data);
  mesh.nodes.emplace(new_node.id, new_node);
}

/**
 * @brief Insert a cell from exchanged data into a mesh.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void mesh_insert_cell(
    QCMesh<D, K, NIDOF, NodalData> &mesh,
    const qcmesh::mesh::SimplexCell<K, PackedCellData<K>> &packed_cell,
    const int mpi_rank) {
  auto new_cell = qcmesh::mesh::traits::convert<
      qcmesh::mesh::SimplexCell<K, QCCellData<D, K>>>(packed_cell);
  new_cell.process_rank = mpi_rank;
  new_cell.data.offset_vectors =
      prepare_offsets(array_ops::as_eigen_matrix(new_cell.data.lattice_vectors),
                      mesh.element_offset_coeffs);
  new_cell.data.mesh_index = mesh.simplex_index_to_key.size();
  mesh.simplex_index_to_key.push_back(new_cell.id);
  mesh.cells.emplace(new_cell.id, new_cell);
}

/**
 * @brief Retrieve the cell weight stored in the sampling atoms for a given cell id.
 */
template <class Mesh>
double cell_weight(const Mesh &mesh,
                   const qcmesh::mesh::SimplexCellId cell_id) {
  return (mesh.cell_key_to_central_sampling_atom.find(cell_id) !=
          mesh.cell_key_to_central_sampling_atom.end())
             ? mesh.sampling_atoms
                   .central_weights[mesh.cell_key_to_central_sampling_atom.at(
                       cell_id)]
             : -1.0;
}

/**
 * @brief Pack nodal data into a datastructure suitable for an
 * exchange with other processes.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
qcmesh::mesh::Node<
    D, PackedNodalData<traits::NodalThermalDataType<typename QCMesh<
                           D, K, NIDOF, NodalData>::TemperatureMode>,
                       NodalData>>
pack_node(const QCMesh<D, K, NIDOF, NodalData> &mesh,
          const qcmesh::mesh::Node<D, QCNodalData> &node) {
  return {.id = node.id,
          .position = node.position,
          .data = {
              static_cast<std::size_t>(
                  mesh.material_idx_of_nodes[node.data.mesh_index]),
              mesh.sampling_atoms.nodal_weights[node.data.mesh_index],
              node.data.type,
              node.data.periodic_boundary_flag,
              mesh.repatoms.pack_data(node.data.mesh_index),
          }};
}

/**
 * @brief Pack cell data into a datastructure suitable for an
 * exchange with other processes.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
qcmesh::mesh::SimplexCell<K, PackedCellData<K>>
pack_cell(const QCMesh<D, K, NIDOF, NodalData> &mesh,
          const qcmesh::mesh::SimplexCell<K, QCCellData<D, K>> &cell) {
  auto packed_cell = qcmesh::mesh::traits::convert<
      qcmesh::mesh::SimplexCell<K, PackedCellData<K>>>(cell);
  packed_cell.data.weight = cell_weight(mesh, cell.id);
  return packed_cell;
}

/**
 * @brief Redistribution of cells and nodes for load balancing or mesh repair.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void redistribute(QCMesh<D, K, NIDOF, NodalData> &mesh) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = world.size();
  const auto mpi_rank = world.rank();

  const auto target_ranks = repartition_cells(mesh.cells);

  using Mesh = QCMesh<D, K, NIDOF, NodalData>;
  using ThermalDataType =
      traits::NodalThermalDataType<typename Mesh::TemperatureMode>;

  auto processed_nodes = std::vector<std::set<qcmesh::mesh::NodeId>>(mpi_size);
  auto cells_to_delete = std::vector<qcmesh::mesh::SimplexCellId>{};
  auto nodes_for_process = std::vector<std::vector<
      qcmesh::mesh::Node<D, PackedNodalData<ThermalDataType, NodalData>>>>(
      mpi_size);
  auto cells_for_process =
      std::vector<std::vector<qcmesh::mesh::SimplexCell<K, PackedCellData<K>>>>(
          mpi_size);
  auto i_cell = std::size_t{0};
  for (const auto &[_, cell] : mesh.cells) {
    const auto target_rank = target_ranks[i_cell++];
    if (target_rank == mpi_rank)
      continue;
    for (const auto node_id : cell.nodes) {
      if (processed_nodes[target_rank].find(node_id) !=
          processed_nodes[target_rank].end())
        continue;
      nodes_for_process[target_rank].emplace_back(
          pack_node(mesh, mesh.nodes.at(node_id)));
      processed_nodes[target_rank].emplace(node_id);
    }
    cells_for_process[target_rank].emplace_back(pack_cell(mesh, cell));
    cells_to_delete.emplace_back(cell.id);
  } // namespace meshing

  auto nodes_from_process = qcmesh::mpi::all_scatter(nodes_for_process);
  auto cells_from_process = qcmesh::mpi::all_scatter(cells_for_process);

  mesh.remove_cells(cells_to_delete);

  auto remaining_cell_weights = std::vector<double>(mesh.cells.size());
  for (const auto &[_, cell] : mesh.cells)
    remaining_cell_weights[cell.data.mesh_index] = cell_weight(mesh, cell.id);
  auto remaining_nodes_weights =
      std::unordered_map<qcmesh::mesh::NodeId, double>{};
  for (const auto &[_, node] : mesh.nodes)
    remaining_nodes_weights.emplace(
        node.id, mesh.sampling_atoms.nodal_weights[node.data.mesh_index]);

  auto unreferenced_nodes = mesh.find_unreferenced_nodes();

  for (const auto &new_nodes : nodes_from_process)
    for (const auto &raw_node : new_nodes) {
      unreferenced_nodes.erase(raw_node.id);
      if (mesh.nodes.find(raw_node.id) == mesh.nodes.end()) {
        mesh_insert_node(mesh, raw_node, mpi_rank);
        remaining_nodes_weights.emplace(raw_node.id, raw_node.data.weight);
      }
    }

  for (const auto &new_cells : cells_from_process)
    for (const auto &raw_cell : new_cells) {
      mesh_insert_cell(mesh, raw_cell, mpi_rank);
      remaining_cell_weights.emplace_back(raw_cell.data.weight);
    }

  mesh.remove_nodes(unreferenced_nodes);
  // renumber mesh nodes (reorder indices according to ghost indices)
  const auto permutation = renumber_mesh_nodes(mesh);
  mesh.repatoms.permute(permutation);
  // rebuild repatoms and sampling atoms
  mesh.repatoms.assign_ghost_do_fs(mesh.global_ghost_indices);
  // rebuild weights
  mesh.build_sampling_atoms(remaining_cell_weights, remaining_nodes_weights);

  // assign repatom weights using sampling atoms
  mesh.assign_repatom_weights();

  setup_process_boundary(mesh);
}

} // namespace meshing
