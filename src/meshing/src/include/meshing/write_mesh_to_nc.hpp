// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Functions to write a mesh to a nc file
 * @author G. Bräunlich
 */

#pragma once

#include "meshing/qc_mesh.hpp"
#include "meshing/temperature_mode.hpp"
#include "netcdf_io/write_nc_file.hpp"
#include <filesystem>
#include <type_traits>

namespace meshing {

namespace detail {
namespace traits {
template <class TemperatureMode> struct BundleNodalData;
template <> struct BundleNodalData<FiniteT> {
  template <std::size_t NIDOF, typename NodalData>
  static netcdf_io::RestartNodalDataFiniteT<
      meshing::traits::N_IMPURITIES<NodalData>>
  bundle_nodal_data(const QCMesh<3, 3, NIDOF, NodalData> &mesh,
                    const qcmesh::mesh::Node<3, QCNodalData> &node) {
    const auto mesh_index = node.data.mesh_index;
    return netcdf_io::RestartNodalDataFiniteT<
        meshing::traits::N_IMPURITIES<NodalData>>{
        .material_id =
            static_cast<std::size_t>(mesh.material_idx_of_nodes[mesh_index]),
        .type = node.data.type,
        .flags = static_cast<unsigned int>(mesh.repatoms.if_fixed[mesh_index]),
        .weight = mesh.sampling_atoms.nodal_weights[mesh_index],
        .impurity_concentrations =
            mesh.repatoms.nodal_data_points[mesh_index].impurity_concentrations,
        .thermal_point = mesh.repatoms.thermal_coordinates[mesh_index],
        .entropy = mesh.repatoms.entropy[mesh_index],
    };
  }
};
template <> struct BundleNodalData<ZeroT> {
  template <std::size_t NIDOF, typename NodalData>
  static netcdf_io::RestartNodalDataZeroT<
      meshing::traits::N_IMPURITIES<NodalData>>
  bundle_nodal_data(const QCMesh<3, 3, NIDOF, NodalData> &mesh,
                    const qcmesh::mesh::Node<3, QCNodalData> &node) {
    const auto mesh_index = node.data.mesh_index;
    return netcdf_io::RestartNodalDataZeroT<
        meshing::traits::N_IMPURITIES<NodalData>>{
        .material_id =
            static_cast<std::size_t>(mesh.material_idx_of_nodes[mesh_index]),
        .type = node.data.type,
        .flags = static_cast<unsigned int>(mesh.repatoms.if_fixed[mesh_index]),
        .weight = mesh.sampling_atoms.nodal_weights[mesh_index],
        .impurity_concentrations =
            mesh.repatoms.nodal_data_points[mesh_index].impurity_concentrations,
    };
  }
};
} // namespace traits

template <class TemperatureMode, std::size_t NIDOF, typename NodalData>
void write_restart_mesh_to_nc(
    const std::filesystem::path &path,
    const QCMesh<3, 3, NIDOF, NodalData> &mesh,
    const netcdf_io::RestartData<3> &restart_data,
    const std::vector<boundarycondition::Boundarycondition<3>>
        &boundary_conditions,
    const boundarycondition::ExternalForceApplicator<3>
        &external_force_applicator) {
  constexpr std::size_t DIM = 3;
  const auto bundle_cell_data = [&mesh](const auto &cell) {
    const auto weight =
        (mesh.cell_key_to_central_sampling_atom.find(cell.id) !=
         mesh.cell_key_to_central_sampling_atom.end())
            ? mesh.sampling_atoms
                  .central_weights[mesh.cell_key_to_central_sampling_atom.at(
                      cell.id)]
            : -1.0;

    return netcdf_io::RestartCellData<DIM>{
        .is_atomistic = cell.data.is_atomistic,
        .density = cell.data.density,
        .jacobian = cell.data.jacobian,
        .weight = weight,
        .basis = cell.data.lattice_vectors,
    };
  };
  const auto bundle_nodal_data = [&mesh](const auto &node) {
    return traits::BundleNodalData<TemperatureMode>::bundle_nodal_data(mesh,
                                                                       node);
  };
  auto updated_restart_data = restart_data;
  updated_restart_data.periodic_lengths = mesh.periodic_lengths;
  netcdf_io::write_restart_file<DIM, DIM>(
      path, mesh.cells, bundle_cell_data, mesh.nodes, bundle_nodal_data,
      updated_restart_data, boundary_conditions, external_force_applicator);
}

} // namespace detail

/**
 * @brief Write a QCMesh + restart data + boundary conditions + external force applicator to a restart NetCdf file.
 */
template <std::size_t NIDOF, typename NodalData>
void write_restart_mesh_to_nc(
    const std::filesystem::path &path,
    const QCMesh<3, 3, NIDOF, NodalData> &mesh,
    const netcdf_io::RestartData<3> &restart_data,
    const std::vector<boundarycondition::Boundarycondition<3>>
        &boundary_conditions,
    const boundarycondition::ExternalForceApplicator<3>
        &external_force_applicator) {
  return detail::write_restart_mesh_to_nc<
      typename std::remove_reference_t<decltype(mesh)>::TemperatureMode>(
      path, mesh, restart_data, boundary_conditions, external_force_applicator);
}

/**
 * @brief Write a QCMesh to a NetCdf file.
 *
 * Lattice vectors are optional.
 */
template <std::size_t NIDOF, typename NodalData>
void write_mesh_to_nc(const std::filesystem::path &path,
                      const QCMesh<3, 3, NIDOF, NodalData> &mesh,
                      const bool save_lattice_vectors) {
  const auto bundle_nodal_data = [&mesh](const auto &node) {
    const auto mesh_index = node.data.mesh_index;
    return netcdf_io::MeshNodalData{
        .material_id =
            static_cast<std::size_t>(mesh.material_idx_of_nodes[mesh_index]),
        .type = node.data.type,
    };
  };
  constexpr std::size_t DIM = 3;
  const auto mesh_data = netcdf_io::MeshData<DIM>{save_lattice_vectors,
                                                  mesh.atomistic_domain.lower,
                                                  mesh.atomistic_domain.upper};
  netcdf_io::write_mesh_file<DIM, DIM>(path, mesh.cells, mesh.nodes,
                                       bundle_nodal_data, mesh_data);
}

} // namespace meshing
