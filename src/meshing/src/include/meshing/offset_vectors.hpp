// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to prepare offsets
 * @author P. Gupta
 */

#pragma once

#include "qcmesh/array_ops/array_ops.hpp"
#include <array>
#include <vector>

namespace array_ops = qcmesh::array_ops;

namespace meshing {

/**
 * @brief Applies a matrix `cell_basis` to each vector in a vector.
 */
template <std::size_t Dimension, class MatrixType>
std::vector<std::array<double, Dimension>> prepare_offsets(
    const MatrixType &cell_basis,
    const std::vector<std::array<double, Dimension>> &offset_coefs) {
  auto offset = std::vector<std::array<double, Dimension>>{};
  offset.reserve(offset_coefs.size());

  for (const auto &coefs : offset_coefs) {
    auto o = std::array<double, Dimension>{};
    array_ops::as_eigen(o) = cell_basis * array_ops::as_eigen(coefs);
    offset.emplace_back(o);
  }
  return offset;
}

} // namespace meshing
