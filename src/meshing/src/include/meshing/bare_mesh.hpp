// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Minimal information needed to create a QC mesh.
 */

#pragma once

#include "qcmesh/mesh/node.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include <vector>

namespace meshing {

/**
 * @brief Minimal information needed to create a QC mesh.
 */
template <std::size_t D, std::size_t K, class NodalDataType, class CellDataType>
struct BareMesh {
  std::vector<qcmesh::mesh::Node<D, NodalDataType>> nodes{};
  std::vector<qcmesh::mesh::SimplexCell<K, CellDataType>> cells{};
};

} // namespace meshing
