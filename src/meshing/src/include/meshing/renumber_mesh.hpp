// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief renumbering of mesh to arrange nodes such that local nodes
 *        are followed by the ghost nodes
 * @authors P. Gupta
 */

#pragma once

#include "meshing/build_interproc_connectivity.hpp"
#include "meshing/partition_ghost_indices.hpp"
#include "meshing/qc_mesh.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include <boost/serialization/unordered_map.hpp>
#include <numeric>

namespace meshing {

/**
 * @brief hange cell keys according to global keys
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void renumber_cells(QCMesh<D, K, NIDOF, NodalData> &mesh) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = static_cast<std::size_t>(world.size());
  const auto mpi_rank = world.rank();

  // change the global keys of mesh simplices
  // and their neighbors. The local ordering doesn't change.
  // so any element data doesn't need to be reordered

  std::vector<std::size_t> noof_cell_keys_all_ranks(mpi_size);
  boost::mpi::all_gather(world, mesh.cells.size(), noof_cell_keys_all_ranks);

  const unsigned int noof_cells =
      std::accumulate(noof_cell_keys_all_ranks.begin(),
                      noof_cell_keys_all_ranks.end(), std::size_t{0});

  mesh.cell_id_counter = noof_cells + 1;

  std::size_t cell_key_offset = 0;

  for (int i_proc = 0; i_proc < mpi_rank; i_proc++) {
    cell_key_offset +=
        static_cast<std::size_t>(noof_cell_keys_all_ranks[i_proc]);
  }

  // new key of ith index is cellKeyOffset+i
  std::unordered_map<qcmesh::mesh::SimplexCellId, qcmesh::mesh::SimplexCellId>
      old_key_to_new_key;
  old_key_to_new_key.reserve(mesh.cells.size());

  std::unordered_map<qcmesh::mesh::SimplexCellId,
                     qcmesh::mesh::SimplexCell<K, QCCellData<D, K>>>
      new_mesh_cells;
  new_mesh_cells.reserve(mesh.cells.size());

  for (std::size_t i = 0; i < mesh.simplex_index_to_key.size(); i++) {
    const auto old_key = mesh.simplex_index_to_key[i];
    const auto new_key = qcmesh::mesh::SimplexCellId{cell_key_offset + i};

    old_key_to_new_key.emplace(old_key, new_key);
    mesh.simplex_index_to_key[i] = new_key;
    new_mesh_cells.insert(std::make_pair(new_key, mesh.cells.at(old_key)));
    new_mesh_cells.at(new_key).data.mesh_index = i;
    new_mesh_cells.at(new_key).id = new_key;
    new_mesh_cells.at(new_key).process_rank = mpi_rank;
  }

  mesh.cells.swap(new_mesh_cells);

  std::unordered_map<qcmesh::mesh::SimplexCellId,
                     qcmesh::mesh::SimplexCell<K, QCCellData<D, K>>>()
      .swap(new_mesh_cells);

  // gather remappings from other ranks (on collision take value from this rank)
  auto global_remapping =
      std::vector<std::unordered_map<qcmesh::mesh::SimplexCellId,
                                     qcmesh::mesh::SimplexCellId>>(mpi_size);
  boost::mpi::all_gather(world, old_key_to_new_key, global_remapping);
  if (global_remapping[mpi_rank] != old_key_to_new_key)
    throw exception::QCException{
        "globally remapped node key doesn't match the new node key"};
  for (std::size_t i = 0; i < mpi_size; i++)
    if (i != static_cast<std::size_t>(mpi_rank))
      old_key_to_new_key.insert(global_remapping[i].begin(),
                                global_remapping[i].end());

  // change neighbor keys
  for (auto &[_, cell] : mesh.cells)
    for (std::size_t d = 0; d < K + 1; d++) {
      if (!cell.neighbors[d].has_value())
        continue;
      const auto neighbor_id = cell.neighbors[d].value();
      if (old_key_to_new_key.find(neighbor_id) != old_key_to_new_key.end())
        cell.neighbors[d] = old_key_to_new_key.at(neighbor_id);
    }
}

inline std::vector<std::size_t>
invert_permutation(const std::vector<std::size_t> &permutation) {
  auto inverted = std::vector<std::size_t>(permutation.size());
  for (std::size_t i = 0; i < permutation.size(); i++)
    inverted[permutation[i]] = i;
  return inverted;
}

/**
 * @brief renumbering arranges nodes such that
 * All local nodes are followed by ghost nodes
 * Ghost nodes are nodes shared with a lower rankID
 *
 * @return The permutation the nodes are reordered with.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
std::vector<std::size_t>
renumber_mesh_nodes(QCMesh<D, K, NIDOF, NodalData> &mesh) {
  build_interproc_connectivity<D>(mesh);

  mesh.ghost_node_offsets.clear();
  // after every refining, renumbering is important.
  // this simplifies my life. Don't know if its going to be used
  // by anyone else though

  const auto [local_indices, ghost_indices] = partition_ghost_indices(mesh);

  // global keys of local nodes before renumbering
  std::vector<qcmesh::mesh::NodeId> old_global_keys_of_local_nodes;
  // global keys of ghost nodes before renumbering
  std::vector<qcmesh::mesh::NodeId> old_global_keys_of_ghost_nodes;

  // node_index_to_key[i] tells the node at ith index
  old_global_keys_of_local_nodes.reserve(local_indices.size());
  old_global_keys_of_ghost_nodes.reserve(ghost_indices.size());
  mesh.ghost_node_offsets.reserve(ghost_indices.size());

  for (std::size_t local_index : local_indices) {
    old_global_keys_of_local_nodes.push_back(
        mesh.node_index_to_key[local_index]);
  }

  for (std::size_t ghost_index : ghost_indices) {
    if (mesh.original_keys_of_periodic_images.find(ghost_index) !=
        mesh.original_keys_of_periodic_images.end()) {
      old_global_keys_of_ghost_nodes.push_back(qcmesh::mesh::NodeId{
          mesh.original_keys_of_periodic_images.at(ghost_index)});

      mesh.ghost_node_offsets.push_back(
          mesh.offsets_of_periodic_images.at(ghost_index));
    } else {
      old_global_keys_of_ghost_nodes.push_back(
          mesh.node_index_to_key[ghost_index]);

      mesh.ghost_node_offsets.push_back(geometry::Point<D>{});
    }
  }

  // new order of local indices based on current indices
  std::vector<std::size_t> new_local_order_of_indices;
  new_local_order_of_indices.reserve(mesh.nodes.size());

  new_local_order_of_indices.insert(new_local_order_of_indices.end(),
                                    make_move_iterator(local_indices.begin()),
                                    make_move_iterator(local_indices.end()));

  new_local_order_of_indices.insert(new_local_order_of_indices.end(),
                                    make_move_iterator(ghost_indices.begin()),
                                    make_move_iterator(ghost_indices.end()));

  std::vector<qcmesh::mesh::NodeId> reordered_index_to_keys;

  reordered_index_to_keys.reserve(mesh.nodes.size());

  // 	to reorder the nodes, we only need
  //	to change the mesh index of nodes.

  // reorder the material ids at nodes and node keys
  for (std::size_t i_node = 0; i_node < mesh.nodes.size(); i_node++) {
    reordered_index_to_keys.push_back(
        mesh.node_index_to_key[new_local_order_of_indices[i_node]]);
  }
  // we use new index of old index to change the mesh index
  auto new_index_of_old_index = invert_permutation(new_local_order_of_indices);

  mesh.node_index_to_key.swap(reordered_index_to_keys);

  std::vector<qcmesh::mesh::NodeId>().swap(reordered_index_to_keys);

  // change the actual localMeshIndex of the nodes
  for (auto &key_node_pair : mesh.nodes) {
    // node.data.mesh_index has the old mesh index
    key_node_pair.second.data.mesh_index =
        new_index_of_old_index[key_node_pair.second.data.mesh_index];
  }

  // change the indices stored in connected_nodes
  for (std::size_t i_proc = 0; i_proc < mesh.connected_node_procs.size();
       i_proc++) {
    for (std::size_t i_node = 0; i_node < mesh.connected_nodes[i_proc].size();
         i_node++) {
      mesh.connected_nodes[i_proc][i_node] =
          new_index_of_old_index[mesh.connected_nodes[i_proc][i_node]];
    }
  }

  // nothing changes with elements keys, so face neighbors are safe
  // nothing changes with nodes keys also, only the indexing in local mesh
  // changes

  std::unordered_map<qcmesh::mesh::NodeId, unsigned int>
      old_global_keys_to_distributed_indices;

  const auto global_key_distribution =
      qcmesh::mpi::all_concat_vec(old_global_keys_of_local_nodes);

  // arrange the global vector
  for (std::size_t global_id_ctr = 0;
       global_id_ctr < global_key_distribution.size(); global_id_ctr++) {

    old_global_keys_to_distributed_indices.insert(
        {global_key_distribution[global_id_ctr], global_id_ctr});
  }

  // these globalIndices allow accessing ghost nodes
  // from a distributed petsc vector
  mesh.global_ghost_indices.clear();
  mesh.global_ghost_indices.reserve(mesh.global_ghost_indices.size());
  mesh.ghost_node_offsets.reserve(mesh.global_ghost_indices.size());

  for (const auto old_global_key : old_global_keys_of_ghost_nodes) {
    if (old_global_keys_to_distributed_indices.find(old_global_key) ==
        old_global_keys_to_distributed_indices.end())
      throw exception::QCException{
          "old_global_key not found in old_global_keys_to_distributed_indices"};

    mesh.global_ghost_indices.push_back(
        old_global_keys_to_distributed_indices.at(old_global_key));
  }

  // we can leave the keys as they are. They are simply the
  // unique identifiers we need to access the nodes and simplices.
  return new_index_of_old_index;
}
} // namespace meshing
