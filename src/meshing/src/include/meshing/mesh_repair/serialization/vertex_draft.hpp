// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief boost serialization for vertex draft
 */

#pragma once

#include "meshing/mesh_repair/vertex_draft.hpp"
#include "meshing/serialization/nodal_data_multispecies.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost {
namespace serialization {

template <class Archive, class ThermalData, class MultiSpeciesData>
void serialize(Archive &ar,
               meshing::QCVertexDraft<ThermalData, MultiSpeciesData> &vx,
               const unsigned int) {
  ar &vx.owner_rank;
  ar &vx.id;
  ar &vx.point;
  ar &vx.material_id;
  ar &vx.site_type;
  ar &vx.thermal_data;
  ar &vx.multi_species_data;
}

/**
 * See https://www.boost.org/doc/libs/1_82_0/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <class ThermalData, class MultiSpeciesData>
struct tracking_level<meshing::QCVertexDraft<ThermalData, MultiSpeciesData>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTBEGIN(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  using type = mpl::int_<boost::serialization::track_never>;
  // NOLINTEND(readability-identifier-naming)
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<
                        meshing::QCVertexDraft<ThermalData, MultiSpeciesData>>,
                    mpl::int_<primitive_type>>::value));
};

template <class ThermalData, class MultiSpeciesData>
struct implementation_level<
    meshing::QCVertexDraft<ThermalData, MultiSpeciesData>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTBEGIN(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  using type = mpl::int_<object_serializable>;
  // NOLINTEND(readability-identifier-naming)
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};
} // namespace serialization

/**
 * Implement a mpi type trait for better performance.
 * BOOST_IS_MPI_DATATYPE(QCVertexDraft<ThermalData, MultiSpeciesData>);
 */
namespace mpi {
template <class ThermalData, class MultiSpeciesData>
struct is_mpi_datatype<meshing::QCVertexDraft<ThermalData, MultiSpeciesData>>
    : public mpl::true_ {};

} // namespace mpi
} // namespace boost
