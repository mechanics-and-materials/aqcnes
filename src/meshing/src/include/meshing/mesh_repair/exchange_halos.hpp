// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Halo mesh exchange for repair
 * @authors P. Gupta, G. Bräunlich
 */

#pragma once
#include "meshing/mesh_topology.hpp"
#include "meshing/nodal_thermal_data.hpp"
#include "meshing/qc_mesh.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi.hpp>
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <unordered_set>

namespace meshing {

template <class ThermalNodalData, std::size_t D, std::size_t NOI>
struct SerializedNode {
  qcmesh::mesh::NodeId id{};
  int material_index{};
  std::array<double, D> position{};
  ThermalNodalData thermal_data{};
  std::array<double, NOI> impurity_concentrations{};
};

template <std::size_t D, std::size_t K> struct SerializedCell {
  qcmesh::mesh::SimplexCellId id{};
  std::array<qcmesh::mesh::NodeId, K + 1> nodes{};
  std::array<std::array<double, D>, D> lattice_vectors{};
};

} // namespace meshing

namespace boost::serialization {

/**
 * @brief Provides serialization of meshing::SerializedCell.
 */
template <typename Archive, std::size_t D, std::size_t K>
void serialize(Archive &ar, meshing::SerializedCell<D, K> &data,
               const unsigned int) {
  ar &data.id;
  ar &data.nodes;
  ar &data.lattice_vectors;
}

/**
 * @brief Provides serialization of meshing::SerializedNode.
 */
template <typename Archive, class ThermalNodalData, std::size_t D,
          std::size_t NOI>
void serialize(Archive &ar,
               meshing::SerializedNode<ThermalNodalData, D, NOI> &data,
               const unsigned int) {
  ar &data.id;
  ar &data.material_index;
  ar &data.position;
  ar &data.thermal_data;
  ar &data.impurity_concentrations;
}

} // namespace boost::serialization

template <class ThermalNodalData, std::size_t D, std::size_t NOI>
struct boost::serialization::tracking_level<
    meshing::SerializedNode<ThermalNodalData, D, NOI>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<
                        meshing::SerializedNode<ThermalNodalData, D, NOI>>,
                    mpl::int_<primitive_type>>::value));
};

template <class ThermalNodalData, std::size_t D, std::size_t NOI>
struct boost::serialization::implementation_level<
    meshing::SerializedNode<ThermalNodalData, D, NOI>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <class ThermalNodalData, std::size_t D, std::size_t NOI>
struct boost::mpi::is_mpi_datatype<
    meshing::SerializedNode<ThermalNodalData, D, NOI>>
    : public boost::mpl::true_ {};

template <std::size_t D, std::size_t K>
struct boost::serialization::tracking_level<meshing::SerializedCell<D, K>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<meshing::SerializedCell<D, K>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t D, std::size_t K>
struct boost::serialization::implementation_level<
    meshing::SerializedCell<D, K>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <std::size_t D, std::size_t K>
struct boost::mpi::is_mpi_datatype<meshing::SerializedCell<D, K>>
    : public boost::mpl::true_ {};

namespace meshing {

template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void exchange_halos(
    QCMesh<D, K, NIDOF, NodalData> &mesh,
    const std::vector<qcmesh::mesh::SimplexCellId> &element_ids) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = world.size();
  const auto mpi_rank = world.rank();
  constexpr std::size_t NOI = traits::N_IMPURITIES<NodalData>;

  // rebuild vertex adjacency. Its needed to locate the adjacent simplices
  // for communicating halo mesh
  build_node_cell_adjacency<K>(mesh);

  // all gather node keys that would require halo elements
  auto added_keys = std::unordered_set<qcmesh::mesh::NodeId>{};
  auto nodes_requiring_halo = std::vector<qcmesh::mesh::NodeId>{};
  nodes_requiring_halo.reserve(mesh.nodes.size());

  for (const auto &s : element_ids) {
    const auto &cell = mesh.cells.at(s);
    for (const auto node_id : cell.nodes) {
      if (mesh.nodes.at(node_id).data.proc_boundary_flag &&
          added_keys.find(node_id) == added_keys.end()) {
        added_keys.insert(node_id);
        nodes_requiring_halo.push_back(node_id);
      }
    }
  }

  auto nodes_requiring_halo_by_proc =
      std::vector<std::vector<qcmesh::mesh::NodeId>>(mpi_size);
  boost::mpi::all_gather(world, nodes_requiring_halo,
                         nodes_requiring_halo_by_proc);

  auto ranks_of_unique_halo_nodes =
      std::unordered_map<qcmesh::mesh::NodeId, std::vector<int>>{};

  auto added_simplices_to_send =
      std::vector<std::unordered_set<qcmesh::mesh::SimplexCellId>>(mpi_size);

  // store the keys of incident elements to the node if present
  for (int i = 0; i < mpi_size; i++)
    for (const auto node_id : nodes_requiring_halo_by_proc[i])
      if (ranks_of_unique_halo_nodes.find(node_id) ==
          ranks_of_unique_halo_nodes.end())
        ranks_of_unique_halo_nodes.insert({node_id, {i}});
      else
        ranks_of_unique_halo_nodes.at(node_id).push_back(i);

  for (const auto &n : ranks_of_unique_halo_nodes) {
    const auto node_key = n.first;

    if (mesh.nodes.find(node_key) != mesh.nodes.end()) {
      // local node Index of this nodeKey
      const auto node_index = mesh.nodes.at(node_key).data.mesh_index;

      // add the incident elements
      const auto noof_incident = n_incident_elements(mesh, node_index);

      for (std::size_t j = 0; j != noof_incident; ++j) {
        const auto adj_simplex_key = incident_element(mesh, node_index, j);

        for (const auto node_rank : n.second) {
          if (node_rank != mpi_rank) {
            if (added_simplices_to_send[node_rank].find(adj_simplex_key) ==
                added_simplices_to_send[node_rank].end()) {
              added_simplices_to_send[node_rank].insert(adj_simplex_key);
            }
          }
        }
      }
    }
  }

  // Pack cell / nodal data for sending
  using NodalThermalDataType =
      decltype(mesh.repatoms.pack_thermal_data(std::size_t{}));
  auto nodes_for_proc =
      std::vector<std::vector<SerializedNode<NodalThermalDataType, D, NOI>>>(
          mpi_size);
  auto cells_for_proc =
      std::vector<std::vector<SerializedCell<D, K>>>(mpi_size);
  for (int rank = 0; rank < mpi_size; rank++) {
    if (rank == mpi_rank)
      continue;
    cells_for_proc[rank].reserve(added_simplices_to_send[rank].size());
    auto processed_node_ids = std::unordered_set<qcmesh::mesh::NodeId>{};
    for (const auto cell_id : added_simplices_to_send[rank]) {
      const auto &cell = mesh.cells.at(cell_id);
      cells_for_proc[rank].push_back(
          {cell_id, cell.nodes, cell.data.lattice_vectors});
      for (const auto node_id : cell.nodes) {
        if (processed_node_ids.find(node_id) != processed_node_ids.end())
          continue;
        const auto &node = mesh.nodes.at(node_id);
        const auto mesh_index = node.data.mesh_index;
        processed_node_ids.insert(node_id);
        nodes_for_proc[rank].push_back(
            {node_id, mesh.material_idx_of_nodes[mesh_index], node.position,
             mesh.repatoms.pack_thermal_data(mesh_index),
             mesh.repatoms.nodal_data_points[mesh_index]
                 .impurity_concentrations});
      }
    }
  }

  // Distribute
  const auto nodes_from_proc = qcmesh::mpi::all_scatter(nodes_for_proc);
  const auto cells_from_proc = qcmesh::mpi::all_scatter(cells_for_proc);

  mesh.halo_cells.clear();
  mesh.halo_nodes.clear();
  mesh.halo_node_material_indices.clear();

  using MeshType = QCMesh<D, K, NIDOF, NodalData>;
  if constexpr (std::is_same_v<typename MeshType::TemperatureMode,
                               meshing::FiniteT>) {
    mesh.halo_node_entropy.clear();
    mesh.halo_node_thermal_points.clear();
  }
  mesh.halo_node_nodal_data.clear();

  // Unpack cell / nodal data
  for (int rank = 0; rank < mpi_size; rank++)
    for (const auto &node : nodes_from_proc[rank])
      if (mesh.nodes.find(node.id) == mesh.nodes.end() &&
          mesh.halo_nodes.find(node.id) == mesh.halo_nodes.end()) {
        mesh.halo_nodes.emplace(node.id, qcmesh::mesh::Node<D, QCNodalData>{
                                             node.id, rank, node.position});
        mesh.halo_node_material_indices.insert({node.id, node.material_index});
        if constexpr (std::is_same_v<typename MeshType::TemperatureMode,
                                     meshing::FiniteT>) {

          mesh.halo_node_thermal_points.insert(
              {node.id, node.thermal_data.thermal_point});
          mesh.halo_node_entropy.insert({node.id, node.thermal_data.entropy});
        }

        mesh.halo_node_nodal_data.emplace(
            node.id, NodalData{node.impurity_concentrations});
      }
  for (int rank = 0; rank < mpi_size; rank++)
    for (const auto &cell : cells_from_proc[rank])
      mesh.halo_cells.emplace(
          cell.id,
          qcmesh::mesh::SimplexCell<K, QCCellData<D, K>>{
              cell.id,
              rank,
              cell.nodes,
              {},
              QCCellData<D, K>{.lattice_vectors = cell.lattice_vectors}});
}

} // namespace meshing
