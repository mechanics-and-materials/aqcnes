// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Mesh repair
 * @authors P. Gupta, G. Bräunlich
 */

#pragma once

#include "meshing/mesh_repair/exchange_halos.hpp"
#include "meshing/mesh_repair/serialization/vertex_draft.hpp"
#include "meshing/mesh_repair/vertex_draft.hpp"
#include "meshing/mesh_topology.hpp"
#include "meshing/nodal_thermal_data.hpp"
#include "meshing/offset_vectors.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/renumber_mesh.hpp"
#include "meshing/serialization/nodal_data_multispecies.hpp"
#include "meshing/serialization/nodal_thermal_data.hpp"
#include "meshing/serialization/qc_cell_data.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/greedy_driver.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/optimize_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/serialization/cell_address.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/serialization/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/edge_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/face_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/repair_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/serialization/cell_draft.hpp"
#include "qcmesh/mesh/serialization/simplex_cell.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include <boost/serialization/utility.hpp>
#include <unordered_set>

namespace meshing {

using QCMeshBase =
    qcmesh::mesh::SimplexMesh<3, 3, QCNodalData, QCCellData<3, 3>>;
using QCCellHandle = qcmesh::mesh::SimplexCellId;
using QCVertexHandle = qcmesh::mesh::NodeId;
using QCCell = qcmesh::mesh::SimplexCell<3, QCCellData<3, 3>>;
using QCCellDraft = qcmesh::mesh::CellDraft<QCCellData<3, 3>>;

/**
 * Called by qcmesh::mesh::apply_change_set
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
void insert_node_from_halo(QCMesh<D, K, NIDOF, MeshParameters...> &mesh,
                           const qcmesh::mesh::NodeId node_id) {
  if (mesh.nodes.find(node_id) == mesh.nodes.end()) {
    const auto mpi_rank = boost::mpi::communicator{}.rank();
    // new node. get coordinates from halo_nodes
    if (mesh.halo_nodes.find(node_id) == mesh.halo_nodes.end())
      throw exception::QCException{"insert_node_from_halo: node_id found "
                                   "neither in local nor in halo mesh"};
    auto &&[new_iter, _insertion_happened] =
        mesh.nodes.insert(std::make_pair(node_id, mesh.halo_nodes.at(node_id)));
    auto &&[_new_key, new_node] = *new_iter;
    // add key to node_index_to_key
    mesh.node_index_to_key.push_back(node_id);
    // set its pair key rank as this rank
    new_node.process_rank = mpi_rank;
    // set its MeshIndex
    new_node.data.mesh_index = mesh.node_index_to_key.size() - 1;
    // push_back corresponding material index
    mesh.material_idx_of_nodes.push_back(
        mesh.halo_node_material_indices.at(node_id));
    // push_back all the nodal data to repatoms. What a messs
    // this is a new local repatom
    mesh.repatoms.push_back();
    mesh.sampling_atoms.push_back_nodal();
    mesh.repatoms.locations[mesh.node_index_to_key.size() - 1] =
        new_node.position;
    mesh.repatoms.lattice_site_type[mesh.node_index_to_key.size() - 1] =
        new_node.data.type;
    mesh.sampling_atoms.nodal_locations[mesh.node_index_to_key.size() - 1] =
        new_node.position;
    using MeshType = QCMesh<D, K, NIDOF, MeshParameters...>;
    if constexpr (std::is_same_v<typename MeshType::TemperatureMode,
                                 meshing::FiniteT>) {
      mesh.repatoms.thermal_coordinates[mesh.node_index_to_key.size() - 1] =
          mesh.halo_node_thermal_points.at(node_id);
      mesh.repatoms.entropy[mesh.node_index_to_key.size() - 1] =
          mesh.halo_node_entropy.at(node_id);
    }
    mesh.repatoms.nodal_data_points[mesh.node_index_to_key.size() - 1] =
        mesh.halo_node_nodal_data.at(node_id);
  }
}

/**
 * @brief Insert a QCCellDraft into a mesh.
 *
 * Called by qcmesh::mesh::apply_change_set
 * This includes:
 * - connecting the cell to the mesh
 * - memory management of nodal data.
 * - Possibly move vertices from halo nodes do nodes.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
qcmesh::mesh::SimplexCell<3, QCCellData<3, 3>> &
insert_cell_without_basis(QCMesh<D, K, NIDOF, MeshParameters...> &mesh,
                          const QCCellDraft &cell_draft) {
  const auto cell_id = cell_draft.cell.id;
  auto &new_cell = qcmesh::mesh::insert_cell_without_basis(mesh, cell_draft);
  new_cell.data.mesh_index = mesh.simplex_index_to_key.size();
  mesh.simplex_index_to_key.push_back(cell_id);

  // push back element data that was deleted
  const auto undeformed_density = cell_density(new_cell, mesh);

  new_cell.data.density = undeformed_density / new_cell.data.jacobian;
  new_cell.data.offset_vectors =
      prepare_offsets<D>(cell_draft.cell_basis, mesh.element_offset_coeffs);
  return new_cell;
}

/**
 * @brief Same as @ref insert_cell, but insert to halo cells instead.
 *
 * Overrides the call in @ref qcmesh::mesh::apply_change_set_distributed_leaving_cells.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
void insert_halo_cell(QCMesh<D, K, NIDOF, MeshParameters...> &mesh,
                      const QCCellDraft &cell_draft) {
  auto &inserted_cell = qcmesh::mesh::insert_halo_cell(mesh, cell_draft);
  inserted_cell.data.offset_vectors =
      prepare_offsets<D>(cell_draft.cell_basis, mesh.element_offset_coeffs);
}

namespace traits {

template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
struct NImpurities<QCMesh<D, K, NIDOF, MeshParameters...>> {
  constexpr static std::size_t VALUE = std::tuple_size<typename QCMesh<
      D, K, NIDOF, MeshParameters...>::NodalDataType::ConcentrationPoint>{};
};

/**
 * @brief Queries the multispecies type depending on the temperature mode (either @ref NodalDataMultispecies (T>0) or @ref qcmesh::mesh::EmptyData (T=0)).
 */
template <class TemperatureMode, std::size_t NOI>
struct NodalMultispeciesDataTypeTrait;

/**
 * @brief Specialization of @ref NodalMultispeciesDataTypeTrait for T>0.
 */
template <std::size_t NOI> struct NodalMultispeciesDataTypeTrait<FiniteT, NOI> {
  using Type = NodalDataMultispecies<NOI>;
};

/**
 * @brief Specialization of @ref NodalMultispeciesDataTypeTrait for T=0.
 */
template <std::size_t NOI> struct NodalMultispeciesDataTypeTrait<ZeroT, NOI> {
  using Type = qcmesh::mesh::EmptyData;
};

template <class TemperatureMode, std::size_t NOI>
using NodalMultispeciesDataType =
    typename NodalMultispeciesDataTypeTrait<TemperatureMode, NOI>::Type;

/**
 * @brief Trait on how to pack / unpack halo nodal data from / to a mesh.
 */
template <class TemperatureMode> struct NodalHaloDataAccess;

/**
 * @brief Specialization of @ref NodalHaloDataAccess for T>0.
 *
 * Packs / unpacks `halo_node_thermal_points` and `halo_node_entropy`.
 */
template <> struct NodalHaloDataAccess<FiniteT> {
  template <class Mesh>
  static std::tuple<NodalThermalDataFiniteT,
                    NodalDataMultispecies<traits::N_IMPURITIES<Mesh>>>
  extract_data_from_mesh(const Mesh &mesh, const qcmesh::mesh::NodeId id) {
    return std::make_tuple(
        NodalThermalDataFiniteT{mesh.get_thermal_point_of_node(id),
                                mesh.get_entropy_of_node(id)},
        mesh.get_nodal_data_of_node(id));
  }
  template <class Mesh>
  static void
  insert_data_into_mesh(Mesh &mesh, const qcmesh::mesh::NodeId id,
                        const NodalThermalDataFiniteT &data,
                        const NodalDataMultispecies<traits::N_IMPURITIES<Mesh>>
                            &multi_species_data) {
    mesh.halo_node_thermal_points.insert({id, data.thermal_point});
    mesh.halo_node_entropy.emplace(id, data.entropy);
    mesh.halo_node_nodal_data.insert({id, multi_species_data});
  }
};

/**
 * @brief Specialization of @ref NodalHaloDataAccess for T=0.
 */
template <> struct NodalHaloDataAccess<ZeroT> {
  template <class Mesh>
  static std::tuple<NodalThermalDataZeroT, qcmesh::mesh::EmptyData>
  extract_data_from_mesh(const Mesh &, const qcmesh::mesh::NodeId) {
    return std::make_tuple(NodalThermalDataZeroT{}, qcmesh::mesh::EmptyData{});
  }
  template <class Mesh>
  static void insert_data_into_mesh(Mesh &, const qcmesh::mesh::NodeId,
                                    const NodalThermalDataZeroT &,
                                    const qcmesh::mesh::EmptyData &) {}
};

/**
 * @brief Extract thermal nodal data from a mesh, based on the trait @ref NodalHaloDataAccess.
 */
template <class Mesh>
std::tuple<NodalThermalDataType<typename Mesh::TemperatureMode>,
           NodalMultispeciesDataType<typename Mesh::TemperatureMode,
                                     traits::N_IMPURITIES<Mesh>>>
extract_halo_data_from_mesh(const Mesh &mesh, const qcmesh::mesh::NodeId id) {
  return NodalHaloDataAccess<
      typename Mesh::TemperatureMode>::extract_data_from_mesh(mesh, id);
}

/**
 * @brief Insert thermal nodal data into a mesh, based on the trait @ref NodalHaloDataAccess.
 */
template <class Mesh>
void insert_halo_data_into_mesh(
    Mesh &mesh, const qcmesh::mesh::NodeId id,
    const NodalThermalDataType<typename Mesh::TemperatureMode> &thermal_data,
    const NodalMultispeciesDataType<typename Mesh::TemperatureMode,
                                    traits::N_IMPURITIES<Mesh>>
        &multispecies_data) {
  NodalHaloDataAccess<typename Mesh::TemperatureMode>::insert_data_into_mesh(
      mesh, id, thermal_data, multispecies_data);
}

} // namespace traits

/**
 * @brief Overload to override call in @ref qcmesh::mesh::apply_change_set.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
void remove_cells(QCMesh<D, K, NIDOF, MeshParameters...> &mesh,
                  const std::vector<qcmesh::mesh::SimplexCellId> &cells) {
  mesh.remove_cells(cells);
}

/**
 * @brief Returns a vector of nodal data for all vertices occuring in cell_drafts.
 *
 * Overrides the call in @ref qcmesh::mesh::apply_change_set_distributed_leaving_cells.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF,
          typename... MeshParameters>
auto collect_nodes(const QCMesh<D, K, NIDOF, MeshParameters...> &mesh,
                   const std::vector<QCCellDraft> &cell_drafts) {
  using Mesh = QCMesh<D, K, NIDOF, MeshParameters...>;
  using ThermalDataT =
      traits::NodalThermalDataType<typename Mesh::TemperatureMode>;
  using MultispeciesDataT =
      traits::NodalMultispeciesDataType<typename Mesh::TemperatureMode,
                                        traits::N_IMPURITIES<Mesh>>;
  auto vertices = std::vector<QCVertexDraft<ThermalDataT, MultispeciesDataT>>{};
  auto vertex_keys = std::unordered_set<qcmesh::mesh::NodeId>{};
  for (const auto &cell_draft : cell_drafts)
    for (const auto &node_id : cell_draft.cell.nodes)
      if (!vertex_keys.emplace(node_id).second) {
        const auto node = qcmesh::mesh::deref_node_id(mesh, node_id);
        const auto &[thermal_data, multispecies_data] =
            traits::extract_halo_data_from_mesh(mesh, node_id);
        const auto draft = QCVertexDraft<ThermalDataT, MultispeciesDataT>{
            node_id,
            node.process_rank,
            node.position,
            mesh.get_material_index_of_node(node_id),
            mesh.get_site_type_of_node(node_id),
            thermal_data,
            multispecies_data};
        vertices.push_back(draft);
      }
  return vertices;
}

/**
 * @brief Insert a halo vertex into a mesh, unpacking nodal data.
 */
template <std::size_t D, std::size_t K, std::size_t NIDOF, typename NodalData>
void insert_halo_node(
    QCMesh<D, K, NIDOF, NodalData> &mesh,
    const QCVertexDraft<
        traits::NodalThermalDataType<
            typename QCMesh<D, K, NIDOF, NodalData>::TemperatureMode>,
        traits::NodalMultispeciesDataType<
            typename QCMesh<D, K, NIDOF, NodalData>::TemperatureMode,
            std::tuple_size<typename NodalData::ConcentrationPoint>{}>>
        &vertex_draft) {
  if (mesh.nodes.find(vertex_draft.id) != mesh.nodes.end())
    return;
  if (mesh.halo_nodes.find(vertex_draft.id) != mesh.halo_nodes.end())
    return;

  auto new_node =
      qcmesh::mesh::Node<D, QCNodalData>{.position = vertex_draft.point};
  new_node.id = vertex_draft.id;
  new_node.process_rank = vertex_draft.owner_rank;
  new_node.data.type = vertex_draft.site_type;
  mesh.halo_nodes.insert({vertex_draft.id, new_node});
  mesh.halo_node_material_indices.insert(
      {vertex_draft.id, vertex_draft.material_id});

  traits::insert_halo_data_into_mesh(mesh, vertex_draft.id,
                                     vertex_draft.thermal_data,
                                     vertex_draft.multi_species_data);
}

} // namespace meshing

/**
 * @brief Implement UndeformedLatticeBasis trait for QCMesh
 */
template <std::size_t NIDOF, typename... MeshParameters>
struct qcmesh::mesh::traits::UndeformedLatticeBasis<
    meshing::QCMesh<3, 3, NIDOF, MeshParameters...>> {
  constexpr static std::size_t DIMENSION = 3;
  using CellType = meshing::QCCell;
  static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  undeformed_lattice_basis(
      const meshing::QCMesh<3, 3, NIDOF, MeshParameters...> &mesh,
      const CellType &cell) {
    const auto material_idx =
        mesh.material_idx_of_nodes[mesh.nodes.at(cell.nodes[0])
                                       .data.mesh_index];
    return mesh.materials[material_idx].basis_vectors;
  }
};

namespace meshing {

template <typename... MeshParameters>
void clean_mesh(QCMesh<3, 3, 2, MeshParameters...> &mesh) {
  if (mesh.cells.size() != mesh.simplex_index_to_key.size())
    throw exception::QCException{"clean_mesh: noof cells does not match the "
                                 "size of simplex_index_to_key map"};

  // upto here, node key maps are correct.
  // store remaining nodes keys and nodal data.
  const auto unreferenced_nodes = mesh.find_unreferenced_nodes();
  mesh.remove_nodes(unreferenced_nodes);
  // renumber mesh nodes (reorder indices according to ghost indices)
  const auto permutation = renumber_mesh_nodes(mesh);
  mesh.repatoms.permute(permutation);
  mesh.repatoms.assign_ghost_do_fs(mesh.global_ghost_indices);

  mesh.build_sampling_atoms();
  // assign repatom weights using sampling atoms
  mesh.assign_repatom_weights();
  setup_process_boundary(mesh);
}

/**
 * @brief Repair a mesh by applying edge removal / face removal operations.
 */
template <typename NodalData>
void repair_mesh(QCMesh<3, 3, 2, NodalData> &mesh,
                 const std::size_t max_iterations = 100) {
  constexpr std::size_t Dimension = 3;
  using Mesh = QCMesh<Dimension, Dimension, 2, NodalData>;
  using ModifierEdgeEraser =
      qcmesh::mesh::EdgeEraser<QCNodalData, QCCellData<3, 3>>;
  using ModifierFaceEraser =
      qcmesh::mesh::FaceEraser<QCNodalData, QCCellData<3, 3>>;

  /**
   * @brief Called before applying a modifier to the provided element.
   * Gathers elements from other mpi ranks
   */
  const auto exchange_halos =
      [](Mesh &mesh, const std::vector<qcmesh::mesh::CellAddress<
                         int, qcmesh::mesh::SimplexCellId>> &cell_handles) {
        const auto mpi_rank = boost::mpi::communicator{}.rank();
        // assume correct node cells adjacencies exist before this function is called.
        auto cell_ids = std::vector<qcmesh::mesh::SimplexCellId>{};
        cell_ids.reserve(cell_handles.size());
        for (const auto cell_address : cell_handles)
          if (cell_address.process_id == mpi_rank)
            cell_ids.push_back(cell_address.cell_handle);
        meshing::exchange_halos(mesh, cell_ids);
        build_adjacency_halo<3>(mesh);
      };

  const auto modifiers =
      std::make_tuple(ModifierEdgeEraser{}, ModifierFaceEraser{});
  auto driver = qcmesh::mesh::GreedyDriver(max_iterations);
  const auto compute_residual =
      [&](const qcmesh::mesh::SimplexCellId cell_handle) {
        const auto cell = qcmesh::mesh::deref_cell_id(mesh, cell_handle);
        return qcmesh::geometry::simplex_metric_residual(
            qcmesh::mesh::simplex_cell_points(mesh, cell));
      };
  const auto gather_local_bad_elements = [](auto &mesh) {
    return qcmesh::mesh::gather_local_independent_bad_elements(
        mesh, REMESH_TOLERANCE);
  };
  const auto gather_bad_elements = [](auto &mesh) {
    return qcmesh::mesh::gather_bad_elements(mesh, REMESH_TOLERANCE);
  };

  qcmesh::mesh::optimize_local_mesh<QCCellDraft, qcmesh::mesh::SimplexCellId>(
      mesh, gather_local_bad_elements, driver, compute_residual,
      qcmesh::mesh::apply_change_set<Mesh, QCCellData<3, 3>>, modifiers);

  renumber_cells(mesh);

  const auto mpi_rank = boost::mpi::communicator{}.rank();
  qcmesh::mesh::optimize_global_mesh<QCCellDraft, qcmesh::mesh::SimplexCellId>(
      mesh, mpi_rank, gather_bad_elements, exchange_halos, driver,
      compute_residual,
      qcmesh::mesh::apply_change_set_distributed_leaving_nodes<
          Mesh, QCCellData<3, 3>>,
      modifiers);
  clean_mesh(mesh);
  mesh.initialize_elements_atomistic_flag();
}

} // namespace meshing
