// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct to hold basic node information
 */

#pragma once

#include "qcmesh/mesh/node.hpp"
#include <array>

namespace meshing {

/**
 * @brief Node of the QC mesh. Contains id, owning rank of MPI process,
 * position in euclidean space and other nodal data.
 */
template <class ThermalData, class MultiSpeciesData> struct QCVertexDraft {
  qcmesh::mesh::NodeId id{};
  int owner_rank{};
  std::array<double, 3> point{};
  int material_id{};
  int site_type{};
  ThermalData thermal_data{};
  MultiSpeciesData multi_species_data{};
};

} // namespace meshing
