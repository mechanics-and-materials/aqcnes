// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct implementation to hold thermal information
 */

#pragma once

#include "meshing/nodal_data_multispecies.hpp"
#include "meshing/temperature_mode.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/mesh/node.hpp"
#include <array>
#include <tuple>

namespace meshing {

/**
 * @brief Nodal thermal data for the zero temperature case.
 */
struct NodalThermalDataZeroT {};

/**
 * @brief Nodal thermal data for the finite temperature case.
 */
struct NodalThermalDataFiniteT {
  std::array<double, 2> thermal_point{};
  double entropy{};
};

/**
 * @brief Implementation of @ref traits::ThermalCoordinates for @ref NodalThermalDataFiniteT.
 */
template <> struct traits::ThermalCoordinates<NodalThermalDataFiniteT> {
  constexpr static std::size_t DIMENSION = 2;
  static const std::array<double, DIMENSION> &
  thermal_coordinates(const NodalThermalDataFiniteT &data) {
    return data.thermal_point;
  }
};
/**
 * @brief Implementation of @ref traits::Entropy for @ref NodalThermalDataFiniteT.
 */
template <> struct traits::Entropy<NodalThermalDataFiniteT> {
  static double entropy(const NodalThermalDataFiniteT &data) {
    return data.entropy;
  }
};

namespace traits {

/**
 * @brief Queries the type depending on the temperature mode (either @ref NodalThermalDataZeroT or @ref NodalThermalDataFiniteT).
 */
template <class TemperatureMode> struct NodalThermalDataTypeTrait;

/**
 * @brief Specialization of @ref NodalThermalDataType for T>0.
 */
template <> struct NodalThermalDataTypeTrait<FiniteT> {
  using Type = NodalThermalDataFiniteT;
};

/**
 * @brief Specialization of @ref NodalThermalDataType for T=0.
 */
template <> struct NodalThermalDataTypeTrait<ZeroT> {
  using Type = NodalThermalDataZeroT;
};

/**
 * @brief Shorthand alias for @ref NodalThermalDataTypeTrait::Type.
 */
template <class TemperatureMode>
using NodalThermalDataType =
    typename NodalThermalDataTypeTrait<TemperatureMode>::Type;

} // namespace traits

} // namespace meshing
