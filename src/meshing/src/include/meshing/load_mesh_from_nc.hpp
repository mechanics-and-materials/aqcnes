// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementations to load a mesh NetCdf file
 * @author G. Bräunlich
 */

#pragma once

#include "input/userinput.hpp"
#include "meshing/array_from_vec.hpp"
#include "meshing/build_repatoms.hpp"
#include "meshing/create_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "meshing/temperature_mode.hpp"
#include "netcdf_io/load_nc_file.hpp"
#include <filesystem>

namespace meshing {

namespace traits {
/**
 * @brief Helper trait to decide which nodal data type to use depending on @ref QCMesh::TemperatureMode.
 */
template <std::size_t NOI, class TemperatureMode> struct MeshNodalData;

template <std::size_t NOI> struct MeshNodalData<NOI, FiniteT> {
  using NodalDataType = netcdf_io::RestartNodalDataFiniteT<NOI>;
};
template <std::size_t NOI> struct MeshNodalData<NOI, ZeroT> {
  using NodalDataType = netcdf_io::RestartNodalDataZeroT<NOI>;
};
} // namespace traits

/**
 * @brief Load a restart NetCdf file to a @ref RestartCellData instance and a @ref meshing::QcMesh instance.
 *
 * Ignores data for the external force applicator and boundary conditions.
 */
template <class MaterialType,
          class MultispeciesDataType = QCMesh<>::NodalDataType,
          std::size_t ThermalDof = QCMesh<>::THERMAL_DOF,
          std::size_t Noi = QCMesh<>::NOI>
std::tuple<netcdf_io::RestartData<MaterialType::DIMENSION>,
           meshing::QCMesh<MaterialType::DIMENSION, MaterialType::DIMENSION,
                           ThermalDof, MultispeciesDataType>>
load_restart_mesh_from_nc(
    const std::filesystem::path &path,
    const std::vector<MaterialType> &materials,
    const std::array<std::optional<PeriodicityParameters>,
                     MaterialType::DIMENSION> &periodicity =
        std::array<std::optional<PeriodicityParameters>,
                   MaterialType::DIMENSION>{}) {
  using Mesh = meshing::QCMesh<MaterialType::DIMENSION, MaterialType::DIMENSION,
                               ThermalDof, MultispeciesDataType>;
  using NodalDataType = typename traits::MeshNodalData<
      Noi, typename Mesh::TemperatureMode>::NodalDataType;

  constexpr std::size_t DIM = MaterialType::DIMENSION;
  const auto [bare_mesh, data] =
      netcdf_io::load_restart_file<DIM, DIM, NodalDataType>(path);
  auto mesh = create_mesh<MultispeciesDataType, ThermalDof>(
      bare_mesh.nodes, bare_mesh.cells, materials, materials[0].offset_coeffs,
      periodicity);
  mesh.periodic_lengths = data.periodic_lengths;
  if (boost::mpi::communicator{}.size() > 1) {
    meshing::redistribute(mesh);
    meshing::redistribute(mesh);
  }
  mesh.initialize_periodic_offsets();
  return std::make_tuple(data, mesh);
}

/**
 * @brief Load a mesh NetCdf file to a @ref meshing::QcMesh instance.
 */
template <class MaterialType,
          class MultispeciesDataType = QCMesh<>::NodalDataType,
          std::size_t ThermalDof = QCMesh<>::THERMAL_DOF>
meshing::QCMesh<MaterialType::DIMENSION, MaterialType::DIMENSION, ThermalDof,
                MultispeciesDataType>
load_mesh_from_nc(const std::filesystem::path &path,
                  const std::vector<MaterialType> &materials,
                  const std::array<std::optional<PeriodicityParameters>,
                                   MaterialType::DIMENSION> &periodicity =
                      std::array<std::optional<PeriodicityParameters>,
                                 MaterialType::DIMENSION>{}) {
  constexpr std::size_t DIM = MaterialType::DIMENSION;
  const auto [bare_mesh, data] = netcdf_io::load_mesh_file<DIM, DIM>(path);
  auto mesh = create_mesh<MultispeciesDataType>(
      bare_mesh.nodes, bare_mesh.cells, materials, materials[0].offset_coeffs,
      periodicity);
  mesh.atomistic_domain = qcmesh::geometry::Box<DIM>{data.atomistic_box_lower,
                                                     data.atomistic_box_upper};
  if (!data.have_lattice_vectors)
    mesh.initialize_bravais_lattice(materials);
  return mesh;
}

/**
 * @brief Load a mesh NetCdf file to a @ref meshing::QcMesh instance.
 */
template <class MaterialType,
          class MultispeciesDataType = QCMesh<>::NodalDataType,
          std::size_t ThermalDof = QCMesh<>::THERMAL_DOF,
          std::size_t Noi = QCMesh<>::NOI>
meshing::QCMesh<MaterialType::DIMENSION, MaterialType::DIMENSION, ThermalDof,
                MultispeciesDataType>
load_mesh_from_nc(
    const std::filesystem::path &path,
    const std::vector<MaterialType> &materials, const double phi_ref,
    const double t_ref,
    const std::array<double, Noi> &initial_impurity_concentrations,
    const double node_effective_radius, const double minimum_barycenter_weight,
    const std::array<std::optional<PeriodicityParameters>,
                     MaterialType::DIMENSION> &periodicity,
    const std::optional<std::array<std::array<double, 3>, 2>>
        &fixed_box_boundaries) {
  auto mesh = load_mesh_from_nc<MaterialType, MultispeciesDataType, ThermalDof>(
      path, materials, periodicity);
  build_repatoms(
      mesh, phi_ref, t_ref,
      meshing::NodalDataMultispecies<Noi>{initial_impurity_concentrations},
      fixed_box_boundaries);
  mesh.domain_bound =
      geometry::distributed_bounding_box(mesh.repatoms.locations);
  mesh.initialize_element_vectors();
  for (auto &[_, cell] : mesh.cells)
    cell.data.density = cell_density(cell, mesh);
  mesh.node_effective_radius = node_effective_radius;
  mesh.minimum_barycenter_weight = minimum_barycenter_weight;
  mesh.build_sampling_atoms();
  mesh.assign_repatom_weights();

  // other element data containers were already
  // allocated while reading the restart file

  mesh.initialize_elements_atomistic_flag();
  if (boost::mpi::communicator{}.size() > 1) {
    meshing::redistribute(mesh);
    meshing::redistribute(mesh);
  }
  mesh.initialize_periodic_offsets();
  return mesh;
}

/**
 * @brief Load a mesh NetCdf file to a @ref meshing::QcMesh instance.
 * The "mesh" format will be chosen if `input.restart_file_name` is unset,
 * the "restart" format otherwise.
 */
template <class MaterialType>
std::tuple<netcdf_io::RestartData<MaterialType::DIMENSION>,
           meshing::QCMesh<MaterialType::DIMENSION, MaterialType::DIMENSION,
                           MaterialType::THERMAL_DOF,
                           typename MaterialType::NodalDataPoint>>
load_nc(const input::Userinput &input,
        const std::vector<MaterialType> &materials) {
  if (!input.restart_file_name.has_value()) {
    auto mesh =
        load_mesh_from_nc<MaterialType, typename MaterialType::NodalDataPoint,
                          MaterialType::THERMAL_DOF,
                          MaterialType::NOOF_IMPURITIES>(
            input.mesh_file, materials, input.Phi_ref, input.T_ref,
            array_from_vec<MaterialType::NOOF_IMPURITIES>(
                input.initial_impurity_concentrations),
            input.node_effective_radius, input.minimum_barycenter_weight,
            input.periodicity, input.fixed_box);
    return std::make_tuple(netcdf_io::RestartData<MaterialType::DIMENSION>{},
                           mesh);
  }
  return load_restart_mesh_from_nc<
      MaterialType, typename MaterialType::NodalDataPoint,
      MaterialType::THERMAL_DOF, MaterialType::NOOF_IMPURITIES>(
      input.restart_file_name.value(), materials, input.periodicity);
}

} // namespace meshing
