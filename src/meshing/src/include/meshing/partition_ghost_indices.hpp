// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Function implementation to find own vs ghost nodes
 * @author P. Gupta, S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include <numeric>

namespace meshing {

template <class Mesh> std::vector<bool> ghost_mask(const Mesh &mesh) {

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  std::vector<bool> if_ghosts(mesh.nodes.size(), false);

  // first identify the current ghost node indices
  for (std::size_t i_proc = 0; i_proc < mesh.connected_node_procs.size();
       i_proc++) {
    const auto node_proc = mesh.connected_node_procs[i_proc];

    for (std::size_t i_node = 0; i_node < mesh.connected_nodes[i_proc].size();
         i_node++) {
      const auto node_index = mesh.connected_nodes[i_proc][i_node];

      if (node_proc == mpi_rank)
        throw exception::QCException(
            "ghost_mask: ghost node belongs to the same proc");

      if (node_index >= if_ghosts.size())
        throw exception::QCException("ghost_mask: node_index out of bounds");

      if (!if_ghosts[node_index] && node_proc < mpi_rank) {
        // a node might be visited multiple times,
        // so we don't assign ghost indices here
        if_ghosts[node_index] = true;
      }
    }
  }
  // Check if a node is periodic image of some original
  for (const auto &[node_index, _] : mesh.original_keys_of_periodic_images) {
    if (!if_ghosts[node_index])
      if_ghosts[node_index] = true;
  }

  return if_ghosts;
}

template <class Mesh> std::size_t count_repatoms(const Mesh &mesh) {
  const auto n_ghosts = mesh.global_ghost_indices.size();
  const auto n_repatoms =
      qcmesh::mpi::all_reduce_sum(mesh.nodes.size() - n_ghosts);
  return n_repatoms;
}

template <class Mesh>
std::tuple<std::vector<std::size_t>, std::vector<std::size_t>>
partition_ghost_indices(const Mesh &mesh) {
  auto out_locals = std::vector<std::size_t>{};
  auto out_ghosts = std::vector<std::size_t>{};

  const auto if_ghosts = ghost_mask(mesh);

  const std::size_t noof_ghosts =
      std::accumulate(if_ghosts.begin(), if_ghosts.end(), 0);

  if (noof_ghosts > mesh.nodes.size())
    throw exception::QCException("partition_ghost_indices: noof ghosts is more "
                                 "than the total mesh nodes");

  out_locals.reserve(mesh.nodes.size() - noof_ghosts);
  out_ghosts.reserve(noof_ghosts);
  for (std::size_t i_node = 0; i_node < mesh.nodes.size(); i_node++) {
    if (if_ghosts[i_node])
      out_ghosts.push_back(i_node);
    else
      out_locals.push_back(i_node);
  }

  return std::make_tuple(out_locals, out_ghosts);
}
} // namespace meshing
