// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Connect boundary cells over processors
 * @authors P. Gupta
 */

#pragma once
#include "meshing/build_periodic_connectivity.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include <set>
#include <unordered_set>

namespace meshing {

/**
 * @brief Set process boundary flag for vertices connected to other processes.
 */
template <class Mesh> void setup_process_boundary(Mesh &mesh) {
  for (const auto &connected_nodes : mesh.connected_nodes)
    for (const auto idx : connected_nodes)
      mesh.nodes.at(mesh.node_index_to_key[idx]).data.proc_boundary_flag = true;
}

template <std::size_t D, class Mesh>
void build_interproc_connectivity(Mesh &mesh) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = world.size();
  const auto mpi_rank = world.rank();

  // mark nodes on boundary
  std::vector<bool> visited_nodes(mesh.nodes.size(), false);

  std::vector<qcmesh::mesh::NodeId> boundary_node_ids;
  boundary_node_ids.reserve(mesh.nodes.size());

  // reset local boundary flags and proc boundary flags
  for (auto &[_, node] : mesh.nodes) {
    node.data.proc_boundary_flag = false;
  }

  for (auto &[_, cell] : mesh.cells) {
    for (std::size_t d = 0; d < cell.neighbors.size(); d++) {
      if (!cell.neighbors[d].has_value() ||
          !qcmesh::mesh::mesh_has_cell(mesh, cell.neighbors[d].value(),
                                       mpi_rank)) {
        // d face is a boundary face
        const auto n_neighbors = cell.neighbors.size();
        for (std::size_t m = 1; m < n_neighbors; m++) {
          const auto &node = mesh.nodes.at(cell.nodes[(d + m) % n_neighbors]);
          const auto node_index = node.data.mesh_index;

          if (!visited_nodes[node_index]) {
            visited_nodes[node_index] = true;
            boundary_node_ids.emplace_back(node.id);

            // Don't set the boundary flag here, we will only do so,
            // if its periodic
          }
        }
      }
    }
  }

  boundary_node_ids.shrink_to_fit();

  const auto noof_boundary_nodes = boundary_node_ids.size();

  std::unordered_set<qcmesh::mesh::NodeId> boundary_node_keys;

  boundary_node_keys.reserve(noof_boundary_nodes);

  for (const auto node_id : boundary_node_ids)
    boundary_node_keys.emplace(node_id);
  auto boundary_node_ids_per_proc =
      std::vector<std::vector<qcmesh::mesh::NodeId>>(mpi_size);
  boost::mpi::all_gather(world, boundary_node_ids, boundary_node_ids_per_proc);

  std::set<std::pair<qcmesh::mesh::NodeId, std::size_t>>
      connected_nodes_ordered;

  mesh.connected_nodes.clear();
  mesh.connected_node_procs.clear();

  mesh.connected_nodes.reserve(mpi_size - 1);
  mesh.connected_node_procs.reserve(mpi_size - 1);

  // arrange nodes on processor boundaries
  std::size_t previous_node_rank = 0;
  for (std::size_t node_rank = 0; node_rank < boundary_node_ids_per_proc.size();
       node_rank++) {
    if (node_rank == static_cast<std::size_t>(mpi_rank))
      continue;
    for (const auto other_proc_boundary_node_key :
         boundary_node_ids_per_proc[node_rank]) {
      if (previous_node_rank < node_rank) {
        if (!connected_nodes_ordered.empty()) {
          mesh.connected_node_procs.emplace_back(previous_node_rank);
          mesh.connected_nodes.emplace_back(std::vector<std::size_t>{});

          std::transform(
              connected_nodes_ordered.begin(), connected_nodes_ordered.end(),
              std::back_inserter(
                  mesh.connected_nodes[mesh.connected_nodes.size() - 1]),
              [](const auto &x) { return x.second; });
        }

        connected_nodes_ordered.clear();
        previous_node_rank = node_rank;
      }

      if (boundary_node_keys.find(other_proc_boundary_node_key) !=
          boundary_node_keys.end()) {
        connected_nodes_ordered.emplace(
            other_proc_boundary_node_key,
            mesh.nodes.at(other_proc_boundary_node_key).data.mesh_index);
        // nodeRank's connected

        if (mesh.nodes.at(other_proc_boundary_node_key).data.mesh_index >=
            mesh.nodes.size())
          throw exception::QCException{
              "mesh_index at other_proc_boundary_node_key out of bounds"};

        // just build connected node and connected node procs now.
        // everything is sorted according to global keys
      }
    }
  }

  // add the last rank which wasn't added

  if (!connected_nodes_ordered.empty()) {
    mesh.connected_node_procs.push_back(previous_node_rank);
    mesh.connected_nodes.push_back(std::vector<std::size_t>{});

    std::transform(connected_nodes_ordered.begin(),
                   connected_nodes_ordered.end(),
                   std::back_inserter(
                       mesh.connected_nodes[mesh.connected_nodes.size() - 1]),
                   [](const auto &x) { return x.second; });
  }

  connected_nodes_ordered.clear();

  mesh.connected_nodes.shrink_to_fit();
  mesh.connected_node_procs.shrink_to_fit();
  if (mesh.has_periodic_dimensions) {
    build_periodic_connectivity<D>(mesh);
  }
}
} // namespace meshing
