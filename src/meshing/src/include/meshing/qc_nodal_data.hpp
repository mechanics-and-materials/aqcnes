// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct for basic nodal data
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "meshing/traits.hpp"
#include <cstddef>

namespace meshing {

struct QCNodalData {
  std::size_t mesh_index{};
  int type{};
  bool proc_boundary_flag = false;
  bool periodic_boundary_flag = false;
};

template <std::size_t D>
struct traits::PeriodicBoundaryFlag<qcmesh::mesh::Node<D, QCNodalData>> {
  static bool
  periodic_boundary_flag(const qcmesh::mesh::Node<D, QCNodalData> &node) {
    return node.data.periodic_boundary_flag;
  }
};

/**
 * @brief Implementation of @ref traits::NodeType for @ref Node.
 */
template <std::size_t D>
struct traits::NodeType<qcmesh::mesh::Node<D, QCNodalData>> {
  static int node_type(const qcmesh::mesh::Node<D, QCNodalData> &node) {
    return node.data.type;
  }
};

} // namespace meshing
