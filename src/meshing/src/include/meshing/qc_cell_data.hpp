// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Struct for cell data in the QC mesh
 * @authors P. Gupta
 */

#pragma once

#include "meshing/traits.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include <array>
#include <vector>

namespace meshing {

template <std::size_t D, std::size_t K> struct QCCellData {
  std::size_t mesh_index{};
  double volume = 0.;
  double quality = 1.0e9;
  double jacobian = 1.0;
  double density = 0.0;
  std::array<std::array<double, K>, K> lattice_vectors{};
  std::vector<std::array<double, D>> offset_vectors{};
  bool is_atomistic = false;
  std::size_t material_index{};
  bool is_mono_lattice = false;
};

/**
 * @brief Implementation of @ref traits::Density for @ref QCCellData.
 */
template <std::size_t D, std::size_t K>
struct traits::Density<QCCellData<D, K>> {
  static double density(const QCCellData<D, K> &data) { return data.density; }
};

/**
 * @brief Implementation of @ref traits::MaterialId for @ref QCCellData.
 */
template <std::size_t D, std::size_t K>
struct traits::MaterialId<QCCellData<D, K>> {
  static std::size_t material_id(const QCCellData<D, K> &data) {
    return data.material_index;
  }
};

} // namespace meshing

/**
 * @brief Implement @ref qcmesh::mesh::traits::LatticeBasis trait for @ref QCCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::LatticeBasis<meshing::QCCellData<D, D>> {
  constexpr static std::size_t DIMENSION = D;
  static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(meshing::QCCellData<D, D> &data) {
    return data.lattice_vectors;
  }
  static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(const meshing::QCCellData<D, D> &data) {
    return data.lattice_vectors;
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::Volume trait for @ref QCCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::Volume<meshing::QCCellData<D, D>> {
  static double &volume(meshing::QCCellData<D, D> &data) { return data.volume; }
  static double volume(const meshing::QCCellData<D, D> &data) {
    return data.volume;
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::Quality trait for @ref QCCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::Quality<meshing::QCCellData<D, D>> {
  static double &quality(meshing::QCCellData<D, D> &data) {
    return data.quality;
  }
  static double quality(const meshing::QCCellData<D, D> &data) {
    return data.quality;
  }
};

/**
 * @brief Implementation of @ref qcmesh::mesh::traits::Jacobian trait for @ref QCCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::Jacobian<meshing::QCCellData<D, D>> {
  static double &jacobian(meshing::QCCellData<D, D> &data) {
    return data.jacobian;
  }
  static double jacobian(const meshing::QCCellData<D, D> &data) {
    return data.jacobian;
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::IsAtomistic trait for @ref QCCellData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::IsAtomistic<meshing::QCCellData<D, D>> {
  static bool &is_atomistic(meshing::QCCellData<D, D> &data) {
    return data.is_atomistic;
  }
  static bool is_atomistic(const meshing::QCCellData<D, D> &data) {
    return data.is_atomistic;
  }
};
