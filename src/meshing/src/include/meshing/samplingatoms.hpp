// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Contains structure for a sampling atom
 * and the sampling atom container.
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "materials/material.hpp"
#include "meshing/lattice_site.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"

namespace array_ops = qcmesh::array_ops;

namespace sampling_atoms {

template <std::size_t Dimension, std::size_t NIDOF, typename NodalData>
class SamplingAtomContainer {
public:
  using AtomicSite = meshing::LatticeSite<Dimension, NIDOF, NodalData>;

  std::vector<std::array<double, Dimension>> nodal_locations{};
  std::vector<std::array<double, Dimension>> central_locations{};

#ifdef FINITE_TEMPERATURE
  std::vector<std::array<double, NIDOF>> central_ido_fs{};
#endif
  std::vector<NodalData> central_nodal_data{};

  std::vector<qcmesh::mesh::SimplexCellId> cell_key_of_central_atoms{};

  std::vector<double> nodal_weights{};
  std::vector<double> central_weights{};

  std::vector<double> nodal_cut_offs{};
  std::vector<double> central_cut_offs{};

  std::vector<int> central_material_ids{};

  std::vector<std::vector<AtomicSite>> nodal_sampling_atom_neighbours{};
  std::vector<std::vector<AtomicSite>> central_sampling_atom_neighbours{};

  std::vector<std::array<double, Dimension>>
      nodal_displacements_overlap_exchange{};

  std::vector<double> nodal_centro_symmetry{};

  std::vector<double> nodal_energies{};
  std::vector<double> central_energies{};

  void clear() {
    nodal_weights.clear();
    central_weights.clear();

    nodal_locations.clear();
    central_locations.clear();

    nodal_cut_offs.clear();
    central_cut_offs.clear();

    central_material_ids.clear();

#ifdef FINITE_TEMPERATURE
    central_ido_fs.clear();
#endif
    central_nodal_data.clear();

    cell_key_of_central_atoms.clear();

    nodal_sampling_atom_neighbours.clear();
    central_sampling_atom_neighbours.clear();

    nodal_displacements_overlap_exchange.clear();

    nodal_centro_symmetry.clear();

    nodal_energies.clear();
    central_energies.clear();
  }

  void resize_nodal_sampling_atoms(const std::size_t &n) {
    // nodal sampling atom locations have to be stored
    // to compute the deformation. Either store displacement
    // or store locations.
    nodal_locations.resize(n, std::array<double, Dimension>{});
    nodal_cut_offs.resize(n, 0.0);
    nodal_weights.resize(n, 0.0);
    nodal_sampling_atom_neighbours.resize(n);

    nodal_displacements_overlap_exchange.resize(
        n, std::array<double, Dimension>{});

    nodal_centro_symmetry.resize(n, 0.0);

    nodal_energies.resize(n, 0.0);
  }

  void push_back_nodal() {
    nodal_locations.push_back(std::array<double, Dimension>{});
    nodal_cut_offs.push_back(0.0);
    nodal_weights.push_back(0.0);
    nodal_sampling_atom_neighbours.push_back(std::vector<AtomicSite>{});

    nodal_displacements_overlap_exchange.push_back(
        std::array<double, Dimension>{});

    nodal_centro_symmetry.push_back(0.0);

    nodal_energies.push_back(0.0);
  }

  void get_central_neighbour_cluster(
      const std::size_t &index,
      std::vector<std::array<double, Dimension>> &clusters) {
    clusters.clear();
    clusters.reserve(central_sampling_atom_neighbours[index].size());

    for (const auto &neighbour : central_sampling_atom_neighbours[index]) {
      clusters.push_back(neighbour.location_);
    }
  }

  void get_central_neighbour_cluster(
      const std::size_t index,
      std::vector<std::array<double, Dimension>> &clusters,
      std::vector<NodalData> &cluster_nodal_data,
      std::vector<int> &cluster_indices, const double custom_cutoff = 0.0) {
    const auto cutoff =
        (custom_cutoff < 1e-6) ? central_cut_offs[index] : custom_cutoff;

    clusters.clear();
    cluster_indices.clear();
    cluster_nodal_data.clear();

    clusters.reserve(central_sampling_atom_neighbours[index].size());
    cluster_indices.reserve(central_sampling_atom_neighbours[index].size());
    cluster_nodal_data.reserve(central_sampling_atom_neighbours[index].size());

    int neighbour_idx = 0;

    for (const auto &neighbour : central_sampling_atom_neighbours[index]) {
      if (array_ops::distance(neighbour.location, central_locations[index]) <
          cutoff) {
        clusters.push_back(neighbour.location);
        cluster_indices.push_back(neighbour_idx);
        cluster_nodal_data.push_back(neighbour.nodal_data);
      }
      neighbour_idx++;
    }

    clusters.shrink_to_fit();
    cluster_indices.shrink_to_fit();
  }

  void get_nodal_neighbour_cluster(
      const std::size_t &index,
      std::vector<std::array<double, Dimension>> &clusters) const {
    clusters.clear();
    clusters.reserve(nodal_sampling_atom_neighbours[index].size());

    for (const auto &neighbour : nodal_sampling_atom_neighbours[index]) {
      if (distance(neighbour.location_, nodal_locations[index]) <
          nodal_cut_offs[index])
        clusters.push_back(neighbour.location_);
    }
    clusters.shrink_to_fit();
  }

  void get_nodal_neighbour_cluster(
      const std::size_t index,
      std::vector<std::array<double, Dimension>> &clusters,
      std::vector<NodalData> &cluster_nodal_data,
      std::vector<int> &cluster_indices,
      const double custom_cutoff = 0.0) const {
    const auto cutoff =
        (custom_cutoff < 1e-6) ? nodal_cut_offs[index] : custom_cutoff;

    clusters.clear();
    cluster_indices.clear();
    cluster_nodal_data.clear();

    clusters.reserve(nodal_sampling_atom_neighbours[index].size());
    cluster_indices.reserve(nodal_sampling_atom_neighbours[index].size());
    cluster_nodal_data.reserve(nodal_sampling_atom_neighbours[index].size());

    int neighbour_idx = 0;

    for (const auto &neighbour : nodal_sampling_atom_neighbours[index]) {
      if (array_ops::distance(neighbour.location, nodal_locations[index]) <
          cutoff) {
        clusters.push_back(neighbour.location);
        cluster_indices.push_back(neighbour_idx);
        cluster_nodal_data.push_back(neighbour.nodal_data);
      }
      neighbour_idx++;
    }

    clusters.shrink_to_fit();
    cluster_indices.shrink_to_fit();
  }

  /**
   * @brief collect indices of the neighbors of (the nodal atom and neighbor j)
   */
  [[nodiscard]] std::vector<std::size_t>
  get_neighbors_duo_nodal(const std::size_t i, const std::size_t j,
                          const double cutoff) const {
    std::size_t neighbour_idx = 0;
    std::vector<std::size_t> neighbor_indices;
    for (const auto &neighbour : nodal_sampling_atom_neighbours[i]) {
      const auto d1 =
          array_ops::distance(nodal_locations[i], neighbour.location);
      const auto d2 = array_ops::distance(
          nodal_sampling_atom_neighbours[i][j].location, neighbour.location);
      if (1.0e-3 < d1 && 1.0e-3 < d2 && (d1 <= cutoff || d2 <= cutoff))
        neighbor_indices.push_back(neighbour_idx);
      neighbour_idx++;
    }
    return neighbor_indices;
  }

  /**
   * @brief collect vector of indices of the neighbors of (the nodal atom and neighbors inside cluster_indices)
   * A very specific function used to get the neighbors for a NEB computation
   * The last index in cluster_indices is the neighbor site to/from which the nodal atom is hopping
   * The final output is a vector of vector of neighbor indices, where the first entry has the union of
   * the neighbors of the nodal atom and the hopping site involved. The other entries of the output have the
   * neighbors of rest of the atoms in cluster_indices
   */
  [[nodiscard]] std::vector<std::vector<std::size_t>>
  get_neighbors_of_cluster_nodal(
      const std::size_t i, const std::vector<std::size_t> &cluster_indices,
      const double cutoff) const {
    std::vector<std::vector<std::size_t>> neighbor_indices;
    std::vector<std::size_t> neighbour_indices_moving_atom;
    std::vector<bool> within_cutoff(cluster_indices.size(), false);
    neighbor_indices.clear();
    neighbour_indices_moving_atom.clear();
    neighbor_indices.resize(cluster_indices.size() - 1);
    std::size_t neighbour_idx = 0;
    for (const auto &neighbour : nodal_sampling_atom_neighbours[i]) {
      bool no_repetition = true;
      for (std::size_t idx = 0; idx < cluster_indices.size(); idx++) {
        const auto d = array_ops::distance(
            nodal_sampling_atom_neighbours[i][cluster_indices[idx]].location,
            neighbour.location);

        no_repetition = no_repetition && (constants::geometric_tolerance < d);
        within_cutoff[idx] = false;
        if (d <= cutoff)
          within_cutoff[idx] = true;
      }

      if (no_repetition) {
        for (std::size_t idx = 0; idx < cluster_indices.size() - 1; idx++)
          if (within_cutoff[idx])
            neighbor_indices[idx].push_back(neighbour_idx);

        if (within_cutoff[cluster_indices.size() - 1])
          neighbour_indices_moving_atom.push_back(neighbour_idx);
        else {
          const auto r =
              array_ops::distance(nodal_locations[i], neighbour.location);
          if (r < cutoff)
            neighbour_indices_moving_atom.push_back(neighbour_idx);
        }
      }
      neighbour_idx++;
    }
    neighbor_indices.insert(neighbor_indices.begin(),
                            neighbour_indices_moving_atom);
    return neighbor_indices;
  }

  [[nodiscard]] double
  get_nodal_centro_symmetry(const std::size_t &index,
                            const std::vector<std::array<double, Dimension>>
                                &centro_symmetry_values) const {
    std::array<std::array<double, Dimension>, 2> mirror_dirs{};
    mirror_dirs[0].fill(0.0);
    mirror_dirs[1].fill(0.0);

    double sign{};

    double closest_neighbour_tol = 99999.9 * 99999.9;

    std::array<double, Dimension> centro_symmetry_location{};

    double centro_symmetry = 0.0;

    for (const auto &c : centro_symmetry_values) {
      for (int i = 0; i < 2; i++) {
        closest_neighbour_tol = 99999.9 * 99999.9;

        sign = (i == 0) ? 1 : -1;

        array_ops::as_eigen(centro_symmetry_location) =
            array_ops::as_eigen(nodal_locations[index]) +
            sign * array_ops::as_eigen(c);

        for (std::size_t nid = 0;
             nid < nodal_sampling_atom_neighbours[index].size(); nid++) {
          const auto d =
              array_ops::as_eigen(
                  nodal_sampling_atom_neighbours[index][nid].location) -
              array_ops::as_eigen(centro_symmetry_location);
          const auto candidate = d.dot(d);
          if (candidate < closest_neighbour_tol) {
            closest_neighbour_tol = candidate;

            array_ops::as_eigen(mirror_dirs[i]) =
                array_ops::as_eigen(
                    nodal_sampling_atom_neighbours[index][nid].location) -
                array_ops::as_eigen(nodal_locations[index]);
          }
        }
      }
      centro_symmetry += (array_ops::as_eigen(mirror_dirs[0]) +
                          array_ops::as_eigen(mirror_dirs[1]))
                             .norm();
    }

    return centro_symmetry;
  }

#ifdef FINITE_TEMPERATURE
  void get_central_neighbour_thermal_cluster(
      const std::size_t &index,
      std::vector<std::array<double, Dimension>> &cluster,
      std::vector<std::array<double, NIDOF>> &thermal_cluster,
      std::vector<NodalData> &cluster_nodal_data,
      std::vector<int> &cluster_indices) {
    cluster.clear();
    thermal_cluster.clear();
    cluster_indices.clear();
    cluster_nodal_data.clear();

    cluster.reserve(central_sampling_atom_neighbours[index].size());
    thermal_cluster.reserve(central_sampling_atom_neighbours[index].size());
    cluster_indices.reserve(central_sampling_atom_neighbours[index].size());
    cluster_nodal_data.reserve(central_sampling_atom_neighbours[index].size());

    int neighbour_idx = 0;

    for (const auto &neighbour : central_sampling_atom_neighbours[index]) {
      if (array_ops::distance(neighbour.location, central_locations[index]) <
          central_cut_offs[index]) {
        cluster.push_back(neighbour.location);
        thermal_cluster.push_back(neighbour.thermal_coordinate);
        cluster_indices.push_back(neighbour_idx);
        cluster_nodal_data.push_back(neighbour.nodal_data);
      }
      neighbour_idx++;
    }
  }

  void get_nodal_neighbour_thermal_cluster(
      const std::size_t &index,
      std::vector<std::array<double, Dimension>> &cluster,
      std::vector<std::array<double, NIDOF>> &thermal_cluster,
      std::vector<NodalData> &cluster_nodal_data,
      std::vector<int> &cluster_indices) {
    cluster.clear();
    thermal_cluster.clear();
    cluster_indices.clear();
    cluster_nodal_data.clear();

    cluster.reserve(nodal_sampling_atom_neighbours[index].size());
    thermal_cluster.reserve(nodal_sampling_atom_neighbours[index].size());
    cluster_indices.reserve(nodal_sampling_atom_neighbours[index].size());
    cluster_nodal_data.reserve(nodal_sampling_atom_neighbours[index].size());

    int neighbour_idx = 0;

    for (const auto &neighbour : nodal_sampling_atom_neighbours[index]) {
      if (array_ops::distance(neighbour.location, nodal_locations[index]) <
          nodal_cut_offs[index]) {
        cluster.push_back(neighbour.location);
        thermal_cluster.push_back(neighbour.thermal_coordinate);
        cluster_indices.push_back(neighbour_idx);
        cluster_nodal_data.push_back(neighbour.nodal_data);
      }
      neighbour_idx++;
    }
  }
#endif
};
} // namespace sampling_atoms
