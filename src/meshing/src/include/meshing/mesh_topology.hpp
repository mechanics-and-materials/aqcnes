// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Implementation of function buildMeshTopology for simplicial mesh
 * @author P. Gupta
 */

#pragma once
#include "exception/qc_exception.hpp"
#include "qcmesh/mesh/face.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include <boost/mpi.hpp>
#include <numeric>

namespace meshing {

template <class Mesh>
bool is_local_node(Mesh &mesh, const qcmesh::mesh::NodeId node_id) {
  return qcmesh::mesh::mesh_has_node(mesh, node_id,
                                     boost::mpi::communicator{}.rank());
}

template <std::size_t K, class Mesh> void build_adjacency_halo(Mesh &mesh) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();

  std::vector<std::size_t> noof_incidences(mesh.nodes.size(), std::size_t{0});

  // also use halo simplices. Only add to nodes that belong to this rank
  for (const auto &[_, cell] : mesh.halo_cells) {
    for (std::size_t d = 0; d < K + 1; d++) {
      // we only store adjacencies of owned nodes
      if (is_local_node(mesh, cell.nodes[d])) {
        if (mesh.nodes.find(cell.nodes[d]) == mesh.nodes.end())
          throw exception::QCException("build_adjacency_halo: cell node not "
                                       "found in local owned mesh nodes");
        ++noof_incidences[mesh.nodes.at(cell.nodes[d]).data.mesh_index];
      }
    }
  }

  // vertex adjacencies are never reused in the rest
  // of the code. Only face adjacencies. If they are required,
  // just build them

  mesh.vertex_adjacency.clear();
  mesh.vertex_adjacency_pointers.clear();
  // we have to resize, we are dealing with
  // iterator pointers later on.
  mesh.vertex_adjacency.resize(std::accumulate(
      noof_incidences.begin(), noof_incidences.end(), std::size_t{0}));

  mesh.vertex_adjacency_pointers.resize(mesh.nodes.size() + 1);

  mesh.vertex_adjacency_pointers[0] = mesh.vertex_adjacency.begin();

  for (std::size_t array_ctr = 0; array_ctr < mesh.nodes.size(); array_ctr++) {
    mesh.vertex_adjacency_pointers[array_ctr + 1] =
        mesh.vertex_adjacency_pointers[array_ctr] + noof_incidences[array_ctr];
  }

  if (mesh.vertex_adjacency_pointers[mesh.vertex_adjacency_pointers.size() -
                                     1] != mesh.vertex_adjacency.end())
    throw exception::QCException(
        "build_adjacency_halo: end of mesh.vertex_adjacency_pointers does not "
        "point to mesh.vertex_adjacency.end()");

  std::vector<std::vector<qcmesh::mesh::SimplexCellId>::iterator> vs_i(
      mesh.nodes.size());
  for (std::size_t nd = 0; nd != mesh.nodes.size(); nd++) {
    vs_i[nd] = mesh.vertex_adjacency_pointers[nd];
  }

  for (const auto &[_, cell] : mesh.halo_cells) {
    for (std::size_t d = 0; d < K + 1; d++) {
      if (is_local_node(mesh, cell.nodes[d])) {
        const auto node_idx = mesh.nodes.at(cell.nodes[d]).data.mesh_index;
        *vs_i[node_idx] = cell.id;
        ++vs_i[node_idx];
      }
    }
  }

  if (mesh.vertex_adjacency_pointers[mesh.vertex_adjacency_pointers.size() -
                                     1] != mesh.vertex_adjacency.end())
    throw exception::QCException(
        "build_adjacency_halo: end of mesh.vertex_adjacency_pointers does not "
        "point to mesh.vertex_adjacency.end()");

  // first build halo incidences

  for (auto &[cell_id, cell] : mesh.halo_cells) {
    cell.neighbors =
        std::array<std::optional<qcmesh::mesh::SimplexCellId>, K + 1>{};

    for (std::size_t d = 0; d < K + 1; d++) {
      auto local_owned_node = std::size_t{};
      auto found_local_owned_node = false;

      const auto face = qcmesh::mesh::face_vertex_ids_by_idx(cell.nodes, d);
      // only get the node belonging to this rank.

      for (std::size_t e = 0; e < K; e++) {
        if (is_local_node(mesh, face[e])) {
          local_owned_node = e;
          found_local_owned_node = true;
          break;
        }
      }
      // if there was a local owned node, only then proceed
      if (found_local_owned_node) {

        const auto vertex_idx =
            mesh.nodes.at(face[local_owned_node]).data.mesh_index;

        // noof halo elements incident
        const auto noof_incident = noof_incidences[vertex_idx];

        for (std::size_t j = 0; j < noof_incident; ++j) {
          const auto adj_simplex_key = incident_element(mesh, vertex_idx, j);

          // if its not the same cell and has this face
          if (cell_id != adj_simplex_key &&
              qcmesh::mesh::cell_contains_face(
                  mesh.halo_cells.at(adj_simplex_key).nodes, face)) {
            cell.neighbors[d] = adj_simplex_key;
            break;
          }
        }
      }
    }
  }

  // now attach the halo cells to existing cells
  for (auto &[cell_id, cell] : mesh.cells) {
    // cell neighbors within rank already exist
    // we only need to add halo cell neighbors.
    for (std::size_t d = 0; d < K + 1; d++) {
      // we only need to check if there is no neighbor assigned
      // if(s.second.neighbors[d].has_value())
      // to repetitively do this, we check if it has some other rank as
      // neighbour
      if (!cell.neighbors[d].has_value() ||
          !qcmesh::mesh::mesh_has_cell(mesh, cell.neighbors[d].value(),
                                       mpi_rank)) {
        const auto face = qcmesh::mesh::face_vertex_ids_by_idx(cell.nodes, d);
        const auto vertex_idx = mesh.nodes.at(face[0]).data.mesh_index;

        // get the halo incidents
        const auto noof_incident = noof_incidences[vertex_idx];

        for (std::size_t j = 0; j < noof_incident; ++j) {
          const auto adj_simplex_key = incident_element(mesh, vertex_idx, j);

          if (cell_id == adj_simplex_key)
            throw exception::QCException("build_adjacency_halo: cell id "
                                         "matches with the adjoining simplex");

          const auto apex_index = qcmesh::mesh::face_apex_index(
              mesh.halo_cells.at(adj_simplex_key).nodes, face);
          if (apex_index.has_value()) {
            cell.neighbors[d] = adj_simplex_key;
            mesh.halo_cells.at(adj_simplex_key).neighbors[apex_index.value()] =
                cell.id;
            break;
          }
        }
      }
    }
  }
}

/**
 * @brief build adjacencies from assigned nodes and simplices
 */
template <std::size_t K, class Mesh>
void build_node_cell_adjacency(Mesh &mesh) {
  std::vector<std::size_t> noof_incidences(mesh.nodes.size(), std::size_t{0});

  for (const auto &s : mesh.cells) {
    for (std::size_t d = 0; d < K + 1; d++) {
      ++noof_incidences[mesh.nodes.at(s.second.nodes[d]).data.mesh_index];
    }
  }

  mesh.vertex_adjacency.clear();
  mesh.vertex_adjacency_pointers.clear();
  // we have to resize, we are dealing with
  // iterator pointers later on
  mesh.vertex_adjacency.resize(std::accumulate(
      noof_incidences.begin(), noof_incidences.end(), std::size_t{0}));

  mesh.vertex_adjacency_pointers.resize(mesh.nodes.size() + 1);

  mesh.vertex_adjacency_pointers[0] = mesh.vertex_adjacency.begin();

  for (std::size_t array_ctr = 0; array_ctr < mesh.nodes.size(); array_ctr++) {
    mesh.vertex_adjacency_pointers[array_ctr + 1] =
        mesh.vertex_adjacency_pointers[array_ctr] + noof_incidences[array_ctr];
  }

  if (mesh.vertex_adjacency_pointers[mesh.vertex_adjacency_pointers.size() -
                                     1] != mesh.vertex_adjacency.end())
    throw exception::QCException("build_node_cell_adjacency: end of "
                                 "mesh.vertex_adjacency_pointers does not "
                                 "point to mesh.vertex_adjacency.end()");

  std::vector<std::vector<qcmesh::mesh::SimplexCellId>::iterator> vs_i(
      mesh.nodes.size());
  for (std::size_t nd = 0; nd != mesh.nodes.size(); nd++) {
    vs_i[nd] = mesh.vertex_adjacency_pointers[nd];
  }

  for (const auto &[_, cell] : mesh.cells) {
    // s = {key, cell}
    // cell[d] = {rank, node_key} (also called pair key in code)

    for (std::size_t d = 0; d < K + 1; d++) {
      // mesh index of the node
      const auto node_idx = mesh.nodes.at(cell.nodes[d]).data.mesh_index;
      *vs_i[node_idx] = cell.id; // node adjacency is the simplex pair key
      ++vs_i[node_idx];
    }
  }

  if (mesh.vertex_adjacency_pointers[mesh.vertex_adjacency_pointers.size() -
                                     1] != mesh.vertex_adjacency.end())
    throw exception::QCException("build_node_cell_adjacency: end of "
                                 "mesh.vertex_adjacency_pointers does not "
                                 "point to mesh.vertex_adjacency.end()");
}

template <std::size_t K, class CellData>
void clear_face_neighbours(
    std::unordered_map<qcmesh::mesh::SimplexCellId,
                       qcmesh::mesh::SimplexCell<K, CellData>> &cells) {
  for (auto &[_, cell] : cells)
    cell.neighbors =
        std::array<std::optional<qcmesh::mesh::SimplexCellId>, K + 1>{};
}

} // namespace meshing
