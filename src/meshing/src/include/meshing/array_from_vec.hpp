// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief convert a vector to an array
 */

#pragma once
#include "exception/qc_exception.hpp"
#include <array>
#include <vector>

template <std::size_t N>
std::array<double, N> array_from_vec(const std::vector<double> &vec) {
  auto arr = std::array<double, N>{};
  if (vec.size() != N)
    throw exception::QCException{
        "array_from_vec: Trying to copy from a vec of size " +
        std::to_string(vec.size()) + " to an array of size " +
        std::to_string(N)};
  std::copy(vec.begin(), vec.end(), arr.begin());
  return arr;
}
