// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief boost serialization for meshing::NodalDataMultispecies<NOI>
 * @author S. Saxena
 */

#pragma once
#include "meshing/nodal_data_multispecies.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost {
namespace serialization {
template <class Archive, std::size_t NOI>
void serialize(Archive &ar, meshing::NodalDataMultispecies<NOI> &data,
               const unsigned int) {
  ar &data.impurity_concentrations;
  ar &data.impurity_concentration_update_rates;
  ar &data.chemical_potentials;
  ar &data.gamma_values;
}

/**
 * See https://www.boost.org/doc/libs/1_82_0/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <std::size_t NOI>
struct tracking_level<meshing::NodalDataMultispecies<NOI>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTBEGIN(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  using type = mpl::int_<boost::serialization::track_never>;
  // NOLINTEND(readability-identifier-naming)
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<implementation_level<meshing::NodalDataMultispecies<NOI>>,
                    mpl::int_<primitive_type>>::value));
};

template <std::size_t NOI>
struct implementation_level<meshing::NodalDataMultispecies<NOI>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTBEGIN(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  using type = mpl::int_<object_serializable>;
  // NOLINTEND(readability-identifier-naming)
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

} // namespace serialization

/**
 * Implement a mpi type trait for better performance.
 * BOOST_IS_MPI_DATATYPE(QCVertexDraft<NodalData>);
 */
namespace mpi {

template <std::size_t NOI>
struct is_mpi_datatype<meshing::NodalDataMultispecies<NOI>>
    : public mpl::true_ {};

} // namespace mpi

} // namespace boost
