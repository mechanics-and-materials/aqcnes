// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief boost serialization for repatoms::RepatomData
 */

#pragma once

#include "meshing/repatom_data.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

/**
 * @brief Provides serialization of qcmesh::mesh::SimplexCell.
 */
template <typename Archive, class ThermalData, class MultiSpeciesDataType>
void serialize(Archive &ar,
               repatoms::RepatomData<ThermalData, MultiSpeciesDataType> &data,
               const unsigned int) {
  ar &data.lattice_site_type;
  ar &data.fixed;
  ar &data.thermal_data;
  ar &data.multi_species_data;
}
} // namespace boost::serialization

template <class ThermalData, class MultiSpeciesDataType>
struct boost::serialization::tracking_level<
    repatoms::RepatomData<ThermalData, MultiSpeciesDataType>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT((mpl::greater<implementation_level<repatoms::RepatomData<
                                        ThermalData, MultiSpeciesDataType>>,
                                    mpl::int_<primitive_type>>::value));
};

template <class ThermalData, class MultiSpeciesDataType>
struct boost::serialization::implementation_level<
    repatoms::RepatomData<ThermalData, MultiSpeciesDataType>> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <class ThermalData, class MultiSpeciesDataType>
struct boost::mpi::is_mpi_datatype<
    repatoms::RepatomData<ThermalData, MultiSpeciesDataType>>
    : public mpl::true_ {};
