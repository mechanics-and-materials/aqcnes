// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief boost serialization for @ref meshing::NodalThermalDataFiniteT
 */

#pragma once

#include "meshing/nodal_thermal_data.hpp"
#include "meshing/serialization/nodal_data_multispecies.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

template <class Archive>
void serialize(Archive &, meshing::NodalThermalDataZeroT &,
               const unsigned int) {}
} // namespace boost::serialization

BOOST_CLASS_IMPLEMENTATION(meshing::NodalThermalDataZeroT, object_serializable)
BOOST_CLASS_TRACKING(meshing::NodalThermalDataZeroT, track_never)
BOOST_IS_MPI_DATATYPE(meshing::NodalThermalDataZeroT)

namespace boost::serialization {

template <class Archive>
void serialize(Archive &ar, meshing::NodalThermalDataFiniteT &data,
               const unsigned int) {
  ar &data.thermal_point;
  ar &data.entropy;
}

} // namespace boost::serialization

BOOST_CLASS_IMPLEMENTATION(meshing::NodalThermalDataFiniteT,
                           object_serializable)
BOOST_CLASS_TRACKING(meshing::NodalThermalDataFiniteT, track_never)
BOOST_IS_MPI_DATATYPE(meshing::NodalThermalDataFiniteT)
