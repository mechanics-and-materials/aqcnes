// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief boost serialization for meshing::QCNodalData
 */

#pragma once

#include "meshing/qc_nodal_data.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

/**
 * @brief Provides serialization of qcmesh::mesh::SimplexCell.
 */
template <typename Archive>
void serialize(Archive &ar, meshing::QCNodalData &data, const unsigned int) {
  ar &data.mesh_index;
  ar &data.type;
  ar &data.proc_boundary_flag;
  ar &data.periodic_boundary_flag;
}
} // namespace boost::serialization

/**
 * See https://www.boost.org/doc/libs/1_82_0/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
BOOST_CLASS_IMPLEMENTATION(meshing::QCNodalData, object_serializable)
BOOST_CLASS_TRACKING(meshing::QCNodalData, track_never)
BOOST_IS_MPI_DATATYPE(meshing::QCNodalData)
