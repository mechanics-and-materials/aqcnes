// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief repartitioning of mesh from a poor partitioning. Responsible for
 *        all the qc data movement
 * @authors P. Gupta, S. Saxena, G. Bräunlich
 */

#include "meshing/redistribute_mesh.hpp"

extern "C" {
#include <parmetis.h>
}

namespace meshing {

std::vector<int> repartition_cells_metis(
    std::vector<int> &&cell_adjacency, std::vector<int> &&cell_start_indices,
    const std::vector<std::size_t> &current_partition_sizes) {
  using Idx = int;    // parmetis data type idx_t
  using Real = float; // parmetis data type real_t
  const auto mpi_size = current_partition_sizes.size();
  const auto n_local_cells = cell_start_indices.size() - 1;

  const auto n_total_cells =
      std::accumulate(current_partition_sizes.begin(),
                      current_partition_sizes.end(), std::size_t{0});

  auto cell_dist = std::vector<Idx>(mpi_size + 1, 0);
  for (std::size_t rank = 0; rank < mpi_size; rank++)
    cell_dist[rank + 1] =
        static_cast<Idx>(cell_dist[rank] + current_partition_sizes[rank]);
  auto vwgt = std::vector<Idx>(n_total_cells, 1);
  auto el_part = std::vector<Idx>(n_local_cells);
  auto ubvec = std::array{Real{1.01}}; // constraints
  auto n_constraint = static_cast<Idx>(ubvec.size());
  auto n_parts = static_cast<Idx>(mpi_size);
  // 0: following 2 options are skipped, 1: use following options
  // 1: print more timing info: default: 0
  // 2: random seed, default: 15
  auto options = std::array<Idx, 3>{1, 3, 15};
  auto wgtflag = Idx{0};
  auto numflag = Idx{0};
  auto tpwgts = std::vector<Real>(n_constraint * mpi_size,
                                  Real{1.0} / static_cast<Real>(mpi_size));
  auto adjwgt = std::vector<Idx>(cell_adjacency.size(), 1);
  auto edgecut = Idx{};

  auto *mpi_comm = MPI_Comm{};
  MPI_Comm_dup(MPI_COMM_WORLD, &mpi_comm);
  auto cell_adjncy = std::vector<Idx>{};
  auto ixadj = std::vector<Idx>{};

  // repartition of mesh
  ParMETIS_V3_PartKway(cell_dist.data(), cell_start_indices.data(),
                       cell_adjacency.data(), vwgt.data(), adjwgt.data(),
                       &wgtflag, &numflag, &n_constraint, &n_parts,
                       tpwgts.data(), ubvec.data(), options.data(), &edgecut,
                       el_part.data(), &mpi_comm);
  MPI_Comm_free(&mpi_comm);
  return el_part;
}

} // namespace meshing
