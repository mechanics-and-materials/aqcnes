// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Quasistatic relaxation of a mesh
 * @authors P. Gupta, S. Saxena
 */

#include "app/app.hpp"
#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qc_env.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "solver/dump_solver_variables.hpp"
#include "solver/multispecies_generic_solver_variables.hpp"
#include "solver/relaxation.hpp"
#include "solver/solver.hpp"
#include "utils/timer.hpp"
#include "vtkwriter/write_mesh.hpp"
#include <boost/mpi.hpp>

namespace {

namespace array_ops = qcmesh::array_ops;

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;

template <typename T>
std::ostream &print_array(std::ostream &out, const std::vector<T> &x) {
  for (const auto &p : x)
    out << array_ops::as_streamable(p) << " ";
  return out;
}

} // namespace

template <std::size_t Noi>
void run(solver::SolverVariables<DIM, Noi> &solver_variables) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();

  // Input file or mesh generation stuff
  solver_variables.materials.emplace_back(materials::create_material<DIM, Noi>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  /** provide initial orientation matrix for the crystal
   * for instance, for an FCC crystal with axes oriented along
   * the closed packed planes, provide
   *  1.0/sqrt(2.0),-1.0/sqrt(2.0),0.0,
   *  1.0/sqrt(3.0),1.0/sqrt(3.0),1.0/sqrt(3.0),
   * -1.0/sqrt(6.0),-1.0/sqrt(6.0),2.0/sqrt(6.0)
   */

  if (mpi_rank == 0) {
    std::cout << "------------------------------------------------------"
              << std::endl;
    std::cout << " WARNING! Please double-check the crystal lattice structure."
              << std::endl;
    std::cout << "	running :- "
              << serialize_lattice_structure(
                     solver_variables.materials[0].lattice_type)
              << std::endl;
    std::cout << "--------------------------------------------------------"
              << std::endl;
  }

  const auto &restart_file_name = solver_variables.input.restart_file_name;

  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  solver_variables.relaxation_fire_parameters =
      solver::fire_parameters_from_userinput(solver_variables.input);

  //=-=-=-=-=-=-=-=-=<Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  // upto here, periodic connectivity is not established
  // because domain bounds are not known, so periodic lengths
  // are unknown
  auto restart_data = netcdf_io::RestartData<DIM>{};
  std::tie(restart_data, solver_variables.local_mesh) =
      meshing::load_nc(solver_variables.input, solver_variables.materials);
  restart_data.apply_fixed_box = solver_variables.input.fixed_box.has_value();

  solver_variables.local_mesh.repatoms.box_displacement_gradient =
      repatoms::diag_displacement_gradient(
          solver_variables.local_mesh.periodic_flags);

  //=-=-=-=-=-=-=-=-=</Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  if (mpi_rank == 0 && solver_variables.local_mesh.has_periodic_dimensions) {
    std::cout << "periodic simulation : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.periodic_flags)
              << std::endl;
    std::cout << "periodic lengths : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.periodic_lengths)
              << std::endl;

    std::cout << " periodic offsets : " << std::endl;
    for (const auto &offset : solver_variables.local_mesh.periodic_offsets)
      std::cout << array_ops::as_streamable(offset) << std::endl;
  }

  // redistribute the mesh again after this so periodic connectivities are
  // built based on the lengths
  meshing::redistribute(solver_variables.local_mesh);

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "--------------------------------------------" << std::endl;
      std::cout << "domain is bound within " << std::endl;
      std::cout << "lower : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.lower)
                << std::endl;
      std::cout << "upper : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.upper)
                << std::endl;
      std::cout << "Verlet list buffer "
                << solver_variables.input.verlet_buffer_factor *
                       solver_variables.input.neighbour_update_displacement
                << std::endl;
    }
  }

  //=-=-=-=-=-=-=-=-=<Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIM>(
          solver_variables.input, restart_data.restartfile_iter);
  // =-=-=-=-=-=-=-=-=</Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//
  //        Repatoms and sampling atoms are built with qc mesh.          //
  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//

  auto clock = utils::Timer{};
  clock.tic();

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "-------------------------------------------------"
                << std::endl;
      std::cout << "--------------- Attempting initial repair--------"
                << std::endl;
      std::cout << "-------------------------------------------------"
                << std::endl;
    }
    // initial repair after triangulation
    meshing::repair_mesh(solver_variables.local_mesh);
  }

  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  // update boundaries
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          solver_variables.input.boundary_types,
          solver_variables.local_mesh.domain_bound);

  if (!restart_file_name.has_value()) {
    std::cout << mpi_rank << " , "
              << solver_variables.boundary_conditions.size() << std::endl;
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // output after initial repair
    vtkwriter::write_vtu_mesh_3d(solver_variables.local_mesh,
                                 meshing::REMESH_TOLERANCE,
                                 "initial_partition/");
  }

  MPI_Barrier(MPI_COMM_WORLD);

  double min_distance{};
  std::size_t id = 0;

  for (const auto &p :
       solver_variables.input.display_neighbourhoods_sampling_atoms) {
    min_distance = 9999.9;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.nodes.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      const auto &node =
          solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .position;

      if (array_ops::distance(node, p) < min_distance) {
        id = node_index;
        min_distance = array_ops::distance(node, p);
      }
    }

    if (min_distance < 1.0) {

      std::cout << mpi_rank << " , " << id << " , "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.nodes
                           .at(solver_variables.local_mesh.node_index_to_key.at(
                               id))
                           .position)

                << std::endl;
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Neighbourhood udpdate styles, "
              << "Verlet : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::VERLET)
              << " , Adaptive : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::ADAPTIVE)
              << " :: "
              << static_cast<std::size_t>(
                     solver_variables.input.neighbour_update_style)
              << std::endl;
  }

  solver_variables.apply_external_force = false;
  solver_variables.apply_boundary_conditions = false;

  //   * Basic flow of one full relaxation step is (after moving repatoms,
  //   * based on use-case)
  //  *
  //   *
  //   * update mesh nodes from repatoms
  //  	 solver::Solver::updateMesh(solver_variables);
  //    *
  //  * update sampling atoms and neighbourhoods from mesh nodes
  //  	 solver::Solver::updateSamplingAtoms(solver_variables);
  //    *
  //  * update forces on repatoms using sampling atoms
  //    solver::Solver::updateRepatomForces(solver_variables);
  //  *
  //  * relax using FIRE+PETSC
  //    solver::Solver::CompositeRelax();

  // --------<Composite solver>------- //
  solver::Solver::set_solver_dofs<DIM, NIDOF>(solver_variables);

  solver::Solver::create_solution_vectors<DIM>(solver_variables);

  if (solver_variables.input.use_petsc)
    solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<Noi>,
                                        DIM, NIDOF>(
        solver_variables.input.abs_tolerance,
        solver_variables.input.rel_tolerance,
        solver_variables.input.s_tolerance, solver_variables.input.max_iter,
        solver_variables.input.max_feval, solver_variables);

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<Noi>, DIM, NIDOF>(solver_variables);

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  const auto display_zones = app::get_bool_env("DISPLAY_ZONES");
  const auto display_element_qualities =
      app::get_bool_env("DISPLAY_ELEMENT_QUALITIES");

  const auto dump_vtk = solver::DumpSolverVariablesVtk(
      restart_data.outputfile_iter, display_zones, display_element_qualities);
  dump_vtk(solver_variables);

  // initial relaxation

  solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                  meshing::NodalDataMultispecies<Noi>, DIM,
                                  NIDOF>(
      solver_variables.input.a_ref,
      solver_variables.input.max_thermal_force_per_atom,
      solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
  solver::dump_solver_variables_nc(solver_variables, restart_data);
  dump_vtk(solver_variables);

  // store thermalized energy in sampling atoms
  solver::Solver::update_sampling_atom_energies<
      meshing::NodalDataMultispecies<Noi>, DIM, NIDOF>(solver_variables);

  // print relaxed info as per user demand
  solver::print_relaxed_info_to_files<DIM, Noi>(solver_variables, false);

  // displace atoms, relax atoms within the "fixed bounds" which is actually
  // free bound. Then measure and store the energy of the free sampling atoms
  solver::Solver::destroy_snes<DIM>(solver_variables);
  // --------<\Composite solver>------- //

  if (mpi_rank == 0 && solver_variables.periodic_simulation_flag()) {
    std::cout << "relaxed periodic offsets were ";
    print_array(std::cout, solver_variables.local_mesh.periodic_offsets);
    std::cout << std::endl;
  }

  const auto elapsed_time = clock.toc();

  std::cout
      << "rank " << mpi_rank << " took " << elapsed_time << " with "
      << solver_variables.local_mesh.sampling_atoms.nodal_locations.size()
      << " nodal sampling atoms "
      << solver_variables.local_mesh.sampling_atoms.central_locations.size()
      << " central sampling atoms " << solver_variables.local_mesh.nodes.size()
      << " nodes and " << solver_variables.local_mesh.cells.size()
      << " elements" << std::endl;
}

int main(int argc, char *argv[]) {
  const auto env = QCEnv(&argc, &argv);

  try {
    auto userinput = input::load_userinput_from_working_dir();
    auto solver_variables =
        solver::create_solver_variables(std::move(userinput));
    std::visit([](auto &solver_variables) { run(solver_variables); },
               solver_variables);
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }

  return 0;
}
