// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to perform a quasistatic loading of a mesh with staggered relaxation steps
 * @authors P. Gupta, S. Saxena, M. Spinola
 */

#include "app/app.hpp"
#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "geometry/point.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "meshing/write_mesh_to_nc.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "solver/dump_solver_variables.hpp"
#include "solver/relaxation.hpp"
#include "solver/solver.hpp"
#include "utils/timer.hpp"
#include "vtkwriter/write_mesh.hpp"
#include <boost/mpi.hpp>

namespace {

namespace array_ops = qcmesh::array_ops;

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

using Matrix = Eigen::Matrix<double, DIM, DIM>;

template <typename T>
std::ostream &print_array(std::ostream &out, const std::vector<T> &x) {
  for (const auto &p : x)
    out << array_ops::as_streamable(p) << " ";
  return out;
}
} // namespace

void run() {
  const auto mpi_rank = boost::mpi::communicator{}.rank();

  solver::SolverVariables<DIM, NOI> solver_variables;
  solver_variables.input = input::load_userinput_from_working_dir();

  // Input file or mesh generation stuff
  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  if (mpi_rank == 0) {
    std::cout << "--------------------------------------------------------"
              << std::endl;
    std::cout << " WARNING! Please double-check the crystal lattice structure."
              << std::endl;
    std::cout << "	running :- "
              << "FCC" << std::endl;
    std::cout << "--------------------------------------------------------"
              << std::endl;
  }

  const auto &restart_file_name = solver_variables.input.restart_file_name;

  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //=-=-=-=-=-=-=-=-=<Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  // upto here, periodic connectivity is not established
  // because domain bounds are not known, so periodic lengths
  // are unknown
  auto restart_data = netcdf_io::RestartData<DIM>{};
  std::tie(restart_data, solver_variables.local_mesh) =
      meshing::load_nc(solver_variables.input, solver_variables.materials);
  restart_data.apply_fixed_box = solver_variables.input.fixed_box.has_value();

  solver_variables.local_mesh.repatoms.box_displacement_gradient =
      repatoms::diag_displacement_gradient(
          solver_variables.local_mesh.periodic_flags);

  // =-=-=-=-=-=-=-=-=</Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  if (mpi_rank == 0 && solver_variables.local_mesh.has_periodic_dimensions) {
    std::cout << "periodic simulation : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.periodic_flags)
              << std::endl;
    std::cout << "periodic lengths : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.periodic_lengths)
              << std::endl;

    std::cout << " periodic offsets : " << std::endl;
    for (const auto &offset : solver_variables.local_mesh.periodic_offsets)
      std::cout << array_ops::as_streamable(offset) << std::endl;
  }

  // redistribute the mesh again after this so periodic connectivities are
  // built based on the lengths
  meshing::redistribute(solver_variables.local_mesh);

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "--------------------------------------------" << std::endl;
      std::cout << "domain is bound within " << std::endl;
      std::cout << "lower : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.lower)
                << std::endl;
      std::cout << "upper : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.upper)
                << std::endl;
      std::cout << "Verlet list buffer "
                << solver_variables.input.verlet_buffer_factor *
                       solver_variables.input.neighbour_update_displacement
                << std::endl;
    }
  }

  //=-=-=-=-=-=-=-=-=<Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIM>(
          solver_variables.input, restart_data.restartfile_iter);
  // =-=-=-=-=-=-=-=-=</Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//
  //        Repatoms and sampling atoms are built with qc mesh.          //
  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//

  auto clock = utils::Timer{};
  clock.tic();

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "-------------------------------------------------"
                << std::endl;
      std::cout << "--------------- Attempting initial repair--------"
                << std::endl;
      std::cout << "-------------------------------------------------"
                << std::endl;
    }
    // initial repair after triangulation
    meshing::repair_mesh(solver_variables.local_mesh);
  }

  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  // update boundaries
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          solver_variables.input.boundary_types,
          solver_variables.local_mesh.domain_bound);

  if (!restart_file_name.has_value()) {
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // output after initial repair
    vtkwriter::write_vtu_mesh_3d(solver_variables.local_mesh,
                                 meshing::REMESH_TOLERANCE,
                                 "initial_partition/");
  }

  MPI_Barrier(MPI_COMM_WORLD);

  double min_distance{};
  std::size_t id = 0;

  for (const auto &p :
       solver_variables.input.display_neighbourhoods_sampling_atoms) {
    min_distance = 9999.9;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.nodes.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      const auto &node =
          solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .position;

      if (array_ops::distance(node, p) < min_distance) {
        id = node_index;
        min_distance = array_ops::distance(node, p);
      }
    }

    if (min_distance < 1.0) {
      std::cout << mpi_rank << " , " << id << " , "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.nodes
                           .at(solver_variables.local_mesh.node_index_to_key.at(
                               id))
                           .position)
                << std::endl;
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Neighbourhood udpdate styles, "
              << "Verlet : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::VERLET)
              << " , Adaptive : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::ADAPTIVE)
              << " :: "
              << static_cast<std::size_t>(
                     solver_variables.input.neighbour_update_style)
              << std::endl;
  }

  solver_variables.relaxation_fire_parameters.time_step =
      solver_variables.input.fire_time_step;

  // Fire parameters
  solver_variables.relaxation_fire_parameters.alpha =
      solver_variables.input.fire_initial_alpha;
  solver_variables.relaxation_fire_parameters.steps_post_reset =
      solver_variables.input.fire_steps_post_reset;

  solver_variables.relaxation_fire_parameters.min_residual =
      solver_variables.input.fire_min_residual;

  // if there is an external force applicator
  solver_variables.apply_external_force = false;

  // if boundary conditions need to be applied
  solver_variables.apply_boundary_conditions = false;

  // if isentropic relaxation. If not, then its isothermal
  solver_variables.isentropic = false;

  double mesh_volume{};

  // --------<Composite solver>------- //
  solver::Solver::set_solver_dofs<DIM, NIDOF>(solver_variables);

  solver::Solver::create_solution_vectors<DIM>(solver_variables);

  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<NOI>, DIM,
                                      NIDOF>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      solver_variables.input.max_iter, solver_variables.input.max_feval,
      solver_variables);

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  const auto display_zones = app::get_bool_env("DISPLAY_ZONES");
  const auto display_element_qualities =
      app::get_bool_env("DISPLAY_ELEMENT_QUALITIES");

  const auto dump_vtk = solver::DumpSolverVariablesVtk(
      restart_data.outputfile_iter, display_zones, display_element_qualities);

  // initial relaxation
  solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                  meshing::NodalDataMultispecies<NOI>, DIM,
                                  NIDOF>(
      solver_variables.input.a_ref,
      solver_variables.input.max_thermal_force_per_atom,
      solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
  solver::dump_solver_variables_nc(solver_variables, restart_data);
  ++restart_data.restartfile_iter;
  dump_vtk(solver_variables);

  // store thermalized energy in sampling atoms
  solver::Solver::update_sampling_atom_energies<
      meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

  double effective_strain = 0.0;
  restart_data.external_strain = 0.0;
  restart_data.external_strain_increment =
      solver_variables.input.indenter_increment;

  Matrix displacement_gradient;
  Matrix deformation_gradient;

  for (std::size_t i = 0; i < DIM; i++) {
    for (std::size_t j = 0; j < DIM; j++) {
      displacement_gradient(static_cast<Eigen::Index>(i),
                            static_cast<Eigen::Index>(j)) =
          solver_variables.input.external_displacement_gradient[i][j];
    }
  }

  geometry::Point<DIM> displacement;
  displacement.fill(0.0);

  const auto e_global_before = qcmesh::mpi::all_reduce_sum(
      solver::local_energy(solver_variables.local_mesh));
  const auto w_global_before = qcmesh::mpi::all_reduce_sum(
      solver::local_weight(solver_variables.local_mesh));
  const auto k_global_before =
      qcmesh::mpi::all_reduce_sum(solver::local_k(solver_variables.local_mesh));
  const auto ts_global_before = qcmesh::mpi::all_reduce_sum(
      solver::local_ts(solver_variables.local_mesh));
  const auto f_global_before =
      e_global_before + k_global_before - ts_global_before;

  mesh_volume = solver_variables.local_mesh.volume();

  std::string filename =
      "energy_volume_" + solver_variables.materials[0].material_name;

  std::ofstream f_external;

#ifdef FINITE_TEMPERATURE
  const auto output_t = std::to_string(solver_variables.input.T_ref);
#else
  const auto output_t = std::string{"0"};
#endif

  filename += "_" + output_t + ".dat";

  if (mpi_rank == 0) {
    f_external.open(filename, ios::app);
    f_external << std::setprecision(15) << restart_data.external_strain << " "
               << solver_variables.input.T_ref << " " << mesh_volume << " "
               << f_global_before << " " << e_global_before << " "
               << k_global_before << " " << w_global_before << " "
               << solver_variables.local_mesh.periodic_lengths[0] << " "
               << solver_variables.local_mesh.periodic_lengths[1] << " "
               << solver_variables.local_mesh.periodic_lengths[2] << std::endl;
    f_external.close();
  }

  std::vector<geometry::Point<DIM>> initial_locations;
  initial_locations = solver_variables.local_mesh.repatoms.locations;

  for (auto &val : solver_variables.input.periodicity)
    if (val.has_value())
      val->relaxation_constraint = true;

  for (unsigned int i = 0; i < solver_variables.input.external_iterations;
       i++) {
    restart_data.external_strain += restart_data.external_strain_increment;
    effective_strain = restart_data.external_strain_increment /
                       (1 + i * restart_data.external_strain_increment);

    deformation_gradient = effective_strain * displacement_gradient +
                           Eigen::Matrix<double, DIM, DIM>::Identity(DIM, DIM);

    if (solver_variables.periodic_simulation_flag()) {
      using NodalDataContainer = meshing::NodalDataMultispecies<NOI>;
      solver::deform_period_offsets<
          meshing::QCMesh<DIM, DIM, NIDOF, NodalDataContainer>, DIM, NIDOF,
          Matrix, NodalDataContainer>(solver_variables.local_mesh,
                                      deformation_gradient);
    }

    // update mesh nodes from repatoms
    solver::Solver::update_mesh<DIM, NIDOF,
                                meshing::NodalDataMultispecies<NOI>>(
        solver_variables);

    // update sampling atoms and neighbourhoods from mesh nodes
    solver::Solver::update_sampling_atoms<meshing::NodalDataMultispecies<NOI>,
                                          DIM, NIDOF>(solver_variables);

    // update forces on repatoms using sampling atoms
    solver::Solver::update_repatom_forces<meshing::NodalDataMultispecies<NOI>,
                                          DIM, NIDOF>(solver_variables);

    solver::Solver::update_sampling_atom_energies<
        meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

    // relax using FIRE+PETSC
    solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                    meshing::NodalDataMultispecies<NOI>, DIM,
                                    NIDOF>(
        solver_variables.input.a_ref,
        solver_variables.input.max_thermal_force_per_atom,
        solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;
    dump_vtk(solver_variables);

    // store thermalized energy in sampling atoms
    solver::Solver::update_sampling_atom_energies<
        meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

    const auto e_global_after = qcmesh::mpi::all_reduce_sum(
        solver::local_energy(solver_variables.local_mesh));
    const auto w_global_after = qcmesh::mpi::all_reduce_sum(
        solver::local_weight(solver_variables.local_mesh));
    const auto k_global_after = qcmesh::mpi::all_reduce_sum(
        solver::local_k(solver_variables.local_mesh));
    const auto ts_global_after = qcmesh::mpi::all_reduce_sum(
        solver::local_ts(solver_variables.local_mesh));
    const auto f_global_after =
        e_global_after + k_global_after - ts_global_after;

    mesh_volume = solver_variables.local_mesh.volume();

    if (mpi_rank == 0) {
      f_external.open(filename, ios::app);
      f_external << std::setprecision(15) << restart_data.external_strain << " "
                 << solver_variables.input.T_ref << " " << mesh_volume << " "
                 << f_global_after << " " << e_global_after << " "
                 << k_global_after << " " << w_global_after << " "
                 << solver_variables.local_mesh.periodic_lengths[0] << " "
                 << solver_variables.local_mesh.periodic_lengths[1] << " "
                 << solver_variables.local_mesh.periodic_lengths[2]
                 << std::endl;
      f_external.close();
    }
  }

  // displace atoms, relax atoms within the "fixed bounds" which is actually
  // free bound. Then measure and store the energy of the free sampling atoms
  solver::Solver::destroy_snes<DIM>(solver_variables);
  // --------<\Composite solver>------- //

  const auto elapsed_time = clock.toc();

  std::cout
      << "rank " << mpi_rank << " took " << elapsed_time << " with "
      << solver_variables.local_mesh.sampling_atoms.nodal_locations.size()
      << " nodal sampling atoms "
      << solver_variables.local_mesh.sampling_atoms.central_locations.size()
      << " central sampling atoms " << solver_variables.local_mesh.nodes.size()
      << " nodes and " << solver_variables.local_mesh.cells.size()
      << " elements" << std::endl;
}
