// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to plot the bulk cluster energy as a function of lattice spacing and position variance
 * @authors P. Gupta, S. Saxena, M. Spinola
 */

#include "constants/constants.hpp"
#include "get_potential_material_name.hpp"
#include "materials/create_material.hpp"
#include "meshing/array_from_vec.hpp"
#include "meshing/qc_mesh.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qc_env.hpp"
#include "solver/solver.hpp"
#include "utils/dynamic_span.hpp"
#include <chrono>

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

using ThermalPoint = std::array<double, NIDOF>;

} // namespace

void run(const double r_r0, const double dr, const std::size_t nr,
         [[maybe_unused]] const double sigma_start,
         [[maybe_unused]] const double dsigma,
         [[maybe_unused]] const std::size_t nsigma) {
  namespace array_ops = qcmesh::array_ops;

  const auto t1 = std::chrono::high_resolution_clock::now();

  solver::SolverVariables<DIM, NOI> solver_variables;

  solver_variables.input = input::load_userinput_from_working_dir();

  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  std::cout << "Initialized\n";

  geometry::Point<DIM> center = {{0.0, 0.0, 0.0}};

  std::array<std::array<double, DIM>, DIM> basis =
      solver_variables.materials[0].basis_vectors;

  geometry::Point<DIM> px = {{basis[0][0], basis[1][0], basis[2][0]}};
  geometry::Point<DIM> py = {{basis[0][1], basis[1][1], basis[2][1]}};
  geometry::Point<DIM> pz = {{basis[0][2], basis[1][2], basis[2][2]}};

  std::vector<geometry::Point<DIM>> lattice;
  geometry::Point<DIM> lattice_point;
  std::vector<geometry::Point<DIM>> neighbours;

  double min_energy = 0.0;
  double r_min{};

  std::string file_name;
  std::string thermal_expansion_file_name;
  std::string force_file_name;
  std::ofstream f_energies;
  std::ofstream f_forces;
  std::ofstream f_thermal_expansion;
  std::vector<geometry::Point<DIM>> forces;

  meshing::NodalDataMultispecies<NOI> nodal_data_atom{array_from_vec<NOI>(
      solver_variables.input.initial_impurity_concentrations)};
  std::vector<meshing::NodalDataMultispecies<NOI>> nodal_data_neighbors;

#ifdef FINITE_TEMPERATURE
  std::vector<double> thermal_forces_atom;
  std::vector<double> thermal_forces_neigh;
  ThermalPoint atom_thermal_coordinates;
  std::vector<ThermalPoint> neighbours_thermal_coordinates;
  double temp = solver_variables.input.T_ref;

  double sigma_min{};
  double entropy = 0.0;

  atom_thermal_coordinates[0] = -log(constants::k_Boltzmann * temp);
  atom_thermal_coordinates[1] = sigma_start;

  const auto potential_material_name =
      get_potential_material_name(solver_variables.input.material);

  file_name = "./" + potential_material_name + "Energy_" +
              std::to_string(temp) + ".txt";
  force_file_name = "./" + potential_material_name + "Forces_" +
                    std::to_string(temp) + ".txt";

  thermal_expansion_file_name =
      "./" + potential_material_name + "ParameterAndSigma.txt";

  f_thermal_expansion.open(thermal_expansion_file_name);

  std::cout << solver_variables.materials[0].neighbor_cutoff_radius << '\n';

  for (int tc = 5; tc < 6; tc++) {
    r_min = 0.0;
    sigma_min = 0.0;
    f_energies.open(file_name);
    temp = 100.0 * tc;
    atom_thermal_coordinates[0] = -log(constants::k_Boltzmann * temp);
    atom_thermal_coordinates[1] = sigma_start;
    min_energy = 0.0;
    for (std::size_t k = 0; k < nsigma; k++) {
      auto r_r0_test = r_r0;
      entropy = -1.5 * constants::k_Boltzmann *
                (atom_thermal_coordinates[0] + atom_thermal_coordinates[1]);
      for (std::size_t j = 0; j < nr; j++) {
        lattice.clear();
        neighbours.clear();
        neighbours_thermal_coordinates.clear();
        nodal_data_neighbors.clear();

        for (int l = -4; l < 4; l++)
          for (int m = -4; m < 4; m++)
            for (int n = -4; n < 4; n++) {
              array_ops::as_eigen(lattice_point) =
                  array_ops::as_eigen(center) +
                  r_r0_test * double(l) * array_ops::as_eigen(px) +
                  r_r0_test * double(m) * array_ops::as_eigen(py) +
                  r_r0_test * double(n) * array_ops::as_eigen(pz);
              lattice.push_back(lattice_point);
            }

        for (const auto &p : lattice) {
          if (array_ops::distance(center, p) > 1.0e-2 &&
              array_ops::distance(center, p) <=
                  (solver_variables.materials[0]
                       .cluster_model->neighbor_cutoff_radius)) {
            neighbours.push_back(p);
            neighbours_thermal_coordinates.push_back(atom_thermal_coordinates);
            nodal_data_neighbors.push_back(nodal_data_atom);
          }
        }

        // Compute forces and energy
        double energy{};
        double kenergy{};

        kenergy = 3.0 * exp(-atom_thermal_coordinates[0]) / 2;
        energy = 0.0;
        if (solver_variables.input.quad_type == 3)
          energy = solver_variables.materials[0].get_thermalized_energy_3(
              center, atom_thermal_coordinates, neighbours,
              neighbours_thermal_coordinates, nodal_data_atom,
              nodal_data_neighbors);
        else if (solver_variables.input.quad_type == 5)
          energy = solver_variables.materials[0].get_thermalized_energy_5(
              center, atom_thermal_coordinates, neighbours,
              neighbours_thermal_coordinates, nodal_data_atom,
              nodal_data_neighbors);

        energy = energy + kenergy - entropy * temp;
        f_energies << energy << '\t';

        if (energy < min_energy) {
          min_energy = energy;
          r_min = r_r0_test;
          sigma_min = atom_thermal_coordinates[1];
        }

        r_r0_test += dr;
      }
      atom_thermal_coordinates[1] += dsigma;
      f_energies << std::endl;
    }

    std::cout << "Temperature: " << temp << std::endl;
    std::cout << " Minimising parameters: Spacing coefficient " << r_min
              << " and Sigma " << sigma_min << std::endl;

    f_thermal_expansion << temp << " " << r_min << " " << sigma_min << " "
                        << min_energy << '\n';
  }
  f_thermal_expansion.close();

#else

  const auto potential_material_name =
      get_potential_material_name(solver_variables.input.material);

  file_name = "./" + potential_material_name + "HamiltonianEnergy_0.txt";
  force_file_name = "./" + potential_material_name + "Forces_0.txt";

  f_energies.open(file_name);
  f_forces.open(force_file_name);

  min_energy = 0.0;

  for (std::size_t j = 0; j < nr; j++) {
    auto r_r0_test = r_r0;

    lattice.clear();
    neighbours.clear();
    nodal_data_neighbors.clear();

    for (int l = -4; l < 4; l++) {
      for (int m = -4; m < 4; m++) {
        for (int n = -4; n < 4; n++) {

          array_ops::as_eigen(lattice_point) =
              array_ops::as_eigen(center) +
              r_r0_test * double(l) * array_ops::as_eigen(px) +
              r_r0_test * double(m) * array_ops::as_eigen(py) +
              r_r0_test * double(n) * array_ops::as_eigen(pz);

          lattice.push_back(lattice_point);
        }
      }
    }

    for (std::size_t i = 0; i < lattice.size(); i++) {
      if (array_ops::distance(center, lattice[i]) > 1.0e-2 &&
          array_ops::distance(center, lattice[i]) <=
              solver_variables.materials[0]
                  .cluster_model->neighbor_cutoff_radius) {
        neighbours.push_back(lattice[i]);
        nodal_data_neighbors.push_back(nodal_data_atom);
      }
    }

    // Compute forces and energy
    const auto energy = solver_variables.materials[0].get_cluster_energy(
        center, neighbours, nodal_data_atom, nodal_data_neighbors);
    std::cout << "energy calc done \n";
    solver_variables.materials[0].get_cluster_forces(
        forces, center, neighbours, nodal_data_atom, nodal_data_neighbors);
    std::cout << "force calc done \n";
    std::cout << '\n';

    min_energy = energy;
    r_min = r_r0_test;

    f_energies << energy << '\n';
    for (std::size_t nc = 0; nc < neighbours.size(); nc++) {
      f_forces << forces[nc][0] << '\t' << forces[nc][1] << '\t'
               << forces[nc][2] << '\n';
    }

    r_r0_test += dr;
  }

  std::cout << " Minimising parameters: Spacing coefficient " << r_min
            << std::endl;
  std::cout << "Minimum energy: " << min_energy << std::endl;

  f_energies.close();
  f_forces.close();
#endif

  const auto t2 = std::chrono::high_resolution_clock::now();
  const auto time_span =
      std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  std::cout << "duration: " << time_span.count() << std::endl;
}

int main(int argc, char *argv[]) {
  const auto env = QCEnv(&argc, &argv);

  const auto args = utils::DynamicSpan(argv, argc);

  try {
    const auto r_r0 = std::stod(args[1]);
    const auto dr = std::stod(args[2]);
    const auto nr = static_cast<std::size_t>(std::stoi(args[3]));
    const auto sigma_start = std::stod(args[4]);
    const auto dsigma = std::stod(args[5]);
    const auto nsigma = static_cast<std::size_t>(std::stoi(args[6]));
    run(r_r0, dr, nr, sigma_start, dsigma, nsigma);
  } catch (const std::out_of_range &e) {
    std::cerr << "💥 Wrong number of arguments: " << e.what() << "\n";
    return 1;
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }

  return 0;
}
