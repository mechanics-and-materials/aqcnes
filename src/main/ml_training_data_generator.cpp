// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to generate GNN training data using Monte-Carlo based energy and force sampling
 * @author S. Saxena, M. Spinola
 */

#include "geometry/point.hpp"
#include "get_potential_material_name.hpp"
#include "materials/create_material.hpp"
#include "meshing/array_from_vec.hpp"
#include "solver/solver.hpp"
#include <chrono>
#include <ctime>
#include <random>

namespace array_ops = qcmesh::array_ops;

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

using ThermalPoint = std::array<double, NIDOF>;
using Matrix = Eigen::Matrix<double, DIM, DIM>;

} // namespace

void random_move(
    const std::vector<geometry::Point<DIM>> &neighbours_initial_locations,
    std::vector<geometry::Point<DIM>> &neighbours,
    std::vector<ThermalPoint> &neighbours_thermal_coordinates,
    ThermalPoint &center_thermal_coordinates, const double mean_phi,
    const double std_phi, const double std_r) {

  geometry::Point<DIM> shift;

  // Make Gaussian random generators for the mean positions and position
  // variances
  std::default_random_engine generator;
  generator.seed(std::random_device{}());
  std::normal_distribution<double> distribution_phi(mean_phi, std_phi);
  std::normal_distribution<double> distribution_r(0.0, std_r);

  for (std::size_t index = 0; index < neighbours_initial_locations.size();
       index++) {
    // change the x,y,and z locatons of this neighbour using a Gaussian
    // distribution
    for (std::size_t d = 0; d < DIM; d++) {
      shift[d] = distribution_r(generator);
    }
    array_ops::as_eigen(neighbours[index]) =
        array_ops::as_eigen(neighbours_initial_locations[index]) +
        array_ops::as_eigen(shift);

    // change the phi of this neighbor using a Gaussian distribution
    neighbours_thermal_coordinates[index][1] = distribution_phi(generator);
  }

  // change the phi of the central atom randomly
  center_thermal_coordinates[1] = distribution_phi(generator);
}

void random_move(
    const std::vector<geometry::Point<DIM>> &neighbours_initial_locations,
    std::vector<geometry::Point<DIM>> &neighbours,
    std::vector<ThermalPoint> &neighbours_thermal_coordinates,
    ThermalPoint &center_thermal_coordinates, const double std_phi,
    const double std_r) {

  geometry::Point<DIM> shift;

  // Make Gaussian random generators for the mean positions and position
  // variances
  std::default_random_engine generator;
  generator.seed(std::random_device{}());
  std::normal_distribution<double> distribution_phi(0.0, std_phi);
  std::normal_distribution<double> distribution_r(0.0, std_r);

  for (std::size_t index = 0; index < neighbours_initial_locations.size();
       index++) {
    // change the x,y,and z locatons of this neighbour using a Gaussian
    // distribution
    for (std::size_t d = 0; d < DIM; d++) {
      shift[d] = distribution_r(generator);
    }
    array_ops::as_eigen(neighbours[index]) =
        array_ops::as_eigen(neighbours_initial_locations[index]) +
        array_ops::as_eigen(shift);

    // change the phi of this neighbor using a Gaussian distribution
    neighbours_thermal_coordinates[index][1] += distribution_phi(generator);
  }

  // change the phi of the central atom randomly
  center_thermal_coordinates[1] += distribution_phi(generator);
}

void run() {
  const auto t1 = std::chrono::high_resolution_clock::now();

  solver::SolverVariables<DIM, NOI> solver_variables;

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  srand(time(nullptr) * (mpi_rank + 1));

  // userinput INPUT;
  solver_variables.input = input::load_userinput_from_working_dir();

  std::size_t noof_samples = solver_variables.input.noof_samples;
  double std_r = solver_variables.input.mean_pos_std_dev;
  double std_phi = solver_variables.input.phi_std_dev;
  double temp = solver_variables.input.T_ref;
  std::string data_file_name;
  std::ofstream f_data;

  const auto potential_material_name =
      get_potential_material_name(solver_variables.input.material);

  data_file_name = "./" + potential_material_name + "TrainingInputData_" +
                   std::to_string(temp) + "_" + std::to_string(mpi_rank) +
                   ".csv";
  f_data.open(data_file_name, std::ios::app);

  double energy{};
  double thermal_force_atom{};
  geometry::Point<DIM> force_central_atom;
  std::vector<geometry::Point<DIM>> forces;
  std::vector<double> thermal_forces_sampling_atom;
  std::vector<double> thermal_forces_neighbors;

  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  if (!solver_variables.input.clusters_compilation_file_name.has_value()) {
    // make a lattice and randomly perturb positions to generate clusters

    geometry::Point<DIM> center{{0.0, 0.0, 0.0}};

    std::array<std::array<double, DIM>, DIM> basis =
        solver_variables.materials[0].basis_vectors;

    geometry::Point<3> px{{basis[0][0], basis[1][0], basis[2][0]}};
    geometry::Point<3> py{{basis[0][1], basis[1][1], basis[2][1]}};
    geometry::Point<3> pz{{basis[0][2], basis[1][2], basis[2][2]}};

    std::vector<geometry::Point<DIM>> lattice;
    geometry::Point<DIM> lattice_point;
    std::vector<geometry::Point<DIM>> neighbours;
    std::vector<geometry::Point<DIM>> prospective_neighbours_initial_locations;
    std::vector<geometry::Point<DIM>> prospective_neighbours;
    std::vector<ThermalPoint> neighbours_thermal_coordinates;
    std::vector<ThermalPoint> prospective_neighbours_thermal_coordinates;

    double sigma_atom{};
    double sigma_neighbor{};
    double sigma_start = solver_variables.input.Phi_ref;
    auto initial_thermal_coordinates =
        ThermalPoint{{-log(constants::k_Boltzmann * temp), sigma_start}};
    ThermalPoint atom_thermal_coordinates = initial_thermal_coordinates;

    lattice.clear();
    prospective_neighbours.clear();
    prospective_neighbours_thermal_coordinates.clear();

    //  Make an initial uniform lattice
    for (int l = -4; l < 4; l++) {
      for (int m = -4; m < 4; m++) {
        for (int n = -4; n < 4; n++) {
          array_ops::as_eigen(lattice_point) =
              array_ops::as_eigen(center) +
              double(l) * array_ops::as_eigen(px) +
              double(m) * array_ops::as_eigen(py) +
              double(n) * array_ops::as_eigen(pz);
          lattice.push_back(lattice_point);
        }
      }
    }

    // Deform the initial lattice according to imposed deformation gradient
    Matrix displacement_gradient;
    Matrix deformation_gradient;
    for (std::size_t i = 0; i < DIM; i++) {
      for (std::size_t j = 0; j < DIM; j++) {
        displacement_gradient(static_cast<Eigen::Index>(i),
                              static_cast<Eigen::Index>(j)) =
            solver_variables.input.external_displacement_gradient[i][j];
      }
    }
    deformation_gradient =
        solver_variables.input.indenter_increment * displacement_gradient +
        Eigen::Matrix<double, DIM, DIM>::Identity(DIM, DIM);
    for (auto &p : lattice) {
      array_ops::as_eigen(p) = deformation_gradient * array_ops::as_eigen(p);
    }

    // Find an initial set of prospective neighbors
    for (const auto &point : lattice) {
      const auto d = array_ops::distance(center, point);
      if (d > 1.0e-3 && d <= (solver_variables.materials[0]
                                  .cluster_model->neighbor_cutoff_radius +
                              6 * std_r)) {
        prospective_neighbours.push_back(point);
        prospective_neighbours_thermal_coordinates.push_back(
            atom_thermal_coordinates);
      }
    }

    prospective_neighbours_initial_locations = prospective_neighbours;

    for (std::size_t i = 0; i < noof_samples; i++) {
      thermal_force_atom = 0.0;
      force_central_atom.fill(0.0);
      // randomly move the input parameters
      random_move(prospective_neighbours_initial_locations,
                  prospective_neighbours,
                  prospective_neighbours_thermal_coordinates,
                  atom_thermal_coordinates, sigma_start, std_phi, std_r);

      // find the real set of neighbors from the prospective list, keep the
      // cutoff a bit more to also include atoms that won't affect the cnetral
      // atom energy
      sigma_atom = exp(-atom_thermal_coordinates[1]);
      neighbours.clear();
      neighbours_thermal_coordinates.clear();
      for (std::size_t idx = 0; idx < prospective_neighbours.size(); idx++) {
        sigma_neighbor =
            exp(-prospective_neighbours_thermal_coordinates[idx][1]);
        const auto d = array_ops::distance(center, prospective_neighbours[idx]);
        if (d <= (solver_variables.materials[0]
                      .cluster_model->neighbor_cutoff_radius +
                  std::max(10 * (sigma_atom + sigma_neighbor), 0.1))) {
          neighbours.push_back(prospective_neighbours[idx]);
          neighbours_thermal_coordinates.push_back(
              prospective_neighbours_thermal_coordinates[idx]);
        }
      }

      auto atom_nodal_data =
          meshing::NodalDataMultispecies<NOI>{array_from_vec<NOI>(
              solver_variables.input.initial_impurity_concentrations)};
      std::vector<meshing::NodalDataMultispecies<NOI>> neighbours_nodal_data =
          std::vector(neighbours.size(), atom_nodal_data);

      // get the new cluster energy and forces
      energy =
          solver_variables.materials[0]
              .get_thermalized_energy_and_forces_metropolis(
                  forces, thermal_forces_sampling_atom,
                  thermal_forces_neighbors, center, atom_thermal_coordinates,
                  neighbours, neighbours_thermal_coordinates, atom_nodal_data,
                  neighbours_nodal_data);

      // print to the file
      f_data << std::setprecision(6) << center[0] << "," << center[1] << ","
             << center[2] << "," << atom_thermal_coordinates[1] << ",";

      for (std::size_t index = 0; index < neighbours.size(); index++) {
        thermal_force_atom -= thermal_forces_sampling_atom[index];
        array_ops::as_eigen(force_central_atom) -=
            array_ops::as_eigen(forces[index]);

        f_data << neighbours[index][0] << "," << neighbours[index][1] << ","
               << neighbours[index][2] << ","
               << neighbours_thermal_coordinates[index][1] << ",";
      }

      f_data << energy;

      f_data << "," << force_central_atom[0] << "," << force_central_atom[1]
             << "," << force_central_atom[2] << ","
             << -(DIM * thermal_force_atom / 2);

      for (std::size_t index = 0; index < neighbours.size(); index++) {

        f_data << "," << forces[index][0] << "," << forces[index][1] << ","
               << forces[index][2] << ","
               << -(DIM * thermal_forces_neighbors[index] / 2);
      }

      if (i < (noof_samples - 1))
        f_data << '\n';
    }

  } else {
    // read clusters from the file and compute energy / forces
    auto clusters_file = std::ifstream(
        solver_variables.input.clusters_compilation_file_name.value().c_str());
    if (!clusters_file.is_open())
      throw ::exception::QCException{
          std::string("Could not open clusters compilation file.")};
    auto line = std::string{};
    std::vector<double> words;
    std::size_t noof_atoms = 0;
    std::size_t line_ctr = 0;
    std::size_t ctr = 0;
    std::vector<geometry::Point<DIM>> neighbours;
    std::vector<geometry::Point<DIM>> neighbours_initial_locations;
    std::vector<ThermalPoint> neighbours_thermal_coordinates;
    line_ctr = 0;
    ctr = 0;
    while (std::getline(clusters_file, line)) {
      if (line_ctr % solver_variables.input.dataset_stride == 0) {
        if (ctr >= (mpi_rank * noof_samples) &&
            ctr < ((mpi_rank + 1) * noof_samples)) {

          neighbours_initial_locations.clear();
          neighbours_thermal_coordinates.clear();
          thermal_force_atom = 0.0;
          force_central_atom.fill(0.0);
          words = utils::string_split_many<double>(line, ',', ' ');
          const auto entries_per_atom = 4 + NOI + 1;
          noof_atoms = words.size() / entries_per_atom;

          geometry::Point<DIM> center{{words[1], words[2], words[3]}};
          auto atom_thermal_coordinates =
              ThermalPoint{{-log(constants::k_Boltzmann * temp), words[4]}};

          auto atom_nodal_data = meshing::NodalDataMultispecies<NOI>{};
          for (std::size_t si = 0; si < NOI + 1; si++)
            atom_nodal_data.impurity_concentrations[si] = words[5 + si];

          std::vector<meshing::NodalDataMultispecies<NOI>>
              neighbours_nodal_data;

          for (std::size_t n_idx = 1; n_idx < noof_atoms; n_idx++) {
            neighbours_initial_locations.push_back(
                geometry::Point<DIM>{{words[entries_per_atom * n_idx + 1],
                                      words[entries_per_atom * n_idx + 2],
                                      words[entries_per_atom * n_idx + 3]}});
            neighbours_thermal_coordinates.push_back(
                ThermalPoint{{-log(constants::k_Boltzmann * temp),
                              words[entries_per_atom * n_idx + 4]}});
            auto neigh_nodal_data = meshing::NodalDataMultispecies<NOI>{};
            for (std::size_t si = 0; si < NOI; si++)
              neigh_nodal_data.impurity_concentrations[si] =
                  words[entries_per_atom * n_idx + 5 + si];

            neighbours_nodal_data.push_back(neigh_nodal_data);
          }
          neighbours = neighbours_initial_locations;

          // randomly move the input parameters
          random_move(neighbours_initial_locations, neighbours,
                      neighbours_thermal_coordinates, atom_thermal_coordinates,
                      std_phi, std_r);

          // get the new cluster energy and forces
          energy = solver_variables.materials[0]
                       .get_thermalized_energy_and_forces_metropolis(
                           forces, thermal_forces_sampling_atom,
                           thermal_forces_neighbors, center,
                           atom_thermal_coordinates, neighbours,
                           neighbours_thermal_coordinates, atom_nodal_data,
                           neighbours_nodal_data);

          // print to the file
          f_data << std::setprecision(6) << center[0] << "," << center[1] << ","
                 << center[2] << "," << atom_thermal_coordinates[1] << ",";

          for (std::size_t si = 0; si < NOI + 1; si++)
            f_data << words[5 + si] << ",";

          for (std::size_t index = 0; index < neighbours.size(); index++) {
            thermal_force_atom -= thermal_forces_sampling_atom[index];
            array_ops::as_eigen(force_central_atom) -=
                array_ops::as_eigen(forces[index]);

            f_data << neighbours[index][0] << "," << neighbours[index][1] << ","
                   << neighbours[index][2] << ","
                   << neighbours_thermal_coordinates[index][1] << ",";

            for (std::size_t si = 0; si < NOI + 1; si++)
              f_data << words[entries_per_atom * (index + 1) + 5 + si] << ",";
          }

          f_data << energy;

          f_data << "," << force_central_atom[0] << "," << force_central_atom[1]
                 << "," << force_central_atom[2] << ","
                 << -(DIM * thermal_force_atom / 2);

          for (std::size_t index = 0; index < neighbours.size(); index++) {

            f_data << "," << forces[index][0] << "," << forces[index][1] << ","
                   << forces[index][2] << ","
                   << -(DIM * thermal_forces_neighbors[index] / 2);
          }

          if (ctr < noof_samples * (mpi_rank + 1) - 1)
            f_data << '\n';
        }
        ctr++;
      }
      line_ctr++;
    }
  }

  f_data.close();

  const auto t2 = std::chrono::high_resolution_clock::now();
  const auto time_span =
      std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  std::cout << "duration: " << time_span.count() << std::endl;
}
