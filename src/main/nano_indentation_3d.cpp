// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Quasistatic nanoindentation of a mesh with heat flow
 * @author P. Gupta
 */

#include "app/app.hpp"
#include "boundarycondition/load_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qc_env.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/dump_solver_variables.hpp"
#include "solver/multispecies_generic_solver_variables.hpp"
#include "solver/solver.hpp"
#include "utils/timer.hpp"
#include "vtkwriter/write_mesh.hpp"
#include <boost/mpi.hpp>

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;

} // namespace

template <std::size_t Noi>
void run(solver::SolverVariables<DIM, Noi> &solver_variables) {
  namespace array_ops = qcmesh::array_ops;

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  // Input file or mesh generation stuff

  solver_variables.materials.emplace_back(materials::create_material<DIM, Noi>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  if (mpi_rank == 0) {
    std::cout << "--------------------------------------------------------"
              << std::endl;
    std::cout << " WARNING! Please double-check the crystal lattice structure."
              << std::endl;
    std::cout << "	running :- "
              << "FCC" << std::endl;
    std::cout << "--------------------------------------------------------"
              << std::endl;
  }

  const auto &restart_file_name = solver_variables.input.restart_file_name;

  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  // upto here, periodic connectivity is not established
  // because domain bounds are not known, so periodic lengths
  // are unknown
  auto restart_data = netcdf_io::RestartData<DIM>{};
  std::tie(restart_data, solver_variables.local_mesh) =
      meshing::load_nc(solver_variables.input, solver_variables.materials);
  restart_data.apply_fixed_box = solver_variables.input.fixed_box.has_value();

  //=-=-=-=-=-=-=-=-=</Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=<Initialize periodic lengths and flags>-=-=-=-=-=-//

  if (mpi_rank == 0) {
    std::cout << "periodic lengths : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.lower)
              << " , "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.upper)
              << std::endl;
  }

  // redistribute the mesh again after this so periodic connectivities are
  // built based on the lengths
  meshing::redistribute(solver_variables.local_mesh);

  //=-=-=-=-=-=-=-=-=</Initialize periodic lengths and flags>-=-=-=-=-=-//

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "--------------------------------------------" << std::endl;
      std::cout << "domain is bound within " << std::endl;
      std::cout << "lower : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.lower)
                << std::endl;
      std::cout << "upper : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.upper)
                << std::endl;
      std::cout << "Verlet list buffer "
                << solver_variables.input.verlet_buffer_factor *
                       solver_variables.input.neighbour_update_displacement
                << std::endl;
    }
  }

  //=-=-=-=-=-=-=-=-=<Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIM>(
          solver_variables.input, restart_data.restartfile_iter);
  // =-=-=-=-=-=-=-=-=</Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//
  //        Repatoms and sampling atoms are built with qc mesh.          //
  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//

  auto clock = utils::Timer{};
  clock.tic();

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "-------------------------------------------------"
                << std::endl;
      std::cout << "--------------- Attempting initial repair--------"
                << std::endl;
      std::cout << "-------------------------------------------------"
                << std::endl;
    }
    // initial repair after triangulation
    meshing::repair_mesh(solver_variables.local_mesh);
  }

  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  if (!restart_file_name.has_value()) {
    // update boundaries
    solver_variables.boundary_conditions =
        boundarycondition::create_boundary_conditions(
            solver_variables.input.boundary_types,
            solver_variables.local_mesh.domain_bound);
  } else
    solver_variables.boundary_conditions =
        boundarycondition::load_boundary_condition<DIM>(
            restart_file_name.value());

  if (!restart_file_name.has_value()) {
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // output after initial repair
    vtkwriter::write_vtu_mesh_3d(solver_variables.local_mesh,
                                 meshing::REMESH_TOLERANCE,
                                 "initial_partition/");
  }

  MPI_Barrier(MPI_COMM_WORLD);

  double min_distance{};
  std::size_t id = 0;

  for (const auto &p :
       solver_variables.input.display_neighbourhoods_sampling_atoms) {
    min_distance = 9999.9;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.nodes.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      const auto &node =
          solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .position;

      if (array_ops::distance(node, p) < min_distance) {
        id = node_index;
        min_distance = array_ops::distance(node, p);
      }
    }

    if (min_distance < 1.0) {
      std::cout << mpi_rank << " , " << id << " , "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.nodes
                           .at(solver_variables.local_mesh.node_index_to_key.at(
                               id))
                           .position)
                << std::endl;
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Neighbourhood udpdate styles, "
              << "Verlet : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::VERLET)
              << " , Adaptive : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::ADAPTIVE)
              << " :: "
              << static_cast<std::size_t>(
                     solver_variables.input.neighbour_update_style)
              << std::endl;
  }

  solver_variables.relaxation_fire_parameters.time_step =
      solver_variables.input.fire_time_step;

  // Fire parameters
  solver_variables.relaxation_fire_parameters.alpha =
      solver_variables.input.fire_initial_alpha;
  solver_variables.relaxation_fire_parameters.steps_post_reset =
      solver_variables.input.fire_steps_post_reset;

  solver_variables.relaxation_fire_parameters.min_residual =
      solver_variables.input.fire_min_residual;

  // if there is an external force applicator
  solver_variables.apply_external_force = false;

  // if boundary conditions need to be applied
  solver_variables.apply_boundary_conditions = false;

  // if isentropic relaxation. If not, then its isothermal
  solver_variables.isentropic = false;

  // --------<Composite solver>------- //
  solver::Solver::set_solver_dofs<DIM, NIDOF>(solver_variables);

  solver::Solver::create_solution_vectors<DIM>(solver_variables);

  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<Noi>, DIM,
                                      NIDOF>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      solver_variables.input.max_iter, solver_variables.input.max_feval,
      solver_variables);

  if (restart_file_name.has_value() && restart_data.restartfile_iter > 2) {
    solver_variables.apply_boundary_conditions = true;

    solver_variables.apply_external_force = true;

    solver_variables.isentropic = solver_variables.input.isentropic;
  }

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<Noi>, DIM, NIDOF>(solver_variables);

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  // initial relaxation
  const auto display_zones = app::get_bool_env("DISPLAY_ZONES");
  const auto display_element_qualities =
      app::get_bool_env("DISPLAY_ELEMENT_QUALITIES");
  const auto dump_vtk = solver::DumpSolverVariablesVtk(
      restart_data.outputfile_iter, display_zones, display_element_qualities);

  if (restart_file_name.has_value())
    dump_vtk(solver_variables);
  solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                  meshing::NodalDataMultispecies<Noi>, DIM,
                                  NIDOF>(
      solver_variables.input.a_ref,
      solver_variables.input.max_thermal_force_per_atom,
      solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
  if (restart_file_name.has_value()) {
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;
    dump_vtk(solver_variables);
  }

//
#ifdef FINITE_TEMPERATURE
  if (solver_variables.isentropic) {
    // diffuse out the gradients. Maybe dynamic or quasistatic
    solver::Solver::dynamic_thermal_transport_ee<
        solver::DumpSolverVariablesVtk, meshing::NodalDataMultispecies<Noi>,
        DIM, NIDOF>(
        restart_data.external_strain_time_step, solver_variables.input.a_ref,
        solver_variables.input.max_thermal_force_per_atom,
        solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);

    if (!restart_file_name.has_value()) {
      // save final output after 1 loadstep
      dump_vtk(solver_variables);

      // save final restart after 1 loadstep
      solver::dump_solver_variables_nc(solver_variables, restart_data);
      ++restart_data.restartfile_iter;
    }
  }
#endif

  // restart_000000 is the initial file. (has restart_iter = 1)
  // restart_000001 is the state after first relaxation (has restart_iter = 2)
  // if using either of those as restart, then apply boundary conditions only after first
  // relaxation
  if (!restart_file_name.has_value() or restart_data.restartfile_iter <= 2) {
    solver_variables.apply_external_force = true;

    solver_variables.apply_boundary_conditions = true;

    solver_variables.isentropic = solver_variables.input.isentropic;

    // if not restarting, then arrange indenter at the top
    // plus one negative step. Determine highest point
    // on z axis. (x=0, y=0)

    double z_max = -99999.9;

    std::vector<geometry::Point<DIM>> nearest_center_points;

    bool found_axial_point = false;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.repatoms.locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      const auto &node =
          solver_variables.local_mesh.repatoms.locations[node_index];
      if (sqrt(node[0] * node[0] + node[1] * node[1]) < 1.0) {
        found_axial_point = true;

        if (node[2] > z_max) {
          id = node_index;
          z_max = node[2];
        }
      }
    }

    if (found_axial_point) {
      nearest_center_points.push_back(
          solver_variables.local_mesh.repatoms.locations[id]);
    }

    auto nearest_center_points_all_ranks =
        std::vector<std::vector<geometry::Point<DIM>>>(mpi_size);
    boost::mpi::all_gather(world, nearest_center_points,
                           nearest_center_points_all_ranks);

    z_max = -9999.9;
    geometry::Point<DIM> closest_center{};

    for (const auto &nearest_center_points_vec :
         nearest_center_points_all_ranks) {
      for (const auto &p : nearest_center_points_vec) {
        if (p[2] > z_max) {
          z_max = p[2];
          closest_center = p;
        }
      }
    }

    solver_variables.external_force_applicator->center[2] =
        closest_center[2] + solver_variables.external_force_applicator->radius -
        solver_variables.input.indenter_increment;

    restart_data.external_strain_time_step =
        solver_variables.input.strain_increment_time_step;
  }

  // store thermalized energy in sampling atoms
  solver::Solver::update_sampling_atom_energies<
      meshing::NodalDataMultispecies<Noi>, DIM, NIDOF>(solver_variables);

  for (unsigned int i = 0; i < solver_variables.input.external_iterations;
       i++) {
    solver_variables.external_force_applicator->update_position();
    // update mesh and sampling atoms not really required here because,
    // the only the position of force applicator is updated and no external displacements
    // of atoms are imposed.
    solver::Solver::update_sampling_atoms<meshing::NodalDataMultispecies<Noi>,
                                          DIM, NIDOF>(solver_variables);

    solver::Solver::update_repatom_forces<meshing::NodalDataMultispecies<Noi>,
                                          DIM, NIDOF>(solver_variables);

    if (restart_file_name.has_value())
      dump_vtk(solver_variables);
    solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                    meshing::NodalDataMultispecies<Noi>, DIM,
                                    NIDOF>(
        solver_variables.input.a_ref,
        solver_variables.input.max_thermal_force_per_atom,
        solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
    if (restart_file_name.has_value()) {
      solver::dump_solver_variables_nc(solver_variables, restart_data);
      ++restart_data.restartfile_iter;
      dump_vtk(solver_variables);
    }

#ifdef FINITE_TEMPERATURE
    if (solver_variables.isentropic) {
      // diffuse out the gradients. Maybe dynamic or quasistatic
      solver::Solver::dynamic_thermal_transport_ee<
          solver::DumpSolverVariablesVtk, meshing::NodalDataMultispecies<Noi>,
          DIM, NIDOF>(restart_data.external_strain_time_step,
                      solver_variables.input.a_ref,
                      solver_variables.input.max_thermal_force_per_atom,
                      solver_variables.input.max_force_per_atom,
                      solver_variables, dump_vtk);

      if (solver_variables.input.solution_files_frequency.has_value() &&
          (i % solver_variables.input.solution_files_frequency.value() == 0)) {
        // save final output after 1 loadstep
        dump_vtk(solver_variables);

        // save final restart after 1 loadstep
        solver::dump_solver_variables_nc(solver_variables, restart_data);
        ++restart_data.restartfile_iter;
      }
    }
#endif

    solver_variables.external_force_applicator->record_force(
        solver_variables.input.T_ref);
  }

  solver::Solver::destroy_snes<DIM>(solver_variables);

  // --------<\PETSC solver>------- //

  const auto elapsed_time = clock.toc();

  std::cout
      << "rank " << mpi_rank << " took " << elapsed_time << " with "
      << solver_variables.local_mesh.sampling_atoms.nodal_locations.size()
      << " nodal sampling atoms "
      << solver_variables.local_mesh.sampling_atoms.central_locations.size()
      << " central sampling atoms " << solver_variables.local_mesh.nodes.size()
      << " nodes and " << solver_variables.local_mesh.cells.size()
      << " elements" << std::endl;
}
int main(int argc, char *argv[]) {
  const auto env = QCEnv(&argc, &argv);

  try {
    auto userinput = input::load_userinput_from_working_dir();
    auto solver_variables =
        solver::create_solver_variables(std::move(userinput));
    std::visit([](auto &solver_variables) { run(solver_variables); },
               solver_variables);
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }

  // Data to save in restart_file
  // 1. mesh - elements (with node-ids)
  // 2. mesh - repatom coordinates (mesh nodes)
  // 3. repatom velocities
  // 4. mesh - edges/faces with neighbouring elements
  // 5. Boundary nodes

  return 0;
}
