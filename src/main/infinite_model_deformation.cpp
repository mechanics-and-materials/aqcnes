// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to quasistatically deform a bulk system assuming an infinite model of repeating clusters
 * @author S. Saxena, M. Spinola
 */

#include "materials/create_material.hpp"
#include "solver/infinite_model_relaxation.hpp"

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

} // namespace

void run() {
  const auto t1 = std::chrono::high_resolution_clock::now();

  solver::SolverVariables<DIM, NOI> solver_variables;

  std::array<std::array<bool, DIM>, 2> constrained{};
  solver_variables.input = input::load_userinput_from_working_dir();
  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  double ts{};
  double free_energy{};
  std::ofstream f_energy;
  f_energy.open("Energies.txt", std::ios::app);
  // Do an initial undeformed relaxation
  solver_variables.input.indenter_increment = 0.0;
  const auto relaxed_results = solver::relax_infinite<DIM, NOI, NIDOF>(
      solver_variables.input, solver_variables.materials[0],
      solver_variables.materials[0].basis_vectors, solver_variables.input.T_ref,
      solver_variables, constrained);
  ts = -1.5 * (constants::k_Boltzmann * solver_variables.input.T_ref) *
       (-log(constants::k_Boltzmann * solver_variables.input.T_ref) +
        relaxed_results.phi);
  free_energy = relaxed_results.e - ts;
  f_energy << "0.0"
           << " " << relaxed_results.phi << " " << relaxed_results.e << " "
           << free_energy << '\n';
  // Start the deformation loop
  for (auto &c : constrained)
    c.fill(true);
  for (unsigned int step = 1;
       step <= solver_variables.input.external_iterations; step++) {
    solver_variables.input.indenter_increment =
        static_cast<double>(step) /
        static_cast<double>(solver_variables.input.external_iterations);
    const auto results = solver::relax_infinite<DIM, NOI, NIDOF>(
        solver_variables.input, solver_variables.materials[0],
        relaxed_results.deformed_basis, solver_variables.input.T_ref,
        solver_variables, constrained);
    ts = -1.5 * (constants::k_Boltzmann * solver_variables.input.T_ref) *
         (-log(constants::k_Boltzmann * solver_variables.input.T_ref) +
          results.phi);
    free_energy = results.e - ts;
    f_energy << step << " " << results.phi << " " << results.e << " "
             << free_energy << '\n';
    solver_variables.input.Phi_ref = results.phi;
  }

  f_energy.close();

  const auto t2 = std::chrono::high_resolution_clock::now();
  const auto time_span =
      std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  std::cout << "duration: " << time_span.count() << std::endl;
}
