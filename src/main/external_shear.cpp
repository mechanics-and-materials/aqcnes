// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to perform a quasistatic displacement driven shear of a mesh with staggered relaxation steps
 * @author P. Gupta
 */

#include "app/app.hpp"
#include "boundarycondition/load_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "geometry/box.hpp"
#include "geometry/point.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "solver/dump_solver_variables.hpp"
#include "solver/solver.hpp"
#include "utils/timer.hpp"
#include "utils/to_string_with_zeros.hpp"
#include "vtkwriter/write_mesh.hpp"
#include <boost/mpi.hpp>

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

} // namespace

void run() {
  namespace array_ops = qcmesh::array_ops;

  solver::SolverVariables<DIM, NOI> solver_variables;

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  // 	// these are for computing quality. Need to make static
  // W2(0,0) = 1.0;
  // W2(0,1) = 0.5;
  // W2(1,0) = 0.0;
  // W2(1,1) = sqrt(3)/2.0;

  // W3(0,0) = 1.0; W3(0,1) = 0.5; W3(0,2) = 0.5;
  // W3(1,0) = 0.0; W3(1,1) = sqrt(3)/2.0; W3(1,2) = sqrt(3)/6.0;
  // W3(2,0) = 0.0; W3(2,1) = 0.0; W3(2,2) = sqrt(2.0/3.0);
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  solver_variables.input = input::load_userinput_from_working_dir();

  // Input file or mesh generation stuff
  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  if (mpi_rank == 0) {
    std::cout << "--------------------------------------------------------"
              << std::endl;
    std::cout << " WARNING! Please double-check the crystal lattice structure."
              << std::endl;
    std::cout << "	running :- "
              << "FCC" << std::endl;
    std::cout << "--------------------------------------------------------"
              << std::endl;
  }

  const auto &restart_file_name = solver_variables.input.restart_file_name;

  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //=-=-=-=-=-=-=-=-=<Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  // upto here, periodic connectivity is not established
  // because domain bounds are not known, so periodic lengths
  // are unknown
  auto restart_data = netcdf_io::RestartData<DIM>{};
  std::tie(restart_data, solver_variables.local_mesh) =
      meshing::load_nc(solver_variables.input, solver_variables.materials);
  restart_data.apply_fixed_box = solver_variables.input.fixed_box.has_value();

  //=-=-=-=-=-=-=-=-=<Initialize periodic lengths and flags>-=-=-=-=-=-//

  if (mpi_rank == 0) {
    std::cout << "periodic lengths : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.lower)
              << " , "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.upper)
              << std::endl;
  }

  // redistribute the mesh again after this so periodic connectivities are
  // built based on the lengths
  meshing::redistribute(solver_variables.local_mesh);

  //=-=-=-=-=-=-=-=-=</Initialize periodic lengths and flags>-=-=-=-=-=-//

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "--------------------------------------------" << std::endl;
      std::cout << "domain is bound within " << std::endl;
      std::cout << "lower : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.lower)
                << std::endl;
      std::cout << "upper : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.upper)
                << std::endl;
      std::cout << "Verlet list buffer "
                << solver_variables.input.verlet_buffer_factor *
                       solver_variables.input.neighbour_update_displacement
                << std::endl;
    }
  }

  //=-=-=-=-=-=-=-=-=<Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIM>(
          solver_variables.input, restart_data.restartfile_iter);
  // =-=-=-=-=-=-=-=-=</Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//
  //        Repatoms and sampling atoms are built with qc mesh.          //
  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//

  auto clock = utils::Timer{};
  clock.tic();

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "-------------------------------------------------"
                << std::endl;
      std::cout << "--------------- Attempting initial repair--------"
                << std::endl;
      std::cout << "-------------------------------------------------"
                << std::endl;
    }
    // initial repair after triangulation
    meshing::repair_mesh(solver_variables.local_mesh);
  }

  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  if (!restart_file_name.has_value()) {
    // update boundaries
    solver_variables.boundary_conditions =
        boundarycondition::create_boundary_conditions(
            solver_variables.input.boundary_types,
            solver_variables.local_mesh.domain_bound);
  } else
    solver_variables.boundary_conditions =
        boundarycondition::load_boundary_condition<DIM>(
            restart_file_name.value());

  if (!restart_file_name.has_value()) {
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // output after initial repair
    vtkwriter::write_vtu_mesh_3d(solver_variables.local_mesh,
                                 meshing::REMESH_TOLERANCE,
                                 "initial_partition/");
  }

  MPI_Barrier(MPI_COMM_WORLD);

  double min_distance{};
  geometry::Point<DIM> node;
  std::size_t id = 0;

  for (const auto &p :
       solver_variables.input.display_neighbourhoods_sampling_atoms) {

    min_distance = 9999.9;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.nodes.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      node =
          solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .position;

      if (array_ops::distance(node, p) < min_distance) {
        id = node_index;
        min_distance = array_ops::distance(node, p);
      }
    }
    if (min_distance < 1.0) {
      std::cout << mpi_rank << " , " << id << " , "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.nodes
                           .at(solver_variables.local_mesh.node_index_to_key.at(
                               id))
                           .position)
                << std::endl;
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Neighbourhood udpdate styles, "
              << "Verlet : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::VERLET)
              << " , Adaptive : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::ADAPTIVE)
              << " :: "
              << static_cast<std::size_t>(
                     solver_variables.input.neighbour_update_style)
              << std::endl;
  }

  solver_variables.relaxation_fire_parameters.time_step =
      solver_variables.input.fire_time_step;

  // Fire parameters
  solver_variables.relaxation_fire_parameters.alpha =
      solver_variables.input.fire_initial_alpha;
  solver_variables.relaxation_fire_parameters.steps_post_reset =
      solver_variables.input.fire_steps_post_reset;

  solver_variables.relaxation_fire_parameters.min_residual =
      solver_variables.input.fire_min_residual;

  // if there is an external force applicator
  solver_variables.apply_external_force = false;

  // if boundary conditions need to be applied
  solver_variables.apply_boundary_conditions = false;

  // if isentropic relaxation. If not, then its isothermal
  solver_variables.isentropic = false;

  double v_all_global{};
  double v_all_local{};
  double k_all_global{};
  double k_all_local{};
  double w_all_local{};
  double w_all_global{};

  // --------<PETSC solver>------- //
  solver::Solver::set_solver_dofs<DIM, NIDOF>(solver_variables);

  solver::Solver::create_solution_vectors<DIM>(solver_variables);

  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<NOI>, DIM,
                                      NIDOF>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      solver_variables.input.max_iter, solver_variables.input.max_feval,
      solver_variables);

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

  std::ofstream f_external;

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  const auto display_zones = app::get_bool_env("DISPLAY_ZONES");
  const auto display_element_qualities =
      app::get_bool_env("DISPLAY_ELEMENT_QUALITIES");
  const auto dump_vtk = solver::DumpSolverVariablesVtk(
      restart_data.outputfile_iter, display_zones, display_element_qualities);
  if (!restart_file_name.has_value()) {
    // do initial relaxation first.
    restart_data.apply_fixed_box = false;
    solver_variables.input.fixed_box = std::nullopt;

    dump_vtk(solver_variables);

    solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                    meshing::NodalDataMultispecies<NOI>, DIM,
                                    NIDOF>(
        solver_variables.input.a_ref,
        solver_variables.input.max_thermal_force_per_atom,
        solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;
    dump_vtk(solver_variables);

    const auto fixed_box_arrays = solver_variables.input.fixed_box.value();
    const auto fixed_box =
        qcmesh::geometry::Box<DIM>{fixed_box_arrays[0], fixed_box_arrays[1]};

    for (std::size_t repatom_index = 0;
         repatom_index < solver_variables.local_mesh.repatoms.locations.size();
         repatom_index++) {
      if (!qcmesh::geometry::contains(
              fixed_box,
              solver_variables.local_mesh.repatoms.locations[repatom_index])) {
        solver_variables.local_mesh.repatoms.if_fixed[repatom_index] = true;
      }
    }

    restart_data.apply_fixed_box = true;
    solver_variables.input.fixed_box = std::nullopt;

    restart_data.external_strain = 0.0;
    restart_data.external_strain_increment =
        solver_variables.input.indenter_increment;
    restart_data.external_strain_time_step =
        solver_variables.input.strain_increment_time_step;
  }

  solver_variables.isentropic = solver_variables.input.isentropic;

  // update energies of sampling atoms
  solver::Solver::update_sampling_atom_energies<
      meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);
  v_all_local = 0.0;
  k_all_local = 0.0;
  w_all_local = 0.0;

  // calculate total energy (phase average of total hamiltonian).
  // thermal coordinate[0] is \Omega/m
  for (std::size_t atom_index = 0;
       atom_index <
       solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
           solver_variables.local_mesh.global_ghost_indices.size();
       atom_index++) {
    v_all_local +=
        solver_variables.local_mesh.sampling_atoms.nodal_energies[atom_index];

#ifdef FINITE_TEMPERATURE
    k_all_local +=
        3.0 *
        exp(-solver_variables.local_mesh.repatoms
                 .thermal_coordinates[atom_index][0]) *
        solver_variables.local_mesh.sampling_atoms.nodal_weights[atom_index] /
        2.0;
#endif

    w_all_local +=
        solver_variables.local_mesh.sampling_atoms.nodal_weights[atom_index];
  }

  for (std::size_t atom_index = 0;
       atom_index <
       solver_variables.local_mesh.sampling_atoms.central_locations.size();
       atom_index++) {
    v_all_local +=
        solver_variables.local_mesh.sampling_atoms.central_energies[atom_index];

#ifdef FINITE_TEMPERATURE
    k_all_local +=
        3.0 *
        exp(-solver_variables.local_mesh.sampling_atoms
                 .central_ido_fs[atom_index][0]) *
        solver_variables.local_mesh.sampling_atoms.central_weights[atom_index] /
        2.0;
#endif

    w_all_local +=
        solver_variables.local_mesh.sampling_atoms.central_weights[atom_index];
  }

  v_all_global = qcmesh::mpi::all_reduce_sum(v_all_local);
  k_all_global = qcmesh::mpi::all_reduce_sum(k_all_local);
  w_all_global = qcmesh::mpi::all_reduce_sum(w_all_local);

  if (mpi_rank == 0) {
    std::cout << "beginning with external shear "
              << restart_data.external_strain << " and shear increment "
              << restart_data.external_strain_increment << std::endl;
  }

  // get the left and right dislocations
  geometry::Point<DIM> local_dislocation_l;
  geometry::Point<DIM> local_dislocation_r;
  geometry::Point<DIM> external_force;
  double local_dislocation_mass_l{};
  double local_dislocation_mass_r{};

  local_dislocation_l.fill(0.0);
  local_dislocation_r.fill(0.0);
  local_dislocation_mass_l = 0.0;
  local_dislocation_mass_r = 0.0;

  for (std::size_t repatom_index = 0;
       repatom_index <
       solver_variables.local_mesh.repatoms.locations.size() -
           solver_variables.local_mesh.global_ghost_indices.size();
       repatom_index++) {
    if (!solver_variables.local_mesh.repatoms.if_fixed[repatom_index] &&
        solver_variables.local_mesh.sampling_atoms
                .nodal_centro_symmetry[repatom_index] > 1.0) {
      if (solver_variables.local_mesh.repatoms.locations[repatom_index][0] <=
          0.0) {
        array_ops::as_eigen(local_dislocation_l) +=
            array_ops::as_eigen(
                solver_variables.local_mesh.repatoms.locations[repatom_index]) *
            solver_variables.local_mesh.sampling_atoms
                .nodal_centro_symmetry[repatom_index];

        local_dislocation_mass_l += solver_variables.local_mesh.sampling_atoms
                                        .nodal_centro_symmetry[repatom_index];
      } else {
        array_ops::as_eigen(local_dislocation_r) +=
            array_ops::as_eigen(
                solver_variables.local_mesh.repatoms.locations[repatom_index]) *
            solver_variables.local_mesh.sampling_atoms
                .nodal_centro_symmetry[repatom_index];

        local_dislocation_mass_r += solver_variables.local_mesh.sampling_atoms
                                        .nodal_centro_symmetry[repatom_index];
      }
    }
  }

  geometry::Point<DIM> dislocation_l;
  geometry::Point<DIM> dislocation_r;
  double mass_l{};
  double mass_r{};

  dislocation_l.fill(0.0);
  dislocation_r.fill(0.0);

  mass_l = qcmesh::mpi::all_reduce_sum(local_dislocation_mass_l);
  mass_r = qcmesh::mpi::all_reduce_sum(local_dislocation_mass_r);

  const auto world = boost::mpi::communicator{};
  boost::mpi::all_reduce(world, local_dislocation_l.data(), DIM,
                         dislocation_l.data(), std::plus<>());
  boost::mpi::all_reduce(world, local_dislocation_r.data(), DIM,
                         dislocation_r.data(), std::plus<>());

  array_ops::as_eigen(dislocation_l) /= mass_l;
  array_ops::as_eigen(dislocation_r) /= mass_r;

  solver_variables.local_mesh.repatoms.get_fixed_region_force(external_force);
  std::string filename = "ExternalStrainForce";

#ifdef FINITE_TEMPERATURE
  filename += "_" +
              utils::to_string_with_zeros(
                  static_cast<int>(solver_variables.input.T_ref), 3) +
              "K";
#endif

  filename += ".dat";

  if (mpi_rank == 0) {
    f_external.open(filename, ios::app);
    f_external << restart_data.external_strain << " "
               << array_ops::as_streamable(external_force) << " "
               << array_ops::distance(dislocation_l, dislocation_r) << " "
               << v_all_global << " " << k_all_global << " " << w_all_global
               << std::endl;
    f_external.close();
  }

  restart_data.external_strain += restart_data.external_strain_increment;

  geometry::Point<DIM> displacement;
  displacement.fill(0.0);

  for (unsigned int i = 0; i < solver_variables.input.external_iterations;
       i++) {
    for (auto &p : solver_variables.local_mesh.repatoms.locations) {
      displacement[0] = p[1] * restart_data.external_strain_increment;
      array_ops::as_eigen(p) += array_ops::as_eigen(displacement);
    }

    // update mesh
    solver::Solver::update_mesh<DIM, NIDOF,
                                meshing::NodalDataMultispecies<NOI>>(
        solver_variables);

    solver::Solver::update_sampling_atoms<meshing::NodalDataMultispecies<NOI>,
                                          DIM, NIDOF>(solver_variables);

    solver::Solver::update_repatom_forces<meshing::NodalDataMultispecies<NOI>,
                                          DIM, NIDOF>(solver_variables);

    solver::Solver::composite_relax<solver::DumpSolverVariablesVtk,
                                    meshing::NodalDataMultispecies<NOI>, DIM,
                                    NIDOF>(
        solver_variables.input.a_ref,
        solver_variables.input.max_thermal_force_per_atom,
        solver_variables.input.max_force_per_atom, solver_variables, dump_vtk);
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;
    dump_vtk(solver_variables);

#ifdef FINITE_TEMPERATURE
    if (solver_variables.isentropic) {
      // diffuse out the gradients. Maybe dynamic or quasistatic
      solver::Solver::dynamic_thermal_transport_ee<
          solver::DumpSolverVariablesVtk, meshing::NodalDataMultispecies<NOI>,
          DIM, NIDOF>(restart_data.external_strain_time_step,
                      solver_variables.input.a_ref,
                      solver_variables.input.max_thermal_force_per_atom,
                      solver_variables.input.max_force_per_atom,
                      solver_variables, dump_vtk);
    }
#endif
    // save final output after 1 loadstep
    dump_vtk(solver_variables);

    // save final restart after 1 loadstep
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // update energies of sampling atoms
    solver::Solver::update_sampling_atom_energies<
        meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

    v_all_local = 0.0;
    k_all_local = 0.0;
    w_all_local = 0.0;

    // calculate total energy (phase average of total hamiltonian).
    // thermal coordinate[0] is \Omega/m
    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.nodal_locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         atom_index++) {
      v_all_local +=
          solver_variables.local_mesh.sampling_atoms.nodal_energies[atom_index];

#ifdef FINITE_TEMPERATURE
      k_all_local +=
          3.0 *
          exp(-solver_variables.local_mesh.repatoms
                   .thermal_coordinates[atom_index][0]) *
          solver_variables.local_mesh.sampling_atoms.nodal_weights[atom_index] /
          2.0;
#endif
      w_all_local +=
          solver_variables.local_mesh.sampling_atoms.nodal_weights[atom_index];
    }

    for (std::size_t atom_index = 0;
         atom_index <
         solver_variables.local_mesh.sampling_atoms.central_locations.size();
         atom_index++) {
      v_all_local += solver_variables.local_mesh.sampling_atoms
                         .central_energies[atom_index];
#ifdef FINITE_TEMPERATURE
      k_all_local += 3.0 *
                     exp(-solver_variables.local_mesh.sampling_atoms
                              .central_ido_fs[atom_index][0]) *
                     solver_variables.local_mesh.sampling_atoms
                         .central_weights[atom_index] /
                     2.0;
#endif
      w_all_local += solver_variables.local_mesh.sampling_atoms
                         .central_weights[atom_index];
    }

    v_all_global = qcmesh::mpi::all_reduce_sum(v_all_local);
    k_all_global = qcmesh::mpi::all_reduce_sum(k_all_local);
    w_all_global = qcmesh::mpi::all_reduce_sum(w_all_local);

    local_dislocation_l.fill(0.0);
    local_dislocation_r.fill(0.0);
    local_dislocation_mass_l = 0.0;
    local_dislocation_mass_r = 0.0;
    dislocation_l.fill(0.0);
    dislocation_r.fill(0.0);

    for (std::size_t repatom_index = 0;
         repatom_index <
         solver_variables.local_mesh.repatoms.locations.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         repatom_index++) {
      if (!solver_variables.local_mesh.repatoms.if_fixed[repatom_index] &&
          solver_variables.local_mesh.sampling_atoms
                  .nodal_centro_symmetry[repatom_index] > 1.0) {
        if (solver_variables.local_mesh.repatoms.locations[repatom_index][0] <=
            0.0) {
          array_ops::as_eigen(local_dislocation_l) +=
              array_ops::as_eigen(solver_variables.local_mesh.repatoms
                                      .locations[repatom_index]) *
              solver_variables.local_mesh.sampling_atoms
                  .nodal_centro_symmetry[repatom_index];

          local_dislocation_mass_l += solver_variables.local_mesh.sampling_atoms
                                          .nodal_centro_symmetry[repatom_index];
        } else {
          array_ops::as_eigen(local_dislocation_r) +=
              array_ops::as_eigen(solver_variables.local_mesh.repatoms
                                      .locations[repatom_index]) *
              solver_variables.local_mesh.sampling_atoms
                  .nodal_centro_symmetry[repatom_index];

          local_dislocation_mass_r += solver_variables.local_mesh.sampling_atoms
                                          .nodal_centro_symmetry[repatom_index];
        }
      }
    }

    mass_l = qcmesh::mpi::all_reduce_sum(local_dislocation_mass_l);
    mass_r = qcmesh::mpi::all_reduce_sum(local_dislocation_mass_r);
    boost::mpi::all_reduce(world, local_dislocation_l.data(), DIM,
                           dislocation_l.data(), std::plus<>());
    boost::mpi::all_reduce(world, local_dislocation_r.data(), DIM,
                           dislocation_r.data(), std::plus<>());

    array_ops::as_eigen(dislocation_l) /= mass_l;
    array_ops::as_eigen(dislocation_r) /= mass_r;

    solver_variables.local_mesh.repatoms.get_fixed_region_force(external_force);

    if (mpi_rank == 0) {
      f_external.open(filename, ios::app);
      f_external << restart_data.external_strain << " "
                 << array_ops::as_streamable(external_force) << " "
                 << array_ops::distance(dislocation_l, dislocation_r) << " "
                 << v_all_global << " " << k_all_global << " " << w_all_global
                 << std::endl;
      f_external.close();
    }

    restart_data.external_strain += restart_data.external_strain_increment;
  }

  solver::Solver::destroy_snes<DIM>(solver_variables);

  // --------<\PETSC solver>------- //

  const auto elapsed_time = clock.toc();

  std::cout
      << "rank " << mpi_rank << " took " << elapsed_time << " with "
      << solver_variables.local_mesh.sampling_atoms.nodal_locations.size()
      << " nodal sampling atoms "
      << solver_variables.local_mesh.sampling_atoms.central_locations.size()
      << " central sampling atoms " << solver_variables.local_mesh.nodes.size()
      << " nodes and " << solver_variables.local_mesh.cells.size()
      << " elements" << std::endl;
}
