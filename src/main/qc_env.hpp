// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include <boost/mpi.hpp>
#include <memory>
#include <petscsys.h>
#include <string_view>

/**
 * @brief Manages initialization and finalization of PETSc.
 */
class PetscEnv {
public:
  /**
   * @brief Initializes PETSc. PETSc is finalized automatically when the
   * instance of PetscEnv is destroyed.
   *
   * @param argc A pointer to `argc` as provided to main().
   * @param argv A pointer to `argv` as provided to main().
   * @param database The path to a PETSC database file.
   * @param help Additional help to print.
   */
  inline PetscEnv(int *const argc, char ***const argv,
                  const std::string_view &database = {},
                  const std::string_view &help = {})
      : token((initialize(argc, argv, database, help), this), &finalize) {}

private:
  /**
   * @brief Initializes PETSc.
   */
  static inline void initialize(int *const argc, char ***const argv,
                                const std::string_view &database,
                                const std::string_view &help) {
    PetscInitialize(argc, argv, database.empty() ? nullptr : database.data(),
                    help.empty() ? nullptr : help.data());
  }

  /**
   * @brief Finalizes PETSc.
   */
  static inline void finalize(void *) { PetscFinalize(); }

  using Token = std::unique_ptr<PetscEnv, decltype(&finalize)>;
  Token token;
};

class QCEnv {
  boost::mpi::environment mpi_env;
  PetscEnv petsc_env;

public:
  inline QCEnv(int *const argc, char ***const argv,
               const std::string_view &database = {},
               const std::string_view &help = {})
      : mpi_env(*argc, *argv), petsc_env(argc, argv, database, help) {
#ifdef MULTI_THREADING
    omp_set_num_threads(4);
#endif
  }
};
