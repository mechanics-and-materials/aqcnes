// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "qc_env.hpp"
#include <boost/mpi.hpp>
#include <iostream>
#include <petscsys.h>
#include <stdexcept>

extern void run();

/** @file
 * @brief main function for workflows.
 */
int main(int argc, char *argv[]) {
  const auto env = QCEnv(&argc, &argv);

  try {
    run();
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }

  return 0;
}
