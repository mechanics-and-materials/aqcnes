// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Initial lattice and mesh generation
 * @authors P. Gupta, S. Saxena, G. Bräunlich, M. Spinola
 */

#include "cli/cli.hpp"
#include "geometry/point.hpp"
#include "lattice/generate_lattice.hpp"
#include "lattice/serialization/lattice_generator_node.hpp"
#include "meshing/create_mesh.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/traits.hpp"
#include "meshing/write_mesh_to_nc.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/mesh/orient_elements.hpp"
#include "qcmesh/mesh/serialization/node.hpp"
#include "qcmesh/mesh/serialization/simplex_cell.hpp"
#include "qcmesh/mesh/traits/node.hpp"
#include "qcmesh/mesh/traits/transfer_data.hpp"
#include "qcmesh/mesh/trim.hpp"
#include "qcmesh/mpi/mpi_sub_range.hpp"
#include "qcmesh/triangulation/triangulation.hpp"
#include "vtkwriter/write_mesh.hpp"
#include "vtkwriter/write_vtu_triangulation.hpp"
#include <boost/mpi.hpp>
#include <boost/program_options.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>
#include <optional>

/**
 * @brief Implementation of @ref qcmesh::mesh:traits::TransferData for @ref lattice::LatticeGeneratorNode -> @ref lattice::LatticeGeneratorNodalData.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::TransferData<lattice::LatticeGeneratorNode<D>,
                                          lattice::LatticeGeneratorNodalData> {
  static void transfer_data(const lattice::LatticeGeneratorNode<D> &from,
                            lattice::LatticeGeneratorNodalData &to) {
    to = from.data;
  }
};

/**
 * @brief Implementation of @ref qcmesh::mesh:traits::Postiion for @ref lattice::LatticeGeneratorNode.
 */
template <std::size_t D>
struct qcmesh::mesh::traits::Position<lattice::LatticeGeneratorNode<D>> {
  constexpr static std::size_t DIMENSION = D;
  static const std::array<double, D> &
  position(const lattice::LatticeGeneratorNode<D> &node) {
    return node.position;
  }
};

/**
 * @brief Implementation of @ref meshing::traits::NodeType for @ref lattice::LatticeGeneratorNode<D>.
 */
template <std::size_t D>
struct meshing::traits::NodeType<lattice::LatticeGeneratorNode<D>> {
  static int node_type(const lattice::LatticeGeneratorNode<D> &node) {
    return node.data.type;
  }
};

/**
 * @brief Implementation of @ref meshing:traits::MaterialId for @ref lattice::LatticeGeneratorNode.
 */
template <std::size_t D>
struct meshing::traits::MaterialId<lattice::LatticeGeneratorNode<D>> {
  static std::size_t material_id(const lattice::LatticeGeneratorNode<D> &node) {
    return node.data.material_index;
  }
};

/**
 * @brief Implementation of @ref meshing:traits::MaterialId for @ref qcmesh::mesh::Node with @ref lattice::LatticeGeneratorNodalData.
 */
template <std::size_t D>
struct meshing::traits::MaterialId<
    qcmesh::mesh::Node<D, lattice::LatticeGeneratorNodalData>> {
  static std::size_t material_id(
      const qcmesh::mesh::Node<D, lattice::LatticeGeneratorNodalData> &node) {
    return node.data.material_index;
  }
};

namespace {

/**
 * @brief All options for the CLI.
 */
struct CliArgs {
  std::filesystem::path project_folder = ".";
  bool output_initial_triangulations = false;
  bool display_element_markers = false;
  std::optional<bool> save_lattice_vectors = std::nullopt;
  std::filesystem::path output_folder = "mesh_files";
  std::filesystem::path partition_folder = "initial_partition/";
  std::filesystem::path lattice_parameter_file =
      "latticegenerator_parameters.json";
};

cli::Result<CliArgs> parse_cmd_line(const int argc, const char **const argv) {
  return cli::Parser<CliArgs>{}
      .add_option("project-folder", &CliArgs::project_folder,
                  "Folder containing the lattice generator config")
      .add_option("output-initial-triangulations",
                  &CliArgs::output_initial_triangulations,
                  "Output initial triangulations")
      .add_option("display-element-markers", &CliArgs::display_element_markers,
                  "Display Element Markers")
      .add_option("save-lattice-vectors", &CliArgs::save_lattice_vectors,
                  "Save lattice vectors (overrides config)")
      .add_option("output-folder", &CliArgs::output_folder,
                  "Folder to save mesh files")
      .add_option("partition-folder", &CliArgs::partition_folder,
                  "Folder to save the initial partition of the mesh")
      .add_option("lattice-parameter-file", &CliArgs::lattice_parameter_file,
                  "Input file containing the parameters to create the lattice.")
      .parse(argc, argv);
}

constexpr std::size_t DIM = 3;

using GlobalMesh =
    qcmesh::mesh::SimplexMesh<DIM, DIM, lattice::LatticeGeneratorNodalData,
                              qcmesh::mesh::EmptyData>;
using LocalMesh = meshing::QCMesh<DIM, DIM>;

#undef FINITE_TEMPERATURE

struct PartialMesh {
  std::vector<qcmesh::mesh::Node<DIM, lattice::LatticeGeneratorNodalData>>
      nodes{};
  std::vector<qcmesh::mesh::SimplexCell<DIM, qcmesh::mesh::EmptyData>> cells{};
};

} // namespace

namespace boost::serialization {

/**
 * @brief Provides serialization of qcmesh::mesh::SimplexCell.
 */
template <typename Archive>
void serialize(Archive &ar, PartialMesh &data, const unsigned int) {
  ar &data.nodes;
  ar &data.cells;
}
} // namespace boost::serialization

/**
 * See https://www.boost.org/doc/libs/1_82_0/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
BOOST_CLASS_IMPLEMENTATION(PartialMesh, object_serializable)
BOOST_CLASS_TRACKING(PartialMesh, track_never)

std::vector<PartialMesh> partition_mesh(const GlobalMesh &global_mesh) {
  const auto mpi_size =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());
  auto local_meshes = std::vector<PartialMesh>{};
  local_meshes.reserve(mpi_size);
  auto global_cell = global_mesh.cells.begin();
  for (std::size_t i = 0; i < mpi_size; i++) {
    const auto [start_idx, range_size] =
        qcmesh::mpi::sub_range(global_mesh.cells.size(), mpi_size, i);
    auto local_cells = decltype(PartialMesh{}.cells){};
    auto local_nodes = decltype(PartialMesh{}.nodes){};
    auto locally_added_nodes = std::set<qcmesh::mesh::NodeId>{};
    local_cells.reserve(range_size);
    for (std::size_t j = 0; j < range_size; j++) {
      const auto &[_, cell] = *global_cell;
      local_cells.push_back(cell);
      ++global_cell;
      for (const auto node_id : cell.nodes) {
        const auto [_, is_new_cell] = locally_added_nodes.emplace(node_id);
        if (is_new_cell)
          local_nodes.push_back(global_mesh.nodes.at(node_id));
      }
    }
    local_meshes.emplace_back(PartialMesh{local_nodes, local_cells});
  }
  return local_meshes;
}

namespace {

LocalMesh
distribute_mesh(const GlobalMesh &global_mesh,
                const std::vector<materials::MaterialBase<DIM>> &materials) {
  const auto local_meshes = partition_mesh(global_mesh);
  auto local_mesh = PartialMesh{};
  boost::mpi::scatter(boost::mpi::communicator{}, local_meshes, local_mesh, 0);
  return meshing::create_mesh(local_mesh.nodes, local_mesh.cells, materials);
}

void write_periodic_file(const std::filesystem::path &output_folder,
                         const qcmesh::geometry::Box<DIM> &domain_bounds) {
  const std::string periodic_path = output_folder / "periodic_info.txt";

  std::cout << "For periodic simulations, give following period lengths "
            << std::endl;
  std::cout << std::setprecision(10)
            << " lx = " << domain_bounds.upper[0] - domain_bounds.lower[0]
            << std::endl
            << " ly = " << domain_bounds.upper[1] - domain_bounds.lower[1]
            << std::endl
            << " lz = " << domain_bounds.upper[2] - domain_bounds.lower[2]
            << std::endl;
  std::cout << "and following x,y,z minima " << std::endl;
  std::cout << std::setprecision(10) << " x_min = " << domain_bounds.lower[0]
            << std::endl
            << " y_min = " << domain_bounds.lower[1] << std::endl
            << " z_min = " << domain_bounds.lower[2] << std::endl;

  std::ofstream f_periodic;
  f_periodic.open(periodic_path, ios::app);

  for (const auto x : domain_bounds.lower)
    f_periodic << x << " ";
  f_periodic << std::endl;
  for (std::size_t i = 0; i < DIM; i++)
    f_periodic << std::setprecision(10)
               << domain_bounds.upper[i] - domain_bounds.lower[i] << " ";
  f_periodic << std::endl;
  f_periodic.close();
}

class NullBuffer : public std::streambuf {
public:
  int overflow(int c) override { return c; }
};

/**
 * @brief Info logger which only logs on rank=0.
 */
std::ostream &log() {
  static NullBuffer null_buffer;
  static std::ostream null_stream(&null_buffer);

  if (boost::mpi::communicator{}.rank() == 0)
    return std::cout << "ℹ️ ";
  return null_stream;
}
/**
 * @brief Warning logger which only logs on rank=0.
 */
std::ostream &warn() {
  static NullBuffer null_buffer;
  static std::ostream null_stream(&null_buffer);

  if (boost::mpi::communicator{}.rank() == 0)
    return std::cerr << "⚠️ ";
  return null_stream;
}

std::string mesh_file_name(const std::string &base,
                           const std::size_t lattice_size) {
  auto name = base + "_" + utils::to_string_with_zeros(lattice_size, 6) + ".nc";
  const auto world = boost::mpi::communicator{};
  boost::mpi::broadcast(world, name, 0);
  return name;
}

template <std::size_t Dimension>
std::vector<std::array<double, Dimension>> unzip_lattice_points(
    const std::vector<lattice::LatticeGeneratorNode<Dimension>> &nodes) {
  auto out = std::vector<std::array<double, Dimension>>{};
  out.reserve(nodes.size());
  for (const auto &node : nodes)
    out.push_back(node.position);
  return out;
}

void generate_mesh(const CliArgs &args) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();

  const auto materials = std::vector{materials::MaterialBase<DIM>{}};

  const auto config = lattice::load_lattice_parameters_from_json<DIM>(
      args.project_folder / args.lattice_parameter_file);
  auto lattice_data = (mpi_rank == 0) ? lattice::generate_lattice(config)
                                      : lattice::LatticeData<DIM>{};

  boost::mpi::broadcast(world, lattice_data.atomistic_domain.lower, 0);
  boost::mpi::broadcast(world, lattice_data.atomistic_domain.upper, 0);

  log() << " domain bounds :" << std::endl
        << " lower :\t"
        << array_ops::as_streamable(lattice_data.domain_bounds.lower)
        << std::endl
        << " upper :\t"
        << array_ops::as_streamable(lattice_data.domain_bounds.upper)
        << std::endl;
  const auto lattice_points = unzip_lattice_points(lattice_data.lattice_nodes);
  auto cells = (mpi_rank == 0)
                   ? qcmesh::triangulation::triangulate(lattice_points)
                   : std::vector<std::array<std::size_t, DIM + 1>>{};
  if (mpi_rank == 0)
    qcmesh::triangulation::clean_0_vol_cells_at_boundary(
        cells, lattice_points, constants::geometric_tolerance);
  log() << "finished initial triangulation" << std::endl
        << " ------------------- " << std::endl;

  if (mpi_rank == 0 && args.output_initial_triangulations)
    vtkwriter::write_vtu_triangulation(
        lattice_points, cells, constants::geometric_tolerance,
        std::filesystem::path{"./InitialTriangulation.vtu"});

  auto global_mesh =
      (mpi_rank == 0)
          ? qcmesh::mesh::create_local_unconnected_simplex_mesh<
                lattice::LatticeGeneratorNodalData, qcmesh::mesh::EmptyData>(
                lattice_data.lattice_nodes, cells, mpi_rank)
          : GlobalMesh{};
  log() << "built global mesh" << std::endl;
  log() << " ------------------- " << std::endl;

  qcmesh::mesh::trim<DIM, DIM>(global_mesh, lattice_data.domain_bounds,
                               constants::geometric_tolerance);
  auto zero_vol_cells = qcmesh::mesh::orient_elements(global_mesh);
  qcmesh::mesh::orient_local_0_vol_cells(global_mesh, zero_vol_cells);

  log() << "trimmed global mesh " << std::endl;
  log() << " ------------------- " << std::endl;

  auto local_mesh = distribute_mesh(global_mesh, materials);
  std::cout << "rank=" << mpi_rank << " uniformly distributed the mesh "
            << std::endl;

  local_mesh.initialize_element_vectors();

  std::cout << "rank=" << mpi_rank << " locally updated elements " << std::endl;
  // assign lattice vectors also according to coarsenings
  // from generate lattice. Very important if there is
  // an initial strain.
  local_mesh.initialize_bravais_lattice(lattice_data.atomistic_domain,
                                        materials);

  std::cout << "rank=" << mpi_rank << " initialize bravais lattice "
            << std::endl;

  local_mesh.atomistic_domain = lattice_data.atomistic_domain;
  local_mesh.initialize_elements_atomistic_flag();

  std::cout << "rank=" << mpi_rank << " initialized atomistics flags "
            << std::endl;

  const auto save_lattice_vectors = args.save_lattice_vectors.has_value()
                                        ? args.save_lattice_vectors.value()
                                        : config.save_lattice_vectors;
  const auto can_save_lattice_vectors =
      lattice::traits::can_save_lattice_vectors(config.method);
  if (save_lattice_vectors && !can_save_lattice_vectors)
    warn() << "save_lattice_vectors is set to true, but selected "
              "lattice generator does not support this. Setting to false."
           << std::endl;
  const auto mesh_folder = args.project_folder / args.output_folder;
  std::filesystem::create_directories(mesh_folder);

  // save mesh .nc file and .pvtu file for visualization
  if (DIM == 3) {
    log() << "saving initial mesh vis file in " << mesh_folder << std::endl
          << " below are the number of degenerate elements in the mesh"
          << std::endl;
    vtkwriter::write_vtu_mesh_3d_minimal(local_mesh, meshing::REMESH_TOLERANCE,
                                         mesh_folder,
                                         args.display_element_markers);
  }
  const auto output_mesh_file =
      mesh_folder /
      mesh_file_name(config.name, lattice_data.lattice_nodes.size());

  log() << "saving mesh file to start simulation in " << output_mesh_file
        << std::endl;
  meshing::write_mesh_to_nc(output_mesh_file, local_mesh,
                            save_lattice_vectors && can_save_lattice_vectors);

  if (mpi_rank == 0)
    write_periodic_file(mesh_folder, lattice_data.domain_bounds);

  if (mpi_rank == 0) {
    std::ofstream f_mesh;
    f_mesh.open(mesh_folder / "mesh_names.txt", ios::app);
    f_mesh << output_mesh_file << std::endl;
    f_mesh.close();
  }

  world.barrier();

  log() << "testing if file is readable -- " << output_mesh_file << std::endl;

  auto local_mesh_read =
      meshing::load_mesh_from_nc(output_mesh_file, materials);

  std::cout << " rank=" << mpi_rank << " ✅" << std::endl;

  if (DIM == 3) {
    const auto partition_folder = args.project_folder / args.partition_folder;
    local_mesh_read.initialize_elements_atomistic_flag();

    log() << "readable file writing in the directory " << partition_folder
          << std::endl
          << " below are the number of degenerate elements in the mesh"
          << std::endl;

    std::filesystem::create_directories(partition_folder);
    vtkwriter::write_vtu_mesh_3d(local_mesh_read, meshing::REMESH_TOLERANCE,
                                 partition_folder);
  }
}

} // namespace

int main(int argc, const char **argv) {
  const auto result = parse_cmd_line(argc, argv);
  if (result.status != cli::Status::Success)
    return result.return_code;
  const auto env = boost::mpi::environment{};

  try {
    generate_mesh(result.args);
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }
  return 0;
}
