// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Remeshing example
 * @author P. Gupta
 */

#include "app/app.hpp"
#include "boundarycondition/create_boundary_condition.hpp"
#include "boundarycondition/load_external_force_applicator.hpp"
#include "materials/create_material.hpp"
#include "meshing/load_mesh_from_nc.hpp"
#include "meshing/mesh_repair/repair_mesh.hpp"
#include "meshing/qc_mesh.hpp"
#include "meshing/redistribute_mesh.hpp"
#include "neighbourhoods/neighbourhoods.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/dump_solver_variables.hpp"
#include "solver/solver.hpp"
#include "utils/timer.hpp"
#include "vtkwriter/write_mesh.hpp"
#include <boost/mpi.hpp>

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;
constexpr std::size_t NOI = 1;

} // namespace

void run() {
  namespace array_ops = qcmesh::array_ops;

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  solver::SolverVariables<DIM, NOI> solver_variables;
  solver_variables.input = input::load_userinput_from_working_dir();

  // Input file or mesh generation stuff
  solver_variables.materials.emplace_back(materials::create_material<DIM, NOI>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));
  if (mpi_rank == 0) {
    std::cout << "--------------------------------------------------------"
              << std::endl;
    std::cout << " WARNING! Please double-check the crystal lattice structure."
              << std::endl;
    std::cout << "	running :- "
              << "FCC" << std::endl;
    std::cout << "--------------------------------------------------------"
              << std::endl;
  }

  const auto &restart_file_name = solver_variables.input.restart_file_name;

  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //=-=-=-=-=-=-=-=-=<Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  // upto here, periodic connectivity is not established
  // because domain bounds are not known, so periodic lengths
  // are unknown
  auto restart_data = netcdf_io::RestartData<DIM>{};
  std::tie(restart_data, solver_variables.local_mesh) =
      meshing::load_nc(solver_variables.input, solver_variables.materials);
  restart_data.apply_fixed_box = solver_variables.input.fixed_box.has_value();

  //=-=-=-=-=-=-=-=-=</Initialize the mesh>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //=-=-=-=-=-=-=-=-=<Initialize periodic lengths and flags>-=-=-=-=-=-//

  solver_variables.local_mesh.periodic_offsets.resize(
      solver_variables.local_mesh.periodic_offset_directions.size());

  for (std::size_t i = 0;
       i < solver_variables.local_mesh.periodic_offset_directions.size(); i++) {
    solver_variables.local_mesh.periodic_offsets[i].fill(0.0);

    for (std::size_t d = 0; d < DIM; d++) {
      solver_variables.local_mesh.periodic_offsets[i][d] +=
          double(solver_variables.local_mesh.periodic_offset_directions[i][d]) *
          solver_variables.local_mesh.periodic_lengths[d];
    }
  }

  if (mpi_rank == 0) {
    std::cout << "periodic lengths : "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.lower)
              << " , "
              << array_ops::as_streamable(
                     solver_variables.local_mesh.domain_bound.upper)
              << std::endl;
  }
  // redistribute the mesh again after this so periodic connectivities are
  // built based on the lengths
  meshing::redistribute(solver_variables.local_mesh);

  //=-=-=-=-=-=-=-=-=</Initialize periodic lengths and flags>-=-=-=-=-=-//

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "--------------------------------------------" << std::endl;
      std::cout << "domain is bound within " << std::endl;
      std::cout << "lower : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.lower)
                << std::endl;
      std::cout << "upper : "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.domain_bound.upper)
                << std::endl;
      std::cout << "Verlet list buffer "
                << solver_variables.input.verlet_buffer_factor *
                       solver_variables.input.neighbour_update_displacement
                << std::endl;
    }
  }

  //=-=-=-=-=-=-=-=-=<Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//
  solver_variables.external_force_applicator =
      boundarycondition::load_external_force_applicator<DIM>(
          solver_variables.input, restart_data.restartfile_iter);
  // =-=-=-=-=-=-=-=-=</Initialize the indenter>-=-=-=-=-=-=-=-=-=-=-=-=-=-//

  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//
  //        Repatoms and sampling atoms are built with qc mesh.          //
  //---------------------------------------------------------------------//
  //---------------------------------------------------------------------//

  auto clock = utils::Timer{};
  clock.tic();

  if (!restart_file_name.has_value()) {
    if (mpi_rank == 0) {
      std::cout << "-------------------------------------------------"
                << std::endl;
      std::cout << "--------------- Attempting initial repair--------"
                << std::endl;
      std::cout << "-------------------------------------------------"
                << std::endl;
    }
    // initial repair after triangulation
    meshing::repair_mesh(solver_variables.local_mesh);
  }

  // resize mesh communication vectors based on mpi size and mpi rank
  solver_variables.local_mesh.reinitialize_exchange_vectors();

  // update boundaries
  solver_variables.boundary_conditions =
      boundarycondition::create_boundary_conditions(
          solver_variables.input.boundary_types,
          solver_variables.local_mesh.domain_bound);

  if (!restart_file_name.has_value()) {
    std::cout << mpi_rank << " , "
              << solver_variables.boundary_conditions.size() << std::endl;
    solver::dump_solver_variables_nc(solver_variables, restart_data);
    ++restart_data.restartfile_iter;

    // output after initial repair
    vtkwriter::write_vtu_mesh_3d(solver_variables.local_mesh,
                                 meshing::REMESH_TOLERANCE,
                                 "initial_partition/");
  }

  MPI_Barrier(MPI_COMM_WORLD);

  double min_distance{};
  std::size_t id = 0;

  for (const auto &p :
       solver_variables.input.display_neighbourhoods_sampling_atoms) {
    min_distance = 9999.9;

    for (std::size_t node_index = 0;
         node_index <
         solver_variables.local_mesh.nodes.size() -
             solver_variables.local_mesh.global_ghost_indices.size();
         node_index++) {
      const auto &node =
          solver_variables.local_mesh.nodes
              .at(solver_variables.local_mesh.node_index_to_key.at(node_index))
              .position;

      if (array_ops::distance(node, p) < min_distance) {
        id = node_index;
        min_distance = array_ops::distance(node, p);
      }
    }

    if (min_distance < 1.0) {
      std::cout << mpi_rank << " , " << id << " , "
                << array_ops::as_streamable(
                       solver_variables.local_mesh.nodes
                           .at(solver_variables.local_mesh.node_index_to_key.at(
                               id))
                           .position)
                << std::endl;
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Neighbourhood udpdate styles, "
              << "Verlet : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::VERLET)
              << " , Adaptive : "
              << (solver_variables.input.neighbour_update_style ==
                  input::NeighbourUpdateStyle::ADAPTIVE)
              << " :: "
              << static_cast<std::size_t>(
                     solver_variables.input.neighbour_update_style)
              << std::endl;
  }

  solver_variables.relaxation_fire_parameters.time_step =
      solver_variables.input.fire_time_step;

  // Fire parameters
  solver_variables.relaxation_fire_parameters.alpha =
      solver_variables.input.fire_initial_alpha;
  solver_variables.relaxation_fire_parameters.steps_post_reset =
      solver_variables.input.fire_steps_post_reset;

  solver_variables.relaxation_fire_parameters.min_residual =
      solver_variables.input.fire_min_residual;

  // if there is an external force applicator
  solver_variables.apply_external_force = false;

  // if boundary conditions need to be applied
  solver_variables.apply_boundary_conditions = false;

  // if isentropic relaxation. If not, then its isothermal
  solver_variables.isentropic = false;

  //   * Basic flow of one full relaxation step is (after moving repatoms,
  //   * based on use-case)
  //  *
  //   *
  //   * update mesh nodes from repatoms
  //  	 solver::Solver::updateMesh(solver_variables);
  //    *
  //  * update sampling atoms and neighbourhoods from mesh nodes
  //  	 solver::Solver::updateSamplingAtoms(solver_variables);
  //    *
  //  * update forces on repatoms using sampling atoms
  //    solver::Solver::updateRepatomForces(solver_variables);
  //  *
  //  * relax using FIRE+PETSC
  //    solver::Solver::CompositeRelax();

  // --------<Composite solver>------- //
  solver::Solver::set_solver_dofs<DIM, NIDOF>(solver_variables);

  solver::Solver::create_solution_vectors<DIM>(solver_variables);

  solver::Solver::create_and_set_snes<meshing::NodalDataMultispecies<NOI>, DIM,
                                      NIDOF>(
      solver_variables.input.abs_tolerance,
      solver_variables.input.rel_tolerance, solver_variables.input.s_tolerance,
      solver_variables.input.max_iter, solver_variables.input.max_feval,
      solver_variables);

  solver::Solver::allocate_initial_neighbourhoods<
      meshing::NodalDataMultispecies<NOI>, DIM, NIDOF>(solver_variables);

  solver_variables.local_mesh.compute_nodal_centro_symmetry();

  dump_solver_variables_vtk(solver_variables, restart_data.outputfile_iter++,
                            app::get_bool_env("DISPLAY_ZONES"),
                            app::get_bool_env("DISPLAY_ELEMENT_QUALITIES"));

  // displace atoms, relax atoms within the "fixed bounds" which is actually
  // free bound. Then measure and store the energy of the free sampling atoms
  solver::Solver::destroy_snes<DIM>(solver_variables);
  // --------<\Composite solver>------- //

  const auto elapsed_time = clock.toc();

  std::cout
      << "rank " << mpi_rank << " took " << elapsed_time << " with "
      << solver_variables.local_mesh.sampling_atoms.nodal_locations.size()
      << " nodal sampling atoms "
      << solver_variables.local_mesh.sampling_atoms.central_locations.size()
      << " central sampling atoms " << solver_variables.local_mesh.nodes.size()
      << " nodes and " << solver_variables.local_mesh.cells.size()
      << " elements" << std::endl;
}
