# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

include(FindPkgConfig)
pkg_check_modules(PETSc PETSc IMPORTED_TARGET REQUIRED)
find_package(AQCNES_qcmesh REQUIRED)
find_package(Boost COMPONENTS mpi REQUIRED)
find_package(CGAL REQUIRED)
if(${MULTI_THREADING})
  find_package(OpenMP REQUIRED)
endif()

add_library(qc_main qc_main.cpp)
target_link_libraries(qc_main
  PUBLIC PkgConfig::PETSc
  PUBLIC Boost::mpi
  PRIVATE qc_compile_defines
)
if(${MULTI_THREADING})
  target_link_libraries(qc_main
    PRIVATE OpenMP
  )
endif()

add_executable(mesh_generator mesh_generator.cpp)
target_compile_features(mesh_generator PUBLIC cxx_std_17)
target_link_libraries(mesh_generator
  cli
  input
  lattice
  vtkwriter
  utils
  app
  geometry
  materials
  meshing
  meshing_nc
  qcmesh::triangulation
)

if (${FINITE_TEMPERATURE})
  set(T_SUFFIX "_FINITE_K")
else()
  set(T_SUFFIX "_0K")
endif()


set(Relaxation3D "Relaxation3D${T_SUFFIX}")
add_executable(${Relaxation3D} relaxation_3d.cpp)
target_link_libraries(${Relaxation3D}
  boundarycondition_nc
  nn_material
  input
  lattice
  vtkwriter
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  materials
  meshing
  meshing_nc
  neighbourhoods
  solver
  geometry
  optional-kim-material
)


add_executable(Remeshing3D remeshing_3d.cpp)
target_link_libraries(Remeshing3D
  qc_main
  boundarycondition_nc
  nn_material
  input
  lattice
  vtkwriter
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  materials
  meshing
  meshing_nc
  neighbourhoods
  solver
  geometry
)


add_executable(ExternalShear external_shear.cpp)
target_link_libraries(ExternalShear
  qc_main
  boundarycondition_nc
  input
  nn_material
  lattice
  vtkwriter
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  materials
  meshing
  meshing_nc
  neighbourhoods
  solver
  geometry
)

set(ElasticConstants "ElasticConstants${T_SUFFIX}")
add_executable(${ElasticConstants} elastic_constants.cpp)
target_link_libraries(${ElasticConstants}
  qc_main
  boundarycondition_nc
  input
  nn_material
  lattice
  vtkwriter
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  geometry
  materials
  meshing
  meshing_nc
  neighbourhoods
  solver
)


add_executable(EnergyMinima energy_minima.cpp)
target_link_libraries(EnergyMinima
  boundarycondition
  input
  nn_material
  lattice
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  geometry
  materials
  meshing
  neighbourhoods
  solver
)


add_executable(InfiniteModelRelaxation infinite_model_relaxation.cpp)
target_link_libraries(InfiniteModelRelaxation
  boundarycondition
  input
  nn_material
  lattice
  geometry
  materials
  meshing
  meshing_nc
  neighbourhoods
  solver
  vtkwriter
  app
  qcmesh::mpi
  optional-kim-material
)


add_executable(InfiniteModelDeformation infinite_model_deformation.cpp)
target_link_libraries(InfiniteModelDeformation
  qc_main
  boundarycondition
  meshing
  input
  lattice
  materials
  neighbourhoods
  solver
  nn_material
  geometry
  vtkwriter
  app
  qcmesh::mpi
)


add_executable(NanoIndentation3D nano_indentation_3d.cpp)
target_link_libraries(NanoIndentation3D
  boundarycondition_nc
  input
  lattice
  materials
  nn_material
  vtkwriter
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  meshing
  meshing_nc
  neighbourhoods
  solver
  geometry
  optional-kim-material
)

if (${FINITE_TEMPERATURE})

add_executable(ml_training_data_generator ml_training_data_generator.cpp)
target_link_libraries(ml_training_data_generator
  qc_main
  boundarycondition
  materials
  meshing
  input
  lattice
  neighbourhoods
  solver
  nn_material
  utils
  app
  qcmesh::array_ops
  qcmesh::mpi
  geometry
)

endif()
