// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Example to relax a bulk system assuming an infinite model of repeating clusters
 * @author S. Saxena, M. Spinola
 */

#include "solver/infinite_model_relaxation.hpp"
#include "materials/create_material.hpp"
#include "qc_env.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "solver/multispecies_generic_solver_variables.hpp"

namespace {

constexpr std::size_t DIM = 3;
constexpr std::size_t NIDOF = 2;

} // namespace

template <std::size_t Noi>
void run(solver::SolverVariables<DIM, Noi> &solver_variables) {
  namespace array_ops = array_ops;

  solver_variables.materials.emplace_back(materials::create_material<DIM, Noi>(
      solver_variables.input,
      lattice::load_lattice_cache<DIM>(solver_variables.input.lattice_file)));

  std::array<std::array<bool, DIM>, 2> constrained{};
  for (auto &c : constrained)
    c.fill(solver_variables.input.periodicity[0].value().relaxation_constraint);

  const auto results = solver::relax_infinite<DIM, Noi, NIDOF>(
      solver_variables.input, solver_variables.materials[0],
      solver_variables.materials[0].basis_vectors, solver_variables.input.T_ref,
      solver_variables, constrained);

  auto lattice_vectors = std::array<geometry::Point<DIM>, DIM>{};
  array_ops::as_eigen_matrix(lattice_vectors) =
      array_ops::as_eigen_matrix(results.deformed_basis).transpose();
  std::cout << "Relaxed Basis Vectors:\n";
  std::cout << array_ops::as_streamable(lattice_vectors[0]) << "\n";
  std::cout << array_ops::as_streamable(lattice_vectors[1]) << "\n";
  std::cout << array_ops::as_streamable(lattice_vectors[2]) << "\n";

  double k = 1.5 * (constants::k_Boltzmann * solver_variables.input.T_ref);
  double ts = -1.5 * (constants::k_Boltzmann * solver_variables.input.T_ref) *
              (-log(constants::k_Boltzmann * solver_variables.input.T_ref) +
               results.phi);
  double free_energy = results.e + k - ts;

  std::ofstream f_phi;
  f_phi.open("RelaxedInfo.txt", std::ios::app);

  f_phi << std::setprecision(10) << solver_variables.input.T_ref << " "
        << 2 * lattice_vectors[2][2] << " " << results.phi << " "
        << results.e + k << " " << free_energy << '\n';

#ifdef FINITE_TEMPERATURE
  std::cout << '\n' << "Relaxed Phi Value: " << results.phi << '\n';
#endif
}
int main(int argc, char *argv[]) {
  const auto env = QCEnv(&argc, &argv);

  const auto t1 = std::chrono::high_resolution_clock::now();

  try {
    auto userinput = input::load_userinput_from_working_dir();
    auto solver_variables =
        solver::create_solver_variables(std::move(userinput));
    std::visit([](auto &solver_variables) { run(solver_variables); },
               solver_variables);
  } catch (const std::exception &e) {
    std::cerr << "💥 " << e.what() << "\n";
    return 1;
  }

  const auto t2 = std::chrono::high_resolution_clock::now();
  const auto time_span =
      std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  std::cout << "duration: " << time_span.count() << std::endl;

  return 0;
}
