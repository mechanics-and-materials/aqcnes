// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief Getting the material name from user input
 */

#pragma once

#include "input/userinput.hpp"

template <typename, typename = void>
struct HasPotentialMaterialName : std::false_type {};

template <typename T>
struct HasPotentialMaterialName<
    T, std::void_t<decltype(std::declval<T>().potential_material_name)>>
    : std::true_type {};

inline std::string
get_potential_material_name(const input::MaterialProperties &material) {

  return std::visit(
      [](const auto &properties) -> std::string {
        if constexpr (HasPotentialMaterialName<
                          std::decay_t<decltype(properties)>>::value) {
          return properties.potential_material_name;
        }
        return "";
      },
      material);
}
