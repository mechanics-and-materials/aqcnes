// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief function implementations for vtk Outputs
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "constants/constants.hpp"
#include "exception/qc_exception.hpp"
#include "meshing/distribute_vec.hpp"
#include "meshing/traits.hpp"
#include "vtkwriter/detail.hpp"
#include <array>
#include <boost/mpi.hpp>
#include <filesystem>
#include <optional>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkMPIController.h>
#include <vtkTetra.h>
#include <vtkXMLPUnstructuredGridWriter.h>

namespace vtkwriter {
namespace detail {
template <typename VTKDATA>
void write_pvtk_files_3d(const VTKDATA &data,
                         const std::filesystem::path &file_name) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(data);
  writer->SetFileName(file_name.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();
  controller->Finalize(1);

  MPI_Barrier(MPI_COMM_WORLD);
}
} // namespace detail

/**
 * Write the vertices of a mesh to a file (in 3D, also write vtu files)
 *
 * @param mesh The mesh to write to the file
 * @param output_centro_symmetry if centrosymmetry needs to be written for sampling atoms
 * @param file_name Path to the file to write vertex data into
 * @param boundary_condition If non-empty, includes zone data in the output based
 *        on the boundary conditions
 */
template <typename MeshType, typename BoundaryCondition>
void write_vtk_point_data_3d(
    const MeshType &mesh,
    const std::vector<BoundaryCondition> &boundary_conditions,
    const bool output_centro_symmetry, const std::filesystem::path &file_name,
    const bool display_element_qualities) {
  auto vtknodes = vtkSmartPointer<vtkPoints>::New();

  auto node_cp = detail::vtk_create_array<vtkDoubleArray>("CentroSymmetry");

  auto element_ranks = detail::vtk_create_array<vtkDoubleArray>("MPI_RANK");
  auto repatom_forces = detail::vtk_create_array<vtkDoubleArray>("Force", 3);
  repatom_forces->SetNumberOfTuples(mesh.repatoms.locations.size());
  auto repatom_weights = detail::vtk_create_array<vtkDoubleArray>("Weights");
  auto samplingatom_weights =
      detail::vtk_create_array<vtkDoubleArray>("Sampling Weights");

  // how many copies of the nodes among all ranks.
  // allocated in renumbering of mesh according to PETSC
  // assign node multiplicities using interproc connections
  std::vector<int> node_multiplicities;
  node_multiplicities.resize(mesh.nodes.size(), 1);

  const auto n_repatoms =
      meshing::traits::CountRepatoms<MeshType>::count_repatoms(mesh);
  const auto n_ghosts = mesh.global_ghost_indices.size();
  const auto n_locals = mesh.nodes.size() - n_ghosts;
  meshing::distribute_vec(n_ghosts, n_locals, n_repatoms,
                          mesh.global_ghost_indices, node_multiplicities);

  for (std::size_t atom_ctr = 0; atom_ctr < mesh.repatoms.locations.size();
       atom_ctr++) {
    vtknodes->InsertNextPoint(mesh.repatoms.locations[atom_ctr][0],
                              mesh.repatoms.locations[atom_ctr][1],
                              mesh.repatoms.locations[atom_ctr][2]);

    repatom_forces->SetTuple3(atom_ctr, mesh.repatoms.forces[atom_ctr][0],
                              mesh.repatoms.forces[atom_ctr][1],
                              mesh.repatoms.forces[atom_ctr][2]);

    repatom_weights->InsertNextValue(node_multiplicities[atom_ctr]);

    samplingatom_weights->InsertNextValue(
        mesh.sampling_atoms.nodal_weights[atom_ctr]);

    if (output_centro_symmetry)
      node_cp->InsertNextValue(
          mesh.sampling_atoms.nodal_centro_symmetry[atom_ctr]);
  }

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  auto vtkumesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
  vtkumesh->SetPoints(vtknodes);

  for (std::size_t i_el = 0; i_el < mesh.cells.size(); i_el++) {
    auto tetrahedron = vtkSmartPointer<vtkTetra>::New();
    const auto cell_key = mesh.simplex_index_to_key[i_el];

    for (int d = 0; d < 3 + 1; d++) {
      const auto node_key = mesh.cells.at(cell_key).nodes[d];

      if (mesh.nodes.find(node_key) == mesh.nodes.end())
        throw exception::QCException{"Node key could not be found "
                                     "in mesh.nodes."};

      const auto node_index = mesh.nodes.at(node_key).data.mesh_index;
      tetrahedron->GetPointIds()->SetId(d, node_index);
    }
    vtkumesh->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());

    element_ranks->InsertNextValue(mpi_rank);
  }

  vtkumesh->GetCellData()->AddArray(element_ranks);

  auto node_markers = detail::vtk_create_array<vtkIntArray>("NodeMarkers");
  for (const auto symmetry : mesh.sampling_atoms.nodal_centro_symmetry)
    node_markers->InsertNextValue(static_cast<int>(symmetry > 1.0));
  vtkumesh->GetPointData()->AddArray(node_markers);

  vtkumesh->GetPointData()->SetVectors(repatom_forces);
  vtkumesh->GetPointData()->AddArray(repatom_weights);
  vtkumesh->GetPointData()->AddArray(samplingatom_weights);

  if (output_centro_symmetry)
    vtkumesh->GetPointData()->AddArray(node_cp);

#ifdef FINITE_TEMPERATURE
  auto repatom_entropies = detail::vtk_create_array<vtkDoubleArray>("Entropy");
  auto repatom_entropy_rates =
      detail::vtk_create_array<vtkDoubleArray>("Entropy Rates");
  auto repatom_dissipation_potential =
      detail::vtk_create_array<vtkDoubleArray>("Dissipation Potential");
  auto repatom_temperature = detail::vtk_create_array<vtkDoubleArray>("T");
  auto repatom_position_variance =
      detail::vtk_create_array<vtkDoubleArray>("Sigma");
  auto repatom_thermal_force =
      detail::vtk_create_array<vtkDoubleArray>("ThermalForce");
  auto repatom_position_entropy =
      detail::vtk_create_array<vtkDoubleArray>("Phi_q");
  for (const auto item : mesh.repatoms.entropy)
    repatom_entropies->InsertNextValue(item);
  for (const auto item : mesh.repatoms.entropy_production_rates)
    repatom_entropy_rates->InsertNextValue(item);
  for (const auto item : mesh.repatoms.dissipative_potential)
    repatom_dissipation_potential->InsertNextValue(item);
  for (const auto &item : mesh.repatoms.thermal_coordinates) {
    repatom_temperature->InsertNextValue(exp(-item[0]) /
                                         (constants::k_Boltzmann));
    repatom_position_variance->InsertNextValue(exp(-item[1]));
  }
  for (const auto item : mesh.repatoms.thermal_forces)
    repatom_thermal_force->InsertNextValue(item);
  for (const auto &item : mesh.repatoms.thermal_coordinates)
    repatom_position_entropy->InsertNextValue(item[1]);
  vtkumesh->GetPointData()->AddArray(repatom_entropies);
  vtkumesh->GetPointData()->AddArray(repatom_entropy_rates);
  vtkumesh->GetPointData()->AddArray(repatom_dissipation_potential);
  vtkumesh->GetPointData()->AddArray(repatom_temperature);
  vtkumesh->GetPointData()->AddArray(repatom_position_variance);
  vtkumesh->GetPointData()->AddArray(repatom_thermal_force);
  vtkumesh->GetPointData()->AddArray(repatom_position_entropy);
#endif

  for (std::size_t i = 0; i < MeshType::NOI; i++) {
    const auto name_1 = "Impurity Concentration " + std::to_string(i + 1);
    const auto name_2 = "Chemical Potential " + std::to_string(i + 1);
    const auto name_3 = "Concentration Update Rate " + std::to_string(i + 1);
    auto repatom_impurity_concentration =
        detail::vtk_create_array<vtkDoubleArray>(name_1);
    auto repatom_chemical_potential =
        detail::vtk_create_array<vtkDoubleArray>(name_2);
    auto repatom_conc_update_rate =
        detail::vtk_create_array<vtkDoubleArray>(name_3);

    for (const auto &item : mesh.repatoms.nodal_data_points) {
      repatom_impurity_concentration->InsertNextValue(
          item.impurity_concentrations[i]);
      repatom_chemical_potential->InsertNextValue(item.chemical_potentials[i]);
      repatom_conc_update_rate->InsertNextValue(
          item.impurity_concentration_update_rates[i]);
    }
    vtkumesh->GetPointData()->AddArray(repatom_impurity_concentration);
    vtkumesh->GetPointData()->AddArray(repatom_chemical_potential);
    vtkumesh->GetPointData()->AddArray(repatom_conc_update_rate);
  }

  if (!boundary_conditions.empty()) {
    auto zone_ctr = std::size_t{0};
    for (const auto &boundary_condition : boundary_conditions) {
      const auto zone_name = std::string{"Zone_"} + std::to_string(zone_ctr++);
      auto zone_data = detail::vtk_create_array<vtkDoubleArray>(zone_name);
      for (const auto &location : mesh.repatoms.locations)
        zone_data->InsertNextValue(
            boundary_condition.point_in_boundary(location) ? 1.0 : 0.0);
      vtkumesh->GetPointData()->AddArray(zone_data);
    }
  }

  if (display_element_qualities) {
    auto element_qualities = detail::vtk_create_array<vtkDoubleArray>("Metric");
    for (std::size_t i_el = 0; i_el < mesh.cells.size(); i_el++) {
      const auto &cell = mesh.cells.at(mesh.simplex_index_to_key[i_el]);
      element_qualities->InsertNextValue(cell.data.quality);
    }
    vtkumesh->GetCellData()->AddArray(element_qualities);
  }

  std::filesystem::create_directories(file_name.parent_path());

  detail::write_pvtk_files_3d(vtkumesh, file_name);
}
} // namespace vtkwriter
