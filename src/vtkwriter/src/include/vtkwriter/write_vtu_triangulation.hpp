// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors P. Gupta
 */

#pragma once

#include <array>
#include <filesystem>
#include <optional>
#include <vector>

namespace vtkwriter {

/**
 * Write triangulation data (3D version)
 *
 * @param points vector of points comprising of 3D nodes.
 * @param triangulation 4-tuples of indices indexing points.
 * @param zero_volume_tolerance If not set to std::nullopt, include element markers for elements with volume below tolerance.
 * @param filename Path to the file to write into.
 */
void write_vtu_triangulation(
    const std::vector<std::array<double, 3>> &points,
    const std::vector<std::array<std::size_t, 4>> &triangulation,
    const std::optional<double> zero_volume_tolerance,
    const std::filesystem::path &filename);

/**
 * Write triangulation data (2D version)
 *
 * @param points vector of points comprising of 2D nodes
 * @param triangulation 3-tuples of indices indexing points
 * @param zero_volume_tolerance If not set to std::nullopt, include element markers for elements with volume below tolerance.
 * @param filename Path to the file to write into
 */
void write_vtu_triangulation(
    const std::vector<std::array<double, 2>> &points,
    const std::vector<std::array<std::size_t, 3>> &triangulation,
    const std::optional<double> zero_volume_tolerance,
    const std::filesystem::path &filename);

} // namespace vtkwriter
