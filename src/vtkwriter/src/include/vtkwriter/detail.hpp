// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors P. Gupta
 */

#pragma once

#include "vtkAppendPolyData.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"

namespace vtkwriter::detail {
/**
 * @brief Creates a smartpointer wrapped vtkUnstructuredGrid, containing points.
 */
vtkSmartPointer<vtkUnstructuredGrid>
vtk_create_grid_with_points(const vtkSmartPointer<vtkPoints> &points);

/**
 * @brief Creates a smartpointer wrapped instance of a VTK array T, preallocating to size n_components.
 */
template <class T>
vtkSmartPointer<T> vtk_create_array(const std::string &name,
                                    const std::size_t n_components = 1) {
  auto arr = vtkSmartPointer<T>::New();
  arr->SetNumberOfComponents(n_components);
  arr->SetName(name.c_str());
  return arr;
}
} // namespace vtkwriter::detail
