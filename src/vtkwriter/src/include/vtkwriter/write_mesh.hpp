// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @brief function implementations for vtk Outputs
 * @authors P. Gupta, S. Saxena
 */

#pragma once

#include "exception/qc_exception.hpp"
#include "meshing/traits.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "vtkwriter/detail.hpp"
#include <boost/mpi.hpp>
#include <filesystem>
#include <optional>
#include <vtkAppendPolyData.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkMPIController.h>
#include <vtkPointData.h>
#include <vtkTetra.h>
#include <vtkTriangle.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLPPolyDataWriter.h>
#include <vtkXMLPUnstructuredGridWriter.h>

namespace vtkwriter {
/**
 * Write a 2d mesh to a file
 *
 * @param mesh The mesh to write to the file
 * @param file_name Path to the file to write into
 */
template <typename MeshType>
void write_mesh_2d(const MeshType &mesh,
                   const std::filesystem::path &file_name) {
  auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  auto triangles = vtkSmartPointer<vtkCellArray>::New();
  auto element_ranks = detail::vtk_create_array<vtkDoubleArray>("MPI_RANK");
  auto element_ids = detail::vtk_create_array<vtkIntArray>("IDs");
  auto element_qualities = detail::vtk_create_array<vtkDoubleArray>("Quality");

  for (int i_node = 0; i_node < mesh.MeshNodes.size(); i_node++)
    vtknodes->InsertNextPoint(mesh.MeshNodes[i_node][0],
                              mesh.MeshNodes[i_node][1], 0.0);

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();
  for (int i_el = 0; i_el < mesh.NoofElements; i_el++) {
    auto triangle = vtkSmartPointer<vtkTriangle>::New();

    for (int d = 0; d < 3; d++) {
      triangle->GetPointIds()->SetId(
          d, mesh.local_node_id_to_idx_[mesh.MeshElements[i_el]
                                            .VerticesOfElement[d]]);
    }
    triangles->InsertNextCell(triangle);
    element_ranks->InsertNextValue(double(mpi_rank));
    element_ids->InsertNextValue(mesh.LocalToGlobalElements[i_el]);
    element_qualities->InsertNextValue(mesh.MeshElements[i_el].quality_);
  }

  auto polydata = vtkSmartPointer<vtkPolyData>::New();

  polydata->SetPoints(vtknodes);
  polydata->SetPolys(triangles);

  polydata->GetCellData()->AddArray(element_ranks);
  polydata->GetCellData()->AddArray(element_ids);
  polydata->GetCellData()->AddArray(element_qualities);

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPPolyDataWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(polydata);

  writer->SetFileName(file_name.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();

  MPI_Barrier(MPI_COMM_WORLD);

  controller->Finalize(1);
}

/**
 * Write a 3d mesh to a file
 *
 * @param mesh The mesh to write to the file
 * @param file_name Path to the file to write into
 */
template <typename MeshType>
void write_mesh_3d(const MeshType &mesh,
                   const std::filesystem::path &file_name) {
  auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  auto element_ranks = detail::vtk_create_array<vtkDoubleArray>("MPI_RANK");
  auto element_qualities = detail::vtk_create_array<vtkDoubleArray>("Quality");

  for (const auto &[_, node] : mesh.nodes)
    vtknodes->InsertNextPoint(node.position[0], node.position[1],
                              node.position[2]);

  auto vtkumesh = vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkumesh->SetPoints(vtknodes);

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  for (int i_el = 0; i_el < mesh.cells.size(); i_el++) {
    auto tetrahedron = vtkSmartPointer<vtkTetra>::New();

    const auto &cell = mesh.cells.at(mesh.simplex_index_to_key[i_el]);
    for (int d = 0; d < 3 + 1; d++)
      tetrahedron->GetPointIds()->SetId(d, cell.nodes[d]);

    vtkumesh->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());

    element_ranks->InsertNextValue(mpi_rank);
    element_qualities->InsertNextValue(cell.data.quality);
  }

  vtkumesh->GetCellData()->AddArray(element_ranks);
  vtkumesh->GetCellData()->AddArray(element_qualities);

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(vtkumesh);
  writer->SetFileName(file_name.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();

  controller->Finalize(1);

  MPI_Barrier(MPI_COMM_WORLD);
}

// https://public.kitware.com/pipermail/paraview/2013-February/027633.html
// The standard rendering pipeline behind surface,
// wireframe and points representations extracts the external
// polygons before rendering. Apply the extract edges,
// shrink, or glyph filters to see internal elements and nodes.

/**
 * Write mesh data to a file.
 *
 * Writes the following files into the output directory:
 * - partition.pvtu
 * - degenerate_elements.pvtu
 * - non_atomistic_elements.pvtu (if display_element_markers is true)
 * - atomistic_elements.pvtu (if display_element_markers is true)
 *
 * @param mesh The mesh to dump
 * @param remesh_tolerance tolerance for triggering remeshing. Output using this function 
 *                          counts the degenerate elements written
 * @param outputfile_directory Target directory
 * @param display_element_markers Include element markers
 * @param display_halo_mesh Include halo mesh
 * @param display_atom_markers Include atom markers
 * @param display_boundary_nodes Include boundary nodes
 */
template <typename MeshType>
void write_vtu_mesh_3d(const MeshType &mesh, const double remesh_tolerance,
                       const std::filesystem::path &output_directory,
                       const bool display_element_markers = true,
                       const bool display_halo_mesh = false,
                       const bool display_atom_markers = true,
                       const bool display_boundary_nodes = true) {
  auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  auto element_volumes = detail::vtk_create_array<vtkDoubleArray>("volume");
  auto element_qualities = detail::vtk_create_array<vtkDoubleArray>("quality");
  auto element_ranks = detail::vtk_create_array<vtkDoubleArray>("ranks");
  auto degenerate_qualities =
      detail::vtk_create_array<vtkDoubleArray>("bad quality");

  for (std::size_t i = 0; i < mesh.nodes.size(); i++) {
    const auto node_key = mesh.node_index_to_key[i];
    vtknodes->InsertNextPoint(mesh.nodes.at(node_key).position[0],
                              mesh.nodes.at(node_key).position[1],
                              mesh.nodes.at(node_key).position[2]);
  }

  auto vtkumesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
  auto degenerate_elements = vtkSmartPointer<vtkUnstructuredGrid>::New();

  auto non_atomistic_elements =
      display_element_markers
          ? std::make_optional(detail::vtk_create_grid_with_points(vtknodes))
          : std::nullopt;
  auto atomistic_elements =
      display_element_markers
          ? std::make_optional(detail::vtk_create_grid_with_points(vtknodes))
          : std::nullopt;

  vtkumesh->SetPoints(vtknodes);
  degenerate_elements->SetPoints(vtknodes);

  int noof_degenerates = 0;

  for (std::size_t i_el = 0; i_el < mesh.cells.size(); i_el++) {
    const auto cell_key = mesh.simplex_index_to_key[i_el];
    const auto &cell = mesh.cells.at(cell_key);

    auto tetrahedron = vtkSmartPointer<vtkTetra>::New();

    for (int d = 0; d < 3 + 1; d++) {
      tetrahedron->GetPointIds()->SetId(
          d, mesh.nodes.at(mesh.cells.at(cell_key).nodes[d]).data.mesh_index);
    }

    vtkumesh->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());

    if (cell.data.quality > remesh_tolerance) {
      noof_degenerates++;

      degenerate_elements->InsertNextCell(tetrahedron->GetCellType(),
                                          tetrahedron->GetPointIds());

      degenerate_qualities->InsertNextValue(cell.data.quality);
    }

    if (non_atomistic_elements && atomistic_elements) {
      if (!cell.data.is_atomistic)
        (*non_atomistic_elements)
            ->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());
      else
        (*atomistic_elements)
            ->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());
    }
    element_qualities->InsertNextValue(cell.data.quality);
    element_volumes->InsertNextValue(cell.data.volume);
    element_ranks->InsertNextValue(mesh.cells.at(cell_key).process_rank);
  }
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  if (display_halo_mesh) {
    std::unordered_map<qcmesh::mesh::NodeId, int> added_halo_nodes;
    auto halo_ctr = mesh.nodes.size();

    for (const auto &halo_node : mesh.halo_nodes) {
      added_halo_nodes.insert(std::make_pair(halo_node.first, halo_ctr));

      vtknodes->InsertNextPoint(halo_node.second.position[0],
                                halo_node.second.position[1],
                                halo_node.second.position[2]);

      halo_ctr++;
    }

    for (const auto &[_, halo_cell] : mesh.halo_cells) {
      auto tetrahedron = vtkSmartPointer<vtkTetra>::New();

      for (int d = 0; d < 3 + 1; d++) {
        const auto is_local =
            qcmesh::mesh::mesh_has_node(mesh, halo_cell.nodes[d], mpi_rank);
        if (is_local)
          tetrahedron->GetPointIds()->SetId(
              d, mesh.nodes.at(halo_cell.nodes[d]).data.mesh_index);
        else {
          if (added_halo_nodes.find(halo_cell.nodes[d]) ==
              added_halo_nodes.end())
            throw exception::QCException{"Halo cell node id could not be found "
                                         "in the added_halo_nodes."};

          tetrahedron->GetPointIds()->SetId(
              d, added_halo_nodes.at(halo_cell.nodes[d]));
        }
      }

      vtkumesh->InsertNextCell(tetrahedron->GetCellType(),
                               tetrahedron->GetPointIds());
      element_ranks->InsertNextValue(halo_cell.process_rank);
    }
  }

  degenerate_elements->GetCellData()->AddArray(degenerate_qualities);

  std::cout << mpi_rank << " total degenerate elements are " << noof_degenerates
            << std::endl;
  if (display_atom_markers) {
    const auto [local_indices, _] =
        meshing::traits::Partition<MeshType>::partition_ghost_indices(mesh);
    std::vector<int> ghost_nodes;
    ghost_nodes.resize(mesh.nodes.size(), 10);
    for (std::size_t i = 0; i < local_indices.size(); i++)
      ghost_nodes[i] = 0;

    auto node_markers =
        detail::vtk_create_array<vtkDoubleArray>("node sampling weights");
    for (const auto val : ghost_nodes)
      node_markers->InsertNextValue(val);
    vtkumesh->GetPointData()->AddArray(node_markers);
  }

  if (display_boundary_nodes) {
    auto boundary_node_markers =
        detail::vtk_create_array<vtkIntArray>("boundary nodes");

    std::vector<int> boundary_marker;
    boundary_marker.resize(mesh.nodes.size(), 0);

    for (std::size_t i_proc = 0; i_proc < mesh.connected_node_procs.size();
         i_proc++)
      for (std::size_t idx : mesh.connected_nodes[i_proc])
        boundary_marker[idx] = 10;

    for (const auto val : boundary_marker)
      boundary_node_markers->InsertNextValue(val);
    vtkumesh->GetPointData()->AddArray(boundary_node_markers);
  }

  vtkumesh->GetCellData()->AddArray(element_qualities);
  vtkumesh->GetCellData()->AddArray(element_volumes);
  vtkumesh->GetCellData()->AddArray(element_ranks);

  const auto filepath =
      std::filesystem::path{output_directory} / "partition.pvtu";
  const auto bad_mesh_file = output_directory / "degenerate_elements.pvtu";

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  auto bad_elements_writer =
      vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(vtkumesh);
  writer->SetFileName(filepath.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();

  bad_elements_writer->SetNumberOfPieces(mpi_size);
  bad_elements_writer->SetStartPiece(mpi_rank);
  bad_elements_writer->SetEndPiece(mpi_rank);

  bad_elements_writer->SetGhostLevel(0);
  bad_elements_writer->SetInputData(degenerate_elements);
  bad_elements_writer->SetFileName(bad_mesh_file.c_str());
  bad_elements_writer->SetDataModeToAscii();
  bad_elements_writer->Update();
  bad_elements_writer->Write();

  if (non_atomistic_elements) {
    auto non_atomistic_elements_writer =
        vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
    non_atomistic_elements_writer->SetNumberOfPieces(mpi_size);
    non_atomistic_elements_writer->SetStartPiece(mpi_rank);
    non_atomistic_elements_writer->SetEndPiece(mpi_rank);

    non_atomistic_elements_writer->SetGhostLevel(0);
    non_atomistic_elements_writer->SetInputData(*non_atomistic_elements);
    const auto weird_file = output_directory / "non_atomistic_elements.pvtu";
    non_atomistic_elements_writer->SetFileName(weird_file.c_str());
    non_atomistic_elements_writer->SetDataModeToAscii();
    non_atomistic_elements_writer->Update();
    non_atomistic_elements_writer->Write();
  }
  if (atomistic_elements) {
    auto atomistic_elements_writer =
        vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
    atomistic_elements_writer->SetNumberOfPieces(mpi_size);
    atomistic_elements_writer->SetStartPiece(mpi_rank);
    atomistic_elements_writer->SetEndPiece(mpi_rank);

    atomistic_elements_writer->SetGhostLevel(0);
    atomistic_elements_writer->SetInputData(*atomistic_elements);
    const auto atomistic_elements_file =
        output_directory / "atomistic_elements.pvtu";
    atomistic_elements_writer->SetFileName(atomistic_elements_file.c_str());
    atomistic_elements_writer->SetDataModeToAscii();
    atomistic_elements_writer->Update();
    atomistic_elements_writer->Write();
  }

  controller->Finalize(1);

  MPI_Barrier(MPI_COMM_WORLD);
}

/**
 * Write minimal mesh data to a file
 *
 * Writes the following files into the output directory:
 * - partition.pvtu
 * - degenerate_elements.pvtu
 * - non_atomistic_elements.pvtu (if display_element_markers is true)
 * - atomistic_elements.pvtu (if display_element_markers is true)
 *
 * @param mesh The mesh to dump
 * @param remesh_tolerance tolerance for triggering remeshing. Output using this function 
 *                          counts the degenerate elements written
 * @param outputfile_directory Target directory
 * @param display_element_markers Include element markers
 */
template <typename MeshType>
void write_vtu_mesh_3d_minimal(const MeshType &mesh, double remesh_tolerance,
                               const std::filesystem::path &output_directory,
                               const bool display_element_markers) {
  auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  auto element_volumes = detail::vtk_create_array<vtkDoubleArray>("volume");
  auto element_qualities = detail::vtk_create_array<vtkDoubleArray>("quality");
  auto element_ranks = detail::vtk_create_array<vtkDoubleArray>("ranks");
  auto degenerate_qualities =
      detail::vtk_create_array<vtkDoubleArray>("bad quality");

  auto non_atomistic_elements =
      display_element_markers
          ? std::make_optional(detail::vtk_create_grid_with_points(vtknodes))
          : std::nullopt;
  auto atomistic_elements =
      display_element_markers
          ? std::make_optional(detail::vtk_create_grid_with_points(vtknodes))
          : std::nullopt;
  for (std::size_t i = 0; i < mesh.nodes.size(); i++) {
    const auto node_key = mesh.node_index_to_key[i];

    vtknodes->InsertNextPoint(mesh.nodes.at(node_key).position[0],
                              mesh.nodes.at(node_key).position[1],
                              mesh.nodes.at(node_key).position[2]);
  }

  auto vtkumesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
  auto degenerate_elements = vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkumesh->SetPoints(vtknodes);
  degenerate_elements->SetPoints(vtknodes);

  int noof_degenerates = 0;

  for (std::size_t i_el = 0; i_el < mesh.cells.size(); i_el++) {
    const auto cell_key = mesh.simplex_index_to_key[i_el];
    const auto &cell = mesh.cells.at(cell_key);
    auto tetrahedron = vtkSmartPointer<vtkTetra>::New();

    for (int d = 0; d < 3 + 1; d++) {
      tetrahedron->GetPointIds()->SetId(
          d, mesh.nodes.at(mesh.cells.at(cell_key).nodes[d]).data.mesh_index);
    }

    vtkumesh->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());

    if (cell.data.quality > remesh_tolerance) {
      noof_degenerates++;
      degenerate_elements->InsertNextCell(tetrahedron->GetCellType(),
                                          tetrahedron->GetPointIds());
      degenerate_qualities->InsertNextValue(cell.data.quality);
    }

    if (non_atomistic_elements && atomistic_elements) {
      if (!cell.data.is_atomistic)
        (*non_atomistic_elements)
            ->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());
      else
        (*atomistic_elements)
            ->InsertNextCell(tetrahedron->GetCellType(),
                             tetrahedron->GetPointIds());
    }
    element_qualities->InsertNextValue(cell.data.quality);
    element_volumes->InsertNextValue(cell.data.volume);
    element_ranks->InsertNextValue(mesh.cells.at(cell_key).process_rank);
  }

  degenerate_elements->GetCellData()->AddArray(degenerate_qualities);

  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  std::cout << mpi_rank << " total degenerate elements are " << noof_degenerates
            << std::endl;

  vtkumesh->GetCellData()->AddArray(element_qualities);
  vtkumesh->GetCellData()->AddArray(element_volumes);
  vtkumesh->GetCellData()->AddArray(element_ranks);

  const auto filepath = output_directory / "partition.pvtu";
  const auto bad_mesh_file = output_directory / "degenerate_elements.pvtu";

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
  auto bad_elements_writer =
      vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(vtkumesh);
  writer->SetFileName(filepath.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();

  bad_elements_writer->SetNumberOfPieces(mpi_size);
  bad_elements_writer->SetStartPiece(mpi_rank);
  bad_elements_writer->SetEndPiece(mpi_rank);

  bad_elements_writer->SetGhostLevel(0);
  bad_elements_writer->SetInputData(degenerate_elements);
  bad_elements_writer->SetFileName(bad_mesh_file.c_str());
  bad_elements_writer->SetDataModeToAscii();
  bad_elements_writer->Update();
  bad_elements_writer->Write();

  if (non_atomistic_elements) {
    auto non_atomistic_elements_writer =
        vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
    non_atomistic_elements_writer->SetNumberOfPieces(mpi_size);
    non_atomistic_elements_writer->SetStartPiece(mpi_rank);
    non_atomistic_elements_writer->SetEndPiece(mpi_rank);

    non_atomistic_elements_writer->SetGhostLevel(0);
    non_atomistic_elements_writer->SetInputData(*non_atomistic_elements);
    const auto weird_file = output_directory / "non_atomistic_elements.pvtu";
    non_atomistic_elements_writer->SetFileName(weird_file.c_str());
    non_atomistic_elements_writer->SetDataModeToAscii();
    non_atomistic_elements_writer->Update();
    non_atomistic_elements_writer->Write();
  }
  if (atomistic_elements) {
    auto atomistic_elements_writer =
        vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
    atomistic_elements_writer->SetNumberOfPieces(mpi_size);
    atomistic_elements_writer->SetStartPiece(mpi_rank);
    atomistic_elements_writer->SetEndPiece(mpi_rank);

    atomistic_elements_writer->SetGhostLevel(0);
    atomistic_elements_writer->SetInputData(*atomistic_elements);
    const auto atomistic_elements_file =
        output_directory / "atomistic_elements.pvtu";
    atomistic_elements_writer->SetFileName(atomistic_elements_file.c_str());
    atomistic_elements_writer->SetDataModeToAscii();
    atomistic_elements_writer->Update();
    atomistic_elements_writer->Write();
  }

  controller->Finalize(1);

  MPI_Barrier(MPI_COMM_WORLD);
}
} // namespace vtkwriter
