// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief function implementations for vtk Outputs
 * @author P. Gupta
 */

#pragma once

#include "constants/constants.hpp"
#include "vtkwriter/detail.hpp"
#include <boost/mpi.hpp>
#include <filesystem>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkMPIController.h>
#include <vtkPointData.h>
#include <vtkRegularPolygonSource.h>
#include <vtkTriangle.h>
#include <vtkXMLPUnstructuredGridWriter.h>

namespace vtkwriter {
namespace detail {
template <typename VTKDATA>
void write_pvtk_files_2d(const VTKDATA &data,
                         const std::filesystem::path &file_name) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = world.rank();
  const auto mpi_size = world.size();

  auto *controller = vtkMPIController::New();
  controller->Initialize();
  vtkMPIController::SetGlobalController(controller);

  auto writer = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

  writer->SetNumberOfPieces(mpi_size);
  writer->SetStartPiece(mpi_rank);
  writer->SetEndPiece(mpi_rank);

  writer->SetGhostLevel(0);
  writer->SetInputData(data);
  writer->SetFileName(file_name.c_str());
  writer->SetDataModeToAscii();
  writer->Update();
  writer->Write();

  controller->Finalize(1);

  MPI_Barrier(MPI_COMM_WORLD);
}
} // namespace detail

/**
 * Write the vertices of a 2d mesh to a file
 *
 * @param mesh The mesh to write to the file
 * @param local_repatom_container data structure containing local repatom data
 * @param force_applicator force applicator for displaying in the vtk files
 * @param output_centro_symmetry if centrosymmetry needs to be written for sampling atoms
 * @param file_name Path to the file to write into
 * @param finite_temperature Zero temperature (false) or finite temperature (true)
 */
template <typename Meshtype, typename RepatomContainer,
          typename ExternalForceApplicator>
void write_vtk_point_data_2d(const Meshtype &mesh,
                             const RepatomContainer &local_repatom_container,
                             const ExternalForceApplicator &force_applicator,
                             bool, const std::filesystem::path &file_name,
                             const bool finite_temperature) {
  const auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  const auto triangles = vtkSmartPointer<vtkCellArray>::New();
  const auto element_ranks =
      detail::vtk_create_array<vtkDoubleArray>("MPI_RANK");
  const auto element_qualities =
      detail::vtk_create_array<vtkDoubleArray>("Quality");
  const auto repatom_forces =
      detail::vtk_create_array<vtkDoubleArray>("Force", 3);
  repatom_forces->SetNumberOfTuples(local_repatom_container.locations_.size());
  const auto repatom_weights =
      detail::vtk_create_array<vtkDoubleArray>("Weights");

  if (finite_temperature) {
    auto repatom_temperature = detail::vtk_create_array<vtkDoubleArray>("T");
    auto repatom_position_variance =
        detail::vtk_create_array<vtkDoubleArray>("Sigma");
    auto repatom_thermal_force =
        detail::vtk_create_array<vtkDoubleArray>("ThermalForce");
    for (int atom_ctr = 0; atom_ctr < local_repatom_container.locations_.size();
         atom_ctr++) {
      repatom_temperature->InsertNextValue(
          local_repatom_container.thermal_coordinates_[atom_ctr][0] /
          (local_repatom_container.mass_[atom_ctr] * constants::k_Boltzmann));
      repatom_position_variance->InsertNextValue(
          local_repatom_container.thermal_coordinates_[atom_ctr][1]);
      repatom_thermal_force->InsertNextValue(
          local_repatom_container.thermal_forces_[atom_ctr]);
    }
  }

  for (int atom_ctr = 0; atom_ctr < local_repatom_container.locations_.size();
       atom_ctr++) {
    vtknodes->InsertNextPoint(local_repatom_container.locations_[atom_ctr][0],
                              local_repatom_container.locations_[atom_ctr][1],
                              0.0);

    repatom_forces->SetTuple3(
        atom_ctr, local_repatom_container.forces_[atom_ctr][0],
        local_repatom_container.forces_[atom_ctr][1], 0.0);

    repatom_weights->InsertNextValue(
        local_repatom_container.weights_[atom_ctr]);
  }

  const auto mpi_rank = boost::mpi::communicator{}.rank();

  for (int i_el = 0; i_el < mesh.NoofElements; i_el++) {
    auto triangle = vtkSmartPointer<vtkTriangle>::New();

    for (int d = 0; d < 3; d++) {
      triangle->GetPointIds()->SetId(
          d, mesh.local_node_id_to_idx_[mesh.MeshElements[i_el]
                                            .VerticesOfElement[d]]);
    }
    triangles->InsertNextCell(triangle);
    element_ranks->InsertNextValue(double(mpi_rank));
    element_qualities->InsertNextValue(mesh.MeshElements[i_el].quality_);
  }

  auto polydata = vtkSmartPointer<vtkPolyData>::New();

  polydata->SetPoints(vtknodes);
  polydata->SetPolys(triangles);
  polydata->GetPointData()->SetVectors(repatom_forces);
  polydata->GetPointData()->AddArray(repatom_weights);
  polydata->GetCellData()->AddArray(element_ranks);
  polydata->GetCellData()->AddArray(element_qualities);

  // *********** indenter file ******************** //
  auto indenter = vtkSmartPointer<vtkPolyData>::New();

  if (force_applicator.OutputFlag()) {
    auto polygon_source = vtkSmartPointer<vtkRegularPolygonSource>::New();
    polygon_source->SetNumberOfSides(100);
    polygon_source->SetRadius(force_applicator.radius_);
    polygon_source->SetCenter(force_applicator.center_[0],
                              force_applicator.center_[1], 0.0);
    polygon_source->Update();

    auto polygon_points =
        polygon_source->GetOutput()->GetPoints()->GetNumberOfPoints();

    auto indenter_vectors = vtkSmartPointer<vtkDoubleArray>::New();
    indenter_vectors->SetNumberOfComponents(3);
    indenter_vectors->SetNumberOfTuples(polygon_points);
    indenter_vectors->SetName("Force");

    auto indenter_scalars = vtkSmartPointer<vtkDoubleArray>::New();
    indenter_scalars->SetNumberOfComponents(1);
    indenter_scalars->SetName("Weights");

    for (vtkIdType i = 0; i < polygon_points; i++) {
      indenter_vectors->SetTuple3(i, 0.0, 0.0, 0.0);
      indenter_scalars->InsertNextValue(0.0);
    }

    indenter->DeepCopy(polygon_source->GetOutput());
    indenter->GetPointData()->SetVectors(indenter_vectors);
    indenter->GetPointData()->AddArray(indenter_scalars);
  }

  auto append_filter = vtkSmartPointer<vtkAppendPolyData>::New();
  append_filter->AddInputData(polydata);
  append_filter->AddInputData(indenter);
  append_filter->Update();

  detail::write_pvtk_files_2d(append_filter->GetOutput(), file_name);
}
} // namespace vtkwriter
