// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @authors P. Gupta
 */

#include "vtkwriter/write_vtu_triangulation.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "vtkCellData.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkPointData.h"
#include "vtkTetra.h"
#include "vtkTriangle.h"
#include "vtkUnstructuredGrid.h"
#include "vtkXMLUnstructuredGridWriter.h"
#include <filesystem>

namespace vtkwriter {
namespace {

vtkSmartPointer<vtkTriangle>
to_simplex(const std::array<std::size_t, 3> &cell) {
  auto triangle = vtkSmartPointer<vtkTriangle>::New();
  for (std::size_t d = 0; d < cell.size(); d++)
    triangle->GetPointIds()->SetId(static_cast<vtkIdType>(d),
                                   static_cast<vtkIdType>(cell[d]));
  return triangle;
}

vtkSmartPointer<vtkTetra> to_simplex(const std::array<std::size_t, 4> &cell) {
  auto tetrahedron = vtkSmartPointer<vtkTetra>::New();
  for (std::size_t d = 0; d < cell.size(); d++)
    tetrahedron->GetPointIds()->SetId(static_cast<vtkIdType>(d),
                                      static_cast<vtkIdType>(cell[d]));
  return tetrahedron;
}

template <std::size_t Dimension>
void write_vtu_triangulation_nd(
    const std::vector<std::array<double, Dimension>> &points,
    const std::vector<std::array<std::size_t, Dimension + 1>> &triangulation,
    const std::optional<double> zero_volume_tolerance,
    const std::filesystem::path &filename) {

  auto vtknodes = vtkSmartPointer<vtkPoints>::New();
  auto element_volumes = vtkSmartPointer<vtkDoubleArray>::New();
  auto dummy_data = vtkSmartPointer<vtkDoubleArray>::New();
  auto element_markers = vtkSmartPointer<vtkIntArray>::New();

  element_volumes->SetNumberOfComponents(1);
  element_volumes->SetName(Dimension == 3 ? "volume" : "area");

  dummy_data->SetNumberOfComponents(1);
  dummy_data->SetName("dummy");

  if (zero_volume_tolerance.has_value()) {
    element_markers->SetNumberOfComponents(1);
    element_markers->SetName("degeneracyMarkers");
  }

  for (const auto &p : points) {
    vtknodes->InsertNextPoint(p[0], p[1], Dimension == 3 ? p[2] : 0.);
    dummy_data->InsertNextValue(0.0);
  }

  auto vtkumesh = vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkumesh->SetPoints(vtknodes);

  for (const auto &cell : triangulation) {
    auto vertices = std::array<std::array<double, Dimension>, Dimension + 1>{};
    for (std::size_t d = 0; d < cell.size(); d++)
      vertices[d] = points[cell[d]];
    const auto simplex = to_simplex(cell);
    vtkumesh->InsertNextCell(simplex->GetCellType(), simplex->GetPointIds());

    const auto element_volume = qcmesh::geometry::simplex_volume(vertices);
    element_volumes->InsertNextValue(element_volume);

    if (zero_volume_tolerance.has_value())
      element_markers->InsertNextValue(
          static_cast<int>(element_volume < zero_volume_tolerance.value()));
  }
  vtkumesh->GetPointData()->AddArray(dummy_data);
  if (zero_volume_tolerance.has_value())
    vtkumesh->GetCellData()->AddArray(element_markers);
  vtkumesh->GetCellData()->AddArray(element_volumes);

  auto writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
  writer->SetInputData(vtkumesh);
  writer->SetFileName(filename.c_str());
  writer->SetDataModeToAscii();

  writer->Update();

  writer->Write();
}

} // namespace

void write_vtu_triangulation(
    const std::vector<std::array<double, 3>> &points,
    const std::vector<std::array<std::size_t, 4>> &triangulation,
    const std::optional<double> zero_volume_tolerance,
    const std::filesystem::path &filename) {
  write_vtu_triangulation_nd(points, triangulation, zero_volume_tolerance,
                             filename);
}
void write_vtu_triangulation(
    const std::vector<std::array<double, 2>> &points,
    const std::vector<std::array<std::size_t, 3>> &triangulation,
    const std::optional<double> zero_volume_tolerance,
    const std::filesystem::path &filename) {
  write_vtu_triangulation_nd(points, triangulation, zero_volume_tolerance,
                             filename);
}

} // namespace vtkwriter
