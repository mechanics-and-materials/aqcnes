// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkwriter/detail.hpp"
#include <gmock/gmock.h>

namespace vtkwriter::detail {
namespace {

struct VtkwriterTest : ::testing::Test {};

TEST_F(VtkwriterTest, vtk_create_grid_with_points_works) {
  auto points = vtkSmartPointer<vtkPoints>::New();
  points->InsertNextPoint(0.0, 0.0, 0.0);
  auto grid = vtk_create_grid_with_points(points);
  auto *const points_in_grid = grid->GetPoints();
  EXPECT_EQ(points->GetNumberOfPoints(), points_in_grid->GetNumberOfPoints());
}

TEST_F(VtkwriterTest, vtk_create_array_works_for_vtkIntArray) {
  auto array = vtk_create_array<vtkIntArray>("test array", 5);
  EXPECT_EQ(array->GetNumberOfComponents(), 5);
  EXPECT_EQ(std::string{array->GetName()}, std::string{"test array"});
}

} // namespace
} // namespace vtkwriter::detail
