// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#pragma once
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace testing {
namespace detail {
/**
 * @brief Returns true if and only if values contains the same points as reference.
 * Equality is checked via DoubleEq instead of `==`.
 */
template <std::size_t Dimension>
inline bool unordered_point_array_eq(
    const std::vector<std::array<double, Dimension>> &value,
    const std::vector<std::array<double, Dimension>> &reference,
    ::testing::MatchResultListener *const stream) noexcept {
  auto matchers =
      std::vector<testing::Matcher<std::array<double, Dimension>>>{};
  for (const auto &p : reference)
    matchers.emplace_back(qcmesh::testing::DoubleArrayEq(p));
  return ::testing::ExplainMatchResult(
      ::testing::UnorderedElementsAreArray(matchers), value, stream);
}
} // namespace detail

/**
 * @brief Variant of UnorderedElementsAreArray using DoubleEq instead of `==`.
 */
MATCHER_P(UnorderedPointArrayEq, reference,
          "value " + std::string(negation ? "not " : "") + "equal to " +
              ::testing::PrintToString(reference)) {
  return detail::unordered_point_array_eq(arg, reference, result_listener);
}
} // namespace testing
