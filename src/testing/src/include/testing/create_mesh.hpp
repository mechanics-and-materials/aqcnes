// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author M. Spinola
 */

#pragma once

#include "meshing/qc_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_vertex.hpp"
#include <vector>

namespace testing {

using qcmesh::mesh::testing::BareMesh;
using qcmesh::mesh::testing::LatticeVertex;

/**
 * @brief Create a distributed mesh based on a @ref BareMesh.
 *
 * Lattice vectors of cells will be initialized based on the lattice coordinates of the vertices.
 */
meshing::QCMesh<3, 3>
create_mesh(const BareMesh<LatticeVertex> &lattice_mesh,
            const bool error_on_gegenerate_cells = true,
            const std::vector<materials::MaterialBase<3>> &materials =
                std::vector<materials::MaterialBase<3>>{},
            const double phi_ref = 4.86475337832, const double t_ref = 300.0);
/**
 * @brief Create a non-distributed mesh (same mesh on all processes) based on a @ref BareMesh.
 *
 * Lattice vectors of cells will be initialized based on the lattice coordinates of the vertices.
 */
meshing::QCMesh<3, 3>
create_local_mesh(const BareMesh<LatticeVertex> &lattice_mesh,
                  const bool error_on_gegenerate_cells = true,
                  const std::vector<materials::MaterialBase<3>> &materials =
                      std::vector<materials::MaterialBase<3>>{},
                  const double phi_ref = 4.86475337832,
                  const double t_ref = 300.0);
/**
 * @brief Create a distributed mesh based on a @ref BareMesh.
 */
meshing::QCMesh<3, 3>
create_mesh(const BareMesh<std::array<double, 3>> &mesh,
            const std::vector<materials::MaterialBase<3>> &materials =
                std::vector<materials::MaterialBase<3>>{},
            const double phi_ref = 4.86475337832, const double t_ref = 300.0);
/**
 * @brief Create a non-distributed mesh (same mesh on all processes) based on a @ref BareMesh.
 */
meshing::QCMesh<3, 3>
create_local_mesh(const BareMesh<std::array<double, 3>> &mesh,
                  const std::vector<materials::MaterialBase<3>> &materials =
                      std::vector<materials::MaterialBase<3>>{},
                  const double phi_ref = 4.86475337832,
                  const double t_ref = 300.0);

} // namespace testing
