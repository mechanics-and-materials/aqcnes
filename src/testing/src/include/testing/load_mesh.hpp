// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "meshing/qc_mesh.hpp"
#include <filesystem>

namespace testing {

/**
 * @brief Loads a mesh from the file at `filename`, assigns materials, and
 * initializes it.
 */
meshing::QCMesh<3, 3> load_mesh(const std::filesystem::path &mesh_file,
                                const std::filesystem::path &lattice_file);

} // namespace testing
