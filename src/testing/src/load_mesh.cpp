// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "testing/load_mesh.hpp"
#include "materials/create_material_base.hpp"
#include "meshing/load_mesh_from_nc.hpp"

namespace testing {

meshing::QCMesh<3, 3> load_mesh(const std::filesystem::path &mesh_file,
                                const std::filesystem::path &lattice_file) {
  constexpr std::size_t DIM = 3;
  const auto cache = lattice::load_lattice_cache<DIM>(lattice_file);
  // From ExtendedFinnisSinclairCopper:
  const auto atomic_masses = std::vector<double>{63.546};
  const auto neighbor_cutoff_radius = 4.32;
  const auto lattice_structure = (DIM == 3) ? lattice::LatticeStructure::FCC
                                            : lattice::LatticeStructure::HCP;
  const auto materials = std::vector{materials::create_material_base<DIM>(
      cache, neighbor_cutoff_radius, atomic_masses, lattice_structure)};
  const auto node_effective_radius = 2.5;
  const auto minimum_barycenter_weight = 1.0;
  const auto phi_ref = 5.545;
  const auto t_ref = 300.0;
  const auto initial_impurity_concentrations = std::array{0.};
  const auto periodicity =
      std::array<std::optional<meshing::PeriodicityParameters>, DIM>{};
  const auto fixed_box_boundaries =
      std::optional<std::array<std::array<double, DIM>, 2>>{};
  return meshing::load_mesh_from_nc(
      mesh_file, materials, phi_ref, t_ref, initial_impurity_concentrations,
      node_effective_radius, minimum_barycenter_weight, periodicity,
      fixed_box_boundaries);
}

} // namespace testing
