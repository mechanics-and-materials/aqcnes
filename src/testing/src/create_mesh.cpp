// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
/** @file
 * @author G. Bräunlich, M. Spinola
 */

#include "testing/create_mesh.hpp"
#include "meshing/build_repatoms.hpp"
#include "meshing/create_mesh.hpp"
#include "meshing/renumber_mesh.hpp"
#include "qcmesh/mpi/mpi_sub_range.hpp"
#include <set>

namespace array_ops = qcmesh::array_ops;

namespace testing {
namespace {

template <std::size_t D, std::size_t K> struct LatticeCell {
  qcmesh::mesh::SimplexCellId id{};
  std::array<qcmesh::mesh::NodeId, K + 1> nodes{};
  std::array<std::array<double, D>, D> basis{};
};

/**
 * @brief Wrapper Type attaching an id.
 */
template <class IdType, class T> struct WithId {
  IdType id{};
  T inner{};
};

template <class NodeType>
using WithNodeId = WithId<qcmesh::mesh::NodeId, NodeType>;

} // namespace
} // namespace testing

/**
 * @brief Implement @ref qcmesh::mesh::traits::LatticeBasis for @ref ::testing::LatticeCell.
 */
template <std::size_t D, std::size_t K>
struct qcmesh::mesh::traits::LatticeBasis<::testing::LatticeCell<D, K>> {
  constexpr static std::size_t DIMENSION = D;
  static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(::testing::LatticeCell<D, K> &cell) {
    return cell.basis;
  }
  static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(const ::testing::LatticeCell<D, K> &cell) {
    return cell.basis;
  }
};

/**
 * @brief Implement @ref meshing::traits::CellNodes for @ref ::testing::LatticeCell.
 */
template <std::size_t D, std::size_t K>
struct meshing::traits::CellNodes<::testing::LatticeCell<D, K>> {
  constexpr static std::size_t DIMENSION = K;
  static const std::array<qcmesh::mesh::NodeId, K + 1> &
  cell_nodes(const ::testing::LatticeCell<D, K> &cell) {
    return cell.nodes;
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::Position for @ref testing::WithId.
 */
template <class NodeType>
struct qcmesh::mesh::traits::Position<
    ::testing::WithNodeId<NodeType>,
    std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
        qcmesh::mesh::traits::Position<NodeType>>>> {
  constexpr static std::size_t DIMENSION =
      qcmesh::mesh::traits::Position<NodeType>::DIMENSION;
  static const std::array<double, DIMENSION> &
  position(const ::testing::WithNodeId<NodeType> &node) {
    return qcmesh::mesh::traits::Position<NodeType>::position(node.inner);
  }
};

/**
 * @brief Implement @ref qcmesh::mesh::traits::LatticeCoordinate for @ref testing::WithId.
 */
template <class NodeType>
struct qcmesh::mesh::traits::LatticeCoordinate<
    ::testing::WithNodeId<NodeType>,
    std::enable_if_t<qcmesh::mesh::traits::IS_IMPLEMENTED<
        qcmesh::mesh::traits::LatticeCoordinate<NodeType>>>> {
  constexpr static std::size_t DIMENSION =
      qcmesh::mesh::traits::LatticeCoordinate<NodeType>::DIMENSION;

  static const std::array<int, DIMENSION> &
  lattice_coordinate(const ::testing::WithNodeId<NodeType> &node) {
    return qcmesh::mesh::traits::LatticeCoordinate<
        NodeType>::lattice_coordinate(node.inner);
  }
};

/**
 * @brief Implement @ref meshing::traits::Id for @ref testing::WithId.
 */
template <class I, class T> struct meshing::traits::Id<testing::WithId<I, T>> {
  using IdType = I;
  static IdType id(const testing::WithId<I, T> &obj) { return obj.id; }
};
/**
 * @brief Implement @ref meshing::traits::Id for @ref ::testing::LatticeCell.
 */
template <std::size_t D, std::size_t K>
struct meshing::traits::Id<::testing::LatticeCell<D, K>> {
  using IdType = qcmesh::mesh::SimplexCellId;
  static IdType id(const ::testing::LatticeCell<D, K> &cell) { return cell.id; }
};

namespace testing {
namespace {

using QCMesh = meshing::QCMesh<3, 3>;

template <class T, std::size_t N, std::size_t M>
std::string to_string(const std::array<std::array<T, N>, M> &matrix) {
  auto repr = std::string{"["};
  for (const auto &row : matrix) {
    repr += '[';
    for (std::size_t i = 0; i < row.size(); i++) {
      repr += std::to_string(row[i]);
      if (i + 1 != row.size())
        repr += ", ";
    }
    repr += ']';
  }
  repr += ']';
  return repr;
}

template <class NodeType>
std::array<std::array<double, 3>, 3> cell_basis_from_lattice_vertices(
    const std::vector<NodeType> &lattice_vertices,
    const std::array<std::size_t, 4> &vertex_indices,
    const bool error_on_gegenerate_cells) {
  const std::array<std::array<int, 3>, 3> n =
      qcmesh::geometry::simplex_jacobian(qcmesh::mesh::cell_lattice_coordinates(
          lattice_vertices, vertex_indices));
  if (array_ops::as_eigen_matrix(n).determinant() == 0 &&
      error_on_gegenerate_cells)
    throw exception::QCException{std::string{"cell ("} +
                                 std::to_string(vertex_indices[0]) + ", " +
                                 std::to_string(vertex_indices[1]) + ", " +
                                 std::to_string(vertex_indices[2]) + ", " +
                                 std::to_string(vertex_indices[3]) +
                                 "): " + "det(N) = 0 for N = " + to_string(n)};
  using PositionTrait = typename qcmesh::mesh::traits::Position<NodeType>;
  const std::array<std::array<double, 3>, 3> jac =
      qcmesh::geometry::simplex_jacobian(std::array{
          PositionTrait::position(lattice_vertices[vertex_indices[0]]),
          PositionTrait::position(lattice_vertices[vertex_indices[1]]),
          PositionTrait::position(lattice_vertices[vertex_indices[2]]),
          PositionTrait::position(lattice_vertices[vertex_indices[3]])});
  constexpr double TOLERANCE = 1.E-10;
  if (std::abs(array_ops::as_eigen_matrix(jac).determinant()) < TOLERANCE &&
      error_on_gegenerate_cells)
    throw exception::QCException{"det(jac) below tolerance"};
  auto cell_basis = std::array<std::array<double, 3>, 3>{};
  array_ops::as_eigen_matrix(cell_basis) =
      (array_ops::as_eigen_matrix(jac).transpose() *
       array_ops::as_eigen_matrix(n).transpose().cast<double>().inverse());
  return cell_basis;
}

template <class NodeType>
std::array<std::array<double, 3>, 3> cell_basis_from_points_fallback(
    const std::vector<NodeType> &vertices,
    const std::array<std::size_t, 4> &vertex_indices, const bool) {
  // TODO: get rid of private API:
  return qcmesh::geometry::simplex_jacobian(
      qcmesh::mesh::detail::cell_node_positions(vertices, vertex_indices));
}

template <class NodeType>
std::array<std::array<double, 3>, 3>
cell_basis_from_points(const std::vector<NodeType> &vertices,
                       const std::array<std::size_t, 4> &vertex_indices,
                       const bool error_on_gegenerate_cells) {
  if constexpr (qcmesh::mesh::traits::IS_IMPLEMENTED<
                    qcmesh::mesh::traits::LatticeCoordinate<NodeType>>)
    return cell_basis_from_lattice_vertices(vertices, vertex_indices,
                                            error_on_gegenerate_cells);
  else
    return cell_basis_from_points_fallback(vertices, vertex_indices,
                                           error_on_gegenerate_cells);
}

template <std::size_t K>
std::array<qcmesh::mesh::NodeId, K>
indexed_nodes(const std::array<std::size_t, K> &node_indices) {
  auto node_ids = std::array<qcmesh::mesh::NodeId, K>{};
  for (std::size_t i = 0; i < K; i++)
    node_ids[i] = qcmesh::mesh::NodeId{node_indices[i]};
  return node_ids;
}

/**
 * @brief Restrict the mesh to a subset of all global cells based on the MPI rank.
 */
template <std::size_t D, std::size_t K, class NodeType>
std::vector<WithNodeId<NodeType>>
restrict_cells(const std::vector<NodeType> &nodes,
               std::vector<LatticeCell<D, K>> &cells) {
  auto restricted_nodes = std::vector<WithNodeId<NodeType>>{};
  const auto [start_idx, range_size] = qcmesh::mpi::mpi_sub_range(cells.size());
  const auto end_idx = start_idx + range_size;
  auto usage_table = std::vector<bool>(nodes.size(), false);
  auto n_nodes = std::size_t{0};
  for (std::size_t idx = start_idx; idx < end_idx; idx++)
    for (const auto node_idx : cells[idx].nodes) {
      if (!usage_table[static_cast<std::size_t>(node_idx)])
        ++n_nodes;
      usage_table[static_cast<std::size_t>(node_idx)] = true;
    }
  restricted_nodes.reserve(n_nodes);
  for (std::size_t i = 0; i < usage_table.size(); i++)
    if (usage_table[i])
      restricted_nodes.emplace_back(
          WithNodeId<NodeType>{qcmesh::mesh::NodeId{i}, nodes[i]});
  cells.erase(cells.begin() + end_idx, cells.end());
  cells.erase(cells.begin(), cells.begin() + start_idx);
  return restricted_nodes;
}

template <class NodeType>
std::vector<LatticeCell<3, 3>>
to_lattice_cells(const std::vector<NodeType> &nodes,
                 const std::vector<std::array<std::size_t, 4>> &cells,
                 const bool error_on_gegenerate_cells) {
  auto lattice_cells = std::vector<LatticeCell<3, 3>>{};
  lattice_cells.reserve(cells.size());
  auto id_counter = std::size_t{0};
  for (const auto &cell : cells) {
    const auto cell_basis =
        cell_basis_from_points(nodes, cell, error_on_gegenerate_cells);
    lattice_cells.emplace_back(
        LatticeCell<3, 3>{qcmesh::mesh::SimplexCellId{id_counter++},
                          indexed_nodes(cell), cell_basis});
  }
  return lattice_cells;
}

namespace implementation {

std::vector<materials::MaterialBase<3>> default_materials() {
  return {materials::MaterialBase<3>{
      std::array<std::array<double, 3>, 3>{{
          {1.0, 0.0, 0.0},
          {0.0, 1.0, 0.0},
          {0.0, 0.0, 1.0},
      }},                          // basis_vectors
      std::array{1.0, 1.0, 1.0},   // lattice_parameter_vec
      {},                          // offset_coeffs
      std::vector<double>{63.546}, // atomic_masses
      4.32,                        // neighbor_cutoff_radius
  }};
}
template <class NodeType>
QCMesh create_mesh(const std::vector<NodeType> &nodes,
                   const std::vector<LatticeCell<3, 3>> &cells,
                   const std::vector<materials::MaterialBase<3>> &materials,
                   const double phi_ref, const double t_ref) {
  constexpr double NODE_EFFECTIVE_RADIUS = 2.5;
  constexpr double MINIMUM_BARYCENTER_WEIGHT = 1.0;
  constexpr meshing::NodalDataMultispecies<QCMesh::NOI> CONC_REF{};

  auto mesh = meshing::create_mesh(
      nodes, cells, materials.empty() ? default_materials() : materials);

  build_repatoms(mesh, phi_ref, t_ref, CONC_REF);
  mesh.node_effective_radius = NODE_EFFECTIVE_RADIUS;
  mesh.minimum_barycenter_weight = MINIMUM_BARYCENTER_WEIGHT;
  mesh.build_sampling_atoms();
  mesh.assign_repatom_weights();

  return mesh;
}

template <class NodeType>
QCMesh create_mesh(const BareMesh<NodeType> &lattice_mesh,
                   const bool error_on_gegenerate_cells,
                   const std::vector<materials::MaterialBase<3>> &materials,
                   const double phi_ref, const double t_ref) {
  auto lattice_cells = to_lattice_cells(
      lattice_mesh.vertices, lattice_mesh.cells, error_on_gegenerate_cells);
  const auto nodes = restrict_cells(lattice_mesh.vertices, lattice_cells);
  auto mesh = create_mesh(nodes, lattice_cells, materials, phi_ref, t_ref);
  setup_process_boundary(mesh);
  return mesh;
}
template <class NodeType>
QCMesh
create_local_mesh(const BareMesh<NodeType> &lattice_mesh,
                  const bool error_on_gegenerate_cells,
                  const std::vector<materials::MaterialBase<3>> &materials,
                  const double phi_ref, const double t_ref) {
  const auto lattice_cells = to_lattice_cells(
      lattice_mesh.vertices, lattice_mesh.cells, error_on_gegenerate_cells);
  return create_mesh(lattice_mesh.vertices, lattice_cells, materials, phi_ref,
                     t_ref);
}

} // namespace implementation
} // namespace

QCMesh create_mesh(const BareMesh<LatticeVertex> &lattice_mesh,
                   const bool error_on_gegenerate_cells,
                   const std::vector<materials::MaterialBase<3>> &materials,
                   const double phi_ref, const double t_ref) {
  return implementation::create_mesh(lattice_mesh, error_on_gegenerate_cells,
                                     materials, phi_ref, t_ref);
}
QCMesh
create_local_mesh(const BareMesh<LatticeVertex> &lattice_mesh,
                  const bool error_on_gegenerate_cells,
                  const std::vector<materials::MaterialBase<3>> &materials,
                  const double phi_ref, const double t_ref) {
  return implementation::create_local_mesh(
      lattice_mesh, error_on_gegenerate_cells, materials, phi_ref, t_ref);
}
QCMesh create_mesh(const BareMesh<geometry::Point<3>> &bare_mesh,
                   const std::vector<materials::MaterialBase<3>> &materials,
                   const double phi_ref, const double t_ref) {
  return implementation::create_mesh(bare_mesh, false, materials, phi_ref,
                                     t_ref);
}
QCMesh
create_local_mesh(const BareMesh<geometry::Point<3>> &bare_mesh,
                  const std::vector<materials::MaterialBase<3>> &materials,
                  const double phi_ref, const double t_ref) {
  return implementation::create_local_mesh(bare_mesh, false, materials, phi_ref,
                                           t_ref);
}

} // namespace testing
