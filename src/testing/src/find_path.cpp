// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.

#include "testing/find_path.hpp"
#include "exception/qc_exception.hpp"

namespace testing::io {

std::filesystem::path find_path(const std::filesystem::path &path) {
  const auto work_dir = std::filesystem::current_path();
  for (auto root_candidate = work_dir;
       root_candidate.parent_path() != root_candidate;
       root_candidate = root_candidate.parent_path()) {
    auto candidate = root_candidate / path;
    if (std::filesystem::exists(candidate))
      return candidate;
  }
  throw exception::QCException(std::string{"Could not find "} + path.string() +
                               " in the working dir " + work_dir.string() +
                               " or its parents");
}

} // namespace testing::io
