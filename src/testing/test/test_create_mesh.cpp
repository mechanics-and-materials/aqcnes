// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of aqcnes.
//
// aqcnes is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// aqcnes. If not, see <https://www.gnu.org/licenses/>.
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/face_connection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "testing/create_mesh.hpp"
#include <gmock/gmock.h>
#include <set>

namespace testing {
namespace {

TEST(test_create_mesh, create_mesh_creates_correct_connectivity) {
  const auto mesh =
      testing::create_mesh(qcmesh::mesh::testing::mesh_lattice_4x4x4(), false);
  EXPECT_THAT(
      qcmesh::mesh::testing::collect_intrinsic_face_connections(mesh.cells),
      ::testing::ContainerEq(
          qcmesh::mesh::testing::collect_extrinsic_face_connections(
              mesh.cells)));
}

TEST(test_create_mesh, create_mesh_includes_all_referenced_nodes) {
  const auto mesh =
      testing::create_mesh(qcmesh::mesh::testing::mesh_lattice_4x4x4(), false);
  auto referenced_nodes = std::set<qcmesh::mesh::NodeId>{};
  for (const auto &[_, cell] : mesh.cells)
    for (const auto node_id : cell.nodes)
      referenced_nodes.emplace(node_id);
  auto available_nodes = std::set<qcmesh::mesh::NodeId>{};
  for (const auto &[node_id, _] : mesh.nodes)
    available_nodes.emplace(node_id);
  EXPECT_THAT(referenced_nodes, ::testing::ContainerEq(available_nodes));
}

} // namespace
} // namespace testing
