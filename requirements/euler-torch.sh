# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of aqcnes.
#
# aqcnes is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# aqcnes is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# aqcnes. If not, see <https://www.gnu.org/licenses/>.

# Enable M&M overlay:
module use /cluster/project/mandm/cluster_apps/lmodules/Core

module load \
       gcc/9.3.0 \
       openmpi/4.1.4 \
       cmake \
       ninja \
       python \
       eigen/3.4.0 \
       gmp \
       gsl \
       cgal \
       boost/1.68.0 \
       petsc \
       mpfr \
       kim-api \
       nlohmann-json \
       scotch/6.1.2 \
       parallel-netcdf \
       libtorch/1.11.0

# modules from M&M overlay:
module load \
       vtk/9.0.1 \
       qcmesh-src/1.0.0 \
       googletest-src/1.14.0

pip install check-jsonschema
